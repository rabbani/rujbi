<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lcustomer {

	//Customer add form
	public function customer_add_form()
	{
		$CI =& get_instance();
		$CI->load->model('Customers');
		$country_list 	= $CI->Customers->country_list();

		$data = array(
				'title' => display('add_customer'),
				'country_list' => $country_list
			);
		$customerForm = $CI->parser->parse('customer/add_customer_form',$data,true);
		return $customerForm;
	}

	//Retrieve  Customer List	
	public function customer_list($links,$per_page,$page,$mobile,$email,$customer)
	{
		$CI =& get_instance();
		$CI->load->model('Customers');
		$customers_list = $CI->Customers->customer_list($per_page,$page,$mobile,$email,$customer);
		$i=0;
		$total=0;
		if(!empty($customers_list)){	
			foreach($customers_list as $k=>$v){$i++;
			   $customers_list[$k]['sl']=$page+$i;
			}
		}
		$data = array(
				'title'		     => display('manage_customer'),
				'customers_list' => $customers_list,
				'links' 		 => $links,
			);
		$customerList = $CI->parser->parse('customer/customer',$data,true);
		return $customerList;
	}

	//Insert customer
	public function insert_customer($data)
	{
		$CI =& get_instance();
		$CI->load->model('Customers');
        $CI->Customers->customer_entry($data);
		return true;
	}
	
	//customer Edit Data
	public function customer_edit_data($customer_id)
	{
		$CI =& get_instance();
		$CI->load->model('Customers');
		$customer_detail = $CI->Customers->retrieve_customer_editdata($customer_id);

		$state_list = $CI->Customers->select_city_country_id($customer_detail[0]['country']);

		$country_list 	= $CI->Customers->country_list();
		$data=array(
			'title'			=>display('customer_edit'),
			'customer_id' 	=> $customer_detail[0]['customer_id'],
			'customer_name' => $customer_detail[0]['customer_name'],
			'customer_address' => $customer_detail[0]['customer_short_address'],
			'customer_mobile' => $customer_detail[0]['customer_mobile'],
			'customer_email' => $customer_detail[0]['customer_email'],
			'customer_address_1' => $customer_detail[0]['customer_address_1'],
			'customer_address_2' => $customer_detail[0]['customer_address_2'],
			'city' 			=> $customer_detail[0]['city'],
			'state' 		=> $customer_detail[0]['state'],
			'country' 		=> $customer_detail[0]['country'],
			'zip' 			=> $customer_detail[0]['zip'],
			'company' 			=> $customer_detail[0]['company'],
			'status' 		=> $customer_detail[0]['status'],
			'state_name' 		=> $customer_detail[0]['state'],
			'country_id' 		=> $customer_detail[0]['country'],
			'country_list' 	=> $country_list,
			'state_list' 		=> $state_list,
			);
		$chapterList = $CI->parser->parse('customer/edit_customer_form',$data,true);
		return $chapterList;
	}
	
	//Customer details
	public function customer_details($customer_id=null,$customer_mobile=null,$email=null)
	{
		$CI =& get_instance();
		$CI->load->model('Customers');
		$CI->load->model('Soft_settings');
		$CI->load->library('occational');

		$customer_detail = $CI->Customers->customer_personal_data($customer_id,$customer_mobile,$email);
		$salesData 		 = $CI->Customers->invoice_data($customer_id);
		$customer_list 	 = $CI->Customers->customer_list();
	
		$totalSales 	= 0;
		$totaSalesAmt 	= 0;

		if(!empty($salesData)){	
			foreach($salesData as $k=>$v){
				$salesData[$k]['final_date'] = $CI->occational->dateConvert($salesData[$k]['date']);
				$totaSalesAmt = ($totaSalesAmt + $salesData[$k]['total_amount']);
			}
		}

		$company_info 		= $CI->Customers->retrieve_company();
		$currency_details 	= $CI->Soft_settings->retrieve_currency_info();

		$data=array(
			'title'				=> display('customer_details'),
			'customer_id' 		=> $customer_detail[0]['customer_id'],
			'customer_code' 	=> $customer_detail[0]['customer_code'],
			'customer_name' 	=> $customer_detail[0]['customer_name'],
			'customer_address' 	=> $customer_detail[0]['customer_short_address'],
			'customer_mobile' 	=> $customer_detail[0]['customer_mobile'],
			'customer_email' 	=> $customer_detail[0]['customer_email'],
			'city' 				=> $customer_detail[0]['city'],
			'state' 			=> $customer_detail[0]['state'],
			'country' 			=> $customer_detail[0]['country_name'],
			'zip' 				=> $customer_detail[0]['zip'],
			'company' 			=> $customer_detail[0]['company'],
			'id' 				=> $customer_id,
			'mobile' 			=> $customer_mobile,
			'email' 			=> $email,
			'salesData'			=> $salesData,
			'totaSalesAmt'		=> $totaSalesAmt,
			'company_info'		=> $company_info,
			'customer_list'		=> $customer_list,
			'currency' 			=> $currency_details[0]['currency_icon'],
			'position' 			=> $currency_details[0]['currency_position'],
			);

		$singlecustomerdetails = $CI->parser->parse('customer/customer_details',$data,true);
		return $singlecustomerdetails;
	}
	//Search customer
	public function customer_search_list($cat_id,$company_id)
	{
		$CI =& get_instance();
		$CI->load->model('Customers');
		$category_list = $CI->Customers->retrieve_category_list();
		$customers_list = $CI->Customers->customer_search_list($cat_id,$company_id);
		$data = array(
				'title' => 'customers List',
				'customers_list' => $customers_list,
				'category_list' => $category_list
			);
		$customerList = $CI->parser->parse('customer/customer',$data,true);
		return $customerList;
	}

		//Retrieve  Customer Search List	
	public function customer_search_item($customer_id)
	{
		$CI =& get_instance();
		$CI->load->model('Customers');
		$customers_list = $CI->Customers->customer_search_item($customer_id);
		$i=0;
		$total=0;
		if ($customers_list) {
			foreach($customers_list as $k=>$v){$i++;
           		$customers_list[$k]['sl']=$i;
		    	$total+=$customers_list[$k]['customer_balance'];
			}
			$data = array(
					'title' => 'Customers Search Item',
					'subtotal'=>$total,
					'customers_list' => $customers_list
				);
			$customerList = $CI->parser->parse('customer/customer',$data,true);
			return $customerList;
		}else{
			redirect('manage_customer');
		}
	}
}
?>