<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lreport {

	//Sales overview report
	public function sales_report(){
		$CI =& get_instance();
		$CI->load->model('Reports');
		$CI->load->model('Soft_settings');
		$CI->load->model('Shipping_methods');
		$CI->load->library('occational');

		$lifetime_orders = $CI->Reports->lifetime_orders();
		$lifetime_orders_sell = $CI->Reports->lifetime_orders_sell();
		$order_delivered = $CI->Reports->order_delivered();
		$order_delivered_sell = $CI->Reports->order_delivered_sell();
		$total_product   = $CI->Reports->total_product();
		$total_seller    = $CI->Reports->total_seller();
		$cancelled_order = $CI->Reports->cancelled_order();
		$returned_order  = $CI->Reports->returned_order();
		$shipping_method_list = $CI->Shipping_methods->shipping_method_list();


		$currency_details = $CI->Soft_settings->retrieve_currency_info();
		$data = array(
				'title' 			=> display('sales_report'),
				'lifetime_orders' 	=> $lifetime_orders,
				'lifetime_orders_sell' 	=> $lifetime_orders_sell->total_amount,
				'order_delivered_sell' 	=> $order_delivered_sell->total_amount,
				'order_delivered' 	=> $order_delivered,
				'total_product' 	=> $total_product,
				'total_seller'		=> $total_seller,
				'cancelled_order'   => $cancelled_order,
				'returned_order'   	=> $returned_order,
				'shipping_method_list'		=> $shipping_method_list,
				'currency' 			=> $currency_details[0]['currency_icon'],
				'position' 			=> $currency_details[0]['currency_position'],
			);

		$reportList = $CI->parser->parse('report/sales_report',$data,true);
		return $reportList;
	}

	//Sales details report
	public function sales_details_report($page,$per_page,$links,$order_no=null,$customer=null,$date=null,$shipping_id=null,$invoice_no=null,$status=null){
		$CI =& get_instance();
		$CI->load->model('Reports');
		$CI->load->model('Soft_settings');
		$CI->load->model('Shipping_methods');
		$CI->load->model('Customers');

		$sell_report_shipping = $CI->Reports->sell_report_shipping($page,$per_page,$order_no,$customer,$date,$shipping_id,$invoice_no,$status);
		$customer_list 		  = $CI->Customers->customer_list();
		$shipping_method_list = $CI->Shipping_methods->shipping_method_list();

		$SubTotalAmnt 	   = 0;
		$SubTotalDelCharge = 0;
		$SubTotalPaid 	   = 0;
		$SubTotalDue 	   = 0;
		$SubTotalProductPrice 	   = 0;
		if(!empty($sell_report_shipping)){
			$i=0;
			foreach($sell_report_shipping as $k=>$v){
				$SubTotalAmnt 	   = $SubTotalAmnt+$v->total_amount;
				$SubTotalDelCharge = $SubTotalDelCharge+$v->service_charge;
				$SubTotalPaid 	   = $SubTotalPaid+$v->paid_amount;
				$SubTotalDue 	   += $v->total_amount-$v->paid_amount;
				$SubTotalProductPrice 	   += $v->total_amount-$v->service_charge;
			}
		}

		$currency_details = $CI->Soft_settings->retrieve_currency_info();
		$data = array(
			'title' 	=> display('sell_report_based_on_shipping_product'),
			'sell_report_shipping' 	=> $sell_report_shipping,
			'SubTotalAmnt' 	=> $SubTotalAmnt,
			'SubTotalDelCharge' 	=> $SubTotalDelCharge,
			'SubTotalPaid' 	=> $SubTotalPaid,
			'SubTotalDue' 	=> $SubTotalDue,
			'SubTotalProductPrice' 	=> $SubTotalProductPrice,
			'customer_list' => $customer_list,
			'shipping_method_list' 	=> $shipping_method_list,
			'links' 	=> $links,
			'currency' 	=> $currency_details[0]['currency_icon'],
			'position' 	=> $currency_details[0]['currency_position'],
		);
		$reportList = $CI->parser->parse('report/sales_details_report',$data,true);
		return $reportList;
	}

    //Order details report
    public function order_details_report($page,$per_page,$links,$order_no=null,$customer=null,$date=null,$shipping_id=null,$status=null){
        $CI =& get_instance();
        $CI->load->model('Reports');
        $CI->load->model('Soft_settings');
        $CI->load->model('Shipping_methods');
        $CI->load->model('Customers');

        $order_details_report = $CI->Reports->order_details_report($page,$per_page,$order_no,$customer,$date,$shipping_id,$status);
        $customer_list 		  = $CI->Customers->customer_list();
        $shipping_method_list = $CI->Shipping_methods->shipping_method_list();

        $SubTotalAmnt 	   = 0;
        $SubTotalDelCharge = 0;
        $SubTotalPaid 	   = 0;
        $SubTotalDue 	   = 0;
        $SubTotalProductPrice 	   = 0;
        if(!empty($order_details_report)){
            $i=0;
            foreach($order_details_report as $k=>$v){
                $SubTotalAmnt 	   = $SubTotalAmnt+$v->total_amount;
                $SubTotalDelCharge = $SubTotalDelCharge+$v->service_charge;
                $SubTotalPaid 	   = $SubTotalPaid+$v->paid_amount;
                $SubTotalDue 	   += $v->total_amount-$v->paid_amount;
                $SubTotalProductPrice 	   += $v->total_amount-$v->service_charge;
            }
        }

        $currency_details = $CI->Soft_settings->retrieve_currency_info();
        $data = array(
            'title' 	=> display('based_on_order_date'),
            'order_details_report' 	=> $order_details_report,
            'SubTotalAmnt' 	=> $SubTotalAmnt,
            'SubTotalDelCharge' 	=> $SubTotalDelCharge,
            'SubTotalPaid' 	=> $SubTotalPaid,
            'SubTotalDue' 	=> $SubTotalDue,
            'SubTotalProductPrice' 	=> $SubTotalProductPrice,
            'customer_list' => $customer_list,
            'shipping_method_list' 	=> $shipping_method_list,
            'links' 	=> $links,
            'currency' 	=> $currency_details[0]['currency_icon'],
            'position' 	=> $currency_details[0]['currency_position'],
        );
        $reportList = $CI->parser->parse('report/order_details_report',$data,true);
        return $reportList;
    }

	//Merchant Sales report
	public function merchant_sell_report($page,$per_page,$links,$order_no=null,$seller=null,$date=null){
		$CI =& get_instance();
		$CI->load->model('Reports');
		$CI->load->model('Soft_settings');
		$CI->load->model('Shipping_methods');
		$CI->load->model('Customers');
		$CI->load->model('Sellers');

		$merchant_sell_report = $CI->Reports->merchant_sell_report($page,$per_page,$order_no,$seller,$date);

		$seller_list 		  = $CI->Sellers->seller_list();
		$shipping_method_list = $CI->Shipping_methods->shipping_method_list();

		$sub_total_price = 0;
		$sub_total_comission = 0;
		$sub_total_payable = 0;
		if(!empty($merchant_sell_report)){
			$i=0;
			foreach($merchant_sell_report as $k=>$v){
				$merchant_sell_report[$k]['payable_amount'] = ($merchant_sell_report[$k]['total_price']-$merchant_sell_report[$k]['total_comission']);

				$sub_total_price += $merchant_sell_report[$k]['total_price'];
				$sub_total_comission += $merchant_sell_report[$k]['total_comission'];
				$sub_total_payable += $merchant_sell_report[$k]['payable_amount'];
			}
		}

		$currency_details = $CI->Soft_settings->retrieve_currency_info();
		$data = array(
			'title' 				=> display('sell_report_based_on_shipping_product'),
			'merchant_sell_report' 	=> $merchant_sell_report,
			'sub_total_price' 		=> $sub_total_price,
			'sub_total_comission' 	=> $sub_total_comission,
			'sub_total_payable' 	=> $sub_total_payable,
			'shipping_method_list' 	=> $shipping_method_list,
			'seller_list' 			=> $seller_list,
			'links' 				=> $links,
			'currency' 				=> $currency_details[0]['currency_icon'],
			'position' 				=> $currency_details[0]['currency_position'],
		);
		$reportList = $CI->parser->parse('report/merchant_sell_report',$data,true);
		return $reportList;
	}
	//Stock report product wise
	public function stock_report_product_wise($page=null,$per_page=null,$links=null,$product_id=null,$seller=null,$product_model=null)
	{
		$CI =& get_instance();
		$CI->load->model('Reports');
		$CI->load->model('Soft_settings');
		$CI->load->model('Products');
		$CI->load->model('Sellers');

		$manage_product = $CI->Reports->manage_product($per_page,$page,$product_id,$seller,$product_model);
		$product_list 	= $CI->Products->manage_product();
		$seller_list 	= $CI->Sellers->seller_list();

		$currency_details = $CI->Soft_settings->retrieve_currency_info();
		$data = array(
			'title' 	=> display('stock_report_product_wise'),
			'manage_product'=> $manage_product,
			'product_list' 	=> $product_list,
			'seller_list' 	=> $seller_list,
			'links' 	=> $links,
			'currency' 	=> $currency_details[0]['currency_icon'],
			'position' 	=> $currency_details[0]['currency_position'],
		);
		$reportList = $CI->parser->parse('report/stock_report_product_wise',$data,true);
		return $reportList;
	}
}
?>