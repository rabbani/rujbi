<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lseller {

	//Seller add form
	public function seller_add_form()
	{
		$CI =& get_instance();
		$CI->load->model('Sellers');
		$data = array(
				'title' => display('add_seller'),
			);
		$sellerForm = $CI->parser->parse('seller/add_seller_form',$data,true);
		return $sellerForm;
	}
	//Retrieve seller List	
	public function seller_list($links=null,$per_page=null,$page=null,$mobile=null,$email=null,$business_name=null)
	{
		$CI =& get_instance();
		$CI->load->model('Sellers');
		$sellers_list = $CI->Sellers->seller_list($per_page,$page,$mobile,$email,$business_name);

		$i=0;

		if(!empty($sellers_list)){	
			foreach($sellers_list as $k=>$v){$i++;
			   $sellers_list[$k]['sl']=$i;
			}
		}

		$data = array(
				'title'		   => display('manage_seller'),
				'sellers_list' => $sellers_list,
				'links' 	   => $links,
			);
		$sellerList = $CI->parser->parse('seller/seller',$data,true);
		return $sellerList;
	}
	//Insert seller
	public function insert_seller($data)
	{
		$CI =& get_instance();
		$CI->load->model('Sellers');
        $CI->Sellers->seller_entry($data);
		return true;
	}
	//Seller Edit Data
	public function seller_edit_data($seller_id)
	{
		$CI =& get_instance();
		$CI->load->model('Sellers');
		$seller_detail = $CI->Sellers->retrieve_seller_editdata($seller_id);
		$data=array(
			'title'			=> display('seller_edit'),
			'id' 			=> $seller_detail[0]['id'],
			'first_name' 	=> $seller_detail[0]['first_name'],
			'last_name'		=> $seller_detail[0]['last_name'],
			'email' 		=> $seller_detail[0]['email'],
			'password' 		=> $seller_detail[0]['password'],
			'mobile' 		=> $seller_detail[0]['mobile'],
			'seller_store_name' => $seller_detail[0]['seller_store_name'],
			'business_name' => $seller_detail[0]['business_name'],
			'address' 		=> $seller_detail[0]['address'],
			'identification_doc_no' => $seller_detail[0]['identification_doc_no'],
			'identification_type' 	=> $seller_detail[0]['identification_type'],
			'affiliate_id' 			=> $seller_detail[0]['affiliate_id'],
			'verfication_status' 	=> $seller_detail[0]['verfication_status'],
			'status' 		=> $seller_detail[0]['status'],
			);
		$chapterList = $CI->parser->parse('seller/edit_seller_form',$data,true);
		return $chapterList;
	}
	//Seller policy
	public function seller_policy()
	{
		$CI =& get_instance();
		$CI->load->model('Sellers');
		$seller_policy  = $CI->Sellers->select_seller_policy();
		$data = array(
				'title' => display('seller_policy'),
				'policy' => $seller_policy->policy,
			);
		$sellerForm = $CI->parser->parse('seller/seller_policy',$data,true);
		return $sellerForm;
	}
}
?>