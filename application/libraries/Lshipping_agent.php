<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lshipping_agent {
	//Add shipping_agent
	public function shipping_agent_add_form()
	{
		$CI =& get_instance();
		$CI->load->model('Shipping_agents');
		$CI->load->model('Categories');

		$category_list = $CI->Categories->category_list(); 

		$data = array(
				'title' => display('add_shipping_agent'),
				'category_list' => $category_list,
			);
		$customerForm = $CI->parser->parse('shipping_agent/add_shipping_agent',$data,true);
		return $customerForm;
	}

	//Retrieve shipping_agent List	
	public function shipping_agent_list()
	{
		$CI =& get_instance();
		$CI->load->model('Shipping_agents');
		$shipping_agent_list = $CI->Shipping_agents->shipping_agent_list(); 

		$i=0;
		if(!empty($shipping_agent_list)){	
			foreach($shipping_agent_list as $k=>$v){$i++;
			   $shipping_agent_list[$k]['sl']=$i;
			}
		}

		$data = array(
				'title' => display('manage_shipping_agent'),
				'shipping_agent_list' => $shipping_agent_list,
			);
		$customerList = $CI->parser->parse('shipping_agent/shipping_agent',$data,true);
		return $customerList;
	}

	//shipping_agent Edit Data
	public function shipping_agent_edit_data($agent_id)
	{
		$CI =& get_instance();
		$CI->load->model('Shipping_agents');
		$CI->load->model('Categories');
		$category_list = $CI->Categories->category_list();

		$shipping_agent_details = $CI->Shipping_agents->retrieve_shipping_agent_editdata($agent_id);
	
		$data=array(
			'title' 		=> display('shipping_agent_edit'),
			'agent_id' 	=> $shipping_agent_details[0]['agent_id'],
			'agent_name' 	=> $shipping_agent_details[0]['agent_name'],
			'agent_phone' 		=> $shipping_agent_details[0]['agent_phone'],
			'agent_email' 		=> $shipping_agent_details[0]['agent_email'],
			'agent_address' 		=> $shipping_agent_details[0]['agent_address'],
			'amount' => $shipping_agent_details[0]['amount'],

			);
		$chapterList = $CI->parser->parse('shipping_agent/edit_shipping_agent',$data,true);
		return $chapterList;
	}
}
?>