<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lproduct {

	//Upload product
	public function upload_product()
	{
		$CI =& get_instance();
		$CI->load->model('Sellers');
		$CI->load->model('Categories');
		$CI->load->model('Units');
		$CI->load->model('Brands');
		$CI->load->model('Variants');
		$product_id    = $CI->session->userdata('product_id');
		$sellers_list  = $CI->Sellers->seller_list();
		$upload_image  = $CI->Products->retrive_product_image_edit_data($product_id);
		$category_list = $CI->Categories->category_list();
		$unit_list 	   = $CI->Units->unit_list();
		$brand_list    = $CI->Brands->brand_list();
		$variant_list  = $CI->Variants->variant_list();
		$languages 	   = $CI->Sellers->languages();

		$data = array(
				'title' 	   => display('upload_product'),
				'sellers_list' => $sellers_list,
				'category_list'=> $category_list,
				'unit_list'	   => $unit_list,
				'brand_list'   => $brand_list,
				'variant_list' => $variant_list,
				'language'     => $languages,
				'upload_image' => $upload_image,
			);
		$sellerForm = $CI->parser->parse('product/add_product_form',$data,true);
		return $sellerForm;
	}
	//Manage Product
	public function manage_product($links=null,$per_page=null,$page=null,$seller=null,$model=null,$title=null,$category=null){
		$CI =& get_instance();
		$CI->load->model('Products');
		$CI->load->model('Sellers');
		$CI->load->model('Categories');
		$CI->load->model('Email_templates');
		$product_list 	= $CI->Products->manage_product($per_page,$page,$seller,$model,$title,$category);
		$seller_list 	= $CI->Sellers->seller_list();
		$category_list 	= $CI->Categories->category_list();
		$template_list 	= $CI->Email_templates->template_list();

		$i=0;
		if(!empty($product_list)){	
			foreach($product_list as $k=>$v){
				$i++;
			   	$product_list[$k]['sl']=$page+$i;
			}
		}

		$data = array(
			'title' 		=> display('manage_product'), 
			'product_list'  => $product_list,
			'seller_list' 	=> $seller_list,
			'category_list' => $category_list,
			'template_list' => $template_list,
			'links' 		=> $links
		);

		$sellerForm = $CI->parser->parse('product/product',$data,true);
		return $sellerForm;
	}
	//Pending Product
	public function pending_product($links=null,$per_page=null,$page=null,$seller=null,$model=null,$title=null,$category=null){
		$CI =& get_instance();
		$CI->load->model('Products');
		$CI->load->model('Sellers');
		$CI->load->model('Categories');
		$product_list 	= $CI->Products->pending_product($per_page,$page,$seller,$model,$title,$category);
		$seller_list 	= $CI->Sellers->seller_list();
		$category_list 	= $CI->Categories->category_list();
		$template_list 	= $CI->Email_templates->template_list();

		$i=0;

		if(!empty($product_list)){	
			foreach($product_list as $k=>$v){$i++;
			   $product_list[$k]['sl']=$page+$i;
			}
		}

		$data = array(
			'title' 		=> display('pending_product'), 
			'product_list'  => $product_list,
			'seller_list' 	=> $seller_list,
			'category_list' => $category_list,
			'template_list' => $template_list,
			'links' 		=> $links
		);

		$sellerForm = $CI->parser->parse('product/pending_product',$data,true);
		return $sellerForm;
	}
	//Seller Product Update Form
	public function product_update_form($product_id)
	{
		$CI =& get_instance();
		$CI->load->model('Sellers');
		$CI->load->model('Products');
		$CI->load->model('Categories');
		$CI->load->model('Units');
		$CI->load->model('Brands');
		$CI->load->model('Variants');

		$seller_detail = $CI->Products->retrive_product_edit_data($product_id);
		$upload_image  = $CI->Products->retrive_product_image_edit_data($product_id);

		$sellers_list  = $CI->Sellers->seller_list();
		$category_list = $CI->Categories->category_list();
		$unit_list 	   = $CI->Units->unit_list();
		$brand_list    = $CI->Brands->brand_list();
		$variant_list  = $CI->Variants->variant_list();
		$languages 	   = $CI->Sellers->languages();

		$data=array(
			'title'			=> display('seller_edit_product'),
			'product_id' 	=> $seller_detail->product_id,
			'seller_id' 	=> $seller_detail->seller_id,
			'seller_uni_id' => $seller_detail->seller_uni_id,
			'category_id' 	=> $seller_detail->category_id,
			'price' 		=> $seller_detail->price,
			'unit_id' 		=> $seller_detail->unit,
			'product_model' => $seller_detail->product_model,
			'product_brand' => $seller_detail->brand_id,
			'variant' 		=> $seller_detail->variant_id,
			'product_type' 	=> $seller_detail->product_type,
			'tag' 			=> $seller_detail->tag,
			'quantity' 		=> $seller_detail->quantity,
			'status' 		=> $seller_detail->status,
			'category_name' => $seller_detail->category_name,
			'first_name' 	=> $seller_detail->first_name,
			'last_name' 	=> $seller_detail->last_name,
			'brand_name' 	=> $seller_detail->brand_name,
			'brand_id' 		=> $seller_detail->brand_id,
			'status' 		=> $seller_detail->status,
			'best_sale' 	=> $seller_detail->best_sale,
			'on_promotion' 	=> $seller_detail->on_promotion,
			'onsale' 		=> $seller_detail->on_sale,
			'onsale_price' 	=> $seller_detail->offer_price,
			'details' 		=> $seller_detail->details,
			'pre_order' 	=> $seller_detail->pre_order,
			'pre_order_quantity' 	=> $seller_detail->pre_order_quantity,
			'comission' 	=> $seller_detail->comission,
			'sellers_list'  => $sellers_list,
			'category_list' => $category_list,
			'unit_list'	    => $unit_list,
			'brand_list'    => $brand_list,
			'variant_list'  => $variant_list,
			'upload_image'  => $upload_image,
			'language'  	=> $languages,
		);

		$chapterList = $CI->parser->parse('product/edit_product_form',$data,true);
		return $chapterList;
	}
	//Pending Product Update
	public function pending_product_update($upload_id=null)
	{
		$CI =& get_instance();
		$CI->load->model('Sellers');
		$CI->load->model('Products');
		$CI->load->model('Categories');
		$CI->load->model('Units');
		$CI->load->model('Brands');
		$CI->load->model('Variants');

		$pending_product = $CI->Products->retrive_pending_product_edit_data($upload_id);
		$upload_image    = $CI->Products->retrive_pending_product_image_edit_data($upload_id);
		
		$sellers_list  = $CI->Sellers->seller_list();
		$category_list = $CI->Categories->category_list();
		$unit_list 	   = $CI->Units->unit_list();
		$brand_list    = $CI->Brands->brand_list();
		$variant_list  = $CI->Variants->variant_list();
		$languages 	   = $CI->Sellers->languages();

		$data=array(
			'title'			=> display('seller_edit_product'),
			'upload_id' 	=> $pending_product->upload_id,
			'product_id' 	=> $pending_product->product_id,
			'seller_id' 	=> $pending_product->seller_id,
			'seller_uni_id' => $pending_product->seller_uni_id,
			'category_id' 	=> $pending_product->category_id,
			'price' 		=> $pending_product->price,
			'unit_id' 		=> $pending_product->unit,
			'product_model' => $pending_product->product_model,
			'product_brand' => $pending_product->brand_id,
			'variant' 		=> $pending_product->variant_id,
			'product_type' 	=> $pending_product->product_type,
			'tag' 			=> $pending_product->tag,
			'quantity' 		=> $pending_product->quantity,
			'status' 		=> $pending_product->status,
			'on_promotion' 	=> $pending_product->on_promotion,
			'best_sale' 	=> $pending_product->best_sale,
			'on_promotion' 	=> $pending_product->on_promotion,
			'pre_order' 	=> $pending_product->pre_order,
			'pre_order_quantity' => $pending_product->pre_order_quantity,
			'details' 		=> $pending_product->details,

			'category_name' => $pending_product->category_name,
			'first_name' 	=> $pending_product->first_name,
			'last_name' 	=> $pending_product->last_name,
			'brand_name' 	=> $pending_product->brand_name,
			'brand_id' 		=> $pending_product->brand_id,
			'status' 		=> $pending_product->status,
			'comission' 	=> $pending_product->comission,
		
			'sellers_list'  => $sellers_list,
			'category_list' => $category_list,
			'unit_list'	    => $unit_list,
			'brand_list'    => $brand_list,
			'variant_list'  => $variant_list,
			'upload_image'  => $upload_image,
			'language'  	=> $languages,
		);

		$chapterList = $CI->parser->parse('product/edit_pending_product',$data,true);
		return $chapterList;
	}
	//Search Product List
	public function product_search_list($product_id)
	{
		$CI =& get_instance();
		$CI->load->model('Products');
		$CI->load->model('Soft_settings');
		$products_list = $CI->Products->product_search_item($product_id);
		$all_product_list = $CI->Products->all_product_list();
		$i=0;
		if ($products_list) {
			foreach($products_list as $k=>$v){$i++;
           $products_list[$k]['sl']=$i;
			}
			$currency_details = $CI->Soft_settings->retrieve_currency_info();
			$data = array(
					'title' 		=> display('manage_product'),
					'products_list' => $products_list,
					'all_product_list' => $all_product_list,
					'currency' 	=> $currency_details[0]['currency_icon'],
					'position' 	=> $currency_details[0]['currency_position'],
				);
			$productList = $CI->parser->parse('product/product',$data,true);
			return $productList;
		}else{
			redirect('Cproduct/manage_product');
		}
	}
	//Product Details
	public function product_details($product_id)
	{
		$CI =& get_instance();
		$CI->load->model('Products');
		$CI->load->library('occational');
		$CI->load->model('Soft_settings');
		$details_info 	= $CI->Products->product_details_info($product_id);
		$salesData 		= $CI->Products->invoice_data($product_id);
		$all_product_list = $CI->Products->all_product_list();

		$totalSales = 0;
		$totaSalesAmt = 0;

		if(!empty($salesData)){	
			foreach($salesData as $k=>$v){
				$salesData[$k]['final_date'] = $CI->occational->dateConvert($salesData[$k]['date']);
				$totalSales = ($totalSales + $salesData[$k]['quantity']);
				$totaSalesAmt = ($totaSalesAmt + $salesData[$k]['total_price']);
			}
		}

		$currency_details = $CI->Soft_settings->retrieve_currency_info();
		$data = array(
				'title'				=> display('product_details'),
				'product_id' 		=> $details_info[0]['product_id'],
				'product_model' 	=> $details_info[0]['product_model'],
				'price'				=> $details_info[0]['price'],
				'product_id'		=> $details_info[0]['product_id'],
				'salesTotalAmount'	=> number_format($totaSalesAmt, 2, '.', ','),
				'all_product_list'	=> $all_product_list,
				'total_sales'		=> $totalSales,
				'salesData'			=> $salesData,
				'currency' 			=> $currency_details[0]['currency_icon'],
				'position' 			=> $currency_details[0]['currency_position'],
			);

		$productList = $CI->parser->parse('product/product_details',$data,true);
		return $productList;
	}

	//Denied Product
	public function denied_product($links=null,$per_page=null,$page=null,$seller=null,$model=null,$title=null,$category=null){
		$CI =& get_instance();
		$CI->load->model('Products');
		$CI->load->model('Sellers');
		$CI->load->model('Categories');
		$product_list 	= $CI->Products->denied_product($per_page,$page,$seller,$model,$title,$category);
		$seller_list 	= $CI->Sellers->seller_list();
		$category_list 	= $CI->Categories->category_list();

		$i=0;

		if(!empty($product_list)){	
			foreach($product_list as $k=>$v){$i++;
			   $product_list[$k]['sl']=$page+$i;
			}
		}

		$data = array(
			'title' 		=> display('denied_product'), 
			'product_list'  => $product_list,
			'seller_list' 	=> $seller_list,
			'category_list' => $category_list,
			'links' 		=> $links
		);

		$sellerForm = $CI->parser->parse('product/denied_product',$data,true);
		return $sellerForm;
	}
}
?>