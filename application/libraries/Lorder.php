<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lorder {

	//Order Add Form
	public function order_add_form()
	{
		$CI =& get_instance();
		$CI->load->model('Orders');
		$CI->load->model('Variants');
		$CI->load->model('Sellers');
		$CI->load->model('Shipping_methods');

		$seller_list  	= $CI->Sellers->seller_list();
		$variant_list  	= $CI->Variants->variant_list();
		$shipping_method_list  	= $CI->Shipping_methods->shipping_method_list();
		
		$data = array(
				'title' 		=> display('new_order'),
				'seller_list' 	=> $seller_list,
				'variant_list' 	=> $variant_list,
				'shipping_method_list' 	=> $shipping_method_list,
			);
		$orderForm = $CI->parser->parse('order/add_order_form',$data,true);
		return $orderForm;
	}	

	//Pre Order Add Form
	public function pre_order_add_form()
	{
		$CI =& get_instance();
		$CI->load->model('Orders');
		$CI->load->model('Variants');
		$CI->load->model('Sellers');
		$CI->load->model('Shipping_methods');

		$seller_list  	= $CI->Sellers->seller_list();
		$variant_list  	= $CI->Variants->variant_list();
		$shipping_method_list  	= $CI->Shipping_methods->shipping_method_list();
		
		$data = array(
				'title' 		=> display('new_order'),
				'seller_list' 	=> $seller_list,
				'variant_list' 	=> $variant_list,
				'shipping_method_list' 	=> $shipping_method_list,
			);
		$preOrderForm = $CI->parser->parse('order/add_pre_order_form',$data,true);
		return $preOrderForm;
	}
	//Retrieve order List
	public function order_list($links=null,$per_page=null,$page=null,$order_no=null,$pre_order_no=null,$customer=null,$date=null,$order_status=null,$shipping=null)
	{
		$CI =& get_instance();
		$CI->load->model('Orders');
		$CI->load->model('Soft_settings');
		$CI->load->model('Customers');
		$CI->load->model('Email_templates');
		$CI->load->model('Shipping_methods');
		$CI->load->model('Shipping_agents');
		$CI->load->library('occational');
		
		$orders_list   = $CI->Orders->order_list($per_page,$page,$order_no,$pre_order_no,$customer,$date,$order_status,$shipping);
		$customer_list = $CI->Customers->customer_list();
		$template_list = $CI->Email_templates->template_list();
		$shipping_method_list = $CI->Shipping_methods->shipping_method_list();
		$shipping_agent_list = $CI->Shipping_agents->shipping_agent_list();

		if(!empty($orders_list)){
			foreach($orders_list as $k=>$v){
				$orders_list[$k]['final_date'] = $CI->occational->dateConvert($orders_list[$k]['date']);
			}
			$i=0;
			foreach($orders_list as $k=>$v){$i++;
			   $orders_list[$k]['sl']=$page+$i;
			}
		}

		$currency_details = $CI->Soft_settings->retrieve_currency_info();
		$data = array(
				'title'    		=> display('manage_order'),
				'orders_list' 	=> $orders_list,
				'customer_list' => $customer_list,
				'template_list' => $template_list,
				'shipping_method_list' => $shipping_method_list,
				'shipping_agent_lists' => $shipping_agent_list,
				'links'    => $links,
				'currency' => $currency_details[0]['currency_icon'],
				'position' => $currency_details[0]['currency_position'],
			);

		$orderList = $CI->parser->parse('order/order',$data,true);
		return $orderList;
	}	
	//Retrieve order tracking
	public function order_tracking($links=null,$per_page=null,$page=null,$order_no=null,$pre_order_no=null,$customer=null,$date=null,$order_status=null)
	{
		$CI =& get_instance();
		$CI->load->model('Orders');
		$CI->load->model('Soft_settings');
		$CI->load->model('Customers');
		$CI->load->model('Email_templates');
		$CI->load->library('occational');
		
		$orders_list   = $CI->Orders->order_list($per_page,$page,$order_no,$pre_order_no,$customer,$date,$order_status);
		$customer_list = $CI->Customers->customer_list();
		$template_list = $CI->Email_templates->template_list();

		if(!empty($orders_list)){
			foreach($orders_list as $k=>$v){
				$orders_list[$k]['final_date'] = $CI->occational->dateConvert($orders_list[$k]['date']);
			}
			$i=0;
			foreach($orders_list as $k=>$v){$i++;
			   $orders_list[$k]['sl']=$page+$i;
			}
		}

		$currency_details = $CI->Soft_settings->retrieve_currency_info();
		$data = array(
				'title'    		=> display('order_tracking'),
				'orders_list' 	=> $orders_list,
				'customer_list' => $customer_list,
				'template_list' => $template_list,
				'links'    		=> $links,
				'currency' 		=> $currency_details[0]['currency_icon'],
				'position' 		=> $currency_details[0]['currency_position'],
			);
		$orderList = $CI->parser->parse('order/order_tracking',$data,true);
		return $orderList;
	}
	//Insert order
	public function insert_order($data)
	{
		$CI =& get_instance();
		$CI->load->model('Orders');
        $CI->Orders->order_entry($data);
		return true;
	}
	//order Edit Data
	public function order_edit_data($order_id)
	{
		$CI =& get_instance();
		$CI->load->model('Orders');
		$CI->load->model('Shipping_methods');

		$order_detail 	  = $CI->Orders->retrieve_order_editdata($order_id);
		$shipping_method_list  	= $CI->Shipping_methods->shipping_method_list();
		$i=0;
		foreach($order_detail as $k=>$v){$i++;
		   $order_detail[$k]['sl']=$i;
		}

		$data=array(
			'title'				=> 	display('order_update'),
			'order_id'			=>	$order_detail[0]['order_id'],
			'order_no'			=>	$order_detail[0]['order_no'],
			'shipping_id'		=>	$order_detail[0]['shipping_id'],
			'customer_id'		=>	$order_detail[0]['customer_id'],
			'customer_name'		=>	$order_detail[0]['customer_name'],
			'date'				=>	$order_detail[0]['date'],
			'total_amount'		=>	$order_detail[0]['total_amount'],
			'paid_amount'		=>	$order_detail[0]['paid_amount'],
			'seller_id'			=>	$order_detail[0]['seller_id'],
			'due_amount'		=>	$order_detail[0]['total_amount']-$order_detail[0]['paid_amount'],
			'total_discount'	=>	$order_detail[0]['total_discount'],
			'product_discount'	=>	$order_detail[0]['total_discount']-$order_detail[0]['order_discount'],
			'order_discount'	=>	$order_detail[0]['order_discount'],
			'service_charge'	=>	$order_detail[0]['service_charge'],
			'details'			=>	$order_detail[0]['details'],
			'status'			=>	$order_detail[0]['status'],
			'order_all_data'	=>	$order_detail,
			'shipping_method_list'	=>	$shipping_method_list,
			);

		$chapterList = $CI->parser->parse('order/edit_order_form',$data,true);
		return $chapterList;
	}
	//order Edit Data
	public function order_return_edit_data($order_id)
	{
		$CI =& get_instance();
		$CI->load->model('Orders');
		$CI->load->model('Shipping_methods');

		$order_detail 	  = $CI->Orders->retrieve_order_editdata($order_id);
		$shipping_method_list  	= $CI->Shipping_methods->shipping_method_list();
		$i=0;
		foreach($order_detail as $k=>$v){$i++;
		   $order_detail[$k]['sl']=$i;
		}

		$data=array(
			'title'				=> 	display('order_update'),
			'order_id'			=>	$order_detail[0]['order_id'],
			'order_no'			=>	$order_detail[0]['order_no'],
			'shipping_id'		=>	$order_detail[0]['shipping_id'],
			'customer_id'		=>	$order_detail[0]['customer_id'],
			'customer_name'		=>	$order_detail[0]['customer_name'],
			'date'				=>	$order_detail[0]['date'],
			'total_amount'		=>	$order_detail[0]['total_amount'],
			'paid_amount'		=>	$order_detail[0]['paid_amount'],
			'seller_id'			=>	$order_detail[0]['seller_id'],
			'due_amount'		=>	$order_detail[0]['total_amount']-$order_detail[0]['paid_amount'],
			'total_discount'	=>	$order_detail[0]['total_discount'],
			'product_discount'	=>	$order_detail[0]['total_discount']-$order_detail[0]['order_discount'],
			'order_discount'	=>	$order_detail[0]['order_discount'],
			'service_charge'	=>	$order_detail[0]['service_charge'],
			'details'			=>	$order_detail[0]['details'],
			'status'			=>	$order_detail[0]['status'],
			'order_status'		=>	$order_detail[0]['order_status'],
			'order_all_data'	=>	$order_detail,
			'shipping_method_list'	=>	$shipping_method_list,
			);

		$chapterList = $CI->parser->parse('order/edit_return_order_form',$data,true);
		return $chapterList;
	}
	//Order Html Data
	public function order_html_data($order_id)
	{
		$CI =& get_instance();
		$CI->load->model('Orders');
		$CI->load->model('Soft_settings');
		$CI->load->library('occational');
		$CI->load->library('Pdfgenerator');
		$order_detail = $CI->Orders->retrieve_order_html_data($order_id);

		$subTotal_quantity 	= 0;
		$subTotal_cartoon 	= 0;
		$subTotal_discount 	= 0;

		if(!empty($order_detail)){
			foreach($order_detail as $k=>$v){
				$order_detail[$k]['final_date'] = $CI->occational->dateConvert($order_detail[$k]['date']);
				$subTotal_quantity = $subTotal_quantity+$order_detail[$k]['quantity'];
			}
			$i=0;
			foreach($order_detail as $k=>$v){$i++;
			   $order_detail[$k]['sl']=$i;
			}
		}

		$currency_details = $CI->Soft_settings->retrieve_currency_info();
		$company_info 	  = $CI->Orders->retrieve_company();
		$data = array(
			'title'				=>	display('order_details'),
			'order_id'			=>	$order_detail[0]['order_id'],
			'order_no'			=>	$order_detail[0]['order_no'],
			'customer_address'	=>	$order_detail[0]['customer_short_address'],
			'customer_name'		=>	$order_detail[0]['customer_name'],
			'customer_mobile'	=>	$order_detail[0]['customer_mobile'],
			'customer_email'	=>	$order_detail[0]['customer_email'],
			'final_date'		=>	$order_detail[0]['final_date'],
			'total_amount'		=>	$order_detail[0]['total_amount'],
			'order_discount' 	=>	$order_detail[0]['order_discount'],
			'service_charge' 	=>	$order_detail[0]['service_charge'],
			'paid_amount'		=>	$order_detail[0]['paid_amount'],
			'due_amount'		=>	$order_detail[0]['total_amount']-$order_detail[0]['paid_amount'],
			'details'			=>	$order_detail[0]['details'],
			'subTotal_quantity'	=>	$subTotal_quantity,
			'order_all_data' 	=>	$order_detail,
			'company_info'		=>	$company_info,
			'currency' 			=> 	$currency_details[0]['currency_icon'],
			'position' 			=> 	$currency_details[0]['currency_position'],
			);

		$chapterList = $CI->parser->parse('order/order_pdf',$data,true);

		// PDF Generator 
		$dompdf = new DOMPDF();
	    $dompdf->load_html($chapterList);
	    $dompdf->render();
	    $output = $dompdf->output();
	    file_put_contents('my-assets/pdf/'.$order_id.'.pdf', $output);
	    $file_path = 'my-assets/pdf/'.$order_id.'.pdf';
	 
	    $send_email = '';
	    if (!empty($data['customer_email'])) {
	    	$send_email = $this->setmail($data['customer_email'],$file_path);
	    }

	    if ($send_email != null) {
	    	return true;
	    }else{
	    	return false;
	    }
	}
	//Send Customer Email with invoice
	public function setmail($email,$file_path)
	{
		$CI =& get_instance();
		$CI->load->model('Soft_settings');
		$CI->load->model('Email_templates');
		$setting_detail = $CI->Soft_settings->retrieve_email_editdata();

		$template 	 	= $CI->Email_templates->retrieve_template('8');

		if ($template) {
			//send email with as a link
	        $setting_detail = $CI->Soft_settings->retrieve_email_editdata();
	        $company_info   = $CI->Companies->company_list();
	       
	        $config = array(
	            'protocol'      => $setting_detail[0]['protocol'],
	            'smtp_host'     => $setting_detail[0]['smtp_host'],
	            'smtp_port'     => $setting_detail[0]['smtp_port'],
	            'smtp_user'     => $setting_detail[0]['sender_email'], 
	            'smtp_pass'     => $setting_detail[0]['password'], 
	            'mailtype'      => $setting_detail[0]['mailtype'], 
	            'charset'       => 'utf-8'
	        );
	        $CI->email->initialize($config);
	        $CI->email->set_mailtype($setting_detail[0]['mailtype']);
	        $CI->email->set_newline("\r\n");
	        
	        //Email content
	        $CI->email->to($email);
	        $CI->email->from($setting_detail[0]['sender_email'],$company_info[0]['company_name']);
	        $CI->email->subject($template->subject);
	        $CI->email->message($template->message);

		    $email = $CI->test_input($email);
			if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			    if($CI->email->send())
			    {
			      	$CI->session->set_userdata(array('message'=>display('email_send_to_customer')));
			      	redirect(base_url('corder/manage_order/all/item'));
			    }else{
			     	$CI->session->set_userdata(array('error_message'=>display('email_not_send')));
			     	redirect(base_url('corder/manage_order/all/item'));
			    }
			}else{
				$CI->session->set_userdata(array('message'=>display('successfully_updated')));
			    redirect(base_url('corder/manage_order/all/item'));
			}
		}else{
			$CI->session->set_userdata(array('error_message'=>display('please_add_template')));
			redirect(base_url('corder/manage_order/all/item'));
		}
	}
	//Email testing for email
	public function test_input($data) {
	  	$data = trim($data);
	  	$data = stripslashes($data);
	  	$data = htmlspecialchars($data);
	  	return $data;
	}
	//Order Details Data
	public function order_details_data($order_id)
	{
		$CI =& get_instance();
		$CI->load->model('Orders');
		$CI->load->model('Soft_settings');
		$CI->load->library('occational');
		$CI->load->library('Pdfgenerator');
		$order_detail = $CI->Orders->retrieve_order_html_data($order_id);

		$subTotal_quantity 	= 0;
		$subTotal_cartoon 	= 0;
		$subTotal_discount 	= 0;

		if(!empty($order_detail)){
			foreach($order_detail as $k=>$v){
				$order_detail[$k]['final_date'] = $CI->occational->dateConvert($order_detail[$k]['date']);
				$subTotal_quantity = $subTotal_quantity+$order_detail[$k]['quantity'];
			}
			$i=0;
			foreach($order_detail as $k=>$v){$i++;
			   $order_detail[$k]['sl']=$i;
			}
		}

		$currency_details = $CI->Soft_settings->retrieve_currency_info();
		$company_info = $CI->Orders->retrieve_company();
		$data=array(
			'title'				=>	display('order_details'),
			'order_id'			=>	$order_detail[0]['order_id'],
			'order_no'			=>	$order_detail[0]['order_no'],
			'customer_address'	=>	$order_detail[0]['customer_short_address'],
			'customer_name'		=>	$order_detail[0]['customer_name'],
			'customer_mobile'	=>	$order_detail[0]['customer_mobile'],
			'customer_email'	=>	$order_detail[0]['customer_email'],
			'final_date'		=>	$order_detail[0]['final_date'],
			'total_amount'		=>	$order_detail[0]['total_amount'],
			'total_discount' 	=>	$order_detail[0]['total_discount'] + $order_detail[0]['order_discount'],
			'service_charge' 	=>	$order_detail[0]['service_charge'],
			'paid_amount'		=>	$order_detail[0]['paid_amount'],
			'due_amount'		=>	$order_detail[0]['total_amount']-$order_detail[0]['paid_amount'],
			'details'			=>	$order_detail[0]['details'],
			'subTotal_quantity'	=>	$subTotal_quantity,
			'order_all_data' 	=>	$order_detail,
			'company_info'		=>	$company_info,
			'currency' 			=> 	$currency_details[0]['currency_icon'],
			'position' 			=> 	$currency_details[0]['currency_position'],
			);

		$chapterList = $CI->parser->parse('order/order_html',$data,true);
		return $chapterList;
	}
	//POS order html Data
	public function pos_order_html_data($order_id)
	{
		$CI =& get_instance();
		$CI->load->model('Orders');
		$CI->load->model('Soft_settings');
		$CI->load->library('occational');
		$order_detail = $CI->Orders->retrieve_order_html_data($order_id);
		$subTotal_quantity = 0;
		$subTotal_cartoon = 0;
		$subTotal_discount = 0;

		if(!empty($order_detail)){
			foreach($order_detail as $k=>$v){
				$order_detail[$k]['final_date'] = $CI->occational->dateConvert($order_detail[$k]['date']);
				$subTotal_quantity = $subTotal_quantity+$order_detail[$k]['quantity'];
			}
			$i=0;
			foreach($order_detail as $k=>$v){$i++;
			   $order_detail[$k]['sl']=$i;
			}
		}

		$currency_details = $CI->Soft_settings->retrieve_currency_info();
		$company_info = $CI->Orders->retrieve_company();
		$data=array(
			'title'				=> display('order_detail'),
			'order_id'			=>	$order_detail[0]['order_id'],
			'order_no'			=>	$order_detail[0]['id'],
			'customer_name'		=>	$order_detail[0]['customer_name'],
			'customer_address'	=>	$order_detail[0]['customer_short_address'],
			'customer_mobile'	=>	$order_detail[0]['customer_mobile'],
			'customer_email'	=>	$order_detail[0]['customer_email'],
			'final_date'		=>	$order_detail[0]['final_date'],
			'total_amount'		=>	$order_detail[0]['total_amount'],
			'subTotal_discount'	=>	$order_detail[0]['total_discount'],
			'paid_amount'		=>	$order_detail[0]['paid_amount'],
			'due_amount'		=>	$order_detail[0]['due_amount'],
			'subTotal_quantity'	=>	$subTotal_quantity,
			'order_all_data' 	=>	$order_detail,
			'company_info'		=>	$company_info,
			'currency' 			=> $currency_details[0]['currency_icon'],
			'position' 			=> $currency_details[0]['currency_position'],
			);
		$chapterList = $CI->parser->parse('order/pos_order_html',$data,true);
		return $chapterList;
	}
	//Retrieve  order List
	public function pre_order_list($links,$per_page,$page,$pre_order_no=null,$customer=null,$date=null)
	{
		$CI =& get_instance();
		$CI->load->model('Orders');
		$CI->load->model('Soft_settings');
		$CI->load->library('occational');
		
		$orders_list = $CI->Orders->pre_order_list($per_page,$page,$pre_order_no,$customer,$date);
		$customer_list = $CI->Customers->customer_list();

		if(!empty($orders_list)){
			foreach($orders_list as $k=>$v){
				$orders_list[$k]['final_date'] = $CI->occational->dateConvert($orders_list[$k]['date']);
			}
			$i=0;
			foreach($orders_list as $k=>$v){$i++;
			   $orders_list[$k]['sl']=$i;
			}
		}

		$currency_details = $CI->Soft_settings->retrieve_currency_info();
		$data = array(
				'title'    => display('manage_pre_order'),
				'orders_list' => $orders_list,
				'customer_list' => $customer_list,
				'links'    => $links,
				'currency' => $currency_details[0]['currency_icon'],
				'position' => $currency_details[0]['currency_position'],
			);
		$orderList = $CI->parser->parse('order/pre_order',$data,true);
		return $orderList;
	}
	//Order Details Data
	public function pre_order_details_data($order_id)
	{
		$CI =& get_instance();
		$CI->load->model('Orders');
		$CI->load->model('Soft_settings');
		$CI->load->library('occational');
		$CI->load->library('Pdfgenerator');
		$order_detail = $CI->Orders->retrieve_pre_order_html_data($order_id);

		$subTotal_quantity 	= 0;
		$subTotal_cartoon 	= 0;
		$subTotal_discount 	= 0;

		if(!empty($order_detail)){
			foreach($order_detail as $k=>$v){
				$order_detail[$k]['final_date'] = $CI->occational->dateConvert($order_detail[$k]['date']);
				$subTotal_quantity = $subTotal_quantity+$order_detail[$k]['quantity'];
			}
			$i=0;
			foreach($order_detail as $k=>$v){$i++;
			   $order_detail[$k]['sl']=$i;
			}
		}

		$currency_details = $CI->Soft_settings->retrieve_currency_info();
		$company_info = $CI->Orders->retrieve_company();
		$data=array(
			'title'				=>	display('order_details'),
			'order_id'			=>	$order_detail[0]['order_id'],
			'order_no'			=>	$order_detail[0]['id'],
			'customer_address'	=>	$order_detail[0]['customer_short_address'],
			'customer_name'		=>	$order_detail[0]['customer_name'],
			'customer_mobile'	=>	$order_detail[0]['customer_mobile'],
			'customer_email'	=>	$order_detail[0]['customer_email'],
			'final_date'		=>	$order_detail[0]['final_date'],
			'total_amount'		=>	$order_detail[0]['total_amount'],
			'total_discount' 	=>	$order_detail[0]['total_discount'] + $order_detail[0]['order_discount'],
			'service_charge' 	=>	$order_detail[0]['service_charge'],
			'paid_amount'		=>	$order_detail[0]['paid_amount'],
			'due_amount'		=>	$order_detail[0]['total_amount']-$order_detail[0]['paid_amount'],
			'details'			=>	$order_detail[0]['details'],
			'subTotal_quantity'	=>	$subTotal_quantity,
			'order_all_data' 	=>	$order_detail,
			'company_info'		=>	$company_info,
			'currency' 			=> 	$currency_details[0]['currency_icon'],
			'position' 			=> 	$currency_details[0]['currency_position'],
			);

		$chapterList = $CI->parser->parse('order/pre_order_html',$data,true);
		return $chapterList;
	}
	//order Edit Data
	public function pre_order_edit_data($order_id)
	{
		$CI =& get_instance();
		$CI->load->model('Orders');

		$order_detail 	  = $CI->Orders->retrieve_pre_order_editdata($order_id);
		$i=0;
		foreach($order_detail as $k=>$v){$i++;
		   $order_detail[$k]['sl']=$i;
		}

		$data=array(
			'title'				=> 	display('order_update'),
			'order_id'			=>	$order_detail[0]['order_id'],
			'customer_id'		=>	$order_detail[0]['customer_id'],
			'customer_name'		=>	$order_detail[0]['customer_name'],
			'date'				=>	$order_detail[0]['date'],
			'total_amount'		=>	$order_detail[0]['total_amount'],
			'paid_amount'		=>	$order_detail[0]['paid_amount'],
			'seller_id'			=>	$order_detail[0]['seller_id'],
			'due_amount'		=>	$order_detail[0]['total_amount']-$order_detail[0]['paid_amount'],
			'total_discount'	=>	$order_detail[0]['total_discount'],
			'product_discount'	=>	$order_detail[0]['total_discount']-$order_detail[0]['order_discount'],
			'order_discount'	=>	$order_detail[0]['order_discount'],
			'service_charge'	=>	$order_detail[0]['service_charge'],
			'details'			=>	$order_detail[0]['details'],
			'status'			=>	$order_detail[0]['status'],
			'order_all_data'	=>	$order_detail,
			);

		$chapterList = $CI->parser->parse('order/edit_pre_order_form',$data,true);
		return $chapterList;
	}
	//Order Tracking
	public function order_traking($order_id=null)
	{
		$CI =& get_instance();
		$CI->load->model('Orders');
		$CI->load->model('Email_templates');

		$order_traking  = $CI->Orders->order_traking($order_id);
		$get_order_no   = $CI->Orders->get_order_no($order_id);
		$order_message  = $CI->Orders->order_message($order_id);
		$template_list  = $CI->Email_templates->template_list();

		$data = array(
				'title' 		=> display('order_tracking'),
				'order_no' 		=> $get_order_no->order_no,
				'customer_email'=> $get_order_no->customer_email,
				'order_traking' => $order_traking,
				'order_id'  	=> $order_id,
				'order_message'	=> $order_message,
				'template_list' => $template_list,
			);
		$orderTracking = $CI->parser->parse('order/order_traking',$data,true);
		return $orderTracking;
	}
}
?>