<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lsetting {

	//Product add form
	public function setting_add_form(){
		$CI =& get_instance();
		$CI->load->model('seller/Settings');

		$seller = $CI->Settings->seller_guarantee();

		$data = array(
			'title'    	 => display('setting'),
			'seller_guarantee' => $seller->seller_guarantee,
		);

		$productList = $CI->parser->parse('seller_dashboard/setting/setting_form',$data,true);
		return $productList;
	}
}
?>