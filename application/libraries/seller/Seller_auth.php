<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Seller_auth {

	//Login....
	public function login($email,$password)
	{
		$CI 	=& get_instance();
		$result = $this->check_valid_seller($email,$password);

        if ($result)
		{
			$key = md5(time());
			$key = str_replace("1", "z", $key);
			$key = str_replace("2", "J", $key);
			$key = str_replace("3", "y", $key);
			$key = str_replace("4", "R", $key);
			$key = str_replace("5", "Kd", $key);
			$key = str_replace("6", "jX", $key);
			$key = str_replace("7", "dH", $key);
			$key = str_replace("8", "p", $key);
			$key = str_replace("9", "Uf", $key);
			$key = str_replace("0", "eXnyiKFj", $key);
			$seller_sid_web = substr($key, rand(0, 3), rand(28, 32));
			
			// codeigniter session stored data			
			$seller_data = array(
				'seller_sid_web' => $seller_sid_web,
				'seller_id' 	 => $result[0]['seller_id'],
				'seller_name' 	 => $result[0]['first_name']." ".$result[0]['last_name'],
				'business_name' => $result[0]['business_name'],
				'seller_image' => $result[0]['image'],
 
				//seller info
				'seller_first_name'  => $result[0]['first_name'], 
			 	'seller_last_name'   => $result[0]['last_name'], 
			 	'seller_email'   	 => $result[0]['email'], 
			);

          	$CI->session->set_userdata($seller_data);
           	return TRUE;
		}else{
			return FALSE;
        }
	}

	//Check valid seller
	function check_valid_seller($email,$password)
	{ 	
		$CI 		=& get_instance();
		$password 	= md5("gef".$password);
        $CI->db->where(array('email'=>$email,'password'=>$password,'status' => '1'));
		$query 		= $CI->db->get('seller_information');
		$result 	= $query->result_array();
		
		if (count($result) == 1)
		{
			$CI->db->select('*');
			$CI->db->from('seller_information');
			$CI->db->where('id',$result[0]['id']);
			$query = $CI->db->get();
			return $query->result_array();
		}
		return false;
	}

	//Logout....
	public function logout()
	{
		$CI =& get_instance();
		$seller_data = array(
				'seller_sid_web','seller_id','seller_name','seller_email','business_name','seller_image'
			);
        $CI->session->unset_userdata($seller_data);
		return true;
	}

	//Check seller auth
	function check_seller_auth($url='')
	{   
        if($url==''){$url = base_url('seller-login');}

		$CI =& get_instance();
        if ((!$this->is_logged()))
		{ 
			$this->logout();
			$error = display('you_are_not_authorised');
			$CI->session->set_userdata(array('error_message'=>$error));
            redirect($url,'refresh'); exit;
        }
	}

	//Check if is logged....
	public function is_logged()
	{
		$CI =& get_instance();
        if($CI->session->userdata('seller_sid_web'))
		{
			return true;
		}
		return false;
	}
	
	//This function is used to Generate Key
	public function generator($lenth)
	{
		$number=array("A","B","C","D","E","F","G","H","I","J","K","L","N","M","O","P","Q","R","S","U","V","T","W","X","Y","Z","1","2","3","4","5","6","7","8","9","0");
	
		for($i=0; $i<$lenth; $i++)
		{
			$rand_value=rand(0,34);
			$rand_number=$number["$rand_value"];
		
			if(empty($con))
			{ 
				$con=$rand_number;
			}
			else
			{
				$con="$con"."$rand_number";
			}
		}
		return $con;
	}
}
?>