<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lproduct {

	//Product add form
	public function product_add_form(){
		$CI =& get_instance();
		$CI->load->model('seller/Products');
		$CI->load->model('Categories');
		$CI->load->model('Units');
		$CI->load->model('Brands');
		$CI->load->model('Variants');
		$CI->load->model('Sellers');

		$upload_id     = $CI->session->userdata('upload_id');
		$category_list = $CI->Categories->category_list();
		$upload_image  = $CI->Products->retrive_product_image_edit_data($upload_id);
		$unit_list 	   = $CI->Units->unit_list();
		$brand_list    = $CI->Brands->brand_list();
		$variant_list  = $CI->Variants->variant_list();
		$languages 	   = $CI->Sellers->languages();

		$data = array(
				'title'    	 => display('upload_product'),
				'category_list' => $category_list,
				'unit_list'  => $unit_list,
				'brand_list' => $brand_list,
				'variant_list' => $variant_list,
				'language'   => $languages,
				'upload_image'  => $upload_image
			);

		$productList = $CI->parser->parse('seller_dashboard/product/upload_product',$data,true);
		return $productList;
	}

	//Manage Product
	public function manage_product($links,$per_page,$page,$model,$title,$category){
		$CI =& get_instance();
		$CI->load->model('seller/Products');
		$CI->load->model('Categories');
		$product_list = $CI->Products->manage_product($per_page,$page,$model,$title,$category);
		$category_list = $CI->Categories->category_list($per_page,$page,$model,$title,$category);

		if(!empty($product_list)){
			$i=0;
			foreach($product_list as $k=>$v){$i++;
			   $product_list[$k]['sl']=$i;
			}
		}

		$data = array(
			'title' 	  => display('manage_product'), 
			'product_list'=> $product_list,
			'category_list'=> $category_list,
			'links'  	  => $links
		);

		$sellerForm = $CI->parser->parse('seller_dashboard/product/product',$data,true);
		return $sellerForm;
	}

	//Seller Product Update Form
	public function seller_product_update_form($upload_id)
	{
		$CI =& get_instance();
		$CI->load->model('seller/Products');
		$CI->load->model('Sellers');
		$CI->load->model('Categories');
		$CI->load->model('Units');
		$CI->load->model('Brands');
		$CI->load->model('Variants');

		$seller_product 	 = $CI->Products->retrive_seller_product_edit_data($upload_id);
		$upload_image  		 = $CI->Products->retrive_seller_image_edit_data($upload_id);
		$product_title 		 = $CI->Products->retrive_seller_title_edit_data($upload_id);
		$product_description = $CI->Products->retrive_seller_description_edit_data($upload_id); 
		$product_specification = $CI->Products->retrive_seller_specification_edit_data($upload_id);

		$in_stock = $CI->Products->get_product_stock($seller_product->product_id);

		if (empty($seller_product)) {
			$CI->session->set_userdata('error_message',display('product_not_found'));
			redirect('seller/product/manage_product');
		}

		$sellers_list  = $CI->Sellers->seller_list();
		$category_list = $CI->Categories->category_list();
		$unit_list 	   = $CI->Units->unit_list();
		$brand_list    = $CI->Brands->brand_list();
		$variant_list  = $CI->Variants->variant_list();
		$languages 	   = $CI->Sellers->languages();

		$data=array(
			'title'			=> display('seller_edit_product'),
			'upload_id' 	=> $seller_product->upload_id,
			'product_id' 	=> $seller_product->product_id,
			'seller_id' 	=> $seller_product->seller_id,
			'seller_uni_id' => $seller_product->seller_uni_id,
			'first_name' 	=> $seller_product->first_name,
			'last_name' 	=> $seller_product->last_name,
			'category_id' 	=> $seller_product->category_id,
			'price' 		=> $seller_product->price,
			'unit_id' 		=> $seller_product->unit,
			'product_model' => $seller_product->product_model,
			'product_brand' => $seller_product->brand_id,
			'variant_id' 	=> $seller_product->variant_id,
			'product_type' 	=> $seller_product->product_type,
			'tag' 			=> $seller_product->tag,
			'quantity' 		=> $seller_product->quantity,
			'status' 		=> $seller_product->status,
			'category_name' => $seller_product->category_name,
			'brand_name' 	=> $seller_product->brand_name,
			'brand_id' 		=> $seller_product->brand_id,
			'status' 		=> $seller_product->status,
			'best_sale' 	=> $seller_product->best_sale,
			'on_promotion' 	=> $seller_product->on_promotion,
			'pre_order' 	=> $seller_product->pre_order,
			'pre_order_quantity' 	=> $seller_product->pre_order_quantity,
			'details' 		=> $seller_product->details,
			'in_stock' 		=> $in_stock,

			'sellers_list'  => $sellers_list,
			'category_list' => $category_list,
			'unit_list'	    => $unit_list,
			'brand_list'    => $brand_list,
			'variant_list'  => $variant_list,
			'upload_image'  => $upload_image,
			'language'  	=> $languages,
			'product_title' => $product_title,
			'product_description'  	=> $product_description,
			'product_specification' => $product_specification,
		);
	
		$chapterList = $CI->parser->parse('seller_dashboard/product/edit_upload_product',$data,true);
		return $chapterList;
	}

}
?>