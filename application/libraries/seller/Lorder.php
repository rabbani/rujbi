<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lorder {
	
	//Retrieve order List
	public function order_list($links,$per_page,$page,$order_no,$pre_order_no,$customer,$date,$order_status)
	{
		$CI =& get_instance();
		$CI->load->model('website/customer/Orders');
		$CI->load->model('Soft_settings');
		$CI->load->model('Customers');
		$CI->load->library('occational');
		
		$orders_list   = $CI->Orders->order_list($per_page,$page,$order_no,$pre_order_no,$customer,$date,$order_status);
		$customer_list = $CI->Customers->customer_list();

		if(!empty($orders_list)){
			foreach($orders_list as $k=>$v){
				$orders_list[$k]['final_date'] = $CI->occational->dateConvert($orders_list[$k]['date']);
			}
			$i=0;
			foreach($orders_list as $k=>$v){$i++;
			   $orders_list[$k]['sl']=$i;
			}
		}

		$currency_details = $CI->Soft_settings->retrieve_currency_info();
		$data = array(
				'title'    => display('manage_order'),
				'orders_list' => $orders_list,
				'customer_list' => $customer_list,
				'links'    => $links,
				'currency' => $currency_details[0]['currency_icon'],
				'position' => $currency_details[0]['currency_position'],
			);
		$orderList = $CI->parser->parse('seller_dashboard/order/order',$data,true);
		return $orderList;
	}
	//Order details data
	public function order_details_data($order_id)
	{
		$CI =& get_instance();
		$CI->load->model('website/customer/Orders');
		$CI->load->model('Soft_settings');
		$CI->load->library('occational');
		$CI->load->library('Pdfgenerator');
		$order_detail = $CI->Orders->retrieve_order_html_data($order_id);

		$subTotal_quantity = 0;
		$subTotal_cartoon = 0;
		$subTotal_discount = 0;

		if(!empty($order_detail)){
			foreach($order_detail as $k=>$v){
				$order_detail[$k]['final_date'] = $CI->occational->dateConvert($order_detail[$k]['date']);
				$subTotal_quantity = $subTotal_quantity+$order_detail[$k]['quantity'];
			}
			$i=0;
			foreach($order_detail as $k=>$v){$i++;
			   $order_detail[$k]['sl']=$i;
			}
		}

		$currency_details = $CI->Soft_settings->retrieve_currency_info();
		$company_info = $CI->Orders->retrieve_company();
		$data=array(
			'title'				=>	display('order_details'),
			'order_id'			=>	$order_detail[0]['order_id'],
			'order_no'			=>	$order_detail[0]['id'],
			'customer_address'	=>	$order_detail[0]['customer_short_address'],
			'customer_name'		=>	$order_detail[0]['customer_name'],
			'customer_mobile'	=>	$order_detail[0]['customer_mobile'],
			'customer_email'	=>	$order_detail[0]['customer_email'],
			'final_date'		=>	$order_detail[0]['final_date'],
			'total_amount'		=>	$order_detail[0]['total_amount'],
			'total_discount' 	=>	$order_detail[0]['total_discount'] + $order_detail[0]['order_discount'],
			'service_charge' 	=>	$order_detail[0]['service_charge'],
			'paid_amount'		=>	$order_detail[0]['paid_amount'],
			'due_amount'		=>	$order_detail[0]['total_amount']-$order_detail[0]['paid_amount'],
			'details'			=>	$order_detail[0]['details'],
			'subTotal_quantity'	=>	$subTotal_quantity,
			'order_all_data' 	=>	$order_detail,
			'company_info'		=>	$company_info,
			'currency' 			=> 	$currency_details[0]['currency_icon'],
			'position' 			=> 	$currency_details[0]['currency_position'],
			);

		$orderDetails = $CI->parser->parse('seller_dashboard/order/order_html',$data,true);
		return $orderDetails;
	}
}
?>