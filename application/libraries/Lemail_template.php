<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lemail_template {
	//Add template
	public function template_add_form()
	{
		$CI =& get_instance();
		$CI->load->model('Email_templates');
		$data = array(
				'title' => display('add_template')
			);
		$customerForm = $CI->parser->parse('template/add_template',$data,true);
		return $customerForm;
	}

	//Retrieve template List	
	public function template_list()
	{
		$CI =& get_instance();
		$CI->load->model('Email_templates');
		$template_list = $CI->Email_templates->template_list(); 

		$i=0;
		if(!empty($template_list)){	
			foreach($template_list as $k=>$v){$i++;
			   $template_list[$k]['sl']=$i;
			}
		}

		$data = array(
				'title' => display('manage_template'),
				'template_list' => $template_list,
			);
		$customerList = $CI->parser->parse('template/template',$data,true);
		return $customerList;
	}

	//template Edit Data
	public function template_edit_data($template_id)
	{
		$CI =& get_instance();
		$CI->load->model('Email_templates');
		$template_details = $CI->Email_templates->retrieve_template_editdata($template_id);
	
		$data=array(
			'title' 	 => display('edit_template'),
			'template_id'=> $template_details->id,
			'name'		 => $template_details->name,
			'subject' 	 => $template_details->subject,
			'message1' 	 => $template_details->message,
			'status' 	 => $template_details->status
			);
		$chapterList = $CI->parser->parse('template/edit_template',$data,true);
		return $chapterList;
	}
}
?>