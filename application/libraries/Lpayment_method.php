<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lpayment_method {
	//Add payment_method
	public function payment_method_add_form()
	{
		$CI =& get_instance();
		$CI->load->model('Payment_methods');
		$CI->load->model('Categories');

		$category_list = $CI->Categories->category_list(); 

		$data = array(
				'title' => display('add_payment_method'),
				'category_list' => $category_list,
			);
		$customerForm = $CI->parser->parse('payment_method/add_payment_method',$data,true);
		return $customerForm;
	}

	//Retrieve payment_method List	
	public function payment_method_list()
	{
		$CI =& get_instance();
		$CI->load->model('Payment_methods');
		$payment_method_list = $CI->Payment_methods->payment_method_list(); 

		$i=0;
		if(!empty($payment_method_list)){	
			foreach($payment_method_list as $k=>$v){$i++;
			   $payment_method_list[$k]['sl']=$i;
			}
		}

		$data = array(
				'title' => display('manage_payment_method'),
				'payment_method_list' => $payment_method_list,
			);
		$customerList = $CI->parser->parse('payment_method/payment_method',$data,true);
		return $customerList;
	}

	//payment_method Edit Data
	public function payment_method_edit_data($method_id)
	{
		$CI =& get_instance();
		$CI->load->model('Payment_methods');
		$CI->load->model('Categories');
		$category_list = $CI->Categories->category_list();

		$payment_method_details = $CI->Payment_methods->retrieve_payment_method_editdata($method_id);
	
		$data=array(
			'title' 		=> display('payment_method_edit'),
			'method_id' 	=> $payment_method_details[0]['method_id'],
			'method_name' 	=> $payment_method_details[0]['method_name'],
			'details' 		=> $payment_method_details[0]['details'],
			'position' 		=> $payment_method_details[0]['position'],
			
			);
		$chapterList = $CI->parser->parse('payment_method/edit_payment_method',$data,true);
		return $chapterList;
	}
}
?>