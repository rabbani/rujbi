<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lorder {

	//Order add form
	public function order_add_form()
	{
		$CI =& get_instance();
		$data = array(
				'title' => display('new_order'),
			);
		$orderForm = $CI->parser->parse('website/customer/order/add_order_form',$data,true);
		return $orderForm;
	}
	//Retrieve order list
	public function order_list($links,$per_page,$page,$order_no,$date,$order_status)
	{
		$CI =& get_instance();
		$CI->load->model('website/customer/Orders');
		$CI->load->model('Soft_settings');
		$CI->load->library('occational');
		
		$orders_list = $CI->Orders->order_list($per_page,$page,$order_no,$date,$order_status);

		if(!empty($orders_list)){
			foreach($orders_list as $k=>$v){
				//$orders_list[$k]['final_date'] = $CI->occational->dateConvert($orders_list[$k]['date']);
			}
			$i=0;
			foreach($orders_list as $k=>$v){$i++;
			   $orders_list[$k]['sl']=$i;
			}
		}

		$currency_details = $CI->Soft_settings->retrieve_currency_info();
		$data = array(
				'title'    => display('manage_order'),
				'orders_list' => $orders_list,
				'links'    => $links,
				'currency' => $currency_details[0]['currency_icon'],
				'position' => $currency_details[0]['currency_position'],
			);
		$orderList = $CI->parser->parse('website/customer/order/order',$data,true);
		return $orderList;
	}	
	//Order tracking
	public function order_tracking($links,$per_page,$page,$order_no,$date,$order_status)
	{
		$CI =& get_instance();
		$CI->load->model('website/customer/Orders');
		$CI->load->model('Soft_settings');
		$CI->load->library('occational');
		
		$order_list = $CI->Orders->order_list($per_page,$page,$order_no,$date,$order_status);

		if(!empty($order_list)){
			$i=0;
			foreach($order_list as $k=>$v){$i++;
			   $order_list[$k]['sl']=$i;
			}
		}

		$currency_details = $CI->Soft_settings->retrieve_currency_info();
		$data = array(
				'title'    => display('order_tracking'),
				'order_list' => $order_list,
				'links' 	=> $links,
				'currency' => $currency_details[0]['currency_icon'],
				'position' => $currency_details[0]['currency_position'],
			);
		$orderList = $CI->parser->parse('website/customer/order/order_tracking',$data,true);
		return $orderList;
	}

	//Order Tracking
	public function order_traking($order_id=null)
	{
		$CI =& get_instance();
		$CI->load->model('website/customer/Orders');
		$CI->load->model('Email_templates');

		$order_traking  = $CI->Orders->order_traking($order_id);
		$get_order_no   = $CI->Orders->get_order_no($order_id);
		$order_message  = $CI->Orders->order_message($order_id);
		$template_list  = $CI->Email_templates->template_list();

		$data = array(
				'title' 		=> display('order_tracking'),
				'order_no' 		=> $get_order_no->id,
				'customer_email'=> $get_order_no->customer_email,
				'order_traking' => $order_traking,
				'order_id'  	=> $order_id,
				'order_message'	=> $order_message,
				'template_list' => $template_list,
			);
		$orderTracking = $CI->parser->parse('website/customer/order/order_traking',$data,true);
		return $orderTracking;
	}

	//Order details data
	public function order_details_data($order_id)
	{
		$CI =& get_instance();
		$CI->load->model('website/customer/Orders');
		$CI->load->model('Soft_settings');
		$CI->load->library('occational');
		$CI->load->library('Pdfgenerator');
		$order_detail = $CI->Orders->retrieve_order_html_data($order_id);

		$subTotal_quantity = 0;
		$subTotal_cartoon = 0;
		$subTotal_discount = 0;

		if(!empty($order_detail)){
			foreach($order_detail as $k=>$v){
				$order_detail[$k]['final_date'] = $CI->occational->dateConvert($order_detail[$k]['date']);
				$subTotal_quantity = $subTotal_quantity+$order_detail[$k]['quantity'];
			}
			$i=0;
			foreach($order_detail as $k=>$v){$i++;
			   $order_detail[$k]['sl']=$i;
			}
		}

		$currency_details = $CI->Soft_settings->retrieve_currency_info();
		$company_info = $CI->Orders->retrieve_company();
		$data=array(
			'title'				=>	display('order_details'),
			'order_id'			=>	$order_detail[0]['order_id'],
			'order_no'			=>	$order_detail[0]['id'],
			'customer_address'	=>	$order_detail[0]['customer_short_address'],
			'customer_name'		=>	$order_detail[0]['customer_name'],
			'customer_mobile'	=>	$order_detail[0]['customer_mobile'],
			'customer_email'	=>	$order_detail[0]['customer_email'],
			'final_date'		=>	$order_detail[0]['final_date'],
			'total_amount'		=>	$order_detail[0]['total_amount'],
			'total_discount' 	=>	$order_detail[0]['total_discount'] + $order_detail[0]['order_discount'],
			'service_charge' 	=>	$order_detail[0]['service_charge'],
			'paid_amount'		=>	$order_detail[0]['paid_amount'],
			'due_amount'		=>	$order_detail[0]['total_amount']-$order_detail[0]['paid_amount'],
			'details'			=>	$order_detail[0]['details'],
			'subTotal_quantity'	=>	$subTotal_quantity,
			'order_all_data' 	=>	$order_detail,
			'company_info'		=>	$company_info,
			'currency' 			=> 	$currency_details[0]['currency_icon'],
			'position' 			=> 	$currency_details[0]['currency_position'],
			);

		$orderDetails = $CI->parser->parse('website/customer/order/order_html',$data,true);
		return $orderDetails;
	}
	//Order Edit Data
	public function order_edit_data($order_id)
	{
		$CI =& get_instance();
		$CI->load->model('website/customer/Orders');

		$order_detail 	  = $CI->Orders->retrieve_order_editdata($order_id);
		$i=0;
		foreach($order_detail as $k=>$v){$i++;
		   $order_detail[$k]['sl']=$i;
		}

		$data=array(
			'title'				=> 	display('order_update'),
			'order_id'			=>	$order_detail[0]['order_id'],
			'customer_id'		=>	$order_detail[0]['customer_id'],
			'customer_name'		=>	$order_detail[0]['customer_name'],
			'date'				=>	$order_detail[0]['date'],
			'total_amount'		=>	$order_detail[0]['total_amount'],
			'paid_amount'		=>	$order_detail[0]['paid_amount'],
			'seller_id'			=>	$order_detail[0]['seller_id'],
			'due_amount'		=>	$order_detail[0]['total_amount']-$order_detail[0]['paid_amount'],
			'total_discount'	=>	$order_detail[0]['total_discount'],
			'product_discount'	=>	$order_detail[0]['total_discount']-$order_detail[0]['order_discount'],
			'order_discount'	=>	$order_detail[0]['order_discount'],
			'service_charge'	=>	$order_detail[0]['service_charge'],
			'details'			=>	$order_detail[0]['details'],
			'status'			=>	$order_detail[0]['status'],
			'order_all_data'	=>	$order_detail,
			);

		$orderEditData = $CI->parser->parse('website/customer/order/edit_order_form',$data,true);
		return $orderEditData;
	}
	//Order Html Data
	public function order_html_data($order_id)
	{
		$CI =& get_instance();
		$CI->load->model('website/customer/Orders');
		$CI->load->model('Soft_settings');
		$CI->load->library('occational');
		$CI->load->library('Pdfgenerator');
		$order_detail = $CI->Orders->retrieve_order_html_data($order_id);

		$subTotal_quantity 	= 0;
		$subTotal_cartoon 	= 0;
		$subTotal_discount 	= 0;

		if(!empty($order_detail)){
			foreach($order_detail as $k=>$v){
				$order_detail[$k]['final_date'] = $CI->occational->dateConvert($order_detail[$k]['date']);
				$subTotal_quantity = $subTotal_quantity+$order_detail[$k]['quantity'];
			}
			$i=0;
			foreach($order_detail as $k=>$v){$i++;
			   $order_detail[$k]['sl']=$i;
			}
		}

		$currency_details = $CI->Soft_settings->retrieve_currency_info();
		$company_info 	  = $CI->Orders->retrieve_company();
		$data = array(
			'title'				=>	display('order_details'),
			'order_id'			=>	$order_detail[0]['order_id'],
			'order_no'			=>	$order_detail[0]['order_id'],
			'customer_address'	=>	$order_detail[0]['customer_short_address'],
			'customer_name'		=>	$order_detail[0]['customer_name'],
			'customer_mobile'	=>	$order_detail[0]['customer_mobile'],
			'customer_email'	=>	$order_detail[0]['customer_email'],
			'final_date'		=>	$order_detail[0]['final_date'],
			'total_amount'		=>	$order_detail[0]['total_amount'],
			'order_discount' 	=>	$order_detail[0]['order_discount'],
			'total_discount' 	=>	$order_detail[0]['total_discount'] + $order_detail[0]['order_discount'],
			'paid_amount'		=>	$order_detail[0]['paid_amount'],
			'due_amount'		=>	$order_detail[0]['total_amount']-$order_detail[0]['paid_amount'],
			'details'			=>	$order_detail[0]['details'],
			'service_charge'	=>	$order_detail[0]['service_charge'],
			'subTotal_quantity'	=>	$subTotal_quantity,
			'order_all_data' 	=>	$order_detail,
			'company_info'		=>	$company_info,
			'currency' 			=> 	$currency_details[0]['currency_icon'],
			'position' 			=> 	$currency_details[0]['currency_position'],
			);

		$chapterList = $CI->parser->parse('order/order_pdf',$data,true);

		//PDF Generator 
		$dompdf = new DOMPDF();
	    $dompdf->load_html($chapterList);
	    $dompdf->render();
	    $output = $dompdf->output();
	    file_put_contents('my-assets/pdf/'.$order_id.'.pdf', $output);
	    $file_path = 'my-assets/pdf/'.$order_id.'.pdf';

	    //File path save to database
	    $CI->db->set('file_path',base_url($file_path));
	    $CI->db->where('order_id',$order_id);
	    $CI->db->update('order');

	    $send_email = '';
	    if (!empty($data['customer_email'])) {
	    	$send_email = $this->setmail($data['customer_email'],$file_path);
	    }

	    if ($send_email != null) {
	    	return true;
	    }else{
	    	return false;
	    }
	}
	//Send Customer Email with invoice
	public function setmail($email,$file_path)
	{
		$CI =& get_instance();
		$CI->load->model('Soft_settings');
		$setting_detail = $CI->Soft_settings->retrieve_email_editdata();

		$subject = display("order_information");
		$message = display("order_info_details").'<br>'.base_url();

	    $config = array(
	      	'protocol' 		=> $setting_detail[0]['protocol'],
	      	'smtp_host' 	=> $setting_detail[0]['smtp_host'],
	      	'smtp_port' 	=> $setting_detail[0]['smtp_port'],
	      	'smtp_user' 	=> $setting_detail[0]['sender_email'], 
	      	'smtp_pass' 	=> $setting_detail[0]['password'], 
	      	'mailtype' 		=> $setting_detail[0]['mailtype'],
	      	'charset' 		=> 'utf-8'
	    );
	    
	    $CI->load->library('email', $config);
	    $CI->email->set_newline("\r\n");
	    $CI->email->from($setting_detail[0]['sender_email']);
	    $CI->email->to($email);
	    $CI->email->subject($subject);
	    $CI->email->message($message);
	    $CI->email->attach($file_path);

	   	$check_email = $this->test_input($email);
		if (filter_var($check_email, FILTER_VALIDATE_EMAIL)) {
		    if($CI->email->send())
		    {
		      	$CI->session->set_userdata(array('message'=>display('email_send_to_customer')));
		      	return true;
		    }else{
		     	$CI->session->set_userdata(array('error_message'=>display('email_not_send')));
		     	return false;
		    }
		}else{
			$CI->session->set_userdata(array('message'=>display('successfully_added')));
		   	return true;
		}
	}
	//Email testing for email
	public function test_input($data) {
	  	$data = trim($data);
	  	$data = stripslashes($data);
	  	$data = htmlspecialchars($data);
	  	return $data;
	}
}
?>