<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lproduct {

	//Product Details Page Load Here
	public function product_details($p_id)
	{
		$CI =& get_instance();
		$CI->load->model('website/Products_model');
		$CI->load->model('website/Homes');
		$CI->load->model('web_settings');
		$CI->load->model('Soft_settings');
		$CI->load->model('Blocks');
		$CI->load->model('Companies');
		$CI->load->model('website/Categories');

		$pro_category_list 		= $CI->Homes->category_list();
		$top_category_list 		= $CI->Homes->top_category_list();
		$parent_category_list 	= $CI->Homes->parent_category_list();
		$best_sales 			= $CI->Homes->best_sales();
		$footer_block 			= $CI->Homes->footer_block();
		$slider_list 			= $CI->web_settings->slider_list();
		$block_list 			= $CI->Blocks->block_list(); 
		$product_info 			= $CI->Products_model->product_info($p_id); 
		$category_id 			= $product_info->category_id;
		$product_id 			= $product_info->product_id;
		$best_sales_category 	= $CI->Products_model->best_sales_category($product_id);
		$get_thumb_image 		= $CI->Products_model->get_thumb_image($product_id);
		$related_product 		= $CI->Products_model->related_product($category_id,$product_id);
		$select_single_category = $CI->Categories->select_single_category($category_id);
		$currency_details 		= $CI->Soft_settings->retrieve_currency_info();
		$soft_settings 			= $CI->Soft_settings->retrieve_setting_editdata();
		$web_settings 			= $CI->web_settings->retrieve_setting_editdata();
		$languages 				= $CI->Homes->languages();
		$currency_info 			= $CI->Homes->currency_info();
		$company_info  			= $CI->Companies->company_list();
		$selected_currency_info = $CI->Homes->selected_currency_info();
		$review_list 			= $CI->Products_model->review_list($product_id);
		$refund_policy 			= $CI->Products_model->retrieve_refund_policy();
		$product_stock 			= $CI->Products_model->stock_report_single_item($product_id);

		$data = array(
				'title' 		=> display('product_details'),
				'category_list' => $parent_category_list,
				'top_category_list' => $top_category_list,
				'slider_list' 	=> $slider_list,
				'block_list' 	=> $block_list,
				'best_sales' 	=> $best_sales,
				'footer_block' 	=> $footer_block,
				'product_name' 	=> $product_info->title,
				'product_image' 	=> $product_info->thumb_image_url,
				'product_description' 	=> $product_info->description,
				'product_id' 	=> $product_info->product_id,
				'seller_id' 	=> $product_info->seller_id,
				'brand_id' 		=> $product_info->brand_id,
				'brand_name'	=> $product_info->brand_name,
				'product_model' => $product_info->product_model,
				'business_name' => $product_info->business_name,
				'price' 		=> $product_info->price,
				'on_sale' 		=> $product_info->on_sale,
				'offer_price' 	=> $product_info->offer_price,
				'first_name' 	=> $product_info->first_name,
				'last_name' 	=> $product_info->last_name,
				'get_thumb_image'=> $get_thumb_image,
				'variant' 		=> $product_info->variant_id,
				'category_name' => $product_info->category_name,
				'category_id' 	=> $category_id,
				'best_sales_category'=> $best_sales_category,
				'related_product' => $related_product,
				'tag' 			=> $product_info->tag,
				'seller_guarantee' => $product_info->seller_guarantee,
				'quantity' 		=> $product_info->quantity,
				'pre_order' 	=> $product_info->pre_order,
				'pre_order_quantity' 	=> (!empty($product_info->pre_order_quantity)?$product_info->pre_order_quantity:0),
				'refund_policy' => $refund_policy->details,
				'pro_category_list' => $pro_category_list,
				'soft_settings' => $soft_settings,
				'languages' 	=> $languages,
				'currency_info' => $currency_info,
				'review_list' 	=> $review_list,
				'product_stock' => $product_stock,
				'select_single_category' 	=> $select_single_category,
				'selected_cur_id' => (($selected_currency_info->currency_id)?$selected_currency_info->currency_id:""),
				'selected_currency_icon'  => $selected_currency_info->currency_icon,
				'selected_currency_name'  => $selected_currency_info->currency_name,
				'web_settings' 	=> $web_settings,
				'logo' 			=> $web_settings[0]['logo'],
				'favicon' 		=> $web_settings[0]['favicon'],
				'footer_text'   => $web_settings[0]['footer_text'],
				'company_name'  => $company_info[0]['company_name'],
				'email'  		=> $company_info[0]['email'],
				'address'  		=> $company_info[0]['address'],
				'mobile'  		=> $company_info[0]['mobile'],
				'website'  		=> $company_info[0]['website'],
				'currency' 		=> $currency_details[0]['currency_icon'],
				'position' 		=> $currency_details[0]['currency_position'],
			);

		$HomeForm = $CI->parser->parse('website/details',$data,true);
		return $HomeForm;
	}

	//Seller product
	public function seller_product($seller_id=null,$price_range=null,$size=null,$sort=null,$rate=null,$seller_score=null)
	{
		$CI =& get_instance();
		$CI->load->model('website/Products_model');
		$CI->load->model('website/Categories');
		$CI->load->model('website/Homes');
		$CI->load->model('web_settings');
		$CI->load->model('Soft_settings');
		$CI->load->model('Blocks');
		$CI->load->model('Companies');
		$CI->load->model('Variants');

		$seller_product   = $CI->Products_model->retrieve_seller_product($seller_id,$price_range,$size,$sort,$rate,$seller_score);
		$top_category_list = $CI->Homes->top_category_list();
		$seller_info 	  = $CI->Products_model->select_seller_info($seller_id);
		$categoryList 	  = $CI->Homes->parent_category_list();
		$pro_category_list= $CI->Homes->category_list();
		$best_sales 	  = $CI->Homes->best_sales();
		$footer_block 	  = $CI->Homes->footer_block();
		$block_list 	  = $CI->Blocks->block_list(); 
		$currency_details = $CI->Soft_settings->retrieve_currency_info();
		$soft_settings 	  = $CI->Soft_settings->retrieve_setting_editdata();
		$web_settings 	  = $CI->web_settings->retrieve_setting_editdata();
		$languages 		  = $CI->Homes->languages();
		$currency_info 	  = $CI->Homes->currency_info();
		$selected_currency_info = $CI->Homes->selected_currency_info();
		$company_info  	  = $CI->Companies->company_list();
		$selected_default_currency_info = $CI->Homes->selected_default_currency_info();
		$variant_list  	  = $CI->Variants->variant_list();

		$max_value   	 = $CI->Products_model->get_max_value_of_pro_seller($seller_id); 
		$min_value   	 = $CI->Products_model->get_min_value_of_pro_seller($seller_id); 


		$max = 0;
		$min = 0;
		if ($max_value->price == $min_value->price) {
			$max = $max_value->price;
			$min = 0;
		}else{
			$max = $max_value->price;
			$min = $min_value->price;
		}

		$from_price = 0;
		$to_price 	= 0;
		if (!(empty($price_range))) {
			$ex = explode("-", $price_range);
	        $from_price = $ex[0];
	        $to_price   = $ex[1];
		}

		// echo count($seller_product);
		// exit();


		$data = array(
			'title' 		=> display('seller_product'),
			'seller_product' => $seller_product,
			'top_category_list' => $top_category_list,
			'seller_id' 	=> $seller_id,
			'seller_name' 	=> $seller_info->first_name." ".$seller_info->last_name,
			'pro_category_list' => $pro_category_list,
			'category_list' => $categoryList,
			'block_list' 	=> $block_list,
			'best_sales' 	=> $best_sales,
			'footer_block' 	=> $footer_block,
			'languages' 	=> $languages,
			'currency_info' => $currency_info,
			'selected_cur_id' => (($selected_currency_info->currency_id)?$selected_currency_info->currency_id:""),
			'selected_currency_icon'  => $selected_currency_info->currency_icon,
			'selected_currency_name'  => $selected_currency_info->currency_name,
			'web_settings'  => $web_settings,
			'soft_settings' => $soft_settings,
			'logo' 			=> $web_settings[0]['logo'],
			'favicon' 		=> $web_settings[0]['favicon'],
			'footer_text'   => $web_settings[0]['footer_text'],
			'company_name'  => $company_info[0]['company_name'],
			'email'  		=> $company_info[0]['email'],
			'address'  		=> $company_info[0]['address'],
			'mobile'  		=> $company_info[0]['mobile'],
			'website'  		=> $company_info[0]['website'],
			'currency' 		=> $currency_details[0]['currency_icon'],
			'position' 		=> $currency_details[0]['currency_position'],
			'max_value' 	=> $max,
			'min_value' 	=> $min,
			'from_price' 	=> $from_price,
			'to_price' 		=> $to_price,
			'default_currency_icon'  => $selected_default_currency_info->currency_icon,
			'variant_list'  => $variant_list,
		);
		$categoryList = $CI->parser->parse('website/seller_product',$data,true);
		return $categoryList;
	}

	//Brand product
	public function brand_product($brand_id=null,$price_range=null,$size=null,$sort=null,$rate=null,$seller_score=null)
	{
		$CI =& get_instance();
		$CI->load->model('website/Products_model');
		$CI->load->model('website/Categories');
		$CI->load->model('website/Homes');
		$CI->load->model('web_settings');
		$CI->load->model('Soft_settings');
		$CI->load->model('Blocks');
		$CI->load->model('Companies');
		$CI->load->model('Variants');

		$max = 0;
		$min = 0;

		$brand_product 	  = $CI->Products_model->retrieve_brand_product($brand_id,$price_range,$size,$sort,$rate,$seller_score);
		$top_category_list = $CI->Homes->top_category_list();
		$brand_info 	  = $CI->Products_model->select_brand_info($brand_id);
		$categoryList 	  = $CI->Homes->parent_category_list();
		$pro_category_list= $CI->Homes->category_list();
		$best_sales 	  = $CI->Homes->best_sales();
		$footer_block 	  = $CI->Homes->footer_block();
		$block_list 	  = $CI->Blocks->block_list(); 
		$currency_details = $CI->Soft_settings->retrieve_currency_info();
		$soft_settings 	  = $CI->Soft_settings->retrieve_setting_editdata();
		$web_settings 	  = $CI->web_settings->retrieve_setting_editdata();
		$languages 		  = $CI->Homes->languages();
		$currency_info 	  = $CI->Homes->currency_info();
		$selected_currency_info = $CI->Homes->selected_currency_info();
		$company_info  	  = $CI->Companies->company_list();
		$selected_default_currency_info = $CI->Homes->selected_default_currency_info();
		$variant_list  	  = $CI->Variants->variant_list();

		$max_value   	 = $CI->Products_model->get_max_value_of_pro($brand_id); 
		$min_value   	 = $CI->Products_model->get_min_value_of_pro($brand_id); 

		if ($max_value->price == $min_value->price) {
			$max = $max_value->price;
			$min = 0;
		}else{
			$max = $max_value->price;
			$min = $min_value->price;
		}

		$from_price = 0;
		$to_price 	= 0;
		if (!(empty($price_range))) {
			$ex = explode("-", $price_range);
	        $from_price = $ex[0];
	        $to_price   = $ex[1];
		}

		$data = array(
			'title' 		=> display('brand_product'),
			'brand_product' => $brand_product,
			'top_category_list' => $top_category_list,
			'brand_id' 		=> $brand_id,
			'brand_name' 	=> $brand_info->brand_name,
			'pro_category_list' => $pro_category_list,
			'category_list' => $categoryList,
			'block_list' 	=> $block_list,
			'best_sales' 	=> $best_sales,
			'footer_block' 	=> $footer_block,
			'languages' 	=> $languages,
			'currency_info' => $currency_info,
			'selected_cur_id' => (($selected_currency_info->currency_id)?$selected_currency_info->currency_id:""),
			'selected_currency_icon'  => $selected_currency_info->currency_icon,
			'selected_currency_name'  => $selected_currency_info->currency_name,
			'web_settings'  => $web_settings,
			'soft_settings' => $soft_settings,
			'logo' 			=> $web_settings[0]['logo'],
			'favicon' 		=> $web_settings[0]['favicon'],
			'footer_text'   => $web_settings[0]['footer_text'],
			'company_name'  => $company_info[0]['company_name'],
			'email'  		=> $company_info[0]['email'],
			'address'  		=> $company_info[0]['address'],
			'mobile'  		=> $company_info[0]['mobile'],
			'website'  		=> $company_info[0]['website'],
			'currency' 		=> $currency_details[0]['currency_icon'],
			'position' 		=> $currency_details[0]['currency_position'],
			'links' 		=> '',
			'max_value' 	=> $max,
			'min_value' 	=> $min,
			'from_price' 	=> $from_price,
			'to_price' 		=> $to_price,
			'default_currency_icon'  => $selected_default_currency_info->currency_icon,
			'variant_list'  => $variant_list,
		);
		$categoryList = $CI->parser->parse('website/brand_product',$data,true);
		return $categoryList;
	}
}
?>