<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lrecover_password {

	//Recover page load here
	public function recover()
	{
		$CI =& get_instance();
		$CI->load->model('website/Homes');
		$CI->load->model('web_settings');
		$CI->load->model('Soft_settings');
		$CI->load->model('Blocks');
		$CI->load->model('Companies');
		$CI->load->model('website/Categories');
		$CI->load->model('website/Products_model');

		$parent_category_list 	= $CI->Homes->parent_category_list();
		$top_category_list 		= $CI->Homes->top_category_list();
		$pro_category_list 		= $CI->Homes->category_list();
		$featured_cat_list 		= $CI->Homes->featured_cat_list();
		$best_sales 			= $CI->Homes->best_sales();
		$footer_block 			= $CI->Homes->footer_block();
		$slider_list 			= $CI->web_settings->slider_list();
		$block_list 			= $CI->Blocks->block_list(); 
		$currency_details 		= $CI->Soft_settings->retrieve_currency_info();
		$soft_settings 			= $CI->Soft_settings->retrieve_setting_editdata();
		$web_settings 			= $CI->web_settings->retrieve_setting_editdata();
		$languages 				= $CI->Homes->languages();
		$currency_info 			= $CI->Homes->currency_info();
		$selected_currency_info = $CI->Homes->selected_currency_info();
		$select_home_adds 		= $CI->Homes->select_home_adds();
		$company_info  			= $CI->Companies->company_list();
		$promotion_product  	= $CI->Products_model->promotion_product();

		$data = array(
				'title' 		=> display('reset_password'),
				'category_list' => $parent_category_list,
				'top_category_list' => $top_category_list,
				'pro_category_list' => $pro_category_list,
				'slider_list' 	=> $slider_list,
				'block_list' 	=> $block_list,
				'best_sales' 	=> $best_sales,
				'footer_block' 	=> $footer_block,
				'languages' 	=> $languages,
				'currency_info' => $currency_info,
				'select_home_adds' => $select_home_adds,
				'selected_cur_id'=> (($selected_currency_info->currency_id)?$selected_currency_info->currency_id:""),
				'selected_currency_icon'  => $selected_currency_info->currency_icon,
				'selected_currency_name'  => $selected_currency_info->currency_name,
				'web_settings'  => $web_settings,
				'soft_settings' => $soft_settings,
				'promotion_product' => $promotion_product,
				'featured_cat_list' => $featured_cat_list,
				'logo' 			=> $web_settings[0]['logo'],
				'favicon' 		=> $web_settings[0]['favicon'],
				'footer_text'   => $web_settings[0]['footer_text'],
				'company_name'  => $company_info[0]['company_name'],
				'email'  		=> $company_info[0]['email'],
				'address'  		=> $company_info[0]['address'],
				'mobile'  		=> $company_info[0]['mobile'],
				'website'  		=> $company_info[0]['website'],
				'currency' 		=> $currency_details[0]['currency_icon'],
				'position' 		=> $currency_details[0]['currency_position'],
			);
		$HomeForm = $CI->parser->parse('website/recover',$data,true);
		return $HomeForm;
	}
	//Reset password
	public function reset_password()
	{
		$CI =& get_instance();
		$CI->load->model('website/Homes');
		$CI->load->model('web_settings');
		$CI->load->model('Soft_settings');
		$CI->load->model('Blocks');
		$CI->load->model('Companies');
		$CI->load->model('website/Categories');
		$CI->load->model('website/Products_model');

		$parent_category_list 	= $CI->Homes->parent_category_list();
		$top_category_list 		= $CI->Homes->top_category_list();
		$pro_category_list 		= $CI->Homes->category_list();
		$featured_cat_list 		= $CI->Homes->featured_cat_list();
		$best_sales 			= $CI->Homes->best_sales();
		$footer_block 			= $CI->Homes->footer_block();
		$slider_list 			= $CI->web_settings->slider_list();
		$block_list 			= $CI->Blocks->block_list(); 
		$currency_details 		= $CI->Soft_settings->retrieve_currency_info();
		$soft_settings 			= $CI->Soft_settings->retrieve_setting_editdata();
		$web_settings 			= $CI->web_settings->retrieve_setting_editdata();
		$languages 				= $CI->Homes->languages();
		$currency_info 			= $CI->Homes->currency_info();
		$selected_currency_info = $CI->Homes->selected_currency_info();
		$select_home_adds 		= $CI->Homes->select_home_adds();
		$company_info  			= $CI->Companies->company_list();
		$promotion_product  	= $CI->Products_model->promotion_product();

		$data = array(
				'title' 		=> display('reset_password'),
				'category_list' => $parent_category_list,
				'top_category_list' => $top_category_list,
				'pro_category_list' => $pro_category_list,
				'slider_list' 	=> $slider_list,
				'block_list' 	=> $block_list,
				'best_sales' 	=> $best_sales,
				'footer_block' 	=> $footer_block,
				'languages' 	=> $languages,
				'currency_info' => $currency_info,
				'select_home_adds' => $select_home_adds,
				'selected_cur_id'=> (($selected_currency_info->currency_id)?$selected_currency_info->currency_id:""),
				'selected_currency_icon'  => $selected_currency_info->currency_icon,
				'selected_currency_name'  => $selected_currency_info->currency_name,
				'web_settings'  => $web_settings,
				'soft_settings' => $soft_settings,
				'promotion_product' => $promotion_product,
				'featured_cat_list' => $featured_cat_list,
				'logo' 			=> $web_settings[0]['logo'],
				'favicon' 		=> $web_settings[0]['favicon'],
				'footer_text'   => $web_settings[0]['footer_text'],
				'company_name'  => $company_info[0]['company_name'],
				'email'  		=> $company_info[0]['email'],
				'address'  		=> $company_info[0]['address'],
				'mobile'  		=> $company_info[0]['mobile'],
				'website'  		=> $company_info[0]['website'],
				'currency' 		=> $currency_details[0]['currency_icon'],
				'position' 		=> $currency_details[0]['currency_position'],
			);
		$HomeForm = $CI->parser->parse('website/reset_password',$data,true);
		return $HomeForm;
	}
	//Recover page load here
	public function seller_password()
	{
		$CI =& get_instance();
		$CI->load->model('website/Homes');
		$CI->load->model('web_settings');
		$CI->load->model('Soft_settings');
		$CI->load->model('Blocks');
		$CI->load->model('Companies');
		$CI->load->model('website/Categories');
		$CI->load->model('website/Products_model');

		$parent_category_list 	= $CI->Homes->parent_category_list();
		$top_category_list 		= $CI->Homes->top_category_list();
		$pro_category_list 		= $CI->Homes->category_list();
		$featured_cat_list 		= $CI->Homes->featured_cat_list();
		$best_sales 			= $CI->Homes->best_sales();
		$footer_block 			= $CI->Homes->footer_block();
		$slider_list 			= $CI->web_settings->slider_list();
		$block_list 			= $CI->Blocks->block_list(); 
		$currency_details 		= $CI->Soft_settings->retrieve_currency_info();
		$soft_settings 			= $CI->Soft_settings->retrieve_setting_editdata();
		$web_settings 			= $CI->web_settings->retrieve_setting_editdata();
		$languages 				= $CI->Homes->languages();
		$currency_info 			= $CI->Homes->currency_info();
		$selected_currency_info = $CI->Homes->selected_currency_info();
		$select_home_adds 		= $CI->Homes->select_home_adds();
		$company_info  			= $CI->Companies->company_list();
		$promotion_product  	= $CI->Products_model->promotion_product();

		$data = array(
				'title' 		=> display('reset_password'),
				'category_list' => $parent_category_list,
				'top_category_list' => $top_category_list,
				'pro_category_list' => $pro_category_list,
				'slider_list' 	=> $slider_list,
				'block_list' 	=> $block_list,
				'best_sales' 	=> $best_sales,
				'footer_block' 	=> $footer_block,
				'languages' 	=> $languages,
				'currency_info' => $currency_info,
				'select_home_adds' => $select_home_adds,
				'selected_cur_id'=> (($selected_currency_info->currency_id)?$selected_currency_info->currency_id:""),
				'selected_currency_icon'  => $selected_currency_info->currency_icon,
				'selected_currency_name'  => $selected_currency_info->currency_name,
				'web_settings'  => $web_settings,
				'soft_settings' => $soft_settings,
				'promotion_product' => $promotion_product,
				'featured_cat_list' => $featured_cat_list,
				'logo' 			=> $web_settings[0]['logo'],
				'favicon' 		=> $web_settings[0]['favicon'],
				'footer_text'   => $web_settings[0]['footer_text'],
				'company_name'  => $company_info[0]['company_name'],
				'email'  		=> $company_info[0]['email'],
				'address'  		=> $company_info[0]['address'],
				'mobile'  		=> $company_info[0]['mobile'],
				'website'  		=> $company_info[0]['website'],
				'currency' 		=> $currency_details[0]['currency_icon'],
				'position' 		=> $currency_details[0]['currency_position'],
			);
		$HomeForm = $CI->parser->parse('website/seller_recover',$data,true);
		return $HomeForm;
	}
	//Reset password
	public function seller_reset_password()
	{
		$CI =& get_instance();
		$CI->load->model('website/Homes');
		$CI->load->model('web_settings');
		$CI->load->model('Soft_settings');
		$CI->load->model('Blocks');
		$CI->load->model('Companies');
		$CI->load->model('website/Categories');
		$CI->load->model('website/Products_model');

		$parent_category_list 	= $CI->Homes->parent_category_list();
		$top_category_list 		= $CI->Homes->top_category_list();
		$pro_category_list 		= $CI->Homes->category_list();
		$featured_cat_list 		= $CI->Homes->featured_cat_list();
		$best_sales 			= $CI->Homes->best_sales();
		$footer_block 			= $CI->Homes->footer_block();
		$slider_list 			= $CI->web_settings->slider_list();
		$block_list 			= $CI->Blocks->block_list(); 
		$currency_details 		= $CI->Soft_settings->retrieve_currency_info();
		$soft_settings 			= $CI->Soft_settings->retrieve_setting_editdata();
		$web_settings 			= $CI->web_settings->retrieve_setting_editdata();
		$languages 				= $CI->Homes->languages();
		$currency_info 			= $CI->Homes->currency_info();
		$selected_currency_info = $CI->Homes->selected_currency_info();
		$select_home_adds 		= $CI->Homes->select_home_adds();
		$company_info  			= $CI->Companies->company_list();
		$promotion_product  	= $CI->Products_model->promotion_product();

		$data = array(
				'title' 		=> display('reset_password'),
				'category_list' => $parent_category_list,
				'top_category_list' => $top_category_list,
				'pro_category_list' => $pro_category_list,
				'slider_list' 	=> $slider_list,
				'block_list' 	=> $block_list,
				'best_sales' 	=> $best_sales,
				'footer_block' 	=> $footer_block,
				'languages' 	=> $languages,
				'currency_info' => $currency_info,
				'select_home_adds' => $select_home_adds,
				'selected_cur_id'=> (($selected_currency_info->currency_id)?$selected_currency_info->currency_id:""),
				'selected_currency_icon'  => $selected_currency_info->currency_icon,
				'selected_currency_name'  => $selected_currency_info->currency_name,
				'web_settings'  => $web_settings,
				'soft_settings' => $soft_settings,
				'promotion_product' => $promotion_product,
				'featured_cat_list' => $featured_cat_list,
				'logo' 			=> $web_settings[0]['logo'],
				'favicon' 		=> $web_settings[0]['favicon'],
				'footer_text'   => $web_settings[0]['footer_text'],
				'company_name'  => $company_info[0]['company_name'],
				'email'  		=> $company_info[0]['email'],
				'address'  		=> $company_info[0]['address'],
				'mobile'  		=> $company_info[0]['mobile'],
				'website'  		=> $company_info[0]['website'],
				'currency' 		=> $currency_details[0]['currency_icon'],
				'position' 		=> $currency_details[0]['currency_position'],
			);
		$HomeForm = $CI->parser->parse('website/seller_reset_password',$data,true);
		return $HomeForm;
	}
}
?>