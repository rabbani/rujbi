<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lcategory {

	//Category product
	public function category_product($cat_id=null,$price_range=null,$size=null,$brand=null,$sort=null,$rate=null,$seller_score=null)
	{
		$CI =& get_instance();
		$CI->load->model('website/Categories');
		$CI->load->model('website/Homes');
		$CI->load->model('web_settings');
		$CI->load->model('Soft_settings');
		$CI->load->model('Blocks');
		$CI->load->model('Companies');
		$CI->load->model('Variants');

		$max_value = 0;
		$min_value = 0;

		$category_product = $CI->Categories->category_product($cat_id,$price_range,$size,$brand,$sort,$rate,$seller_score);
		$top_category_list = $CI->Homes->top_category_list();
		$total_cat_pro 	  = $CI->Categories->select_total_sub_cat_pro($cat_id);
		$category 		  = $CI->Categories->select_single_category($cat_id);
		$categoryList 	  = $CI->Homes->parent_category_list();
		$pro_category_list= $CI->Homes->category_list();
		$best_sales 	  = $CI->Homes->best_sales();
		$footer_block 	  = $CI->Homes->footer_block();
		$block_list 	  = $CI->Blocks->block_list(); 
		$currency_details = $CI->Soft_settings->retrieve_currency_info();
		$soft_settings 	  = $CI->Soft_settings->retrieve_setting_editdata();
		$web_settings 	  = $CI->web_settings->retrieve_setting_editdata();
		$languages 		  = $CI->Homes->languages();
		$currency_info 	  = $CI->Homes->currency_info();
		$selected_currency_info = $CI->Homes->selected_currency_info();
		$selected_default_currency_info = $CI->Homes->selected_default_currency_info();
		$company_info  	  = $CI->Companies->company_list();
		$variant_list  	  = $CI->Variants->variant_list();

		$max_value 	 	  = $CI->Categories->select_max_value_of_cat_pro($cat_id,1);
		$min_value        = $CI->Categories->select_max_value_of_cat_pro($cat_id,0);

		//Max value and min value
		if ($max_value == $min_value) {
			$min_value = 0;
		}
		
		//Price range
		$from_price = 0;
		$to_price 	= 0;
		if (!(empty($price_range))) {
			$ex = explode("-", $price_range);
	        $from_price = $ex[0];
	        $to_price   = $ex[1];
		}

		$data = array(
				'title' 		=> display('category_wise_product'),
				'category_product' => $category_product,
				'top_category_list' => $top_category_list,
				'category_id' 	=> $cat_id,
				'category_name' => $category[0]['category_name'],
				'pro_category_list' => $pro_category_list,
				'category_list' => $categoryList,
				'block_list' 	=> $block_list,
				'best_sales' 	=> $best_sales,
				'footer_block' 	=> $footer_block,
				'languages' 	=> $languages,
				'currency_info' => $currency_info,
				'selected_cur_id' => (($selected_currency_info->currency_id)?$selected_currency_info->currency_id:""),
				'selected_currency_icon' => $selected_currency_info->currency_icon,
				'selected_currency_name' => $selected_currency_info->currency_name,
				'default_currency_icon'  => $selected_default_currency_info->currency_icon,
				'web_settings'  => $web_settings,
				'soft_settings' => $soft_settings,
				'logo' 			=> $web_settings[0]['logo'],
				'favicon' 		=> $web_settings[0]['favicon'],
				'footer_text'   => $web_settings[0]['footer_text'],
				'company_name'  => $company_info[0]['company_name'],
				'email'  		=> $company_info[0]['email'],
				'address'  		=> $company_info[0]['address'],
				'mobile'  		=> $company_info[0]['mobile'],
				'website'  		=> $company_info[0]['website'],
				'currency' 		=> $currency_details[0]['currency_icon'],
				'position' 		=> $currency_details[0]['currency_position'],
				'max_value' 	=> (!empty($max_value)?$max_value:0),
				'min_value' 	=> (!empty($min_value)?$min_value:0),
				'from_price' 	=> $from_price,
				'to_price' 		=> $to_price,
				'variant_list' 	=> $variant_list,
			);
		$CategoryForm = $CI->parser->parse('website/category',$data,true);
		return $CategoryForm;
	}
	//Category wise product
	public function category_wise_product($cat_id=null,$links=null,$per_page=null,$page=null)
	{
		$CI =& get_instance();
		$CI->load->model('website/Categories');
		$CI->load->model('website/Homes');
		$CI->load->model('web_settings');
		$CI->load->model('Soft_settings');
		$CI->load->model('Blocks');

		$category_wise_product 	= $CI->Categories->category_wise_product($cat_id,$per_page,$page);
		$category 		= $CI->Categories->select_single_category($cat_id);
		$categoryList 	= $CI->Homes->parent_category_list();
		$best_sales 	= $CI->Homes->best_sales();
		$footer_block 	= $CI->Homes->footer_block();
		$block_list 	= $CI->Blocks->block_list(); 
		$pro_category_list= $CI->Homes->category_list();
		$currency_details = $CI->Soft_settings->retrieve_currency_info();
		$Soft_settings 	= $CI->Soft_settings->retrieve_setting_editdata();
		$languages 		= $CI->Homes->languages();
		$currency_info 	= $CI->Homes->currency_info();
		$selected_currency_info = $CI->Homes->selected_currency_info();
		$max_value 		= $CI->Categories->select_max_value_of_pro($cat_id);
		$min_value 		= $CI->Categories->select_min_value_of_pro($cat_id);
		$select_category_product = $CI->Categories->select_category_product();

		$data = array(
				'title' 		=> display('category_wise_product'),
				'category_wise_product' => $category_wise_product,
				'category_name' => $category[0]['category_name'],
				'category_id' => $category[0]['category_id'],
				'category_list' => $categoryList,
				'block_list' 	=> $block_list,
				'best_sales' 	=> $best_sales,
				'footer_block' 	=> $footer_block,
				'pro_category_list' => $pro_category_list,
				'Soft_settings' => $Soft_settings,
				'languages' 	=> $languages,
				'currency_info' => $currency_info,
				'select_category_product' => $select_category_product,
				'selected_cur_id' => (($selected_currency_info->currency_id)?$selected_currency_info->currency_id:""),
				'max_value' 	=> (!empty($max_value->price)?$max_value->price:0),
				'min_value' 	=> (!empty($min_value->offer_price)?$min_value->offer_price:0),
				'category_name' => $category[0]['category_name'],
				'currency' 		=> $currency_details[0]['currency_icon'],
				'position' 		=> $currency_details[0]['currency_position'],
				'links' 		=> $links,
			);
		$HomeForm = $CI->parser->parse('website/category_product',$data,true);
		return $HomeForm;
	}
	
	//Retrieve  category List	
	public function category_list()
	{
		$CI =& get_instance();
		$CI->load->model('website/Categories');
		$category_list = $CI->Categories->category_list();  //It will get only Credit categorys
		$i=0;
		$total=0;
		if(!empty($category_list)){	
			foreach($category_list as $k=>$v){$i++;
			   $category_list[$k]['sl']=$i;
			}
		}
		$data = array(
				'title' 		=> 'Categories List',
				'category_list' => $category_list,
			);
		$categoryList = $CI->parser->parse('category/category',$data,true);
		return $categoryList;
	}

	//category Edit Data
	public function category_edit_data($category_id=null)
	{
		$CI =& get_instance();
		$CI->load->model('website/Categories');
		$category_detail = $CI->Categories->retrieve_category_editdata($category_id);
		$data=array(
			'category_id' 	=> $category_detail[0]['category_id'],
			'category_name' => $category_detail[0]['category_name'],
			'status' 		=> $category_detail[0]['status']
			);
		$chapterList = $CI->parser->parse('category/edit_category_form',$data,true);
		return $chapterList;
	}	
	//Category product search
	public function category_product_search($cat_id=null,$product_name=null,$price_range=null,$size=null,$sort=null,$rate=null,$seller_score=null,$brand_id=null)
	{
		$CI =& get_instance();
		$CI->load->model('website/Categories');
		$CI->load->model('website/Homes');
		$CI->load->model('web_settings');
		$CI->load->model('Soft_settings');
		$CI->load->model('Blocks');
		$CI->load->model('Companies');
		$CI->load->model('Variants');

		$category_product = $CI->Categories->retrieve_category_product($cat_id,$product_name,$price_range,$size,$sort,$rate,$seller_score,$brand_id);
		$top_category_list = $CI->Homes->top_category_list();
		$category 		  = $CI->Categories->select_single_category($cat_id);
		$categoryList 	  = $CI->Homes->parent_category_list();
		$pro_category_list= $CI->Homes->category_list();
		$best_sales 	  = $CI->Homes->best_sales();
		$footer_block 	  = $CI->Homes->footer_block();
		$block_list 	  = $CI->Blocks->block_list(); 
		$currency_details = $CI->Soft_settings->retrieve_currency_info();
		$soft_settings 	  = $CI->Soft_settings->retrieve_setting_editdata();
		$web_settings 	  = $CI->web_settings->retrieve_setting_editdata();
		$languages 		  = $CI->Homes->languages();
		$currency_info 	  = $CI->Homes->currency_info();
		$selected_currency_info = $CI->Homes->selected_currency_info();
		$company_info  	  = $CI->Companies->company_list();
		$variant_list  	  = $CI->Variants->variant_list();
		$selected_default_currency_info = $CI->Homes->selected_default_currency_info();
		$max_value 		  = $CI->Categories->select_max_value_of_search_pro($cat_id,$product_name);
		$min_value 		  = $CI->Categories->select_min_value_of_search_pro($cat_id,$product_name);

		$from_price = 0;
		$to_price 	= 0;
		if (!(empty($price_range))) {
			$ex = explode("-", $price_range);
	        $from_price = $ex[0];
	        $to_price   = $ex[1];
		}

		if ($cat_id == 'all') {
			$category_name =  "ALL";
		}else{
			$category_name = $category[0]['category_name'];
		}

		$data = array(
			'title' 		=> display('category_wise_product'),
			'category_product' => $category_product,
			'top_category_list' => $top_category_list,
			'category_id' 	=> $cat_id,
			'product_name' 	=> $product_name,
			'pro_category_list' => $pro_category_list,
			'category_list' => $categoryList,
			'block_list' 	=> $block_list,
			'best_sales' 	=> $best_sales,
			'footer_block' 	=> $footer_block,
			'languages' 	=> $languages,
			'currency_info' => $currency_info,
			'selected_cur_id' => (($selected_currency_info->currency_id)?$selected_currency_info->currency_id:""),
			'selected_currency_icon' => $selected_currency_info->currency_icon,
			'selected_currency_name' => $selected_currency_info->currency_name,
			'default_currency_icon'  => $selected_default_currency_info->currency_icon,
			'web_settings'  => $web_settings,
			'soft_settings' => $soft_settings,
			'logo' 			=> $web_settings[0]['logo'],
			'favicon' 		=> $web_settings[0]['favicon'],
			'footer_text'   => $web_settings[0]['footer_text'],
			'company_name'  => $company_info[0]['company_name'],
			'email'  		=> $company_info[0]['email'],
			'address'  		=> $company_info[0]['address'],
			'mobile'  		=> $company_info[0]['mobile'],
			'website'  		=> $company_info[0]['website'],
			'currency' 		=> $currency_details[0]['currency_icon'],
			'position' 		=> $currency_details[0]['currency_position'],
			'max_value' 	=> (!empty($max_value[0]['price'])?$max_value[0]['price']:0),
			'min_value' 	=> (!empty($min_value[0]['price'])?$min_value[0]['price']:0),
			'from_price' 	=> $from_price,
			'to_price' 		=> $to_price,
			'variant_list' 	=> $variant_list,
			'category_name' => $category_name,
		);

		$categoryList = $CI->parser->parse('website/category_product',$data,true);
		return $categoryList;
	}

	//Product search
	public function search_category_product($cat_id=null,$product_name=null,$price_range=null,$size=null,$sort=null,$rate=null,$seller_score=null,$brand_id=null)
	{
		$CI =& get_instance();
		$CI->load->model('website/Categories');
		$CI->load->model('website/Homes');
		$CI->load->model('web_settings');
		$CI->load->model('Soft_settings');
		$CI->load->model('Blocks');
		$CI->load->model('Companies');
		$CI->load->model('Variants');

		$category_product = $CI->Categories->retrieve_category_product($cat_id,$product_name,$price_range,$size,$sort,$rate,$seller_score,$brand_id);
		$top_category_list = $CI->Homes->top_category_list();
		$category 		  = $CI->Categories->select_single_category($cat_id);
		$categoryList 	  = $CI->Homes->parent_category_list();
		$pro_category_list= $CI->Homes->category_list();
		$best_sales 	  = $CI->Homes->best_sales();
		$footer_block 	  = $CI->Homes->footer_block();
		$block_list 	  = $CI->Blocks->block_list(); 
		$currency_details = $CI->Soft_settings->retrieve_currency_info();
		$soft_settings 	  = $CI->Soft_settings->retrieve_setting_editdata();
		$web_settings 	  = $CI->web_settings->retrieve_setting_editdata();
		$languages 		  = $CI->Homes->languages();
		$currency_info 	  = $CI->Homes->currency_info();
		$selected_currency_info = $CI->Homes->selected_currency_info();
		$company_info  	  = $CI->Companies->company_list();
		$variant_list  	  = $CI->Variants->variant_list();
		$selected_default_currency_info = $CI->Homes->selected_default_currency_info();
		$max_value 		  = $CI->Categories->select_max_value_of_search_pro($cat_id,$product_name);
		$min_value 		  = $CI->Categories->select_min_value_of_search_pro($cat_id,$product_name);

		$from_price = 0;
		$to_price 	= 0;
		if (!(empty($price_range))) {
			$ex = explode("-", $price_range);
	        $from_price = $ex[0];
	        $to_price   = $ex[1];
		}

		if ($cat_id == 'all') {
			$category_name =  "ALL";
		}else{
			$category_name = $category[0]['category_name'];
		}

		$data = array(
			'title' 		=> display('category_wise_product'),
			'category_product' => $category_product,
			'top_category_list' => $top_category_list,
			'category_id' 	=> $cat_id,
			'product_name' 	=> $product_name,
			'pro_category_list' => $pro_category_list,
			'category_list' => $categoryList,
			'block_list' 	=> $block_list,
			'best_sales' 	=> $best_sales,
			'footer_block' 	=> $footer_block,
			'languages' 	=> $languages,
			'currency_info' => $currency_info,
			'selected_cur_id' => (($selected_currency_info->currency_id)?$selected_currency_info->currency_id:""),
			'selected_currency_icon' => $selected_currency_info->currency_icon,
			'selected_currency_name' => $selected_currency_info->currency_name,
			'default_currency_icon'  => $selected_default_currency_info->currency_icon,
			'web_settings'  => $web_settings,
			'soft_settings' => $soft_settings,
			'logo' 			=> $web_settings[0]['logo'],
			'favicon' 		=> $web_settings[0]['favicon'],
			'footer_text'   => $web_settings[0]['footer_text'],
			'company_name'  => $company_info[0]['company_name'],
			'email'  		=> $company_info[0]['email'],
			'address'  		=> $company_info[0]['address'],
			'mobile'  		=> $company_info[0]['mobile'],
			'website'  		=> $company_info[0]['website'],
			'currency' 		=> $currency_details[0]['currency_icon'],
			'position' 		=> $currency_details[0]['currency_position'],
			'max_value' 	=> (!empty($max_value[0]['price'])?$max_value[0]['price']:0),
			'min_value' 	=> (!empty($min_value[0]['price'])?$min_value[0]['price']:0),
			'from_price' 	=> $from_price,
			'to_price' 		=> $to_price,
			'variant_list' 	=> $variant_list,
			'category_name' => $category_name,
		);

		$categoryList = $CI->parser->parse('website/category_product',$data,true);
		return $categoryList;
	}
}
?>