<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lcomission {
	//Add comission
	public function comission_add_form()
	{
		$CI =& get_instance();
		$CI->load->model('Comissions');
		$CI->load->model('Categories');
		$category_list = $CI->Categories->category_list();

		$data = array(
				'title' 		=> display('add_comission'),
				'category_list' => $category_list,
			);
		$comissionForm = $CI->parser->parse('comission/add_comission',$data,true);
		return $comissionForm;
	}
	//Retrieve  comission List	
	public function comission_list()
	{
		$CI =& get_instance();
		$CI->load->model('Comissions');
		$comission_list = $CI->Comissions->comission_list(); 

		$i=0;
		if(!empty($comission_list)){	
			foreach($comission_list as $k=>$v){$i++;
			   $comission_list[$k]['sl']=$i;
			}
		}

		$data = array(
				'title' => display('manage_comission'),
				'comission_list' => $comission_list,
			);
		$comissionList = $CI->parser->parse('comission/comission',$data,true);
		return $comissionList;
	}
	//comission Edit Data
	public function comission_edit_data($comission_id)
	{
		$CI =& get_instance();
		$CI->load->model('Comissions');
		$CI->load->model('Categories');
		$comission_details = $CI->Comissions->retrieve_comission_editdata($comission_id);
		$category_list 	   = $CI->Categories->category_list();
	
		$data=array(
			'title' 		=> display('comission_edit'),
			'id' 			=> $comission_details[0]['id'],
			'category_id' 	=> $comission_details[0]['category_id'],
			'rate'			=> $comission_details[0]['rate'],
			'status'		=> $comission_details[0]['status'],
			'category_list' => $category_list
			);
		$chapterList = $CI->parser->parse('comission/edit_comission',$data,true);
		return $chapterList;
	}
	//Comission report
	public function comission_report($links=null,$per_page=null,$page=null,$seller=null,$product_id=null,$category_id=null){
		$CI =& get_instance();
		$CI->load->model('Comissions');
		$CI->load->model('Sellers');
		$CI->load->model('Products');
		$CI->load->model('Categories');
		$CI->load->model('Soft_settings');
		$comission_report = $CI->Comissions->comission_report($per_page,$page,$seller,$product_id,$category_id);

		$seller_list    = $CI->Sellers->seller_list();
		$manage_product = $CI->Products->manage_product();
		$category_list  = $CI->Categories->category_list();

		$i=0;
		$total_price = 0;
		$total_discount = 0;
		$total_amount = 0;
		if(!empty($comission_report)){	
			foreach($comission_report as $k=>$v){
				$i++;
			   	$comission_report[$k]['sl']=$i;
			   	$comission_report[$k]['total_income'] = ($comission_report[$k]['total_amount']* $comission_report[$k]['seller_percentage']) / 100;

			   	$total_price    = $total_price+$comission_report[$k]['total_price'];
			   	$total_discount = $total_discount+($comission_report[$k]['quantity'] * $comission_report[$k]['discount']);
			   	$total_amount 	= $total_amount+$comission_report[$k]['total_income'];
			}
		}

		$currency_details = $CI->Soft_settings->retrieve_currency_info();

		$data = array(
				'title' 	   => display('comission_report'),
				'comission_report' => $comission_report,
				'total_amount' => number_format($total_amount, 2, '.', ','),
				'total_discount'=> number_format($total_discount, 2, '.', ','),
				'total_price'  => number_format($total_price, 2, '.', ','),
				'seller_list'  => $seller_list,
				'manage_product'=> $manage_product,
				'category_list'=> $category_list,
				'links'		   => $links,
				'currency' 	   =>  $currency_details[0]['currency_icon'],
				'position' 	   =>  $currency_details[0]['currency_position'],
			);
		$comissionList = $CI->parser->parse('comission/comission_report',$data,true);
		return $comissionList;
	}
}
?>