<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Products extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
	}

	//Product List
	public function manage_product($per_page=null,$page=null,$model=null,$title=null,$category=null){

		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$seller_id = $this->session->userdata('seller_id');
		$this->db->select('
				a.*,
				b.category_name,
				c.first_name,
				c.last_name,
				c.seller_id,
				d.brand_name,
				a.upload_id,
				e.title
			');
		$this->db->from('upload_product a');
		$this->db->join('product_category b','a.category_id = b.category_id','left');
		$this->db->join('seller_information c','a.seller_id = c.seller_id','left');
		$this->db->join('brand d','a.brand_id = d.brand_id','left');
		$this->db->join('upload_product_title e','a.upload_id = e.upload_id','left');
		$this->db->where('e.lang_id',$lang_id);
		$this->db->where('a.seller_id',$seller_id);
//

//        $this->db->select('a.*,b.*,c.*,d.first_name,d.last_name,e.brand_name');
//        $this->db->from('product_information a');
//        $this->db->join('product_category b','a.category_id = b.category_id','left');
//        $this->db->join('product_title c','a.product_id = c.product_id','left');
//        $this->db->join('seller_information d','a.seller_id = d.seller_id','left');
//        $this->db->join('brand e','a.brand_id = e.brand_id','left');
//        $this->db->where('c.lang_id',$lang_id);
//        $this->db->where('a.seller_id',$seller_id);


//dd($this->db->last_query());
		if ($model) {
			$this->db->like('a.product_model', $model, 'both');
		}

		if ($title) {
			$this->db->like('e.title', $title, 'both'); 
		}

		if ($category) {
			$this->db->where('a.category_id',$category);
		}

		$this->db->order_by('a.upload_id','desc');
		$this->db->limit($per_page,$page);
		$query = $this->db->get();
		return $query->result_array();
	}	
	//Product Count
	public function product_count($model=null,$title=null,$category=null){

		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$seller_id = $this->session->userdata('seller_id');
		$this->db->select('
				a.*,
				b.category_name,
				c.first_name,
				c.last_name,
				c.seller_id,
				d.brand_name,
				a.upload_id,
				e.title
			');
		$this->db->from('upload_product a');
		$this->db->join('product_category b','a.category_id = b.category_id','left');
		$this->db->join('seller_information c','a.seller_id = c.seller_id','left');
		$this->db->join('brand d','a.brand_id = d.brand_id','left');
		$this->db->join('upload_product_title e','a.upload_id = e.upload_id','left');
		$this->db->where('e.lang_id',$lang_id);
		$this->db->where('a.seller_id',$seller_id);

		if ($model) {
			$this->db->like('a.product_model', $model, 'both');
		}

		if ($title) {
			$this->db->like('e.title', $title, 'both'); 
		}

		if ($category) {
			$this->db->where('a.category_id',$category);
		}
		
		$this->db->order_by('a.upload_id','desc');
		$query = $this->db->get();
		return $query->num_rows();
	}
	//Total Seller Product
	public function total_seller_product($seller_id)
	{
		$this->db->select('a.*,b.seller_name');
		$this->db->from('product a');
		$this->db->join('seller_information b','b.seller_id = a.seller_id');
		$this->db->where('a.seller_id',$seller_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->num_rows();	
		}
		return false;
	}
	//Retrieve product html data
	public function retrieve_product_html_data($product_id)
	{
		$this->db->select('
			a.*,
			b.*,
			c.*,
			d.product_id,
			d.product_name,
			d.product_details,
			d.product_model,d.unit,
			e.unit_short_name,
			f.variant_name,
			a.product_details
			');
		$this->db->from('product a');
		$this->db->join('seller_information b','b.seller_id = a.seller_id');
		$this->db->join('product_details c','c.product_id = a.product_id');
		$this->db->join('product_information d','d.product_id = c.product_id');
		$this->db->join('unit e','e.unit_id = d.unit','left');
		$this->db->join('variant f','f.variant_id = c.variant_id','left');
		$this->db->where('a.product_id',$product_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();	
		}
		return false;
	}
	//Retrive seller product edit data
	public function retrive_seller_product_edit_data($upload_id){
		$seller_id = $this->session->userdata('seller_id');
		$this->db->select('
				a.*,
				b.category_name,
				c.first_name,
				c.last_name,
				c.id as seller_uni_id,
				c.seller_id,
				d.brand_name,
				d.brand_id
			');
		$this->db->from('upload_product a');
		$this->db->join('product_category b','a.category_id = b.category_id','left');
		$this->db->join('seller_information c','a.seller_id = c.seller_id','left');
		$this->db->join('brand d','a.brand_id = d.brand_id','left');
		$this->db->where('upload_id',$upload_id);
		$this->db->where('a.seller_id',$seller_id);
		$query = $this->db->get();
		return $query->row();
	}
	//Retrieve seller image edit data
	public function retrive_seller_image_edit_data($upload_id)
	{
		$this->db->select('*');
		$this->db->from('upload_image');
		$this->db->where('upload_id',$upload_id);
		$this->db->where('image_type',1);
		$query = $this->db->get();
		return $query->result_array();
	}
	//Retrieve title edit data
	public function retrive_seller_title_edit_data($upload_id)
	{
		$this->db->select('*');
		$this->db->from('upload_product_title');
		$this->db->where('upload_id',$upload_id);
		$query = $this->db->get();
		return $query->result_array();
	}
	//Retrieve upload title
	public function retrive_upload_title($upload_id=null,$lang_id=null)
	{
		$this->db->select('*');
		$this->db->from('upload_product_title');
		$this->db->where('upload_id',$upload_id);
		$this->db->where('lang_id',$lang_id);
		$query = $this->db->get();
		return $query->row();
	}
	//Retrieve upload description
	public function retrive_upload_description($upload_id=null,$lang_id=null)
	{
		$this->db->select('*');
		$this->db->from('upload_product_description');
		$this->db->where('upload_id',$upload_id);
		$this->db->where('lang_id',$lang_id);
		$this->db->where('description_type',1);
		$query = $this->db->get();
		return $query->row();
	}
	//Retrieve upload specification
	public function retrive_upload_specification($upload_id=null,$lang_id=null)
	{
		$this->db->select('*');
		$this->db->from('upload_product_description');
		$this->db->where('upload_id',$upload_id);
		$this->db->where('lang_id',$lang_id);
		$this->db->where('description_type',2);
		$query = $this->db->get();
		return $query->row();
	}
	//Retrieve description edit data
	public function retrive_seller_description_edit_data($upload_id)
	{
		$this->db->select('*');
		$this->db->from('upload_product_description');
		$this->db->where('upload_id',$upload_id);
		$this->db->where('description_type',1);
		$query = $this->db->get();
		return $query->result_array();
	}
	//Retrieve specification edit data
	public function retrive_seller_specification_edit_data($upload_id)
	{
		$this->db->select('*');
		$this->db->from('upload_product_description');
		$this->db->where('upload_id',$upload_id);
		$this->db->where('description_type',2);
		$query = $this->db->get();
		return $query->result_array();
	}

	#===============================================#
	#=================Upload Product================#
	#===============================================#
	//Product Image
	public function insert_product_image($data)
	{
		if ($data['status'] == 1) {
			$result= $this->db->select('*')
					->from('upload_image')
					->where('status',1)
					->where('upload_id',$data['upload_id'])
					->where('image_type',1)
					->get()
					->num_rows();
			if ($result > 0) {
				return FALSE;
			}else{
				$this->db->insert('upload_image',$data);
				return TRUE;
			}
		}else{
			$this->db->insert('upload_image',$data);
			return TRUE;
		}
	}
	//Retrieve seller image edit data
	public function retrive_product_image_edit_data($upload_id=null)
	{
		$this->db->select('*');
		$this->db->from('upload_image');
		$this->db->where('upload_id',$upload_id);
		$this->db->where('image_type',1);
		$query = $this->db->get();
		return $query->result_array();
	}
	//Primary image existing check
	public function primary_image_exist_check($upload_id=null){
		$result= $this->db->select('*')
					->from('upload_image')
					->where('status',1)
					->where('upload_id',$upload_id)
					->where('image_type',1)
					->get()
					->num_rows();
		if ($result > 0) {
			return 0;
		}else{
			return 1;
		}
	}
	//Make image primary
	public function make_img_primary($image_id=null,$upload_id=null,$image_path=null){
		$result = $this->db->select('*')
				->from('upload_image')
				->where('upload_id',$upload_id)
				->where('status',1)
				->where('image_type',1)
				->get()
				->row();
		if ($result) {
			$this->db->set('status',0)
					->where('upload_image_id',$result->upload_image_id)
					->update('upload_image');

			$this->db->set('status',1)
					->where('upload_image_id',$image_id)
					->update('upload_image');
		}else{
			$this->db->set('status',1)
					->where('upload_image_id',$image_id)
					->update('upload_image');
		}
		return true;
	}
	//Seller Product Entry
	public function insert_product_title($data)
	{
		$result = $this->db->select('*')
					->from('upload_product_title')
					->where('upload_id',$data['upload_id'])
					->where('lang_id',$data['lang_id'])
					->get()
					->num_rows();

		if ($result > 0) {
			return false;
		}else{
			$this->db->insert('upload_product_title',$data);
			return TRUE;
		}
	}
    //Product Product Description
	public function upload_product_description($data=null)
	{
		$result = $this->db->select('*')
						->from('upload_product_description')
						->where('upload_id',$data['upload_id'])
						->where('lang_id',$data['lang_id'])
						->where('description_type',$data['description_type'])
						->get()
						->num_rows();

		if ($result > 0) {
			return false;
		}else{
			$this->db->insert('upload_product_description',$data);
			return TRUE;
		}
	}
	//Retrive comission value
	public function retrive_com_value($category_id=null){
		$result = $this->db->select('*')
			->from('comission')
			->where('category_id',$category_id)
			->get()
			->row();

		return $result;
	}
	//Get product stock
	public function get_product_stock($product_id=null){

		$result = $this->db->select('quantity')
				->from('product_information')
				->where('product_id',$product_id)
				->where('status',2)
				->get()
				->row();
		if ($result) {
			return $result->quantity;
		}else{
			return 0;
		}
		
	}
}