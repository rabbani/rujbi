<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Settings extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
	}

	public function update_seller_setting($data=null)
	{
		$seller_id = $this->session->userdata('seller_id');
		$this->db->where('seller_id',$seller_id)
				->update('seller_information',$data);
		return true;
	}

	public function seller_guarantee()
	{
		$seller_id = $this->session->userdata('seller_id');
		return $result = $this->db->select('seller_guarantee')
				->from('seller_information')
				->where('seller_id',$seller_id)
				->get()
				->row();
	}
}