<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Orders extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}

	//Order List
	public function order_list($per_page=null,$page=null,$order_no=null,$pre_order_no=null,$customer=null,$date=null,$order_status=null)
	{
		$seller_id = $this->session->userdata('seller_id');

		$this->db->select('a.*,d.customer_name');
		$this->db->from('order a');
		$this->db->join('seller_order b','b.order_id = a.order_id');
		$this->db->join('seller_information c','c.seller_id = b.seller_id');
		$this->db->join('customer_information d','d.customer_id = a.customer_id');
		$this->db->where('c.seller_id',$seller_id);

		if ($order_no) {
			$this->db->where('a.id',$order_no);
		}

		if ($pre_order_no) {
			$this->db->where('a.pre_order_id',$pre_order_no);
		}

		if ($customer) {
			$this->db->where('a.customer_id',$customer);
		}

		if ($order_status) {
			$this->db->where('a.order_status',$order_status);
		}

		if ($date) {
			$a = explode("---", $date);
			if (count($a) == 1) {
				$from_date =  $a[0];
				$this->db->where('a.date', $from_date);
			}else{
				$from_date = $a[0];
				$this->db->where('a.date >=', $from_date);
				$to_date   = $a[1];
				$this->db->where('a.date <=', $to_date);
			}
			
		}

		$this->db->order_by('a.id','desc');
		$this->db->group_by('a.id');
		$this->db->limit($per_page,$page);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();	
		}
		return false;
	}	
	//Order Count
	public function order_count($order_no=null,$pre_order_no=null,$customer=null,$date=null,$order_status=null)
	{
		$seller_id = $this->session->userdata('seller_id');

		$this->db->select('a.*,d.customer_name');
		$this->db->from('order a');
		$this->db->join('seller_order b','b.order_id = a.order_id');
		$this->db->join('seller_information c','c.seller_id = b.seller_id');
		$this->db->join('customer_information d','d.customer_id = a.customer_id');
		$this->db->where('c.seller_id',$seller_id);

		if ($order_no) {
			$this->db->where('a.id',$order_no);
		}

		if ($pre_order_no) {
			$this->db->where('a.pre_order_id',$pre_order_no);
		}

		if ($customer) {
			$this->db->where('a.customer_id',$customer);
		}

		if ($order_status) {
			$this->db->where('a.order_status',$order_status);
		}

		if ($date) {
			$a = explode("---", $date);
			if (count($a) == 1) {
				$from_date =  $a[0];
				$this->db->where('a.date', $from_date);
			}else{
				$from_date = $a[0];
				$this->db->where('a.date >=', $from_date);
				$to_date   = $a[1];
				$this->db->where('a.date <=', $to_date);
			}
			
		}

		$this->db->order_by('a.id','desc');
		$this->db->group_by('a.id');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->num_rows();	
		}
		return false;
	}
	//Retrieve order_html_data
	public function retrieve_order_html_data($order_id)
	{
		$lang = $this->db->select('language')
				->from('soft_setting')
				->where('setting_id',1)
				->get()
				->row();

		if ($lang) {
			$language = $lang->language;
		}else{
			$language = 'english';
		}
		$seller_id = $this->session->userdata('seller_id');

		$this->db->select('
			a.*,
			b.customer_short_address,
			b.customer_name,
			b.customer_mobile,
			b.customer_email,
			c.*,
			d.product_id,
			d.product_model,d.unit,
			e.unit_short_name,
			f.variant_name,
			g.title as product_name
			');
		$this->db->from('order a');
		$this->db->join('customer_information b','b.customer_id = a.customer_id');
		$this->db->join('seller_order c','c.order_id = a.order_id');
		$this->db->join('product_information d','d.product_id = c.product_id');
		$this->db->join('unit e','e.unit_id = d.unit','left');
		$this->db->join('variant f','f.variant_id = c.variant_id','left');
		$this->db->join('product_title g','g.product_id = c.product_id','left');
		$this->db->where('c.seller_id',$seller_id);
		$this->db->where('a.order_id',$order_id);
		$this->db->where('g.lang_id',$language);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();	
		}
		return false;
	}
	//Retrieve company Edit Data
	public function retrieve_company()
	{
		$this->db->select('*');
		$this->db->from('company_information');
		$this->db->limit('1');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();	
		}
		return false;
	}
} 
