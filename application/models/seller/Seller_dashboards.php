<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Seller_dashboards extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}

	//Retrive profile data
	public function profile_edit_data()
	{
		$seller_id = $this->session->userdata('seller_id');
		$this->db->select('*');
		$this->db->from('seller_information');
		$this->db->where('seller_id',$seller_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();	
		}
		return false;
	}

	//Update seller profile
	public function profile_update()
	{
		$this->load->library('upload');
	    if (($_FILES['image']['name'])) {
            $files = $_FILES;
            $config=array();
            $config['upload_path'] 	 = 'assets/dist/img/profile_picture/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|JPEG|GIF|JPG|PNG';
            $config['max_size']      = '1024';
            $config['max_width']     = '*';
            $config['max_height']    = '*';
            $config['overwrite']     = FALSE;
            $config['encrypt_name']  = true; 

            $this->upload->initialize($config);
            if (!$this->upload->do_upload('image')) {
                $sdata['error_message'] = $this->upload->display_errors();
                $this->session->set_userdata($sdata);
                redirect('seller/seller_dashboard/edit_profile');
            } else {
                $view =$this->upload->data();
                $image=base_url($config['upload_path'].$view['file_name']);
            }
        }

       	$old_image 	 = $this->input->post('old_image');
       	$seller_id   = $this->session->userdata('seller_id');

		$data = array(
			'first_name' 		=> $this->input->post('first_name'),
			'last_name'  		=> $this->input->post('last_name'),
			'email'  			=> $this->input->post('email'),
			'address' 			=> $this->input->post('address'),
			'seller_store_name'	=> $this->input->post('store_name'),
			'business_name'		=> $this->input->post('business_name'),
			'identification_doc_no' => $this->input->post('identification_doc_no'),
			'identification_type' => $this->input->post('identification_type'),
			'affiliate_id'  	=> $this->input->post('affiliate_id'),
			'image'  		    => (!empty($image)?$image:$old_image),
		);

		$this->db->where('seller_id',$seller_id);
		$this->db->update('seller_information',$data);

		return true;
	}

	//Change password
	public function change_password($email,$old_password,$new_password)
	{
		$user_name 	= md5("gef".$new_password);
		$password 	= md5("gef".$old_password);
        $this->db->where(array('email'=>$email,'password'=>$password));
		$query 		= $this->db->get('seller_information');
		$result 	=  $query->result_array();
		
		if (count($result) == 1)
		{	
			$this->db->set('password',$user_name);
			$this->db->where('password',$password);
			$this->db->where('email',$email);
			$this->db->update('seller_information');

			return true;
		}
		return false;
	}
	//Total Balance
	public function total_balance($seller_id=null){
		
		$this->db->select('
						SUM((quantity * rate)-(quantity * discount_per_product)) as total_price
						');
		$this->db->from('order a');
		$this->db->join('seller_order b','a.order_id = b.order_id');
		$this->db->join('seller_information c','b.seller_id = c.seller_id');
		$this->db->where('a.delivered !=',null);
		$this->db->where('c.seller_id',$seller_id);
		$this->db->order_by('a.id','desc');
		$query = $this->db->get();
		return $query->row()->total_price;
	}
	//Total Comission
	public function total_comission($seller_id=null){

		$this->db->select('
						SUM((((quantity * rate)-(quantity * discount_per_product)) * seller_percentage)/100) as total_comission
						');
		$this->db->from('order a');
		$this->db->join('seller_order b','a.order_id = b.order_id');
		$this->db->join('seller_information c','b.seller_id = c.seller_id');
		$this->db->where('a.delivered !=',null);
		$this->db->where('c.seller_id',$seller_id);
		$this->db->order_by('a.id','desc');
		$query = $this->db->get();
		return $query->row()->total_comission;
	}
	//Total paid amount of seller
    public function total_paid_amount($seller_id=null){

        $this->db->select('
                        SUM(amount) as total_paid_amount
                        ');
        $this->db->from('payment a');
        $this->db->where('seller_id',$seller_id);
        $query = $this->db->get();
        return $query->row()->total_paid_amount;
    }

	//Monthly order status
	public function monthly_product_deliver($day=null,$status=null){

		$seller_id = $this->session->userdata('seller_id');

		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$result = $this->db->query("
							SELECT count(a.product_info_id) as total_product FROM product_information a
							JOIN product_title b 
								ON a.product_id = b.product_id 
							WHERE MONTH(approved_date) = MONTH(CURRENT_TIMESTAMP) 
								AND YEAR(approved_date) = YEAR(CURRENT_TIMESTAMP)
								AND DAY(approved_date) = $day
								AND a.seller_id = '$seller_id'
								AND b.lang_id = '$lang_id'
								AND a.status = $status;
							");

		return $result->row();
	}
	//Yearly order status
	public function yearly_product_status($month=null,$status=null){

		$seller_id = $this->session->userdata('seller_id');

		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$result = $this->db->query("
							SELECT count(a.product_info_id) as total_product FROM product_information a
							JOIN product_title b 
								ON a.product_id = b.product_id
							WHERE MONTH(approved_date)  = $month
								AND YEAR(approved_date) = YEAR(CURRENT_TIMESTAMP)
								AND seller_id  = '$seller_id'
								AND b.lang_id  = '$lang_id'
								AND a.status     = $status;
							");

		return $result->row();
	}
	//Comission report
	public function comission_report($page=null,$per_page=null,$order_no=null,$date=null){
		$seller_id = $this->session->userdata('seller_id');

		$this->db->select('
						a.*,
						c.first_name,
						c.last_name,
						c.mobile,
						SUM((quantity * rate)-(quantity * discount_per_product)) as total_price,
						SUM((((quantity * rate)-(quantity * discount_per_product)) * seller_percentage)/100) as total_comission,
						');
		$this->db->from('order a');
		$this->db->join('seller_order b','a.order_id = b.order_id');
		$this->db->join('seller_information c','b.seller_id = c.seller_id');

		if ($order_no) {
			$this->db->where('a.id',$order_no);
		}

		if ($date) {
			$a = explode("---", $date);
			if (count($a) == 1) {
				$from_date =  $a[0];
				$this->db->where('a.date', $from_date);
			}else{
				$from_date = $a[0];
				$this->db->where('a.date >=', $from_date);
				$to_date   = $a[1];
				$this->db->where('a.date <=', $to_date);
			}
			
		}

		$this->db->where('b.seller_id',$seller_id);
		$this->db->where('a.delivered !=',null);
		$this->db->order_by('a.id','desc');
		$this->db->group_by('a.id');
		$this->db->limit($per_page,$page);
		$query = $this->db->get();
		return $query->result_array();
	}
	//Comission report count
	public function comission_report_count($order_no=null,$date=null){
		$seller_id = $this->session->userdata('seller_id');

		$this->db->select('
						a.*,
						c.first_name,
						c.last_name,
						c.mobile,
						SUM((quantity * rate)-(quantity * discount_per_product)) as total_price,
						SUM((((quantity * rate)-(quantity * discount_per_product)) * seller_percentage)/100) as total_comission,
						');
		$this->db->from('order a');
		$this->db->join('seller_order b','a.order_id = b.order_id');
		$this->db->join('seller_information c','b.seller_id = c.seller_id');

		if ($order_no) {
			$this->db->where('a.id',$order_no);
		}

		if ($date) {
			$a = explode("---", $date);
			if (count($a) == 1) {
				$from_date =  $a[0];
				$this->db->where('a.date', $from_date);
			}else{
				$from_date = $a[0];
				$this->db->where('a.date >=', $from_date);
				$to_date   = $a[1];
				$this->db->where('a.date <=', $to_date);
			}
			
		}

		$this->db->where('b.seller_id',$seller_id);
		$this->db->where('a.delivered !=',null);
		$this->db->order_by('a.id','desc');
		$this->db->group_by('a.id');
		$query = $this->db->get();
		return $query->num_rows();
	}
	//Seller Name
	public function product_name($product_id){

		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$seller_info = $this->db->select('*')
				->from('product_title')
				->where('product_id',$product_id)
				->where('lang_id',$lang_id)
				->get()
				->row();

		return $seller_info->title;
	}
}