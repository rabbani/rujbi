<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Reports extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}

	public function lifetime_orders(){
		$lifetime_orders = $this->db->select('*')
								->from('order')
								->get()
								->num_rows();

		return $lifetime_orders;
	}	

	public function lifetime_orders_sell(){
		$lifetime_orders = $this->db->select('sum(total_amount) as total_amount')
								->from('order')
								->get()
								->row();

		return $lifetime_orders;
	}	

	public function order_delivered(){
		$order_delivered = $this->db->select('*')
								->from('order')
								->where('order_status',4)
								->get()
								->num_rows();

		return $order_delivered;
	}		

	public function order_delivered_sell(){
		$order_delivered = $this->db->select('sum(total_amount) as total_amount')
								->from('order')
								->where('order_status',4)
								->get()
								->row();

		return $order_delivered;
	}	

	public function cancelled_order(){
		$cancelled_order = $this->db->select('*')
								->from('order')
								->where('order_status',6)
								->get()
								->num_rows();

		return $cancelled_order;
	}		

	public function returned_order(){
		$returned_order = $this->db->select('*')
								->from('order')
								->where('order_status',5)
								->get()
								->num_rows();

		return $returned_order;
	}	

	public function total_product(){
		$total_product = $this->db->select('*')
								->from('product_information')
								->get()
								->num_rows();

		return $total_product;
	}	

	public function total_seller(){
		$total_product = $this->db->select('*')
								->from('seller_information')
								->get()
								->num_rows();

		return $total_product;
	}

	public function order_report($day=null){

		$re   = $this->input->get('re');
		$date = $this->input->get('date');

		if (($re == 'order') && ($date !=null)) {
			$date = explode('-',$date);
			$month= $date[0];
			$year = $date[1];

			$result = $this->db->query("
				SELECT 
					SUM(total_amount) as total_amount
				FROM
					`order`
			    WHERE 
			    	Year(pending) = '$year'
			    AND 
			    	Month(pending)= '$month'
			    AND
			    	DAY(pending)  = '$day'
			    ");

			return $result->row();
		}else{
			$result = $this->db->query("
				SELECT 
					SUM(total_amount) as total_amount
				FROM
					`order`
			    WHERE 
			    	Year(pending) = YEAR(CURRENT_TIMESTAMP)
			    AND 
			    	Month(pending)= MONTH(CURRENT_TIMESTAMP) 
			    AND
			    	DAY(pending)  = '$day'
			    ");

			return $result->row();
		}
	}

	public function sell_report($day=null){

		$re   = $this->input->get('re');
		$date = $this->input->get('date');

		if (($re == 'sell') && ($date !=null)) {
			$date = explode('-',$date);
			$month= $date[0];
			$year = $date[1];

			$result = $this->db->query("
				SELECT 
					SUM(total_amount) as total_amount
				FROM
					`order`
			    WHERE 
			    	Year(shipping) = '$year'
			    AND 
			    	Month(shipping)= '$month'
			    AND
			    	DAY(shipping)  = '$day'
			    ");

			return $result->row();
		}else{
			$result = $this->db->query("
				SELECT 
					SUM(total_amount) as total_amount
				FROM
					`order`
			    WHERE 
			    	Year(shipping) = YEAR(CURRENT_TIMESTAMP)
			    AND 
			    	Month(shipping)= MONTH(CURRENT_TIMESTAMP) 
			    AND
			    	DAY(shipping)  = '$day'
			    ");

			return $result->row();
		}
	}

	public function net_sell_report($day=null){

		$re   = $this->input->get('re');
		$date = $this->input->get('date');

		if (($re == 'net_sell') && ($date !=null)) {
			$date = explode('-',$date);
			$month= $date[0];
			$year = $date[1];

			$result = $this->db->query("
				SELECT 
					SUM(total_amount) as total_amount
				FROM
					`order`
			    WHERE 
			    	Year(delivered) = '$year'
			    AND 
			    	Month(delivered)= '$month'
			    AND
			    	DAY(delivered)  = '$day'
			    ");

			return $result->row();
		}else{
			$result = $this->db->query("
				SELECT 
					SUM(total_amount) as total_amount
				FROM
					`order`
			    WHERE 
			    	Year(delivered) = YEAR(CURRENT_TIMESTAMP)
			    AND 
			    	Month(delivered)= MONTH(CURRENT_TIMESTAMP) 
			    AND
			    	DAY(delivered)  = '$day'
			    ");

			return $result->row();
		}
	}

	public function get_total_delivered_order($shipping_id=null)
	{
		return $total_order = $this->db->select('*')
							->from('order')
							->where('shipping_id',$shipping_id)
							->where('delivered !=',null)
							->get()
							->num_rows();
	}

	public function get_total_shipping_order($shipping_id=null)
	{
		return $total_order = $this->db->select('*')
							->from('order')
							->where('shipping_id',$shipping_id)
							->where('shipping !=',null)
							->get()
							->num_rows();
	}

	public function get_total_delivered_money($shipping_id=null)
	{
		return $total_order = $this->db->select('sum(total_amount) as total_amount')
							->from('order')
							->where('shipping_id',$shipping_id)
							->where('delivered !=',null)
							->get()
							->row();
	}

	public function get_total_shipping_money($shipping_id=null)
	{
		return $total_order = $this->db->select('sum(total_amount) as total_amount')
							->from('order')
							->where('shipping_id',$shipping_id)
							->where('shipping !=',null)
							->get()
							->row();
	}

	public function sell_report_shipping($page,$per_page,$order_no=null,$customer=null,$date=null,$shipping_id=null,$invoice_no=null,$status=null)
	{
		$this->db->select('
						a.*,
						b.customer_name,
						b.customer_short_address,
						b.customer_mobile,
						c.method_name
						');
		$this->db->from('invoice a');
		$this->db->join('customer_information b','a.customer_id = b.customer_id');
		$this->db->join('shipping_method c','a.shipping_id = c.method_id');

		if ($order_no) {
			$this->db->where('a.order_no',$order_no);
		}

		if ($customer) {
			$this->db->like('b.customer_name',$customer,'both');
			$this->db->or_like('b.customer_mobile',$customer,'both');
		}

		if ($shipping_id) {
			$this->db->where('a.shipping_id',$shipping_id);
		}

		if ($date) {
			$a = explode("---", $date);
			if (count($a) == 1) {
				$from_date =  $a[0];
				$this->db->where('a.date', $from_date);
			}else{
				$from_date = $a[0];
				$this->db->where('a.date >=', $from_date);
				$to_date   = $a[1];
				$this->db->where('a.date <=', $to_date);
			}
		}

		if ($invoice_no) {
			$a = explode("-", $invoice_no);
			if (count($a) == 1) {
				$from_invoice =  $a[0];
				$this->db->where('a.invoice', $from_invoice);
			}else{
				$from_invoice =  $a[0];
				$this->db->where('a.invoice >=', $from_invoice);
				$to_invoice   =  $a[1];
				$this->db->where('a.invoice <=', $to_invoice);
			}
		}

		if ($status) {
			$this->db->where('a.invoice_status',$status);
		}

		$this->db->order_by('a.id','desc');
		$this->db->limit($per_page,$page);
		$query = $this->db->get();
		return $query->result();
	}

    public function order_details_report($page,$per_page,$order_no=null,$customer=null,$date=null,$shipping_id=null,$status=null)
    {
        $this->db->select('
						a.*,
						b.customer_name,
						b.customer_short_address,
						b.customer_mobile,
						c.method_name
						');
        $this->db->from('order a');
        $this->db->join('customer_information b','a.customer_id = b.customer_id');
        $this->db->join('shipping_method c','a.shipping_id = c.method_id');

        if ($order_no) {
            $this->db->where('a.order_no',$order_no);
        }

        if ($customer) {
            $this->db->like('b.customer_name',$customer,'both');
            $this->db->or_like('b.customer_mobile',$customer,'both');
        }

        if ($shipping_id) {
            $this->db->where('a.shipping_id',$shipping_id);
        }

        if ($date) {
            $a = explode("---", $date);
            if (count($a) == 1) {
                $from_date =  $a[0];
                $this->db->where('a.date', $from_date);
            }else{
                $from_date = $a[0];
                $this->db->where('a.date >=', $from_date);
                $to_date   = $a[1];
                $this->db->where('a.date <=', $to_date);
            }
        }

        if ($status) {
            $this->db->where('a.order_status',$status);
        }

        $this->db->order_by('a.id','desc');
        $this->db->limit($per_page,$page);
        $query = $this->db->get();
        return $query->result();
    }

    public function order_details_report_count($page,$per_page,$order_no=null,$customer=null,$date=null,$shipping_id=null,$status=null)
    {
        $this->db->select('
						a.*,
						b.customer_name,
						b.customer_short_address,
						b.customer_mobile,
						c.method_name
						');
        $this->db->from('order a');
        $this->db->join('customer_information b','a.customer_id = b.customer_id');
        $this->db->join('shipping_method c','a.shipping_id = c.method_id');

        if ($order_no) {
            $this->db->where('a.order_no',$order_no);
        }

        if ($customer) {
            $this->db->like('b.customer_name',$customer,'both');
            $this->db->or_like('b.customer_mobile',$customer,'both');
        }

        if ($shipping_id) {
            $this->db->where('a.shipping_id',$shipping_id);
        }

        if ($date) {
            $a = explode("---", $date);
            if (count($a) == 1) {
                $from_date =  $a[0];
                $this->db->where('a.date', $from_date);
            }else{
                $from_date = $a[0];
                $this->db->where('a.date >=', $from_date);
                $to_date   = $a[1];
                $this->db->where('a.date <=', $to_date);
            }
        }

        if ($status) {
            $this->db->where('a.order_status',$status);
        }

        $this->db->order_by('a.id','desc');
        $query = $this->db->get();
        return $query->num_rows();
    }

	public function sell_report_shipping_count($order_no=null,$customer=null,$date=null,$shipping_id=null,$invoice_no=null,$status=null)
	{
		$this->db->select('
						a.*,
						b.customer_name,
						b.customer_short_address,
						b.customer_mobile,
						c.method_name
						');
		$this->db->from('invoice a');
		$this->db->join('customer_information b','a.customer_id = b.customer_id');
		$this->db->join('shipping_method c','a.shipping_id = c.method_id');

		if ($order_no) {
			$this->db->where('a.order_no',$order_no);
		}

		if ($customer) {
			$this->db->like('b.customer_name',$customer,'both');
			$this->db->or_like('b.customer_mobile',$customer,'both');
		}

		if ($shipping_id) {
			$this->db->where('a.shipping_id',$shipping_id);
		}

		if ($date) {
			$a = explode("---", $date);
			if (count($a) == 1) {
				$from_date =  $a[0];
				$this->db->where('a.date', $from_date);
			}else{
				$from_date = $a[0];
				$this->db->where('a.date >=', $from_date);
				$to_date   = $a[1];
				$this->db->where('a.date <=', $to_date);
			}
		}

		if ($invoice_no) {
			$a = explode("-", $invoice_no);
			if (count($a) == 1) {
				$from_invoice =  $a[0];
				$this->db->where('a.invoice', $from_invoice);
			}else{
				$from_invoice =  $a[0];
				$this->db->where('a.invoice >=', $from_invoice);
				$to_invoice   =  $a[1];
				$this->db->where('a.invoice <=', $to_invoice);
			}
		}

		if ($status) {
			$this->db->where('a.invoice_status',$status);
		}

		$this->db->order_by('a.id','desc');
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function merchant_sell_report($page=null,$per_page=null,$order_no=null,$seller=null,$date=null)
	{

		$this->db->select('
						a.*,
						c.first_name,
						c.last_name,
						c.mobile,
						SUM((quantity * rate)-(quantity * discount_per_product)) as total_price,
						SUM((((quantity * rate)-(quantity * discount_per_product)) * seller_percentage)/100) as total_comission,
						');
		$this->db->from('order a');
		$this->db->join('seller_order b','a.order_id = b.order_id');
		$this->db->join('seller_information c','b.seller_id = c.seller_id');
		if ($order_no) {
			$this->db->where('a.order_no',$order_no);
		}

		if ($seller) {
			$this->db->where('b.seller_id',$seller);
		}

		if ($date) {
			$a = explode("---", $date);
			if (count($a) == 1) {
				$from_date =  $a[0];
				$this->db->where('a.date', $from_date);
			}else{
				$from_date = $a[0];
				$this->db->where('a.date >=', $from_date);
				$to_date   = $a[1];
				$this->db->where('a.date <=', $to_date);
			}
			
		}
		$this->db->where('a.delivered !=',null);
		$this->db->order_by('a.id','desc');
		$this->db->group_by('a.id');
		$this->db->limit($per_page,$page);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function merchant_sell_report_count($order_no=null,$seller=null,$date=null)
	{
		$this->db->select('
						a.*,
						c.first_name,
						c.last_name,
						c.mobile,
						SUM((quantity * rate)-(quantity * discount_per_product)) as total_price,
						SUM((((quantity * rate)-(quantity * discount_per_product)) * seller_percentage)/100) as total_comission,
						');
		$this->db->from('order a');
		$this->db->join('seller_order b','a.order_id = b.order_id');
		$this->db->join('seller_information c','b.seller_id = c.seller_id');
		if ($order_no) {
			$this->db->where('a.order_no',$order_no);
		}

		if ($seller) {
			$this->db->where('b.seller_id',$seller);
		}

		if ($date) {
			$a = explode("---", $date);
			if (count($a) == 1) {
				$from_date =  $a[0];
				$this->db->where('a.date', $from_date);
			}else{
				$from_date = $a[0];
				$this->db->where('a.date >=', $from_date);
				$to_date   = $a[1];
				$this->db->where('a.date <=', $to_date);
			}
			
		}
		$this->db->where('a.delivered !=',null);
		$this->db->order_by('a.id','desc');
		$this->db->group_by('a.id');
		$query = $this->db->get();
		return $query->num_rows();
	}

	//Seller Name
	public function seller_name($seller_id){
		$seller_info = $this->db->select('*')
				->from('seller_information')
				->where('seller_id',$seller_id)
				->get()
				->row();

		return $seller_info->first_name.' '.$seller_info->last_name;
	}

	//Stock report product wise
	public function lifetime_restocked($product_id = null)
	{
		$result = $this->db->select('product_id,SUM(quantity) as total_product')
				->from('quantiy_log')
				->where('product_id',$product_id)
				->where('quantity >',0)
				->get()
				->row();
		return $result->total_product;
	}	
	//Stock report product wise
	public function lifetime_wastage($product_id = null)
	{
		$result = $this->db->select('product_id,SUM(quantity) as total_product')
				->from('quantiy_log')
				->where('product_id',$product_id)
				->where('quantity <',0)
				->get()
				->row();
		return $result->total_product;
	}
	//Stock report product wise
	public function last_stock($product_id = null)
	{
		$result = $this->db->select('product_id,SUM(quantity) as total_product')
				->from('quantiy_log')
				->where('product_id',$product_id)
				->get()
				->row();
		return $result->total_product;
	}
	//Total sell product
	public function total_sold_out($product_id=null)
	{
		$this->db->select('
						SUM(quantity) as total_sold_out
						');
		$this->db->from('order a');
		$this->db->join('seller_order b','a.order_id = b.order_id');
		$this->db->where('a.order_status !=',1);
		$this->db->where('a.order_status !=',2);
		$this->db->where('a.order_status !=',5);
		$this->db->where('a.order_status !=',6);
		$this->db->where('b.product_id',$product_id);
		$query = $this->db->get();
		return $query->row()->total_sold_out;
	}
	//Get invoice no by order id
	public function get_invoice_no($order_no){
		return $this->db->select('*')
				->from('invoice')
				->where('order_no',$order_no)
				->get()
				->row();
	}

	public function manage_product($per_page=null,$page=null,$product_id=null,$seller=null,$product_model=null){

		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$this->db->select('
				a.*,
				b.category_name,
				c.first_name,
				c.last_name,
				c.seller_id,
				c.email,
				d.brand_name,
				e.title
			');
		$this->db->from('product_information a');
		$this->db->join('product_category b','a.category_id = b.category_id','left');
		$this->db->join('seller_information c','a.seller_id = c.seller_id','left');
		$this->db->join('brand d','a.brand_id = d.brand_id','left');
		$this->db->join('product_title e','e.product_id = a.product_id','left');
		$this->db->where('e.lang_id',$lang_id);
		
		if ($product_id) {
			$this->db->like('e.title',$product_id,'both');
		}

		if ($seller) {
			$this->db->where('a.seller_id',$seller);
		}

		if ($product_model) {
			$this->db->like('a.product_model',$product_model,'both');
		}

		$this->db->where('a.status',2);
		$this->db->order_by('product_info_id','desc');
		$this->db->limit($per_page,$page);
		$query = $this->db->get();
		return $query->result_array();
	}	

	public function stock_report_product_wise_count($product_id=null,$seller=null,$product_model=null){

		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$this->db->select('
				a.*,
				b.category_name,
				c.first_name,
				c.last_name,
				c.seller_id,
				c.email,
				d.brand_name,
				e.title
			');
		$this->db->from('product_information a');
		$this->db->join('product_category b','a.category_id = b.category_id','left');
		$this->db->join('seller_information c','a.seller_id = c.seller_id','left');
		$this->db->join('brand d','a.brand_id = d.brand_id','left');
		$this->db->join('product_title e','e.product_id = a.product_id','left');
		$this->db->where('e.lang_id',$lang_id);

		if ($product_id) {
			$this->db->like('e.title',$product_id,'both');
		}

		if ($seller) {
			$this->db->where('a.seller_id',$seller);
		}

		if ($product_model) {
			$this->db->like('a.product_model',$product_model,'both');
		}

		$this->db->where('a.status',2);
		$this->db->order_by('product_info_id','desc');
		$query = $this->db->get();
		return $query->num_rows();
	}	
}