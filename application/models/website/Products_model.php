<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Products_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}

	//Product info
	public function product_info($p_id=null)
	{
		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$this->db->select('a.*,
			b.*,
			c.*,
			d.brand_name,
			e.business_name,e.seller_guarantee,e.first_name,e.last_name, f.description');
		$this->db->from('product_information a');
		$this->db->join('product_category b','a.category_id = b.category_id','left');
		$this->db->join('product_title c','a.product_id = c.product_id','left');
		$this->db->join('brand d','a.brand_id = d.brand_id','left');
		$this->db->join('seller_information e','a.seller_id = e.seller_id','left');
		$this->db->join('product_description f','a.product_id = f.product_id','left');
		$this->db->where('a.product_id',$p_id);
		$this->db->where('c.lang_id',$lang_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();	
		}
		return false;
	}
	//Retrive promotion product
	public function promotion_product(){

		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$this->db->select('a.*,b.*');
		$this->db->from('product_information a');
        $this->db->join('product_title b','a.product_id = b.product_id','left');
		$this->db->where('b.lang_id',$lang_id);
		$this->db->where('a.on_promotion',1);
		$this->db->limit(4);
		$this->db->order_by('a.product_info_id','desc');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();	
		}
		return false;
	}
	//Product gallery image
	public function get_thumb_image($p_id)
	{
		$this->db->select('*');
		$this->db->from('product_image');
		$this->db->where('product_id',$p_id);
		$this->db->order_by('product_image_id');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();	
		}
		return false;
	}
	//Product review list
	public function review_list($p_id)
	{
		$this->db->select('*');
		$this->db->from('product_review');
		$this->db->where('product_id',$p_id);
		$this->db->order_by('product_review_id','desc');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();	
		}
		return false;
	}
	//Get customer name
	public function get_customer_name($customer_id){
		$result = $this->db->select('customer_name')
				->from('customer_information')
				->where('customer_id',$customer_id)
				->get()
				->row();
		if ($result) {
			return $result;
		}else{
			return null;
		}
	}
	//Get total five start rating
	public function get_total_five_start_rating($product_id = null,$rate){
		return $this->db->select('*')
				->from('product_review')
				->where('product_id',$product_id)
				->where('rate',$rate)
				->get()
				->num_rows();
	}
	//Stock Report Single Product
	public function stock_report_single_item($p_id)
	{
		$this->db->select("sum(a.quantity) as totalPurchaseQnty");
		$this->db->from('product_information a');
		$this->db->where('a.product_id',$p_id);
		$this->db->where(array('a.status'=>2));
		$query = $this->db->get();
		$purchase=$query->row();

		$this->db->select("sum(a.quantity) as totalSalesQnty");
		$this->db->from('invoice_details a');
		$this->db->where('a.product_id',$p_id);
		$query = $this->db->get();
		$sale=$query->row();

		return $purchase->totalPurchaseQnty - $sale->totalSalesQnty;
	}
	//Category wise related product
	public function related_product($cat_id,$p_id)
	{
		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$this->db->select('a.*,b.*,c.*,d.first_name,d.last_name,e.brand_name');
		$this->db->from('product_information a');
		$this->db->join('product_category b','a.category_id = b.category_id','left');
		$this->db->join('product_title c','a.product_id = c.product_id','left');
		$this->db->join('seller_information d','a.seller_id = d.seller_id','left');
		$this->db->join('brand e','a.brand_id = e.brand_id','left');
		$this->db->where('a.category_id',$cat_id);
		$this->db->where('c.lang_id',$lang_id);
		$this->db->where_not_in('a.product_id',$p_id);
		$query = $this->db->get();
		return $query->result();
	}	
	//Category wise best product
	public function best_sales_category($p_id)
	{
		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$this->db->select('a.*,b.*,c.*,d.first_name,d.last_name,e.brand_name');
		$this->db->from('product_information a');
		$this->db->join('product_category b','a.category_id = b.category_id','left');
		$this->db->join('product_title c','a.product_id = c.product_id','left');
		$this->db->join('seller_information d','a.seller_id = d.seller_id','left');
		$this->db->join('brand e','a.brand_id = e.brand_id','left');
		$this->db->where('a.best_sale',1);
		$this->db->order_by('a.product_info_id','desc');
		$this->db->where('c.lang_id',$lang_id);
		$this->db->where_not_in('a.product_id',$p_id);
		$query = $this->db->get();
		return $query->result();
	}
	//Retrieve retrive refund policy
	public function retrieve_refund_policy()
	{
		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$this->db->select('*');
		$this->db->from('link_page');
		$this->db->where('page_id',7);
		$this->db->where('language_id',$lang_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();	
		}
		return false;
	}
	//Retrieve seller product
	public function retrieve_seller_product($seller_id=null,$price_range=null,$size=null,$sort=null,$rate=null,$seller_score=null)
	{
		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$this->db->select('a.*,b.*,c.*,d.first_name,d.last_name,e.brand_name');
		$this->db->from('product_information a');
		$this->db->join('product_category b','a.category_id = b.category_id','left');
		$this->db->join('product_title c','a.product_id = c.product_id','left');
		$this->db->join('seller_information d','a.seller_id = d.seller_id','left');
		$this->db->join('brand e','a.brand_id = e.brand_id','left');
		$this->db->where('c.lang_id',$lang_id);
		$this->db->where('a.seller_id',$seller_id);

		if ($price_range) {
			$ex = explode("-", $price_range);
	        $from = $ex[0];
	        $to = $ex[1];
	        $this->db->where('price >=', $from);
			$this->db->where('price <=', $to);
		}

		if ($sort) {
			 if ($sort == 'new') {
                $this->db->order_by('a.product_info_id','desc');
            }elseif ($sort == 'discount') {
                $this->db->order_by('a.offer_price','desc');
            }elseif ($sort == 'low_to_high') {
                $this->db->order_by('a.price','asc');
            }elseif ($sort == 'high_to_low') {
                $this->db->order_by('a.price','desc');
            }
		}else{
			$this->db->order_by('a.product_info_id','desc');
		}

		if ($size) {
			$this->db->where('a.variant_id', $size);
		}

		$query = $this->db->get();
		$seller_pro =  $query->result_array();

        if ($seller_score) {
        	$seller_pro = $this->get_product_by_seller_rate($seller_pro,$seller_score);
        }

		if ($rate) {
			return $this->get_rating_product($seller_pro,$rate);
		}else{
			return $seller_pro;
		}
	}
	//Retrieve seller product
	public function retrieve_seller_product_count($seller_id=null)
	{
		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$this->db->select('a.*,b.*,c.*,d.first_name,d.last_name,e.brand_name');
		$this->db->from('product_information a');
		$this->db->join('product_category b','a.category_id = b.category_id','left');
		$this->db->join('product_title c','a.product_id = c.product_id','left');
		$this->db->join('seller_information d','a.seller_id = d.seller_id','left');
		$this->db->join('brand e','a.brand_id = e.brand_id','left');
		$this->db->order_by('a.product_info_id','desc');
		$this->db->where('c.lang_id',$lang_id);
		$this->db->where('a.seller_id',$seller_id);
		$query = $this->db->get();
		return $query->num_rows();
	}
	//Retrive seller info
	public function select_seller_info($seller_id){
		$this->db->select('a.first_name,a.last_name');
		$this->db->from('seller_information a');
		$this->db->where('a.seller_id',$seller_id);
		$query = $this->db->get();
		return $query->row();
	}
	//Retrieve brand product
	public function retrieve_brand_product($brand_id=null,$price_range=null,$size=null,$sort=null,$rate=null,$seller_score=null)
	{
		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$this->db->select('a.*,b.*,c.*,d.first_name,d.last_name,e.brand_name');
		$this->db->from('product_information a');
		$this->db->join('product_category b','a.category_id = b.category_id','left');
		$this->db->join('product_title c','a.product_id = c.product_id','left');
		$this->db->join('seller_information d','a.seller_id = d.seller_id','left');
		$this->db->join('brand e','a.brand_id = e.brand_id','left');
		$this->db->where('c.lang_id',$lang_id);
		$this->db->where('a.brand_id',$brand_id);

		if ($price_range) {
			$ex = explode("-", $price_range);
	        $from = $ex[0];
	        $to = $ex[1];
	        $this->db->where('price >=', $from);
			$this->db->where('price <=', $to);
		}

		if ($sort) {
			 if ($sort == 'new') {
                $this->db->order_by('a.product_info_id','desc');
            }elseif ($sort == 'discount') {
                $this->db->order_by('a.offer_price','desc');
            }elseif ($sort == 'low_to_high') {
                $this->db->order_by('a.price','asc');
            }elseif ($sort == 'high_to_low') {
                $this->db->order_by('a.price','desc');
            }
		}else{
			$this->db->order_by('a.product_info_id','desc');
		}

		if ($size) {
			$this->db->where('a.variant_id', $size);
		}

		$query = $this->db->get();
		$brand_pro =  $query->result_array();


        if ($seller_score) {
        	$brand_pro = $this->get_product_by_seller_rate($brand_pro,$seller_score);
        }

		if ($rate) {
			return $this->get_rating_product($brand_pro,$rate);
		}else{
			return $brand_pro;
		}
	}
	//Get product by seller score 
	public function get_product_by_seller_rate($brand_pro=null,$rate=null){
		$rate = explode('-', $rate);
		$rate = $rate[0];

		$n_brand_pro = array();
		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$rater = 0;
		$rates = 0;

		if ($brand_pro) {
			foreach ($brand_pro as $product) {
				$rater  = $this->get_total_rater_by_seller_id($product['seller_id']);
                $result = $this->get_total_rate_by_seller_id($product['seller_id']);
                if ($rater) {
                	$total_rate = $result->rates/$rater;
	                if ($total_rate >= $rate ) {
						$this->db->select('a.*,b.*,c.brand_name,d.first_name,d.last_name');
						$this->db->from('product_information a');
						$this->db->join('product_title b','a.product_id = b.product_id','left');
						$this->db->join('brand c','c.brand_id = a.brand_id','left');
						$this->db->join('seller_information d','d.seller_id = a.seller_id','left');
						$this->db->where('b.lang_id',$lang_id);
						$this->db->where('a.product_id',$product['product_id']);
						$this->db->where('a.status',2);
						$query = $this->db->get();
						$thrd_brand_pro = $query->result_array();

						if ($thrd_brand_pro) {
							foreach ($thrd_brand_pro as $t_brand_pro) {
								array_push($n_brand_pro, $t_brand_pro);
							}
						}
	                }
	            }
			}
			if (count($n_brand_pro) == 0) {
				return $brand_pro;
			}else{
				return $n_brand_pro;
			}
		}else{
        	return $brand_pro;
        }
	}
	//Get total rater by seller id
	public function get_total_rater_by_seller_id($seller_id=null){
		$rater = $this->db->select('rate')
                ->from('seller_review')
                ->where('seller_id',$seller_id)
                ->get()
                ->num_rows();
        return $rater;
	}
	//Get total rate by seller id
	public function get_total_rate_by_seller_id($seller_id=null){
		$rate = $this->db->select('sum(rate) as rates')
                        ->from('seller_review')
                        ->where('seller_id',$seller_id)
                        ->get()
                        ->row();
        return $rate;
	}
	//Get rating product by rate
	public function get_rating_product($brand_pro=null,$rate=null){
		$rate = explode('-', $rate);
		$rate = $rate[0];

		$n_cat_pro = array();
		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		if ($brand_pro) {
			foreach ($brand_pro as $product) {
				$rater  = $this->get_total_rater_by_product_id($product['product_id']);
                $result = $this->get_total_rate_by_product_id($product['product_id']);
                if ($rater) {
                	$total_rate = $result->rates/$rater;
	                if ($total_rate >= $rate ) {
						$this->db->select('a.*,b.*,c.brand_name,d.first_name,d.last_name');
						$this->db->from('product_information a');
						$this->db->join('product_title b','a.product_id = b.product_id','left');
						$this->db->join('brand c','c.brand_id = a.brand_id','left');
						$this->db->join('seller_information d','d.seller_id = a.seller_id','left');
						$this->db->where('b.lang_id',$lang_id);
						$this->db->where('a.product_id',$product['product_id']);
						$this->db->where('a.status',2);
						$query = $this->db->get();
						$thrd_cat_pro = $query->result_array();

						if ($thrd_cat_pro) {
							foreach ($thrd_cat_pro as $t_cat_pro) {
								array_push($n_cat_pro, $t_cat_pro);
							}
						}
	                }
	            }
			}
			return $n_cat_pro;
		}else{
        	return $brand_pro;
        }
	}

	//Get total rater by product id
	public function get_total_rater_by_product_id($product_id=null){
		$rater = $this->db->select('rate')
                ->from('product_review')
                ->where('product_id',$product_id)
                ->get()
                ->num_rows();
        return $rater;
	}
	//Get total rate by product id
	public function get_total_rate_by_product_id($product_id=null){
		$rate = $this->db->select('sum(rate) as rates')
                        ->from('product_review')
                        ->where('product_id',$product_id)
                        ->get()
                        ->row();
        return $rate;
	}
	//Retrieve brand product
	public function retrieve_brand_product_count($brand_id=null)
	{
		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$this->db->select('a.*,b.*,c.*,d.first_name,d.last_name,e.brand_name');
		$this->db->from('product_information a');
		$this->db->join('product_category b','a.category_id = b.category_id','left');
		$this->db->join('product_title c','a.product_id = c.product_id','left');
		$this->db->join('seller_information d','a.seller_id = d.seller_id','left');
		$this->db->join('brand e','a.brand_id = e.brand_id','left');
		$this->db->order_by('a.product_info_id','desc');
		$this->db->where('c.lang_id',$lang_id);
		$this->db->where('a.brand_id',$brand_id);
		$query = $this->db->get();
		return $query->num_rows();
	}
	//Retrive brand info
	public function select_brand_info($brand_id=null){
		$this->db->select('a.brand_name');
		$this->db->from('brand a');
		$this->db->where('a.brand_id',$brand_id);
		$query = $this->db->get();
		return $query->row();
	}
	//Select max value of product
	public function get_max_value_of_pro($brand_id=null){
		$this->db->select_max('price');
		$this->db->from('product_information');
		$this->db->where('brand_id',$brand_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();	
		}
		return 0;
	}	
	//Select min value of product
	public function get_min_value_of_pro($brand_id=null){
		$this->db->select_min('price');
		$this->db->from('product_information');
		$this->db->where('brand_id',$brand_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();	
		}
		return 0;
	}	
	//Select max value of product
	public function get_max_value_of_pro_seller($seller_id=null){
		$this->db->select_max('price');
		$this->db->from('product_information');
		$this->db->where('seller_id',$seller_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();	
		}
		return 0;
	}	
	//Select min value of product
	public function get_min_value_of_pro_seller($seller_id=null){
		$this->db->select_min('price');
		$this->db->from('product_information');
		$this->db->where('seller_id',$seller_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();	
		}
		return 0;
	}
}