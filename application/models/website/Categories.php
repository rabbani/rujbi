<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Categories extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}
	//Multidimention array sorting
	public function array_msort($array, $cols)
	{
	    $colarr = array();
	    foreach ($cols as $col => $order) {
	        $colarr[$col] = array();
	        foreach ($array as $k => $row) { $colarr[$col]['_'.$k] = strtolower($row[$col]); }
	    }
	    $eval = 'array_multisort(';
	    foreach ($cols as $col => $order) {
	        $eval .= '$colarr[\''.$col.'\'],'.$order.',';
	    }
	    $eval = substr($eval,0,-1).');';
	    eval($eval);
	    $ret = array();
	    foreach ($colarr as $col => $arr) {
	        foreach ($arr as $k => $v) {
	            $k = substr($k,1);
	            if (!isset($ret[$k])) $ret[$k] = $array[$k];
	            $ret[$k][$col] = $array[$k][$col];
	        }
	    }
	    return $ret;
	}
    //Category brand product
	public function category_product($cat_id=null,$price_range=null,$size=null,$brand=null,$sort=null,$rate=null,$seller_rate=null)
	{

		$all_brand     = (explode("--",$brand));
		$arr2 	 	   = array();

		$lang_id 	   = 0;
		$user_lang 	   = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		if ($sort == 'best_sale') {
			$this->db->select('
				c.*,
				d.title,
				e.brand_name,
				f.first_name,
				f.last_name
				');
			$this->db->from('order a');
			$this->db->join('seller_order b','a.order_id = b.order_id');
			$this->db->join('product_information c','c.product_id = b.product_id','left');
			$this->db->join('product_title d','d.product_id = c.product_id','left');
			$this->db->join('brand e','e.brand_id = c.brand_id','left');
			$this->db->join('seller_information f','f.seller_id = c.seller_id','left');

			if ($price_range) {
				$ex = explode("-", $price_range);
		        $from = $ex[0];
		        $to = $ex[1];
		        $this->db->where('c.price >=', $from);
				$this->db->where('c.price <=', $to);
			}

			if ($brand) {
				$this->db->where_in('c.brand_id', $all_brand);
			}

			if ($size) {
				$this->db->where('c.variant_id', $size);
			}

			$this->db->where('b.category_id',$cat_id);
			$this->db->group_by('b.product_id');
			$query = $this->db->get();
			$w_cat_pro = $query->result_array();
		}else{
			$this->db->select('a.*,b.*,c.brand_name,d.first_name,d.last_name');
			$this->db->from('product_information a');
			$this->db->join('product_title b','a.product_id = b.product_id','left');
			$this->db->join('brand c','c.brand_id = a.brand_id','left');
			$this->db->join('seller_information d','d.seller_id = a.seller_id','left');
			$this->db->where('b.lang_id',$lang_id);
			$this->db->where('a.category_id',$cat_id);
			$this->db->where('a.status',2);

			if ($price_range) {
				$ex = explode("-", $price_range);
		        $from = $ex[0];
		        $to = $ex[1];
		        $this->db->where('price >=', $from);
				$this->db->where('price <=', $to);
			}

			if ($brand) {
				$this->db->where_in('a.brand_id', $all_brand);
			}

			if ($size) {
				$this->db->where('a.variant_id', $size);
			}

			//$this->db->limit($per_page,$page);
			$query = $this->db->get();
			$w_cat_pro = $query->result_array();

			//First category
			$first_cat= $this->db->select('*')
							->from('product_category')
							->where('parent_category_id',$cat_id)
							->where('cat_type',2)
							->get()
							->result();
			if ($first_cat) {
				foreach ($first_cat as $f_cat) {

					$this->db->select('a.*,b.*,c.brand_name,d.first_name,d.last_name');
					$this->db->from('product_information a');
					$this->db->join('product_title b','a.product_id = b.product_id','left');
					$this->db->join('brand c','c.brand_id = a.brand_id','left');
					$this->db->join('seller_information d','d.seller_id = a.seller_id','left');
					$this->db->where('b.lang_id',$lang_id);
					$this->db->where('a.category_id',$f_cat->category_id);
					$this->db->where('a.status',2);

					if ($price_range) {
						$ex = explode("-", $price_range);
				        $from = $ex[0];
				        $to = $ex[1];
				        $this->db->where('price >=', $from);
						$this->db->where('price <=', $to);
					}

					if ($brand) {
						$this->db->where_in('a.brand_id', $all_brand);
					}

					if ($size) {
						$this->db->where('a.variant_id', $size);
					}

					//$this->db->limit($per_page,$page);
					$query = $this->db->get();
					$first_cat_pro = $query->result_array();

					if ($first_cat_pro) {
						foreach ($first_cat_pro as $f_cat_pro) {
							array_push($w_cat_pro, $f_cat_pro);
						}
					}

					// Second category
					$second_cat = $this->db->select('*')
										->from('product_category')
										->where('parent_category_id',$f_cat->category_id)
										->where('cat_type',2)
										->get()
										->result();
					if ($second_cat) {
						foreach ($second_cat as $s_cat) {

							$this->db->select('a.*,b.*,c.brand_name,d.first_name,d.last_name');
							$this->db->from('product_information a');
							$this->db->join('product_title b','a.product_id = b.product_id','left');
							$this->db->join('brand c','c.brand_id = a.brand_id','left');
							$this->db->join('seller_information d','d.seller_id = a.seller_id','left');
							$this->db->where('b.lang_id',$lang_id);
							$this->db->where('a.category_id',$s_cat->category_id);
							$this->db->where('a.status',2);
							if ($price_range) {
								$ex = explode("-", $price_range);
						        $from = $ex[0];
						        $to = $ex[1];
						        $this->db->where('price >=', $from);
								$this->db->where('price <=', $to);
							}

							if ($brand) {
								$this->db->where_in('a.brand_id', $all_brand);
							}

							if ($size) {
								$this->db->where('a.variant_id', $size);
							}
							//$this->db->limit($per_page,$page);
							$query = $this->db->get();
							$sec_cat_pro = $query->result_array();


							if ($sec_cat_pro) {
								foreach ($sec_cat_pro as $s_cat_pro) {
									array_push($w_cat_pro, $s_cat_pro);
								}
							}

							//Third category
							$third_cat = $this->db->select('*')
										->from('product_category')
										->where('parent_category_id',$s_cat->category_id)
										->where('cat_type',2)
										->get()
										->result();

							if ($third_cat) {
								foreach ($third_cat as $t_cat) {

									$this->db->select('a.*,b.*,c.brand_name,d.first_name,d.last_name');
									$this->db->from('product_information a');
									$this->db->join('product_title b','a.product_id = b.product_id','left');
									$this->db->join('brand c','c.brand_id = a.brand_id','left');
									$this->db->join('seller_information d','d.seller_id = a.seller_id','left');
									$this->db->where('b.lang_id',$lang_id);
									$this->db->where('a.category_id',$t_cat->category_id);
									$this->db->where('a.status',2);
									if ($price_range) {
										$ex = explode("-", $price_range);
								        $from = $ex[0];
								        $to = $ex[1];
								        $this->db->where('price >=', $from);
										$this->db->where('price <=', $to);
									}
									if ($brand) {
										$this->db->where_in('a.brand_id', $all_brand);
									}
									if ($size) {
										$this->db->where('a.variant_id', $size);
									}
									//$this->db->limit($per_page,$page);
									$query = $this->db->get();
									$thrd_cat_pro = $query->result_array();

									if ($thrd_cat_pro) {
										foreach ($thrd_cat_pro as $t_cat_pro) {
											array_push($w_cat_pro, $t_cat_pro);
										}
									}
								}
							}
						}
					}
				}
			}
		}

		if ($rate) {
        	$w_cat_pro = $this->get_rating_product($w_cat_pro,$rate);
        }

        if ($seller_rate) {
        	$w_cat_pro = $this->get_product_by_seller_rate($w_cat_pro,$seller_rate);
        }

		if ($sort != null && $w_cat_pro != null) {
            if ($sort == 'new') {
                $w_cat_pro = $this->Categories->array_msort($w_cat_pro, array('product_info_id'=>SORT_DESC));
            }elseif ($sort == 'discount') {
                $w_cat_pro = $this->Categories->array_msort($w_cat_pro, array('offer_price'=>SORT_ASC));
            }elseif ($sort == 'low_to_high') {
                $w_cat_pro = $this->Categories->array_msort($w_cat_pro, array('price'=>SORT_ASC));
            }elseif ($sort == 'high_to_low') {
                $w_cat_pro = $this->Categories->array_msort($w_cat_pro, array('price'=>SORT_DESC));
            }
        }else{
        	$w_cat_pro = $this->Categories->array_msort($w_cat_pro, array('product_info_id'=>SORT_DESC));
        }

       	return $w_cat_pro;
	}
	//Get rating product by rate
	public function get_rating_product($w_cat_pro='',$rate=''){
		$rate = explode('-', $rate);
		$rate = $rate[0];

		$n_cat_pro = array();
		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		if ($w_cat_pro) {
			foreach ($w_cat_pro as $product) {
				$rater  = $this->get_total_rater_by_product_id($product['product_id']);
                $result = $this->get_total_rate_by_product_id($product['product_id']);
                if ($rater) {
                	$total_rate = $result->rates/$rater;
	                if ($total_rate >= $rate ) {
						$this->db->select('a.*,b.*,c.brand_name,d.first_name,d.last_name');
						$this->db->from('product_information a');
						$this->db->join('product_title b','a.product_id = b.product_id','left');
						$this->db->join('brand c','c.brand_id = a.brand_id','left');
						$this->db->join('seller_information d','d.seller_id = a.seller_id','left');
						$this->db->where('b.lang_id',$lang_id);
						$this->db->where('a.product_id',$product['product_id']);
						$this->db->where('a.status',2);
						$query = $this->db->get();
						$thrd_cat_pro = $query->result_array();

						if ($thrd_cat_pro) {
							foreach ($thrd_cat_pro as $t_cat_pro) {
								array_push($n_cat_pro, $t_cat_pro);
							}
						}
	                }
	            }
			}
			return $n_cat_pro;
		}else{
        	return $w_cat_pro;
        }
	}
	//Get product by seller score 
	public function get_product_by_seller_rate($w_cat_pro='',$rate=''){
		$rate = explode('-', $rate);
		$rate = $rate[0];

		$n_cat_pro = array();
		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$rater = 0;
		$rates = 0;

		if ($w_cat_pro) {
			foreach ($w_cat_pro as $product) {
				$rater  = $this->get_total_rater_by_seller_id($product['seller_id']);
                $result = $this->get_total_rate_by_seller_id($product['seller_id']);
                if ($rater) {
                	$total_rate = $result->rates/$rater;
	                if ($total_rate >= $rate ) {
						$this->db->select('a.*,b.*,c.brand_name,d.first_name,d.last_name');
						$this->db->from('product_information a');
						$this->db->join('product_title b','a.product_id = b.product_id','left');
						$this->db->join('brand c','c.brand_id = a.brand_id','left');
						$this->db->join('seller_information d','d.seller_id = a.seller_id','left');
						$this->db->where('b.lang_id',$lang_id);
						$this->db->where('a.product_id',$product['product_id']);
						$this->db->where('a.status',2);
						$query = $this->db->get();
						$thrd_cat_pro = $query->result_array();

						if ($thrd_cat_pro) {
							foreach ($thrd_cat_pro as $t_cat_pro) {
								array_push($n_cat_pro, $t_cat_pro);
							}
						}
	                }
	            }
			}
			if (count($n_cat_pro) == 0) {
				return $w_cat_pro;
			}else{
				
			}
		}else{
        	return $w_cat_pro;
        }
	}
	//Get total rater by seller id
	public function get_total_rater_by_seller_id($seller_id=null){
		$rater = $this->db->select('rate')
                ->from('seller_review')
                ->where('seller_id',$seller_id)
                ->get()
                ->num_rows();
        return $rater;
	}
	//Get total rate by seller id
	public function get_total_rate_by_seller_id($seller_id=null){
		$rate = $this->db->select('sum(rate) as rates')
                        ->from('seller_review')
                        ->where('seller_id',$seller_id)
                        ->get()
                        ->row();
        return $rate;
	}
	//Select all sub category product
	public function select_total_sub_cat_pro($cat_id=null,$brand=null){

		$all_brand = (explode("--",$brand));
		$total_pro = 0;
		$lang_id   = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$price_range = $this->input->get('price');
        $size        = $this->input->get('size');
        $sort        = $this->input->get('sort');
        $rate        = $this->input->get('rate');
        $seller_rate = $this->input->get('seller_rate');

		$this->db->select('a.*,b.*,c.brand_name,d.first_name,d.last_name');
		$this->db->from('product_information a');
		$this->db->join('product_title b','a.product_id = b.product_id','left');
		$this->db->join('brand c','c.brand_id = a.brand_id','left');
		$this->db->join('seller_information d','d.seller_id = a.seller_id','left');
		$this->db->where('b.lang_id',$lang_id);
		$this->db->where('a.category_id',$cat_id);
		$this->db->where('a.status',2);

		if ($price_range) {
			$ex = explode("-", $price_range);
	        $from = $ex[0];
	        $to = $ex[1];
	        $this->db->where('price >=', $from);
			$this->db->where('price <=', $to);
		}

		if ($brand) {
			$this->db->where_in('a.brand_id', $all_brand);
		}

		if ($size) {
			$this->db->where('a.variant_id', $size);
		}
		$query = $this->db->get();
		$w_cat_pro = $query->result_array();

		//First category
		$first_cat= $this->db->select('*')
						->from('product_category')
						->where('parent_category_id',$cat_id)
						->where('cat_type',2)
						->get()
						->result();
		if ($first_cat) {
			foreach ($first_cat as $f_cat) {

				$this->db->select('a.*,b.*,c.brand_name,d.first_name,d.last_name');
				$this->db->from('product_information a');
				$this->db->join('product_title b','a.product_id = b.product_id','left');
				$this->db->join('brand c','c.brand_id = a.brand_id','left');
				$this->db->join('seller_information d','d.seller_id = a.seller_id','left');
				$this->db->where('b.lang_id',$lang_id);
				$this->db->where('a.category_id',$f_cat->category_id);
				$this->db->where('a.status',2);

				if ($price_range) {
					$ex = explode("-", $price_range);
			        $from = $ex[0];
			        $to = $ex[1];
			        $this->db->where('price >=', $from);
					$this->db->where('price <=', $to);
				}

				if ($brand) {
					$this->db->where_in('a.brand_id', $all_brand);
				}

				if ($size) {
					$this->db->where('a.variant_id', $size);
				}
				$query = $this->db->get();
				$first_cat_pro = $query->result_array();

				if ($first_cat_pro) {
					foreach ($first_cat_pro as $f_cat_pro) {
						array_push($w_cat_pro, $f_cat_pro);
					}
				}

				// Second category
				$second_cat = $this->db->select('*')
									->from('product_category')
									->where('parent_category_id',$f_cat->category_id)
									->where('cat_type',2)
									->get()
									->result();
				if ($second_cat) {
					foreach ($second_cat as $s_cat) {

						$this->db->select('a.*,b.*,c.brand_name,d.first_name,d.last_name');
						$this->db->from('product_information a');
						$this->db->join('product_title b','a.product_id = b.product_id','left');
						$this->db->join('brand c','c.brand_id = a.brand_id','left');
						$this->db->join('seller_information d','d.seller_id = a.seller_id','left');
						$this->db->where('b.lang_id',$lang_id);
						$this->db->where('a.category_id',$s_cat->category_id);
						$this->db->where('a.status',2);
						if ($price_range) {
							$ex = explode("-", $price_range);
					        $from = $ex[0];
					        $to = $ex[1];
					        $this->db->where('price >=', $from);
							$this->db->where('price <=', $to);
						}

						if ($brand) {
							$this->db->where_in('a.brand_id', $all_brand);
						}

						if ($size) {
							$this->db->where('a.variant_id', $size);
						}
						$query = $this->db->get();
						$sec_cat_pro = $query->result_array();


						if ($sec_cat_pro) {
							foreach ($sec_cat_pro as $s_cat_pro) {
								array_push($w_cat_pro, $s_cat_pro);
							}
						}

						//Third category
						$third_cat = $this->db->select('*')
									->from('product_category')
									->where('parent_category_id',$s_cat->category_id)
									->where('cat_type',2)
									->get()
									->result();

						if ($third_cat) {
							foreach ($third_cat as $t_cat) {

								$this->db->select('a.*,b.*,c.brand_name,d.first_name,d.last_name');
								$this->db->from('product_information a');
								$this->db->join('product_title b','a.product_id = b.product_id','left');
								$this->db->join('brand c','c.brand_id = a.brand_id','left');
								$this->db->join('seller_information d','d.seller_id = a.seller_id','left');
								$this->db->where('b.lang_id',$lang_id);
								$this->db->where('a.category_id',$t_cat->category_id);
								$this->db->where('a.status',2);
								if ($price_range) {
									$ex = explode("-", $price_range);
							        $from = $ex[0];
							        $to = $ex[1];
							        $this->db->where('price >=', $from);
									$this->db->where('price <=', $to);
								}

								if ($brand) {
									$this->db->where_in('a.brand_id', $all_brand);
								}
								if ($size) {
									$this->db->where('a.variant_id', $size);
								}
								$query = $this->db->get();
								$thrd_cat_pro = $query->result_array();

								if ($thrd_cat_pro) {
									foreach ($thrd_cat_pro as $t_cat_pro) {
										array_push($w_cat_pro, $t_cat_pro);
									}
								}
							}
						}
					}
				}
			}
		}

		if ($rate) {
        	$w_cat_pro = $this->get_rating_product($w_cat_pro,$rate);
        }

        if ($seller_rate) {
        	$w_cat_pro = $this->get_product_by_seller_rate($w_cat_pro,$seller_rate);
        }

		if ($sort != null && $w_cat_pro != null) {
            if ($sort == 'new') {
                $w_cat_pro = $this->Categories->array_msort($w_cat_pro, array('product_info_id'=>SORT_DESC));
            }elseif ($sort == 'discount') {
                $w_cat_pro = $this->Categories->array_msort($w_cat_pro, array('offer_price'=>SORT_ASC));
            }elseif ($sort == 'low_to_high') {
                $w_cat_pro = $this->Categories->array_msort($w_cat_pro, array('price'=>SORT_ASC));
            }elseif ($sort == 'high_to_low') {
                $w_cat_pro = $this->Categories->array_msort($w_cat_pro, array('price'=>SORT_DESC));
            }
        }else{
        	$w_cat_pro = $this->Categories->array_msort($w_cat_pro, array('product_info_id'=>SORT_DESC));
        }
        return count($w_cat_pro);
	}
	//Select all sub category no of product
	public function select_total_sub_cat_no_of_pro($cat_id=null){

		$total_pro = 0;
		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$this->db->select('a.*,b.*');
		$this->db->from('product_information a');
		$this->db->join('product_title b','a.product_id = b.product_id','left');
		$this->db->join('brand c','c.brand_id = a.brand_id','left');
		$this->db->where('b.lang_id',$lang_id);
		$this->db->where('a.category_id',$cat_id);
		$this->db->where('a.status',2);

		$query = $this->db->get();
		$parent_cat = $query->num_rows();

		$total_pro = $total_pro+$parent_cat;

		$sec_cat = $this->db->select('*')
						->from('product_category')
						->where('parent_category_id',$cat_id)
						->get()
						->result();
		if ($sec_cat) {
			foreach ($sec_cat as $s_cat) {
				$this->db->select('a.*,b.*');
				$this->db->from('product_information a');
				$this->db->join('product_title b','a.product_id = b.product_id','left');
				$this->db->join('brand c','c.brand_id = a.brand_id','left');
				$this->db->where('b.lang_id',$lang_id);
				$this->db->where('a.category_id',$s_cat->category_id);
				$this->db->where('a.status',2);

				$query = $this->db->get();
				$sct = $query->num_rows();

				$total_pro = $total_pro+$sct;
				if ($s_cat) {
					$last_cat = $this->db->select('*')
							->from('product_category')
							->where('parent_category_id',$s_cat->category_id)
							->get()
							->result();
					if ($last_cat) {
						foreach ($last_cat as $l_ct) {
							$this->db->select('a.*,b.*');
							$this->db->from('product_information a');
							$this->db->join('product_title b','a.product_id = b.product_id','left');
							$this->db->join('brand c','c.brand_id = a.brand_id','left');
							$this->db->where('b.lang_id',$lang_id);
							$this->db->where('a.category_id',$l_ct->category_id);
							$this->db->where('a.status',2);

							$query = $this->db->get();
							$lct = $query->num_rows();

							$total_pro = $total_pro+$lct;
						}
					}
				}
			}
		}
		return $total_pro;
	}
	//Select all sub category brand info
	public function select_sub_cat_brand_info($cat_id='')
	{
		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$this->db->select('c.*,a.category_id');
		$this->db->from('product_information a');
		$this->db->join('product_title b','a.product_id = b.product_id','left');
		$this->db->join('brand c','c.brand_id = a.brand_id','left');
		$this->db->join('seller_information d','d.seller_id = a.seller_id','left');
		$this->db->where('b.lang_id',$lang_id);
		$this->db->where('a.category_id',$cat_id);
		$this->db->where('a.status',2);
		$this->db->group_by('a.brand_id');

		$query = $this->db->get();
		$w_cat_pro = $query->result_array();

		//First category
		$first_cat= $this->db->select('*')
						->from('product_category')
						->where('parent_category_id',$cat_id)
						->where('cat_type',2)
						->get()
						->result();
		if ($first_cat) {
			foreach ($first_cat as $f_cat) {

				$this->db->select('c.*,a.category_id');
				$this->db->from('product_information a');
				$this->db->join('product_title b','a.product_id = b.product_id','left');
				$this->db->join('brand c','c.brand_id = a.brand_id','left');
				$this->db->join('seller_information d','d.seller_id = a.seller_id','left');
				$this->db->where('b.lang_id',$lang_id);
				$this->db->where('a.category_id',$f_cat->category_id);
				$this->db->where('a.status',2);
				$this->db->group_by('a.brand_id');

				$query = $this->db->get();
				$first_cat_pro = $query->result_array();

				if ($first_cat_pro) {
					foreach ($first_cat_pro as $f_cat_pro) {
						array_push($w_cat_pro, $f_cat_pro);
					}
				}

				// Second category
				$second_cat = $this->db->select('*')
									->from('product_category')
									->where('parent_category_id',$f_cat->category_id)
									->where('cat_type',2)
									->get()
									->result();
				if ($second_cat) {
					foreach ($second_cat as $s_cat) {

						$this->db->select('c.*,a.category_id');
						$this->db->from('product_information a');
						$this->db->join('product_title b','a.product_id = b.product_id','left');
						$this->db->join('brand c','c.brand_id = a.brand_id','left');
						$this->db->join('seller_information d','d.seller_id = a.seller_id','left');
						$this->db->where('b.lang_id',$lang_id);
						$this->db->where('a.category_id',$s_cat->category_id);
						$this->db->where('a.status',2);
						$this->db->group_by('a.brand_id');
						
						$query = $this->db->get();
						$sec_cat_pro = $query->result_array();

						if ($sec_cat_pro) {
							foreach ($sec_cat_pro as $s_cat_pro) {
								array_push($w_cat_pro, $s_cat_pro);
							}
						}

						//Third category
						$third_cat = $this->db->select('*')
									->from('product_category')
									->where('parent_category_id',$s_cat->category_id)
									->where('cat_type',2)
									->get()
									->result();

						if ($third_cat) {
							foreach ($third_cat as $t_cat) {

								$this->db->select('c.*,a.category_id');
								$this->db->from('product_information a');
								$this->db->join('product_title b','a.product_id = b.product_id','left');
								$this->db->join('brand c','c.brand_id = a.brand_id','left');
								$this->db->join('seller_information d','d.seller_id = a.seller_id','left');
								$this->db->where('b.lang_id',$lang_id);
								$this->db->where('a.category_id',$t_cat->category_id);
								$this->db->where('a.status',2);
								$this->db->group_by('a.brand_id');

								$query = $this->db->get();
								$thrd_cat_pro = $query->result_array();

								if ($thrd_cat_pro) {
									foreach ($thrd_cat_pro as $t_cat_pro) {
										array_push($w_cat_pro, $t_cat_pro);
									}
								}
							}
						}
					}
				}
			}
		}
		return  $this->unique_multidim_array($w_cat_pro,'brand_id');
	}
	//Select all sub category brand info
	public function select_sub_cat_brand_info_search($cat_id='',$search_key='')
	{
		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$this->db->select('c.*,a.category_id');
		$this->db->from('product_information a');
		$this->db->join('product_title b','a.product_id = b.product_id','left');
		$this->db->join('brand c','c.brand_id = a.brand_id','left');
		$this->db->join('seller_information d','d.seller_id = a.seller_id','left');
		$this->db->where('b.lang_id',$lang_id);
		$this->db->where('a.category_id',$cat_id);
		$this->db->where('a.status',2);
		$this->db->like('c.brand_name', $search_key, 'both'); 
		$this->db->group_by('a.brand_id');

		$query = $this->db->get();
		$w_cat_pro = $query->result_array();

		//First category
		$first_cat= $this->db->select('*')
						->from('product_category')
						->where('parent_category_id',$cat_id)
						->where('cat_type',2)
						->get()
						->result();
		if ($first_cat) {
			foreach ($first_cat as $f_cat) {

				$this->db->select('c.*,a.category_id');
				$this->db->from('product_information a');
				$this->db->join('product_title b','a.product_id = b.product_id','left');
				$this->db->join('brand c','c.brand_id = a.brand_id','left');
				$this->db->join('seller_information d','d.seller_id = a.seller_id','left');
				$this->db->where('b.lang_id',$lang_id);
				$this->db->where('a.category_id',$f_cat->category_id);
				$this->db->where('a.status',2);
				$this->db->like('c.brand_name', $search_key, 'both'); 
				$this->db->group_by('a.brand_id');

				$query = $this->db->get();
				$first_cat_pro = $query->result_array();

				if ($first_cat_pro) {
					foreach ($first_cat_pro as $f_cat_pro) {
						array_push($w_cat_pro, $f_cat_pro);
					}
				}

				// Second category
				$second_cat = $this->db->select('*')
									->from('product_category')
									->where('parent_category_id',$f_cat->category_id)
									->where('cat_type',2)
									->get()
									->result();
				if ($second_cat) {
					foreach ($second_cat as $s_cat) {

						$this->db->select('c.*,a.category_id');
						$this->db->from('product_information a');
						$this->db->join('product_title b','a.product_id = b.product_id','left');
						$this->db->join('brand c','c.brand_id = a.brand_id','left');
						$this->db->join('seller_information d','d.seller_id = a.seller_id','left');
						$this->db->where('b.lang_id',$lang_id);
						$this->db->where('a.category_id',$s_cat->category_id);
						$this->db->where('a.status',2);
						$this->db->like('c.brand_name', $search_key, 'both'); 
						$this->db->group_by('a.brand_id');
						
						$query = $this->db->get();
						$sec_cat_pro = $query->result_array();

						if ($sec_cat_pro) {
							foreach ($sec_cat_pro as $s_cat_pro) {
								array_push($w_cat_pro, $s_cat_pro);
							}
						}

						//Third category
						$third_cat = $this->db->select('*')
									->from('product_category')
									->where('parent_category_id',$s_cat->category_id)
									->where('cat_type',2)
									->get()
									->result();

						if ($third_cat) {
							foreach ($third_cat as $t_cat) {

								$this->db->select('c.*,a.category_id');
								$this->db->from('product_information a');
								$this->db->join('product_title b','a.product_id = b.product_id','left');
								$this->db->join('brand c','c.brand_id = a.brand_id','left');
								$this->db->join('seller_information d','d.seller_id = a.seller_id','left');
								$this->db->where('b.lang_id',$lang_id);
								$this->db->where('a.category_id',$t_cat->category_id);
								$this->db->where('a.status',2);
								$this->db->like('c.brand_name', $search_key, 'both'); 
								$this->db->group_by('a.brand_id');

								$query = $this->db->get();
								$thrd_cat_pro = $query->result_array();

								if ($thrd_cat_pro) {
									foreach ($thrd_cat_pro as $t_cat_pro) {
										array_push($w_cat_pro, $t_cat_pro);
									}
								}
							}
						}
					}
				}
			}
		}
		return  $this->unique_multidim_array($w_cat_pro,'brand_id');
	}
	//Select brand product
	public function total_brand_pro($brand_id=null,$cat_id=null)
	{
		$total_pro = 0;
		$lang_id = 0;

		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$this->db->select('b.*,count(a.product_id) as total');
		$this->db->from('product_information a');
		$this->db->join('brand b','a.brand_id = b.brand_id','left');
		$this->db->where('a.category_id',$cat_id);
		$this->db->where('a.brand_id',$brand_id);
		$this->db->group_by('a.brand_id');
		$this->db->where('a.status',2);

		$query = $this->db->get();
		$parent_cat = $query->num_rows();
		$total_pro = $total_pro+$parent_cat;

		$sec_cat = $this->db->select('*')
						->from('product_category')
						->where('parent_category_id',$cat_id)
						->get()
						->result();
		if ($sec_cat) {
			foreach ($sec_cat as $s_cat) {
				$this->db->select('b.*,count(a.product_id) as total');
				$this->db->from('product_information a');
				$this->db->join('brand b','a.brand_id = b.brand_id','left');
				$this->db->where('a.category_id',$s_cat->category_id);
				$this->db->where('a.brand_id',$brand_id);
				$this->db->group_by('a.brand_id');
				$this->db->where('a.status',2);

				$query = $this->db->get();
				$sct = $query->num_rows();

				$total_pro = $total_pro+$sct;
				if ($s_cat) {
					$last_cat = $this->db->select('*')
							->from('product_category')
							->where('parent_category_id',$s_cat->category_id)
							->get()
							->result();
							
					if ($last_cat) {
						foreach ($last_cat as $l_ct) {
							$this->db->select('b.*,count(a.product_id) as total');
							$this->db->from('product_information a');
							$this->db->join('brand b','a.brand_id = b.brand_id','left');
							$this->db->where('a.category_id',$l_ct->category_id);
							$this->db->where('a.brand_id',$brand_id);
							$this->db->group_by('a.brand_id');
							$this->db->where('a.status',2);

							$query = $this->db->get();
							$lct = $query->num_rows();

							$total_pro = $total_pro+$lct;
						}
					}
				}
			}
		}
		return $total_pro;             
	}
	//Multidimentional array unique
	public function unique_multidim_array($array, $key) { 
	    $temp_array = array(); 
	    $i = 0; 
	    $key_array = array(); 
	    
	    foreach($array as $val) { 
	        if (!in_array($val[$key], $key_array)) { 
	            $key_array[$i] = $val[$key]; 
	            $temp_array[$i] = $val; 
	        } 
	        $i++; 
	    } 
	    return $temp_array; 
	}
	//brand Search Item
	public function brand_search_item($brand_id)
	{
		$this->db->select('*');
		$this->db->from('brand');
		$this->db->where('brand_id',$brand_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();	
		}
		return false;
	}
	//Category wise product 
	public function category_wise_product($cat_id,$per_page,$page)
	{
		$this->db->select('*');
		$this->db->from('product_information');
		$this->db->where('category_id',$cat_id);
		$this->db->limit($per_page,$page);
		$this->db->order_by('product_name');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();	
		}
		return false;
	}
	//Category price range product
	public function cat_price_range_pro($min,$max,$cat_id)
	{
		$this->db->select('*');
		$this->db->from('product_information');
		$this->db->where('category_id',$cat_id);
		$this->db->where('price >=', $min);
		$this->db->where('price <=', $max);
		$this->db->order_by('product_name');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();	
		}
		return false;
	}
	//Category wise product count
	public function category_wise_product_count($cat_id)
	{
		$this->db->select('*');
		$this->db->from('product_information');
		$this->db->where('category_id',$cat_id);
		$this->db->order_by('product_name');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->num_rows();	
		}
		return false;
	}
	//Select single category  
	public function select_single_category($cat_id)
	{
		$this->db->select('*');
		$this->db->from('product_category');
		$this->db->where('category_id',$cat_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();	
		}
		return false;
	}
	//Select single category  
	public function select_single_category_by_id($cat_id)
	{
		$this->db->select('*');
		$this->db->from('product_category');
		$this->db->where('category_id',$cat_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();	
		}
		return false;
	}
	//Retrive category product
	public function retrieve_category_product($cat_id=null,$product_name=null,$price_range=null,$size=null,$sort=null,$rate=null,$seller_score=null,$brand=null)
	{
		$all_brand = (explode("--",$brand));
		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$this->db->select('a.*,b.*,c.*,d.first_name,d.last_name,e.brand_name');
		$this->db->from('product_information a');
		$this->db->join('product_category b','a.category_id = b.category_id','left');
		$this->db->join('product_title c','a.product_id = c.product_id','left');
		$this->db->join('seller_information d','a.seller_id = d.seller_id','left');
		$this->db->join('brand e','a.brand_id = e.brand_id','left');
		$this->db->where('a.status',2);

		if ($price_range) {
			$ex = explode("-", $price_range);
	        $from = $ex[0];
	        $to = $ex[1];
	        $this->db->where('price >=', $from);
			$this->db->where('price <=', $to);
		}
	
		if ($size) {
			$this->db->where('a.variant_id', $size);
		}

		if ($product_name) {
			$this->db->like('c.title', $product_name, 'both');
			$this->db->or_like('a.product_model', $product_name,'both');
			$this->db->where('a.product_model',$product_name,'both');
		}

		if ($brand) {
			$this->db->where_in('a.brand_id', $all_brand);
		}

		$this->db->where('c.lang_id',$lang_id);
		$this->db->order_by('a.product_info_id','desc');
		$query = $this->db->get();
		$w_cat_pro = $query->result_array();

		if ($rate) {
        	$w_cat_pro = $this->get_rating_product($w_cat_pro,$rate);
        }

        if ($seller_score) {
        	$w_cat_pro = $this->get_product_by_seller_rate($w_cat_pro,$seller_score);
        }

        return $w_cat_pro;
	}
	//Retrive category product
	public function retrieve_category_product_count($cat_id=null,$product_name=null)
	{
		$total_pro = 0;
		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$this->db->select('a.*,b.*,c.*,d.first_name,d.last_name,e.brand_name');
		$this->db->from('product_information a');
		$this->db->join('product_category b','a.category_id = b.category_id','left');
		$this->db->join('product_title c','a.product_id = c.product_id','left');
		$this->db->join('seller_information d','a.seller_id = d.seller_id','left');
		$this->db->join('brand e','a.brand_id = e.brand_id','left');
		$this->db->where('a.status',2);

		if ($cat_id != 'all') {
			$this->db->like('c.title', $product_name, 'both');
			$this->db->or_like('a.product_model', $product_name, 'both');
			$this->db->where('a.category_id',$cat_id);
		}

		$this->db->where('c.lang_id',$lang_id);
		$query = $this->db->get();
		$parent_cat = $query->num_rows();
		$total_pro = $total_pro+$parent_cat;
		return $total_pro;
	}
	//Retrive category product ajax
	public function category_product_search_ajax($cat_id=null,$search_value=null)
	{
		$arr2 	 = array();
		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$this->db->select('a.*,count(a.category_id) as total_pro');
		$this->db->from('product_category a');
		$this->db->join('product_information b','a.category_id = b.category_id','left');
		$this->db->join('product_title c','b.product_id = c.product_id','left');
		$this->db->where('b.status',2);
		$this->db->like('c.title', $search_value, 'both');
		$this->db->or_like('b.product_model', $search_value, 'both');
		$this->db->group_by('a.category_id');
		$this->db->where('c.lang_id',$lang_id);

		if ($cat_id != 'all') {
			$this->db->where('a.category_id',$cat_id);
		}

		$category = $this->db->get();

		if ($category->num_rows() > 0) {
			
			$this->db->select('a.*,b.*');
			$this->db->from('product_information a');
			$this->db->join('product_title b','a.product_id = b.product_id','left');
			$this->db->like('b.title', $search_value, 'both');
			$this->db->or_like('a.product_model', $search_value, 'both');
			$this->db->where('a.status',2);
			if ($cat_id != 'all') {
				$this->db->where('a.category_id',$cat_id);
			}
			$this->db->where('b.lang_id',$lang_id);
			$product = $this->db->get();

			$data = array(
				'category_info' => $category->result(), 
				'product_info' => $product->result(), 
			);

			return $data;
		}
		return false;
	}
	//Select min value of product
	public function maxValueInArray($array, $keyToSearch)
	{
	    $currentMax = NULL;
	    foreach($array as $arr)
	    {
	        foreach($arr as $key => $value)
	        {
	            if ($key == $keyToSearch && ($value >= $currentMax))
	            {
	                $currentMax = $value;
	            }
	        }
	    }

	    return $currentMax;
	}
	//Minvalue by array key
	public function min_by_key($arr=null, $key=null){ 
	    $min = array(); 
	    foreach ($arr as $val) { 
	        if (!isset($val[$key]) and is_array($val)) { 
	            $min2 = min_by_key($val, $key); 
	            $min[$min2] = 1; 
	        } elseif (!isset($val[$key]) and !is_array($val)) { 
	            return false; 
	        } elseif (isset($val[$key])) { 
	            $min[$val[$key]] = 1; 
	        } 
	    }
	    if (count($min) > 0) {
	    	return min( array_keys($min) ); 
	    }else{
	    	return 0;
	    }
	}
	//Select max price of a category product
	public function select_max_value_of_cat_pro($cat_id=null,$val=null)
	{
		$arr2 	 = array();
		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$sort = $this->input->get('sort');

		if ($sort == 'best_sale') {
			$this->db->select('
				c.*,
				d.title,
				e.brand_name,
				f.first_name,
				f.last_name
				');
			$this->db->from('order a');
			$this->db->join('seller_order b','a.order_id = b.order_id');
			$this->db->join('product_information c','c.product_id = b.product_id','left');
			$this->db->join('product_title d','d.product_id = c.product_id','left');
			$this->db->join('brand e','e.brand_id = c.brand_id','left');
			$this->db->join('seller_information f','f.seller_id = c.seller_id','left');
			$this->db->where('b.category_id',$cat_id);
			$this->db->group_by('b.product_id');
			$query = $this->db->get();
			$w_cat_pro = $query->result_array();
		}else{

			$this->db->select('a.*,b.*');
			$this->db->from('product_information a');
			$this->db->join('product_title b','a.product_id = b.product_id','left');
			$this->db->join('brand c','c.brand_id = a.brand_id','left');
			$this->db->where('b.lang_id',$lang_id);
			$this->db->where('a.category_id',$cat_id);
			$this->db->where('a.status',2);
			$query = $this->db->get();
			$w_cat_pro = $query->result_array();

			//First category
			$first_cat= $this->db->select('*')
							->from('product_category')
							->where('parent_category_id',$cat_id)
							->where('cat_type',2)
							->get()
							->result();
			if ($first_cat) {
				foreach ($first_cat as $f_cat) {

					$this->db->select('a.*,b.*');
					$this->db->from('product_information a');
					$this->db->join('product_title b','a.product_id = b.product_id','left');
					$this->db->join('brand c','c.brand_id = a.brand_id','left');
					$this->db->where('b.lang_id',$lang_id);
					$this->db->where('a.category_id',$f_cat->category_id);
					$this->db->where('a.status',2);
					$query = $this->db->get();
					$first_cat_pro = $query->result_array();

					if ($first_cat_pro) {
						foreach ($first_cat_pro as $f_cat_pro) {
							array_push($w_cat_pro, $f_cat_pro);
						}
					}

					// Second category
					$second_cat = $this->db->select('*')
										->from('product_category')
										->where('parent_category_id',$f_cat->category_id)
										->where('cat_type',2)
										->get()
										->result();
					if ($second_cat) {
						foreach ($second_cat as $s_cat) {

							$this->db->select('a.*,b.*');
							$this->db->from('product_information a');
							$this->db->join('product_title b','a.product_id = b.product_id','left');
							$this->db->join('brand c','c.brand_id = a.brand_id','left');
							$this->db->where('b.lang_id',$lang_id);
							$this->db->where('a.category_id',$s_cat->category_id);
							$this->db->where('a.status',2);
							$query = $this->db->get();
							$sec_cat_pro = $query->result_array();


							if ($sec_cat_pro) {
								foreach ($sec_cat_pro as $s_cat_pro) {
									array_push($w_cat_pro, $s_cat_pro);
								}
							}

							//Third category
							$third_cat = $this->db->select('*')
										->from('product_category')
										->where('parent_category_id',$s_cat->category_id)
										->where('cat_type',2)
										->get()
										->result();

							if ($third_cat) {
								foreach ($third_cat as $t_cat) {

									$this->db->select('a.*,b.*');
									$this->db->from('product_information a');
									$this->db->join('product_title b','a.product_id = b.product_id','left');
									$this->db->join('brand c','c.brand_id = a.brand_id','left');
									$this->db->where('b.lang_id',$lang_id);
									$this->db->where('a.category_id',$t_cat->category_id);
									$this->db->where('a.status',2);
									$query = $this->db->get();
									$thrd_cat_pro = $query->result_array();

									if ($thrd_cat_pro) {
										foreach ($thrd_cat_pro as $t_cat_pro) {
											array_push($w_cat_pro, $t_cat_pro);
										}
									}
								}
							}
						}
					}
				}
			}

		}

		if ($val == 1) {
			return $pro = $this->Categories->maxValueInArray($w_cat_pro, 'price');
		}else{
			return $pro = $this->Categories->min_by_key($w_cat_pro, 'price');
		}
	}
	//Select max value of product
	public function select_max_value_of_pro($cat_id)
	{
		$this->db->select_max('price');
		$this->db->from('product_information');
		$this->db->where('product_information.category_id',$cat_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();	
		}
		return 0;
	}
	//Select min value of product
	public function select_min_value_of_pro($cat_id)
	{
		$this->db->select_min('price');
		$this->db->from('product_information');
		$this->db->where('product_information.category_id',$cat_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();	
		}
		return 0;
	}
	//Select max value of serch product
	public function select_max_value_of_search_pro($cat_id=null,$search_key=null)
	{
		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$this->db->select_max('a.price');
		$this->db->from('product_information a');
		$this->db->join('product_title c','a.product_id = c.product_id','left');
		if ($cat_id != 'all') {
			$this->db->like('c.title', $search_key, 'both');
			$this->db->where('a.category_id',$cat_id);
		}
		$this->db->where('c.lang_id',$lang_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();	
		}
		return 0;
	}
	//Select min value of serch product
	public function select_min_value_of_search_pro($cat_id=null,$search_key=null)
	{
		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$this->db->select_min('a.price');
		$this->db->from('product_information a');
		$this->db->join('product_title c','a.product_id = c.product_id','left');
		if ($cat_id != 'all') {
			$this->db->like('c.title', $search_key, 'both');
			$this->db->where('a.category_id',$cat_id);
		}
		$this->db->where('c.lang_id',$lang_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();	
		}
		return 0;
	}
	//Select categories product
	public function select_category_product()
	{
		$this->db->select('*');
		$this->db->from('advertisement');
		$this->db->where('add_page','category');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();	
		}
		return false;
	}
	//Select parent category
	public function select_parent_category($cat_id=null){
		$data   = array();
		$result = $this->db->select('*')
						->from('product_category')
						->where('category_id',$cat_id)
						->get()
						->row();

		$data = array(
			'category_id' 	=> $result->category_id,
			'category_name' => $result->category_name,
		);
	
		if ($result->parent_category_id) {
			$result1 = $this->db->select('*')
							->from('product_category')
							->where('category_id',$result->parent_category_id)
							->get()
							->row();
			$data = array(
					'category_id' => $result->parent_category_id,
					'category_name' => $result->category_name,
				);
			if ($result1) {
				$data = array(
					'category_id' => $result1->category_id,
					'category_name' => $result1->category_name,
				);

				$result2 = $this->db->select('*')
							->from('product_category')
							->where('category_id',$result1->parent_category_id)
							->get()
							->row();

				if ($result2) {
					$data = array(
						'category_id' => $result2->category_id,
						'category_name' => $result2->category_name,
					);
				}

			}
		}
		return $data;
	}
	//Select first category
	public function select_first_category($cat_id=null){
		$data   = array();
		$result = $this->db->select('*')
						->from('product_category')
						->where('category_id',$cat_id)
						->get()
						->row();
//dd($cat_id);
		if ($result->parent_category_id) {
			$result1 = $this->db->select('*')
							->from('product_category')
							->where('category_id',$result->parent_category_id)
							->get()
							->row();
			if ($result1) {
				
				$new = $this->db->select('*')
							->from('product_category')
							->where('category_id',$result1->parent_category_id)
							->get()
							->row();
				if ($new) {
					$data = array(
						'category_name' => $new->category_name, 
						'category_id' 	=> $new->category_id, 
					);
					return $data;
				}else{
					return false;
				}
			}
		}
	}
	//Select second category
	public function select_secnd_category($cat_id=null){
		$data   = array();
		$result = $this->db->select('*')
						->from('product_category')
						->where('category_id',$cat_id)
						->get()
						->row();

		if ($result) {
			$new = $this->db->select('*')
						->from('product_category')
						->where('category_id',$result->parent_category_id)
						->get()
						->row();
			if ($new) {
				$data = array(
					'category_name' => $new->category_name, 
					'category_id' 	=> $new->category_id, 
				);
				return $data;
			}
			return false;
		}
	}
	//Select second category
	public function select_third_category($cat_id=null){
		$data   = array();
		$result = $this->db->select('*')
						->from('product_category')
						->where('category_id',$cat_id)
						->get()
						->row();

		if ($result->category_id) {
			$data = array(
				'category_name' => $result->category_name, 
				'category_id' 	=> $result->category_id, 
			);
			return $data;
		}
	}
	//Select no of products by category
	public function select_of_product_by_category($cat_id=null){
		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$price_range = $this->input->get('price');
        $size        = $this->input->get('size');
        $brand       = $this->input->get('brand');
        $sort        = $this->input->get('sort');
        $rate        = $this->input->get('rate');

		$this->db->select('a.*,b.*');
		$this->db->from('product_information a');
		$this->db->join('product_title b','a.product_id = b.product_id','left');
		$this->db->join('brand c','c.brand_id = a.brand_id','left');
		$this->db->where('b.lang_id',$lang_id);
		$this->db->where('a.category_id',$cat_id);
		$this->db->where('a.status',2);
		$this->db->order_by('a.product_info_id','asc');

		if ($price_range) {
			$ex = explode("-", $price_range);
	        $from = $ex[0];
	        $to = $ex[1];
	        $this->db->where('price >=', $from);
			$this->db->where('price <=', $to);
		}

		if ($brand) {
			$this->db->where('a.brand_id', $brand);
		}

		if ($size) {
			$this->db->where('a.variant_id', $size);
		}

		if ($sort) {
			if ($sort == 'new') {
				$this->db->order_by('a.product_info_id','desc');
			}elseif ($sort == 'discount') {
				$this->db->where('on_sale','1');
				$this->db->order_by('offer_price','asc');
			}elseif ($sort == 'low_to_high') {
				$this->db->order_by('a.price','asc');
			}elseif ($sort == 'high_to_low') {
				$this->db->order_by('a.price','desc');
			}
		}

		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return $query->num_rows();	
		}
		return 0;
	}
	//Get total rater by product id
	public function get_total_rater_by_product_id($product_id=null){
		$rater = $this->db->select('rate')
                ->from('product_review')
                ->where('product_id',$product_id)
                ->where('status',1)
                ->get()
                ->num_rows();
        return $rater;
	}
	//Get total rate by product id
	public function get_total_rate_by_product_id($product_id=null){
		$rate = $this->db->select('sum(rate) as rates')
                    ->from('product_review')
                    ->where('product_id',$product_id)
                    ->get()
                    ->row();
        return $rate;
	}
}