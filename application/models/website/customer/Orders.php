<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Orders extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}

	//order List
	public function order_list($per_page=null,$page=null,$order_no=null,$date=null,$order_status=null)
	{
		$customer_id = $this->session->userdata('customer_id');
		$this->db->select('a.*,b.customer_name');
		$this->db->from('order a');
		$this->db->join('customer_information b','b.customer_id = a.customer_id');
		$this->db->where('b.customer_id',$customer_id);

		if ($order_no) {
			$this->db->where('a.id',$order_no);
		}

		if ($order_status) {
			$this->db->where('a.order_status',$order_status);
		}

		if ($date) {
			$a = explode("---", $date);
			if (count($a) == 1) {
				$from_date =  $a[0];
				$this->db->where('a.date', $from_date);
			}else{
				$from_date = $a[0];
				$this->db->where('a.date >=', $from_date);
				$to_date   = $a[1];
				$this->db->where('a.date <=', $to_date);
			}
			
		}
		$this->db->order_by('a.id','desc');
		$this->db->limit($per_page,$page);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();	
		}
		return false;
	}		

	//order Count
	public function order_count($order_no=null,$date=null,$order_status=null)
	{
		$customer_id = $this->session->userdata('customer_id');
		$this->db->select('a.*,b.customer_name');
		$this->db->from('order a');
		$this->db->join('customer_information b','b.customer_id = a.customer_id');
		$this->db->where('b.customer_id',$customer_id);

		if ($order_no) {
			$this->db->where('a.id',$order_no);
		}

		if ($order_status) {
			$this->db->where('a.order_status',$order_status);
		}

		if ($date) {
			$a = explode("---", $date);
			if (count($a) == 1) {
				$from_date =  $a[0];
				$this->db->where('a.date', $from_date);
			}else{
				$from_date = $a[0];
				$this->db->where('a.date >=', $from_date);
				$to_date   = $a[1];
				$this->db->where('a.date <=', $to_date);
			}
			
		}

		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->num_rows();	
		}
		return false;
	}	

	//Order List Count
	public function total_customer_order($customer_id)
	{
		$this->db->select('a.*,b.customer_name');
		$this->db->from('order a');
		$this->db->join('customer_information b','b.customer_id = a.customer_id');
		$this->db->where('b.customer_id',$customer_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->num_rows();	
		}
		return false;
	}

	//Order entry
	public function order_entry()
	{
		//Order information
		$order_id 			= $this->auth->generator(15);
		$quantity 			= $this->input->post('product_quantity');
		$available_quantity = $this->input->post('available_quantity');
		$product_id 		= $this->input->post('product_id');
		$customer_id 		= $this->session->userdata('customer_id');

		//Product existing check
		if ($product_id == null) {
			$this->session->set_userdata(array('error_message'=>display('please_select_product')));
			redirect('corder');
		}

		//Data inserting into order table
		$data=array(
			'order_id'			=>	$order_id,
			'customer_id'		=>	$customer_id,
			'date'				=>	$this->input->post('invoice_date'),
			'total_amount'		=>	$this->input->post('grand_total_price'),
			'details'			=>	$this->input->post('details'),
			'total_discount' 	=> 	$this->input->post('total_discount') + $this->input->post('invoice_discount'),
			'order_discount' 	=> 	$this->input->post('invoice_discount'),
			'paid_amount'		=>	$this->input->post('paid_amount'),
			'affiliate_id'		=>	null,
			'number_product'	=>	null,
			'service_charge'	=>	$this->input->post('service_charge'),
		);
		$this->db->insert('order',$data);


		//Seller order info
		$rate 		= $this->input->post('product_rate');
		$p_id 		= $this->input->post('product_id');
		$total_amount = $this->input->post('total_price');
		$discount 	= $this->input->post('discount');
		$variants 	= $this->input->post('variant_id');
		$seller_ids = $this->input->post('seller_id');

		//Seller order entry
		for ($i=0, $n=count($p_id); $i < $n; $i++) {
			$product_quantity = $quantity[$i];
			$product_rate 	  = $rate[$i];
			$product_id 	  = $p_id[$i];
			$discount_rate    = $discount[$i];
			$total_price      = $total_amount[$i];
			$variant_id       = $variants[$i];
			$seller_id        = $seller_ids[$i];
			
			$seller_order = array(
				'seller_order_id'	=>	$this->auth->generator(15),
				'order_id'			=>	$order_id,
				'seller_id'			=>	$seller_id,
				'customer_id'		=>	$customer_id,
				'product_id'		=>	$product_id,
				'variant_id'		=>	$variant_id,
				'quantity'			=>	$product_quantity,
				'rate'				=>	$product_rate,
				'total_price'       =>	$total_price,
				'discount_per_product' =>	$discount_rate,
			);

			if(!empty($product_id))
			{
				$result = $this->db->select('*')
									->from('seller_order')
									->where('order_id',$order_id)
									->where('product_id',$product_id)
									->where('variant_id',$variant_id)
									->get()
									->num_rows();
				if ($result > 0) {
					$this->db->set('quantity', 'quantity+'.$product_quantity, FALSE);
					$this->db->set('total_price', 'total_price+'.$total_price, FALSE);
					$this->db->where('order_id', $order_id);
					$this->db->where('product_id', $product_id);
					$this->db->where('variant_id', $variant_id);
					$this->db->update('seller_order');
				}else{
					$this->db->insert('seller_order',$seller_order);
				}
			}
		}

		//Tax info
		$cgst = $this->input->post('cgst');
		$sgst = $this->input->post('sgst');
		$igst = $this->input->post('igst');
		$cgst_id = $this->input->post('cgst_id');
		$sgst_id = $this->input->post('sgst_id');
		$igst_id = $this->input->post('igst_id');

		//Tax collection summary for three
		//CGST tax info
		for ($i=0, $n=count($cgst); $i < $n; $i++) {
			$cgst_tax = $cgst[$i];
			$cgst_tax_id = $cgst_id[$i];
			$cgst_summary = array(
				'order_tax_col_id'	=>	$this->auth->generator(15),
				'order_id'		=>	$order_id,
				'tax_amount' 		=> 	$cgst_tax, 
				'tax_id' 			=> 	$cgst_tax_id,
				'date'				=>	$this->input->post('invoice_date'),
			);
			if(!empty($cgst[$i])){
				$result= $this->db->select('*')
							->from('order_tax_col_summary')
							->where('order_id',$order_id)
							->where('tax_id',$cgst_tax_id)
							->get()
							->num_rows();
				if ($result > 0) {
					$this->db->set('tax_amount', 'tax_amount+'.$cgst_tax, FALSE);
					$this->db->where('order_id', $order_id);
					$this->db->where('tax_id', $cgst_tax_id);
					$this->db->update('order_tax_col_summary');
				}else{
					$this->db->insert('order_tax_col_summary',$cgst_summary);
				}
			}
		}

		//SGST tax info
		for ($i=0, $n=count($sgst); $i < $n; $i++) {
			$sgst_tax = $sgst[$i];
			$sgst_tax_id = $sgst_id[$i];
			
			$sgst_summary = array(
				'order_tax_col_id'	=>	$this->auth->generator(15),
				'order_id'			=>	$order_id,
				'tax_amount' 		=> 	$sgst_tax, 
				'tax_id' 			=> 	$sgst_tax_id,
				'date'				=>	$this->input->post('invoice_date'),
			);
			if(!empty($sgst[$i])){
				$result= $this->db->select('*')
							->from('order_tax_col_summary')
							->where('order_id',$order_id)
							->where('tax_id',$sgst_tax_id)
							->get()
							->num_rows();
				if ($result > 0) {
					$this->db->set('tax_amount', 'tax_amount+'.$sgst_tax, FALSE);
					$this->db->where('order_id', $order_id);
					$this->db->where('tax_id', $sgst_tax_id);
					$this->db->update('order_tax_col_summary');
				}else{
					$this->db->insert('order_tax_col_summary',$sgst_summary);
				}
			}
		}

		//IGST tax info
       	for ($i=0, $n=count($igst); $i < $n; $i++) {
			$igst_tax = $igst[$i];
			$igst_tax_id = $igst_id[$i];
			
			$igst_summary = array(
				'order_tax_col_id'	=>	$this->auth->generator(15),
				'order_id'		=>	$order_id,
				'tax_amount' 		=> 	$igst_tax, 
				'tax_id' 			=> 	$igst_tax_id,
				'date'				=>	$this->input->post('invoice_date'),
			);
			if(!empty($igst[$i])){
				$result= $this->db->select('*')
							->from('order_tax_col_summary')
							->where('order_id',$order_id)
							->where('tax_id',$igst_tax_id)
							->get()
							->num_rows();

				if ($result > 0) {
					$this->db->set('tax_amount', 'tax_amount+'.$igst_tax, FALSE);
					$this->db->where('order_id', $order_id);
					$this->db->where('tax_id', $igst_tax_id);
					$this->db->update('order_tax_col_summary');
				}else{
					$this->db->insert('order_tax_col_summary',$igst_summary);
				}
			}
		}
		//Tax collection summary for three

		//Tax collection details for three
		//CGST tax info
		for ($i=0, $n=count($cgst); $i < $n; $i++) {
			$cgst_tax 	 = $cgst[$i];
			$cgst_tax_id = $cgst_id[$i];
			$product_id  = $p_id[$i];
			$variant_id  = $variants[$i];
			$cgst_details = array(
				'order_tax_col_de_id'=>	$this->auth->generator(15),
				'order_id'			=>	$order_id,
				'amount' 			=> 	$cgst_tax, 
				'product_id' 		=> 	$product_id, 
				'tax_id' 			=> 	$cgst_tax_id,
				'variant_id' 		=> 	$variant_id,
				'date'				=>	$this->input->post('invoice_date'),
			);
			if(!empty($cgst[$i])){

				$result= $this->db->select('*')
							->from('order_tax_col_details')
							->where('order_id',$order_id)
							->where('tax_id',$cgst_tax_id)
							->where('product_id',$product_id)
							->where('variant_id',$variant_id)
							->get()
							->num_rows();
				if ($result > 0) {
					$this->db->set('amount', 'amount+'.$cgst_tax, FALSE);
					$this->db->where('order_id', $order_id);
					$this->db->where('tax_id', $cgst_tax_id);
					$this->db->where('variant_id', $variant_id);
					$this->db->update('order_tax_col_details');
				}else{
					$this->db->insert('order_tax_col_details',$cgst_details);
				}
			}
		}

		//SGST tax info
		for ($i=0, $n=count($sgst); $i < $n; $i++) {
			$sgst_tax 	 = $sgst[$i];
			$sgst_tax_id = $sgst_id[$i];
			$product_id  = $p_id[$i];
			$variant_id  = $variants[$i];
			$sgst_summary = array(
				'order_tax_col_de_id'	=>	$this->auth->generator(15),
				'order_id'			=>	$order_id,
				'amount' 			=> 	$sgst_tax, 
				'product_id' 		=> 	$product_id, 
				'tax_id' 			=> 	$sgst_tax_id,
				'variant_id' 		=> 	$variant_id,
				'date'				=>	$this->input->post('invoice_date'),
			);
			if(!empty($sgst[$i])){
				$result= $this->db->select('*')
							->from('order_tax_col_details')
							->where('order_id',$order_id)
							->where('tax_id',$sgst_tax_id)
							->where('product_id',$product_id)
							->where('variant_id',$variant_id)
							->get()
							->num_rows();
				if ($result > 0) {
					$this->db->set('amount', 'amount+'.$sgst_tax, FALSE);
					$this->db->where('order_id', $order_id);
					$this->db->where('tax_id', $sgst_tax_id);
					$this->db->where('variant_id', $variant_id);
					$this->db->update('order_tax_col_details');
				}else{
					$this->db->insert('order_tax_col_details',$sgst_summary);
				}
			}
		}

		//IGST tax info
		for ($i=0, $n=count($igst); $i < $n; $i++) {
			$igst_tax 	 = $igst[$i];
			$igst_tax_id = $igst_id[$i];
			$product_id  = $p_id[$i];
			$variant_id  = $variants[$i];
			$igst_summary = array(
				'order_tax_col_de_id'=>	$this->auth->generator(15),
				'order_id'			=>	$order_id,
				'amount' 			=> 	$igst_tax, 
				'product_id' 		=> 	$product_id, 
				'tax_id' 			=> 	$igst_tax_id,
				'variant_id' 		=> 	$variant_id,
				'date'				=>	$this->input->post('invoice_date'),
			);
			if(!empty($igst[$i])){
				$result= $this->db->select('*')
							->from('order_tax_col_details')
							->where('order_id',$order_id)
							->where('tax_id',$igst_tax_id)
							->where('product_id',$product_id)
							->where('variant_id',$variant_id)
							->get()
							->num_rows();
				if ($result > 0) {
					$this->db->set('amount', 'amount+'.$igst_tax, FALSE);
					$this->db->where('order_id', $order_id);
					$this->db->where('tax_id', $igst_tax_id);
					$this->db->where('variant_id', $variant_id);
					$this->db->update('order_tax_col_details');
				}else{
					$this->db->insert('order_tax_col_details',$igst_summary);
				}
			}
		}
		//Tax collection details for three
		return $order_id;
	}
	//Get Supplier rate of a product
	public function supplier_rate($product_id)
	{
		$this->db->select('supplier_price');
		$this->db->from('product_information');
		$this->db->where(array('product_id' => $product_id)); 
		$query = $this->db->get();
		return $query->result_array();
	
	}
	//Retrieve order html data
	public function retrieve_order_html_data($order_id)
	{
		$lang = $this->db->select('language')
				->from('soft_setting')
				->where('setting_id',1)
				->get()
				->row();

		if ($lang) {
			$language = $lang->language;
		}else{
			$language = 'english';
		}
		$this->db->select('
			a.*,
			b.customer_short_address,
			b.customer_name,
			b.customer_mobile,
			b.customer_email,
			c.*,
			d.product_id,
			d.product_model,d.unit,
			e.unit_short_name,
			f.variant_name,
			g.title as product_name
			');
		$this->db->from('order a');
		$this->db->join('customer_information b','b.customer_id = a.customer_id');
		$this->db->join('seller_order c','c.order_id = a.order_id');
		$this->db->join('product_information d','d.product_id = c.product_id');
		$this->db->join('unit e','e.unit_id = d.unit','left');
		$this->db->join('variant f','f.variant_id = c.variant_id','left');
		$this->db->join('product_title g','g.product_id = c.product_id','left');
		$this->db->where('a.order_id',$order_id);
		$this->db->where('g.lang_id',$language);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();	
		}
		return false;
	}
	//Retrieve company Edit Data
	public function retrieve_company()
	{
		$this->db->select('*');
		$this->db->from('company_information');
		$this->db->limit('1');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();	
		}
		return false;
	}

	//Get total product
	public function get_total_product($product_id){

		$this->db->select('
			product_id,
			price,
			seller_id,
			unit,
			variant_id,
			product_model,
			on_sale,
			offer_price,
			unit.unit_short_name
			');
		$this->db->from('product_information');
		$this->db->join('unit','unit.unit_id = product_information.unit','left');
		$this->db->where(array('product_id' => $product_id,'status' => 1)); 
		$product_information = $this->db->get()->row();

		$html = "";
		if (!empty($product_information->variant_id)) {
			$exploded = explode(',',$product_information->variant_id);
			$html .="<option value=\"\">".display('select_variant')."</option>";
	        foreach ($exploded as $elem) {
		        $this->db->select('*');
		        $this->db->from('variant');
		        $this->db->where('variant_id',$elem);
		        $this->db->order_by('variant_name','asc');
		        $result = $this->db->get()->row();
		        $html .="<option value=".$result->variant_id.">".$result->variant_name."</option>";
	    	}
	    }

		$this->db->select('tax.*,tax_product_service.product_id,tax_percentage');
		$this->db->from('tax_product_service');
		$this->db->join('tax','tax_product_service.tax_id = tax.tax_id','left');
		$this->db->where('tax_product_service.product_id',$product_id);
		$tax_information = $this->db->get()->result();

		//New tax calculation for discount
		if(!empty($tax_information)){
			foreach($tax_information as $k=>$v){
			   if ($v->tax_id == 'H5MQN4NXJBSDX4L') {
			   		$tax['cgst_tax'] 	= ($v->tax_percentage)/100;
			   		$tax['cgst_name']	= $v->tax_name; 
			   		$tax['cgst_id']	 	= $v->tax_id; 
			   }elseif($v->tax_id == '52C2SKCKGQY6Q9J'){
			   		$tax['sgst_tax'] 	= ($v->tax_percentage)/100;
			   		$tax['sgst_name']	= $v->tax_name; 
			   		$tax['sgst_id']	 	= $v->tax_id; 
			   }elseif($v->tax_id == '5SN9PRWPN131T4V'){
			   		$tax['igst_tax'] 	= ($v->tax_percentage)/100;
			   		$tax['igst_name']	= $v->tax_name; 
			   		$tax['igst_id']		= $v->tax_id; 
			   }
			}
		}

		$discount = "";
		if ($product_information->on_sale == 1) {
			$discount = ($product_information->price - $product_information->offer_price);
		}

		$data2 = array(
			'price' 		=> $product_information->price, 
			'variant_id' 	=> $product_information->variant_id, 
			'seller_id' 	=> $product_information->seller_id, 
			'product_model' => $product_information->product_model, 
			'product_id' 	=> $product_information->product_id, 
			'variant' 		=> $html, 
			'discount' 		=> $discount, 
			'sgst_tax' 		=> (!empty($tax['sgst_tax'])?$tax['sgst_tax']:null), 
			'cgst_tax' 		=> (!empty($tax['cgst_tax'])?$tax['cgst_tax']:null), 
			'igst_tax' 		=> (!empty($tax['igst_tax'])?$tax['igst_tax']:null), 
			'cgst_id' 		=> (!empty($tax['cgst_id'])?$tax['cgst_id']:null), 
			'sgst_id' 		=> (!empty($tax['sgst_id'])?$tax['sgst_id']:null), 
			'igst_id' 		=> (!empty($tax['igst_id'])?$tax['igst_id']:null), 
			'unit' 			=> $product_information->unit_short_name, 
			);

		return $data2;
	}
	//Retrieve order Edit Data
	public function retrieve_order_editdata($order_id)
	{

		$lang = $this->db->select('language')
				->from('soft_setting')
				->where('setting_id',1)
				->get()
				->row();

		if ($lang) {
			$language = $lang->language;
		}else{
			$language = 'english';
		}

		$this->db->select('
				a.*,
				b.customer_name,
				c.*,
				c.product_id,
				d.product_model,
				a.status,
				e.title as product_name
			');

		$this->db->from('order a');
		$this->db->join('customer_information b','b.customer_id = a.customer_id');
		$this->db->join('seller_order c','c.order_id = a.order_id');
		$this->db->join('product_information d','d.product_id = c.product_id');
		$this->db->join('product_title e','e.product_id = c.product_id');
		$this->db->where('a.order_id',$order_id);
		$this->db->where('e.lang_id',$language);
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return $query->result_array();	
		}
		return false;
	}
		//Update order
	public function update_order()
	{
		//Order information
		$order_id  	 = $this->input->post('order_id');
		$customer_id = $this->session->userdata('customer_id');

		if($order_id!='')
		{

			//Data update into order table
			$data=array(
				'order_id'			=>	$order_id,
				'customer_id'		=>	$customer_id,
				'date'				=>	$this->input->post('invoice_date'),
				'total_amount'		=>	$this->input->post('grand_total_price'),
				'total_discount' 	=> 	$this->input->post('total_discount'),
				'order_discount' 	=> 	$this->input->post('invoice_discount'),
				'service_charge' 	=> 	$this->input->post('service_charge'),
				'paid_amount'		=>	$this->input->post('paid_amount'),
				'details'			=>	$this->input->post('details'),
				'status'			=>	1,
				'pending'			=>	$this->input->post('invoice_date'),
			);

			$this->db->where('order_id',$order_id);
			$result = $this->db->delete('order');

			if ($result) {
				$this->db->insert('order',$data);
			}
		}

		//Seller order info
		$rate 		= $this->input->post('product_rate');
		$p_id 		= $this->input->post('product_id');
		$total_amount = $this->input->post('total_price');
		$discount 	= $this->input->post('discount');
		$variants 	= $this->input->post('variant_id');
		$seller_ids = $this->input->post('seller_id');
		$quantity 	= $this->input->post('product_quantity');

		//Delete old invoice info
		if (!empty($order_id)) {
			$this->db->where('order_id',$order_id);
			$this->db->delete('seller_order'); 
		}

		//Seller order for entry
		for ($i=0, $n=count($p_id); $i < $n; $i++) {
			$product_quantity = $quantity[$i];
			$product_rate 	  = $rate[$i];
			$product_id 	  = $p_id[$i];
			$discount_rate    = $discount[$i];
			$total_price      = $total_amount[$i];
			$variant_id       = $variants[$i];
			$seller_id        = $seller_ids[$i];
			
			$seller_order = array(
				'order_id'			=>	$order_id,
				'seller_id'			=>	$seller_id,
				'customer_id'		=>	$customer_id,
				'product_id'		=>	$product_id,
				'variant_id'		=>	$variant_id,
				'quantity'			=>	$product_quantity,
				'rate'				=>	$product_rate,
				'total_price'       =>	$total_price,
				'discount_per_product' =>	$discount_rate,
			);

			if(!empty($product_id))
			{
				$result = $this->db->select('*')
									->from('seller_order')
									->where('order_id',$order_id)
									->where('product_id',$product_id)
									->where('variant_id',$variant_id)
									->get()
									->num_rows();
				if ($result > 0) {
					$this->db->set('quantity', 'quantity+'.$product_quantity, FALSE);
					$this->db->set('total_price', 'total_price+'.$total_price, FALSE);
					$this->db->where('order_id', $order_id);
					$this->db->where('product_id', $product_id);
					$this->db->where('variant_id', $variant_id);
					$this->db->update('seller_order');
				}else{
					$this->db->insert('seller_order',$seller_order);
				}
			}
		}

		//Tax info
		$cgst = $this->input->post('cgst');
		$sgst = $this->input->post('sgst');
		$igst = $this->input->post('igst');
		$cgst_id = $this->input->post('cgst_id');
		$sgst_id = $this->input->post('sgst_id');
		$igst_id = $this->input->post('igst_id');

		//Tax collection summary for three

		//Delete all tax  from summary
		$this->db->where('order_id',$order_id);
		$this->db->delete('order_tax_col_summary');

		//CGST Tax Summary
		for ($i=0, $n=count($cgst); $i < $n; $i++) {
			$cgst_tax = $cgst[$i];
			$cgst_tax_id = $cgst_id[$i];
			$cgst_summary = array(
				'order_tax_col_id'	=>	$this->auth->generator(15),
				'order_id'			=>	$order_id,
				'tax_amount' 		=> 	$cgst_tax, 
				'tax_id' 			=> 	$cgst_tax_id,
				'date'				=>	$this->input->post('invoice_date'),
			);
			if(!empty($cgst[$i])){
				$result= $this->db->select('*')
							->from('order_tax_col_summary')
							->where('order_id',$order_id)
							->where('tax_id',$cgst_tax_id)
							->get()
							->num_rows();
				if ($result > 0) {
					$this->db->set('tax_amount', 'tax_amount+'.$cgst_tax, FALSE);
					$this->db->where('order_id', $order_id);
					$this->db->where('tax_id', $cgst_tax_id);
					$this->db->update('order_tax_col_summary');
				}else{
					$this->db->insert('order_tax_col_summary',$cgst_summary);
				}
			}
		}

		//SGST Tax Summary
		for ($i=0, $n=count($sgst); $i < $n; $i++) {
			$sgst_tax = $sgst[$i];
			$sgst_tax_id = $sgst_id[$i];
			
			$sgst_summary = array(
				'order_tax_col_id'	=>	$this->auth->generator(15),
				'order_id'			=>	$order_id,
				'tax_amount' 		=> 	$sgst_tax, 
				'tax_id' 			=> 	$sgst_tax_id,
				'date'				=>	$this->input->post('invoice_date'),
			);
			if(!empty($sgst[$i])){
				$result= $this->db->select('*')
							->from('order_tax_col_summary')
							->where('order_id',$order_id)
							->where('tax_id',$sgst_tax_id)
							->get()
							->num_rows();
				if ($result > 0) {
					$this->db->set('tax_amount', 'tax_amount+'.$sgst_tax, FALSE);
					$this->db->where('order_id', $order_id);
					$this->db->where('tax_id', $sgst_tax_id);
					$this->db->update('order_tax_col_summary');
				}else{
					$this->db->insert('order_tax_col_summary',$sgst_summary);
				}
			}
		}

		//IGST Tax Summary
		for ($i=0, $n=count($igst); $i < $n; $i++) {
			$igst_tax = $igst[$i];
			$igst_tax_id = $igst_id[$i];
			
			$igst_summary = array(
				'order_tax_col_id'	=>	$this->auth->generator(15),
				'order_id'		=>	$order_id,
				'tax_amount' 		=> 	$igst_tax, 
				'tax_id' 			=> 	$igst_tax_id,
				'date'				=>	$this->input->post('invoice_date'),
			);
			if(!empty($igst[$i])){
				$result= $this->db->select('*')
							->from('order_tax_col_summary')
							->where('order_id',$order_id)
							->where('tax_id',$igst_tax_id)
							->get()
							->num_rows();

				if ($result > 0) {
					$this->db->set('tax_amount', 'tax_amount+'.$igst_tax, FALSE);
					$this->db->where('order_id', $order_id);
					$this->db->where('tax_id', $igst_tax_id);
					$this->db->update('order_tax_col_summary');
				}else{
					$this->db->insert('order_tax_col_summary',$igst_summary);
				}
			}
		}
		//Tax collection summary for three

		//Tax collection details for three
		//Delete all tax  from summary
		$this->db->where('order_id',$order_id);
		$this->db->delete('order_tax_col_details');

		//CGST Tax Details
		for ($i=0, $n=count($cgst); $i < $n; $i++) {
			$cgst_tax 	 = $cgst[$i];
			$cgst_tax_id = $cgst_id[$i];
			$product_id  = $p_id[$i];
			$variant_id  = $variants[$i];
			$cgst_details = array(
				'order_tax_col_de_id'=>	$this->auth->generator(15),
				'order_id'			=>	$order_id,
				'amount' 			=> 	$cgst_tax, 
				'product_id' 		=> 	$product_id, 
				'tax_id' 			=> 	$cgst_tax_id,
				'variant_id' 		=> 	$variant_id,
				'date'				=>	$this->input->post('invoice_date'),
			);
			if(!empty($cgst[$i])){
				$result= $this->db->select('*')
							->from('order_tax_col_details')
							->where('order_id',$order_id)
							->where('tax_id',$cgst_tax_id)
							->where('product_id',$product_id)
							->where('variant_id',$variant_id)
							->get()
							->num_rows();
				if ($result > 0) {
					$this->db->set('amount', 'amount+'.$cgst_tax, FALSE);
					$this->db->where('order_id', $order_id);
					$this->db->where('tax_id', $cgst_tax_id);
					$this->db->where('variant_id', $variant_id);
					$this->db->update('order_tax_col_details');
				}else{
					$this->db->insert('order_tax_col_details',$cgst_details);
				}
			}
		}

		//SGST Tax Details
		for ($i=0, $n=count($sgst); $i < $n; $i++) {
			$sgst_tax 	 = $sgst[$i];
			$sgst_tax_id = $sgst_id[$i];
			$product_id  = $p_id[$i];
			$variant_id  = $variants[$i];
			$sgst_summary = array(
				'order_tax_col_de_id'	=>	$this->auth->generator(15),
				'order_id'		=>	$order_id,
				'amount' 			=> 	$sgst_tax, 
				'product_id' 		=> 	$product_id, 
				'tax_id' 			=> 	$sgst_tax_id,
				'variant_id' 		=> 	$variant_id,
				'date'				=>	$this->input->post('invoice_date'),
			);
			if(!empty($sgst[$i])){
				$result= $this->db->select('*')
							->from('order_tax_col_details')
							->where('order_id',$order_id)
							->where('tax_id',$sgst_tax_id)
							->where('product_id',$product_id)
							->where('variant_id',$variant_id)
							->get()
							->num_rows();
				if ($result > 0) {
					$this->db->set('amount', 'amount+'.$sgst_tax, FALSE);
					$this->db->where('order_id', $order_id);
					$this->db->where('tax_id', $sgst_tax_id);
					$this->db->where('variant_id', $variant_id);
					$this->db->update('order_tax_col_details');
				}else{
					$this->db->insert('order_tax_col_details',$sgst_summary);
				}
			}
		}

		//IGST Tax Details
		for ($i=0, $n=count($igst); $i < $n; $i++) {
			$igst_tax 	 = $igst[$i];
			$igst_tax_id = $igst_id[$i];
			$product_id  = $p_id[$i];
			$variant_id  = $variants[$i];
			$igst_summary = array(
				'order_tax_col_de_id'=>	$this->auth->generator(15),
				'order_id'		=>	$order_id,
				'amount' 			=> 	$igst_tax, 
				'product_id' 		=> 	$product_id, 
				'tax_id' 			=> 	$igst_tax_id,
				'variant_id' 		=> 	$variant_id,
				'date'				=>	$this->input->post('invoice_date'),
			);
			if(!empty($igst[$i])){
				$result= $this->db->select('*')
							->from('order_tax_col_details')
							->where('order_id',$order_id)
							->where('tax_id',$igst_tax_id)
							->where('product_id',$product_id)
							->where('variant_id',$variant_id)
							->get()
							->num_rows();
				if ($result > 0) {
					$this->db->set('amount', 'amount+'.$igst_tax, FALSE);
					$this->db->where('order_id', $order_id);
					$this->db->where('tax_id', $igst_tax_id);
					$this->db->where('variant_id', $variant_id);
					$this->db->update('order_tax_col_details');
				}else{
					$this->db->insert('order_tax_col_details',$igst_summary);
				}
			}
		}
		//End tax details
		return $order_id;
	}
	// Delete order Item
	public function delete_order($order_id)
	{	
		//Delete order table
		$this->db->where('order_id',$order_id);
		$this->db->delete('order'); 
		//Delete seller_order table
		$this->db->where('order_id',$order_id);
		$this->db->delete('seller_order'); 
		//Order tax summary delete
		$this->db->where('order_id',$order_id);
		$this->db->delete('order_tax_col_summary'); 
		//Order tax details delete
		$this->db->where('order_id',$order_id);
		$this->db->delete('order_tax_col_details'); 
		return true;
	}	

	// Cancel order Item
	public function order_cancel($order_id)
	{
		//Status change to cancel
		$this->db->set('order_status',6);
		$this->db->where('order_id',$order_id);
		$this->db->update('order');

		//Update product stock
		$seller_order = $this->db->select('*')
				->from('seller_order')
				->where('order_id',$order_id)
				->get()
				->result();

		if ($seller_order) {
			foreach ($seller_order as $product) {
				$this->db->set('quantity', 'quantity+'.$product->quantity,FALSE);
				$this->db->where('product_id', $product->product_id);
				$this->db->where('seller_id', $product->seller_id);
				$this->db->update('product_information');
			}
		}
		return true;
	}

	//NUMBER GENERATOR
	public function number_generator()
	{
		$this->db->select_max('invoice', 'invoice_no');
		$query = $this->db->get('invoice');	
		$result = $query->result_array();	
		$order_no = $result[0]['invoice_no'];
		if ($order_no !='') {
			$order_no = $order_no + 1;	
		}else{
			$order_no = 1000;
		}
		return $order_no;		
	}
	//NUMBER GENERATOR FOR ORDER
	public function number_generator_order()
	{
		$this->db->select_max('order', 'order_no');
		$query = $this->db->get('order');	
		$result = $query->result_array();	
		$order_no = $result[0]['order_no'];
		if ($order_no !='') {
			$order_no = $order_no + 1;	
		}else{
			$order_no = 1000;
		}
		return $order_no;		
	}
	//Order tracking count for admin
	public function order_traking_count_customer($order_id='')
	{
		$order_traking = $this->db->select('*')
							->from('order_tracking')
							->where('order_id',$order_id)
							->where('customer_status',0)
							->get()
							->num_rows();
		return $order_traking;
	}
	//Order traking
	public function order_traking($order_id=null){

		$this->db->set('customer_status',1)
				->where('order_id',$order_id)
				->update('order_tracking');

		$order_traking = $this->db->select('*')
							->from('order_tracking')
							->where('order_id',$order_id)
							->order_by('id')
							->get()
							->result();
		return $order_traking;
	}
	//Get order no
	public function get_order_no($order_id=null){
		$order_no = $this->db->select('*')
							->from('order a')
							->join('customer_information b','a.customer_id = b.customer_id')
							->where('a.order_id',$order_id)
							->get()
							->row();
		return $order_no;
	}
	//Order message
	public function order_message($order_id=null){
		$order_message = $this->db->select('*')
							->from('order_tracking')
							->where('order_id',$order_id)
							->where('order_status',7)
							->order_by('id','desc')
							->limit(1)
							->get()
							->row();
		
		if ($order_message) {
			return $order_message->message;
		}else{
			return false;
		}
	}
	//New order tracking count
	public function new_order_tracking_count()
	{
		$customer_id = $this->session->userdata('customer_id');
		$this->db->select('a.*');
		$this->db->from('order_tracking a');
		$this->db->where('a.customer_id',$customer_id);
		$this->db->where('a.customer_status','0');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->num_rows();	
		}
		return false;
	}
} 
