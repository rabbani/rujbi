<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sellers extends CI_Model {

	private $table  = "language";
    private $phrase = "phrase";

	public function __construct()
	{
		parent::__construct();
	}
	//Count seller
	public function count_seller()
	{
		$this->db->select('*');
		$this->db->from('seller_information');
		$this->db->where('status',1);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->num_rows();	
		}
		return false;
	}
	//Seller List
	public function seller_list($per_page=null,$page=null,$mobile=null,$email=null,$business_name=null)
	{
		$this->db->select('*');
		$this->db->from('seller_information');

		if ($mobile) {
			$this->db->where('seller_information.mobile',$mobile);
		}

		if ($email) {
			$this->db->where('seller_information.email',$email);
		}

		if ($business_name) {
			$this->db->where('seller_information.business_name',$business_name);
		}

		$this->db->limit($per_page,$page);
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();	
		}
		return false;
	}	
	//Seller Count
	public function seller_count($mobile=null,$email=null,$business_name=null)
	{
		$this->db->select('*');
		$this->db->from('seller_information');

		if ($mobile) {
			$this->db->where('seller_information.mobile',$mobile);
		}

		if ($email) {
			$this->db->where('seller_information.email',$email);
		}

		if ($business_name) {
			$this->db->where('seller_information.business_name',$business_name);
		}

		$this->db->order_by('id','desc');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->num_rows();	
		}
		return false;
	}	
	//Pending Seller Count
	public function total_pending_seller()
	{
		$this->db->select('*');
		$this->db->from('seller_information');
		$this->db->where('status',2);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->num_rows();	
		}
		return false;
	}
	//Seller Entry
	public function seller_entry($data)
	{
		$this->db->select('*');
		$this->db->from('seller_information');
		$this->db->where('email',$data['email']);

		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return FALSE;
		}else{
			$this->db->insert('seller_information',$data);
			return TRUE;
		}
	}
	//Retrieve seller Edit Data
	public function retrieve_seller_editdata($seller_id)
	{
		$this->db->select('*');
		$this->db->from('seller_information');
		$this->db->where('id',$seller_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();	
		}
		return false;
	}
	//Retrieve seller Search Data
	public function seller_search_item($seller_id)
	{
		$this->db->select('*');
		$this->db->from('seller_information');
		$this->db->where('id',$seller_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();	
		}
		return false;
	}
	//Update Seller
	public function update_seller($data,$id)
	{
		$this->db->select('*');
		$this->db->from('seller_information');
		$this->db->where('seller_store_name',$data['seller_store_name']);
		$q = $this->db->get();


		if ($q->num_rows() > 0) {
			$query = $this->db->set('first_name',$data['first_name'])
					->set('last_name',$data['last_name'])
					->set('email',$data['email'])
					->set('password',$data['password'])
					->set('address',$data['address'])
					->set('mobile',$data['mobile'])
					->set('business_name',$data['business_name'])
					->set('identification_doc_no',$data['identification_doc_no'])
					->set('identification_type',$data['identification_type'])
					->set('affiliate_id',$data['affiliate_id'])
					->set('status',$data['status'])
					->where('id',$id)
					->update('seller_information');
			return true;
		}else{
			$this->db->where('id',$id);
			$this->db->update('seller_information',$data);
			return true;
		}
	}
	// Delete seller Item
	public function delete_seller($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('seller_information');	
		return true;
	}
	//Select seller policy
	public function select_seller_policy(){
		$result = $this->db->select('*')
						->from('seller_policy')
						->where('id',1)
						->get()
						->row();
		return $result;
	}

	public function languages()
    { 
        if ($this->db->table_exists($this->table)) { 

            $fields = $this->db->field_data($this->table);

            $i = 1;
            foreach ($fields as $field)
            {  
                if ($i++ > 2)
                $result[$field->name] = ucfirst($field->name);
            }

            if (!empty($result)) return $result;


        } else {
            return false; 
        }
    }
}


