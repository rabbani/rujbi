<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Accounts extends CI_Model {
  
    public function __construct(){
        parent::__construct();
    }
    //Data entry to payment table
    public function payment_entry($data){
        $result = $this->db->insert('payment',$data);
        if ($result) {
            return true;
        }else{
            return false;
        }
    }   
    //All Payment list
    public function payment_list($per_page=null,$page=null,$seller_id=null,$date=null){
        $this->db->select('a.*,b.first_name,b.last_name');
        $this->db->from('payment a');
        $this->db->join('seller_information b','a.seller_id = b.seller_id');

        if ($seller_id) {
            $this->db->where('a.seller_id', $seller_id);
        }

        if ($date) {
            $a = explode("---", $date);
            if (count($a) == 1) {
                $from_date =  $a[0];
                $this->db->where('a.date', $from_date);
            }else{
                $from_date = $a[0];
                $this->db->where('a.date >=', $from_date);
                $to_date   = $a[1];
                $this->db->where('a.date <=', $to_date);
            }
        }
        $this->db->limit($per_page,$page);
        $query = $this->db->get();
        $result = $query->result();
        if ($result) {
            return $result;
        }else{
            return false;
        }
    }   
    //Payment list count
    public function manage_payment_count($seller_id=null,$date=null){
        $result = $this->db->select('a.*,b.first_name,b.last_name')
                        ->from('payment a')
                        ->join('seller_information b','a.seller_id = b.seller_id')
                        ->get()
                        ->num_rows();
        if ($result) {
            return $result;
        }else{
            return false;
        }
    }   
    //Retrive payment edit data
    public function retrive_payment_edit_data($id=null){
        $result = $this->db->select('a.*,b.first_name,b.last_name')
                        ->from('payment a')
                        ->join('seller_information b','a.seller_id = b.seller_id')
                        ->where('a.id',$id)
                        ->get()
                        ->row();
        if ($result) {
            return $result;
        }else{
            return false;
        }
    }    
    //Update Payment
    public function update_payment($data=null,$id=null){
        $result = $this->db->where('id',$id)
                        ->update('payment',$data);
        if ($result) {
            return $result;
        }else{
            return false;
        }
    }
    //Seller List
    public function seller_list($per_page=null,$page=null,$seller_id=null)
    {
        $this->db->select('*');
        $this->db->from('seller_information');
        if ($seller_id) {
            $this->db->where('seller_id',$seller_id);
        }
        $this->db->limit($per_page,$page);
        $this->db->order_by('id','desc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();  
        }
        return false;
    }     
    //Seller List Count
    public function seller_list_count($seller_id=null)
    {
        $this->db->select('*');
        $this->db->from('seller_information');
        if ($seller_id) {
            $this->db->where('seller_id',$seller_id);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();  
        }
        return false;
    }

    //Seller List
    public function seller_list_by_id($seller_id=null)
    {
        $this->db->select('*');
        $this->db->from('seller_information');
        if ($seller_id) {
            $this->db->where('seller_id',$seller_id);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }


    //Total Balance of seller
    public function total_balance($seller_id=null){
        
        $this->db->select('
                        SUM((quantity * rate)-(quantity * discount_per_product)) as total_price
                        ');
        $this->db->from('order a');
        $this->db->join('seller_order b','a.order_id = b.order_id');
        $this->db->join('seller_information c','b.seller_id = c.seller_id');
        $this->db->where('a.delivered !=',null);
        $this->db->where('c.seller_id',$seller_id);
        $this->db->order_by('a.id','desc');
        $query = $this->db->get();
        return $query->row()->total_price;
    }
    //Total Comission of seller
    public function total_comission($seller_id=null){

        $this->db->select('
                        SUM((((quantity * rate)-(quantity * discount_per_product)) * seller_percentage)/100) as total_comission
                        ');
        $this->db->from('order a');
        $this->db->join('seller_order b','a.order_id = b.order_id');
        $this->db->join('seller_information c','b.seller_id = c.seller_id');
        $this->db->where('a.delivered !=',null);
        $this->db->where('c.seller_id',$seller_id);
        $this->db->order_by('a.id','desc');
        $query = $this->db->get();
        return $query->row()->total_comission;
    }    
    //Total paid amount of seller
    public function total_paid_amount($seller_id=null){

        $this->db->select('
                        SUM(amount) as total_paid_amount
                        ');
        $this->db->from('payment a');
        $this->db->where('seller_id',$seller_id);
        $query = $this->db->get();
        return $query->row()->total_paid_amount;
    }
}