<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Payment_methods extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}
	//Count payment method
	public function payment_method_entry($data)
	{
		$this->db->select('*');
		$this->db->from('payment_method');
		$this->db->where('position',$data['position']);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return FALSE;
		}else{
			$this->db->insert('payment_method',$data);
			return TRUE;
		}
	}
	//Payment method List
	public function payment_method_list()
	{
		$this->db->select('*');
		$this->db->from('payment_method');
		$this->db->order_by('position');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();	
		}
		return false;
	}
	//Payment method Search Item
	public function payment_method_search_item($method_id)
	{
		$this->db->select('*');
		$this->db->from('payment_method');
		$this->db->where('method_id',$method_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();	
		}
		return false;
	}
	//Retrieve payment method Edit Data
	public function retrieve_payment_method_editdata($method_id)
	{
		$this->db->select('*');
		$this->db->from('payment_method');
		$this->db->where('method_id',$method_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();	
		}
		return false;
	}
	
	//Update payment method
	public function update_payment_method($data,$method_id)
	{

		$this->db->select('*');
		$this->db->from('payment_method');
		$this->db->where('position',$data['position']);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$this->db->set('method_name',$data['method_name']);
			$this->db->set('details',$data['details']);
			$this->db->where('method_id',$method_id);
			$this->db->update('payment_method');
			return FALSE;
		}else{
			$this->db->where('method_id',$method_id);
			$result = $this->db->update('payment_method',$data);
			return TRUE;
		}
	}
	// Delete payment method Item
	public function delete_payment_method($method_id)
	{
		$this->db->where('method_id',$method_id);
		$this->db->delete('payment_method'); 	
		return true;
	}
}