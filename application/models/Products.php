<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Products extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}
	//Count Product
	public function count_product()
	{
		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$this->db->select('
				a.*,
				b.category_name,
				c.first_name,
				c.last_name,
				c.seller_id,
				c.email,
				d.brand_name,
				e.title
			');
		$this->db->from('product_information a');
		$this->db->join('product_category b','a.category_id = b.category_id','left');
		$this->db->join('seller_information c','a.seller_id = c.seller_id','left');
		$this->db->join('brand d','a.brand_id = d.brand_id','left');
		$this->db->join('product_title e','e.product_id = a.product_id','left');
		$this->db->where('e.lang_id',$lang_id);

		$this->db->where('a.status',2);
		$query = $this->db->get();
		return $query->num_rows();
	}
	//All Product List
	public function all_product_list()
	{
		$query=$this->db->select('
				a.*,
				b.first_name,
				b.last_name,
				c.category_name,
				d.unit_short_name,
		
			')
			->from('product_information a')
			->join('seller_information b', 'a.seller_id = b.id','left')
			->join('product_category c','c.category_id = a.category_id','left')
			->join('unit d','d.unit_id = a.unit','left')
			->where('a.status',1)
			->get();
		if ($query->num_rows() > 0) {
		 	return $query->result_array();	
		}
		return false;
	}	
	//Product generator id check 
	public function product_id_check($product_id){
		$query=$this->db->select('*')
				->from('product_information')
				->where('product_id',$product_id)
				->get();
		if ($query->num_rows() > 0) {
		 	return true;	
		}else{
			return false;
		}
	}
	//Retrieve company Edit Data
	public function retrieve_company()
	{
		$this->db->select('*');
		$this->db->from('company_information');
		$this->db->limit('1');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();	
		}
		return false;
	}
	// Delete Product Item
	public function delete_product($product_id)
	{
		$this->db->where('product_id',$product_id);
		$this->db->delete('product_information'); 
		$this->session->set_userdata(array('message'=>display('successfully_delete')));

		$this->db->select('*');
		$this->db->from('product_information');
		$this->db->where('status',1);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			$json_product[] = array('label'=>$row->product_name."-(".$row->product_model.")",'value'=>$row->product_id);
		}
		$cache_file = './my-assets/js/admin_js/json/product.json';
		$productList = json_encode($json_product);
		file_put_contents($cache_file,$productList);
		redirect('cproduct/manage_product/all/item');
	}
	//Product By Search 
	public function product_search_item($product_id)
	{
		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$this->db->select('
				a.*,
				b.category_name,
				c.first_name,
				c.last_name,
				c.seller_id,
				c.email,
				d.brand_name,
				e.title
			');
		$this->db->from('product_information a');
		$this->db->join('product_category b','a.category_id = b.category_id','left');
		$this->db->join('seller_information c','a.seller_id = c.seller_id','left');
		$this->db->join('brand d','a.brand_id = d.brand_id','left');
		$this->db->join('product_title e','e.product_id = a.product_id','left');
		$this->db->where('e.lang_id',$lang_id);
		$this->db->where('a.status',2);
		$this->db->where('a.product_id',$product_id);
		$query = $this->db->get();
		return $query->result_array();
	}
	//Duplicate Entry Checking 
	public function product_model_search($product_model)
	{
		$this->db->select('*');
		$this->db->from('product_information');
		$this->db->where('product_model',$product_model);
		$query = $this->db->get();
		return $this->db->affected_rows();
	}
	//Product Details
	public function product_details_info($product_id)
	{
		$this->db->select('*');
		$this->db->from('product_information');
		$this->db->where('product_id',$product_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();	
		}
		return false;
	}
	// Invoice Data for specific data
	public function invoice_data($product_id)
	{
		$this->db->select('
			a.*,
			b.*,
			sum(b.total_price)  - (b.discount * b.quantity) as total_price,
			c.customer_name
			');
		$this->db->from('invoice a');
		$this->db->join('invoice_details b','b.invoice_id = a.invoice_id');
		$this->db->join('customer_information c','c.customer_id = a.customer_id');
		$this->db->where('b.product_id',$product_id);
		$this->db->group_by('b.invoice_id');
		$this->db->order_by('a.invoice_id','asc');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();	
		}
		return false;
	}
	#==========================================#
	#=========== Approved Product =============#
	#==========================================#

	//Product List
	public function manage_product($per_page=null,$page=null,$seller=null,
                                   $model=null,$title=null,$category=null){

		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$this->db->select('
				a.*,
				b.category_name,
				c.first_name,
				c.last_name,
				c.seller_id,
				c.email,
				d.brand_name,
				e.title
			');
		$this->db->from('product_information a');
		$this->db->join('product_category b','a.category_id = b.category_id','left');
		$this->db->join('seller_information c','a.seller_id = c.seller_id','left');
		$this->db->join('brand d','a.brand_id = d.brand_id','left');
		$this->db->join('product_title e','e.product_id = a.product_id','left');
		$this->db->where('e.lang_id',$lang_id);

		if ($seller) {
			$this->db->where('a.seller_id',$seller);
		}

		if ($model) {
			$this->db->like('a.product_model', $model, 'both');
		}

		if ($title) {
			$this->db->like('e.title', $title, 'both'); 
		}

		if ($category) {
			$this->db->where('a.category_id',$category);
		}

		$this->db->where('a.status',2);
		$this->db->order_by('product_info_id','desc');
		$this->db->limit($per_page,$page);
		$query = $this->db->get();
		return $query->result_array();
	}	
	//Product Count
	public function product_count($seller=null,$model=null,$title=null,$category=null){

		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$this->db->select('
				a.*,
				b.category_name,
				c.first_name,
				c.last_name,
				c.seller_id,
				c.email,
				d.brand_name,
				e.title
			');
		$this->db->from('product_information a');
		$this->db->join('product_category b','a.category_id = b.category_id','left');
		$this->db->join('seller_information c','a.seller_id = c.seller_id','left');
		$this->db->join('brand d','a.brand_id = d.brand_id','left');
		$this->db->join('product_title e','e.product_id = a.product_id','left');
		$this->db->where('e.lang_id',$lang_id);

		if ($seller) {
			$this->db->where('a.seller_id',$seller);
		}

		if ($model) {
			$this->db->like('a.product_model', $model, 'both');
		}

		if ($title) {
			$this->db->like('e.title', $title, 'both'); 
		}

		if ($category) {
			$this->db->where('a.category_id',$category);
		}
		$this->db->where('a.status',2);
		$this->db->order_by('product_info_id','desc');
		$query = $this->db->get();
		return $query->num_rows();
	}
	//Product Image
	public function insert_product_image($data)
	{
		if ($data['status'] == 1) {
			$result= $this->db->select('*')
					->from('product_image')
					->where('status',1)
					->where('product_id',$data['product_id'])
					->where('image_type',1)
					->get()
					->num_rows();
			if ($result > 0) {
				return FALSE;
			}else{

				$this->db->set('thumb_image_url',$data['image_path'])
						->where('product_id',$data['product_id'])
						->update('product_information');

				$this->db->insert('product_image',$data);
				return TRUE;
			}
		}else{
			$this->db->insert('product_image',$data);
			return TRUE;
		}
	}
	//Primary image existing check
	public function primary_image_exist_check($product_id=null){
		$result= $this->db->select('*')
					->from('product_image')
					->where('status',1)
					->where('product_id',$product_id)
					->where('image_type',1)
					->get()
					->num_rows();
		if ($result > 0) {
			return 0;
		}else{
			return 1;
		}
	}	
	//Primary image existing check for upload image
	public function primary_image_exist_check_upload($upload_id=null){
		$result= $this->db->select('*')
					->from('upload_image')
					->where('status',1)
					->where('upload_id',$upload_id)
					->where('image_type',1)
					->get()
					->num_rows();
		if ($result > 0) {
			return 0;
		}else{
			return 1;
		}
	}
	//Make image primary
	public function make_img_primary($image_id=null,$product_id=null,$image_path=null){
		$result = $this->db->select('*')
				->from('product_image')
				->where('product_id',$product_id)
				->where('status',1)
				->where('image_type',1)
				->get()
				->row();
		if ($result) {
			$this->db->set('status',0)
					->where('product_image_id',$result->product_image_id)
					->update('product_image');

			$this->db->set('status',1)
					->where('product_image_id',$image_id)
					->update('product_image');
		}else{
			$this->db->set('status',1)
					->where('product_image_id',$image_id)
					->update('product_image');
		}
		$this->db->set('thumb_image_url',$image_path)
			->where('product_id',$product_id)
			->update('product_information');
		return true;
	}
	//Insert product title
	public function insert_product_title($data)
	{
		$result = $this->db->select('*')
					->from('product_title')
					->where('product_id',$data['product_id'])
					->where('lang_id',$data['lang_id'])
					->get()
					->num_rows();

		if ($result > 0) {
			return false;
		}else{
			$this->db->insert('product_title',$data);
			return TRUE;
		}
	}
	//Seller Product Description Entry
	public function insert_product_description($data)
	{
		$result = $this->db->select('*')
						->from('product_description')
						->where('product_id',$data['product_id'])
						->where('lang_id',$data['lang_id'])
						->where('description_type',$data['description_type'])
						->get()
						->num_rows();

		if ($result > 0) {
			return false;
		}else{
			$this->db->insert('product_description',$data);
			return TRUE;
		}
	}
	//Retrieve Product Edit Data
	public function retrive_product_edit_data($product_id=null)
	{
		$this->db->select('
				a.*,
				b.category_name,
				c.first_name,
				c.last_name,
				c.id as seller_uni_id,
				c.seller_id,
				d.brand_name,
				d.brand_id
			');
		$this->db->from('product_information a');
		$this->db->join('product_category b','a.category_id = b.category_id','left');
		$this->db->join('seller_information c','a.seller_id = c.seller_id','left');
		$this->db->join('brand d','a.brand_id = d.brand_id','left');
		$this->db->where('product_id',$product_id);
		$query = $this->db->get();
		return $query->row();
	}
	//Retrieve seller image edit data
	public function retrive_product_image_edit_data($product_id=null)
	{
		$this->db->select('*');
		$this->db->from('product_image');
		$this->db->where('product_id',$product_id);
		$this->db->where('image_type',1);
		$query = $this->db->get();
		return $query->result_array();
	}
	//Retrieve title
	public function retrive_product_title($product_id=null,$lang_id=null)
	{
		$this->db->select('*');
		$this->db->from('product_title');
		$this->db->where('product_id',$product_id);
		$this->db->where('lang_id',$lang_id);
		$query = $this->db->get();
		return $query->row();
	}
	//Retrieve description
	public function retrive_product_description($product_id=null,$lang_id=null)
	{
		$this->db->select('*');
		$this->db->from('product_description');
		$this->db->where('product_id',$product_id);
		$this->db->where('lang_id',$lang_id);
		$this->db->where('description_type',1);
		$query = $this->db->get();
		return $query->row();
	}
	//Retrieve specification
	public function retrive_product_specification($product_id=null,$lang_id=null)
	{
		$this->db->select('*');
		$this->db->from('product_description');
		$this->db->where('product_id',$product_id);
		$this->db->where('lang_id',$lang_id);
		$this->db->where('description_type',2);
		$query = $this->db->get();
		return $query->row();
	}
	//Seller product update
	public function seller_product_update($data=null,$upload_id=null,$product_id=null){

		if ($data['status'] == 2) {

			$product = $this->db->select('*')
					->from('product_information')
					->where('product_id',$product_id)
					->get()
					->num_rows();

			if ($product > 0) {

				$product_info =array(
					'product_id' 	=> $product_id,
					'seller_id' 	=> $data['seller_id'],
					'category_id' 	=> $data['category_id'],
					'price' 		=> $data['price'],
					'quantity' 		=> $data['quantity'],
					'product_model' => $data['product_model'],
					'variant_id' 	=> $data['variant'],
					'unit' 			=> $data['unit'],
					'brand_id' 		=> $data['product_brand'],
					'product_type' 	=> $data['product_type'],
					'tag' 			=> $data['tag'],
					'status' 		=> 1,
					'approved_date' => date("Y-m-d h:i:s"),
					'update_date' 	=> date("Y-m-d h:i:s"),
				);

				$this->db->where('product_id',$product_id);
				$this->db->update('product_information',$product_info);

				//Product information update
				$this->product_image_update($upload_id,$product_id);
				$this->product_title_update($upload_id,$product_id);
				$this->product_description_update($upload_id,$product_id);
				$this->product_image_url_update($upload_id,$product_id);
				//Product information update json
				$this->create_product_json();
			}else{

				$product_info =array(
					'product_id' 	=> $product_id,
					'seller_id' 	=> $data['seller_id'],
					'category_id' 	=> $data['category_id'],
					'price' 		=> $data['price'],
					'quantity' 		=> $data['quantity'],
					'product_model' => $data['product_model'],
					'variant_id' 	=> $data['variant'],
					'unit' 			=> $data['unit'],
					'brand_id' 		=> $data['product_brand'],
					'product_type' 	=> $data['product_type'],
					'tag' 			=> $data['tag'],
					'status' 		=> 1,
					'approved_date' => date("Y-m-d h:i:s"),
				);
				$this->db->insert('product_information',$product_info);
				$this->product_image_update($upload_id,$product_id);

				//Product information update
				$this->product_image_update($upload_id,$product_id);
				$this->product_title_update($upload_id,$product_id);
				$this->product_description_update($upload_id,$product_id);
				$this->product_image_url_update($upload_id,$product_id);

				//Product information update json
				$this->create_product_json();
			}
			return true;
		}else{
			$product_info =array(
				'product_id' 	=> $product_id,
				'seller_id' 	=> $data['seller_id'],
				'category_id' 	=> $data['category_id'],
				'price' 		=> $data['price'],
				'quantity' 		=> $data['quantity'],
				'product_model' => $data['product_model'],
				'variant_id' 	=> $data['variant'],
				'unit' 			=> $data['unit'],
				'brand_id' 		=> $data['product_brand'],
				'product_type' 	=> $data['product_type'],
				'tag' 			=> $data['tag'],
				'status'		=> 0,
				'approved_date' => date("Y-m-d h:i:s"),
				'update_date' 	=> date("Y-m-d h:i:s"),
			);

			$this->db->where('product_id',$product_id);
			$result = $this->db->update('product_information',$product_info);

			//Product information update json
			$this->create_product_json();
			return true;
		}
	}
	//Insert product information
	public function product_information_update($product_status=null,$upload_id=null,$product_id=null){
		$upload_product = $this->db->select('*')
				->from('upload_product')
				->where('product_id',$product_id)
				->get()
				->row();

		if ($upload_product) {

			if ($product_status == 1) {
				$this->db->set('status',1)
					->where('product_id',$product_id)
					->update('upload_product');

				$this->db->set('status',1)
					->where('product_id',$product_id)
					->update('product_information');
			}elseif ($product_status == 2) {
				$this->db->set('status',2)
					->where('product_id',$product_id)
					->update('upload_product');

				$data = array(
					'product_id' => $product_id,
					'seller_id'  => $upload_product->seller_id,
					'category_id'=> $upload_product->category_id,
					'price'  	 => $upload_product->price,
					'unit'  	 => $upload_product->unit,
					'product_model'  => $upload_product->product_model,
					'brand_id'   => $upload_product->brand_id,
					'variant_id' => $upload_product->variant_id,
					'product_type'=> $upload_product->product_type,
					'tag'        => $upload_product->tag,
					'quantity'   => $upload_product->quantity,
					'best_sale'   => $upload_product->best_sale,
					'on_promotion'=> $upload_product->on_promotion,
					'pre_order'   => $upload_product->pre_order,
					'pre_order_quantity'   => $upload_product->pre_order_quantity,
					'details'    => $upload_product->details,
					'approved_date' => date("Y-m-d h:i:s"),
					'update_date' 	=> date("Y-m-d h:i:s"),
					'status'  	 => 2
				);

				$product = $this->db->select('*')
						->from('product_information')
						->where('product_id',$product_id)
						->get()
						->num_rows();

				if ($product > 0) {
					$this->db->set('seller_id',$upload_product->seller_id)
							->set('category_id',$upload_product->category_id)
							->set('price',$upload_product->price)
							->set('unit',$upload_product->unit)
							->set('product_model',$upload_product->product_model)
							->set('brand_id',$upload_product->brand_id)
							->set('variant_id',$upload_product->variant_id)
							->set('product_type',$upload_product->product_type)
							->set('tag',$upload_product->tag)
							->set('quantity','quantity+'.$upload_product->quantity,FALSE)
							->set('best_sale',$upload_product->best_sale)
							->set('on_promotion',$upload_product->on_promotion)
							->set('pre_order',$upload_product->pre_order)
							->set('pre_order_quantity',$upload_product->pre_order_quantity)
							->set('details',$upload_product->details)
							->set('approved_date',date("Y-m-d h:i:s"))
							->set('update_date',date("Y-m-d h:i:s"))
							->set('status',2)
							->where('product_id',$product_id)
							->update('product_information');
				}else{
					$this->db->insert('product_information',$data);
				}

				//Product information update
				$this->product_image_update($upload_id,$product_id);
				$this->product_title_update($upload_id,$product_id);
				$this->product_description_update($upload_id,$product_id);
				$this->product_image_url_update($upload_id,$product_id);
				
				//Product information update json
				$this->create_product_json();

				return true;
			}else{
				$this->db->set('status',3)
					->where('product_id',$product_id)
					->update('upload_product');

				$this->db->set('status',3)
					->where('product_id',$product_id)
					->update('product_information');
			}
		}
		return true;
	}
	//Insert Product Image
	public function product_image_update($upload_id,$product_id){

		$upload_image = $this->db->select('*')
				->from('upload_image')
				->where('upload_id',$upload_id)
				->get()
				->result();

		if ($upload_image) {
			$image_url = 0;
			foreach ($upload_image as $image) {
				$url = $image->image_url;
				$last = explode('/', $url);

				if ($image->image_type == 1) {
					if (file_exists('my-assets/image/product/temp/thumb/'.end($last))) {
						copy('my-assets/image/product/temp/thumb/'.end($last), 'my-assets/image/product/website/thumb/'.end($last));
						$image_url = 'my-assets/image/product/website/thumb/'.end($last);
					}
				}else{
					if (file_exists('my-assets/image/product/temp/'.end($last))) {
						copy('my-assets/image/product/temp/'.end($last), 'my-assets/image/product/website/'.end($last));
						$image_url = 'my-assets/image/product/website/'.end($last);
					}
				}

				$data = array(
					'product_id' 	=> $product_id, 
					'image_name' 	=> $image->image_name, 
					'image_type' 	=> $image->image_type, 
					'image_path' 	=> $image_url, 
					'upload_date'	=> date("Y-m-d h:i:s"), 
					'size' 			=> $image->image_size, 
					'status' 		=> $image->status, 
				);

				$result = $this->db->select('*')
						->from('product_image')
						->where('product_id',$product_id)
						->where('image_path',$image_url)
						->where('image_type',$image->image_type)
						->where('image_name',$image->image_name)
						->get()
						->num_rows();
				if ($result > 0) {
					return false;
				}else{
					$this->db->insert('product_image',$data);
				}
			}
		}
		return true;
	}	
	//Product image url update
	public function product_image_url_update($upload_id,$product_id){

		$upload_image = $this->db->select('*')
				->from('product_image')
				->where('product_id',$product_id)
				->where('status',1)
				->get()
				->row();

		if ($upload_image) {

			$this->db->set('thumb_image_url',$upload_image->image_path)
				->where('product_id',$product_id)
				->update('product_information');
		}
		return true;
	}
	//Insert Product Title
	public function product_title_update($upload_id,$product_id){
		$upload_title = $this->db->select('*')
				->from('upload_product_title')
				->where('upload_id',$upload_id)
				->get()
				->result();

		if ($upload_title) {
			foreach ($upload_title as $u_title) {
				$product_title = $this->db->select('*')
							->from('product_title')
							->where('product_id',$product_id)
							->where('lang_id',$u_title->lang_id)
							->get()
							->num_rows();

				if ($product_title > 0) {
					$this->db->set('status',1);
					$this->db->set('title',$u_title->title);
					$this->db->where('product_id',$product_id);
					$this->db->where('lang_id',$u_title->lang_id);
					$this->db->update('product_title');
				}else{
					$data = array(
						'product_id'=> $product_id, 
						'lang_id' 	=> $u_title->lang_id, 
						'title' 	=> $u_title->title,
						'status' 	=> 1, 
					);
					$this->db->insert('product_title',$data);
				}
			}
		}
		return true;
	}
	//Insert Product Description
	public function product_description_update($upload_id,$product_id){
		$upload_description = $this->db->select('*')
				->from('upload_product_description')
				->where('upload_id',$upload_id)
				->get()
				->result();

		if ($upload_description) {
			foreach ($upload_description as $p_description) {
				$product_description = $this->db->select('*')
							->from('product_description')
							->where('product_id',$product_id)
							->where('lang_id',$p_description->lang_id)
							->where('description_type',$p_description->description_type)
							->get()
							->num_rows();

				if ($product_description > 0) {
					$this->db->set('status',1);
					$this->db->set('description',$p_description->description);
					$this->db->where('product_id',$product_id);
					$this->db->where('lang_id',$p_description->lang_id);
					$this->db->where('description_type',$p_description->description_type);
					$this->db->update('product_description');
				}else{
					$data = array(
						'product_id' 	=> $product_id, 
						'lang_id' 		=> $p_description->lang_id, 
						'description' 	=> $p_description->description,
						'description_type' 	=> $p_description->description_type,
						'status' 		=> 1, 
					);
					$this->db->insert('product_description',$data);
				}
			}
		}
		return true;
	}
	//Create product json
	public function create_product_json(){

		$lang = $this->db->select('language')
				->from('soft_setting')
				->where('setting_id',1)
				->get()
				->row();

		if ($lang) {
			$language = $lang->language;
		}else{
			$language = 'english';
		}

		$this->db->select('a.*,b.*');
		$this->db->from('product_information a');
		$this->db->join('product_title b','a.product_id = b.product_id');
		$this->db->where('b.lang_id',$language);
		$this->db->where('a.status',2);
		$this->db->order_by('b.title','asc');
		$query = $this->db->get();

		if ($query->result()) {
			foreach ($query->result() as $row) {
				$json_seller[] = array('label'=>$row->title.'('.$row->product_model.')','value'=>$row->product_id);
			}
			$cache_file = './my-assets/js/admin_js/json/product.json';
			$productList = json_encode($json_seller);
			file_put_contents($cache_file,$productList);
		}
		return true;
	}
	//Retrive seller product image
	public function retrive_seller_product_image_edit_data($upload_image_id){
		$this->db->select('
				a.*
			');
		$this->db->from('upload_image a');
		$this->db->where('upload_image_id',$upload_image_id);
		
		$query = $this->db->get();
		return $query->row();
	}
	//Seller product update
	public function update_product_image($data,$upload_image_id){
		$this->db->where('upload_image_id',$upload_image_id);
		$result = $this->db->update('upload_image',$data);

		if ($result) {
			return true;
		}else{
			return false;
		}
	}

	#==========================================#
	#=========== Pending Product ==============#
	#==========================================#

	//Pending Product List
	public function pending_product($per_page=null,$page=null,$seller=null,$model=null,$title=null,$category=null){


		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$this->db->select('
				a.*,
				b.category_name,
				c.first_name,
				c.last_name,
				c.seller_id,
				c.email,
				d.brand_name,
				e.title
			');
		$this->db->from('upload_product a');
		$this->db->join('product_category b','a.category_id = b.category_id','left');
		$this->db->join('seller_information c','a.seller_id = c.seller_id','left');
		$this->db->join('brand d','a.brand_id = d.brand_id','left');
		$this->db->join('upload_product_title e','e.upload_id = a.upload_id','left');
		$this->db->where('e.lang_id',$lang_id);

		if ($seller) {
			$this->db->where('a.seller_id',$seller);
		}

		if ($model) {
			$this->db->like('a.product_model', $model, 'both');
		}

		if ($title) {
			$this->db->like('e.title', $title, 'both'); 
		}

		if ($category) {
			$this->db->where('a.category_id',$category);
		}
		$this->db->where('a.status',1);
		$this->db->limit($per_page,$page);
		$this->db->order_by('upload_id','desc');
		$query = $this->db->get();
		return $query->result_array();
	}	
	//Pending Product Count
	public function pending_product_count($per_page=null,$page=null,$seller=null,$model=null,$title=null,$category=null){


		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}


		$this->db->select('
				a.*,
				b.category_name,
				c.first_name,
				c.last_name,
				c.seller_id,
				c.email,
				d.brand_name
			');
		$this->db->from('upload_product a');
		$this->db->join('product_category b','a.category_id = b.category_id','left');
		$this->db->join('seller_information c','a.seller_id = c.seller_id','left');
		$this->db->join('brand d','a.brand_id = d.brand_id','left');
		$this->db->join('upload_product_title e','e.upload_id = a.upload_id','left');
		$this->db->where('e.lang_id',$lang_id);

		if ($seller) {
			$this->db->where('a.seller_id',$seller);
		}

		if ($model) {
			$this->db->like('a.product_model', $model, 'both');
		}

		if ($title) {
			$this->db->like('e.title', $title, 'both'); 
		}

		if ($category) {
			$this->db->where('a.category_id',$category);
		}

		$this->db->where('a.status',1);
		$this->db->order_by('upload_id','desc');
		$query = $this->db->get();
		return $query->num_rows();
	}
	//Retrieve seller Product Edit Data
	public function retrive_pending_product_edit_data($upload_id)
	{
		$this->db->select('
				a.*,
				b.category_name,
				c.first_name,
				c.last_name,
				c.id as seller_uni_id,
				c.seller_id,
				d.brand_name,
				d.brand_id
			');
		$this->db->from('upload_product a');
		$this->db->join('product_category b','a.category_id = b.category_id','left');
		$this->db->join('seller_information c','a.seller_id = c.seller_id','left');
		$this->db->join('brand d','a.brand_id = d.brand_id','left');
		$this->db->where('upload_id',$upload_id);
		$query = $this->db->get();
		return $query->row();
	}
	//Retrieve seller image edit data
	public function retrive_pending_product_image_edit_data($upload_id)
	{
		$this->db->select('*');
		$this->db->from('upload_image');
		$this->db->where('upload_id',$upload_id);
		$this->db->where('image_type',1);
		$query = $this->db->get();
		return $query->result_array();
	}
	//Retrieve pending title 
	public function retrive_pending_product_title($upload_id=null,$lang_id=null)
	{
		$this->db->select('*');
		$this->db->from('upload_product_title');
		$this->db->where('upload_id',$upload_id);
		$this->db->where('lang_id',$lang_id);
		$query = $this->db->get();
		return $query->row();
	}
	//Retrieve pending description
	public function retrive_pending_product_description($upload_id=null,$lang_id=null)
	{
		$this->db->select('*');
		$this->db->from('upload_product_description');
		$this->db->where('upload_id',$upload_id);
		$this->db->where('lang_id',$lang_id);
		$this->db->where('description_type',1);
		$query = $this->db->get();
		return $query->row();
	}
	//Retrieve pending product specification
	public function retrive_pending_product_specification($upload_id=null,$lang_id=null)
	{
		$this->db->select('*');
		$this->db->from('upload_product_description');
		$this->db->where('upload_id',$upload_id);
		$this->db->where('lang_id',$lang_id);
		$this->db->where('description_type',2);
		$query = $this->db->get();
		return $query->row();
	}
	//Product Image
	public function insert_upload_image($data)
	{
		if ($data['status'] == 1) {
			$result= $this->db->select('*')
					->from('upload_image')
					->where('status',1)
					->where('upload_id',$data['upload_id'])
					->where('image_type',1)
					->get()
					->num_rows();
			if ($result > 0) {
				return FALSE;
			}else{
				$this->db->insert('upload_image',$data);
				return TRUE;
			}
		}else{
			$this->db->insert('upload_image',$data);
			return TRUE;
		}
	}
	//Insert upload title
	public function insert_upload_title($data)
	{
		$result = $this->db->select('*')
					->from('upload_product_title')
					->where('upload_id',$data['upload_id'])
					->where('lang_id',$data['lang_id'])
					->get()
					->num_rows();

		if ($result > 0) {
			return false;
		}else{
			$this->db->insert('upload_product_title',$data);
			return TRUE;
		}
	}
    //Seller Product Description Entry
	public function insert_upload_description($data)
	{
		$result = $this->db->select('*')
						->from('upload_product_description')
						->where('upload_id',$data['upload_id'])
						->where('lang_id',$data['lang_id'])
						->where('description_type',$data['description_type'])
						->get()
						->num_rows();

		if ($result > 0) {
			return false;
		}else{
			$this->db->insert('upload_product_description',$data);
			return TRUE;
		}
	}
	//Make image primary
	public function make_pending_img_primary($image_id=null,$upload_id=null){
		$result = $this->db->select('*')
				->from('upload_image')
				->where('upload_id',$upload_id)
				->where('status',1)
				->where('image_type',1)
				->get()
				->row();
		if ($result) {
			$this->db->set('status',0)
					->where('upload_image_id',$result->upload_image_id)
					->update('upload_image');

			$this->db->set('status',1)
					->where('upload_image_id',$image_id)
					->update('upload_image');
		}else{
			$this->db->set('status',1)
					->where('upload_image_id',$image_id)
					->update('upload_image');
		}
		return true;
	}
	// //Retrive seller product image
	// public function retrive_seller_product_image_edit_data1($upload_image_id){
	// 	$this->db->select('
	// 			a.*
	// 		');
	// 	$this->db->from('upload_image a');
	// 	$this->db->where('upload_image_id',$upload_image_id);
		
	// 	$query = $this->db->get();
	// 	return $query->row();
	// }


	#==========================================#
	#=========== Denied Product ==============#
	#==========================================#

	//Denied Product List
	public function denied_product($per_page=null,$page=null,$seller=null,$model=null,$title=null,$category=null){


		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}


		$this->db->select('
				a.*,
				b.category_name,
				c.first_name,
				c.last_name,
				c.seller_id,
				c.email,
				d.brand_name,
				e.title
			');
		$this->db->from('upload_product a');
		$this->db->join('product_category b','a.category_id = b.category_id','left');
		$this->db->join('seller_information c','a.seller_id = c.seller_id','left');
		$this->db->join('brand d','a.brand_id = d.brand_id','left');
		$this->db->join('upload_product_title e','e.upload_id = a.upload_id','left');
		$this->db->where('e.lang_id',$lang_id);

		if ($seller) {
			$this->db->where('a.seller_id',$seller);
		}

		if ($model) {
			$this->db->like('a.product_model', $model, 'both');
		}

		if ($title) {
			$this->db->like('e.title', $title, 'both'); 
		}

		if ($category) {
			$this->db->where('a.category_id',$category);
		}
		$this->db->where('a.status',3);
		$this->db->limit($per_page,$page);
		$this->db->order_by('upload_id','desc');
		$query = $this->db->get();
		return $query->result_array();
	}	
	//Pending Product Count
	public function denied_product_count($per_page=null,$page=null,$seller=null,$model=null,$title=null,$category=null){


		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}


		$this->db->select('
				a.*,
				b.category_name,
				c.first_name,
				c.last_name,
				c.seller_id,
				c.email,
				d.brand_name
			');
		$this->db->from('upload_product a');
		$this->db->join('product_category b','a.category_id = b.category_id','left');
		$this->db->join('seller_information c','a.seller_id = c.seller_id','left');
		$this->db->join('brand d','a.brand_id = d.brand_id','left');
		$this->db->join('upload_product_title e','e.upload_id = a.upload_id','left');
		$this->db->where('e.lang_id',$lang_id);

		if ($seller) {
			$this->db->where('a.seller_id',$seller);
		}

		if ($model) {
			$this->db->like('a.product_model', $model, 'both');
		}

		if ($title) {
			$this->db->like('e.title', $title, 'both'); 
		}

		if ($category) {
			$this->db->where('a.category_id',$category);
		}

		$this->db->where('a.status',3);
		$this->db->order_by('upload_id','desc');
		$query = $this->db->get();
		return $query->num_rows();
	}
	//Retrive comission value
	public function retrive_com_value($category_id=null){
		$result = $this->db->select('*')
			->from('comission')
			->where('category_id',$category_id)
			->get()
			->row();

		return $result;
	}

	//Retrive product info 
	public function retrive_product_info($product_id=null){

		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$this->db->select('
				a.*,
				b.category_name,
				c.first_name,
				c.last_name,
				c.seller_id,
				c.email,
				d.brand_name,
				e.title
			');
		$this->db->from('product_information a');
		$this->db->join('product_category b','a.category_id = b.category_id','left');
		$this->db->join('seller_information c','a.seller_id = c.seller_id','left');
		$this->db->join('brand d','a.brand_id = d.brand_id','left');
		$this->db->join('product_title e','e.product_id = a.product_id','left');
		$this->db->where('e.lang_id',$lang_id);
		// $this->db->where('a.status',2);
		$this->db->where('a.product_id',$product_id);
		$query = $this->db->get();
		return $query->row();
	}
}