<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Shipping_agents extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}
	//Count shipping_agent
	public function shipping_agent_entry($data)
	{
//		$this->db->select('*');
//		$this->db->from('shipping_agents');
//		$this->db->where('position',$data['position']);
//		$query = $this->db->get();
//		if ($query->num_rows() > 0) {
//			return FALSE;
//		}else{
			$this->db->insert('shipping_agents',$data);
			return TRUE;
//		}
	}

	//shipping_agent List
	public function shipping_agent_list()
	{
		$this->db->select('*');
		$this->db->from('shipping_agents');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();	
		}
		return false;
	}
	//shipping_agent Search Item
	public function shipping_agent_search_item($agent_id)
	{
		$this->db->select('*');
		$this->db->from('shipping_agents');
		$this->db->where('agent_id',$agent_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();	
		}
		return false;
	}
	//Retrieve shipping_agent Edit Data
	public function retrieve_shipping_agent_editdata($agent_id)
	{
		$this->db->select('*');
		$this->db->from('shipping_agents');
		$this->db->where('agent_id',$agent_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();	
		}
		return false;
	}
	
	//Update Shipping_agents
	public function update_shipping_agent($data,$agent_id)
	{
		$this->db->select('*');
		$this->db->from('shipping_agents');
//		$this->db->where('position',$data['position']);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$this->db->set('agent_name',$data['agent_name']);
			$this->db->set('agent_phone',$data['agent_phone']);
			$this->db->set('agent_email',$data['agent_email']);
			$this->db->set('agent_address',$data['agent_address']);
			$this->db->set('amount',$data['amount']);
			$this->db->where('agent_id',$agent_id);
			$this->db->update('shipping_agents');
			return FALSE;
		}else{
			$this->db->where('agent_id',$agent_id);
			$result = $this->db->update('shipping_agents',$data);
			return TRUE;
		}
	}
	// Delete shipping_agent Item
	public function delete_shipping_agent($agent_id)
	{
		$this->db->where('agent_id',$agent_id);
		$this->db->delete('shipping_agents');
		return true;
	}
}