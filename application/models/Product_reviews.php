<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Product_reviews extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}
	
	//product_review List
	public function product_review_list()
	{
		$this->db->select('
			a.*,
			b.product_model,
			c.customer_name');
		$this->db->from('product_review a');
		$this->db->join('customer_information c','c.customer_id = a.reviewer_id','left');
		$this->db->join('product_information b','b.product_id = a.product_id','left');
		// $this->db->order_by('d.title','asc');
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return $query->result_array();	
		}
		return false;
	}

	//Product list
	public function product_list(){
		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$query=$this->db->select('
					a.*,b.title as product_name
				')
				->from('product_information a')
				->join('product_title b','a.product_id = b.product_id')
				->where('lang_id',$lang_id)
				->get();
		if ($query->num_rows() > 0) {
		 	return $query->result_array();	
		}
		return false;
	}

	//Product search by language
	public function product_search_item($product_id){
		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$query=$this->db->select('
					a.*,b.title as product_name
				')
				->from('product_information a')
				->join('product_title b','a.product_id = b.product_id')
				->where('lang_id',$lang_id)
				->where('a.product_id',$product_id)
				->get();
		if ($query->num_rows() > 0) {
		 	return $query->result_array();	
		}
		return false;
	}

	//Selecet product name
	public function select_product_name($product_id){
		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}
		$result = $this->db->select('title')
						->from('product_title')
						->where('product_id',$product_id)
						->where('lang_id',$lang_id)
						->get()
						->row();
		
		if ($result) {
			return $result->title;
		}else{
			return false;
		}
	}

	//product_review Search Item
	public function product_review_search_item($product_review_id)
	{
		$this->db->select('*');
		$this->db->from('product_review');
		$this->db->where('product_review_id',$product_review_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();	
		}
		return false;
	}
	//Count product_review
	public function product_review_entry($data)
	{
		$result = $this->db->insert('product_review',$data);
	
		if ($result) {
			return TRUE;
		}else{
			return FALSE;
		}
	}
	//Retrieve product_review Edit Data
	public function retrieve_product_review_editdata($product_review_id)
	{
		$this->db->select('*');
		$this->db->from('product_review');
		$this->db->where('product_review_id',$product_review_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();	
		}
		return false;
	}
	
	//Update Product Reviews
	public function update_product_review($data,$product_review_id)
	{

		$this->db->where('product_review_id',$product_review_id);
		$result = $this->db->update('product_review',$data);
		return TRUE;
	}
	// Delete product_review Item
	public function delete_product_review($product_review_id)
	{
		$this->db->where('product_review_id',$product_review_id);
		$this->db->delete('product_review'); 	
		return true;
	}
}