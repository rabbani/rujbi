<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Comissions extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}
	//comission List
	public function comission_list()
	{
		$this->db->select('*');
		$this->db->from('comission a');
		$this->db->join('product_category b','a.category_id = b.category_id');
		$this->db->order_by('a.id','desc');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();	
		}
		return false;
	}
	//comission entry
	public function comission_entry($data)
	{
		$this->db->select('*');
		$this->db->from('comission');
		$this->db->where('category_id',$data['category_id']);
		$this->db->where('status',1);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return FALSE;
		}else{
			$this->db->insert('comission',$data);
			return TRUE;
		}
	}
	//Retrieve comission Edit Data
	public function retrieve_comission_editdata($comission_id)
	{
		$this->db->select('*');
		$this->db->from('comission');
		$this->db->where('id',$comission_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();	
		}
		return false;
	}
	//Update Comissions
	public function update_comission($data,$comission_id)
	{
		$result = $this->db->select('*')
						->from('comission')
						->where('category_id',$data['category_id'])
						->where('id !=',$comission_id)
						->get()
						->num_rows();
		if ($result > 0) {
			return false;
		}else{
			$this->db->where('id',$comission_id);
			$result = $this->db->update('comission',$data);
			if ($result) {
				return true;
			}
			return false;
		}
	}
	// Delete comission Item
	public function delete_comission($comission_id)
	{
		$this->db->where('id',$comission_id);
		$this->db->delete('comission'); 	
		return true;
	}
	//Comission report
	public function comission_report($per_page=null,$page=null,$seller=null,$product_id=null,$category_id=null){

		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$this->db->select('
						a.*,
						MIN((quantity * rate) - (quantity * discount)) as total_amount,
						d.category_name
						');
		$this->db->from('invoice_details a');
		$this->db->join('seller_information b','a.seller_id = b.seller_id');
		$this->db->join('product_title c','c.product_id = a.product_id');
		$this->db->join('product_category d','d.category_id = a.category_id');

		if ($seller) {
			$this->db->where('a.seller_id',$seller);
		}
		if ($product_id) {
			$this->db->where('a.product_id',$product_id);
		}
		if ($category_id) {
			$this->db->where('a.category_id',$category_id);
		}

		$this->db->where('c.lang_id',$lang_id);
		$this->db->group_by('a.invoice_details_id');
		$this->db->limit($per_page,$page);
		$result = $this->db->get();
		return $result->result_array();
	}	

	//Comission report count
	public function comission_report_count($seller=null,$product_id=null,$category_id=null){

		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$this->db->select('
						a.*,
						MIN((quantity * rate) - (quantity * discount)) as total_amount,
						d.category_name
						');
		$this->db->from('invoice_details a');
		$this->db->join('seller_information b','a.seller_id = b.seller_id');
		$this->db->join('product_title c','c.product_id = a.product_id');
		$this->db->join('product_category d','d.category_id = a.category_id');

		if ($seller) {
			$this->db->where('a.seller_id',$seller);
		}
		if ($product_id) {
			$this->db->where('a.product_id',$product_id);
		}
		if ($category_id) {
			$this->db->where('a.category_id',$category_id);
		}

		$this->db->where('c.lang_id',$lang_id);
		$this->db->group_by('a.invoice_details_id');
		$result = $this->db->get();
		return $result->num_rows();
	}

	//Seller Name
	public function seller_name($seller_id){
		$seller_info = $this->db->select('*')
				->from('seller_information')
				->where('seller_id',$seller_id)
				->get()
				->row();

		return $seller_info->first_name.' '.$seller_info->last_name;
	}	

	//Seller Name
	public function product_name($product_id){

		$lang_id = 0;
		$user_lang = $this->session->userdata('language');
		if (empty($user_lang)) {
			$lang_id = 'english';
		}else{
			$lang_id = $user_lang;
		}

		$seller_info = $this->db->select('*')
				->from('product_title')
				->where('product_id',$product_id)
				->where('lang_id',$lang_id)
				->get()
				->row();

		return $seller_info->title;
	}
}