<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cemail_template extends CI_Controller {

	function __construct() {
      	parent::__construct();
		$this->load->library('lemail_template');
		$this->load->model('Email_templates');
		$this->permission->module('email_template')->redirect();
    }
	//Default loading for template system.
	public function index()
	{
		$this->permission->check_label('add_template')->create()->redirect();
		$content = $this->lemail_template->template_add_form();
		$this->template->full_admin_html_view($content);
	}
	//Insert template
	public function insert_template()
	{
		$this->permission->check_label('add_template')->create()->redirect();
		$this->form_validation->set_rules('name', display('name'), 'trim|required');
		$this->form_validation->set_rules('subject', display('subject'), 'trim|required');
		$this->form_validation->set_rules('message', display('message'), 'trim|required');
		$this->form_validation->set_rules('status', display('status'), 'trim|required');

		if ($this->form_validation->run() == FALSE)
        {
        	$data = array(
				'title' => display('add_template')
			);
        	$content = $this->parser->parse('template/add_template',$data,true);
			$this->template->full_admin_html_view($content);
        }else{

			$data=array(
				'name' 	  => $this->input->post('name'),
				'subject' => $this->input->post('subject'),
				'message' => $this->input->post('message'),
				'status'  => $this->input->post('status'),
			);

			$result=$this->Email_templates->template_entry($data);
			
			if ($result == TRUE) {
					
				$this->session->set_userdata(array('message'=>display('successfully_added')));

				if(isset($_POST['add-template'])){
					redirect(base_url('manage_template'));
				}elseif(isset($_POST['add-template-another'])){
					redirect(base_url('cemail_template'));
				}

			}else{
				$this->session->set_userdata(array('error_message'=>display('already_exists')));
				redirect('cemail_template');
			}
        }
	}
	//Manage template
	public function manage_template()
	{
		$this->permission->check_label('manage_template')->redirect();
        $content =$this->lemail_template->template_list();
		$this->template->full_admin_html_view($content);;
	}
	//Template Update Form
	public function template_update_form($template_id)
	{	
		$this->permission->check_label('manage_template')->update()->redirect();
		$content = $this->lemail_template->template_edit_data($template_id);
		$this->template->full_admin_html_view($content);
	}
	// Template Update
	public function template_update($template_id=null)
	{
		$this->permission->check_label('manage_template')->update()->redirect();
		$this->form_validation->set_rules('name', display('name'), 'trim|required');
		$this->form_validation->set_rules('subject', display('subject'), 'trim|required');
		$this->form_validation->set_rules('message', display('message'), 'trim|required');
		$this->form_validation->set_rules('status', display('status'), 'trim|required');
		
		if ($this->form_validation->run() == FALSE)
        {
        	redirect('manage_template');
        }else{

			$data = array(
				'name' 	  => $this->input->post('name'),
				'subject' => $this->input->post('subject'),
				'message' => $this->input->post('message'),
				'status'  => $this->input->post('status'),
			);

			$result=$this->Email_templates->update_template($data,$template_id);

			if ($result) {
				$this->session->set_userdata(array('message'=>display('successfully_updated')));
				redirect('manage_template');
			}else{
				$this->session->set_userdata(array('error_message'=>display('not_updated')));
				redirect('manage_template');
			}
        }
	}
	// Template Delete
	public function template_delete($template_id)
	{
		$this->permission->check_label('manage_template')->delete()->redirect();
		$this->Email_templates->delete_template($template_id);
		$this->session->set_userdata(array('message'=>display('successfully_delete')));
		redirect('manage_template');
	}
}