<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permission_setup extends CI_Controller {
 	
 	public function __construct()
 	{
 		parent::__construct();
 	// 	$this->load->model(array(
 	// 		'module_permission_model',
 	// 		'module_model', 
 	// 		'user_model'
 	// 	));
 		
		// if (! $this->session->userdata('isAdmin'))
		// 	redirect('login');
 	}


	public function index()
	{
		$data['p_menu']     = $this->db->select('menu_id,menu_title')->get('sec_menu_item')->result();
		$data['title']      = display('module_permission_list');
		$data['module'] 	= "dashboard";  
		$content = $this->parser->parse('ptest/permission_set',$data,true);
		$this->template->full_admin_html_view($content);
	}

	public function save()
	{
		$this->form_validation->set_rules('menu_title', 'Menu Title','required|is_unique[sec_menu_item.menu_title]');

		if ($this->form_validation->run()) {
			$setData = array(
				'menu_title' 		=> $this->input->post('menu_title'),
				'page_url' 			=> $this->input->post('page_url'),
				'module' 			=> $this->input->post('module'),
				'parent_menu' 		=> $this->input->post('parent_menu'),
				'is_report' 		=> ($this->input->post('is_report')?1:0),
				'createdate'		=> date('Y-m-d'),
				'createby'			=> $this->session->userdata('user_id'),
			);
			$this->db->insert('sec_menu_item',$setData);
			$this->session->set_flashdata('message', display('save_successfully'));
			redirect('permission_setup');
		}else {
			$this->session->set_flashdata('error_message', validation_errors());
			redirect('permission_setup');
		}
	}

	public function menu_item_list()
	{
		$data['title']      = 'Module Permission List';

		$limit=15;

        @$start = ($this->uri->segment(3)?$this->uri->segment(3):0);

        $config = $this->pasination($limit,'sec_menu_item','permission_setup/menu_item_list');
        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();

		$data['menu_item_list'] = $this->db->select('*')->limit($limit,$start)->order_by('menu_id','desc')->get('sec_menu_item')->result();

		$content = $this->parser->parse('ptest/permission_set_list',$data,true);
		$this->template->full_admin_html_view($content);
	}

	public function edit($id)
	{
		$p_menu    = $this->db->select('menu_id,menu_title')->get('sec_menu_item')->result();
		$menu_item = $this->db->select('*')->where('menu_id',$id)->get('sec_menu_item')->row();

		$data = array(
			'title' 	 => 'Edit Permission',
			'menu_id' 	 => $menu_item->menu_id, 
			'menu_title' => $menu_item->menu_title, 
			'page_url' 	 => $menu_item->page_url, 
			'module' 	 => $menu_item->module, 
			'parent_menu'=> $menu_item->parent_menu, 
			'p_menu'	 => $p_menu, 
		);
		
		$content = $this->parser->parse('ptest/edit',$data,true);
		$this->template->full_admin_html_view($content);
	}

	public function update()
	{
		$setData = array(
			'menu_title' 		=> $this->input->post('menu_title'),
			'page_url' 			=> $this->input->post('page_url'),
			'module' 			=> $this->input->post('module'),
			'parent_menu' 		=> $this->input->post('parent_menu'),
			'is_report' 		=> ($this->input->post('is_report')?1:0)
		);
		$menu_id = $this->input->post('menu_id');
		$this->db->where('menu_id',$menu_id)->update('sec_menu_item',$setData);
		$this->session->set_flashdata('message', display('successfully_updated'));
		redirect('permission_setup/menu_item_list');
	}

	public function pasination($limit,$tbl,$url){
        $total_rows = $this->db->select('*')
        ->from($tbl)
        ->get()
        ->num_rows();
        $config["base_url"] = base_url($url);
        $config["total_rows"] = $total_rows;
        $config["per_page"] = $limit;
        // $config['suffix'] = '?'.http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'];
        // integrate bootstrap pagination
        $config['full_tag_open'] = "<ul class='pagination col-xs pull-right'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        return $config;
	}

	public function delete($id){
		$this->db->where('menu_id',$id)->delete('sec_menu_item');
		$this->session->set_flashdata('message', display('delete_successfully'));
		redirect('permission_setup/menu_item_list');
	}
}
