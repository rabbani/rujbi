<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Csoft_setting extends CI_Controller {

	function __construct() {
      	parent::__construct();
		$this->load->library('lsoft_setting');
		$this->load->model('Soft_settings');
    }

	//Default loading for Category system.
	public function index()
	{
		$this->permission->check_label('setting')->read()->redirect();
		$content = $this->lsoft_setting->setting_add_form();
		$this->template->full_admin_html_view($content);
	}

	// Setting Update
	public function update_setting()
	{
		$this->permission->check_label('setting')->update()->redirect();
		if ($_FILES['logo']['name']) {
			$config['upload_path']          = 'my-assets/image/logo/';
	        $config['allowed_types']        = 'gif|jpg|png|jpeg|JPEG|GIF|JPG|PNG';
	        $config['max_size']             = "1024";
	        $config['max_width']            = "*";
	        $config['max_height']           = "*";
	        $config['encrypt_name'] 		= TRUE;

	        $this->load->library('upload', $config);
	        if ( ! $this->upload->do_upload('logo'))
	        {
	            $error = array('error' => $this->upload->display_errors());
	            $this->session->set_userdata(array('error_message'=> $this->upload->display_errors()));
	            redirect(base_url('csoft_setting'));
	        }
	        else
	        {
	        	$image =$this->upload->data();
	        	$logo = base_url()."my-assets/image/logo/".$image['file_name'];
	        }
		}

		if ($_FILES['favicon']['name']) {
			$config['upload_path']          = 'my-assets/image/logo/';
	        $config['allowed_types']        = 'gif|jpg|png|jpeg|JPEG|GIF|JPG|PNG';
	        $config['max_size']             = "1024";
	        $config['max_width']            = "*";
	        $config['max_height']           = "*";
	        $config['encrypt_name'] 		= TRUE;

	        $this->load->library('upload', $config);
	        if ( ! $this->upload->do_upload('favicon'))
	        {
	            $error = array('error' => $this->upload->display_errors());
	            $this->session->set_userdata(array('error_message'=> $this->upload->display_errors()));
	            redirect(base_url('csoft_setting'));
	        }
	        else
	        {
	        	$image =$this->upload->data();
	        	$favicon = base_url()."my-assets/image/logo/".$image['file_name'];
	        }
		}

		if ($_FILES['invoice_logo']['name']) {
			$config['upload_path']          = 'my-assets/image/logo/';
	        $config['allowed_types']        = 'gif|jpg|png|jpeg|JPEG|GIF|JPG|PNG';
	        $config['max_size']             = "1024";
	        $config['max_width']            = "*";
	        $config['max_height']           = "*";
	        $config['encrypt_name'] 		= TRUE;

	        $this->load->library('upload', $config);
	        if ( ! $this->upload->do_upload('invoice_logo'))
	        {
	            $error = array('error' => $this->upload->display_errors());
	            $this->session->set_userdata(array('error_message'=> $this->upload->display_errors()));
	            redirect(base_url('csoft_setting'));
	        }
	        else
	        {
	        	$image =$this->upload->data();
	        	$invoice_logo = base_url()."my-assets/image/logo/".$image['file_name'];
	        }
		}

		$old_logo 	 = $this->input->post('old_logo');
		$old_invoice_logo = $this->input->post('old_invoice_logo');
		$old_favicon = $this->input->post('old_favicon');

		$language = $this->input->post('language');
		$this->session->set_userdata('language',$language);

		$data=array(
			'logo' 			=> (!empty($logo)?$logo:$old_logo),
			'invoice_logo' 	=> (!empty($invoice_logo)?$invoice_logo:$old_invoice_logo),
			'favicon' 		=> (!empty($favicon)?$favicon:$old_favicon),
			'footer_text'	=> $this->input->post('footer_text'),
			'language' 		=> $language,
			'rtr' 			=> $this->input->post('rtr'),
			'captcha' 		=> $this->input->post('captcha'),
			'site_key' 		=> $this->input->post('site_key'),
			'secret_key' 	=> $this->input->post('secret_key'),
			);

		$this->Soft_settings->update_setting($data);
		$this->session->set_userdata(array('message'=>display('successfully_updated')));
		redirect(base_url('csoft_setting'));
	}

	//Email Configuration
	public function email_configuration(){
		$this->permission->check_label('email_configuration')->read()->redirect();
		$content = $this->lsoft_setting->email_configuration_form();
		$this->template->full_admin_html_view($content);
	}
	
	//Update email configuration
	public function update_email_configuration()
	{
		$this->permission->check_label('email_configuration')->update()->redirect();
		$data=array(
			'protocol' 		=> $this->input->post('protocol'),
			'mailtype'		=> $this->input->post('mailtype'),
			'smtp_host' 	=> $this->input->post('smtp_host'),
			'smtp_port' 	=> $this->input->post('smtp_port'),
			'sender_email' 	=> $this->input->post('sender_email'),
			'password' 		=> $this->input->post('password'),
		);

		$this->Soft_settings->update_email_config($data);
		$this->session->set_userdata(array('message'=>display('successfully_updated')));
		redirect(base_url('email_configuration'));
	}

	//Payment Configuration
	public function payment_gateway_setting(){
		$this->permission->check_label('payment_gateway_setting')->read()->redirect();
		$content = $this->lsoft_setting->payment_configuration_form();
		$this->template->full_admin_html_view($content);
	}

	//Update payment configuration
	public function update_payment_gateway_setting($id = null)
	{
		$this->permission->check_label('payment_gateway_setting')->update()->redirect();
		if ($id == 2) {
			$data=array(
				'shop_id' 		=> $this->input->post('shop_id'),
				'secret_key'	=> $this->input->post('secret_key'),
				'status' 		=> $this->input->post('status'),
			);
			$this->Soft_settings->update_payment_gateway_setting($data,$id);
		}else if ($id == 1){
			$data=array(
				'public_key' => $this->input->post('public_key'),
				'private_key'=> $this->input->post('private_key'),
				'status' 	 => $this->input->post('status'),
			);

			$this->Soft_settings->update_payment_gateway_setting($data,$id);
		}else if ($id == 3){
			$data=array(
				'paypal_email' => $this->input->post('paypal_email'),
				'currency'	   => $this->input->post('currency'),
				'status' 	   => $this->input->post('status'),
			);

			$this->Soft_settings->update_payment_gateway_setting($data,$id);
		}
		
		$this->session->set_userdata(array('message'=>display('successfully_updated')));
		redirect(base_url('payment_gateway_setting'));
	}	

	//Social Configuration
	public function social_login_setting(){
		$this->permission->check_label('social_setting')->read()->redirect();
		$content = $this->lsoft_setting->social_login_setting();
		$this->template->full_admin_html_view($content);
	}

	//Update social configuration
	public function update_social_setting($id = null)
	{
		$this->permission->check_label('social_setting')->update()->redirect();
		if ($id == 1) {
			$data=array(
				'client_id' 	=> $this->input->post('client_id'),
				'client_secret'	=> $this->input->post('client_secret'),
				'api_key'		=> $this->input->post('api_key'),
				'status' 		=> $this->input->post('status'),
			);
			$this->Soft_settings->update_social_setting($data,$id);
		}else if ($id == 2){
			$data=array(
				'fb_app_id' 	=> $this->input->post('app_id'),
				'fb_app_secret' => $this->input->post('app_secret'),
				'status' 	 	=> $this->input->post('status'),
			);
			$this->Soft_settings->update_social_setting($data,$id);
		}
		
		$this->session->set_userdata(array('message'=>display('successfully_updated')));
		redirect(base_url('social_login_setting'));
	}
}