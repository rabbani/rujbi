<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cblock extends CI_Controller {

	function __construct() {
      	parent::__construct();
		$this->load->library('lblock');
		$this->load->model('Blocks');
    }

	//Default loading for block system.
	public function index()
	{
		$this->permission->check_label('add_block')->create()->redirect();
		$content = $this->lblock->block_add_form();
		$this->template->full_admin_html_view($content);
	}

	//Insert block
	public function insert_block()
	{
		$this->permission->check_label('add_block')->create()->redirect();
		$this->form_validation->set_rules('block_cat_id', display('category'), 'trim|required');
		$this->form_validation->set_rules('block_position', display('block_position'), 'trim|required');
		$this->form_validation->set_rules('block_style', display('block_style'), 'trim|required');

		if ($this->form_validation->run() == FALSE)
        {
        	$data = array(
				'title' => display('add_block')
			);
        	$content = $this->parser->parse('block/add_block',$data,true);
			$this->template->full_admin_html_view($content);
        }else{

			$data=array(
				'block_id' 		=> $this->auth->generator(15),
				'block_cat_id' 	=> $this->input->post('block_cat_id'),
				'block_css' 	=> $this->input->post('block_css'),
				'block_position'=> $this->input->post('block_position'),
				'block_style'	=> $this->input->post('block_style'),
				'status' 		=> 1
				);

			$result=$this->Blocks->block_entry($data);

			if ($result == TRUE) {
					
				$this->session->set_userdata(array('message'=>display('successfully_added')));

				if(isset($_POST['add-block'])){
					redirect(base_url('manage_block'));
				}elseif(isset($_POST['add-block-another'])){
					redirect(base_url('cblock'));
				}

			}else{
				$this->session->set_userdata(array('error_message'=>display('already_exists')));
				redirect(base_url('cblock'));
			}
        }
	}
	//Manage block
	public function manage_block()
	{
		$this->permission->check_label('manage_block')->read()->redirect();
        $content =$this->lblock->block_list();
		$this->template->full_admin_html_view($content);;
	}
	//block Update Form
	public function block_update_form($block_id)
	{	
		$this->permission->check_label('manage_block')->update()->redirect();
		$content = $this->lblock->block_edit_data($block_id);
		$this->template->full_admin_html_view($content);
	}
	// block Update
	public function block_update($block_id=null)
	{
		$this->permission->check_label('manage_block')->update()->redirect();
		$this->form_validation->set_rules('block_cat_id', display('category'), 'trim|required');
		$this->form_validation->set_rules('block_position', display('block_position'), 'trim|required');
		$this->form_validation->set_rules('block_style', display('block_style'), 'trim|required');

		if ($this->form_validation->run() == FALSE)
        {
        	$data = array(
				'title' => display('add_block')
			);
        	$content = $this->parser->parse('block/add_block',$data,true);
			$this->template->full_admin_html_view($content);
        }else{

			$data=array(
				'block_id' 		=> $this->auth->generator(15),
				'block_cat_id' 	=> $this->input->post('block_cat_id'),
				'block_css' 	=> $this->input->post('block_css'),
				'block_position'=> $this->input->post('block_position'),
				'block_style'	=> $this->input->post('block_style'),
				'status' 		=> 1
				);

			$result=$this->Blocks->update_block($data,$block_id);

			if ($result == TRUE) {
				$this->session->set_userdata(array('message'=>display('successfully_updated')));
				redirect('manage_block');
			}else{
				$this->session->set_userdata(array('error_message'=>display('already_exists')));
				redirect('manage_block');
			}
        }
	}
	// block Delete
	public function block_delete($block_id)
	{
		$this->permission->check_label('manage_block')->delete()->redirect();
		$this->Blocks->delete_block($block_id);
		$this->session->set_userdata(array('message'=>display('successfully_delete')));
		redirect('manage_block');
	}
	//Inactive
	public function inactive($id){
		$this->permission->check_label('manage_block')->update()->redirect();
		$this->db->set('status', 0);
		$this->db->where('block_id',$id);
		$this->db->update('block');
		$this->session->set_userdata(array('error_message'=>display('successfully_inactive')));
		redirect(base_url('manage_block'));
	}
	//Active 
	public function active($id){
		$this->permission->check_label('manage_block')->update()->redirect();
		$this->db->set('status', 1);
		$this->db->where('block_id',$id);
		$this->db->update('block');
		$this->session->set_userdata(array('message'=>display('successfully_active')));
		redirect(base_url('manage_block'));
	}
}