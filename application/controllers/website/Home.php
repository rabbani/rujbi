<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home extends CI_Controller {

	function __construct() {
      	parent::__construct();
		$this->load->library('website/Lhome');
		$this->load->library('paypal_lib'); 
		$this->load->model('website/Homes');
		$this->load->model('website/Products_model');
		$this->load->model('Subscribers');
		$this->load->model('website/Categories');
		$this->load->model('Web_settings');
		$this->load->model('Soft_settings');
		$this->load->model('web_settings');
		$this->load->model('Companies');
		$this->load->model('Blocks');
    }
	//Default loading for Home Index.
	public function index()
	{
		$content = $this->lhome->home_page();
		$this->template->full_website_html_view($content);
	}
	//Submit a subcriber.
	public function add_subscribe()
	{
		$data=array(
			'subscriber_id' => $this->generator(15),
			'apply_ip' 		=> $this->input->ip_address(),
			'email' 		=> $this->input->post('sub_email'),
			'status' 		=> 1
		);

		$result=$this->Subscribers->subscriber_entry($data);

		if ($result) {
			echo "2";
		}else{
			echo "3";
		}
	}
	//Add to cart for
	public function add_to_cart(){

		$product_id = $this->input->post('product_id');

		$discount = 0;
		$offer_price = 0;
		$cgst = 0;
		$cgst_id = 0;

		$sgst = 0;
		$sgst_id = 0;	

		$igst = 0;
		$igst_id = 0;

		if ($product_id) {
			$product_details = $this->Homes->product_details($product_id);

			if ($product_details->on_sale) {
			 	$price = $product_details->offer_price;
			 	$offer_price = $product_details->offer_price;
			 	$discount = $product_details->price - $product_details->offer_price;
			}else{
				$price = $product_details->price;
			}

			//CGST product tax
			$this->db->select('*');
            $this->db->from('tax_product_service');
            $this->db->where('product_id',$product_details->product_id);
            $this->db->where('tax_id','H5MQN4NXJBSDX4L');
            $tax_info = $this->db->get()->row();

            if (!empty($tax_info)) {
            	if (($product_details->on_sale == 1)) {
			 		$cgst = ($tax_info->tax_percentage * $product_details->offer_price)/100;
			 		$cgst_id = $tax_info->tax_id;
			 	}else{
			 		$cgst = ($tax_info->tax_percentage * $product_details->price)/100;
			 		$cgst_id = $tax_info->tax_id;
			 	}
		 	}

			//SGST product tax
			$this->db->select('*');
            $this->db->from('tax_product_service');
            $this->db->where('product_id',$product_details->product_id);
            $this->db->where('tax_id','52C2SKCKGQY6Q9J');
            $tax_info = $this->db->get()->row();
            
            if (!empty($tax_info)) {
            	if (($product_details->on_sale == 1)) {
			 		$sgst = ($tax_info->tax_percentage * $product_details->offer_price)/100;
			 		$sgst_id = $tax_info->tax_id;
			 	}else{
			 		$sgst = ($tax_info->tax_percentage * $product_details->price)/100;
			 		$sgst_id = $tax_info->tax_id;
			 	}
		 	}

		 	//IGST product tax
			$this->db->select('*');
            $this->db->from('tax_product_service');
            $this->db->where('product_id',$product_details->product_id);
            $this->db->where('tax_id','5SN9PRWPN131T4V');
            $tax_info = $this->db->get()->row();
            
            if (!empty($tax_info)) {
            	if (($product_details->on_sale == 1)) {
			 		$igst = ($tax_info->tax_percentage * $product_details->offer_price)/100;
			 		$igst_id = $tax_info->tax_id;
			 	}else{
			 		$igst = ($tax_info->tax_percentage * $product_details->price)/100;
			 		$igst_id = $tax_info->tax_id;
			 	}
		 	}

		 	if ($product_details->quantity > 0) {
			 	//Shopping cart validation
			 	$flag 	 = TRUE;
	        	$dataTmp = $this->cart->contents();

				if ($dataTmp) {
				 	foreach ($dataTmp as $item) {
			            if (($item['product_id'] == $product_id)) {
			            	$data = array(
							        'rowid' => $item['rowid'],
							        'qty'   => $item['qty'] + 1
							);
							$this->cart->update($data);
			                $flag = FALSE;
			                break;
			            }
			        }
		    	}

		        if ($flag) {
		        	$data = array(
				        'id'      		  => $this->generator(15),
				        'product_id'      => $product_details->product_id,
				        'qty'     		  => 1,
				        'price'           => $price,
				        'actual_price'    => $product_details->price,
				        'seller_id'  	  => $product_details->seller_id,
				        'offer_price'     => $offer_price,
				        'name'    		  => $product_details->title,
				        'variant' 		  => (!empty($product_details->variant)?$product_details->variant:null),
				        'discount'		  => $discount,
				        'pre_order'		  => false,
				        'options' => array(
				        	'image' => $product_details->thumb_image_url, 
				        	'model' => $product_details->product_model,
				        	'cgst' 	=> $cgst,
				        	'sgst' 	=> $sgst,
				        	'igst' 	=> $igst,
				        	'cgst_id' 	=> $cgst_id,
				        	'sgst_id' 	=> $sgst_id,
				        	'igst_id' 	=> $igst_id,
				        )
					);

					$result = $this->cart->insert($data);
					if ($result) {
						echo "1";
					}else{
						echo "2";
					}
		        }
		        echo "1";
		 	}else{
		 		echo "2";
		 	}
		}
	}
	//Add to cart for details
	public function add_to_cart_details(){

		$product_id = $this->input->post('product_id');
		$qnty 		= $this->input->post('qnty');
		$variant    = $this->input->post('variant');

		$discount = 0;
		$offer_price = 0;
		$cgst = 0;
		$cgst_id = 0;

		$sgst = 0;
		$sgst_id = 0;	

		$igst = 0;
		$igst_id = 0;

		if ($product_id) {
			$product_details = $this->Homes->product_details($product_id);

			if ($product_details->on_sale) {
			 	$price 		 = $product_details->offer_price;
			 	$offer_price = $product_details->offer_price;
			 	$discount 	 = $product_details->price - $product_details->offer_price;
			}else{
				$price = $product_details->price;
			}

			//CGST product tax
			$this->db->select('*');
            $this->db->from('tax_product_service');
            $this->db->where('product_id',$product_details->product_id);
            $this->db->where('tax_id','H5MQN4NXJBSDX4L');
            $tax_info = $this->db->get()->row();

            if (!empty($tax_info)) {
            	if (($product_details->on_sale == 1)) {
			 		$cgst = ($tax_info->tax_percentage * $product_details->offer_price)/100;
			 		$cgst_id = $tax_info->tax_id;
			 	}else{
			 		$cgst = ($tax_info->tax_percentage * $product_details->price)/100;
			 		$cgst_id = $tax_info->tax_id;
			 	}
		 	}

			//SGST product tax
			$this->db->select('*');
            $this->db->from('tax_product_service');
            $this->db->where('product_id',$product_details->product_id);
            $this->db->where('tax_id','52C2SKCKGQY6Q9J');
            $tax_info = $this->db->get()->row();
            
            if (!empty($tax_info)) {
            	if (($product_details->on_sale == 1)) {
			 		$sgst = ($tax_info->tax_percentage * $product_details->offer_price)/100;
			 		$sgst_id = $tax_info->tax_id;
			 	}else{
			 		$sgst = ($tax_info->tax_percentage * $product_details->price)/100;
			 		$sgst_id = $tax_info->tax_id;
			 	}
		 	}

		 	//IGST product tax
			$this->db->select('*');
            $this->db->from('tax_product_service');
            $this->db->where('product_id',$product_details->product_id);
            $this->db->where('tax_id','5SN9PRWPN131T4V');
            $tax_info = $this->db->get()->row();
            
            if (!empty($tax_info)) {
            	if (($product_details->on_sale == 1)) {
			 		$igst = ($tax_info->tax_percentage * $product_details->offer_price)/100;
			 		$igst_id = $tax_info->tax_id;
			 	}else{
			 		$igst = ($tax_info->tax_percentage * $product_details->price)/100;
			 		$igst_id = $tax_info->tax_id;
			 	}
		 	}

		 	if ($product_details->quantity > 0) {
			 	//Shopping cart validation for quantity
		 		$pro_qty_check = $this->Homes->pro_qty_check($product_details->product_id,$qnty);
		 		if ($pro_qty_check) {
				 	$flag 	 = TRUE;
		        	$dataTmp = $this->cart->contents();
					if ($dataTmp) {
					 	foreach ($dataTmp as $item) {
				            if (($item['product_id'] == $product_id) && ($item['variant'] == $variant)) {
				            	$data = array(
								        'rowid' => $item['rowid'],
								        'qty'   => $item['qty'] + $qnty
								);
								$this->cart->update($data);
				                $flag = FALSE;
				                break;
				            }
				        }
			    	}
			        if ($flag) {
			        	$data = array(
					        'id'      	 	=> $this->generator(15),
					        'product_id' 	=> $product_details->product_id,
					        'qty'     	 	=> $qnty,
					        'price'   	 	=> $price,
					        'actual_price'  => $product_details->price,
					        'seller_id'  	=> $product_details->seller_id,
					        'offer_price'   => $offer_price,
					        'name'    		=> $product_details->title,
					        'discount'		=> $discount,
					        'variant' 		=> (!empty($variant)?$variant:null),
					        'pre_order'		=> false, //Set value for pre-order
					        'options' => array(
					        	'image' => $product_details->thumb_image_url, 
					        	'model' => $product_details->product_model,
					        	'cgst' 	=> $cgst,
					        	'sgst' 	=> $sgst,
					        	'igst' 	=> $igst,
					        	'cgst_id' 	=> $cgst_id,
					        	'sgst_id' 	=> $sgst_id,
					        	'igst_id' 	=> $igst_id,
					        )
						);
						$result = $this->cart->insert($data);
			        }
			        echo "1";
			    }else{
			    	echo "3";
			    }
		    }elseif ($product_details->pre_order_quantity > 0) {
		    	//Shopping cart validation
		    	$pre_pro_qty_check = $this->Homes->pre_pro_qty_check($product_details->product_id,$qnty);
		    	if ($pre_pro_qty_check) {

				 	$flag 	 = TRUE;
		        	$dataTmp = $this->cart->contents();

					if ($dataTmp) {
					 	foreach ($dataTmp as $item) {
				            if (($item['product_id'] == $product_id) && ($item['variant'] == $variant)) {
				            	$data = array(
								        'rowid' => $item['rowid'],
								        'qty'   => $item['qty'] + $qnty
								);
								$this->cart->update($data);
				                $flag = FALSE;
				                break;
				            }
				        }
			    	}

			        if ($flag) {
			        	$data = array(
					        'id'      		=> $this->generator(15),
					        'product_id'    => $product_details->product_id,
					        'qty'     		=> $qnty,
					        'price'   		=> $price,
					        'actual_price'  => $product_details->price,
					        'seller_id'  	=> $product_details->seller_id,
					        'offer_price'   => $offer_price,
					        'name'    		=> $product_details->title,
					        'discount'		=> $discount,
					        'variant' 		=> (!empty($variant)?$variant:null),
					        'pre_order'		=> true, //Set value for pre-order
					        'options' => array(
					        	'image' 	=> $product_details->thumb_image_url, 
					        	'model' 	=> $product_details->product_model,
					        	'cgst' 		=> $cgst,
					        	'sgst' 		=> $sgst,
					        	'igst' 		=> $igst,
					        	'cgst_id' 	=> $cgst_id,
					        	'sgst_id' 	=> $sgst_id,
					        	'igst_id' 	=> $igst_id,
					        )
						);

						$result = $this->cart->insert($data);
			        }
			        echo "1";
			    }else{
			    	echo "3";
			    }
		    }else{
		    	echo "2";
		    }
		}
	}
	//Delete item on your cart
	public function delete_cart(){
		$rowid  = $this->input->post('row_id');
		$result = $this->cart->remove($rowid);
		if ($result) {
			echo "1";
		}
	}
	//Delete item on your cart
	public function delete_cart_by_click($rowid){
		$result = $this->cart->remove($rowid);
		if ($result) {
			$this->session->set_userdata(array('message'=>display('successfully_updated')));
			redirect('view_cart');
		}
	}
	//Update your cart
	public function update_cart(){

		$product_id = $this->input->post('product_id');
		$qnty 		= $this->input->post('qty');
		$variant    = $this->input->post('variant_id');

		$discount = 0;
		$offer_price = 0;
		$cgst = 0;
		$cgst_id = 0;

		$sgst = 0;
		$sgst_id = 0;	

		$igst = 0;
		$igst_id = 0;

		if ($product_id) {
			$product_details = $this->Homes->product_details($product_id);

			if ($product_details->on_sale) {
			 	$price 		 = $product_details->offer_price;
			 	$offer_price = $product_details->offer_price;
			 	$discount 	 = $product_details->price - $product_details->offer_price;
			}else{
				$price = $product_details->price;
			}

			//CGST product tax
			$this->db->select('*');
            $this->db->from('tax_product_service');
            $this->db->where('product_id',$product_details->product_id);
            $this->db->where('tax_id','H5MQN4NXJBSDX4L');
            $tax_info = $this->db->get()->row();

            if (!empty($tax_info)) {
            	if (($product_details->on_sale == 1)) {
			 		$cgst = ($tax_info->tax_percentage * $product_details->offer_price)/100;
			 		$cgst_id = $tax_info->tax_id;
			 	}else{
			 		$cgst = ($tax_info->tax_percentage * $product_details->price)/100;
			 		$cgst_id = $tax_info->tax_id;
			 	}
		 	}

			//SGST product tax
			$this->db->select('*');
            $this->db->from('tax_product_service');
            $this->db->where('product_id',$product_details->product_id);
            $this->db->where('tax_id','52C2SKCKGQY6Q9J');
            $tax_info = $this->db->get()->row();
            
            if (!empty($tax_info)) {
            	if (($product_details->on_sale == 1)) {
			 		$sgst = ($tax_info->tax_percentage * $product_details->offer_price)/100;
			 		$sgst_id = $tax_info->tax_id;
			 	}else{
			 		$sgst = ($tax_info->tax_percentage * $product_details->price)/100;
			 		$sgst_id = $tax_info->tax_id;
			 	}
		 	}

		 	//IGST product tax
			$this->db->select('*');
            $this->db->from('tax_product_service');
            $this->db->where('product_id',$product_details->product_id);
            $this->db->where('tax_id','5SN9PRWPN131T4V');
            $tax_info = $this->db->get()->row();
            
            if (!empty($tax_info)) {
            	if (($product_details->on_sale == 1)) {
			 		$igst = ($tax_info->tax_percentage * $product_details->offer_price)/100;
			 		$igst_id = $tax_info->tax_id;
			 	}else{
			 		$igst = ($tax_info->tax_percentage * $product_details->price)/100;
			 		$igst_id = $tax_info->tax_id;
			 	}
		 	}

		 	if ($product_details->quantity > 0) {
		 		if ($product_details->quantity >= $qnty) {
				 	$flag 	 = TRUE;
		        	$dataTmp = $this->cart->contents();
					if ($dataTmp) {
					 	foreach ($dataTmp as $item) {
				            if (($item['product_id'] == $product_id) && ($item['variant'] == $variant)) {
				            	$data = array(
								        'rowid' => $item['rowid'],
								        'qty'   => $qnty
								);
								$this->cart->update($data);
				                $flag = FALSE;
				                break;
				            }
				        }
			    	}
			        $this->session->set_userdata(array('message'=>display('successfully_updated')));
			        echo "1";
			    }else{
			    	$this->session->set_userdata(array('error_message'=>display('you_can_not_order_more_than_stock')));
			    	echo "3";
			    }
		    }elseif ($product_details->pre_order_quantity > 0) {
		    	if ($product_details->pre_order_quantity >= $qnty) {

				 	$flag 	 = TRUE;
		        	$dataTmp = $this->cart->contents();

					if ($dataTmp) {
					 	foreach ($dataTmp as $item) {
				            if (($item['product_id'] == $product_id) && ($item['variant'] == $variant)) {
				            	$data = array(
								        'rowid' => $item['rowid'],
								        'qty'   => $qnty
								);
								$this->cart->update($data);
				                $flag = FALSE;
				                break;
				            }
				        }
			    	}
			        $this->session->set_userdata(array('message'=>display('successfully_updated')));
			        echo "1";
			    }else{
			    	$this->session->set_userdata(array('error_message'=>display('you_can_not_order_more_than_stock')));
			    	echo "3";
			    }
		    }else{
		    	$this->session->set_userdata(array('error_message'=>display('out_of_stock')));
		    	echo "2";
		    }
		}
	}
	//Destroy all clear data
	public function destroy_cart(){
		$this->cart->destroy();
		$this->session->set_userdata(array('message'=>display('successfully_updated')));
		redirect('view_cart');
	}
	//Set ship cost to cart
	public function set_ship_cost_cart(){

		$shipping_cost  = $this->input->post('shipping_cost');
		$ship_cost_name = $this->input->post('ship_cost_name');
		$method_id 		= $this->input->post('method_id');

		//Shippin and billing info set to session
		$first_name 	= $this->input->post('first_name');
		$last_name  	= $this->input->post('last_name');
		$customer_email = $this->input->post('customer_email');
		$customer_mobile= $this->input->post('customer_mobile');
		$customer_address_1  = $this->input->post('customer_address_1');
		$customer_address_2  = $this->input->post('customer_address_2');
		$company  		= $this->input->post('company');
		$city  			= $this->input->post('city');
		$zip  			= $this->input->post('zip');
		$country  		= $this->input->post('country');
		$state  		= $this->input->post('state');
		$ac_pass  		= $this->input->post('ac_pass');
		$privacy_policy = $this->input->post('privacy_policy');
		$creat_ac 		= $this->input->post('creat_ac');

		$ship_first_name = $this->input->post('ship_first_name');
		$ship_last_name  = $this->input->post('ship_last_name');
		$ship_company    = $this->input->post('ship_company');
		$ship_mobile     = $this->input->post('ship_mobile');
		$ship_email      = $this->input->post('ship_email');
		$ship_address_1  = $this->input->post('ship_address_1');
		$ship_address_2  = $this->input->post('ship_address_2');
		$ship_city       = $this->input->post('ship_city');
		$ship_zip        = $this->input->post('ship_zip');
		$ship_country    = $this->input->post('ship_country');
		$ship_state      = $this->input->post('ship_state');
		$payment_method  = $this->input->post('payment_method');
		$order_details = $this->input->post('order_details');
		$diff_ship_adrs  = $this->input->post('diff_ship_adrs');

		//Set data to session
		$this->session->set_userdata(
			array(
			 	'first_name'   	  => $first_name, 
			 	'last_name'    	  => $last_name, 
			 	'customer_email'  => $customer_email, 
			 	'customer_mobile' => $customer_mobile, 
			 	'customer_address_1'  => $customer_address_1, 
			 	'customer_address_2'  => $customer_address_2, 
			 	'company'  		 => $company, 
			 	'city'  		 => $city, 
			 	'zip'  			 => $zip, 
			 	'country'  		 => $country, 
			 	'state'  		 => $state, 
			 	'ac_pass'  	 	=> $ac_pass,
			 	'privacy_policy' => $privacy_policy, 
			 	'creat_ac' 		 => $creat_ac,

			 	'ship_first_name'=> $ship_first_name,  
			 	'ship_last_name' => $ship_last_name,  
			 	'ship_company' 	 => $ship_company,  
			 	'ship_mobile' 	 => $ship_mobile,  
			 	'ship_email' 	 => $ship_email,  
			 	'ship_address_1' => $ship_address_1,  
			 	'ship_address_2' => $ship_address_2,  
			 	'ship_city' 	 => $ship_city,  
			 	'ship_zip' 		 => $ship_zip,  
			 	'ship_country' 	 => $ship_country,  
			 	'ship_state' 	 => $ship_state,  
			 	'payment_method' => $payment_method,  
			 	'order_details'  => $order_details, 
			 	'cart_ship_cost' => $shipping_cost, 
	        	'cart_ship_name' => $ship_cost_name, 
	        	'method_id' 	 => $method_id, 
	        	'diff_ship_adrs' => $diff_ship_adrs, 
			)
		);
		echo "1";
	}
	//Apply Coupon
	public function apply_coupon(){

		$this->form_validation->set_rules('coupon_code', display('coupon_code'), 'required');
		if ($this->form_validation->run() == FALSE){
            $this->session->set_userdata(array('error_msg'=>  validation_errors()));
	        redirect('view_cart');
        }else{
        	$coupon_code = $this->input->post('coupon_code');
			$result = $this->db->select('*')
								->from('coupon')
								->where('coupon_discount_code',$coupon_code)
								->where('status',1)
								->get()
								->row();

			if ($result) {
				$diff = abs(strtotime($result->end_date) - strtotime($result->start_date));
				$years 	= floor($diff / (365*60*60*24));
				$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
				$days 	= floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

				if ((!empty($days) || !empty($months) || !empty($years))) {

					if ($result->discount_type == 1) {

						$this->db->set('status', '2');
						$this->db->where('coupon_discount_code', $result->coupon_discount_code);
						$this->db->update('coupon');

						$this->session->unset_userdata('coupon_amnt');
				    	$this->session->set_userdata('coupon_amnt',$result->discount_amount); 
				    	$this->session->set_userdata('message','Your coupon is used'); 
						redirect('view_cart');
					}elseif ($result->discount_type == 2) {
						$this->db->set('status', '2');
						$this->db->where('coupon_discount_code', $result->coupon_discount_code);
						$this->db->update('coupon');

						$total_dis = ($this->cart->total() * $result->discount_percentage)/100;
						$this->session->unset_userdata('coupon_amnt');
				    	$this->session->set_userdata('coupon_amnt',$total_dis); 
				    	$this->session->set_userdata('message',display('your_coupon_is_used')); 
						redirect('view_cart');
					}

				}else{
					$this->session->set_userdata('error_msg', display('coupon_is_expired'));
					redirect('view_cart');
				}
			}else{
				$this->session->set_userdata('error_msg',display('your_coupon_is_used')); 
				redirect('view_cart');
			}
        }
	}
	//Add Wish list
	public function add_wishlist(){

		if (!$this->user_auth->is_logged()) {
			echo '3';
		}else{
			$data = array(
				'wishlist_id'=> $this->generator(15),
				'user_id'	 => $this->input->post('customer_id'), 
				'product_id' => $this->input->post('product_id'), 
				'status' 	 => '1', 
			);
			$add_wishlist 	= $this->Homes->add_wishlist($data);

			if ($add_wishlist) {
				echo '1';
			}else{
				echo '2';
			}	
		}
	}
	//Add Review
	public function add_review(){

		if (!$this->user_auth->is_logged()) {
			echo '3';
		}else{
			$data = array(
				'product_review_id'	=> $this->generator(15),
				'reviewer_id'	 	=> $this->input->post('customer_id'), 
				'product_id' 		=> $this->input->post('product_id'), 
				'comments' 	 		=> $this->input->post('review_msg'), 
				'rate' 	 			=> $this->input->post('rate'), 
				'status' 	 		=> '0', 
			);
			$add_review 	= $this->Homes->add_review($data);
			if ($add_review) {
				echo '1';
			}else{
				echo '2';
			}	
		}
	}
	//Change Language
	public function change_language(){
		$language = $this->input->post('language');
		if ($language) {
			$this->session->unset_userdata('language');
			$this->session->set_userdata('language',strtolower($language));
			echo '2';
		}else{
			echo '3';
		}
	}
	//Change Currency
	public function change_currency(){
		$currency_id = $this->input->post('currency_id');
		if ($currency_id) {
			$this->session->unset_userdata('currency_new_id');
			$this->session->set_userdata('currency_new_id',$currency_id);
			echo '2';
		}else{
			echo '3';
		}
	}
	//View Cart
	public function view_cart(){
		$content = $this->lhome->view_cart();
		$this->template->full_website_html_view($content);
	}
	//Retrive district
	public function retrive_district(){
		$country_id = $this->input->post('country_id');

		if ($country_id) {
			$select_district_info = $this->Homes->select_district_info($country_id);

			$html = "";
			if ($select_district_info) {
				$html .= "<select name=\"zone_id\" id=\"input-payment-zone\" class=\"form-control\" required=\"\">";
				foreach ($select_district_info as $district) {
					$html .= "<option value=".$district->name.">$district->name</option>";
				}
				$html .= "</select>";
				echo $html;
			}
		}
	}
	//checkout
	public function checkout(){
		$content = $this->lhome->checkout();
		$this->template->full_website_html_view($content);
	}
	//Submit checkout
	public function submit_checkout(){

		//Payment method
		$order_id 		 = $this->auth->generator(15);
		$customer_id 	 = $this->auth->generator(15);
		$payment_method  = $this->input->post('payment_method');
		$diff_ship_adrs  = $this->input->post('diff_ship_adrs');

		//If customer is logged
		if ($this->user_auth->is_logged()) {
			$customer_id = $this->session->userdata('customer_id');

			//Shipping data entry
    		$ship_country_id = $this->session->userdata('ship_country');
    		$ship_country 	 = $this->db->select('*')
			        				->from('countries')
			        				->where('id',$ship_country_id)
			        				->get()
			        				->row();

			$ship_short_address = "";
    		if ($this->session->userdata('city')) {
    			$ship_short_address .= $this->session->userdata('city').',';
    		}if ($this->session->userdata('state')) {
    			$ship_short_address .= $this->session->userdata('state').',';
    		}if ($country->name) {
    			$ship_short_address .= $country->name.',';
    		}if ($this->session->userdata('zip')) {
    			$ship_short_address .= $this->session->userdata('zip').',';
    		}if ($this->session->userdata('customer_address_1')) {
    			$ship_short_address .= $this->session->userdata('customer_address_1');
    		}

    		//New customer shipping entry
    		$shipping=array(
				'customer_id' 	=> $customer_id,
				'order_id' 		=> $order_id,
				'customer_name' => $this->session->userdata('ship_first_name').' '.$this->session->userdata('ship_last_name'),
				'first_name' 	=> $this->session->userdata('ship_first_name'),
				'last_name' 	=> $this->session->userdata('ship_last_name'),
				'customer_short_address'=> $ship_short_address,
				'customer_address_1' 	=> $this->session->userdata('ship_address_1'),
				'customer_address_2' 	=> $this->session->userdata('ship_address_2'),
				'city' 			=> $this->session->userdata('ship_city'),
				'state' 		=> $this->session->userdata('ship_state'),
				'country' 		=> $this->session->userdata('ship_country'),
				'zip' 			=> $this->session->userdata('ship_zip'),
				'company' 		=> $this->session->userdata('ship_company'),
				'customer_mobile'=> $this->session->userdata('ship_mobile'),
				'customer_email' => $this->session->userdata('ship_email'),
			);
			$this->Homes->shipping_entry($shipping);
		}else{
			$email 		= $this->session->userdata('customer_email');
			$phone 		= $this->session->userdata('customer_mobile');
			$password 	= $this->session->userdata('ac_pass');

			if ($diff_ship_adrs == 1) {
				$creat_ac = $this->input->post('creat_ac');
				if ($creat_ac == 1) {

					$exists = $this->Homes->customer_exist_check($email,$phone);
					if ($exists) {
						$this->session->set_userdata('error_message',display('customer_already_exists'));
						redirect(base_url('checkout'));
					}else{
						//Customer login info entry
						$customer_login=array(
							'customer_id' => $customer_id,
							'phone'		  => (!empty($phone)?$phone:null),
							'email'       => (!empty($email)?$email:null),
							'password'    => (!empty($password)?md5("gef".$password):null),
						);
						$this->db->insert('customer_login',$customer_login);
					}
				}

	    		//For creating customer short address
	    		$country_id = $this->session->userdata('country');
	    		$country 	= $this->db->select('*')
				        				->from('countries')
				        				->where('id',$country_id)
				        				->get()
				        				->row();

	    		$short_address = "";
	    		if ($this->session->userdata('city')) {
	    			$short_address .= $this->session->userdata('city').',';
	    		}if ($this->session->userdata('state')) {
	    			$short_address .= $this->session->userdata('state').',';
	    		}if ($country->name) {
	    			$short_address .= $country->name.',';
	    		}if ($this->session->userdata('zip')) {
	    			$short_address .= $this->session->userdata('zip').',';
	    		}if ($this->session->userdata('customer_address_1')) {
	    			$short_address .= $this->session->userdata('customer_address_1');
	    		}

	    		echo $short_address;
	    		exit();

		    	$billing=array(
					'customer_id' 	=> $customer_id,
					'customer_code' => $this->customer_number_generator(),
					'customer_name' => $this->session->userdata('first_name').' '.$this->session->userdata('last_name'),
					'first_name' 	=> $this->session->userdata('first_name'),
					'last_name' 	=> $this->session->userdata('last_name'),
					'customer_short_address'=> $short_address,
					'customer_address_1' => $this->session->userdata('customer_address_1'),
					'customer_address_2' => $this->session->userdata('customer_address_2'),
					'city' 			=> $this->session->userdata('city'),
					'state' 		=> $this->session->userdata('state'),
					'country' 		=> $this->session->userdata('country'),
					'zip' 			=> $this->session->userdata('zip'),
					'company' 		=> $this->session->userdata('company'),
					'customer_mobile'=> $this->session->userdata('customer_mobile'),
					'customer_email'=> $this->session->userdata('customer_email'),
					'image'			=> base_url().'my-assets/image/avatar.png',
				);

				//Billing information insert
		    	$this->db->insert('customer_information',$billing);

    			//Shipping data entry
	    		$ship_country_id = $this->session->userdata('ship_country');
	    		$ship_country 	 = $this->db->select('*')
				        				->from('countries')
				        				->where('id',$ship_country_id)
				        				->get()
				        				->row();
		    	$ship_short_address = "";
	    		if ($this->session->userdata('city')) {
	    			$ship_short_address .= $this->session->userdata('city').',';
	    		}if ($this->session->userdata('state')) {
	    			$ship_short_address .= $this->session->userdata('state').',';
	    		}if ($country->name) {
	    			$ship_short_address .= $country->name.',';
	    		}if ($this->session->userdata('zip')) {
	    			$ship_short_address .= $this->session->userdata('zip').',';
	    		}if ($this->session->userdata('customer_address_1')) {
	    			$ship_short_address .= $this->session->userdata('customer_address_1');
	    		}

	    		//New customer shipping entry
	    		$shipping=array(
					'customer_id' 	=> $customer_id,
					'order_id' 		=> $order_id,
					'customer_name' => $this->session->userdata('ship_first_name').' '.$this->session->userdata('ship_last_name'),
					'first_name' 	=> $this->session->userdata('ship_first_name'),
					'last_name' 	=> $this->session->userdata('ship_last_name'),
					'customer_short_address'=> $ship_short_address,
					'customer_address_1' 	=> $this->session->userdata('ship_address_1'),
					'customer_address_2' 	=> $this->session->userdata('ship_address_2'),
					'city' 			=> $this->session->userdata('ship_city'),
					'state' 		=> $this->session->userdata('ship_state'),
					'country' 		=> $this->session->userdata('ship_country'),
					'zip' 			=> $this->session->userdata('ship_zip'),
					'company' 		=> $this->session->userdata('ship_company'),
					'customer_mobile'=> $this->session->userdata('ship_mobile'),
					'customer_email' => $this->session->userdata('ship_email'),
				);
	    		$this->Homes->shipping_entry($shipping);
			}else{
				$creat_ac = $this->input->post('creat_ac');
				if ($creat_ac == 1) {
					$exists = $this->Homes->customer_exist_check($email,$phone);
					if ($exists) {
						$this->session->set_userdata('error_message',display('customer_already_exists'));
						redirect(base_url('checkout'));
					}else{
						//Customer login info entry
						$customer_login=array(
							'customer_id' => $customer_id,
							'phone'		  => (!empty($phone)?$phone:null),
							'email'       => (!empty($email)?$email:null),
							'password'    => (!empty($password)?md5("gef".$password):null),
						);
						$this->db->insert('customer_login',$customer_login);
					}
				}


				//For creating customer short address
	    		$country_id = $this->session->userdata('country');
	    		$country 	= $this->db->select('*')
				        				->from('countries')
				        				->where('id',$country_id)
				        				->get()
				        				->row();

				$short_address = "";
	    		if ($this->session->userdata('city')) {
	    			$short_address .= $this->session->userdata('city').',';
	    		}if ($this->session->userdata('state')) {
	    			$short_address .= $this->session->userdata('state').',';
	    		}if ($country->name) {
	    			$short_address .= $country->name.',';
	    		}if ($this->session->userdata('zip')) {
	    			$short_address .= $this->session->userdata('zip').',';
	    		}if ($this->session->userdata('customer_address_1')) {
	    			$short_address .= $this->session->userdata('customer_address_1');
	    		}

		    	$billing=array(
					'customer_id' 	=> $customer_id,
					'customer_code' => $this->customer_number_generator(),
					'customer_name' => $this->session->userdata('first_name').' '.$this->session->userdata('last_name'),
					'first_name' 	=> $this->session->userdata('first_name'),
					'last_name' 	=> $this->session->userdata('last_name'),
					'customer_short_address'=> $short_address,
					'customer_address_1' => $this->session->userdata('customer_address_1'),
					'customer_address_2' => $this->session->userdata('customer_address_2'),
					'city' 			=> $this->session->userdata('city'),
					'state' 		=> $this->session->userdata('state'),
					'country' 		=> $this->session->userdata('country'),
					'zip' 			=> $this->session->userdata('zip'),
					'company' 		=> $this->session->userdata('company'),
					'customer_mobile'=> $this->session->userdata('customer_mobile'),
					'customer_email'=> $this->session->userdata('customer_email'),
					'image'			=> base_url().'my-assets/image/avatar.png',
				);
	    		//Billing info entry
    			$this->Homes->billing_entry($billing);
	    		//Shipping info entry
	    		$this->Homes->shipping_entry($billing);
			}
		}
    	//Cash on delivery
    	if ($payment_method == 1) {
    		//Order entry
            $return_order_id = $this->Homes->order_entry($customer_id,$order_id);
    		$result   		 = $this->order_inserted_data($return_order_id);
    		$this->cart->destroy();
    		$array_items = array('customer_name', 'first_name','last_name','customer_short_address','customer_address_1','customer_address_2','city','state','country','zip','company','customer_mobile','customer_email');
			$this->session->unset_userdata($array_items);
    		redirect(base_url());
    	}
	}
	//Retrive right now inserted data to create html
	public function order_inserted_data($order_id)
	{	
		$CI =& get_instance();
		$CI->load->library('website/Lhome');
		return $content = $CI->lhome->order_html_data($order_id);	
	}
	#========Logout=======#
	public function logout()
	{	
		if ($this->user_auth->logout())
			redirect('checkout');
	}
	//This function is used to Generate Key
	public function generator($lenth)
	{
		$number=array("A","B","C","D","E","F","G","H","I","J","K","L","N","M","O","P","Q","R","S","U","V","T","W","X","Y","Z","1","2","3","4","5","6","7","8","9","0");
	
		for($i=0; $i<$lenth; $i++)
		{
			$rand_value=rand(0,34);
			$rand_number=$number["$rand_value"];
		
			if(empty($con))
			{ 
			$con=$rand_number;
			}
			else
			{
			$con="$con"."$rand_number";}
		}
		return $con;
	}
	//NUMBER GENERATOR
	public function customer_number_generator()
	{
		$this->db->select_max('customer_code');
		$query = $this->db->get('customer_information');	
		$result = $query->result_array();	
		$customer_code = $result[0]['customer_code'];
		if ($customer_code !='') {
			$customer_code = $customer_code + 1;	
		}else{
			$customer_code = 1000;
		}
		return $customer_code;		
	}
}