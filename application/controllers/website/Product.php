<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Product extends CI_Controller {

	function __construct() {
      	parent::__construct();
		$this->load->library('website/Lproduct');
		$this->load->model('website/Products_model');
		$this->load->model('Subscribers');
    }

	//Default loading for Product Index.
	public function index()
	{
		$content = $this->lproduct->Product_page();
		$this->template->full_website_html_view($content);
	}	

	//Product Details.
	public function product_details($p_id=null)
	{
//	    dd($p_id);
		$content = $this->lproduct->product_details($p_id);
		$this->template->full_website_html_view($content);
	}	

	//Seller product
	public function seller_product($seller_id=null)
	{
		$price_range = $this->input->get('price');
        $size        = $this->input->get('size');
        $sort        = $this->input->get('sort');
        $rate        = $this->input->get('rate');
        $seller_score  = $this->input->get('seller_score');

		$content = $this->lproduct->seller_product($seller_id,$price_range,$size,$sort,$rate,$seller_score);
		$this->template->full_website_html_view($content);
	}	

	//Brand product
	public function brand_product($brand_id=null)
	{
		$price_range = $this->input->get('price');
        $size        = $this->input->get('size');
        $sort        = $this->input->get('sort');
        $rate        = $this->input->get('rate');
        $seller_score  = $this->input->get('seller_score');

		$content = $this->lproduct->brand_product($brand_id,$price_range,$size,$sort,$rate,$seller_score);
		$this->template->full_website_html_view($content);
	}

	//Submit a subscriber.
	public function add_subscribe()
	{
		$data=array(
			'subscriber_id' => $this->generator(15),
			'apply_ip' 		=> $this->input->ip_address(),
			'email' 		=> $this->input->post('sub_email'),
			'status' 		=> 1
		);

		$result=$this->Subscribers->subscriber_entry($data);

		if ($result) {
			echo "2";
		}else{
			echo "3";
		}
	}

	//Submit rating
	public function submit_review(){
		$user_id = $this->session->userdata('customer_id');
		if ($user_id) {
			$data = array(
				'product_id'  => $this->input->post('product_id'),
				'rate'		  => $this->input->post('rate'),
				'reviewer_id' => $this->session->userdata('customer_id'),
				'title' 	  => $this->input->post('title'),
				'comments' 	  => $this->input->post('review_msg'),
				'date_time'   => date("Y-m-d h:i:s"),
				'status'      => 1,
			);

			$result = $this->db->select('*')
							->from('product_review')
							->where('reviewer_id',$data['reviewer_id'])
							->where('product_id',$data['product_id'])
							->get()
							->num_rows();
			
			if ($result > 0) {
				echo "2";
			}else{
				$result= $this->db->insert('product_review',$data);
				if ($result) {
					echo '1';
				}else{
					echo '2';
				}
			}
		}else{
			echo "3";
		}
	}	

	//Submit seller rating
	public function submit_seller_review(){
		$user_id = $this->session->userdata('customer_id');
		if ($user_id) {
			$data = array(
				'seller_id'   => $this->input->post('seller_id'),
				'rate'		  => $this->input->post('rate'),
				'reviewer_id' => $this->session->userdata('customer_id'),
				'title' 	  => $this->input->post('title'),
				'comments' 	  => $this->input->post('review_msg'),
				'date_time'   => date("Y-m-d h:i:s"),
				'status'      => 1,
			);

			$result = $this->db->select('*')
							->from('seller_review')
							->where('reviewer_id',$data['reviewer_id'])
							->where('seller_id',$data['seller_id'])
							->get()
							->num_rows();
			
			if ($result > 0) {
				echo "2";
			}else{
				$result= $this->db->insert('seller_review',$data);
				if ($result) {
					echo '1';
				}else{
					echo '2';
				}
			}
		}else{
			echo "3";
		}
	}

	//This function is used to Generate Key
	public function generator($lenth)
	{
		$number=array("A","B","C","D","E","F","G","H","I","J","K","L","N","M","O","P","Q","R","S","U","V","T","W","X","Y","Z","1","2","3","4","5","6","7","8","9","0");
	
		for($i=0; $i<$lenth; $i++)
		{
			$rand_value=rand(0,34);
			$rand_number=$number["$rand_value"];
		
			if(empty($con)){ 
				$con=$rand_number;
			}
			else{
				$con="$con"."$rand_number";
			}
		}
		return $con;
	}
}