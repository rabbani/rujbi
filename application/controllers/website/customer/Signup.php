<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Signup extends CI_Controller {

	function __construct() {
      	parent::__construct();
		$this->load->library('website/customer/Lsignup');
		$this->load->model('website/customer/Signups');
		$this->load->model('Subscribers');
		$this->load->model('Soft_settings');
		$this->load->model('Companies');
		$this->load->model('Email_templates');
    }

	//Default loading for Home Index.
	public function index()
	{
		$content = $this->lsignup->signup_page();
		$this->template->full_website_html_view($content);
	}

	//Submit a user signup.
	public function user_signup()
	{
		$this->form_validation->set_rules('first_name', display('first_name'), 'trim|required');
		$this->form_validation->set_rules('last_name', display('last_name'), 'trim|required');
		$this->form_validation->set_rules('password', display('password'), 'trim|required');
		$this->form_validation->set_rules('phone', display('phone'), 'trim|required');

		if ($this->form_validation->run() == FALSE)
        {
        	$data = array(
				'title' => display('sign_up')
			);
			$this->session->set_userdata('error_message',validation_errors());
        	redirect('signup');
        }else{

			$data=array(
				'customer_id' 	=> $this->generator(15),
				'customer_code' => $this->number_generator(),
				'first_name' 	=> $this->input->post('first_name'),
				'last_name' 	=> $this->input->post('last_name'),
				'customer_name' => $this->input->post('first_name').' '.$this->input->post('last_name'),
				'customer_email'=> (!empty($this->input->post('email'))?$this->input->post('email'):null),
				'customer_mobile'=> $this->input->post('phone'),
				'image' 		=> base_url('assets/dist/img/user.png'),
				'status' 		=> 1,
			);
			$result=$this->Signups->user_signup($data);
			if ($result) {

				//send email with as a link
	            $setting_detail = $this->Soft_settings->retrieve_email_editdata();
	            $company_info   = $this->Companies->company_list();
	            $template 		= $this->Email_templates->retrieve_template('8');

	            $config = array(
	                'protocol'      => $setting_detail[0]['protocol'],
	                'smtp_host'     => $setting_detail[0]['smtp_host'],
	                'smtp_port'     => $setting_detail[0]['smtp_port'],
	                'smtp_user'     => $setting_detail[0]['sender_email'], 
	                'smtp_pass'     => $setting_detail[0]['password'], 
	                'mailtype'      => $setting_detail[0]['mailtype'], 
	                'charset'       => 'utf-8'
	            );
	            $this->email->initialize($config);
	            $this->email->set_mailtype($setting_detail[0]['mailtype']);
	            $this->email->set_newline("\r\n");

	            //Email Message Set
	            $message  = !empty($template->message)?$template->message:null;
	            $message .= "<p>".display('your_information')."

	            			<table style=\"border:1px solid\">
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('first_name').":</th>
									<td style=\"border: 1px solid;\">".$data['first_name']."</td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('last_name').":</th>
									<td style=\"border: 1px solid;\">".$data['last_name']."</td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('email').":</th>
									<td style=\"border: 1px solid;\">".$data['customer_email']."</td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('password').":</th>
									<td style=\"border: 1px solid;\">".$this->input->post('password')."</td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('phone').":</th>
									<td style=\"border: 1px solid;\">".$data['customer_mobile']."</td>
								</tr>
							</table>";
	            
	            //Email content
	            $this->email->to($data['customer_email']);
	            $this->email->from($setting_detail[0]['sender_email'],$company_info[0]['company_name']);
	            $this->email->subject(!empty($template->subject)?$template->subject:null);
	            $this->email->message($message);

			    $email = $this->test_email($data['customer_email']);
				if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
				    if($this->email->send())
				    {
				    	$this->session->set_userdata(array(
							'message' 		=> display('you_have_successfully_signup'),
							'customer_email'=> $this->input->post('email'),
						));
				      	redirect(base_url('signup'));
				    }else{
				    	$this->session->set_userdata(array(
							'error_message' => display('email_was_not_sent_please_contact_administrator'),
							'customer_email'=> $this->input->post('email'),
						));
				     	redirect(base_url('signup'));
				    }
				}else{
					$this->session->set_userdata(array(
						'message' => display('your_email_was_not_found'),
						'customer_email'=> $this->input->post('email'),
					));
			     	redirect(base_url('signup'));
				}
			}else{
				$this->session->set_userdata(array('error_message'=>display('phone_no_or_email_already_exists')));
				redirect(base_url('signup'));
			}
		}
	}
	//Email testing for email
	public function test_email($data) {
	  	$data = trim($data);
	  	$data = stripslashes($data);
	  	$data = htmlspecialchars($data);
	  	return $data;
	}
	//Google plus sign up customer
	public function google_plus(){
		if (isset($_GET['code'])) {
			$customer_id = $this->generator(15);

			$this->googleplus->getAuthenticate();
			$this->session->set_userdata('user_profile',$this->googleplus->getUserInfo());
			$user_profile = $this->session->userdata('user_profile');

			//Explode image ext
			$last = explode('.', $user_profile['picture']);
			$path = FCPATH.'assets/dist/img/profile_picture/'.$customer_id.'.'.end($last);

			$arrContextOptions=array(
			    "ssl"=>array(
			        "verify_peer"=>false,
			        "verify_peer_name"=>false,
			    ),
			);  
			$response  = file_get_contents($user_profile['picture'], false, stream_context_create($arrContextOptions));
			file_put_contents($path, $response);
			$image_url = 'assets/dist/img/profile_picture/'.$customer_id.'.'.end($last);

			if ($user_profile['email']) {

				$data=array(
					'customer_id' 	=> $customer_id,
					'customer_name' => $user_profile['name'],
					'first_name' 	=> $user_profile['given_name'],
					'last_name' 	=> $user_profile['family_name'],
					'customer_email'=> $user_profile['email'],
					'image' 		=> base_url().$image_url,
					'password'		=> md5("gef".$user_profile['email']),
					'status' 		=> 0,
				);
				$result=$this->Signups->user_signup($data);

				if ($result) {

					$key = md5(time());
					$key = str_replace("1", "z", $key);
					$key = str_replace("2", "J", $key);
					$key = str_replace("3", "y", $key);
					$key = str_replace("4", "R", $key);
					$key = str_replace("5", "Kd", $key);
					$key = str_replace("6", "jX", $key);
					$key = str_replace("7", "dH", $key);
					$key = str_replace("8", "p", $key);
					$key = str_replace("9", "Uf", $key);
					$key = str_replace("0", "eXnyiKFj", $key);
					$customer_sid_web = substr($key, rand(0, 3), rand(28, 32));
					
					// codeigniter session stored data			
					$user_data = array(
						'customer_sid_web' => $customer_sid_web,
						'customer_id' 	   => $customer_id,
						'customer_name'    => $user_profile['name'],
						//customer shipping info
					 	'customer_email'   => $user_profile['email'], 
					 	'message' 		   => display('you_have_successfully_signup'),
					);
		          	$this->session->set_userdata($user_data);
					redirect(base_url(''));
				}else{
					$this->session->set_userdata(array('error_message'=>display('you_have_not_sign_up')));
					redirect(base_url(''));
				}
			}else{
				$this->session->set_userdata(array('error_message'=>display('you_have_not_sign_up')));
				redirect(base_url(''));
			}
		}else{
			$this->session->set_userdata(array('error_message'=>display('you_have_not_sign_up')));
			redirect(base_url(''));
		}
	}
	//This function is used to Generate Key
	public function generator($lenth)
	{
		$number=array("A","B","C","D","E","F","G","H","I","J","K","L","N","M","O","P","Q","R","S","U","V","T","W","X","Y","Z","1","2","3","4","5","6","7","8","9","0");
	
		for($i=0; $i<$lenth; $i++)
		{
			$rand_value=rand(0,34);
			$rand_number=$number["$rand_value"];
		
			if(empty($con))
			{ 
			$con=$rand_number;
			}
			else
			{
			$con="$con"."$rand_number";}
		}
		return $con;
	}
	//NUMBER GENERATOR
	public function number_generator()
	{
		$this->db->select_max('customer_code');
		$query = $this->db->get('customer_information');	
		$result = $query->result_array();	
		$customer_code = $result[0]['customer_code'];
		if ($customer_code !='') {
			$customer_code = $customer_code + 1;	
		}else{
			$customer_code = 1000;
		}
		return $customer_code;		
	}
}