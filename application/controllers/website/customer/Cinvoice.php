<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cinvoice extends CI_Controller {
	
	function __construct() {
      	parent::__construct();
      	$this->load->model('website/customer/Invoices');
      	$this->load->library('website/customer/linvoice');
      	$this->user_auth->check_customer_auth();
    }

    //Cinvoice default index load
	public function index()
	{
		$content = $this->linvoice->invoice_add_form();
		$this->template->full_customer_html_view($content);
	}

	//Manage invoice 
	public function manage_invoice()
	{
		$invoice_no = $this->input->get('invoice_no');
		$date 		= $this->input->get('date');

		#
        #pagination starts
        #
        $config["base_url"] 	    = base_url('customer/invoice/');
        $config['reuse_query_string']= true;
        $config["total_rows"] 	    = $this->Invoices->invoice_count($invoice_no,$date);
        $config["per_page"] 	    = 10;
        $config["uri_segment"] 	    = 3;
        $config["num_links"] 	    = 5;
	    /* This Application Must Be Used With BootStrap 3 * */
        $config['full_tag_open'] 	= "<ul class='pagination'>";
        $config['full_tag_close'] 	= "</ul>";
        $config['num_tag_open'] 	= '<li>';
        $config['num_tag_close'] 	= '</li>';
        $config['cur_tag_open'] 	= "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] 	= "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] 	= "<li>";
        $config['next_tag_close'] 	= "</li>";
        $config['prev_tag_open'] 	= "<li>";
        $config['prev_tagl_close'] 	= "</li>";
        $config['first_tag_open'] 	= "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] 	= "<li>";
        $config['last_tagl_close'] 	= "</li>";
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $links = $this->pagination->create_links();
        #
        #pagination ends
        #

        $content = $this->linvoice->invoice_list($links,$config["per_page"],$page,$invoice_no,$date);
		$this->template->full_customer_html_view($content);
	}

	//Retrive right now inserted data to cretae html
	public function invoice_inserted_data($invoice_id)
	{	
		$content = $this->linvoice->invoice_html_data($invoice_id);		
		$this->template->full_customer_html_view($content);
	}
}