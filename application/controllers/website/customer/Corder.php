<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Corder extends CI_Controller {
	
	function __construct() {
      	parent::__construct();
      	$this->user_auth->check_customer_auth();
      	$this->load->library('website/customer/Lorder');
      	$this->load->model('website/customer/Orders');
      	date_default_timezone_set('Asia/Dhaka');
    }
    //Index page load first
	public function index()
	{
		$content = $this->lorder->order_add_form();
		$this->template->full_customer_html_view($content);
	}
	//Add new order
	public function new_order()
	{
		$content = $this->lorder->order_add_form();
		$this->template->full_customer_html_view($content);
	}
	//Insert order
	public function insert_order()
	{
		$order_id = $this->Orders->order_entry();
		$this->session->set_userdata(array('message'=>display('successfully_added')));
		$this->order_inserted_data($order_id);
		redirect(base_url('website/customer/Corder/manage_order'));
	}
	//Retrive right now inserted data to cretae html
	public function order_inserted_data($order_id)
	{
		$content = $this->lorder->order_html_data($order_id);		
		$this->template->full_customer_html_view($content);
	}
	//Retrive right now inserted data to create html
	public function order_details_data($order_id)
	{
		$content = $this->lorder->order_details_data($order_id);	
		$this->template->full_customer_html_view($content);
	}
	//Order Update Form
	public function order_update_form($order_id)
	{
		$content = $this->lorder->order_edit_data($order_id);
		$this->template->full_customer_html_view($content);
	}
	//Retrive product data
	public function retrieve_product_data()
	{
		$product_id 	= $this->input->post('product_id');
		$product_info 	= $this->Orders->get_total_product($product_id);
		echo json_encode($product_info);
	}
	//Update order
	public function order_update(){
		$order_id = $this->Orders->update_order();
		$this->session->set_userdata(array('message'=>display('successfully_updated')));
		$this->order_inserted_data($order_id);
		redirect(base_url('customer/order/manage_order'));
	}
	// Order delete
	public function order_delete($order_id)
	{
		$result = $this->Orders->delete_order($order_id);
		if ($result) {
			$this->session->set_userdata(array('message'=>display('successfully_delete')));
			redirect('customer/order/manage_order');
		}	
	}	
	// Order cancel
	public function order_cancel($order_id)
	{
		$result = $this->Orders->order_cancel($order_id);
		if ($result) {
			$this->session->set_userdata(array('message'=>display('successfully_order_cancelled')));
			redirect('customer/order/manage_order');
		}	
	}
	//Manage order
	public function manage_order()
	{

		$order_no 		= $this->input->get('order_no');
		$date 			= $this->input->get('date');
		$order_status 	= $this->input->get('order_status');

		#
        #pagination starts
        #
        $config["base_url"] 	    = base_url('customer/order/manage_order/');
        $config['reuse_query_string']= true;
        $config["total_rows"] 	    = $this->Orders->order_count($order_no,$date,$order_status);
        $config["per_page"] 	    = 10;
        $config["uri_segment"] 	    = 4;
        $config["num_links"] 	    = 5; 
	    /* This Application Must Be Used With BootStrap 3 * */
        $config['full_tag_open'] 	= "<ul class='pagination'>";
        $config['full_tag_close'] 	= "</ul>";
        $config['num_tag_open'] 	= '<li>';
        $config['num_tag_close'] 	= '</li>';
        $config['cur_tag_open'] 	= "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] 	= "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] 	= "<li>";
        $config['next_tag_close'] 	= "</li>";
        $config['prev_tag_open'] 	= "<li>";
        $config['prev_tagl_close'] 	= "</li>";
        $config['first_tag_open'] 	= "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] 	= "<li>";
        $config['last_tagl_close'] 	= "</li>";
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $links = $this->pagination->create_links();
        #
        #pagination ends
        #

        $content = $this->lorder->order_list($links,$config["per_page"],$page,$order_no,$date,$order_status);
		$this->template->full_customer_html_view($content);
	}
	//Order Tracking
	public function order_tracking()
	{

		$order_no 		= $this->input->get('order_no');
		$date 			= $this->input->get('date');
		$order_status 	= $this->input->get('order_status');

		#
        #pagination starts
        #
        $config["base_url"] 	    = base_url('customer/order/order_tracking/');
        $config['reuse_query_string']= true;
        $config["total_rows"] 	    = $this->Orders->order_count($order_no,$date,$order_status);
        $config["per_page"] 	    = 10;
        $config["uri_segment"] 	    = 4;
        $config["num_links"] 	    = 5; 
	    /* This Application Must Be Used With BootStrap 3 * */
        $config['full_tag_open'] 	= "<ul class='pagination'>";
        $config['full_tag_close'] 	= "</ul>";
        $config['num_tag_open'] 	= '<li>';
        $config['num_tag_close'] 	= '</li>';
        $config['cur_tag_open'] 	= "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] 	= "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] 	= "<li>";
        $config['next_tag_close'] 	= "</li>";
        $config['prev_tag_open'] 	= "<li>";
        $config['prev_tagl_close'] 	= "</li>";
        $config['first_tag_open'] 	= "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] 	= "<li>";
        $config['last_tagl_close'] 	= "</li>";
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $links = $this->pagination->create_links();
        #
        #pagination ends
        #

        $content = $this->lorder->order_tracking($links,$config["per_page"],$page,$order_no,$date,$order_status);
		$this->template->full_customer_html_view($content);
	}
	//Order traking
	public function order_traking($order_id=null){
		$content = $this->lorder->order_traking($order_id);
		$this->template->full_customer_html_view($content);
	}
	//Submit message for order tracking
	public function submit_message($order_id=null){
		$message 		= $this->input->post('message');
		//Insert data into order tracking
		$order_tracking=array(
			'order_id'	=>	$order_id,
			'customer_id'	=>	$this->session->userdata('customer_id'),
			'order_status'	=>	8,
			'date'		=>	date("Y-m-d h:i a"),
			'message'   => $message, 
		);
		$result = $this->db->insert('order_tracking',$order_tracking);

		if ($result) {
			$this->session->set_userdata(array('message'=>display('message_send_successfully')));
			redirect('customer/order/order_traking/'.$order_id);
		}
	}

}