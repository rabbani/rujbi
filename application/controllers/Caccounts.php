<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Caccounts extends CI_Controller {
	function __construct() 
    {
        parent::__construct();
        $this->load->library('laccounts');
        $this->load->model('Accounts');
        $this->load->model('Sellers');
        $this->permission->module('accounts')->redirect();
    }
	#============Default Load Add Payment========# 
  	public function index()
  	{
        $this->permission->check_label('payment')->create()->redirect();
        $seller_list = $this->Sellers->seller_list();
        $data = array(
            'title'       => display('add_payment'), 
            'seller_list' => $seller_list
        );
        $content = $this->parser->parse('accounts/add_payment',$data,true);
        $this->template->full_admin_html_view($content);   
  	}
    #===============Add Payment========#    
    public function add_payment()
    {
        $this->permission->check_label('payment')->create()->redirect();
        $seller_list = $this->Sellers->seller_list();
        $data = array(
            'title'       => display('add_payment'), 
            'seller_list' => $seller_list
        );

        $content = $this->parser->parse('accounts/add_payment',$data,true);
        $this->template->full_admin_html_view($content);
    }
    #===============Payment entry==============#
    public function payment_entry()
    {
        $this->permission->check_label('payment')->create()->redirect();
        $data = array(
                'transaction_id'=>  $this->auth->generator(10),
                'date'          =>  $this->input->post('payment_date'),
                'seller_id'     =>  $this->input->post('seller_id'),
                'amount'        =>  $this->input->post('amount'),
                'description'   =>  $this->input->post('description'), 
            );
        $result = $this->Accounts->payment_entry($data);
        if($result){
            $this->session->set_userdata(array('message'=>display('successfully_payment_paid')));
            redirect('caccounts/manage_payment/all/item');
        }else{
            $this->session->set_userdata(array('error_message'=>display('payment_not_paid')));
            redirect('caccounts/manage_payment/all/item');
        }
    }
    #===============Accounts summary==========#
    public function manage_payment()
    {
        $this->permission->check_label('manage_payment')->redirect();
        $seller_id = $this->input->get('seller_id');
        $date = $this->input->get('date');

        #
        #pagination starts
        #
        $config["base_url"]         = base_url('caccounts/manage_payment/all/item');
        $config['reuse_query_string']= true;
        $config["total_rows"]       = $this->Accounts->manage_payment_count($seller_id,$date);
        $config["per_page"]         = 10;
        $config["uri_segment"]      = 5;
        $config["num_links"]        = 5; 
        /* This Application Must Be Used With BootStrap 3 * */
        $config['full_tag_open']    = "<ul class='pagination'>";
        $config['full_tag_close']   = "</ul>";
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
        $config['cur_tag_open']     = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close']    = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open']    = "<li>";
        $config['next_tag_close']   = "</li>";
        $config['prev_tag_open']    = "<li>";
        $config['prev_tagl_close']  = "</li>";
        $config['first_tag_open']   = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open']    = "<li>";
        $config['last_tagl_close']  = "</li>";
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $links = $this->pagination->create_links();
        #
        #pagination ends
        #
        $seller_list  = $this->Sellers->seller_list();
        $payment_list = $this->Accounts->payment_list($config["per_page"],$page,$seller_id,$date);
        $SubTotalAmnt = 0;
        if(!empty($payment_list)){
            $i=0;
            foreach($payment_list as $k=>$v){
                $SubTotalAmnt = $SubTotalAmnt+$v->amount;
            }
        }

        $currency_details = $this->Soft_settings->retrieve_currency_info();
        $data=array(
            'title'=> display('manage_payment'),
            'payment_list'  => $payment_list,
            'SubTotalAmnt'  => $SubTotalAmnt,
            'seller_list'   => $seller_list,
            'links'         => $links,
            'seller_id'     => $seller_id,
            'date'          => $date,
            'currency'      => $currency_details[0]['currency_icon'],
            'position'      => $currency_details[0]['currency_position'],
        );
      
        $content = $this->parser->parse('accounts/manage_payment',$data,true);
        $this->template->full_admin_html_view($content);  
    }
    #================Payment Update Form===========#
    public function payment_update_form($id='')
    {
        $this->permission->check_label('manage_payment')->update()->redirect();
        $seller_list     = $this->Sellers->seller_list();
        $payment_details = $this->Accounts->retrive_payment_edit_data($id);
        $data = array(
            'title'      => display('edit_payment'), 
            'id'         => $payment_details->id,
            'date'       => $payment_details->date,
            'seller_id'  => $payment_details->seller_id,
            'amount'     => $payment_details->amount,
            'description'=> $payment_details->description,
            'seller_list'=> $seller_list,
        );
        $content = $this->parser->parse('accounts/edit_payment',$data,true);
        $this->template->full_admin_html_view($content);
    }
    #===============Update Payment==============#
    public function update_payment($id=null)
    {
        $this->permission->check_label('manage_payment')->update()->redirect();
        $data = array(
                'date'          =>  $this->input->post('payment_date'),
                'seller_id'     =>  $this->input->post('seller_id'),
                'amount'        =>  $this->input->post('amount'),
                'description'   =>  $this->input->post('description'), 
            );
        $result = $this->Accounts->update_payment($data,$id);
        if($result){
            $this->session->set_userdata(array('message'=>display('successfully_payment_paid')));
            redirect('caccounts/manage_payment/all/item');
        }else{
            $this->session->set_userdata(array('error_message'=>display('payment_not_paid')));
            redirect('caccounts/manage_payment/all/item');
        }
    }
    #================Delete Payment===========#
    public function payment_delete($id='')
    {
        $this->permission->check_label('manage_payment')->delete()->redirect();
        $this->db->where('id', $id);
        $result = $this->db->delete('payment');
        if($result){
            $this->session->set_userdata(array('message'=>display('delete_successfully')));
            redirect('caccounts/manage_payment/all/item');
        }else{
            $this->session->set_userdata(array('error_message'=>display('payment_not_paid')));
            redirect('caccounts/manage_payment/all/item');
        }
    }
    #=============Payment Report========#
    public function payment_report()
    {

        $this->permission->check_label('payment_report')->read()->redirect();
        $seller_id = $this->input->get('seller_id');

        #
        #pagination starts
        #
        $config["base_url"]         = base_url('caccounts/payment_report/all/item/');
        $config['reuse_query_string']= true;
        $config["total_rows"]       = $this->Accounts->seller_list_count($seller_id);
        $config["per_page"]         = 10;
        $config["uri_segment"]      = 5;
        $config["num_links"]        = 5; 
        /* This Application Must Be Used With BootStrap 3 * */
        $config['full_tag_open']    = "<ul class='pagination'>";
        $config['full_tag_close']   = "</ul>";
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
        $config['cur_tag_open']     = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close']    = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open']    = "<li>";
        $config['next_tag_close']   = "</li>";
        $config['prev_tag_open']    = "<li>";
        $config['prev_tagl_close']  = "</li>";
        $config['first_tag_open']   = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open']    = "<li>";
        $config['last_tagl_close']  = "</li>";
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $links = $this->pagination->create_links();
        #
        #pagination ends
        #

        $seller_list    = $this->Sellers->seller_list();
        $seller_list_report    = $this->Accounts->seller_list($config["per_page"],$page,$seller_id);
        $payment_report = array();

        $SubTotalBlance=0;
        $SubTotalComission=0;
        $SubTotalPayBlance=0;
        $SubTotalPaidBlance=0;
        $SubTotalUnPaidBlance=0;

        if(!empty($seller_list_report)){
            $i=0;
            foreach($seller_list_report as $k=>$seller){
                $i++;
                $payment_report[$k]['sl']       = $i;
                $payment_report[$k]['seller_name']     = $seller['first_name'].' '.$seller['last_name'];
                $payment_report[$k]['total_balance']   = $this->Accounts->total_balance($seller['seller_id']);
                $payment_report[$k]['total_comission'] = $this->Accounts->total_comission($seller['seller_id']);
                $payment_report[$k]['payable_balance'] = $payment_report[$k]['total_balance'] - $payment_report[$k]['total_comission'];
                $payment_report[$k]['total_paid_amount']  =  $this->Accounts->total_paid_amount($seller['seller_id']);
                $payment_report[$k]['total_unpaid_amount'] =  $payment_report[$k]['payable_balance'] - $payment_report[$k]['total_paid_amount'];

                $SubTotalBlance    = $SubTotalBlance + $payment_report[$k]['total_balance'];
                $SubTotalComission = $SubTotalComission + $payment_report[$k]['total_comission'];
                $SubTotalPayBlance = $SubTotalPayBlance + $payment_report[$k]['payable_balance'];
                $SubTotalPaidBlance = $SubTotalPaidBlance + $payment_report[$k]['total_paid_amount'];
                $SubTotalUnPaidBlance = $SubTotalUnPaidBlance + $payment_report[$k]['total_unpaid_amount'];
            }
        }

        $currency_details = $this->Soft_settings->retrieve_currency_info();
        $data=array(
            'title'               => display('payment_report'),
            'payment_report'      => $payment_report,
            'seller_list'         => $seller_list,
            'SubTotalBlance'      => $SubTotalBlance,
            'SubTotalComission'   => $SubTotalComission,
            'SubTotalPayBlance'   => $SubTotalPayBlance,
            'SubTotalPaidBlance'  => $SubTotalPaidBlance,
            'SubTotalUnPaidBlance'=> $SubTotalUnPaidBlance,
            'links'         => $links,
            'seller_id'     => $seller_id,
            'currency'      => $currency_details[0]['currency_icon'],
            'position'      => $currency_details[0]['currency_position'],
        );
      
        $content = $this->parser->parse('accounts/payment_report',$data,true);
        $this->template->full_admin_html_view($content);  
    }

    #=============Paid Seller Payment Report========#
    public function paid_seller_report()
    {
        $this->permission->check_label('paid')->read()->redirect();
        $seller_id = $this->input->get('seller_id');

        $seller_list           = $this->Sellers->seller_list();
        $seller_list_report    = $this->Accounts->seller_list_by_id($seller_id);
        $payment_report = array();

        $SubTotalBlance=0;
        $SubTotalComission=0;
        $SubTotalPayBlance=0;
        $SubTotalPaidBlance=0;
        $SubTotalUnPaidBlance=0;

        if(!empty($seller_list_report)){
            $i=0;
            foreach($seller_list_report as $k=>$seller){
                $i++;
                $payment_report[$k]['sl']       = $i;
                $payment_report[$k]['seller_name']     = $seller['first_name'].' '.$seller['last_name'];
                $payment_report[$k]['total_balance']   = $this->Accounts->total_balance($seller['seller_id']);
                $payment_report[$k]['total_comission'] = $this->Accounts->total_comission($seller['seller_id']);
                $payment_report[$k]['payable_balance'] = $payment_report[$k]['total_balance'] - $payment_report[$k]['total_comission'];
                $payment_report[$k]['total_paid_amount']  =  $this->Accounts->total_paid_amount($seller['seller_id']);
                $payment_report[$k]['total_unpaid_amount'] =  $payment_report[$k]['payable_balance'] - $payment_report[$k]['total_paid_amount'];

                if (($payment_report[$k]['total_paid_amount'] > 0) && ($payment_report[$k]['total_paid_amount'] >= $payment_report[$k]['payable_balance'])) {

                    $SubTotalBlance    = $SubTotalBlance + $payment_report[$k]['total_balance'];
                    $SubTotalComission = $SubTotalComission + $payment_report[$k]['total_comission'];
                    $SubTotalPayBlance = $SubTotalPayBlance + $payment_report[$k]['payable_balance'];
                    $SubTotalPaidBlance = $SubTotalPaidBlance + $payment_report[$k]['total_paid_amount'];
                    $SubTotalUnPaidBlance = $SubTotalUnPaidBlance + $payment_report[$k]['total_unpaid_amount'];
                }
            }
        }

        $currency_details = $this->Soft_settings->retrieve_currency_info();
        $data=array(
            'title'               => display('payment_report'),
            'payment_report'      => $payment_report,
            'seller_list'         => $seller_list,
            'SubTotalBlance'      => $SubTotalBlance,
            'SubTotalComission'   => $SubTotalComission,
            'SubTotalPayBlance'   => $SubTotalPayBlance,
            'SubTotalPaidBlance'  => $SubTotalPaidBlance,
            'SubTotalUnPaidBlance'=> $SubTotalUnPaidBlance,
            'seller_id'     => $seller_id,
            'currency'      => $currency_details[0]['currency_icon'],
            'position'      => $currency_details[0]['currency_position'],
        );

        $content = $this->parser->parse('accounts/paid_seller_report',$data,true);
        $this->template->full_admin_html_view($content);
    }
    #=============Un Paid Seller Payment Report========#
    public function unpaid_seller_report()
    {
        $this->permission->check_label('unpaid')->read()->redirect();
        $seller_id = $this->input->get('seller_id');

        $seller_list           = $this->Sellers->seller_list();
        $seller_list_report    = $this->Accounts->seller_list_by_id($seller_id);
        $payment_report = array();

        $SubTotalBlance=0;
        $SubTotalComission=0;
        $SubTotalPayBlance=0;
        $SubTotalPaidBlance=0;
        $SubTotalUnPaidBlance=0;

        if(!empty($seller_list)){
            $i=0;
            foreach($seller_list as $k=>$seller){
                $i++;
                $payment_report[$k]['sl']       = $i;
                $payment_report[$k]['seller_name']     = $seller['first_name'].' '.$seller['last_name'];
                $payment_report[$k]['total_balance']   = $this->Accounts->total_balance($seller['seller_id']);
                $payment_report[$k]['total_comission'] = $this->Accounts->total_comission($seller['seller_id']);
                $payment_report[$k]['payable_balance'] = $payment_report[$k]['total_balance'] - $payment_report[$k]['total_comission'];
                $payment_report[$k]['total_paid_amount']  =  $this->Accounts->total_paid_amount($seller['seller_id']);
                $payment_report[$k]['total_unpaid_amount'] =  $payment_report[$k]['payable_balance'] - $payment_report[$k]['total_paid_amount'];

                if (($payment_report[$k]['total_paid_amount'] == 0) && $payment_report[$k]['payable_balance'] > 0) {

                    $SubTotalBlance    = $SubTotalBlance + $payment_report[$k]['total_balance'];
                    $SubTotalComission = $SubTotalComission + $payment_report[$k]['total_comission'];
                    $SubTotalPayBlance = $SubTotalPayBlance + $payment_report[$k]['payable_balance'];
                    $SubTotalPaidBlance = $SubTotalPaidBlance + $payment_report[$k]['total_paid_amount'];
                    $SubTotalUnPaidBlance = $SubTotalUnPaidBlance + $payment_report[$k]['total_unpaid_amount'];
                }
            }
        }

        $currency_details = $this->Soft_settings->retrieve_currency_info();
        $data=array(
            'title'               => display('payment_report'),
            'payment_report'      => $payment_report,
            'seller_list'         => $seller_list,
            'SubTotalBlance'      => $SubTotalBlance,
            'SubTotalComission'   => $SubTotalComission,
            'SubTotalPayBlance'   => $SubTotalPayBlance,
            'SubTotalPaidBlance'  => $SubTotalPaidBlance,
            'SubTotalUnPaidBlance'=> $SubTotalUnPaidBlance,
            'seller_id'     => $seller_id,
            'currency'      => $currency_details[0]['currency_icon'],
            'position'      => $currency_details[0]['currency_position'],
        );

        $content = $this->parser->parse('accounts/unpaid_seller_report',$data,true);
        $this->template->full_admin_html_view($content);
    }
}
