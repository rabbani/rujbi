<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User extends CI_Controller {
	
	function __construct() {
      	parent::__construct(); 
		$this->load->library('lusers');
		$this->load->model('Userm');
    }
    #==============User page load============#
	public function index()
	{
		$this->permission->check_label('add_user')->create()->redirect();
		$content = $this->lusers->user_add_form();
		$this->template->full_admin_html_view($content);
	}
	#==============Insert User==============#
	public function insert_user()
	{
		$this->permission->check_label('add_user')->create()->redirect();
		$data=array(
			'first_name'=> $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'email' 	=> $this->input->post('email'),
			'password' 	=> md5("gef".$this->input->post('password')),
			'logo' 		=> 'assets/website/image/login.png',
			'status' 	=> 1
			);

		$result = $this->lusers->insert_user($data);
		if ($result) {
			$this->session->set_userdata(array('message'=>display('successfully_added')));
			if(isset($_POST['add-user'])){
				redirect('manage_user');
			}elseif(isset($_POST['add-user-another'])){
				redirect(base_url('manage_user'));
			}
		}else{
			$this->session->set_userdata(array('error_message'=>display('already_exists')));
			redirect(base_url('manage_user'));
		}
	}
	#================Manage User===============#
	public function manage_user()
	{
		$this->permission->check_label('manage_users')->read()->redirect();
        $content = $this->lusers->user_list();
		$this->template->full_admin_html_view($content);
	}
	#===============User update form================#
	public function user_update_form($user_id)
	{
		$this->permission->check_label('manage_users')->update()->redirect();
		$content = $this->lusers->user_edit_data($user_id);
		$this->template->full_admin_html_view($content);
	}
	#===============User update===================#
	public function user_update()
	{
		$this->permission->check_label('manage_users')->update()->redirect();
		$user_id  = $this->input->post('user_id');
		$this->Userm->update_user($user_id);
		$this->session->set_userdata(array('message'=>display('successfully_updated')));
		redirect(base_url('manage_user'));
	}
	#============User delete===========#
	public function user_delete($user_id)
	{
		$this->permission->check_label('manage_users')->delete()->redirect();
		$this->Userm->delete_user($user_id);
		$this->session->set_userdata(array('message'=>display('successfully_delete')));
		redirect('manage_user');
	}
}