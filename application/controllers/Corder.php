<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Corder extends CI_Controller {
	
	function __construct() {
      	parent::__construct();
      	$this->load->library('Lorder');
      	$this->load->model('Orders');
      	$this->load->model('Companies');
      	$this->load->model('Email_templates');
      	$this->load->model('Shipping_methods');
    }
    //Index page loading
	public function index()
	{
		$this->permission->check_label('new_order')->create()->redirect();
		$content = $this->lorder->order_add_form();
		$this->template->full_admin_html_view($content);
	}
	//Add new order
	public function new_order()
	{
		$this->permission->check_label('new_order')->create()->redirect();
		$content = $this->lorder->order_add_form();
		$this->template->full_admin_html_view($content);
	}

	//Add new Pre-order
	public function new_pre_order()
	{
		//$this->permission->check_label('new_order')->create()->redirect();
		$content = $this->lorder->pre_order_add_form();
		$this->template->full_admin_html_view($content);
	}
	//Retrive delivery charge
	public function retrive_delivery_charge()
	{
		$this->permission->check_label('new_order')->create()->redirect();
		$shipping_id = $this->input->post('shipping_id');
		$ship_info   = $this->Shipping_methods->shipping_method_search_item($shipping_id);

		if ($ship_info) {
			echo $ship_info->charge_amount;
		}else{
			return false;
		}
	}
	//Insert order
	public function insert_order()
	{
		$this->permission->check_label('new_order')->create()->redirect();
		$order_id = $this->Orders->order_entry();
		$this->session->set_userdata(array('message'=>display('successfully_added')));
		$this->order_inserted_data($order_id);

		if(isset($_POST['add-order'])){
			redirect(base_url('corder/manage_order/all/item'));
		}elseif(isset($_POST['add-order-another'])){
			redirect(base_url('corder'));
		}
	}
	//Retrive right now inserted data to create html
	public function order_inserted_data($order_id=null)
	{
		$this->permission->check_label('manage_order')->read()->redirect();
		$content = $this->lorder->order_html_data($order_id);	
	}	
	//Retrive right now inserted data to create html
	public function order_details_data($order_id=null)
	{
		$this->permission->check_label('manage_order')->read()->redirect();
		$content = $this->lorder->order_details_data($order_id);	
		$this->template->full_admin_html_view($content);
	}
	//Manage order
	public function manage_order()
	{
		$this->permission->check_label('manage_order')->redirect();
		$order_no 		= $this->input->get('order_no');
		$pre_order_no 	= $this->input->get('pre_order_no');
		$customer 		= $this->input->get('customer');
		$date 			= $this->input->get('date');
		$order_status 	= $this->input->get('order_status');
		$shipping 		= $this->input->get('shipping');
		$shipping_agent 		= $this->input->get('shipping_agent');

		#
        #pagination starts
        #
        $config["base_url"] 	    = base_url('corder/manage_order/all/item/');
        $config['reuse_query_string']= true;
        $config["total_rows"] 	    = $this->Orders->order_count($order_no,$pre_order_no,$customer,$date,$order_status,$shipping);
        $config["per_page"] 	    = 100;
        $config["uri_segment"] 	    = 5;
        $config["num_links"] 	    = 5; 
	    /* This Application Must Be Used With BootStrap 3 * */
        $config['full_tag_open'] 	= "<ul class='pagination'>";
        $config['full_tag_close'] 	= "</ul>";
        $config['num_tag_open'] 	= '<li>';
        $config['num_tag_close'] 	= '</li>';
        $config['cur_tag_open'] 	= "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] 	= "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] 	= "<li>";
        $config['next_tag_close'] 	= "</li>";
        $config['prev_tag_open'] 	= "<li>";
        $config['prev_tagl_close'] 	= "</li>";
        $config['first_tag_open'] 	= "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] 	= "<li>";
        $config['last_tagl_close'] 	= "</li>";
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $links = $this->pagination->create_links();
        #
        #pagination ends
        #

        $content = $this->lorder->order_list($links,$config["per_page"],$page,$order_no,$pre_order_no,$customer,
            $date,$order_status,$shipping, $shipping_agent);
		$this->template->full_admin_html_view($content);
	}
	//Order Update Form
	public function order_update_form($order_id=null)
	{
		$this->permission->check_label('manage_order')->update()->redirect();
		$content = $this->lorder->order_edit_data($order_id);
		$this->template->full_admin_html_view($content);
	}
	//Order Return Form
	public function order_return_form($order_id=null)
	{
		$this->permission->check_label('manage_order')->update()->redirect();
		$content = $this->lorder->order_return_edit_data($order_id);
		$this->template->full_admin_html_view($content);
	}
	//Order Update
	public function order_update()
	{
		$this->permission->check_label('manage_order')->update()->redirect();
		$order_id = $this->Orders->update_order();
		$this->session->set_userdata(array('message'=>display('successfully_updated')));
		$this->order_inserted_data($order_id);
		redirect(base_url('corder/manage_order/all/item'));
	}
	//Return Order
	public function return_order()
	{
		$this->permission->check_label('manage_order')->update()->redirect();
		$order_id = $this->Orders->return_order();
		$this->session->set_userdata(array('message'=>display('successfully_updated')));
		$this->order_inserted_data($order_id);
		redirect(base_url('corder/manage_order/all/item'));
	}
	//Delete order product 
	public function delete_order_product()
	{

		$quantity=0;
		$amount=0;
		$real_price=0;
		$total_dis=0;

		$order_id 	= $this->input->post('order_id');
		$product_id = $this->input->post('product_id');
		$order_no 	= $this->Orders->get_order_no_by_order_id($order_id);

		$product_info = $this->db->select('*')
				->from('seller_order')
				->where('order_id',$order_id)
				->where('product_id',$product_id)
				->get()
				->row();

		if ($product_info) {

			$quantity 	= $product_info->quantity;
			$amount   	= $product_info->total_price; 
			$real_price = $product_info->total_price - ($quantity * $product_info->discount_per_product); 
			$total_dis 	= $quantity * $product_info->discount_per_product;

			$this->db->set('total_amount','total_amount-'.$real_price,false)
					->set('total_discount','total_discount-'.$total_dis,false)
					->where('order_id',$order_id)
					->update('order');

			$this->db->set('quantity','quantity+'.$product_info->quantity,FALSE)
					->where('product_id',$product_id)
					->update('product_information');

			$this->db->where('product_id',$product_id)
					->where('order_id',$order_id)
					->delete('seller_order');

			$this->db->where('product_id',$product_id)
					->where('order_no',$order_no)
					->delete('invoice_details');
		}

		return true;

	}
	//Order paid data
	public function order_paid_data($order_id=null){
		$this->permission->check_label('manage_order')->update()->redirect();
		$order_id = $this->Orders->order_paid_data($order_id);
		$this->session->set_userdata(array('message'=>display('successfully_added')));
		redirect('corder/manage_order/all/item');
	}
	//Pre order paid data
	public function pre_order_paid_data($order_id=null){
		$this->permission->check_label('manage_pre_order')->update()->redirect();
		$order_id = $this->Orders->pre_order_paid_data($order_id);
		$this->session->set_userdata(array('message'=>display('successfully_added')));
		redirect('corder/manage_pre_order/all/item');
	}
	//Retrive product data
	public function retrieve_product_data()
	{
		$this->permission->check_label('manage_order')->insert()->redirect();
		$product_id = $this->input->post('product_id');
		$product_info = $this->Orders->get_total_product($product_id);
		echo json_encode($product_info);
	}
	//Order delete
	public function order_delete($order_id=null,$order_no=null)
	{
		$this->permission->check_label('manage_order')->delete()->redirect();
		$result = $this->Orders->delete_order($order_id,$order_no);
		if ($result) {
			$this->session->set_userdata(array('message'=>display('successfully_delete')));
			redirect('corder/manage_order/all/item');
		}	
	}	
	//Pre-order delete
	public function pre_order_delete($order_id=null)
	{
		$this->permission->check_label('manage_pre_order')->delete()->redirect();
		$result = $this->Orders->delete_pre_order($order_id);
		if ($result) {
			$this->session->set_userdata(array('message'=>display('successfully_delete')));
			redirect('corder/manage_pre_order/all/item');
		}	
	}
	//Manage Pre-order
	public function manage_pre_order()
	{
		$this->permission->check_label('manage_pre_order')->access();
		$pre_order_no 	= $this->input->get('pre_order_no');
		$customer 		= $this->input->get('customer');
		$date 			= $this->input->get('date');

		#
        #pagination starts
        #
        $config["base_url"] 	    = base_url('corder/manage_pre_order/all/item');
        $config['reuse_query_string']= true;
        $config["total_rows"] 	    = $this->Orders->pre_order_count($pre_order_no,$customer,$date);
        $config["per_page"] 	    = 50;
        $config["uri_segment"] 	    = 5;
        $config["num_links"] 	    = 5; 
	    /* This Application Must Be Used With BootStrap 3 * */
        $config['full_tag_open'] 	= "<ul class='pagination'>";
        $config['full_tag_close'] 	= "</ul>";
        $config['num_tag_open'] 	= '<li>';
        $config['num_tag_close'] 	= '</li>';
        $config['cur_tag_open'] 	= "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] 	= "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] 	= "<li>";
        $config['next_tag_close'] 	= "</li>";
        $config['prev_tag_open'] 	= "<li>";
        $config['prev_tagl_close'] 	= "</li>";
        $config['first_tag_open'] 	= "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] 	= "<li>";
        $config['last_tagl_close'] 	= "</li>";
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $links = $this->pagination->create_links();
        #
        #pagination ends
        #

        $content = $this->lorder->pre_order_list($links,$config["per_page"],$page,$pre_order_no,$customer,$date);
		$this->template->full_admin_html_view($content);
	}
	//Retrive right now inserted data to create html
	public function pre_order_details_data($order_id=null)
	{
		$this->permission->check_label('manage_pre_order')->read()->redirect();
		$content = $this->lorder->pre_order_details_data($order_id);	
		$this->template->full_admin_html_view($content);
	}
	//Order Update Form
	public function pre_order_update_form($order_id=null)
	{
		$this->permission->check_label('manage_pre_order')->update()->redirect();
		$content = $this->lorder->pre_order_edit_data($order_id);
		$this->template->full_admin_html_view($content);
	}
	//Pre Order Update
	public function pre_order_update()
	{
		$this->permission->check_label('manage_pre_order')->update()->redirect();
		$order_id = $this->Orders->pre_order_update();
		$this->session->set_userdata(array('message'=>display('successfully_updated')));
		$this->order_inserted_data($order_id);
		redirect(base_url('manage_pre_order'));
	}

	//update shipping agent

    public function update_shipping_agent($order_id=null)
    {
        $shipping_agent = $this->input->post('shipping_agent');
        $this->db->set('shipping_agent_id',$shipping_agent)
            ->where('order_id',$order_id)
            ->update('order');
        $this->session->set_userdata(array('message'=>display('successfully_updated')));
        redirect(base_url('corder/manage_order/all/item'));
    }

	//Update status
	public function update_status($order_id=null){

		$this->permission->check_label('manage_order')->update()->redirect();
		$order_status = $this->input->post('order_status');

		$template = $this->Email_templates->retrieve_template($order_status);

		if ($template) {

			//Shipping delivery return/cancel
			if ($order_status == 5 || $order_status == 6) {
				$this->db->set('shipping',null)
						->where('order_id',$order_id)
						->update('order');

				$order_details= $this->db->select('*')
						->from('seller_order')
						->where('order_id',$order_id)
						->get()
						->result();

				if ($order_details) {
					foreach ($order_details as $orders) {
						$this->db->set('quantity', 'quantity+'.$orders->quantity, FALSE)
								->where('product_id',$orders->product_id)
								->update('product_information');
					}
				}
			}

			// Order status when processing then order must be converted to invoice
			if ($order_status == 2) {
				$this->Orders->order_paid_data($order_id);
			}

			//When order status shipping then update order table shipping field with current time stamp
			if ($order_status == 3) {
				$this->db->set('shipping',date("Y-m-d"))
						->where('order_id',$order_id)
						->update('order');
			}

			//When order status delivered then update order table delivered field with current time stamp
			if ($order_status == 4) {
				$this->db->set('delivered',date("Y-m-d"))
						->where('order_id',$order_id)
						->update('order');
			}

		  	//Update order status
			$this->db->set('order_status',$this->input->post('order_status'));
			$this->db->where('order_id',$order_id);
			$result = $this->db->update('order');

			//Update invoice status
			$order = $this->Orders->get_order_no($order_id);
			$this->db->set('invoice_status',$this->input->post('order_status'))
					->where('order_no',$order->order_no)
					->update('invoice');

			//Invoice and invoice details data delete
			if ($order_status == 5) {
				$this->db->where('order_no',$order->order_no)
						->delete('invoice');

				$this->db->where('order_no',$order->id)
						->delete('invoice_details');
			}

			//Insert data into order tracking table
			$order_tracking = array(
				'order_id'	=>	$order_id,
				'user_id'	=>	$this->session->userdata('user_id'),
				'order_status'	=>	$this->input->post('order_status'),
				'date'		=>	date("Y-m-d h:i a"),
			);
			$this->db->insert('order_tracking',$order_tracking);
			
			if ($result == TRUE) {	
	
				$customer_email = $this->input->post('customer_email');

				//send email with as a link
	            $setting_detail = $this->Soft_settings->retrieve_email_editdata();
	            $company_info   = $this->Companies->company_list();
	           
	            $config = array(
	                'protocol'      => $setting_detail[0]['protocol'],
	                'smtp_host'     => $setting_detail[0]['smtp_host'],
	                'smtp_port'     => $setting_detail[0]['smtp_port'],
	                'smtp_user'     => $setting_detail[0]['sender_email'], 
	                'smtp_pass'     => $setting_detail[0]['password'], 
	                'mailtype'      => $setting_detail[0]['mailtype'], 
	                'charset'       => 'utf-8'
	            );
	            $this->email->initialize($config);
	            $this->email->set_mailtype($setting_detail[0]['mailtype']);
	            $this->email->set_newline("\r\n");
	            
	            //Email content
	            $this->email->to($customer_email);
	            $this->email->from($setting_detail[0]['sender_email'],$company_info[0]['company_name']);
	            $this->email->subject($template->subject);
	            $this->email->message($template->message);

			    $email = $this->test_input($customer_email);
				if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
				    if($this->email->send())
				    {
				      	$this->session->set_userdata(array('message'=>display('email_send_to_customer')));
				      	redirect(base_url('corder/manage_order/all/item'));
				    }else{
				     	$this->session->set_userdata(array('error_message'=>display('email_not_send')));
				     	redirect(base_url('corder/manage_order/all/item'));
				    }
				}else{
					$this->session->set_userdata(array('message'=>display('successfully_updated')));
				    redirect(base_url('corder/manage_order/all/item'));
				}
			}else{
				$this->session->set_userdata(array('error_message'=>display('already_exists')));
				redirect(base_url('corder/manage_order/all/item'));
			}
		}else{
			$this->session->set_userdata(array('error_message'=>display('please_add_template')));
			redirect(base_url('corder/manage_order/all/item'));
		}
	}
	//Email testing for email
	public function test_input($data) {
	  	$data = trim($data);
	  	$data = stripslashes($data);
	  	$data = htmlspecialchars($data);
	  	return $data;
	}
	//Product stock check
	public function product_stock_check()
	{
		$product_id = $this->input->post('product_id');

		if ($product_id) {
			$result = $this->db->select('*')
					->from('product_information')
					->where('product_id',$product_id)
					->get()
					->row();
			echo $result->quantity;
		}else{
			echo 'no';
		}
	}
	//Retrive customer name
	public function retrive_customer_name()
	{
		$customer_id = $this->input->post('customer_id');
		$customer = $this->db->select('*')
							->from('customer_information')
							->where('customer_id',$customer_id)
							->get()
							->row();
		if ($customer) {
			echo $customer->customer_name .'<br>'. $customer->customer_short_address;
		}else{
			return false;
		}
	}	

	//Customer existing check by mobile
	public function customer_existing_check()
	{
		$customer_mobile = $this->input->post('customer_mobile');

		$customer = $this->db->select('*')
							->from('customer_information')
							->where('customer_mobile',$customer_mobile)
							->get()
							->row();

		if ($customer) {
			echo "1";
		}else{
			echo "2";
		}
	}
}