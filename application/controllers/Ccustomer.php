<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ccustomer extends CI_Controller {

	function __construct() {
      	parent::__construct();
		$this->load->library('lcustomer');
		$this->load->model('Customers');
		$this->load->model('Companies');
		$this->load->model('Email_templates');
		$this->permission->module('customer')->redirect();
    }
	//Default loading for Customer System.
	public function index()
	{
		$this->permission->check_label('add_customer')->create()->redirect();
		$content = $this->lcustomer->customer_add_form();
		$this->template->full_admin_html_view($content);
	}
	//Insert Product and upload
	public function insert_customer()
	{
		$this->permission->check_label('add_customer')->create()->redirect();
		$customer_id=$this->auth->generator(15);

	  	//Customer  basic information adding.
		$data=array(
			'customer_id' 		=> $customer_id,
			'customer_code' 	=> $this->number_generator(),
			'customer_name' 	=> $this->input->post('customer_name'),
			'customer_mobile' 	=> $this->input->post('mobile'),
			'customer_email' 	=> $this->input->post('email'),
			'customer_short_address' => $this->input->post('address'),
			'customer_address_1'=> $this->input->post('customer_address_1'),
			'customer_address_2'=> $this->input->post('customer_address_2'),
			'city' 				=> $this->input->post('city'),
			'state' 			=> $this->input->post('state'),
			'country' 			=> $this->input->post('country'),
			'zip' 				=> $this->input->post('zip'),
			'status' 			=> 1
		);

		$result = $this->Customers->customer_entry($data);
		
		if ($result == TRUE) {

			// $country = $this->Customers->select_county_name($data['country']);

			// //send email with as a link
   //          $setting_detail = $this->Soft_settings->retrieve_email_editdata();
   //          $company_info   = $this->Companies->company_list();
   //          $template_details 		= $this->Email_templates->retrieve_template('8');

   //          $config = array(
   //              'protocol'      => $setting_detail[0]['protocol'],
   //              'smtp_host'     => $setting_detail[0]['smtp_host'],
   //              'smtp_port'     => $setting_detail[0]['smtp_port'],
   //              'smtp_user'     => $setting_detail[0]['sender_email'], 
   //              'smtp_pass'     => $setting_detail[0]['password'], 
   //              'mailtype'      => $setting_detail[0]['mailtype'], 
   //              'charset'       => 'utf-8'
   //          );
   //          $this->email->initialize($config);
   //          $this->email->set_mailtype($setting_detail[0]['mailtype']);
   //          $this->email->set_newline("\r\n");

   //          //Email Message Set
   //          $message = (!empty($template_details->message))?$template_details->message:null;
   //          $message .= "<p>".display('your_information')."<table style=\"border:1px solid\">
			// 				<tr>
			// 					<th style=\"text-align: left;border: 1px solid;\">".display('customer_name').":</th>
			// 					<td style=\"border: 1px solid;\">".$data['customer_name']."</td>
			// 				</tr>
			// 				<tr>
			// 					<th style=\"text-align: left;border: 1px solid;\">".display('customer_email').":</th>
			// 					<td style=\"border: 1px solid;\">".$data['customer_email']."</td>
			// 				</tr>
			// 				<tr>
			// 					<th style=\"text-align: left;border: 1px solid;\">".display('customer_mobile').":</th>
			// 					<td style=\"border: 1px solid;\">".$data['customer_mobile']."</td>
			// 				</tr>
			// 				<tr>
			// 					<th style=\"text-align: left;border: 1px solid;\">".display('address').":</th>
			// 					<td style=\"border: 1px solid;\">".$data['customer_short_address']."</td>
			// 				</tr>
			// 				<tr>
			// 					<th style=\"text-align: left;border: 1px solid;\">".display('city').":</th>
			// 					<td style=\"border: 1px solid;\">".$data['city']."</td>
			// 				</tr>
			// 				<tr>
			// 					<th style=\"text-align: left;border: 1px solid;\">".display('state').":</th>
			// 					<td style=\"border: 1px solid;\">".$data['state']."</td>
			// 				</tr>
			// 				<tr>
			// 					<th style=\"text-align: left;border: 1px solid;\">".display('country').":</th>
			// 					<td style=\"border: 1px solid;\">".((!empty($country))?$country->name:null)."</td>
			// 				</tr>
			// 				<tr>
			// 					<th style=\"text-align: left;border: 1px solid;\">".display('zip').":</th>
			// 					<td style=\"border: 1px solid;\">".$data['zip']."</td>
			// 				</tr>
			// 			</table>";
            
   //          //Email content
   //          $this->email->to($data['customer_email']);
   //          $this->email->from($setting_detail[0]['sender_email'],$company_info[0]['company_name']);
   //          $this->email->subject(!empty($template_details->subject)?$template_details->subject:null);
   //          $this->email->message($message);

		 //    $email = $this->test_email($data['customer_email']);
			// if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			//     if($this->email->send())
			//     {
			//     	$this->session->set_userdata(array(
			// 			'message' 		=> display('successfully_added'),
			// 		));
			//     }else{
			//     	$this->session->set_userdata(array(
			// 			'error_message' => display('email_was_not_sent_please_contact_administrator'),
			// 		));
			//     }
			// }else{
			// 	$this->session->set_userdata(array(
			// 		'error_message' => display('your_email_was_not_found'),
			// 	));
			// }

			$this->session->set_userdata(array('success'=>display('successfully_added')));
			if(isset($_POST['add-customer'])){
				redirect(base_url('manage_customer'));
			}elseif(isset($_POST['add-customer-another'])){
				redirect(base_url('ccustomer'));
			}
		}else{
			$this->session->set_userdata(array('error_message'=>display('already_exists')));
			redirect(base_url('ccustomer'));
		}
	}
	//Email testing for email
	public function test_email($data) {
	  	$data = trim($data);
	  	$data = stripslashes($data);
	  	$data = htmlspecialchars($data);
	  	return $data;
	}
	//Manage customer
	public function manage_customer()
	{

		if (!$this->permission->check_label('manage_customer')->access()) {
			$this->permission->check_label('manage_customer')->redirect();
		}

		$mobile = $this->input->get('mobile');
		$email 	= $this->input->get('email');
		$customer 	= $this->input->get('customer');

		#
        #pagination starts
        #
        $config["base_url"] 	    = base_url('manage_customer/');
        $config['reuse_query_string']= true;
        $config["total_rows"] 	    = $this->Customers->customer_count($mobile,$email,$customer);
        $config["per_page"] 	    = 10;
        $config["uri_segment"] 	    = 3;
        $config["num_links"] 	    = 5; 
	    /* This Application Must Be Used With BootStrap 3 * */
        $config['full_tag_open'] 	= "<ul class='pagination'>";
        $config['full_tag_close'] 	= "</ul>";
        $config['num_tag_open'] 	= '<li>';
        $config['num_tag_close'] 	= '</li>';
        $config['cur_tag_open'] 	= "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] 	= "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] 	= "<li>";
        $config['next_tag_close'] 	= "</li>";
        $config['prev_tag_open'] 	= "<li>";
        $config['prev_tagl_close'] 	= "</li>";
        $config['first_tag_open'] 	= "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] 	= "<li>";
        $config['last_tagl_close'] 	= "</li>";
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $links = $this->pagination->create_links();
        #
        #pagination ends
        #

		$content = $this->lcustomer->customer_list($links,$config["per_page"],$page,$mobile,$email,$customer);
		$this->template->full_admin_html_view($content);;
	}
	//Customer Update Form
	public function customer_update_form($customer_id)
	{	
		$this->permission->check_label('manage_customer')->update()->redirect();
		$content = $this->lcustomer->customer_edit_data($customer_id);
		$this->template->full_admin_html_view($content);
	}
	// Customer Update
	public function customer_update()
	{
		$this->permission->check_label('manage_customer')->update()->redirect();
		$customer_id  = $this->input->post('customer_id');

	  	//Customer  basic information adding.
		$data=array(
			'customer_name' 	=> $this->input->post('customer_name'),
			'customer_mobile' 	=> $this->input->post('mobile'),
			'customer_email' 	=> $this->input->post('email'),
			'customer_short_address' => $this->input->post('address'),
			'customer_address_1' => $this->input->post('customer_address_1'),
			'customer_address_2' => $this->input->post('customer_address_2'),
			'city' 				=> $this->input->post('city'),
			'state' 			=> $this->input->post('state'),
			'country' 			=> $this->input->post('country'),
			'zip' 				=> $this->input->post('zip'),
			'company' 				=> $this->input->post('company'),
			'status' 			=> 1
			);

		$this->Customers->update_customer($data,$customer_id);
		$this->session->set_userdata(array('message'=>display('successfully_updated')));
		redirect('manage_customer');
	}
	//Select city by country id
	public function select_city_country_id()
	{
		$country_id = $this->input->post('country_id');
		$states = $this->Customers->select_city_country_id($country_id);

		$html ="";
		if ($states) {
			$html .="<select class=\"form-control select2\" id=\"country\" name=\"country\" style=\"width: 100%\">
					<option value=\"\">".display('select_one')."</option>";
			foreach ($states as $state) {
            $html .="<option value='".$state->name."'>".$state->name."</option>";
			}
			$html .="</select>";
		}
		echo $html;
	}
	//Customer Details
	public function customer_details($customer_id=null)
	{
		if (!$this->permission->check_label('customer_details')->access()) {
			$this->permission->check_label('customer_details')->redirect();
		}
		$content = $this->lcustomer->customer_details($customer_id);
		$this->template->full_admin_html_view($content);
	}
	//Customer search item for details
	public function customer_search_details()
	{
		$this->permission->check_label('customer_details')->read()->redirect();
		$customer_id = $this->input->post('customer_id');
		$customer_mobile = $this->input->post('customer_mobile');	
		$email = $this->input->post('email');			
		$content = $this->lcustomer->customer_details($customer_id,$customer_mobile,$email);
		$this->template->full_admin_html_view($content);
	}
	// Customer delete
	public function customer_delete($customer_id)
	{
		$this->permission->check_label('manage_customer')->delete()->redirect();
		$this->Customers->delete_customer($customer_id);
		$this->session->set_userdata(array('message'=>display('successfully_delete')));
		redirect('manage_customer');
	}
	//Customer search
	public function customer_search(){
		$this->permission->check_label('manage_customer')->read()->redirect();
		$customer_info = $this->input->post('customer_info');
		$customer_info = $this->Customers->customer_search($customer_info);

		if ($customer_info) {
			$tr = "";
			$i = 1;
			foreach ($customer_info as $customer) {
				$tr .="<tr>
						<td>".$i."</td>
						<td>";
							if ($this->session->userdata('user_type') == '4') {
								$customer['customer_name'];
							}else{
					$tr .="	<a href=".base_url().'ccustomer/customer_details/'.$customer['customer_id'].">".$customer['customer_name']."</a>";
							}	
					$tr .="</td>";
					$tr .="<td>".$customer['customer_short_address']."</td>";
					$tr .="<td>".$customer['customer_mobile']."</td>";
					$tr .="<td>".$customer['customer_email']."</td>";
					$tr .="<td><center>
							<a href=".base_url().'ccustomer/customer_update_form/'.$customer['customer_id']." class=\"btn btn-info btn-sm\" data-toggle=\"tooltip\" data-placement=\"left\" title=".display('update')."><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i></a>

							<a href=".base_url().'ccustomer/customer_delete/'.$customer['customer_id']." class=\"btn btn-danger btn-sm\" onclick=\"return confirm(".display('are_you_sure_want_to_delete').")\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"\" data-original-title=".display('delete')."><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></a>
						</center></td>";
				$tr .="</tr>";
				$i++;
			}
			echo $tr;
		}
	}

	//NUMBER GENERATOR
	public function number_generator()
	{
		$this->db->select_max('customer_code');
		$query = $this->db->get('customer_information');	
		$result = $query->result_array();	
		$customer_code = $result[0]['customer_code'];
		if ($customer_code !='') {
			$customer_code = $customer_code + 1;	
		}else{
			$customer_code = 1000;
		}
		return $customer_code;		
	}

	//Customer_
	public function customer_code_update()
	{
		$customer = $this->db->select('*')
				->from('customer_information')
				->get()
				->result();

		if ($customer) {
			foreach ($customer as $c) {
				$this->db->set('customer_code',$this->number_generator())
						->where('customer_id',$c->customer_id)
						->update('customer_information');
			}
		}
	}
}