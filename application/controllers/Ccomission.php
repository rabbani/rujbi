<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ccomission extends CI_Controller {

	function __construct() {
     	parent::__construct();
		$this->load->library('lcomission');
		$this->load->model('Comissions');
		$this->permission->module('comission')->redirect();
    }
	//Default loading for comission system.
	public function index()
	{
		$this->permission->check_label('add_comission')->create()->redirect();
		$content = $this->lcomission->comission_add_form();
		$this->template->full_admin_html_view($content);
	}
	//Insert comission
	public function insert_comission()
	{
		$this->permission->check_label('add_comission')->create()->redirect();
		$this->form_validation->set_rules('category', display('category'), 'trim|required');
		$this->form_validation->set_rules('comission', display('comission'), 'trim|required');

		if ($this->form_validation->run() == FALSE)
        {
        	$data = array(
				'title' => display('add_comission')
			);
        	$content = $this->parser->parse('comission/add_comission',$data,true);
			$this->template->full_admin_html_view($content);
        }else{
		
			$data=array(
				'category_id'=> $this->input->post('category'),
				'rate' 		 => $this->input->post('comission'),
				'status' 	 => 1
			);

			$result=$this->Comissions->comission_entry($data);

			if ($result == TRUE) {
					
				$this->session->set_userdata(array('message'=>display('successfully_added')));

				if(isset($_POST['add-comission'])){
					redirect(base_url('manage_comission'));
				}elseif(isset($_POST['add-comission-another'])){
					redirect(base_url('ccomission'));
				}

			}else{
				$this->session->set_userdata(array('error_message'=>display('already_inserted')));
				redirect(base_url('ccomission'));
			}
        }
	}
	//Manage comission
	public function manage_comission()
	{
		$this->permission->check_label('manage_comission')->redirect();
        $content =$this->lcomission->comission_list();
		$this->template->full_admin_html_view($content);
	}
	//Comission Update Form
	public function comission_update_form($comission_id=null)
	{	
		$this->permission->check_label('manage_comission')->update()->redirect();
		$content = $this->lcomission->comission_edit_data($comission_id);
		$this->template->full_admin_html_view($content);
	}
	//Comission Update
	public function comission_update($comission_id=null)
	{
		$this->permission->check_label('manage_comission')->update()->redirect();
		$this->form_validation->set_rules('category', display('category'), 'trim|required');
		$this->form_validation->set_rules('comission', display('comission'), 'trim|required');
		$this->form_validation->set_rules('status', display('status'), 'trim|required');

		if ($this->form_validation->run() == FALSE)
        {
        	$data = array(
				'title' => display('manage_comission')
			);
        	$content = $this->parser->parse('comission/manage_comission',$data,true);
			$this->template->full_admin_html_view($content);
        }
        else
        {

			$data=array(
				'category_id' 	=> $this->input->post('category'),
				'rate' 		=> $this->input->post('comission'),
				'status' 	=> $this->input->post('status'),
			);

			$result=$this->Comissions->update_comission($data,$comission_id);

			if ($result == TRUE) {
				$this->session->set_userdata(array('message'=>display('successfully_updated')));
				redirect('manage_comission');
			}else{
				$this->session->set_userdata(array('error_message'=>display('already_exists')));
				redirect('manage_comission');
			}
        }
	}
	// comission Delete
	public function comission_delete($comission_id)
	{	
		$this->permission->check_label('manage_comission')->delete()->redirect();
		$this->Comissions->delete_comission($comission_id);
		$this->session->set_userdata(array('message'=>display('successfully_delete')));
		redirect('manage_comission');
	}
	//Comission report
	public function comission_report(){

		$this->permission->check_label('comission_report')->read()->redirect();
		$seller 	 = $this->input->get('seller_id');
		$product_id  = $this->input->get('product_id');
		$category_id = $this->input->get('category_id');

		#
        #pagination starts
        #
        $config["base_url"] 	    = base_url('comission_report/');
        $config['reuse_query_string']= true;
        $config["total_rows"] 	    = $this->Comissions->comission_report_count($seller,$product_id,$category_id);
        $config["per_page"] 	    = 10;
        $config["uri_segment"] 	    = 3;
        $config["num_links"] 	    = 5; 
	    /* This Application Must Be Used With BootStrap 3 * */
        $config['full_tag_open'] 	= "<ul class='pagination'>";
        $config['full_tag_close'] 	= "</ul>";
        $config['num_tag_open'] 	= '<li>';
        $config['num_tag_close'] 	= '</li>';
        $config['cur_tag_open'] 	= "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] 	= "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] 	= "<li>";
        $config['next_tag_close'] 	= "</li>";
        $config['prev_tag_open'] 	= "<li>";
        $config['prev_tagl_close'] 	= "</li>";
        $config['first_tag_open'] 	= "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] 	= "<li>";
        $config['last_tagl_close'] 	= "</li>";
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $links = $this->pagination->create_links();
        #
        #pagination ends
        #

		$content =$this->lcomission->comission_report($links,$config["per_page"],$page,$seller,$product_id,$category_id);
		$this->template->full_admin_html_view($content);
	}
}