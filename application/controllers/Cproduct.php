<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cproduct extends CI_Controller {
	
	function __construct() {
     	parent::__construct();
		$this->load->model('Products');
		$this->load->model('Companies');
		$this->load->model('Email_templates');
		$this->load->library('lproduct');
		$this->permission->module('product')->redirect();
    }
    //Index page load
	public function index()
	{	
		$this->permission->check_label('add_product')->create()->redirect();
		$content = $this->lproduct->upload_product();
		$this->template->full_admin_html_view($content);
	}
	//Manage product
	public function manage_product()
	{
		$this->permission->check_label('manage_product')->access();
		$seller = $this->input->get('seller');
		$model 	= $this->input->get('model');
		$title 	= $this->input->get('title');
		$category = $this->input->get('category');

		#
        #pagination starts
        #
        $config["base_url"] 	    = base_url('cproduct/manage_product/all/item/');
        $config['reuse_query_string']= true;
        $config["total_rows"] 	    = $this->Products->product_count($seller,$model,$title,$category);
        $config["per_page"] 	    = 50;
        $config["uri_segment"] 	    = 5;
        $config["num_links"] 	    = 5; 
	    /* This Application Must Be Used With BootStrap 3 * */
        $config['full_tag_open'] 	= "<ul class='pagination'>";
        $config['full_tag_close'] 	= "</ul>";
        $config['num_tag_open'] 	= '<li>';
        $config['num_tag_close'] 	= '</li>';
        $config['cur_tag_open'] 	= "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] 	= "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] 	= "<li>";
        $config['next_tag_close'] 	= "</li>";
        $config['prev_tag_open'] 	= "<li>";
        $config['prev_tagl_close'] 	= "</li>";
        $config['first_tag_open'] 	= "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] 	= "<li>";
        $config['last_tagl_close'] 	= "</li>";
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $links = $this->pagination->create_links();
        #
        #pagination ends
        #

		$content = $this->lproduct->manage_product($links,$config["per_page"],$page,$seller,$model,$title,$category);
		$this->template->full_admin_html_view($content);
	}
	//Insert upload product
	public function insert_product(){
		$this->permission->check_label('add_product')->create()->redirect();
		$variant = 0;
		if ($this->input->post('variant')) {
			$variant = implode(",",$this->input->post('variant'));
		}else{
			$variant = 0;
		}
		$product_id = $this->generator(8);
		$data=array(
			'product_id'	=> $product_id,
			'seller_id' 	=> $this->input->post('seller_id'),
			'category_id' 	=> $this->input->post('category_id'),
			'price' 		=> $this->input->post('price'),
			'offer_price' 	=> $this->input->post('onsale_price'),
			'unit' 			=> $this->input->post('unit'),
			'product_model' => $this->input->post('product_model'),
			'brand_id' 		=> $this->input->post('brand'),
			'variant_id' 	=> (!empty($variant))?$variant:null,
			'product_type' 	=> $this->input->post('product_type'),
			'tag' 			=> $this->input->post('tag_value'),
			'quantity'  	=> $this->input->post('quantity'),
			'approved_date' => date("Y-m-d h:i:s"),
			'best_sale' 	=> $this->input->post('best_sale'),
			'comission' 	=> $this->input->post('comission'),
			'on_sale' 		=> $this->input->post('onsale'),
			'on_promotion' 	=> $this->input->post('on_promotion'),
			'details' 		=> $this->input->post('details'),
			'pre_order' 	=> $this->input->post('pre_order'),
			'pre_order_quantity'=> $this->input->post('pre_order_quantity'),
			'status' 		=> 1,
		);

		$this->db->select('*');
		$this->db->from('product_information');
		$this->db->where('product_model',$data['product_model']);
		$query = $this->db->get();

		if ($query->num_rows() > 0) {

			$data=array(
				'seller_id' 	=> $this->input->post('seller_id'),
				'category_id' 	=> $this->input->post('category_id'),
				'price' 		=> $this->input->post('price'),
				'offer_price' 	=> $this->input->post('onsale_price'),
				'unit' 			=> $this->input->post('unit'),
				'product_model' => $this->input->post('product_model'),
				'brand_id' 		=> $this->input->post('brand'),
				'variant_id' 	=> (!empty($variant))?$variant:null,
				'product_type' 	=> $this->input->post('product_type'),
				'tag' 			=> $this->input->post('tag_value'),
				'quantity'  	=> $this->input->post('quantity'),
				'approved_date' => date("Y-m-d h:i:s"),
				'best_sale' 	=> $this->input->post('best_sale'),
				'comission' 	=> $this->input->post('comission'),
				'on_sale' 		=> $this->input->post('onsale'),
				'on_promotion' 	=> $this->input->post('on_promotion'),
				'details' 		=> $this->input->post('details'),
				'pre_order' 	=> $this->input->post('pre_order'),
				'pre_order_quantity'=> $this->input->post('pre_order_quantity'),
				'status' 		=> 1,
			);

			//Product model exists product update
			$this->db->where('product_model',$data['product_model'])
					->update('product_information',$data);
			echo "1";
		}else{
			$insert = $this->db->insert('product_information',$data);
			if ($insert) {
				$this->Products->create_product_json();
				$this->session->set_userdata('product_id',$data['product_id']);
				echo "1";
			}
		}
	}
	//Seller Product Update
	public function product_update_form($product_id){
		$this->permission->check_label('manage_product')->update()->redirect();
		$content = $this->lproduct->product_update_form($product_id);
		$this->template->full_admin_html_view($content);
	}
	//Update upload product
	public function update_product(){
		$this->permission->check_label('manage_product')->update()->redirect();
		$product_id  = $this->input->post('product_id');
		$variant = 0;
		if ($this->input->post('variant')) {
			$variant = implode(",",$this->input->post('variant'));
		}else{
			$variant = 0;
		}

		$data = array(
			'seller_id' 	=> $this->input->post('seller_id'),
			'category_id' 	=> $this->input->post('category_id'),
			'price' 		=> $this->input->post('price'),
			'offer_price' 	=> $this->input->post('onsale_price'),
			'unit' 			=> $this->input->post('unit'),
			'product_model' => $this->input->post('product_model'),
			'brand_id' 		=> $this->input->post('brand'),
			'variant_id' 	=> (!empty($variant))?$variant:null,
			'product_type' 	=> $this->input->post('product_type'),
			'tag' 			=> $this->input->post('tag_value'),
			'quantity'  	=> $this->input->post('quantity'),
			'approved_date' => date("Y-m-d h:i:s"),
			'best_sale' 	=> $this->input->post('best_sale'),
			'on_sale' 		=> $this->input->post('onsale'),
			'on_promotion' 	=> $this->input->post('on_promotion'),
			'details' 		=> $this->input->post('details'),
			'pre_order' 	=> $this->input->post('pre_order'),
			'pre_order_quantity'=> $this->input->post('pre_order_quantity'),
			'comission'		=> $this->input->post('comission'),
			'status' 		=> $this->input->post('status'),
		);

		$this->db->select('*');
		$this->db->from('product_information');
		$this->db->where('product_model',$data['product_model']);
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			$data1 = array(
				'seller_id' 	=> $this->input->post('seller_id'),
				'category_id' 	=> $this->input->post('category_id'),
				'price' 		=> $this->input->post('price'),
				'offer_price' 	=> $this->input->post('onsale_price'),
				'unit' 			=> $this->input->post('unit'),
				'brand_id' 		=> $this->input->post('brand'),
				'variant_id' 	=> (!empty($variant))?$variant:null,
				'product_type' 	=> $this->input->post('product_type'),
				'tag' 			=> $this->input->post('tag_value'),
				'approved_date' => date("Y-m-d h:i:s"),
				'best_sale' 	=> $this->input->post('best_sale'),
				'on_sale' 		=> $this->input->post('onsale'),
				'on_promotion' 	=> $this->input->post('on_promotion'),
				'details' 		=> $this->input->post('details'),
				'pre_order' 	=> $this->input->post('pre_order'),
				'pre_order_quantity'=> $this->input->post('pre_order_quantity'),
				'comission'		=> $this->input->post('comission'),
				'status' 		=> $this->input->post('status'),
			);
			$this->db->where('product_id',$product_id);
			$result = $this->db->update('product_information',$data1);
			$this->Products->create_product_json();

			if ($this->input->post('quantity')) {
				//update stock
				$this->db->set('quantity','quantity+'.$this->input->post('quantity'),FALSE)
						->where('product_id',$product_id)
						->update('product_information');

				//insert stock
				$in_stock = $this->input->post('in_stock');
				$data = array(
					'product_id'   => $product_id,
					'quantity' 	   => $this->input->post('quantity'),
					'date' 	   	   => date("Y-m-d"),
					'pre_quantity' => $in_stock,
				);
				$this->db->insert('quantiy_log',$data);
			}
			echo "1";
		}else{
			$this->db->where('product_id',$product_id);
			$this->db->update('product_information',$data);
			$this->Products->create_product_json();

			if ($this->input->post('quantity')) {
				//update stock
				$this->db->set('quantity','quantity+'.$this->input->post('quantity'),FALSE)
						->where('product_id',$product_id)
						->update('product_information');

				//insert stock
				$in_stock = $this->input->post('in_stock');
				$data = array(
					'product_id'   => $product_id,
					'quantity' 	   => $this->input->post('quantity'),
					'date' 	   	   => date("Y-m-d"),
					'pre_quantity' => $in_stock,
				);
				$this->db->insert('quantiy_log',$data);
			}
			echo "1";
		}
	}
	//File upload 
	public function insert_image(){

		$this->permission->check_label('add_product')->create()->redirect();
		$image_type = 0;
		$size 		= 0;

		$product_id = $this->session->userdata('product_id');

		if (empty($product_id)) {
			echo "3";
		}else{
			//Product image upload
	    	if (!empty($_FILES)) {
			
				$config['upload_path']        = 'my-assets/image/product/website/';
		        $config['allowed_types']      = 'gif|jpg|png|jpeg|JPEG|GIF|JPG|PNG';
		        $config['max_size']           = "1024";
		        $config['max_width']          = "3000";
		        $config['max_height']         = "3000";
		        $config['encrypt_name'] 	  = TRUE;

		        $this->load->library('upload', $config);
		        if ( ! $this->upload->do_upload('file'))
		        {
		            echo $this->upload->display_errors();
		        }
		        else
		        {
		        	$image = $this->upload->data();

		        	//Resize image config
					$config['image_library'] 	= 'gd2';
					$config['source_image'] 	= $image['full_path'];
					$config['maintain_ratio'] 	= FALSE;
					$config['width']         	= 1440;
					$config['height']       	= 1440;
					$config['new_image']       	= 'my-assets/image/product/website/thumb/'.$image['file_name'];
					$this->load->library('image_lib', $config);
					$resize = $this->image_lib->resize();

					//For image type 1=thumb
					$size = filesize($config['new_image']);
					$size = $size/1048576;

					//Get image extention
					$info 	   = getimagesize($config['new_image']);
					$extension = image_type_to_extension($info[2]);

					$product_id    = $this->session->userdata('product_id');
					$primary_image = $this->Products->primary_image_exist_check($product_id);
				
					$data = array(
						'product_id' 	=> $product_id,
						'image_name' 	=> $image['file_name'],
						'image_type'  	=> 1,
						'image_path'  	=> $config['new_image'],
						'image_extension' => $extension,
						'upload_date'	=> date("Y-m-d h:i:s"),
						'size' 			=> number_format($size,2),
						'status' 		=> (!empty($primary_image)?$primary_image:0),
					);
					//Insert for image type=1 thumb
					$result=$this->Products->insert_product_image($data);

					if ($result) {
						//Configuration for type 2=large
						$img_url = 'my-assets/image/product/website/'.$image['file_name'];
						$size = filesize($img_url);
						$size = $size/1048576;

						$data1 = array(
							'product_id' 	=> $this->session->userdata('product_id'),
							'image_name' 	=> $image['file_name'],
							'image_type'  	=> 2,
							'image_path'  	=> $img_url,
							'image_extension' => $extension,
							'upload_date'	=> date("Y-m-d h:i:s"),
							'size' 			=> number_format($size,2),
							'status' 		=> 0,
						);
						//Insert product image
						$result=$this->Products->insert_product_image($data1);
					}

					if ($result) {
						echo '1';
					}else{
						echo "2";
					}
		        }
			}
		}
	}
	//Update file upload
	public function updateFileUpload(){

		$this->permission->check_label('manage_product')->update()->redirect();
		$image_type = 0;
		$size 		= 0;

		$product_id  = $this->input->post('product_id');

		if (empty($product_id)) {
			echo "3";
		}else{
			//Product image upload
	    	if (!empty($_FILES)) {
			
				$config['upload_path']  = "my-assets/image/product/website/";
		        $config['allowed_types']= "gif|jpg|png|jpeg|JPEG|GIF|JPG|PNG";
		        $config['max_size']     = "1024";
		        $config['max_width']    = "3000";
		        $config['max_height']   = "3000";
		        $config['encrypt_name'] = TRUE;

		        $this->load->library('upload', $config);
		        if ( ! $this->upload->do_upload('file'))
		        {
		            echo $this->upload->display_errors();
		        }
		        else
		        {
		        	$image = $this->upload->data();

		        	//Resize image config
					$config['image_library'] 	= 'gd2';
					$config['source_image'] 	= $image['full_path'];
					$config['maintain_ratio'] 	= FALSE;
					$config['width']         	= 1440;
					$config['height']       	= 1440;
					$config['new_image']       	= 'my-assets/image/product/website/thumb/'.$image['file_name'];
					$this->load->library('image_lib', $config);
					$resize = $this->image_lib->resize();

					//For image type 1=thumb
					$size = filesize($config['new_image']);
					$size = $size/1048576;

					//Get image extention
					$info 	   = getimagesize($config['new_image']);
					$extension = image_type_to_extension($info[2]);

					$primary_image = $this->Products->primary_image_exist_check($product_id);
				
					$data = array(
						'product_id' 	=> $product_id,
						'image_name' 	=> $image['file_name'],
						'image_type'  	=> 1,
						'image_path'  	=> $config['new_image'],
						'image_extension' => $extension,
						'upload_date'	=> date("Y-m-d h:i:s"),
						'size' 			=> number_format($size,2),
						'status' 		=> (!empty($primary_image)?$primary_image:0),
					);

					//Insert for image type=1 thumb
					$result=$this->Products->insert_product_image($data);

					if ($result) {
						//Configuration for type 2=large
						$img_url = 'my-assets/image/product/website/'.$image['file_name'];
						$size = filesize($img_url);
						$size = $size/1048576;

						$data1 = array(
							'product_id' 	=> $product_id,
							'image_name' 	=> $image['file_name'],
							'image_type'  	=> 2,
							'image_path'  	=> $img_url,
							'image_extension' => $extension,
							'upload_date'	=> date("Y-m-d h:i:s"),
							'size' 			=> number_format($size,2),
							'status' 		=> 0,
						);
						//Insert product image
						$result=$this->Products->insert_product_image($data1);
					}

					if ($result) {
						echo '1';
					}else{
						echo "2";
					}
		        }
			}
		}
	}
	//Delete file
	public function delete_image(){
		$this->permission->check_label('add_product')->delete()->redirect();
		$image_id   = $this->input->post('image_id');
		$image_path = FCPATH.$this->input->post('image_path');

		if (file_exists($image_path)) {
			unlink($image_path);
		}
		$delete   = $this->db->where('image_name',$image_id)
							->delete('product_image');
		if ($delete) {
			echo "1";
		}else{
			echo "2";
		}
	}
	//Make image primary
	public function make_img_primary(){
		$image_id 	= $this->input->post('image_id');
		$product_id = $this->input->post('product_id');
		$image_path = $this->input->post('image_path');
		$result   	= $this->Products->make_img_primary($image_id,$product_id,$image_path);
		if ($result) {
			echo "1";
		}else{
			echo "2";
		}
	}
	//Upload title
	public function insert_title(){
		$this->permission->check_label('add_product')->create()->redirect();
		$result 	  = FALSE;
		$lang_id 	  = $this->input->post('lang_id');
		$upload_title = $this->input->post('upload_title');
		$product_id   = $this->session->userdata('product_id');

		if (empty(array_filter($lang_id)) || empty(array_filter($upload_title)) || empty($product_id)) {
			echo "3";
		}else{
			if ($lang_id) {
				for ($i=0;$i < count($lang_id); $i++) {

					$language_id = $lang_id[$i];
					$title 	 	 = $upload_title[$i];
					
					$lang = $this->db->select('*')
								->from('product_title')
								->where('lang_id',$language_id)
								->where('product_id',$product_id)
								->get()
								->num_rows();

					if ($lang > 0) {
						$this->db->set('title',$title);
						$this->db->where('product_id',$product_id);
						$this->db->where('lang_id',$language_id);
						$result = $this->db->update('product_title');
					}else{
						$data = array(
							'product_id' => $product_id,
							'lang_id' 	=> $language_id,
							'title' 	=> $title,
							'status' 	=> 1,
						);
						$result = $this->Products->insert_product_title($data);
					}
				}
				if ($result) {
					echo "1";
				}else{
					echo "2";
				}
			}
		}
	}
	//Update Title
	public function updateTitle(){
		$this->permission->check_label('manage_product')->update()->redirect();
		$result 	  = FALSE;
		$lang_id 	  = $this->input->post('lang_id');
		$upload_title = $this->input->post('upload_title');
		$product_id   = $this->input->post('product_id');

		if (empty(array_filter($lang_id)) || empty(array_filter($upload_title)) || empty($product_id)) {
			echo "3";
		}else{
			for ($i=0;$i < count($lang_id); $i++) {

				$language_id = $lang_id[$i];
				$title 	 	 = $upload_title[$i];

				$lang = $this->db->select('*')
							->from('product_title')
							->where('lang_id',$language_id)
							->where('product_id',$product_id)
							->get()
							->num_rows();

				if ($lang > 0) {
					$this->db->set('title',$title);
					$this->db->where('product_id',$product_id);
					$this->db->where('lang_id',$language_id);
					$result = $this->db->update('product_title');
				}else{
					$data = array(
						'product_id' => $product_id,
						'lang_id' 	=> $language_id,
						'title' 	=> $title,
						'status' 	=> 0,
					);
					$result = $this->Products->insert_product_title($data);
				}
				
			}
			
			if ($result) {
				echo "1";
			}else{
				echo "2";
			}
		}
	}
	//Upload description
	public function insert_description(){
		$this->permission->check_label('add_product')->create()->redirect();
		$result 	  = FALSE;
		$lang_id 	  = $this->input->post('lang_id');
		$description  = $this->input->post('description');
		$product_id   = $this->session->userdata('product_id');

		if (empty(array_filter($lang_id)) || empty(array_filter($description)) || empty($product_id)) {
			echo "3";
		}else{
			if ($product_id) {
				for ($i=0;$i < count($lang_id); $i++) {
					$language_id = $lang_id[$i];
					$desc 	 	 = $description[$i];
					$type 	 	 = 1;
				
					$des_exis = $this->db->select('*')
								->from('product_description')
								->where('lang_id',$language_id)
								->where('product_id',$product_id)
								->where('description_type',$type)
								->get()
								->num_rows();
					if ($des_exis > 0) {
						$this->db->set('description',$desc);
						$this->db->where('product_id',$product_id);
						$this->db->where('lang_id',$language_id);
						$this->db->where('description_type',$type);
						$result = $this->db->update('product_description');
					}else{
						$data = array(
							'product_id' 	=> $product_id,
							'lang_id' 		=> $language_id,
							'description' 	=> $desc,
							'description_type' => $type,
							'status' 		=> 0,
						);
						$result = $this->Products->insert_product_description($data);
					}
				}
				if ($result) {		
					echo "1";
				}else{
					echo "2";
				}
			}
		}
	}
	//Update description
	public function updateDescription(){
		$this->permission->check_label('manage_product')->update()->redirect();
		$result 	  = FALSE;
		$lang_id 	  = $this->input->post('lang_id');
		$description  = $this->input->post('description');
		$product_id    = $this->input->post('product_id');

		if (empty(array_filter($lang_id)) || empty(array_filter($description)) || empty($product_id)) {
			echo "3";
		}else{

			for ($i=0;$i < count($lang_id); $i++) {

				$language_id = $lang_id[$i];
				$desc 	 	 = $description[$i];
				$type 	 	 = 1;

				$des_exis = $this->db->select('*')
							->from('product_description')
							->where('lang_id',$language_id)
							->where('product_id',$product_id)
							->where('description_type',$type)
							->get()
							->num_rows();

				if ($des_exis > 0) {
					$this->db->set('description',$desc);
					$this->db->where('product_id',$product_id);
					$this->db->where('lang_id',$language_id);
					$this->db->where('description_type',$type);
					$result = $this->db->update('product_description');
				}else{
					$data = array(
						'product_id' 	=> $product_id,
						'lang_id' 		=> $language_id,
						'description' 	=> $desc,
						'description_type' => $type,
						'status' 		=> 1,
					);
					$result = $this->Products->insert_product_description($data);
				}
				
			}
			
			if ($result) {		
				echo "1";
			}else{
				echo "2";
			}
		}
	}
	//Upload Specification
	public function insert_specification(){
		$this->permission->check_label('add_product')->create()->redirect();
		$result 	  = FALSE;
		$lang_id 	  = $this->input->post('lang_id');
		$description  = $this->input->post('description');
		$product_id   = $this->session->userdata('product_id');

		$product = $this->db->select('*')
				->from('product_title')
				->where('product_id',$product_id)
				->get()
				->num_rows();

		$des = $this->db->select('*')
				->from('product_description')
				->where('product_id',$product_id)
				->where('description_type',1)
				->get()
				->num_rows();

		if (empty(array_filter($lang_id)) || empty(array_filter($description)) || empty($product_id) || $product == 0 || $des == 0) {
			echo "3";
		}else{
			if ($product_id) {
				for ($i=0;$i < count($lang_id); $i++) {
					$language_id = $lang_id[$i];
					$desc 	 	 = $description[$i];
					$type 	 	 = 2;

					$des_exis = $this->db->select('*')
								->from('product_description')
								->where('lang_id',$language_id)
								->where('product_id',$product_id)
								->where('description_type',$type)
								->get()
								->num_rows();

					if ($des_exis > 0) {
						$this->db->set('description',$desc);
						$this->db->where('product_id',$product_id);
						$this->db->where('lang_id',$language_id);
						$this->db->where('description_type',$type);
						$result = $this->db->update('product_description');
					}else{
						$data = array(
							'product_id' 	=> $product_id,
							'lang_id' 		=> $language_id,
							'description' 	=> $desc,
							'description_type' => $type,
							'status' 		=> 1,
						);
						$result = $this->Products->insert_product_description($data);
					}
					
				}
				
				if ($result) {

					//Update product status
					$this->db->set('status',2)
							->where('product_id',$product_id)
							->update('product_information');

					$p_q=$this->db->select('quantity')
							->from('product_information')
							->where('product_id',$product_id)
							->get()
							->row();

					//Data insert info quantity log
					$data = array(
						'product_id' 	 => $product_id, 
						'date' 		 	 => date('Y-m-d'), 
						'quantity' 	 	 => $p_q->quantity
					);
					$this->db->insert('quantiy_log',$data);

					$seller = $this->db->select('b.email')
							->from('product_information a ')
							->join('seller_information b','a.seller_id = b.seller_id')
							->where('product_id',$product_id)
							->get()
							->row();

					if ($seller->email) {

						$setting_detail = $this->Soft_settings->retrieve_email_editdata();
						$product_info   = $this->Products->retrive_product_info($product_id);
						$company_info   = $this->Companies->company_list();
						$template 		= $this->Email_templates->retrieve_template('11');

			            $config = array(
			                'protocol'      => $setting_detail[0]['protocol'],
			                'smtp_host'     => $setting_detail[0]['smtp_host'],
			                'smtp_port'     => $setting_detail[0]['smtp_port'],
			                'smtp_user'     => $setting_detail[0]['sender_email'], 
			                'smtp_pass'     => $setting_detail[0]['password'], 
			                'mailtype'      => $setting_detail[0]['mailtype'], 
			                'charset'       => 'utf-8'
			            );
			            $this->email->initialize($config);
			            $this->email->set_mailtype($setting_detail[0]['mailtype']);
			            $this->email->set_newline("\r\n");

			            //Email Message Set
			            $subject  = $template->subject;
			            $message  = $template->message;
			            $message .= "<p>".display('your_product_information')."

			            			<table style=\"border:1px solid\">
										<tr>
											<th style=\"text-align: left;border: 1px solid;\">".display('title').":</th>
											<td style=\"border: 1px solid;\">".$product_info->title."</td>
										</tr>
										<tr>
											<th style=\"text-align: left;border: 1px solid;\">".display('category_name').":</th>
											<td style=\"border: 1px solid;\">".$product_info->category_name."</td>
										</tr>
										<tr>
											<th style=\"text-align: left;border: 1px solid;\">".display('price').":</th>
											<td style=\"border: 1px solid;\">".$product_info->price."</td>
										</tr>
										<tr>
											<th style=\"text-align: left;border: 1px solid;\">".display('product_model').":</th>
											<td style=\"border: 1px solid;\">".$product_info->product_model."</td>
										</tr>
										<tr>
											<th style=\"text-align: left;border: 1px solid;\">".display('brand').":</th>
											<td style=\"border: 1px solid;\">".$this->input->post('brand_name')."</td>
										</tr>
										<tr>
											<th style=\"text-align: left;border: 1px solid;\">".display('product_type').":</th>
											<td style=\"border: 1px solid;\">".$product_info->product_type."</td>
										</tr>
										<tr>
											<th style=\"text-align: left;border: 1px solid;\">".display('tag').":</th>
											<td style=\"border: 1px solid;\">".$product_info->tag."</td>
										</tr>
										<tr>
											<th style=\"text-align: left;border: 1px solid;\">".display('quantity').":</th>
											<td style=\"border: 1px solid;\">".$product_info->quantity."</td>
										</tr>
										<tr>
											<th style=\"text-align: left;border: 1px solid;\">".display('image').":</th>
											<td style=\"border: 1px solid;\"><img src=".base_url($product_info->thumb_image_url)."></td>
										</tr>
										<tr>
											<th style=\"text-align: left;border: 1px solid;\">".display('comission').":</th>
											<td style=\"border: 1px solid;\">".$product_info->comission."</td>
										</tr>
									</table>";
			            
			            //Email content
			            $this->email->to($seller->email);
			            $this->email->from($setting_detail[0]['sender_email'],$company_info[0]['company_name']);
			            $this->email->subject($subject);
			            $this->email->message($message);

					    $email = $this->test_input($seller->email);
						if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
						    if($this->email->send())
						    {
						    	echo "1";
						    }else{
						     	echo "3";
						    }
						}else{
						    echo "3";
						}
					}else{
						echo "3";
					}
					
					//Unset product id
					$this->session->unset_userdata('product_id');
				}else{
					echo "2";
				}
			}
		}
	}
	//Update specification
	public function updateSpecification(){
		$this->permission->check_label('manage_product')->update()->redirect();
		$result 	  = FALSE;
		$lang_id 	  = $this->input->post('lang_id');
		$description  = $this->input->post('description');
		$product_id   = $this->input->post('product_id');

		$product = $this->db->select('*')
				->from('product_title')
				->where('product_id',$product_id)
				->get()
				->num_rows();

		$des = $this->db->select('*')
				->from('product_description')
				->where('product_id',$product_id)
				->where('description_type',1)
				->get()
				->num_rows();

		if (empty(array_filter($lang_id)) || empty(array_filter($description)) || empty($product_id) || $product == 0 || $des == 0) {
			echo "3";
		}else{

			for ($i=0;$i < count($lang_id); $i++) {

				$language_id = $lang_id[$i];
				$desc 	 	 = $description[$i];
				$type 	 	 = 2;

				$des_exis = $this->db->select('*')
							->from('product_description')
							->where('lang_id',$language_id)
							->where('product_id',$product_id)
							->where('description_type',$type)
							->get()
							->num_rows();

				if ($des_exis > 0) {
					$this->db->set('description',$desc);
					$this->db->where('product_id',$product_id);
					$this->db->where('lang_id',$language_id);
					$this->db->where('description_type',$type);
					$result = $this->db->update('product_description');
				}else{
					$data = array(
						'product_id' 	=> $product_id,
						'lang_id' 		=> $language_id,
						'description' 	=> $desc,
						'description_type' => $type,
						'status' 		=> 0,
					);
					$result = $this->Products->insert_product_description($data);
				}
			}
			
			if ($result) {
				$this->session->unset_userdata('product_id');
				echo "1";
			}else{
				echo "2";
			}
		}
	}
	//Product status update
	public function update_approved_status($product_id=null){
		$this->permission->check_label('manage_product')->update()->redirect();

		$product_status = $this->input->post('product_status');

		if ($product_status) {
			$this->db->set('status',$product_status)
					->where('product_id',$product_id)
					->update('product_information');

			$this->db->set('status',$product_status)
					->where('product_id',$product_id)
					->update('upload_product');
			$result = $this->Products->create_product_json();
		}

		if ($result == TRUE) {
	
			$seller_email   = $this->input->post('seller_email');
			
			if ($product_status) {

				if ($product_status == 1) {
					$template_id = 10;
				}elseif ($product_status == 2) {
					$template_id = 11;
				}elseif ($product_status == 3) {
					$template_id = 12;
				}

				//send email with as a link
	            $setting_detail = $this->Soft_settings->retrieve_email_editdata();
	            $company_info   = $this->Companies->company_list();
	            $template 		= $this->Email_templates->retrieve_template($template_id);
	            $product_info   = $this->Products->retrive_product_info($product_id);

	            $config = array(
	                'protocol'      => $setting_detail[0]['protocol'],
	                'smtp_host'     => $setting_detail[0]['smtp_host'],
	                'smtp_port'     => $setting_detail[0]['smtp_port'],
	                'smtp_user'     => $setting_detail[0]['sender_email'], 
	                'smtp_pass'     => $setting_detail[0]['password'], 
	                'mailtype'      => $setting_detail[0]['mailtype'], 
	                'charset'       => 'utf-8'
	            );
	            $this->email->initialize($config);
	            $this->email->set_mailtype($setting_detail[0]['mailtype']);
	            $this->email->set_newline("\r\n");

	            $message = $template->message;
	            $message .= "<p>".display('your_product_information')."
	            			<table style=\"border:1px solid\">
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('title').":</th>
									<td style=\"border: 1px solid;\">".$product_info->title."</td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('category_name').":</th>
									<td style=\"border: 1px solid;\">".$product_info->category_name."</td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('price').":</th>
									<td style=\"border: 1px solid;\">".$product_info->price."</td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('product_model').":</th>
									<td style=\"border: 1px solid;\">".$product_info->product_model."</td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('brand').":</th>
									<td style=\"border: 1px solid;\">".$this->input->post('brand_name')."</td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('product_type').":</th>
									<td style=\"border: 1px solid;\">".$product_info->product_type."</td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('tag').":</th>
									<td style=\"border: 1px solid;\">".$product_info->tag."</td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('quantity').":</th>
									<td style=\"border: 1px solid;\">".$product_info->quantity."</td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('image').":</th>
									<td style=\"border: 1px solid;\"><img src=".base_url($product_info->thumb_image_url)." height=\"50\" width=\"50\"></td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('comission').":</th>
									<td style=\"border: 1px solid;\">".$product_info->comission."</td>
								</tr>
							</table>";
	            
	            //Email content
	            $this->email->to($seller_email);
	            $this->email->from($setting_detail[0]['sender_email'],$company_info[0]['company_name']);
	            $this->email->subject($template->subject);
	            $this->email->message($message);

			    $email = $this->test_input($seller_email);
				if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
				    if($this->email->send())
				    {
				      	$this->session->set_userdata(array('message'=>display('email_send_to_seller')));
				      	redirect(base_url('cproduct/manage_product/all/item'));
				    }else{
				     	$this->session->set_userdata(array('error_message'=>display('email_was_not_sent_please_contact_administrator')));
				     	redirect(base_url('cproduct/manage_product/all/item'));
				    }
				}else{
					$this->session->set_userdata(array('error_message'=>display('your_email_was_not_found')));
				    redirect(base_url('cproduct/manage_product/all/item'));
				}
			}else{
				$this->session->set_userdata(array('error_message'=>display('please_add_template')));
				redirect(base_url('cproduct/manage_product/all/item'));
			}
		}else{
			$this->session->set_userdata(array('error_message'=>display('already_exists')));
			redirect(base_url('cproduct/manage_product/all/item'));
		}
	}
	//Email testing for email
	public function test_input($data) {
	  $data = trim($data);
	  $data = stripslashes($data);
	  $data = htmlspecialchars($data);
	  return $data;
	}
	//Product delete
	public function delete_product($product_id=null){
		$this->permission->check_label('manage_product')->delete()->redirect();
		$product_image = $this->db->select('*')
						->from('product_image')
						->where('product_id',$product_id)
						->get()
						->result();

		if ($product_image) {
			foreach ($product_image as $image) {
				$image_path = FCPATH.$image->image_path;
				if (file_exists($image_path)) {
					unlink($image_path);
				}
			}
		}

		//Delete row from database
		$result = $this->db->where('product_id',$product_id)
						->delete('product_image');

		$result = $this->db->where('product_id',$product_id)
						->delete('product_description');

		$result = $this->db->where('product_id',$product_id)
						->delete('product_title');

		$result = $this->db->where('product_id',$product_id)
						->delete('product_information');
		
		$this->session->set_userdata(array('message'=>display('delete_successfully')));
		redirect('cproduct/manage_product/all/item');
	}
	//Retrieve Single Item  By Search
	public function product_by_search()
	{
		$this->load->library('lproduct');
		$product_id = $this->input->post('product_id');		
        $content = $this->lproduct->product_search_list($product_id);
		$this->template->full_admin_html_view($content);
	}
	//Retrieve Single Item  By Search
	public function product_details($product_id=null)
	{
		$this->permission->check_label('manage_product')->read()->redirect();
		$this->product_id=$product_id;
		$this->load->library('lproduct');	
        $content = $this->lproduct->product_details($product_id);
		$this->template->full_admin_html_view($content);
	}
	//Retrieve Single Item  By Search
	public function product_details_single()
	{
		$this->permission->check_label('manage_product')->read()->redirect();
		$product_id = $this->input->post('product_id');
		$this->product_id=$product_id;
		$this->load->library('lproduct');	
        $content = $this->lproduct->product_details($product_id);
		$this->template->full_admin_html_view($content);
	}
	//This function is used to Generate Key
	public function generator($lenth)
	{
		$this->load->model('Products');

		$number=array("1","2","3","4","5","6","7","8","9","0");
		for($i=0; $i<$lenth; $i++)
		{
			$rand_value=rand(0,8);
			$rand_number=$number["$rand_value"];
		
			if(empty($con))
			{ 
				$con=$rand_number;
			}
			else
			{
				$con="$con"."$rand_number";
			}
		}

		$result = $this->Products->product_id_check($con);

		if ($result === true) {
			$this->generator(8);
		}else{
			return $con;
		}
	}

	#==========================================#
	#=========== Pending Product ==============#
	#==========================================#

	//Pending product
	public function pending_product()
	{
		$this->permission->check_label('pending_product')->access();
		$seller = $this->input->get('seller');
		$model 	= $this->input->get('model');
		$title 	= $this->input->get('title');
		$category = $this->input->get('category');

		#
        #pagination starts
        #
        $config["base_url"] 	    = base_url('cproduct/pending_product/all/item');
        $config['reuse_query_string']= true;
        $config["total_rows"] 	    = $this->Products->pending_product_count($seller,$model,$title,$category);
        $config["per_page"] 	    = 30;
        $config["uri_segment"] 	    = 5;
        $config["num_links"] 	    = 5; 
	    /* This Application Must Be Used With BootStrap 3 * */
        $config['full_tag_open'] 	= "<ul class='pagination'>";
        $config['full_tag_close'] 	= "</ul>";
        $config['num_tag_open'] 	= '<li>';
        $config['num_tag_close'] 	= '</li>';
        $config['cur_tag_open'] 	= "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] 	= "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] 	= "<li>";
        $config['next_tag_close'] 	= "</li>";
        $config['prev_tag_open'] 	= "<li>";
        $config['prev_tagl_close'] 	= "</li>";
        $config['first_tag_open'] 	= "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] 	= "<li>";
        $config['last_tagl_close'] 	= "</li>";
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $links = $this->pagination->create_links();
        #
        #pagination ends
        #

		$content = $this->lproduct->pending_product($links,$config["per_page"],$page,$seller,$model,$title,$category);
		$this->template->full_admin_html_view($content);
	}
	//Pending Product Update
	public function pending_product_update($upload_id=null){
		$this->permission->check_label('pending_product')->update()->redirect();
		$content = $this->lproduct->pending_product_update($upload_id);
		$this->template->full_admin_html_view($content);
	}
	//Update upload product
	public function update_upload_product(){

		$this->permission->check_label('pending_product')->update()->redirect();
		$upload_id  = $this->input->post('upload_id');
		$product_id = $this->input->post('product_id');

		$variant = 0;
		if ($this->input->post('variant')) {
			$variant = implode(",",$this->input->post('variant'));
		}else{
			$variant = 0;
		}

		$data = array(
			'seller_id' 	=> $this->input->post('seller_id'),
			'product_id' 	=> $this->input->post('product_id'),
			'category_id' 	=> $this->input->post('category_id'),
			'price' 		=> $this->input->post('price'),
			'quantity'  	=> $this->input->post('quantity'),
			'product_model' => $this->input->post('product_model'),
			'variant_id' 	=>  (!empty($variant))?$variant:null,
			'unit' 			=> $this->input->post('unit'),
			'brand_id' 		=> $this->input->post('brand'),
			'product_type' 	=> $this->input->post('product_type'),
			'tag' 			=> $this->input->post('tag_value'),
			'status' 		=> $this->input->post('status'),

			'on_promotion' 	=> $this->input->post('on_promotion'),
			'details' 		=> $this->input->post('details'),
			'best_sale' 	=> $this->input->post('best_sale'),
			'pre_order' 	=> $this->input->post('pre_order'),
			'pre_order_quantity' => $this->input->post('pre_order_quantity'),
			'comission' 	=> $this->input->post('comission'),
		);

		$this->db->select('*');
		$this->db->from('upload_product');
		$this->db->where('product_model',$data['product_model']);
		$query = $this->db->get();

		if ($query->num_rows() > 0) {

			$data1 = array(
				'seller_id' 	=> $this->input->post('seller_id'),
				'product_id' 	=> $this->input->post('product_id'),
				'category_id' 	=> $this->input->post('category_id'),
				'price' 		=> $this->input->post('price'),
				'quantity'  	=> $this->input->post('quantity'),
				'variant_id' 	=> (!empty($variant))?$variant:null,
				'unit' 			=> $this->input->post('unit'),
				'brand_id' 		=> $this->input->post('brand'),
				'product_type' 	=> $this->input->post('product_type'),
				'tag' 			=> $this->input->post('tag_value'),
				'status' 		=> $this->input->post('status'),

				'on_promotion' 	=> $this->input->post('on_promotion'),
				'details' 		=> $this->input->post('details'),
				'best_sale' 	=> $this->input->post('best_sale'),
				'pre_order' 	=> $this->input->post('pre_order'),
				'pre_order_quantity' => $this->input->post('pre_order_quantity'),
				'comission' 	=> $this->input->post('comission'),
			);
			//Product information update
			$this->db->where('product_id',$product_id);
			$this->db->update('product_information',$data1);


			$this->db->where('upload_id',$upload_id);
			$result = $this->db->update('upload_product',$data1);
			echo "1";
		}else{
			//Product information update
			$this->db->where('product_id',$product_id);
			$this->db->update('product_information',$data);

			//Upload product
			$this->db->where('upload_id',$upload_id);
			$this->db->update('upload_product',$data);
			echo "1";
		}
	}
	//Update file upload
	public function update_upload_image(){

		$this->permission->check_label('pending_product')->update()->redirect();
		$image_type = 0;
		$size 		= 0;

		$upload_id  = $this->input->post('upload_id');

		if (empty($upload_id)) {
			echo "3";
		}else{
			//Product image upload
	    	if (!empty($_FILES)) {
			
				$config['upload_path']          = "my-assets/image/product/temp/";
		        $config['allowed_types']        = "gif|jpg|png|jpeg|JPEG|GIF|JPG|PNG";
		        $config['max_size']             = "1024";
		        $config['max_width']            = "3000";
		        $config['max_height']           = "3000";
		        $config['encrypt_name'] 		= TRUE;

		        $this->load->library('upload', $config);
		        if ( ! $this->upload->do_upload('file'))
		        {
		            echo $this->upload->display_errors();
		        }
		        else
		        {
		        	$image = $this->upload->data();

		        	//Resize image config
					$config['image_library'] 	= 'gd2';
					$config['source_image'] 	= $image['full_path'];
					$config['maintain_ratio'] 	= FALSE;
					$config['width']         	= 1440;
					$config['height']       	= 1440;
					$config['new_image']       	= 'my-assets/image/product/temp/thumb/'.$image['file_name'];
					$this->load->library('image_lib', $config);
					$resize = $this->image_lib->resize();

					//For image type 1=thumb
					$size = filesize($config['new_image']);
					$size = $size/1048576;

					//Get image extention
					$info 	   = getimagesize($config['new_image']);
					$extension = image_type_to_extension($info[2]);

					$primary_image = $this->input->post('primary_image');

					$primary_image = $this->Products->primary_image_exist_check_upload($upload_id);


					$data = array(
						'upload_id' 	=> $upload_id,
						'image_name' 	=> $image['file_name'],
						'image_size' 	=> number_format($size,2),
						'image_extension' => $extension,
						'date_of_upload'=> date("Y-m-d h:i:s"),
						'image_type'  	=> 1,
						'image_url'  	=> $config['new_image'],
						'status' 		=> $primary_image,
					);

					//Insert for image type=1 thumb
					$result=$this->Products->insert_upload_image($data);

					if ($result) {
						//Configuration for type 2=large
						$img_url = 'my-assets/image/product/temp/'.$image['file_name'];
						$size = filesize($img_url);
						$size = $size/1048576;

						$data1 = array(
							'upload_id' 	=> $upload_id,
							'image_name' 	=> $image['file_name'],
							'image_size' 	=> number_format($size,2),
							'image_extension' => $extension,
							'date_of_upload'=> date("Y-m-d h:i:s"),
							'image_type'  	=> 2,
							'image_url'  	=> $img_url,
							'status' 		=> 0,
						);
						//Insert product image
						$result=$this->Products->insert_upload_image($data1);
					}

					if ($result) {
						echo '1';
					}else{
						echo "2";
					}
		        }
			}
		}
	}
	//Delete file
	public function delete_upload_image(){
		$this->permission->check_label('pending_product')->delete()->redirect();
		$image_id   = $this->input->post('image_id');
		$image_path = FCPATH.$this->input->post('image_path');
		if (file_exists(base_url($image_path))) {
			unlink($image_path);
		}
		$delete   = $this->db->where('image_name',$image_id)
							->delete('upload_image');
		if ($delete) {
			echo "1";
		}else{
			echo "2";
		}
	}
	//Make pending image primary
	public function make_pending_img_primary(){
		$this->permission->check_label('pending_product')->create()->redirect();
		$image_id  = $this->input->post('image_id');
		$upload_id = $this->input->post('upload_id');

		$result   = $this->Products->make_pending_img_primary($image_id,$upload_id);
		if ($result) {
			echo "1";
		}else{
			echo "2";
		}
	}
	//Update Title
	public function update_upload_title(){
		$this->permission->check_label('pending_product')->update()->redirect();
		$result 	  = FALSE;
		$lang_id 	  = $this->input->post('lang_id');
		$upload_title = $this->input->post('upload_title');
		$upload_id    = $this->input->post('upload_id');

		if (empty(array_filter($lang_id)) || empty(array_filter($upload_title)) || empty($upload_id)) {
			echo "3";
		}else{
			for ($i=0;$i < count($lang_id); $i++) {

				$language_id = $lang_id[$i];
				$title 	 	 = $upload_title[$i];

				$lang = $this->db->select('*')
							->from('upload_product_title')
							->where('lang_id',$language_id)
							->where('upload_id',$upload_id)
							->get()
							->num_rows();

				if ($lang > 0) {
					$this->db->set('title',$title);
					$this->db->where('upload_id',$upload_id);
					$this->db->where('lang_id',$language_id);
					$result = $this->db->update('upload_product_title');
				}else{
					$data = array(
						'upload_id' => $upload_id,
						'lang_id' 	=> $language_id,
						'title' 	=> $title,
						'status' 	=> 0,
					);
					$result = $this->Products->insert_upload_title($data);
				}
				
			}
			
			if ($result) {
				echo "1";
			}else{
				echo "2";
			}
		}
	}
	//Update description
	public function update_upload_escription(){
		$this->permission->check_label('pending_product')->update()->redirect();
		$result 	  = FALSE;
		$lang_id 	  = $this->input->post('lang_id');
		$description  = $this->input->post('description');
		$upload_id    = $this->input->post('upload_id');

		if (empty(array_filter($lang_id)) || empty(array_filter($description)) || empty($upload_id)) {
			echo "3";
		}else{

			for ($i=0;$i < count($lang_id); $i++) {

				$language_id = $lang_id[$i];
				$desc 	 	 = $description[$i];
				$type 	 	 = 1;

				$des_exis = $this->db->select('*')
							->from('upload_product_description')
							->where('lang_id',$language_id)
							->where('upload_id',$upload_id)
							->where('description_type',$type)
							->get()
							->num_rows();

				if ($des_exis > 0) {
					$this->db->set('description',$desc);
					$this->db->where('upload_id',$upload_id);
					$this->db->where('lang_id',$language_id);
					$this->db->where('description_type',$type);
					$result = $this->db->update('upload_product_description');
				}else{
					$data = array(
						'upload_id' 	=> $upload_id,
						'lang_id' 		=> $language_id,
						'description' 	=> $desc,
						'description_type' => $type,
						'status' 		=> 0,
					);
					$result = $this->Products->insert_upload_description($data);
				}
				
			}
			
			if ($result) {		
				echo "1";
			}else{
				echo "2";
			}
		}
	}
	//Update specification
	public function update_upload_specification(){
		$this->permission->check_label('pending_product')->update()->redirect();
		$result 	  = FALSE;
		$lang_id 	  = $this->input->post('lang_id');
		$description  = $this->input->post('description');
		$upload_id    = $this->input->post('upload_id');

		$product = $this->db->select('*')
				->from('upload_product_title')
				->where('upload_id',$upload_id)
				->get()
				->num_rows();

		$des = $this->db->select('*')
				->from('upload_product_description')
				->where('upload_id',$upload_id)
				->where('description_type',1)
				->get()
				->num_rows();
		
		if (empty(array_filter($lang_id)) || empty(array_filter($description)) || empty($upload_id) || ($product == 0) || ($des == 0)) {
			echo "3";
		}else{

			for ($i=0;$i < count($lang_id); $i++) {

				$language_id = $lang_id[$i];
				$desc 	 	 = $description[$i];
				$type 	 	 = 2;

				$des_exis = $this->db->select('*')
							->from('upload_product_description')
							->where('lang_id',$language_id)
							->where('upload_id',$upload_id)
							->where('description_type',$type)
							->get()
							->num_rows();

				if ($des_exis > 0) {
					$this->db->set('description',$desc);
					$this->db->where('upload_id',$upload_id);
					$this->db->where('lang_id',$language_id);
					$this->db->where('description_type',$type);
					$result = $this->db->update('upload_product_description');
				}else{
					$data = array(
						'upload_id' 	=> $upload_id,
						'lang_id' 		=> $language_id,
						'description' 	=> $desc,
						'description_type' => $type,
						'status' 		=> 0,
					);
					$result = $this->Products->insert_product_description($data);
				}
			}
			
			if ($result) {		
				echo "1";
			}else{
				echo "2";
			}
		}
	}
	//Product delete
	public function delete_upload_product($upload_id=null){
		$this->permission->check_label('pending_product')->delete()->redirect();
		$upload_image = $this->db->select('*')
								->from('upload_image')
								->where('upload_id',$upload_id)
								->get()
								->result();
		if ($upload_image) {
			foreach ($upload_image as $image) {
				$image_path = FCPATH.$image->image_url;
				if (file_exists($image_path)) {
					unlink($image_path);
				}
			}
		}

		//Delete row from database
		$result = $this->db->where('upload_id',$upload_id)
						->delete('upload_image');
		$result = $this->db->where('upload_id',$upload_id)
						->delete('upload_product');
		$result = $this->db->where('upload_id',$upload_id)
						->delete('upload_product_description');
		$result = $this->db->where('upload_id',$upload_id)
						->delete('upload_product_title');
		if ($result) {
			$this->session->set_userdata(array('message'=>display('delete_successfully')));
			redirect('cproduct/pending_product/all/item');
		}
	}
	//Product status update
	public function update_upload_status($upload_id=null){
		$this->permission->check_label('pending_product')->update()->redirect();
		$product_status = $this->input->post('product_status');
		$product_id 	= $this->input->post('product_id');

		$new_quantity 	= $this->db->select('*')
								->from('upload_product')
								->where('product_id',$product_id)
								->get()
								->row();

		if ($product_status) {
			$result = $this->Products->product_information_update($product_status,$upload_id,$product_id);
		}

		//Insert data into quantity log table 
		if ($product_status == 2) {
			$data = array(
				'upload_id'  	 => $upload_id, 
				'product_id' 	 => $product_id, 
				'date' 		 	 => date('Y-m-d'), 
				'quantity' 	 	 => $new_quantity->quantity, 
				'pre_quantity' 	 => ($new_quantity->quantity)?$new_quantity->quantity : 0
			);
			$this->db->insert('quantiy_log',$data);
		}

		if ($result == TRUE) {

			if ($product_status == 1) {
				$template_id = 10;
			}elseif ($product_status == 2) {
				$template_id = 11;
			}elseif ($product_status == 3) {
				$template_id = 12;
			}

			$seller_email = $this->input->post('seller_email');
			$template 	  = $this->Email_templates->retrieve_template($template_id);
			
			if ($template) {

				//send email with as a link
	            $setting_detail = $this->Soft_settings->retrieve_email_editdata();
	            $company_info   = $this->Companies->company_list();
	            $product_info   = $this->Products->retrive_product_info($product_id);

	            $config = array(
	                'protocol'      => $setting_detail[0]['protocol'],
	                'smtp_host'     => $setting_detail[0]['smtp_host'],
	                'smtp_port'     => $setting_detail[0]['smtp_port'],
	                'smtp_user'     => $setting_detail[0]['sender_email'], 
	                'smtp_pass'     => $setting_detail[0]['password'], 
	                'mailtype'      => $setting_detail[0]['mailtype'], 
	                'charset'       => 'utf-8'
	            );
	            $this->email->initialize($config);
	            $this->email->set_mailtype($setting_detail[0]['mailtype']);
	            $this->email->set_newline("\r\n");

	            $message = $template->message;
	            if ($product_info) {
	            $message .= "<p>".display('your_product_information')."
	            			<table style=\"border:1px solid\">
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('title').":</th>
									<td style=\"border: 1px solid;\">".$product_info->title."</td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('category_name').":</th>
									<td style=\"border: 1px solid;\">".$product_info->category_name."</td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('price').":</th>
									<td style=\"border: 1px solid;\">".$product_info->price."</td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('product_model').":</th>
									<td style=\"border: 1px solid;\">".$product_info->product_model."</td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('brand').":</th>
									<td style=\"border: 1px solid;\">".$this->input->post('brand_name')."</td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('product_type').":</th>
									<td style=\"border: 1px solid;\">".$product_info->product_type."</td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('tag').":</th>
									<td style=\"border: 1px solid;\">".$product_info->tag."</td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('quantity').":</th>
									<td style=\"border: 1px solid;\">".$product_info->quantity."</td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('image').":</th>
									<td style=\"border: 1px solid;\"><img src=".base_url($product_info->thumb_image_url)."></td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('comission').":</th>
									<td style=\"border: 1px solid;\">".$product_info->comission."</td>
								</tr>
							</table>";
	            }
	            //Email content
	            $this->email->to($seller_email);
	            $this->email->from($setting_detail[0]['sender_email'],$company_info[0]['company_name']);
	            $this->email->subject($template->subject);
	            $this->email->message($message);

			    $email = $this->test_input($seller_email);
				if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
				    if($this->email->send())
				    {
				      	$this->session->set_userdata(array('message'=>display('email_send_to_seller')));
				      	redirect(base_url('cproduct/pending_product/all/item'));
				    }else{
				     	$this->session->set_userdata(array('error_message'=>display('email_was_not_sent_please_contact_administrator')));
				     	redirect(base_url('cproduct/pending_product/all/item'));
				    }
				}else{
					$this->session->set_userdata(array('error_message'=>display('your_email_was_not_found')));
				    redirect(base_url('cproduct/pending_product/all/item'));
				}
			}else{
				$this->session->set_userdata(array('error_message'=>display('please_add_template')));
				redirect(base_url('cproduct/pending_product/all/item'));
			}
		}else{
			$this->session->set_userdata(array('error_message'=>display('already_exists')));
			redirect(base_url('cproduct/pending_product/all/item'));
		}
	}

	#==========================================#
	#=========== Denied Product ==============#
	#==========================================#

	//Denied product
	public function denied_product()
	{
		$this->permission->check_label('denied_product')->access();
		$seller = $this->input->get('seller');
		$model 	= $this->input->get('model');
		$title 	= $this->input->get('title');
		$category = $this->input->get('category');

		#
        #pagination starts
        #
        $config["base_url"] 	    = base_url('cproduct/denied_product/all/item');
        $config['reuse_query_string']= true;
        $config["total_rows"] 	    = $this->Products->denied_product_count($seller,$model,$title,$category);
        $config["per_page"] 	    = 30;
        $config["uri_segment"] 	    = 5;
        $config["num_links"] 	    = 5; 
	    /* This Application Must Be Used With BootStrap 3 * */
        $config['full_tag_open'] 	= "<ul class='pagination'>";
        $config['full_tag_close'] 	= "</ul>";
        $config['num_tag_open'] 	= '<li>';
        $config['num_tag_close'] 	= '</li>';
        $config['cur_tag_open'] 	= "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] 	= "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] 	= "<li>";
        $config['next_tag_close'] 	= "</li>";
        $config['prev_tag_open'] 	= "<li>";
        $config['prev_tagl_close'] 	= "</li>";
        $config['first_tag_open'] 	= "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] 	= "<li>";
        $config['last_tagl_close'] 	= "</li>";
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $links = $this->pagination->create_links();
        #
        #pagination ends
        #

		$content = $this->lproduct->denied_product($links,$config["per_page"],$page,$seller,$model,$title,$category);
		$this->template->full_admin_html_view($content);
	}
	//Retrive comission value
	public function retrive_com_value(){
		$category_id = $this->input->post('category_id');
		$result = $this->Products->retrive_com_value($category_id);

		$html = "";
		$html .="<div class=\"col-sm-6\">
                <div class=\"form-group row\">
                    <label for=\"comission\" class=\"col-sm-3 col-form-label\">".display('comission')."</label>
                    <div class=\"col-sm-9\">
                        <input class=\"form-control\" name =\"comission\" id=\"comission\" type=\"text\" placeholder='".display('comission')."' value='".(!empty($result)?$result->rate:0)."'>
                    </div>
                </div>
            </div>";
		echo $html;
	}
}