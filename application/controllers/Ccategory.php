<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ccategory extends CI_Controller {

	function __construct() {
      	parent::__construct();
		$this->load->library('lcategory');
		$this->load->model('Categories');
    }
	//Default loading for Category system.
	public function index()
	{
		$this->permission->check_label('add_category')->create()->redirect();
		$content = $this->lcategory->category_add_form();
		$this->template->full_admin_html_view($content);
	}
	//Category add form
	public function manage_category()
	{
		$this->permission->check_label('manage_category')->redirect();
        $content =$this->lcategory->category_list();
		$this->template->full_admin_html_view($content);;
	}
	//Insert Category and upload
	public function insert_category()
	{
		$this->permission->check_label('add_category')->create()->redirect();
		if ($_FILES['cat_image']['name']) {
			//Chapter chapter add start
			$config['upload_path']          = './my-assets/image/category/';
	        $config['allowed_types']        = 'gif|jpg|png|jpeg|JPEG|GIF|JPG|PNG';
	        $config['max_size']             = "1024";
	        $config['max_width']            = "3000";
	        $config['max_height']           = "3000";
	        $config['encrypt_name'] 		= TRUE;

	        $this->load->library('upload', $config);
	        if ( ! $this->upload->do_upload('cat_image'))
	        {
	            $this->session->set_userdata(array('error_message'=>  $this->upload->display_errors()));
	            redirect('ccategory');
	        }
	        else
	        {
	        	$image =$this->upload->data();

	        	//Resize image config
				$config['image_library'] 	= 'gd2';
				$config['source_image'] 	= $image['full_path'];
				$config['maintain_ratio'] 	= FALSE;
				$config['width']         	= 450;
				$config['height']       	= 550;
				$config['new_image']       	= 'my-assets/image/category'.$image['file_name'];
				$this->load->library('image_lib', $config);
				$resize = $this->image_lib->resize();

				$image_url = base_url().$config['new_image'];
	        }
		}		

		if ($_FILES['cat_favicon']['name']) {
			//Chapter chapter add start
			$config['upload_path']          = './my-assets/image/category/';
	        $config['allowed_types']        = 'gif|jpg|png|jpeg|JPEG|GIF|JPG|PNG';
	        $config['max_size']             = "*";
	        $config['max_width']            = "*";
	        $config['max_height']           = "*";
	        $config['encrypt_name'] 		= TRUE;

	        $this->load->library('upload', $config);
	        if ( ! $this->upload->do_upload('cat_favicon'))
	        {
	            $this->session->set_userdata(array('error_message'=>  $this->upload->display_errors()));
	            redirect('ccategory');
	        }
	        else
	        {
	        	$image 	  = $this->upload->data();
	        	$cat_icon = base_url()."my-assets/image/category/".$image['file_name'];
	        }
		}

		$parent_category = $this->input->post('parent_category');

	  	//Category  basic information adding.
		$data=array(
			'category_id' 	=> $this->auth->generator(15),
			'category_name' => $this->input->post('category_name'),
			'top_menu' 		=> $this->input->post('top_menu'),
			'menu_pos' 		=> $this->input->post('menu_position'),
			'featured' 		=> $this->input->post('featured'),
			'details' 		=> $this->input->post('details'),
			'cat_favicon'   => (!empty($cat_icon)?$cat_icon:base_url('my-assets/image/category.png')),
			'parent_category_id' => $parent_category,
			'cat_image' 	=> (!empty($image_url)?$image_url:null),
			'cat_type' 		=> (!empty($parent_category)?2:1),
			'home_page' 	=> (!empty($this->input->post('mobile_home_page'))?$this->input->post('mobile_home_page'):0),
			'status' 		=> 1
		);

		$result = $this->Categories->category_entry($data);

		if ($result == TRUE) {		
			$this->session->set_userdata(array('message'=>display('successfully_added')));
			if(isset($_POST['add-category'])){
				redirect(base_url('manage_category'));
			}elseif(isset($_POST['add-category-another'])){
				redirect(base_url('ccategory'));
			}
		}else{
			$this->session->set_userdata(array('error_message'=>display('parent_category_name_exists')));
			redirect(base_url('ccategory'));
		}
	}
	//Category Update Form
	public function category_update_form($category_id)
	{	
		$this->permission->check_label('manage_category')->update()->redirect();
		$content = $this->lcategory->category_edit_data($category_id);
		$this->template->full_admin_html_view($content);
	}
	//Category Update
	public function category_update()
	{
		
		$this->permission->check_label('manage_category')->update()->redirect();
		$category_id  = $this->input->post('category_id');

		if ($_FILES['cat_image']['name']) {
			//Chapter chapter add start
			$config['upload_path']          = './my-assets/image/category/';
	        $config['allowed_types']        = 'gif|jpg|png|jpeg|JPEG|GIF|JPG|PNG';
	        $config['max_size']             = "1024";
	        $config['max_width']            = "3000";
	        $config['max_height']           = "3000";
	        $config['encrypt_name'] 		= TRUE;

	        $this->load->library('upload', $config);
	        if ( ! $this->upload->do_upload('cat_image'))
	        {
	            $this->session->set_userdata(array('error_message'=>  $this->upload->display_errors()));
	            redirect('manage_category');
	        }
	        else
	        {
	        	$image =$this->upload->data();

	        	//Resize image config
				$config['image_library'] 	= 'gd2';
				$config['source_image'] 	= $image['full_path'];
				$config['maintain_ratio'] 	= FALSE;
				$config['width']         	= 450;
				$config['height']       	= 550;
				$config['new_image']       	= 'my-assets/image/category'.$image['file_name'];
				$this->load->library('image_lib', $config);
				$resize = $this->image_lib->resize();

				$image_url = base_url().$config['new_image'];
	        }
		}

		if ($_FILES['cat_favicon']['name']) {
			//Chapter chapter add start
			$config['upload_path']          = './my-assets/image/category/';
	        $config['allowed_types']        = 'gif|jpg|png|jpeg|JPEG|GIF|JPG|PNG';
	        $config['max_size']             = "*";
	        $config['max_width']            = "*";
	        $config['max_height']           = "*";
	        $config['encrypt_name'] 		= TRUE;

	        $this->load->library('upload', $config);
	        if ( ! $this->upload->do_upload('cat_favicon'))
	        {
	            $this->session->set_userdata(array('error_message'=>  $this->upload->display_errors()));
	            redirect('ccategory');
	        }
	        else
	        {
	        	$image 	  = $this->upload->data();
	        	$cat_icon = base_url()."my-assets/image/category/".$image['file_name'];
	        }
		}

		$old_image = $this->input->post('old_image');
		$old_cat_icon = $this->input->post('old_cat_icon');

		$parent_category = $this->input->post('parent_category');

		//Category basic information update.
		$data=array(
			'category_name' => $this->input->post('category_name'),
			'top_menu' 		=> $this->input->post('top_menu'),
			'menu_pos' 		=> $this->input->post('menu_position'),
			'featured' 		=> $this->input->post('featured'),
			'details' 		=> $this->input->post('details'),
			'home_page' 	=> $this->input->post('mobile_home_page'),
			'cat_favicon' 	=> (!empty($cat_icon)?$cat_icon:$old_cat_icon),
			'parent_category_id' => $parent_category,
			'cat_image' 	=> (!empty($image_url)?$image_url:$old_image),
			'cat_type' 		=> (!empty($parent_category)?2:1),
			'status' 		=> $this->input->post('status')
		);
		$result = $this->Categories->update_category($data,$category_id);

		if ($result == TRUE) {		
			$this->session->set_userdata(array('message'=>display('successfully_updated')));
			redirect(base_url('manage_category'));
		}else{
			$this->session->set_userdata(array('error_message'=>display('parent_category_name_exists')));
			redirect(base_url('manage_category'));
		}
	}
	//Category delete
	public function category_delete($category_id)
	{
		$this->permission->check_label('manage_category')->delete()->redirect();
		$this->Categories->delete_category($category_id);
		$this->session->set_userdata(array('message'=>display('successfully_delete')));
		redirect('manage_category');
	}
}