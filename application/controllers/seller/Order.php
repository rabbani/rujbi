<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Order extends CI_Controller {
	
	function __construct() {
      	parent::__construct();
      	$this->seller_auth->check_seller_auth();
      	$this->load->library('seller/Lorder');
      	$this->load->model('seller/Orders');
    }

    //Index page load first
	public function index()
	{
		$content = $this->lorder->order_add_form();
		$this->template->full_seller_html_view($content);
	}
	//Manage order
	public function manage_order()
	{

		$order_no 		= $this->input->get('order_no');
		$pre_order_no 	= $this->input->get('pre_order_no');
		$customer 		= $this->input->get('customer');
		$date 			= $this->input->get('date');
		$order_status 	= $this->input->get('order_status');

		#
        #pagination starts
        #
        $config["base_url"] 	    = base_url('seller/manage_order/');
        $config['reuse_query_string']= true;
        $config["total_rows"] 	    = $this->Orders->order_count($order_no,$pre_order_no,$customer,$date,$order_status);
        $config["per_page"] 	    = 10;
        $config["uri_segment"] 	    = 3;
        $config["num_links"] 	    = 5; 
	    /* This Application Must Be Used With BootStrap 3 * */
        $config['full_tag_open'] 	= "<ul class='pagination'>";
        $config['full_tag_close'] 	= "</ul>";
        $config['num_tag_open'] 	= '<li>';
        $config['num_tag_close'] 	= '</li>';
        $config['cur_tag_open'] 	= "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] 	= "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] 	= "<li>";
        $config['next_tag_close'] 	= "</li>";
        $config['prev_tag_open'] 	= "<li>";
        $config['prev_tagl_close'] 	= "</li>";
        $config['first_tag_open'] 	= "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] 	= "<li>";
        $config['last_tagl_close'] 	= "</li>";
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $links = $this->pagination->create_links();
        #
        #pagination ends
        #

        $content = $this->lorder->order_list($links,$config["per_page"],$page,$order_no,$pre_order_no,$customer,$date,$order_status);
		$this->template->full_seller_html_view($content);
	}
	//Retrive right now inserted data to create html
	public function order_details_data($order_id)
	{
		$content = $this->lorder->order_details_data($order_id);	
		$this->template->full_seller_html_view($content);
	}
}