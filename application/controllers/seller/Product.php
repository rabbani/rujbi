<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Product extends CI_Controller {
	
	function __construct() {
      	parent::__construct();
      	$this->load->library('seller/lproduct');
      	$this->load->model('Sellers');
      	$this->load->model('seller/Products');
      	$this->seller_auth->check_seller_auth();
    }
    //Product default index load
	public function index()
	{
		$content = $this->lproduct->product_add_form();
		$this->template->full_seller_html_view($content);
	}
	//Upload product view
	public function upload_product(){
		$content = $this->lproduct->product_add_form();
		$this->template->full_seller_html_view($content);
	}
	//Insert upload product
	public function insert_upload_product(){

		$variant = 0;
		if ($this->input->post('variant')) {
			$variant = implode(",",$this->input->post('variant'));
		}else{
			$variant = 0;
		}

		$category_id = $this->input->post('category_id');
		$comission = $this->Products->retrive_com_value($category_id);

		$data=array(
			'product_id'	=> $this->generator(8),
			'seller_id' 	=> $this->session->userdata('seller_id'),
			'category_id' 	=> $this->input->post('category_id'),
			'price' 		=> $this->input->post('price'),
			'quantity'  	=> $this->input->post('quantity'),
			'product_model' => $this->input->post('product_model'),
			'variant_id' 	=> (!empty($variant)?$variant:null),
			'unit' 			=> $this->input->post('unit'),
			'brand_id' 		=> $this->input->post('brand'),
			'product_type' 	=> $this->input->post('product_type'),
			'tag' 			=> $this->input->post('tag_value'),
			'on_promotion' 	=> $this->input->post('on_promotion'),
			'details' 		=> $this->input->post('details'),
			'best_sale' 	=> $this->input->post('best_sale'),
			'pre_order' 	=> $this->input->post('pre_order'),
			'pre_order_quantity' => $this->input->post('pre_order_quantity'),
			'comission' 	=> !empty($comission)?$comission->rate:null,
			'status' 		=> 1,
		);

		$this->db->select('*');
		$this->db->from('upload_product');
		$this->db->where('product_model',$data['product_model']);
		$query = $this->db->get();

		if ($query->num_rows() > 0) {

			//Product model exists product update
			$this->db->where('product_model',$data['product_model'])
					->update('product_information',$data);

			//Product model exists product update
			$this->db->where('product_model',$data['product_model'])
					->update('upload_product',$data);
			echo "1";

		}else{
			$this->db->select('*');
			$this->db->from('product_information');
			$this->db->where('product_model',$data['product_model']);
			$query = $this->db->get();

			if ($query->num_rows() > 0) {
				echo "2";
			}else{

				//$this->db->insert('product_information',$data);
				$this->db->insert('upload_product',$data);

				$insert_id = $this->db->insert_id();
				$this->session->set_userdata('upload_id',$insert_id);
				echo "1";
			}
		}
	}
	//Update upload product
	public function update_upload_product(){
		$upload_id = $this->input->post('upload_id');
		$variant = 0;
		if ($this->input->post('variant')) {
			$variant = implode(",",$this->input->post('variant'));
		}else{
			$variant = 0;
		}
		
		$category_id = $this->input->post('category_id');
		$comission = $this->Products->retrive_com_value($category_id);

		$data=array(
			'seller_id' 	=> $this->input->post('seller_id'),
			'category_id' 	=> $this->input->post('category_id'),
			'price' 		=> $this->input->post('price'),
			'quantity'  	=> $this->input->post('quantity'),
			'product_model' => $this->input->post('product_model'),
			'variant_id' 	=> (!empty($variant)?$variant:null),
			'unit' 			=> $this->input->post('unit'),
			'brand_id' 		=> $this->input->post('brand'),
			'product_type' 	=> $this->input->post('product_type'),
			'tag' 			=> $this->input->post('tag_value'),
			'status' 		=> $this->input->post('status'),
			'on_promotion' 	=> $this->input->post('on_promotion'),
			'best_sale' 	=> $this->input->post('best_sale'),
			'details' 		=> $this->input->post('details'),
			'pre_order' 	=> $this->input->post('pre_order'),
			'comission' 	=> !empty($comission)?$comission->rate:null,
			'pre_order_quantity' 	=> $this->input->post('pre_order_quantity'),
		);

		$this->db->select('*');
		$this->db->from('upload_product');
		$this->db->where('product_model',$data['product_model']);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {

			$this->db->select('*');
			$this->db->from('product_information');
			$this->db->where('product_model',$data['product_model']);
			$query = $this->db->get();

			if ($query->num_rows() > 0) {

				$data1 = array(
					'seller_id' 	=> $this->input->post('seller_id'),
					'category_id' 	=> $this->input->post('category_id'),
					'price' 		=> $this->input->post('price'),
					'quantity'  	=> $this->input->post('quantity'),
					'variant_id' 	=> (!empty($variant)?$variant:null),
					'unit' 			=> $this->input->post('unit'),
					'brand_id' 		=> $this->input->post('brand'),
					'product_type' 	=> $this->input->post('product_type'),
					'tag' 			=> $this->input->post('tag_value'),
					'status' 		=> $this->input->post('status'),
					'on_promotion' 	=> $this->input->post('on_promotion'),
					'best_sale' 	=> $this->input->post('best_sale'),
					'details' 		=> $this->input->post('details'),
					'pre_order' 	=> $this->input->post('pre_order'),
					'pre_order_quantity' 	=> $this->input->post('pre_order_quantity'),
					'comission' 	=> (!empty($comission)?$comission->rate:null),
					'status' 		=> 1,
				);

				$this->db->where('upload_id',$upload_id);
				$this->db->update('upload_product',$data1);
				echo "1";
			}else{
				$this->db->where('upload_id',$upload_id);
				$this->db->update('upload_product',$data);
				echo "1";
			}
		}else{
			$data1 = array(
				'seller_id' 	=> $this->input->post('seller_id'),
				'category_id' 	=> $this->input->post('category_id'),
				'price' 		=> $this->input->post('price'),
				'quantity'  	=> $this->input->post('quantity'),
				'product_model' => $this->input->post('product_model'),
				'variant_id' 	=> (!empty($variant)?$variant:null),
				'unit' 			=> $this->input->post('unit'),
				'brand_id' 		=> $this->input->post('brand'),
				'product_type' 	=> $this->input->post('product_type'),
				'tag' 			=> $this->input->post('tag_value'),
				'status' 		=> $this->input->post('status'),
				'on_promotion' 	=> $this->input->post('on_promotion'),
				'best_sale' 	=> $this->input->post('best_sale'),
				'details' 		=> $this->input->post('details'),
				'pre_order' 	=> $this->input->post('pre_order'),
				'pre_order_quantity' 	=> $this->input->post('pre_order_quantity'),
				'comission' 	=> (!empty($comission)?$comission->rate:null),
				'status' 		=> 1,
			);
			$this->db->where('upload_id',$upload_id);
			$this->db->update('upload_product',$data1);
			echo "1";
		}
	}
	//File upload 
	public function fileUpload(){
		$image_type = 0;
		$size 		= 0;

		$upload_id  = $this->session->userdata('upload_id');

		if (empty($upload_id)) {
			echo "3";
		}else{
			//Product image upload
	    	if (!empty($_FILES)) {
			
				$config['upload_path']        = "my-assets/image/product/temp/";
		        $config['allowed_types']      = "gif|jpg|png|jpeg|JPEG|GIF|JPG|PNG";
		        $config['max_size']           = "*";
		        $config['max_width']          = "*";
		        $config['max_height']         = "*";
		        $config['encrypt_name'] 	  = TRUE;

		        $this->load->library('upload', $config);
		        if ( ! $this->upload->do_upload('file'))
		        {
		            echo $this->upload->display_errors();
		        }
		        else
		        {
		        	$image = $this->upload->data();

		        	//Resize image config
					$config['image_library'] 	= 'gd2';
					$config['source_image'] 	= $image['full_path'];
					$config['maintain_ratio'] 	= FALSE;
					$config['width']         	= 400;
					$config['height']       	= 400;
					$config['new_image']       	= 'my-assets/image/product/temp/thumb/'.$image['file_name'];
					$this->load->library('image_lib', $config);
					$resize = $this->image_lib->resize();

					//For image type 1=thumb
					$size = filesize($config['new_image']);
					$size = $size/1048576;

					//Get image extention
					$info 	   = getimagesize($config['new_image']);
					$extension = image_type_to_extension($info[2]);

					
					$upload_id     = $this->session->userdata('upload_id');
					$primary_image = $this->Products->primary_image_exist_check($upload_id);
				
					$data = array(
						'upload_id' 	=> $upload_id,
						'image_name' 	=> $image['file_name'],
						'image_size' 	=> number_format($size,2),
						'image_extension' => $extension,
						'date_of_upload'=> date("Y-m-d h:i:s"),
						'image_type'  	=> 1,
						'image_url'  	=> $config['new_image'],
						'status' 		=> (!empty($primary_image)?$primary_image:0),
					);
					//Insert for image type=1 thumb
					$result=$this->Products->insert_product_image($data);

					if ($result) {
						//Configuration for type 2=large
						$img_url = 'my-assets/image/product/temp/'.$image['file_name'];
						$size = filesize($img_url);
						$size = $size/1048576;

						$data1 = array(
							'upload_id' 	=> $upload_id,
							'image_name' 	=> $image['file_name'],
							'image_size' 	=> number_format($size,2),
							'image_extension' => $extension,
							'date_of_upload'=> date("Y-m-d h:i:s"),
							'image_type'  	=> 2,
							'image_url'  	=> $img_url,
							'status' 		=> 0,
						);
						//Insert product image
						$result=$this->Products->insert_product_image($data1);
					}

					if ($result) {
						echo '1';
					}else{
						echo "2";
					}
		        }
			}
		}
	}
	//Update file upload
	public function updateFileUpload(){

		$image_type = 0;
		$size 		= 0;

		$upload_id  = $this->input->post('upload_id');

		if (empty($upload_id)) {
			echo "3";
		}else{
			//Product image upload
	    	if (!empty($_FILES)) {
			
				$config['upload_path']          = "my-assets/image/product/temp/";
		        $config['allowed_types']        = "gif|jpg|png|jpeg|JPEG|GIF|JPG|PNG";
		        $config['max_size']             = "*";
		        $config['max_width']            = "*";
		        $config['max_height']           = "*";
		        $config['encrypt_name'] 		= TRUE;

		        $this->load->library('upload', $config);
		        if ( ! $this->upload->do_upload('file'))
		        {
		            echo $this->upload->display_errors();
		        }
		        else
		        {
		        	$image = $this->upload->data();

		        	//Resize image config
					$config['image_library'] 	= 'gd2';
					$config['source_image'] 	= $image['full_path'];
					$config['maintain_ratio'] 	= FALSE;
					$config['width']         	= 400;
					$config['height']       	= 400;
					$config['new_image']       	= 'my-assets/image/product/temp/thumb/'.$image['file_name'];
					$this->load->library('image_lib', $config);
					$resize = $this->image_lib->resize();

					//For image type 1=thumb
					$size = filesize($config['new_image']);
					$size = $size/1048576;

					//Get image extention
					$info 	   = getimagesize($config['new_image']);
					$extension = image_type_to_extension($info[2]);

					$primary_image = $this->Products->primary_image_exist_check($upload_id);

					$data = array(
						'upload_id' 	=> $upload_id,
						'image_name' 	=> $image['file_name'],
						'image_size' 	=> number_format($size,2),
						'image_extension' => $extension,
						'date_of_upload'=> date("Y-m-d h:i:s"),
						'image_type'  	=> 1,
						'image_url'  	=> $config['new_image'],
						'status' 		=> (!empty($primary_image)?$primary_image:0),
					);
					//Insert for image type=1 thumb
					$result=$this->Products->insert_product_image($data);

					//Configuration for type 2=large
					$img_url = 'my-assets/image/product/temp/'.$image['file_name'];
					$size = filesize($img_url);
					$size = $size/1048576;

					$data1 = array(
						'upload_id' 	=> $upload_id,
						'image_name' 	=> $image['file_name'],
						'image_size' 	=> number_format($size,2),
						'image_extension' => $extension,
						'date_of_upload'=> date("Y-m-d h:i:s"),
						'image_type'  	=> 2,
						'image_url'  	=> $img_url,
						'status' 		=> 0,
					);
					//Insert product image
					$result=$this->Products->insert_product_image($data1);

					if ($result) {
						echo '1';
					}else{
						echo "2";
					}
		        }
			}
		}
	}
	//Delete file
	public function delete_image(){
		$image_id   = $this->input->post('image_id');
		$image_path = FCPATH.$this->input->post('image_path');
	
		if (file_exists($image_path)) {
			unlink($image_path);
		}
		$delete   = $this->db->where('image_name',$image_id)
							->delete('upload_image');
		if ($delete) {
			echo "1";
		}else{
			echo "2";
		}
	}
	//Make image primary
	public function make_img_primary(){
		$image_id 	= $this->input->post('image_id');
		$upload_id  = $this->input->post('upload_id');
		$image_path = $this->input->post('image_path');

		$result   	= $this->Products->make_img_primary($image_id,$upload_id,$image_path);
		if ($result) {
			echo "1";
		}else{
			echo "2";
		}
	}
	//Upload title
	public function uploadTitle(){
		$result 	  = FALSE;
		$lang_id 	  = $this->input->post('lang_id');
		$upload_title = $this->input->post('upload_title');
		$upload_id    = $this->session->userdata('upload_id');

		if (empty(array_filter($lang_id)) || empty(array_filter($upload_title)) || empty($upload_id)) {
			echo "3";
		}else{
			if ($lang_id) {
				for ($i=0;$i < count($lang_id); $i++) {

					$language_id = $lang_id[$i];
					$title 	 	 = $upload_title[$i];
					
					$lang = $this->db->select('*')
								->from('upload_product_title')
								->where('lang_id',$language_id)
								->where('upload_id',$upload_id)
								->get()
								->num_rows();

					if ($lang > 0) {
						$this->db->set('title',$title);
						$this->db->where('upload_id',$upload_id);
						$this->db->where('lang_id',$language_id);
						$result = $this->db->update('upload_product_title');
					}else{
						$data = array(
							'upload_id' => $upload_id,
							'lang_id' 	=> $language_id,
							'title' 	=> $title,
							'status' 	=> 1,
						);
						$result = $this->Products->insert_product_title($data);
					}
				}
				if ($result) {
					echo "1";
				}else{
					echo "2";
				}
			}
		}
	}
	//Update Title
	public function updateTitle(){
		$result = FALSE;
		$lang_id 	  = $this->input->post('lang_id');
		$upload_title = $this->input->post('upload_title');
		$upload_id    = $this->input->post('upload_id');

		if (empty($lang_id) || empty($upload_title) || empty($upload_id)) {
			echo "3";
		}else{
			for ($i=0;$i < count($lang_id); $i++) {

				$language_id = $lang_id[$i];
				$title 	 	 = $upload_title[$i];

				$lang = $this->db->select('*')
							->from('upload_product_title')
							->where('lang_id',$language_id)
							->where('upload_id',$upload_id)
							->get()
							->num_rows();

				if ($lang > 0) {
					$this->db->set('title',$title);
					$this->db->where('upload_id',$upload_id);
					$this->db->where('lang_id',$language_id);
					$result = $this->db->update('upload_product_title');
				}else{
					$data = array(
						'upload_id' => $upload_id,
						'lang_id' 	=> $language_id,
						'title' 	=> $title,
						'status' 	=> 0,
					);
					$result = $this->Sellers->insert_product_title($data);
				}
				
			}
			if ($result) {
				echo "1";
			}else{
				echo "2";
			}
		}
	}
	//Upload description
	public function uploadDescription(){
		$result 	  = FALSE;
		$lang_id 	  = $this->input->post('lang_id');
		$description  = $this->input->post('description');
		$upload_id    = $this->session->userdata('upload_id');

		if (empty(array_filter($lang_id)) || empty(array_filter($description)) || empty($upload_id)) {
			echo "3";
		}else{
			if ($upload_id) {
				for ($i=0;$i < count($lang_id); $i++) {
					$language_id = $lang_id[$i];
					$desc 	 	 = $description[$i];
					$type 	 	 = 1;
				
					$des_exis = $this->db->select('*')
								->from('upload_product_description')
								->where('lang_id',$language_id)
								->where('upload_id',$upload_id)
								->where('description_type',$type)
								->get()
								->num_rows();

					if ($des_exis > 0) {
						$this->db->set('description',$desc);
						$this->db->where('upload_id',$upload_id);
						$this->db->where('lang_id',$language_id);
						$this->db->where('description_type',$type);
						$result = $this->db->update('upload_product_description');
					}else{
						$data = array(
							'upload_id' 	=> $upload_id,
							'lang_id' 		=> $language_id,
							'description' 	=> $desc,
							'description_type' => $type,
							'status' 		=> 0,
						);
						$result = $this->Products->upload_product_description($data);
					}
				}
				
				if ($result) {		
					echo "1";
				}else{
					echo "2";
				}
			}
		}
	}
	//Update description
	public function updateDescription(){
		$result = FALSE;

		$lang_id 	  = $this->input->post('lang_id');
		$description  = $this->input->post('description');
		$upload_id    = $this->input->post('upload_id');

		if (empty(array_filter($lang_id)) || empty(array_filter($description)) || empty($upload_id)) {
			echo "3";
		}else{

			for ($i=0;$i < count($lang_id); $i++) {

				$language_id = $lang_id[$i];
				$desc 	 	 = $description[$i];
				$type 	 	 = 1;

				$des_exis = $this->db->select('*')
							->from('upload_product_description')
							->where('lang_id',$language_id)
							->where('upload_id',$upload_id)
							->where('description_type',$type)
							->get()
							->num_rows();

				if ($des_exis > 0) {
					$this->db->set('description',$desc);
					$this->db->where('upload_id',$upload_id);
					$this->db->where('lang_id',$language_id);
					$this->db->where('description_type',$type);
					$result = $this->db->update('upload_product_description');
				}else{
					$data = array(
						'upload_id' 	=> $upload_id,
						'lang_id' 		=> $language_id,
						'description' 	=> $desc,
						'description_type' => $type,
						'status' 		=> 0,
					);
					$result = $this->Sellers->insert_product_description($data);
				}
				
			}
			
			if ($result) {		
				echo "1";
			}else{
				echo "2";
			}
		}
	}
	//Upload Specification
	public function uploadSpecification(){
		$result 	  = FALSE;
		$lang_id 	  = $this->input->post('lang_id');
		$description  = $this->input->post('description');
		$upload_id    = $this->session->userdata('upload_id');

		$title = $this->db->select('*')
				->from('upload_product_title')
				->where('upload_id',$upload_id)
				->get()
				->num_rows();

		$des = $this->db->select('*')
				->from('upload_product_description')
				->where('upload_id',$upload_id)
				->where('description_type',1)
				->get()
				->num_rows();

		if (empty(array_filter($lang_id)) || empty(array_filter($description)) || empty($upload_id) || ($title == 0) || ($des == 0)) {
			echo "3";
		}else{
			if ($upload_id) {
				for ($i=0;$i < count($lang_id); $i++) {
					$language_id = $lang_id[$i];
					$desc 	 	 = $description[$i];
					$type 	 	 = 2;

					$des_exis = $this->db->select('*')
								->from('upload_product_description')
								->where('lang_id',$language_id)
								->where('upload_id',$upload_id)
								->where('description_type',$type)
								->get()
								->num_rows();

					if ($des_exis > 0) {
						$this->db->set('description',$desc);
						$this->db->where('upload_id',$upload_id);
						$this->db->where('lang_id',$language_id);
						$this->db->where('description_type',$type);
						$result = $this->db->update('upload_product_description');
					}else{
						$data = array(
							'upload_id' 	=> $upload_id,
							'lang_id' 		=> $language_id,
							'description' 	=> $desc,
							'description_type' => $type,
							'status' 		=> 0,
						);
						$result = $this->Products->upload_product_description($data);
					}
				}
				if ($result) {
					$this->session->unset_userdata('upload_id');	
					echo "1";
				}else{
					echo "2";
				}
			}
		}
	}
	//Update specification
	public function updateSpecification(){
		$result = FALSE;

		$lang_id 	  = $this->input->post('lang_id');
		$description  = $this->input->post('description');
		$upload_id    = $this->input->post('upload_id');

		if (empty($lang_id) || empty($description) || empty($upload_id)) {
			echo "3";
		}else{

			for ($i=0;$i < count($lang_id); $i++) {

				$language_id = $lang_id[$i];
				$desc 	 	 = $description[$i];
				$type 	 	 = 2;

				$des_exis = $this->db->select('*')
							->from('upload_product_description')
							->where('lang_id',$language_id)
							->where('upload_id',$upload_id)
							->where('description_type',$type)
							->get()
							->num_rows();

				if ($des_exis > 0) {
					$this->db->set('description',$desc);
					$this->db->where('upload_id',$upload_id);
					$this->db->where('lang_id',$language_id);
					$this->db->where('description_type',$type);
					$result = $this->db->update('upload_product_description');
				}else{
					$data = array(
						'upload_id' 	=> $upload_id,
						'lang_id' 		=> $language_id,
						'description' 	=> $desc,
						'description_type' => $type,
						'status' 		=> 0,
					);
					$result = $this->Sellers->insert_product_description($data);
				}
			}
			
			if ($result) {		
				echo "1";
			}else{
				echo "2";
			}
		}
	}
	//Manage product
	public function manage_product()
	{
		$model 	= $this->input->get('model');
		$title 	= $this->input->get('title');
		$category = $this->input->get('category');

		#
        #pagination starts
        #
        $config["base_url"] 	    = base_url('seller/manage_product/all/item/');
        $config['reuse_query_string']= true;
        $config["total_rows"] 	    = $this->Products->product_count($model,$title,$category);
        $config["per_page"] 	    = 30;
        $config["uri_segment"] 	    = 5;
        $config["num_links"] 	    = 5; 
	    /* This Application Must Be Used With BootStrap 3 * */
        $config['full_tag_open'] 	= "<ul class='pagination'>";
        $config['full_tag_close'] 	= "</ul>";
        $config['num_tag_open'] 	= '<li>';
        $config['num_tag_close'] 	= '</li>';
        $config['cur_tag_open'] 	= "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] 	= "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] 	= "<li>";
        $config['next_tag_close'] 	= "</li>";
        $config['prev_tag_open'] 	= "<li>";
        $config['prev_tagl_close'] 	= "</li>";
        $config['first_tag_open'] 	= "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] 	= "<li>";
        $config['last_tagl_close'] 	= "</li>";
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $links = $this->pagination->create_links();
        #
        #pagination ends
        #

		$content = $this->lproduct->manage_product($links,$config["per_page"],$page,$model,$title,$category);
		$this->template->full_seller_html_view($content);
	}
	//Seller Product Update
	public function seller_product_update_form($upload_id){
		$content = $this->lproduct->seller_product_update_form($upload_id);
		$this->template->full_seller_html_view($content);
	}
	//This function is used to Generate Product Key
	public function generator($lenth)
	{
		$this->load->model('Products');

		$number=array("1","2","3","4","5","6","7","8","9","0");
		for($i=0; $i<$lenth; $i++)
		{
			$rand_value=rand(0,8);
			$rand_number=$number["$rand_value"];
		
			if(empty($con))
			{ 
				$con =$rand_number;
			}
			else
			{
				$con = "$con"."$rand_number";
			}
		}
		return $con;
	}
}