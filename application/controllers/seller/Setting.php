<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Setting extends CI_Controller {
	
	function __construct() {
      	parent::__construct();
      	$this->load->library('seller/lsetting');
      	$this->load->model('seller/Settings');
      	$this->seller_auth->check_seller_auth();
    }

    //Product default index load
	public function index()
	{
		$content = $this->lsetting->setting_add_form();
		$this->template->full_seller_html_view($content);
	}
	//Update seller setting
	public function update_seller_setting()
	{
		$data=array(
			'seller_guarantee' 	=> $this->input->post('seller_guarantee'),
			);

		$this->Settings->update_seller_setting($data);

		$this->session->set_userdata(array('message'=>display('successfully_updated')));
		redirect(base_url('seller_setting'));
	}

	//This function is used to Generate Product Key
	public function generator($lenth)
	{
		$number=array("1","2","3","4","5","6","7","8","9","0");
		for($i=0; $i<$lenth; $i++)
		{
			$rand_value=rand(0,8);
			$rand_number=$number["$rand_value"];
		
			if(empty($con))
			{ 
				$con =$rand_number;
			}
			else
			{
				$con = "$con"."$rand_number";
			}
		}
		return $con;
	}
}