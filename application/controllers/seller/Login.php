<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Controller {

	function __construct() {
      	parent::__construct();
		$this->load->library('seller/Llogin');
		$this->load->library('seller/Seller_auth');
		$this->load->model('seller/Logins');
		$this->load->model('Subscribers');
		$this->load->model('Companies');
    }

	//Default loading for Home Index.
	public function index()
	{
		if ($this->seller_auth->is_logged() )
		{
			$this->output->set_header("Location: ".base_url('seller-dashboard'), TRUE, 302);
		}
		$content = $this->llogin->login_page();
		$this->template->full_website_html_view($content);
	}

	#==============Do Login=======#
	public function do_login()
	{
		$error 		= '';
		$email 		= $this->input->post('email');
		$password 	= $this->input->post('password');
		$remember_me= $this->input->post('remember_me');

		if ($remember_me == 1) {
			$email_cookie = array(
			        'name'   =>  'seller_email',
			        'value'  =>  $email,
			        'expire' =>  '86500',
				);
			$this->input->set_cookie($email_cookie);

			$pass_cookie = array(
			        'name'   => 'seller_password',
			        'value'  => $password,
			        'expire' => '86500',
				);
			$this->input->set_cookie($pass_cookie);
		}


		if ( $email == '' || $password == '' || $this->seller_auth->login($email, $password) === FALSE ){
			$error = display('wrong_username_or_password');
		}

		if ( $error != '' ){
			$this->session->set_userdata(array('error_message'=>$error));
			$this->output->set_header("Location: ".base_url('seller-login'), TRUE, 302);
		}else{

			$com_info = $this->Companies->retrieve_company_editdata('NOILG8EGCRXXBWUEUQBM');
			$this->session->set_userdata(array('message'=>display('welcome_to')." ".$com_info[0]['company_name']));
			$this->output->set_header("Location: ".base_url('seller-dashboard'), TRUE, 302);
        }
	}

	#==============Seller Logout =============#
	public function logout()
	{
		$CI =& get_instance();
		$seller_data = array(
				'seller_sid_web',
				'seller_id',
				'seller_name',
				'seller_email',
				'business_name',
				'seller_image',
				'seller_first_name',
				'seller_last_name',
			);
        $CI->session->unset_userdata($seller_data);
        $this->session->set_userdata(array('message'=>display('you_have_successfully_logout')));
		$this->output->set_header("Location: ".base_url(''), TRUE, 302);
	}

}