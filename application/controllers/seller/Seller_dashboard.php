<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Seller_dashboard extends CI_Controller {
	
	function __construct() {
      	parent::__construct();
	  	$this->load->model('Soft_settings');
	  	$this->load->model('seller/Seller_dashboards');
	  	$this->seller_auth->check_seller_auth();
    }

    //Default seller index load.
    public function index(){

		if (!$this->seller_auth->is_logged() )
		{
			$this->output->set_header("Location: ".base_url('login'), TRUE, 302);
		}
		$seller_id = $this->session->userdata('seller_id');
		$total_balance    = $this->Seller_dashboards->total_balance($seller_id);
		$total_comission  = $this->Seller_dashboards->total_comission($seller_id);
		$total_paid_amount= $this->Seller_dashboards->total_paid_amount($seller_id);
		$total_minimum_balance = $total_balance - $total_comission;
		$total_unpaid_balance = $total_minimum_balance - $total_paid_amount;
	   
		$currency_details = $this->Soft_settings->retrieve_currency_info();
	    $data = array(
	    	'title' 		=> display('dashboard'),
	    	'total_comission' => $total_comission,
	    	'total_balance' => $total_balance,
	    	'total_minimum_balance' => $total_minimum_balance,
	    	'total_paid_amount' => $total_paid_amount,
	    	'total_unpaid_balance' => $total_unpaid_balance,
	    	'currency' 		=> $currency_details[0]['currency_icon'],
			'position' 		=> $currency_details[0]['currency_position'],
	    	);

		$content = $this->parser->parse('seller_dashboard/include/seller_home',$data,true);
		$this->template->full_seller_html_view($content);
    }

    //Comission report
    public function report(){

    	$order_no 	 = $this->input->get('order_no');
		$date 	 	 = $this->input->get('date');

		#
        #pagination starts
        #
        $config["base_url"] 	    = base_url('seller/report/');
        $config['reuse_query_string']= true;
        $config["total_rows"] 	    = $this->Seller_dashboards->comission_report_count($order_no,$date);
        $config["per_page"] 	    = 10;
        $config["uri_segment"] 	    = 3;
        $config["num_links"] 	    = 5; 
	    /* This Application Must Be Used With BootStrap 3 * */
        $config['full_tag_open'] 	= "<ul class='pagination'>";
        $config['full_tag_close'] 	= "</ul>";
        $config['num_tag_open'] 	= '<li>';
        $config['num_tag_close'] 	= '</li>';
        $config['cur_tag_open'] 	= "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] 	= "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] 	= "<li>";
        $config['next_tag_close'] 	= "</li>";
        $config['prev_tag_open'] 	= "<li>";
        $config['prev_tagl_close'] 	= "</li>";
        $config['first_tag_open'] 	= "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] 	= "<li>";
        $config['last_tagl_close'] 	= "</li>";
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $links = $this->pagination->create_links();
        #
        #pagination ends
        #

    	$merchant_sell_report = $this->Seller_dashboards->comission_report($page,$config["per_page"],$order_no,$date);
    	$currency_details 	  = $this->Soft_settings->retrieve_currency_info();

		$sub_total_price = 0;
		$sub_total_comission = 0;
		$sub_total_payable = 0;
		if(!empty($merchant_sell_report)){
			$i=0;
			foreach($merchant_sell_report as $k=>$v){
				$merchant_sell_report[$k]['payable_amount'] = ($merchant_sell_report[$k]['total_price']-$merchant_sell_report[$k]['total_comission']);

				$sub_total_price += $merchant_sell_report[$k]['total_price'];
				$sub_total_comission += $merchant_sell_report[$k]['total_comission'];
				$sub_total_payable += $merchant_sell_report[$k]['payable_amount'];
			}
		}


	    $data = array(
	    	'title' 		=> display('comission_report'),
			'merchant_sell_report' 	=> $merchant_sell_report,
			'sub_total_price' 		=> $sub_total_price,
			'sub_total_comission' 	=> $sub_total_comission,
			'sub_total_payable' 	=> $sub_total_payable,
			'links' 		=> $links,
	    	'currency' 		=> $currency_details[0]['currency_icon'],
			'position' 		=> $currency_details[0]['currency_position'],
	    	);

    	$content = $this->parser->parse('seller_dashboard/comission/comission_report',$data,true);
		$this->template->full_seller_html_view($content);
    }

    #==========Seller logout==========#
	public function logout()
	{	
		if ($this->seller_auth->logout())
		$this->output->set_header("Location: ".base_url(), TRUE, 302);
	}

	//Update user profile from
	public function edit_profile()
	{

		$this->load->model('sellers');
		$edit_data 		= $this->Seller_dashboards->profile_edit_data();
		
		$data = array(
			'title' 	 		=> display('update_profile'),
			'id' 		 		=> $edit_data->id,
			'first_name' 		=> $edit_data->first_name,
			'last_name'  		=> $edit_data->last_name,
			'email'  	 		=> $edit_data->email,
			'password' 	 		=> $edit_data->password,
			'image' 	 		=> $edit_data->image,
			'seller_store_name' => $edit_data->seller_store_name,
			'business_name' 	=> $edit_data->business_name,
			'address' 	 		=> $edit_data->address,
			'identification_doc_no'=> $edit_data->identification_doc_no,
			'identification_type' => $edit_data->identification_type,
			'affiliate_id' 	 	=> $edit_data->affiliate_id,
			'verfication_status'=> $edit_data->verfication_status,
		);	

		$content = $this->parser->parse('seller_dashboard/edit_profile',$data,true);
		$this->template->full_seller_html_view($content);
	}

	#=============Update Profile========#
	public function update_profile()
	{
		$this->Seller_dashboards->profile_update();
		$this->session->set_userdata(array('message'=> display('successfully_updated')));
		redirect(base_url('seller/seller_dashboard/edit_profile'));
	}

	#=============Change Password Form=========# 
	public function change_password_form()
	{	
		$content = $this->parser->parse('seller_dashboard/change_password',array('title'=>display('change_password')),true);
		$this->template->full_seller_html_view($content);
	}

	#============Change Password===========#
	public function change_password()
	{
		$error = '';
		$email 			= $this->input->post('email');
		$old_password 	= $this->input->post('old_password');
		$new_password 	= $this->input->post('password');
		$repassword 	= $this->input->post('repassword');

		$edit_data 		= $this->Seller_dashboards->profile_edit_data();
		$old_email 	  	= $edit_data->email;

		if ( $email == '' || $old_password == '' || $new_password == ''){
			$error = display('blank_field_does_not_accept');
		}else if($email != $old_email){
			$error = display('you_put_wrong_email_address');
		}else if(strlen($new_password)<6 ){
			$error = display('new_password_at_least_six_character');
		}else if($new_password != $repassword ){
			$error = display('password_and_repassword_does_not_match');
		}else if($this->Seller_dashboards->change_password($email,$old_password,$new_password) === FALSE ){
			$error = display('you_are_not_authorised_person');
		}

		if ( $error != '' )
		{
			$this->session->set_userdata(array('error_message'=>$error));
			$this->output->set_header("Location: ".base_url().'seller/seller_dashboard/change_password_form', TRUE, 302);
		}else{
			$logout = $this->seller_auth->logout();
			if ($logout) {
				$this->session->set_userdata(array('message'=>display('successfully_changed_password')));
				$this->output->set_header("Location: ".base_url('seller-login').'', TRUE, 302);
			}
        }
	}
}