<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Signup extends CI_Controller {

	function __construct() {
      	parent::__construct();
		$this->load->library('seller/Lsignup');
		$this->load->model('seller/Signups');
		$this->load->model('Subscribers');
		$this->load->model('Soft_settings');
		$this->load->model('Companies');
		$this->load->model('Email_templates');
    }

	//Default loading for Home Index.
	public function index()
	{
		$content = $this->lsignup->signup_page();
		$this->template->full_website_html_view($content);
	}

	//Submit a seller signup.
	public function seller_signup()
	{
	    $this->form_validation->set_rules('first_name', display('first_name'), 'required|trim');
        $this->form_validation->set_rules('last_name', display('last_name'), 'required|trim');
        $this->form_validation->set_rules('business_name', display('business_name'), 'required');
        $this->form_validation->set_rules('mobile', display('mobile'), 'required|trim');
        $this->form_validation->set_rules('email', display('email'), 'required|trim');
        $this->form_validation->set_rules('address', display('address'), 'required|trim');
        $this->form_validation->set_rules('password', display('password'), 'required|trim|min_length[6]');

        if ($this->form_validation->run() == FALSE)
        {
            $this->session->set_userdata(array('error_message'=> validation_errors()));
			redirect(base_url('seller-signup'));
        }
        else
        {
			$data=array(
				'seller_id' 	=> $this->seller_auth->generator(10),
				'first_name' 	=> $this->input->post('first_name'),
				'last_name' 	=> $this->input->post('last_name'),
				'business_name' => $this->input->post('business_name'),
				'mobile' 		=> $this->input->post('mobile'),
				'email'			=> $this->input->post('email'),
				'address'		=> $this->input->post('address'),
				'image' 		=> base_url('assets/dist/img/user.png'),
				'password' 		=> md5("gef".$this->input->post('password')),
				'status' 		=> 2,
			);

			$result = $this->Signups->seller_signup($data);

			if ($result) {

				//send email with as a link
	            $setting_detail = $this->Soft_settings->retrieve_email_editdata();
	            $company_info   = $this->Companies->company_list();
	            $template_details 		= $this->Email_templates->retrieve_template('9');

	            $config = array(
	                'protocol'      => $setting_detail[0]['protocol'],
	                'smtp_host'     => $setting_detail[0]['smtp_host'],
	                'smtp_port'     => $setting_detail[0]['smtp_port'],
	                'smtp_user'     => $setting_detail[0]['sender_email'], 
	                'smtp_pass'     => $setting_detail[0]['password'], 
	                'mailtype'      => $setting_detail[0]['mailtype'], 
	                'charset'       => 'utf-8'
	            );
	            $this->email->initialize($config);
	            $this->email->set_mailtype($setting_detail[0]['mailtype']);
	            $this->email->set_newline("\r\n");

	            //Email Message Set
	            $message  = !empty($template_details->message)?$template_details->message:null;
	            $message .= "<p>".display('your_information')."

	            			<table style=\"border:1px solid\">
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('first_name').":</th>
									<td style=\"border: 1px solid;\">".$data['first_name']."</td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('last_name').":</th>
									<td style=\"border: 1px solid;\">".$data['last_name']."</td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('email').":</th>
									<td style=\"border: 1px solid;\">".$data['email']."</td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('business_name').":</th>
									<td style=\"border: 1px solid;\">".$data['business_name']."</td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('address').":</th>
									<td style=\"border: 1px solid;\">".$data['address']."</td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('password').":</th>
									<td style=\"border: 1px solid;\">".$this->input->post('password')."</td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('phone').":</th>
									<td style=\"border: 1px solid;\">".$data['mobile']."</td>
								</tr>
							</table>";
	            
	            //Email content
	            $this->email->to($data['email']);
	            $this->email->from($setting_detail[0]['sender_email'],$company_info[0]['company_name']);
	            $this->email->subject(!empty($template_details->subject)?$template_details->subject:null);
	            $this->email->message($message);

			    $email = $this->test_email($data['email']);
				if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
				    if($this->email->send())
				    {
				    	$this->session->set_userdata(array(
							'message' 		=> display('you_have_successfully_signup'),
							'seller_email'=> $this->input->post('email'),
						));
				      	redirect(base_url('seller-signup'));
				    }else{
				    	$this->session->set_userdata(array(
							'error_message' => display('email_was_not_sent_please_contact_administrator'),
							'seller_email'=> $this->input->post('email'),
						));
				     	redirect(base_url('seller-signup'));
				    }
				}else{
					$this->session->set_userdata(array(
						'message' => display('your_email_was_not_found'),
						'seller_email'=> $this->input->post('email'),
					));
			     	redirect(base_url('seller-signup'));
				}
			}else{
				$this->session->set_userdata(array('error_message'=>display('email_already_exists')));
				redirect(base_url('seller-signup'));
			}
		}
	}
	//Email testing for email
	public function test_email($data) {
	  	$data = trim($data);
	  	$data = stripslashes($data);
	  	$data = htmlspecialchars($data);
	  	return $data;
	}
}