<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Corder_tracking extends CI_Controller {
	
	function __construct() {
      	parent::__construct();
      	$this->load->library('Lorder');
      	$this->load->model('Orders');
      	$this->permission->module('order_tracking')->redirect();
		date_default_timezone_set('Asia/Dhaka');
    }
    //Index page loading
	public function index()
	{
		$this->permission->check_label('order_tracking')->access();
		$order_no 		= $this->input->get('order_no');
		$pre_order_no 	= $this->input->get('pre_order_no');
		$customer 		= $this->input->get('customer');
		$date 			= $this->input->get('date');
		$order_status 	= $this->input->get('order_status');

		#
        #pagination starts
        #
        $config["base_url"] 	    = base_url('corder_tracking/index');
        $config['reuse_query_string']= true;
        $config["total_rows"] 	    = $this->Orders->order_count($order_no,$pre_order_no,$customer,$date,$order_status);
        $config["per_page"] 	    = 50;
        $config["uri_segment"] 	    = 3;
        $config["num_links"] 	    = 5; 
	    /* This Application Must Be Used With BootStrap 3 * */
        $config['full_tag_open'] 	= "<ul class='pagination'>";
        $config['full_tag_close'] 	= "</ul>";
        $config['num_tag_open'] 	= '<li>';
        $config['num_tag_close'] 	= '</li>';
        $config['cur_tag_open'] 	= "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] 	= "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] 	= "<li>";
        $config['next_tag_close'] 	= "</li>";
        $config['prev_tag_open'] 	= "<li>";
        $config['prev_tagl_close'] 	= "</li>";
        $config['first_tag_open'] 	= "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] 	= "<li>";
        $config['last_tagl_close'] 	= "</li>";
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $links = $this->pagination->create_links();
        #
        #pagination ends
        #

        $content = $this->lorder->order_tracking($links,$config["per_page"],$page,$order_no,$pre_order_no,$customer,$date,$order_status);
		$this->template->full_admin_html_view($content);
	}
	//Order traking
	public function order_traking($order_id=null){
		//$this->permission->module('order_tracking')->read()->redirect();
		$content = $this->lorder->order_traking($order_id);
		$this->template->full_admin_html_view($content);
	}
	//Submit message for order tracking
	public function submit_message($order_id=null){
		$customer_email = $this->input->post('customer_email');
		$message 		= $this->input->post('message');

		//Insert data into order tracking
		$order_tracking=array(
			'order_id'	=>	$order_id,
			'user_id'	=>	$this->session->userdata('user_id'),
			'order_status'	=>	7,
			'date'		=>	date("Y-m-d h:i a"),
			'message'   => $message, 
		);

		$result = $this->db->insert('order_tracking',$order_tracking);

		if ($result) {
			$this->session->set_userdata(array('message'=> display('message_send_to_customer')));
	        redirect('corder_tracking/order_traking/'.$order_id);
		}
	}
}