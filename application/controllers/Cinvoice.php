<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class cinvoice extends CI_Controller {
	
	function __construct() {
      	parent::__construct();
      	$this->load->library('linvoice');
      	$this->load->model('Invoices');
      	$this->load->model('Soft_settings');
      	$this->permission->module('invoice')->redirect();
    }
    //Default invoice add from loading
	public function index()
	{
		$this->permission->check_label('new_invoice')->create()->redirect();
		$content = $this->linvoice->invoice_add_form();
		$this->template->full_admin_html_view($content);
	}
	//Add new invoice
	public function new_invoice()
	{
		$this->permission->check_label('new_invoice')->create()->redirect();
		$content = $this->linvoice->invoice_add_form();
		$this->template->full_admin_html_view($content);
	}
	//Manage invoice
	public function manage_invoice()
	{
		$this->permission->check_label('manage_invoice')->redirect();
		$invoice_no = $this->input->get('invoice_no');
		$customer 	= $this->input->get('customer');
		$customer_name 	= $this->input->get('customer_name');
		$date 		= $this->input->get('date');
		$order_no 	= $this->input->get('order_no');
		$shipping 	= $this->input->get('shipping');

		#
        #pagination starts
        #
        $config["base_url"] 	    = base_url('cinvoice/manage_invoice/all/list');
        $config['reuse_query_string']= true;
        $config["total_rows"] 	    = $this->Invoices->invoice_count($invoice_no,$customer,$date,$order_no);
        $config["per_page"] 	    = 100;
        $config["uri_segment"] 	    = 5;
        $config["num_links"] 	    = 5; 
	    /* This Application Must Be Used With BootStrap 3 * */
        $config['full_tag_open'] 	= "<ul class='pagination'>";
        $config['full_tag_close'] 	= "</ul>";
        $config['num_tag_open'] 	= '<li>';
        $config['num_tag_close'] 	= '</li>';
        $config['cur_tag_open'] 	= "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] 	= "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] 	= "<li>";
        $config['next_tag_close'] 	= "</li>";
        $config['prev_tag_open'] 	= "<li>";
        $config['prev_tagl_close'] 	= "</li>";
        $config['first_tag_open'] 	= "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] 	= "<li>";
        $config['last_tagl_close'] 	= "</li>";
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $links = $this->pagination->create_links();
        #
        #pagination ends
        #

		$content = $this->linvoice->invoice_list($links,$config["per_page"],$page,$invoice_no,$customer,
            $customer_name,$date,
            $order_no, $shipping);
		$this->template->full_admin_html_view($content);
	}
	//Insert new invoice
	public function insert_invoice()
	{
		$this->permission->check_label('new_invoice')->create()->redirect();
		$invoice_id = $this->Invoices->invoice_entry();
		$this->session->set_userdata(array('message'=>display('successfully_added')));
		$this->invoice_inserted_data($invoice_id);
	}
	//Invoice Update Form
	public function invoice_update_form($invoice_id)
	{
		$this->permission->check_label('manage_invoice')->update()->redirect();
		$content = $this->linvoice->invoice_edit_data($invoice_id);
		$this->template->full_admin_html_view($content);
	}
	// Invoice Update
	public function invoice_update()
	{
		$this->permission->check_label('manage_invoice')->update()->redirect();
		$invoice_id = $this->Invoices->update_invoice();
		$this->session->set_userdata(array('message'=>display('successfully_updated')));
		$this->invoice_inserted_data($invoice_id);
	}
	//Retrive right now inserted data to cretae html
	public function invoice_inserted_data($invoice_id=null)
	{
		$this->permission->check_label('manage_invoice')->read()->redirect();
		$content = $this->linvoice->invoice_html_data($invoice_id);		
		$this->template->full_admin_html_view($content);
	}	
	//Retrive right now inserted data to cretae html
	public function invoice_details_data($order_no=null)
	{
		$this->permission->check_label('manage_invoice')->read()->redirect();
		$content = $this->linvoice->invoice_details_data($order_no);		
		$this->template->full_admin_html_view($content);
	}	
	//Retrive right now pos inserted data to cretae html
	public function pos_invoice_inserted_data($invoice_id)
	{
		$this->permission->check_label('manage_invoice')->read()->redirect();
		$content = $this->linvoice->pos_invoice_html_data($invoice_id);		
		$this->template->full_admin_html_view($content);
	}
	// Retrieve product data
	public function retrieve_product_data()
	{
		$this->permission->check_label('manage_invoice')->read()->redirect();
		$product_id = $this->input->post('product_id');
		$product_info = $this->Invoices->get_total_product($product_id);
		echo json_encode($product_info);
	}
	// Invoice Delete
	public function invoice_delete($invoice_id)
	{
		$this->permission->check_label('manage_invoice')->delete()->redirect();
		$result = $this->Invoices->delete_invoice($invoice_id);
		if ($result) {
			$this->session->set_userdata(array('message'=>display('successfully_delete')));
			redirect('cinvoice/manage_invoice');
		}
	}
	//Search product by product name and category
	public function search_product(){

		$product_name = $this->input->post('product_name');
		$category_id  = $this->input->post('category_id');
		$product_search = $this->Invoices->product_search($product_name,$category_id);
        if ($product_search) {
            foreach ($product_search as $product) {
            echo "<div class=\"col-xs-6 col-sm-4 col-md-2 col-p-3\">";
                echo "<div class=\"panel panel-bd product-panel select_product\">";
                    echo "<div class=\"panel-body\">";
                        echo "<img src=\"$product->image_thumb\" class=\"img-responsive\" alt=\"\">";
                        echo "<input type=\"hidden\" name=\"select_product_id\" class=\"select_product_id\" value='".$product->product_id."'>";
                    echo "</div>";
                    echo "<div class=\"panel-footer\">$product->product_name - ($product->product_model)</div>";
                echo "</div>";
            echo "</div>";
        	}
        }else{
        	echo "420";
        }
	}
}