<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Recover_password extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('website/Lrecover_password');
        $this->load->model('website/Homes');
        $this->load->model('Soft_settings');
        $this->load->model('Companies');
        $this->load->model('Email_templates');
    }
    public function index(){
        $content = $this->lrecover_password->recover();
        $this->template->full_website_html_view($content);
    }
    public function recover(){
        $content = $this->lrecover_password->recover();
        $this->template->full_website_html_view($content);
    }
    public function seller_password(){
        $content = $this->lrecover_password->seller_password();
        $this->template->full_website_html_view($content);
    }
    public function password_reset(){
      
        $email        = $this->input->post('email');
        $seller_email = $this->input->post('seller_email');

        if ($email) {
            //check if email is in the database
            if($this->Homes->customer_email_exists($email)){

                //$them_pass is the varible to be sent to the user's email
                $temp_pass = md5(uniqid());

                //send email with #temp_pass as a link
                $setting_detail  = $this->Soft_settings->retrieve_email_editdata();
                $company_info    = $this->Companies->company_list();
                $template_details= $this->Email_templates->retrieve_template('8');

                $config = array(
                    'protocol'      => $setting_detail[0]['protocol'],
                    'smtp_host'     => $setting_detail[0]['smtp_host'],
                    'smtp_port'     => $setting_detail[0]['smtp_port'],
                    'smtp_user'     => $setting_detail[0]['sender_email'], 
                    'smtp_pass'     => $setting_detail[0]['password'], 
                    'mailtype'      => $setting_detail[0]['mailtype'], 
                    'charset'       => 'utf-8'
                );
                $this->email->initialize($config);
                $this->email->set_mailtype($setting_detail[0]['mailtype']);
                $this->email->set_newline("\r\n");
                
                //Email content
                $message  = !empty($template_details->message)?$template_details->message:null;
                $message .= "<p><a href='".base_url()."recover_password/reset_password/$temp_pass'> ".display('click_here')." </a>".display('if_you_reset_password_if_not_then_ignore')."</p>";
                
                $this->email->to($email);
                $this->email->from($setting_detail[0]['sender_email'],$company_info[0]['company_name']);
                $this->email->subject(!empty($template_details->subject)?$template_details->subject:null);
                $this->email->message($message);

                if($this->email->send()){
                    if($this->Homes->temp_reset_password($temp_pass,$email)){
                        $this->session->set_userdata(array('message'=> display('varifaction_mail_was_sent_please_check_your_email')));
                        redirect('recover_password');
                    }
                }
                else{
                    $this->session->set_userdata(array('error_message'=>display('email_was_not_sent_please_contact_administrator')));
                    redirect('recover_password');
                }
            }else{
                $this->session->set_userdata(array('error_message'=>display('your_email_was_not_found')));
                redirect('recover_password');
            }
        }else{
            //check if email is in the database
            if($this->Homes->seller_email_exists($seller_email)){

                //$them_pass is the varible to be sent to the user's email
                $temp_pass = md5(uniqid());

                //send email with #temp_pass as a link
                $setting_detail  = $this->Soft_settings->retrieve_email_editdata();
                $company_info    = $this->Companies->company_list();
                $template_details= $this->Email_templates->retrieve_template('9');

                $config = array(
                    'protocol'      => $setting_detail[0]['protocol'],
                    'smtp_host'     => $setting_detail[0]['smtp_host'],
                    'smtp_port'     => $setting_detail[0]['smtp_port'],
                    'smtp_user'     => $setting_detail[0]['sender_email'], 
                    'smtp_pass'     => $setting_detail[0]['password'], 
                    'mailtype'      => $setting_detail[0]['mailtype'], 
                    'charset'       => 'utf-8'
                );
                $this->email->initialize($config);
                $this->email->set_mailtype($setting_detail[0]['mailtype']);
                $this->email->set_newline("\r\n");
                
                //Email content
                $message  = !empty($template_details->message)?$template_details->message:null;
                $message .= "<p><a href='".base_url()."recover_password/seller_reset_password/token/$temp_pass'> "
                    .display('click_here')." </a>".display('if_you_reset_password_if_not_then_ignore')."</p>";
                
                $this->email->to($seller_email);
                $this->email->from($setting_detail[0]['sender_email'],$company_info[0]['company_name']);
                $this->email->subject(!empty($template_details->subject)?$template_details->subject:null);
                $this->email->message($message);

                if($this->email->send()){
                    if($this->Homes->temp_seller_reset_password($temp_pass,$seller_email)){
                        $this->session->set_userdata(array('message'=> display('varifaction_mail_was_sent_please_check_your_email')));
                        redirect('recover_password_seller_password');
                    }
                }
                else{
                    $this->session->set_userdata(array('error_message'=>display('email_was_not_sent_please_contact_administrator')));
                    redirect('recover_password_seller_password');
                }
            }else{
                $this->session->set_userdata(array('error_message'=>display('your_email_was_not_found')));
                redirect('recover_password_seller_password');
            }
        }
    }
    //Reset password from email
    public function reset_password($temp_pass=null){

        if ($temp_pass == null) {
            $this->session->set_userdata(array('error_message'=>display('this_key_is_not_valid')));
            redirect('recover_password_seller_password');
        }

        if($this->Homes->is_temp_pass_valid($temp_pass)){
            $content = $this->lrecover_password->reset_password();
            $this->template->full_website_html_view($content);
        }else{
            $this->session->set_userdata(array('error_message'=>display('this_key_is_not_valid')));
            redirect('recover_password');
        }
    }
    //Seller reset password from email
    public function seller_reset_password($temp_pass=null){

        if ($temp_pass == null) {
            $this->session->set_userdata(array('error_message'=>display('this_key_is_not_valid')));
            redirect('recover_password_seller_password');
        }

        if($this->Homes->is_seller_temp_pass_valid($temp_pass)){
            $content = $this->lrecover_password->seller_reset_password();
            $this->template->full_website_html_view($content);
        }else{
            $this->session->set_userdata(array('error_message'=>display('this_key_is_not_valid')));
            redirect('recover_password_seller_password');
        }
    }


    //Update password from page
    public function update_password(){
        $this->form_validation->set_rules('password', display('password'), 'required|trim');
        $this->form_validation->set_rules('cpassword', display('confirm_password'), 'required|trim|matches[password]');
        $this->form_validation->set_rules('_token', 'Token', 'required|trim');
        if($this->form_validation->run()){
            $password   = md5("gef".$this->input->post('password'));
            $_token     = $this->input->post('_token');
            $result=$this->Homes->update_password($password,$_token);
            if($result){
               $this->session->set_userdata(array('message'=> display('password_is_updated')));
                redirect('login'); 
            }
        }else{
            $this->session->set_userdata(array('error_message'=> display('password_not_matched')));
            redirect('recover_password/reset_password/token/'.$_token);
        }
    }

    //Update password from page
    public function seller_update_password(){
        $_token     = $this->input->post('_token');
        $this->form_validation->set_rules('password', display('password'), 'required|trim');
        $this->form_validation->set_rules('cpassword', display('confirm_password'), 'required|trim|matches[password]');
        $this->form_validation->set_rules('_token', 'Token', 'required|trim');
        if($this->form_validation->run()){
            $password   = md5("gef".$this->input->post('password'));
            $_token     = $this->input->post('_token');
            $result=$this->Homes->seller_update_password($password,$_token);
            if($result){
               $this->session->set_userdata(array('message'=> display('password_is_updated')));
                redirect('seller-login');
            }
        }else{
            $this->session->set_userdata(array('error_message'=> display('password_not_matched')));
            redirect('recover_password/seller_reset_password/'.$_token);    
        }
    }
}
?>