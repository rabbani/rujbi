<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cshipping_agent extends CI_Controller {

	function __construct() {
      	parent::__construct();
		$this->load->library('lshipping_agent');
		$this->load->model('Shipping_agents');
    }
	//Default loading for Shipping agent system.
	public function index()
	{
		$this->permission->check_label('add_shipping_agent')->create()->redirect();
		$content = $this->lshipping_agent->shipping_agent_add_form();
		$this->template->full_admin_html_view($content);
	}
	//Insert Shipping agent
	public function insert_shipping_agent()
	{
		$this->permission->check_label('add_shipping_agent')->create()->redirect();
		$this->form_validation->set_rules('agent_name', display('name'), 'trim|required');
		$this->form_validation->set_rules('phone', display('phone'), 'trim|required');
		$this->form_validation->set_rules('email', display('email'), 'trim|required');
		$this->form_validation->set_rules('address', display('address'), 'trim|required');
		$this->form_validation->set_rules('amount', display('amount'), 'trim|required');

		if ($this->form_validation->run() == FALSE)
        {
        	$data = array(
				'title' => display('add_shipping_agent')
			);
        	$content = $this->parser->parse('shipping_agent/add_shipping_agent',$data,true);
			$this->template->full_admin_html_view($content);
        }else{

			$data=array(

				'agent_name' => $this->input->post('agent_name'),
				'agent_phone' 	  => $this->input->post('phone'),
				'agent_email' 	  => $this->input->post('email'),
				'agent_address'	  => $this->input->post('address'),
				'amount'	=> $this->input->post('amount'),
			);


			$result=$this->Shipping_agents->shipping_agent_entry($data);

			if ($result == TRUE) {

				$this->session->set_userdata(array('message'=>display('successfully_added')));

				if(isset($_POST['add-shipping_agent'])){
					redirect(base_url('manage_shipping_agent'));
				}elseif(isset($_POST['add-shipping_agent-another'])){
					redirect(base_url('cshipping_agent'));
				}

			}else{
				$this->session->set_userdata(array('error_message'=>display('already_exists')));
				redirect(base_url('cshipping_agent'));
			}
        }
	}
	//Manage Shipping agent
	public function manage_shipping_agent()
	{
		$this->permission->check_label('manage_shipping_agent')->read()->redirect();
        $content =$this->lshipping_agent->shipping_agent_list();
		$this->template->full_admin_html_view($content);;
	}
	//Shipping agent Update Form
	public function shipping_agent_update_form($agent_id)
	{	
		$this->permission->check_label('manage_shipping_agent')->update()->redirect();
		$content = $this->lshipping_agent->shipping_agent_edit_data($agent_id);
		$this->template->full_admin_html_view($content);
	}
	// Shipping agent Update
	public function shipping_agent_update($agent_id=null)
	{
        $this->permission->check_label('add_shipping_agent')->create()->redirect();
        $this->form_validation->set_rules('agent_name', display('name'), 'trim|required');
        $this->form_validation->set_rules('phone', display('phone'), 'trim|required');
        $this->form_validation->set_rules('email', display('email'), 'trim|required');
        $this->form_validation->set_rules('address', display('address'), 'trim|required');
        $this->form_validation->set_rules('amount', display('amount'), 'trim|required');

		if ($this->form_validation->run() == FALSE)
        {
        	$data = array(
				'title' => display('add_shipping_agent')
			);
        	$content = $this->parser->parse('shipping_agent/add_shipping_agent',$data,true);
			$this->template->full_admin_html_view($content);
        }else{
			$data=array(
				'agent_name' => $this->input->post('agent_name'),
				'agent_phone' 	  => $this->input->post('phone'),
				'agent_email' 	  => $this->input->post('email'),
				'agent_address'	  => $this->input->post('address'),
				'amount'	=> $this->input->post('amount'),
			);

			$result=$this->Shipping_agents->update_shipping_agent($data,$agent_id);

			if ($result == TRUE) {
				$this->session->set_userdata(array('message'=>display('successfully_updated')));
				redirect('manage_shipping_agent');
			}else{
				$this->session->set_userdata(array('error_message'=>display('already_exists')));
				redirect('manage_shipping_agent');
			}
        }
	}
	//Shipping agent Delete
	public function shipping_agent_delete($agent_id=null)
	{
		$this->permission->check_label('manage_shipping_agent')->delete()->redirect();
		$this->Shipping_agents->delete_shipping_agent($agent_id);
		$this->session->set_userdata(array('message'=>display('successfully_delete')));
		redirect('manage_shipping_agent');
	}
	//Inactive
	public function inactive($id=null){
		$this->permission->check_label('manage_shipping_agent')->update()->redirect();
		$this->db->set('status', 0);
		$this->db->where('shipping_agent_id',$id);
		$this->db->update('shipping_agent');
		$this->session->set_userdata(array('error_message'=>display('successfully_inactive')));
		redirect(base_url('manage_shipping_agent'));
	}
	//Active 
	public function active($id=null){
		$this->permission->check_label('manage_shipping_agent')->update()->redirect();
		$this->db->set('status', 1);
		$this->db->where('shipping_agent_id',$id);
		$this->db->update('shipping_agent');
		$this->session->set_userdata(array('message'=>display('successfully_active')));
		redirect(base_url('manage_shipping_agent'));
	}
}