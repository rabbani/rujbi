<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cpayment_method extends CI_Controller {

	function __construct() {
      parent::__construct();
		$this->load->library('lpayment_method');
		$this->load->model('Payment_methods');
    }
	//Default loading for payment method system.
	public function index()
	{
		$content = $this->lpayment_method->payment_method_add_form();
		$this->template->full_admin_html_view($content);
	}
	//Insert payment method
	public function insert_payment_method()
	{
		$this->form_validation->set_rules('position', display('position'), 'trim|required');
		$this->form_validation->set_rules('method_name', display('name'), 'trim|required');
		$this->form_validation->set_rules('details', display('details'), 'trim|required');

		if ($this->form_validation->run() == FALSE)
        {
        	$data = array(
				'title' => display('add_payment_method')
			);
        	$content = $this->parser->parse('payment_method/add_payment_method',$data,true);
			$this->template->full_admin_html_view($content);
        }else{

			$data=array(
				'method_id'	  => $this->auth->generator(15),
				'position' 	  => $this->input->post('position'),
				'method_name' => $this->input->post('method_name'),
				'details'	  => $this->input->post('details'),
			);

			$result=$this->Payment_methods->payment_method_entry($data);

			if ($result == TRUE) {
					
				$this->session->set_userdata(array('message'=>display('successfully_added')));

				if(isset($_POST['add-payment_method'])){
					redirect(base_url('cpayment_method/manage_payment_method'));
				}elseif(isset($_POST['add-payment_method-another'])){
					redirect(base_url('cpayment_method'));
				}

			}else{
				$this->session->set_userdata(array('error_message'=>display('already_exists')));
				redirect(base_url('cpayment_method'));
			}
        }
	}
	//Manage payment method
	public function manage_payment_method()
	{
        $content =$this->lpayment_method->payment_method_list();
		$this->template->full_admin_html_view($content);;
	}
	//payment method Update Form
	public function payment_method_update_form($method_id)
	{	
		$content = $this->lpayment_method->payment_method_edit_data($method_id);
		$this->template->full_admin_html_view($content);
	}
	// payment method Update
	public function payment_method_update($method_id=null)
	{

		$this->form_validation->set_rules('position', display('position'), 'trim|required');
		$this->form_validation->set_rules('method_name', display('name'), 'trim|required');
		$this->form_validation->set_rules('details', display('details'), 'trim|required');
		

		if ($this->form_validation->run() == FALSE)
        {
        	$this->session->set_userdata(array('error_message'=>validation_errors()));
			redirect('cpayment_method/manage_payment_method');
        }else{
			$data=array(
				'position' 	  => $this->input->post('position'),
				'method_name' => $this->input->post('method_name'),
				'details'	  => $this->input->post('details'),
			);

			$result=$this->Payment_methods->update_payment_method($data,$method_id);

			if ($result == TRUE) {
				$this->session->set_userdata(array('message'=>display('successfully_updated')));
				redirect('cpayment_method/manage_payment_method');
			}else{
				$this->session->set_userdata(array('error_message'=>display('already_exists')));
				redirect('cpayment_method/manage_payment_method');
			}
        }
	}
	// payment_method Delete
	public function payment_method_delete($method_id)
	{
		$this->Payment_methods->delete_payment_method($method_id);
		$this->session->set_userdata(array('message'=>display('successfully_delete')));
		redirect('cpayment_method/manage_payment_method');
	}
	//Inactive
	public function inactive($id){
		$this->db->set('status', 0);
		$this->db->where('method_id',$id);
		$this->db->update('payment_method');
		$this->session->set_userdata(array('error_message'=>display('successfully_inactive')));
		redirect(base_url('cpayment_method/manage_payment_method'));
	}
	//Active 
	public function active($id){
		$this->db->set('status', 1);
		$this->db->where('method_id',$id);
		$this->db->update('payment_method');
		$this->session->set_userdata(array('message'=>display('successfully_active')));
		redirect(base_url('cpayment_method/manage_payment_method'));
	}
}