<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Creport extends CI_Controller {
	
	function __construct() {
     	parent::__construct();
		$this->load->library('lreport');
		$this->load->library('occational');
      	$this->load->model('Soft_settings');
      	$this->load->model('Reports');
    }

	public function index()
	{
        $this->permission->check_label('sales_report')->read()->redirect();
        $content = $this->lreport->sales_report();
		$this->template->full_admin_html_view($content);
	}
	public function sales_report()
	{
        $this->permission->check_label('sales_report')->read()->redirect();
        $content = $this->lreport->sales_report();
		$this->template->full_admin_html_view($content);
	}

	public function sales_details_report()
	{
        $this->permission->check_label('sales_details_report')->read()->redirect();
		$order_no = $this->input->get('order_no');
		$customer = $this->input->get('customer');
		$date 	  = $this->input->get('date');
		$shipping_id = $this->input->get('shipping_id');
		$invoice_no  = $this->input->get('invoice_no');
		$status 	 = $this->input->get('status');

		#
        #pagination starts
        #
        $config["base_url"] 	    = base_url('creport/sales_details_report/all/item/');
        $config['reuse_query_string']= true;
        $config["total_rows"] 	    = $this->Reports->sell_report_shipping_count($order_no,$customer,$date,$shipping_id,$invoice_no,$status);
        $config["per_page"] 	    = 30;
        $config["uri_segment"] 	    = 5;
        $config["num_links"] 	    = 5; 
	    /* This Application Must Be Used With BootStrap 3 * */
        $config['full_tag_open'] 	= "<ul class='pagination'>";
        $config['full_tag_close'] 	= "</ul>";
        $config['num_tag_open'] 	= '<li>';
        $config['num_tag_close'] 	= '</li>';
        $config['cur_tag_open'] 	= "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] 	= "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] 	= "<li>";
        $config['next_tag_close'] 	= "</li>";
        $config['prev_tag_open'] 	= "<li>";
        $config['prev_tagl_close'] 	= "</li>";
        $config['first_tag_open'] 	= "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] 	= "<li>";
        $config['last_tagl_close'] 	= "</li>";
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $links = $this->pagination->create_links();
        #
        #pagination ends
        #

        $content = $this->lreport->sales_details_report($page,$config["per_page"],$links,$order_no,$customer,$date,$shipping_id,$invoice_no,$status);
		$this->template->full_admin_html_view($content);
	}


    //Sales details report
    public function order_details_report()
    {
        $this->permission->check_label('order_details_report')->read()->redirect();
        $order_no = $this->input->get('order_no');
        $customer = $this->input->get('customer');
        $date     = $this->input->get('date');
        $shipping_id = $this->input->get('shipping_id');
        $invoice_no  = $this->input->get('invoice_no');
        $status      = $this->input->get('status');

        #
        #pagination starts
        #
        $config["base_url"]         = base_url('creport/order_details_report/all/item');
        $config['reuse_query_string']= true;
        $config["total_rows"]       = $this->Reports->order_details_report_count($order_no,$customer,$date,$shipping_id,$status);
        $config["per_page"]         = 30;
        $config["uri_segment"]      = 5;
        $config["num_links"]        = 5;
        /* This Application Must Be Used With BootStrap 3 * */
        $config['full_tag_open']    = "<ul class='pagination'>";
        $config['full_tag_close']   = "</ul>";
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
        $config['cur_tag_open']     = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close']    = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open']    = "<li>";
        $config['next_tag_close']   = "</li>";
        $config['prev_tag_open']    = "<li>";
        $config['prev_tagl_close']  = "</li>";
        $config['first_tag_open']   = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open']    = "<li>";
        $config['last_tagl_close']  = "</li>";
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $links = $this->pagination->create_links();
        #
        #pagination ends
        #

        $content = $this->lreport->order_details_report($page,$config["per_page"],$links,$order_no,$customer,$date,$shipping_id,$status);
        $this->template->full_admin_html_view($content);
    }

	public function merchant_sell_report()
	{
        $this->permission->check_label('sales_overview_report')->read()->redirect();
		$order_no 	 = $this->input->get('order_no');
		$seller 	 = $this->input->get('seller');
		$date 	 	 = $this->input->get('date');

		#
        #pagination starts
        #
        $config["base_url"] 	    = base_url('creport/merchant_sell_report/all/item');
        $config['reuse_query_string']= true;
        $config["total_rows"] 	    = $this->Reports->merchant_sell_report_count($order_no,$seller,$date);
        $config["per_page"] 	    = 30;
        $config["uri_segment"] 	    = 5;
        $config["num_links"] 	    = 5; 
	    /* This Application Must Be Used With BootStrap 3 * */
        $config['full_tag_open'] 	= "<ul class='pagination'>";
        $config['full_tag_close'] 	= "</ul>";
        $config['num_tag_open'] 	= '<li>';
        $config['num_tag_close'] 	= '</li>';
        $config['cur_tag_open'] 	= "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] 	= "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] 	= "<li>";
        $config['next_tag_close'] 	= "</li>";
        $config['prev_tag_open'] 	= "<li>";
        $config['prev_tagl_close'] 	= "</li>";
        $config['first_tag_open'] 	= "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] 	= "<li>";
        $config['last_tagl_close'] 	= "</li>";
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $links = $this->pagination->create_links();
        #
        #pagination ends
        #

        $content = $this->lreport->merchant_sell_report($page,$config["per_page"],$links,$order_no,$seller,$date);
		$this->template->full_admin_html_view($content);
	}
	//Stock Report
	public function stock()
	{
        $this->permission->check_label('stock_management')->read()->redirect();
        $product_id    = $this->input->get('product_name');
		$product_model = $this->input->get('product_model');
		$seller 	   = $this->input->get('seller');

		#
        #pagination starts
        #
        $config["base_url"] 	    = base_url('creport/stock');
        $config['reuse_query_string']= true;
        $config["total_rows"] 	    = $this->Reports->stock_report_product_wise_count($product_id,$seller,$product_model);
        $config["per_page"] 	    = 50;
        $config["uri_segment"] 	    = 3;
        $config["num_links"] 	    = 5; 
	    /* This Application Must Be Used With BootStrap 3 * */
        $config['full_tag_open'] 	= "<ul class='pagination'>";
        $config['full_tag_close'] 	= "</ul>";
        $config['num_tag_open'] 	= '<li>';
        $config['num_tag_close'] 	= '</li>';
        $config['cur_tag_open'] 	= "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] 	= "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] 	= "<li>";
        $config['next_tag_close'] 	= "</li>";
        $config['prev_tag_open'] 	= "<li>";
        $config['prev_tagl_close'] 	= "</li>";
        $config['first_tag_open'] 	= "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] 	= "<li>";
        $config['last_tagl_close'] 	= "</li>";
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $links = $this->pagination->create_links();
        #
        #pagination ends
        #

		$content = $this->lreport->stock_report_product_wise($page,$config["per_page"],$links,$product_id,$seller,$product_model);
		$this->template->full_admin_html_view($content);
	}
}