<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cseller extends CI_Controller {

	function __construct() {
      	parent::__construct();
		$this->load->library('lseller');
		$this->load->model('Sellers');
		$this->load->model('Companies');
		$this->load->model('Email_templates');
    }
	//Default loading for seller System.
	public function index()
	{
		$this->permission->check_label('add_seller')->create()->redirect();
		$content = $this->lseller->seller_add_form();
		$this->template->full_admin_html_view($content);
	}
	//Insert upload
	public function insert_seller()
	{
		$this->permission->check_label('add_seller')->create()->redirect();
		$this->form_validation->set_rules('first_name', display('first_name'), 'trim|required');
		$this->form_validation->set_rules('last_name', display('last_name'), 'trim|required');
		$this->form_validation->set_rules('email', display('email'), 'trim|required|valid_email');
		$this->form_validation->set_rules('password', display('password'), 'trim|required|min_length[5]');
		$this->form_validation->set_rules('address', display('address'), 'trim');
		$this->form_validation->set_rules('seller_store_name', display('store_name'), 'trim|max_length[20]|min_length[4]');
		$this->form_validation->set_rules('business_name', display('business_name'), 'trim');
		$this->form_validation->set_rules('identification_doc_no', display('identification_doc_no'), 'trim');
		$this->form_validation->set_rules('identification_type', display('identification_type'), 'trim');
		$this->form_validation->set_rules('affiliate_id', display('affiliate_id'), 'trim');

		if ($this->form_validation->run() == FALSE)
        {
        	$data = array(
				'title' => display('add_seller')
			);
			$this->session->set_userdata(array('error_message'=>validation_errors()));
        	redirect(base_url('cseller'));
        }else{

		  	//Seller  basic information adding.
			$data=array(
				'seller_id'			=> $this->generator(8),
				'first_name' 		=> $this->input->post('first_name'),
				'last_name' 		=> $this->input->post('last_name'),
				'email' 			=> $this->input->post('email'),
				'password' 			=> md5("gef".$this->input->post('password')),
				'mobile'  			=> $this->input->post('mobile'),
				'address'  			=> $this->input->post('address'),
				'seller_store_name' => $this->input->post('seller_store_name'),
				'business_name' 	=> $this->input->post('business_name'),
				'identification_doc_no' => $this->input->post('identification_doc_no'),
				'identification_type' => $this->input->post('identification_type'),
				'affiliate_id' 		=> $this->input->post('affiliate_id'),
				'image' 			=> 'assets/dist/img/user.png',
				'status' 			=> '1',
			);

			$identification_type = "";
			if ($data['identification_type'] == 1) {
				$identification_type = display('driving_licence');
			}elseif($data['identification_type'] == 2){
				$identification_type = display('national_id');
			}elseif($data['identification_type'] == 3){
				$identification_type = display('passport_no');
			}else{
				$identification_type = display('others');
			}

			$result = $this->Sellers->seller_entry($data);
			
			if ($result == TRUE) {

				//send email with as a link
	            $setting_detail = $this->Soft_settings->retrieve_email_editdata();
	            $company_info   = $this->Companies->company_list();
	            $template_details = $this->Email_templates->retrieve_template('9');

	            $config = array(
	                'protocol'      => $setting_detail[0]['protocol'],
	                'smtp_host'     => $setting_detail[0]['smtp_host'],
	                'smtp_port'     => $setting_detail[0]['smtp_port'],
	                'smtp_user'     => $setting_detail[0]['sender_email'], 
	                'smtp_pass'     => $setting_detail[0]['password'], 
	                'mailtype'      => $setting_detail[0]['mailtype'], 
	                'charset'       => 'utf-8'
	            );
	            $this->email->initialize($config);
	            $this->email->set_mailtype($setting_detail[0]['mailtype']);
	            $this->email->set_newline("\r\n");

	            //Email Message Set
	            $message = (!empty($template_details->message))?$template_details->message:null;
	            $message .= "<p>".display('your_information')."<table style=\"border:1px solid\">
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('first_name').":</th>
									<td style=\"border: 1px solid;\">".$data['first_name']."</td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('last_name').":</th>
									<td style=\"border: 1px solid;\">".$data['last_name']."</td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('email').":</th>
									<td style=\"border: 1px solid;\">".$data['email']."</td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('password').":</th>
									<td style=\"border: 1px solid;\">".$this->input->post('password')."</td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('mobile').":</th>
									<td style=\"border: 1px solid;\">".$data['mobile']."</td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('address').":</th>
									<td style=\"border: 1px solid;\">".$data['address']."</td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('store_name').":</th>
									<td style=\"border: 1px solid;\">".$data['seller_store_name']."</td>
								</tr>		
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('business_name').":</th>
									<td style=\"border: 1px solid;\">".$data['business_name']."</td>
								</tr>		
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('identification_doc_no').":</th>
									<td style=\"border: 1px solid;\">".$data['identification_doc_no']."</td>
								</tr>		
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('identification_type').":</th>
									<td style=\"border: 1px solid;\">".$identification_type."</td>
								</tr>	
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('affiliate_id').":</th>
									<td style=\"border: 1px solid;\">".$data['affiliate_id']."</td>
								</tr>
								<tr>
									<th style=\"text-align: left;border: 1px solid;\">".display('status').":</th>
									<td style=\"border: 1px solid;\">".display('active')."</td>
								</tr>
							</table>";
	            
	            //Email content
	            $this->email->to($data['email']);
	            $this->email->from($setting_detail[0]['sender_email'],$company_info[0]['company_name']);
	            $this->email->subject(!empty($template_details->subject)?$template_details->subject:null);
	            $this->email->message($message);

			    $email = $this->test_email($data['email']);
				if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
				    if($this->email->send())
				    {
				    	$this->session->set_userdata(array(
							'message' 		=> display('successfully_added'),
						));
				    }else{
				    	$this->session->set_userdata(array(
							'error_message' => display('email_was_not_sent_please_contact_administrator'),
						));
				    }
				}else{
					$this->session->set_userdata(array(
						'error_message' => display('your_email_was_not_found'),
					));
				}

				if(isset($_POST['add-seller'])){
					redirect(base_url('cseller/manage_seller/all/item'));
				}elseif(isset($_POST['add-seller-another'])){
					redirect(base_url('cseller'));
				}
			}else{
				$this->session->set_userdata(array('error_message'=>display('already_exists')));
				redirect(base_url('cseller'));
			}
		}
	}
	//Email testing for email
	public function test_email($data) {
	  	$data = trim($data);
	  	$data = stripslashes($data);
	  	$data = htmlspecialchars($data);
	  	return $data;
	}
	//Check seller store name are exists
	public function check_seller_store_name(){
		$this->permission->check_label('add_seller')->create()->redirect();
		$store_name = $this->input->post('store_name');
		$result =$this->db->select('*')
				->from('seller_information')
				->where('seller_store_name',$store_name)
				->get()
				->num_rows();

		if ($result > 0) {
			echo "1";
		}else{
			echo $store_name;
		}
	}
	//Manage seller
	public function manage_seller()
	{
		$this->permission->check_label('manage_seller')->redirect();

		$mobile 		= $this->input->get('mobile');
		$email 			= $this->input->get('email');
		$business_name 	= $this->input->get('business_name');

		#
        #pagination starts
        #
        $config["base_url"] 	    = base_url('cseller/manage_seller/all/item');
        $config['reuse_query_string']= true;
        $config["total_rows"] 	    = $this->Sellers->seller_count($mobile,$email,$business_name);
        $config["per_page"] 	    = 10;
        $config["uri_segment"] 	    = 5;
        $config["num_links"] 	    = 5; 
	    /* This Application Must Be Used With BootStrap 3 * */
        $config['full_tag_open'] 	= "<ul class='pagination'>";
        $config['full_tag_close'] 	= "</ul>";
        $config['num_tag_open'] 	= '<li>';
        $config['num_tag_close'] 	= '</li>';
        $config['cur_tag_open'] 	= "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] 	= "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] 	= "<li>";
        $config['next_tag_close'] 	= "</li>";
        $config['prev_tag_open'] 	= "<li>";
        $config['prev_tagl_close'] 	= "</li>";
        $config['first_tag_open'] 	= "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] 	= "<li>";
        $config['last_tagl_close'] 	= "</li>";
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $links = $this->pagination->create_links();
        #
        #pagination ends
        #

		$content = $this->lseller->seller_list($links,$config["per_page"],$page,$mobile,$email,$business_name);
		$this->template->full_admin_html_view($content);;
	}
	//Seller update form
	public function seller_update_form($seller_id)
	{
		$this->permission->check_label('manage_seller')->update()->redirect();
		$content = $this->lseller->seller_edit_data($seller_id);
		$this->template->full_admin_html_view($content);
	}
	// Seller Update
	public function update_seller($id)
	{
		$this->permission->check_label('manage_seller')->update()->redirect();
		$this->form_validation->set_rules('first_name', display('first_name'), 'trim|required');
		$this->form_validation->set_rules('last_name', display('last_name'), 'trim|required');
		$this->form_validation->set_rules('email', display('email'), 'trim|required|valid_email');
		$this->form_validation->set_rules('address', display('address'), 'trim');
		$this->form_validation->set_rules('seller_store_name', display('store_name'), 'trim|max_length[20]|min_length[4]');
		$this->form_validation->set_rules('business_name', display('business_name'), 'trim');
		$this->form_validation->set_rules('identification_doc_no', display('identification_doc_no'), 'trim');
		$this->form_validation->set_rules('identification_type', display('identification_type'), 'trim');
		$this->form_validation->set_rules('affiliate_id', display('affiliate_id'), 'trim');

		if ($this->form_validation->run() == FALSE)
        {
        	$data = array(
				'title' => display('add_seller')
			);
			$this->session->set_userdata(array('error_message'=>validation_errors()));
        	redirect(base_url('cseller/manage_seller/all/item'));
        }else{

        	$new_password = $this->input->post('password');
        	$old_password = $this->input->post('old_password');
		  	
			$data=array(
				'first_name' 		=> $this->input->post('first_name'),
				'last_name' 		=> $this->input->post('last_name'),
				'email' 			=> $this->input->post('email'),
				'password' 			=> (!empty($new_password)?md5("gef".$new_password):$old_password),
				'address'  			=> $this->input->post('address'),
				'mobile'  			=> $this->input->post('mobile'),
				'seller_store_name' => $this->input->post('seller_store_name'),
				'business_name' 	=> $this->input->post('business_name'),
				'identification_doc_no' => $this->input->post('identification_doc_no'),
				'identification_type'=> $this->input->post('identification_type'),
				'affiliate_id' 		=> $this->input->post('affiliate_id'),
				'status' 		    => $this->input->post('status'),
			);

			$result = $this->Sellers->update_seller($data,$id);
			if ($result == TRUE) {		
				$this->session->set_userdata(array('message'=>display('successfully_updated')));
				redirect(base_url('cseller/manage_seller/all/item'));
			}else{
				$this->session->set_userdata(array('error_message'=>display('store_name_already_exists')));
				redirect(base_url('cseller/manage_seller/all/item'));
			}
		}
	}
	//Seller Delete
	public function seller_delete($id)
	{	
		$this->permission->check_label('manage_seller')->delete()->redirect();
		$this->Sellers->delete_seller($id);
		$this->session->set_userdata(array('message'=>display('successfully_delete')));
		redirect('cseller/manage_seller/all/item');
	}
	//Seller policy
	public function seller_policy()
	{
		$this->permission->check_label('seller_policy')->redirect();
		$content = $this->lseller->seller_policy();
		$this->template->full_admin_html_view($content);
	}
	//Update seller policy
	public function update_seller_policy(){

		$this->permission->check_label('seller_policy')->update()->redirect();
		if ($_FILES['image']['name']) {
			$config['upload_path']          = 'my-assets/image/gallery/';
	        $config['allowed_types']        = 'gif|jpg|png|jpeg|JPEG|GIF|JPG|PNG';
	        $config['max_size']             = "1024";
	        $config['max_width']            = "*";
	        $config['max_height']           = "*";
	        $config['encrypt_name'] 		= TRUE;

	        $this->load->library('upload', $config);
	        if ( ! $this->upload->do_upload('image'))
	        {
	            $this->session->set_userdata(array('error_message'=> $this->upload->display_errors()));
	            redirect(base_url('seller_policy'));
	        }
	        else
	        {
	        	$image =$this->upload->data();
	        	$seller_image = base_url()."my-assets/image/gallery/".$image['file_name'];
	        }
		}

		$new_image = (!empty($seller_image)?$seller_image:null);
		$policy = $this->input->post('policy');
		$result = $this->db->set('image',$new_image)
				->set('policy',$policy)
				->where('id',1)
				->update('seller_policy');
		if ($result) {
			$this->session->set_userdata(array('message'=>display('successfully_updated')));
			redirect(base_url('seller_policy'));
		}
	}
	//This function is used to Generate Key
	public function generator($lenth)
	{
		$number=array("A","B","C","D","E","F","G","H","I","J","K","L","N","M","O","P","Q","R","S","U","V","T","W","X","Y","Z","1","2","3","4","5","6","7","8","9","0");
	
		for($i=0; $i<$lenth; $i++)
		{
			$rand_value=rand(0,34);
			$rand_number=$number["$rand_value"];
		
			if(empty($con))
			{ 
				$con=$rand_number;
			}
			else
			{
				$con="$con"."$rand_number";
			}
		}
		return $con;
	}
}