<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

//Front end routing start
$route['default_controller'] = 'Welcome';
$route['home/add_to_cart'] = 'website/Home/add_to_cart';
$route['home/delete_cart/(:any)'] = 'website/Home/delete_cart/$1';
$route['home/update_cart'] = 'website/Home/update_cart';
$route['home/apply_coupon'] = 'website/Home/apply_coupon';
$route['checkout'] = 'website/Home/checkout';
$route['view_cart'] = 'website/Home/view_cart';
$route['submit_checkout'] = 'website/Home/submit_checkout';
$route['(:any)/(:any)'] = 'website/Category/category_product/$2';
$route['category/(:any)/(:any)'] = 'website/Category/category_product/$1/$2';
$route['category_product_brand/(:any)/(:any)'] = 'website/Category/category_product_brand/$1/$2';
$route['(:any)/(:any)/(:num)'] = 'website/Product/product_details/$3';
$route['category_product_search'] = 'website/Category/category_product_search';
$route['search_category_product'] = 'website/Category/search_category_product';
$route['category_product_search_url/(:any)'] = 'website/Category/category_product_search_url/$1';
$route['category_product_search_url/(:any)/(:any)'] = 'website/Category/category_product_search_url/$1/$2';
$route['category_product/(:any)'] = 'website/Category/category_wise_product/$1';
$route['category_product/(:any)/(:num)'] = 'website/Category/category_wise_product/$1/$2';
$route['seller_product/list/(:any)'] = 'website/Product/seller_product/$1';
$route['brand_product/list/(:any)'] = 'website/Product/brand_product/$1';
$route['add_subscribe'] = 'website/Home/add_subscribe';
//Front end routing end

//Admin Dashboard Start
$route['admin'] = 'dashboard/login';
$route['customer_do_login'] = 'dashboard/do_login';

$route['cseller/manage_seller/all/item'] = 'cseller/manage_seller';
$route['cseller/seller_update_form/item/(:num)'] = 'cseller/seller_update_form/$1';
$route['cseller/update_seller/item/(:num)'] = 'cseller/update_seller/$1';
$route['cseller/seller_delete/item/(:num)'] = 'cseller/seller_delete/$1';
$route['check_seller_store_name'] = 'cseller/check_seller_store_name';
$route['insert_seller'] = 'cseller/insert_seller';
$route['check_seller_store_name'] = 'cseller/check_seller_store_name';
$route['cproduct_check_seller_store_name'] = 'cproduct/check_seller_store_name';
$route['cproduct_delete_upload_image'] = 'cproduct/delete_upload_image';
$route['cproduct_make_pending_img_primary'] = 'cproduct/make_pending_img_primary';
$route['cproduct_update_upload_product'] = 'cproduct/update_upload_product';
$route['cproduct_update_upload_title'] = 'cproduct/update_upload_title';
$route['cproduct_update_upload_escription'] = 'cproduct/update_upload_escription';
$route['cproduct_update_upload_specification'] = 'cproduct/update_upload_specification';

$route['edit_profile'] = 'dashboard/edit_profile';
$route['update_profile'] = 'dashboard/update_profile';
$route['change_password_form'] = 'dashboard/change_password_form';
$route['dashboard_logout'] = 'dashboard/logout';
$route['insert_brand'] = 'cbrand/insert_brand';
$route['manage_brand'] = 'cbrand/manage_brand';
$route['new_invoice'] = 'cinvoice/new_invoice';
$route['insert_invoice'] = 'cinvoice/insert_invoice';
$route['cinvoice/manage_invoice/all/item'] = 'cinvoice/manage_invoice';
$route['cinvoice/invoice_details_data/item/(:num)'] = 'cinvoice/invoice_details_data/$1';

$route['corder/manage_order/all/item'] = 'corder/manage_order';
$route['corder/order_details_data/(:num)'] = 'corder/order_details_data/$1';

$route['insert_order'] = 'corder/insert_order';
$route['new_order'] = 'corder/new_order';
$route['retrive_delivery_charge'] = 'corder/retrive_delivery_charge';
$route['customer/existing/check/by/phone'] = 'corder/customer_existing_check';
$route['corder/manage_pre_order/all/item'] = 'corder/manage_pre_order';
$route['retrive_customer_name'] = 'corder/retrive_customer_name';
$route['corder_get_message'] = 'corder/get_message';
$route['order_update'] = 'corder/order_update';
$route['stock'] = 'creport/stock';

$route['retrieve_product_data'] = 'Store_invoice/retrieve_product_data';
$route['cinvoice/retrieve_product_data/all/item'] = 'cinvoice/retrieve_product_data';

$route['cproduct/update_upload_status/item/(:num)'] = 'cproduct/update_upload_status/$1';
$route['cproduct/product_update_form/item/(:num)'] = 'cproduct/product_update_form/$1';
$route['cproduct/manage_product/all/item'] = 'cproduct/manage_product';
$route['cproduct/pending_product/all/item'] = 'cproduct/pending_product';
$route['cproduct/denied_product/all/item'] = 'cproduct/denied_product';
$route['retrive_com_value'] = 'cproduct/retrive_com_value';
$route['update_product'] = 'cproduct/update_product';
$route['insert_product'] = 'cproduct/insert_product';

$route['make_img_primary'] = 'cproduct/make_img_primary';
$route['insert_image'] = 'cproduct/insert_image';
$route['delete_image'] = 'cproduct/delete_image';
$route['insert_title'] = 'cproduct/insert_title';
$route['updateTitle'] = 'cproduct/updateTitle';
$route['updateDescription'] = 'cproduct/updateDescription';
$route['updateSpecification'] = 'cproduct/updateSpecification';
$route['cproduct/delete_product/item/(:num)'] = 'cproduct/delete_product/$1';
$route['cproduct/pending_product_update/item/(:num)'] = 'cproduct/pending_product_update/$1';

$route['insert_description'] = 'cproduct/insert_description';
$route['insert_specification'] = 'cproduct/insert_specification';
$route['manage_customer'] = 'ccustomer/manage_customer';
$route['customer_details'] = 'ccustomer/customer_details';
$route['insert_customer'] = 'ccustomer/insert_customer';
$route['customer_update'] = 'ccustomer/customer_update';
$route['select_city_country_id'] = 'ccustomer/select_city_country_id';
$route['customer_search_details'] = 'ccustomer/customer_search_details';
$route['manage_seller'] = 'cseller/manage_seller';
$route['seller_policy'] = 'cseller/seller_policy';
$route['update_seller_policy'] = 'cseller/update_seller_policy';
$route['manage_category'] = 'ccategory/manage_category';
$route['insert_category'] = 'ccategory/insert_category';
$route['category_update'] = 'ccategory/category_update';
$route['manage_variant'] = 'cvariant/manage_variant';
$route['insert_variant'] = 'cvariant/insert_variant';
$route['manage_unit'] = 'cunit/manage_unit';
$route['insert_unit'] = 'cunit/insert_unit';

$route['manage_comission'] = 'ccomission/manage_comission';
$route['insert_comission'] = 'ccomission/insert_comission';
$route['comission_report'] = 'ccomission/comission_report';
$route['ccomission/comission_update/item/(:num)'] = 'ccomission/comission_update/$1';
$route['ccomission/comission_update_form/item/(:num)'] = 'ccomission/comission_update_form/$1';
$route['ccomission/comission_delete/item/(:num)'] = 'ccomission/comission_delete/$1';
$route['manage_currency'] = 'ccurrency/manage_currency';
$route['insert_currency'] = 'ccurrency/insert_currency';

$route['manage_template'] = 'cemail_template/manage_template';
$route['insert_template'] = 'cemail_template/insert_template';
$route['cemail_template/template_update_form/item/(:num)'] = 'cemail_template/template_update_form/$1';
$route['cemail_template/template_update/item/(:num)'] = 'cemail_template/template_update/$1';

$route['sales_report'] = 'creport/sales_report';
$route['creport/sales_details_report/all/item'] = 'creport/sales_details_report';
$route['creport/merchant_sell_report/all/item'] = 'creport/merchant_sell_report';
$route['creport/order_details_report/all/item'] = 'creport/order_details_report';

$route['add_payment'] = 'caccounts/add_payment';
$route['caccounts/manage_payment/all/item'] = 'caccounts/manage_payment';
$route['payment_entry'] = 'caccounts/payment_entry';
$route['caccounts/payment_report/all/item'] = 'caccounts/payment_report';
$route['caccounts/paid_seller_report/all/item'] = 'caccounts/paid_seller_report';
$route['caccounts/unpaid_seller_report/all/item'] = 'caccounts/unpaid_seller_report';
$route['caccounts/payment_update_form/item/(:num)'] = 'caccounts/payment_update_form/$1';
$route['caccounts/update_payment/item/(:num)'] = 'caccounts/update_payment/$1';
$route['caccounts/payment_delete/item/(:num)'] = 'caccounts/payment_delete/$1';

$route['add_slider'] = 'cweb_setting/add_slider';
$route['cweb_setting_submit_add'] = 'cweb_setting/submit_add';
$route['setting'] = 'cweb_setting/setting';

$route['manage_company'] = 'company_setup/manage_company';
$route['company_setup_company_update_form'] = 'company_setup/company_update_form';


$route['manage_user'] = 'user/manage_user';
$route['insert_user'] = 'user/insert_user';
$route['user_update'] = 'user/user_update';
$route['user/user_update_form/item/(:any)'] = 'user/user_update_form/$1';

$route['email_configuration'] = 'csoft_setting/email_configuration';
$route['payment_gateway_setting'] = 'csoft_setting/payment_gateway_setting';
$route['social_login_setting'] = 'csoft_setting/social_login_setting';
$route['csoft_setting/update_email_configuration/item/(:num)'] = 'csoft_setting/update_email_configuration/$1';
$route['csoft_setting/update_payment_gateway_setting/item/(:num)'] = 'csoft_setting/update_payment_gateway_setting/$1';

$route['csoft_setting/update_social_setting/item/(:num)'] = 'csoft_setting/update_social_setting/$1';
$route['update_setting'] = 'csoft_setting/update_setting';

$route['create_system_role'] = 'role/create_system_role';
$route['role_list'] = 'role/role_list';
$route['role/edit_role/item/(:num)'] = 'role/edit_role/$1';
$route['role/delete_role/item/(:num)'] = 'role/delete_role/$1';
$route['role_save_update'] = 'role/save_update';
//$route['dashboard_role_list'] = 'role/role_list';
$route['assign_role_to_user'] = 'role/assign_role_to_user';
$route['role_save_create'] = 'role/save_create';
$route['save_role_access'] = 'role/save_role_access';
$route['role/edit_access_role/item/(:num)'] = 'role/edit_access_role/$1';
$route['role/delete_access_role/item/(:num)'] = 'role/delete_access_role/$1';
$route['update_access_role'] = 'role/update_access_role';

$route['user_access_role'] = 'role/user_access_role';
$route['add_wishlist'] = 'website/Home/add_wishlist';

$route['data_synchronizer_form'] = 'data_synchronizer/form';
$route['synchronize'] = 'data_synchronizer/synchronize';
$route['data_synchronizer_ftp_download'] = 'data_synchronizer/ftp_download';

$route['backup_restore_process'] = 'backup_restore/process';
$route['backup_restore_download'] = 'backup_restore/download';
$route['backup_restore_delete'] = 'backup_restore/delete';
$route['data_synchronizer_import'] = 'data_synchronizer/import';
$route['data_synchronizer_ftp_upload'] = 'data_synchronizer/ftp_upload';


$route['manage_slider'] = 'cweb_setting/manage_slider';
$route['submit_slider'] = 'cweb_setting/submit_slider';
$route['manage_add'] = 'cweb_setting/manage_add';
$route['submit_add'] = 'admin/submit_add';

$route['insert_block'] = 'cblock/insert_block';
$route['manage_block'] = 'cblock/manage_block';

$route['manage_product_review'] = 'cproduct_review/manage_product_review';
$route['cproduct_review/inactive/item/(:any)'] = 'cproduct_review/inactive/$1';
$route['cproduct_review/active/item/(:any)'] = 'cproduct_review/active/$1';
$route['cproduct_review/product_review_update_form/item/(:num)'] = 'cproduct_review/product_review_update_form/$1';
$route['cproduct_review/product_review_update/item/(:num)'] = 'cproduct_review/product_review_update/$1';
$route['cproduct_review/product_review_delete/item/(:num)'] = 'cproduct_review/product_review_delete/$1';
$route['manage_subscriber'] = 'csubscriber/manage_subscriber';
$route['manage_wishlist'] = 'cwishlist/manage_wishlist';
$route['insert_wishlist'] = 'cwishlist/insert_wishlist';

$route['manage_web_footer'] = 'cweb_footer/manage_web_footer';
$route['insert_web_footer'] = 'cweb_footer/insert_web_footer';

$route['manage_link_page'] = 'clink_page/manage_link_page';
$route['insert_link_page'] = 'clink_page/insert_link_page';
$route['clink_page/inactive/item/(:num)'] = 'clink_page/inactive/$1';
$route['clink_page/active/item/(:num)'] = 'clink_page/active/$1';
$route['clink_page/link_page_update_form/item/(:num)'] = 'clink_page/link_page_update_form/$1';
$route['clink_page/link_page_delete/item/(:num)'] = 'clink_page/link_page_delete/$1';

$route['manage_coupon'] = 'ccoupon/manage_coupon';
$route['insert_coupon'] = 'ccoupon/insert_coupon';

$route['manage_contact_form'] = 'cweb_setting/manage_contact_form';
$route['manage_about_us'] = 'cabout_us/manage_about_us';
$route['insert_about_us'] = 'cabout_us/insert_about_us';
$route['cabout_us/inactive/item/(:num)'] = 'cabout_us/inactive/$1';
$route['cabout_us/active/item/(:num)'] = 'cabout_us/active/$1';
$route['cabout_us/about_us_update_form/item/(:num)'] = 'cabout_us/about_us_update_form/$1';
$route['cabout_us/about_us_update/item/(:num)'] = 'cabout_us/about_us_update/$1';

$route['manage_our_location'] = 'cour_location/manage_our_location';
$route['insert_our_location'] = 'cour_location/insert_our_location';
$route['cour_location/inactive/item/(:num)'] = 'cour_location/inactive/$1';
$route['cour_location/active/item/(:num)'] = 'cour_location/active/$1';
$route['cour_location/our_location_update_form/item/(:num)'] = 'cour_location/our_location_update_form/$1';
$route['cour_location/our_location_update/item/(:num)'] = 'cour_location/our_location_update/$1';
$route['cour_location/our_location_delete/item/(:num)'] = 'cour_location/our_location_delete/$1';

$route['manage_shipping_method'] = 'cshipping_method/manage_shipping_method';
$route['insert_shipping_method'] = 'cshipping_method/insert_shipping_method';
$route['cshipping_method/shipping_method_update_form/item/(:num)'] = 'cshipping_method/shipping_method_update_form/$1';
$route['cshipping_method/shipping_method_update/item/(:num)'] = 'cshipping_method/shipping_method_update/$1';
$route['cshipping_method/shipping_method_delete/item/(:num)'] = 'cshipping_method/shipping_method_delete/$1';

$route['manage_shipping_agent'] = 'cshipping_agent/manage_shipping_agent';
$route['insert_shipping_agent'] = 'cshipping_agent/insert_shipping_agent';
$route['cshipping_agent/shipping_agent_update_form/item/(:num)'] = 'cshipping_agent/shipping_agent_update_form/$1';
$route['cshipping_agent/shipping_agent_update/item/(:num)'] = 'cshipping_agent/shipping_agent_update/$1';
$route['cshipping_agent/shipping_agent_delete/item/(:num)'] = 'cshipping_agent/shipping_agent_delete/$1';


$route['cweb_setting/update_web_settings/item/(:num)'] = 'cweb_setting/update_web_settings/$1';
$route['company_update'] = 'company_setup/company_update';


$route['phrase'] = 'language/phrase';
$route['addPhrase'] = 'language/addPhrase';
$route['addlanguage'] = 'language/addlanguage';
$route['addlebel'] = 'language/addlebel';
$route['Language/edit_save_ajax/item'] = 'Language/edit_save_ajax/item';
$route['language/editPhrase/item/(:num)'] = 'language/editPhrase/$1';


//Admin Dashboard End

//Customer dashboard and profile start
$route['login'] = 'website/customer/Login';
$route['logout'] = 'website/customer/Customer_dashboard/Logout';
$route['do_login'] = 'website/customer/Login/do_login';
$route['signup'] = 'website/customer/Signup';
$route['user_signup'] = 'website/customer/Signup/user_signup';
$route['customer_dashboard'] = 'website/customer/Customer_dashboard';
$route['customer/customer_dashboard/edit_profile'] = 'website/customer/Customer_dashboard/edit_profile';
$route['customer/customer_dashboard/update_profile'] = 'website/customer/Customer_dashboard/update_profile';
$route['customer/customer_dashboard/change_password_form'] = 'website/customer/Customer_dashboard/change_password_form';
$route['customer/customer_dashboard/change_password'] = 'website/customer/Customer_dashboard/change_password';
//Customer dashboard and profile end

//Customer order start
$route['customer/order'] = 'website/customer/Corder/new_order';
$route['customer/insert_order'] = 'website/customer/Corder/insert_order';
$route['customer/order/manage_order'] = 'website/customer/Corder/manage_order';
$route['customer/order/manage_order/(:any)'] = 'website/customer/Corder/manage_order/$1';
$route['customer/order/order_tracking'] = 'website/customer/Corder/order_tracking';
$route['customer/order/order_tracking/(:any)'] = 'website/customer/Corder/order_tracking/$1';
$route['customer/order/order_traking/(:any)'] = 'website/customer/Corder/order_traking/$1';
$route['customer/order/order_details_data/(:any)'] = 'website/customer/Corder/order_details_data/$1';
$route['customer/order/order_update_form/(:any)'] = 'website/customer/Corder/order_update_form/$1';
$route['customer/order/order_delete/(:any)'] = 'website/customer/Corder/order_delete/$1';
$route['customer/order/order_cancel/(:any)'] = 'website/customer/Corder/order_cancel/$1';
//Customer order end

//Customer invoice start
$route['customer_invoice'] = 'website/customer/Cinvoice/manage_invoice';
$route['customer/invoice/(:any)'] = 'website/customer/Cinvoice/manage_invoice/$1';
$route['customer/invoice/invoice_inserted_data/(:any)'] = 'website/customer/Cinvoice/invoice_inserted_data/$1';
//Customer invoice end

//Link page 
$route['about_us'] = 'website/Setting/about_us';
$route['contact_us'] = 'website/Setting/contact_us';
$route['delivery_info'] = 'website/Setting/delivery_info';
$route['privacy_policy'] = 'website/Setting/privacy_policy';
$route['terms_condition'] = 'website/Setting/terms_condition';
$route['help'] = 'website/Setting/help';
$route['submit_contact'] = 'website/Setting/submit_contact';
//Link page end

//Seller dashboard
$route['seller-signup'] = 'seller/signup';
$route['seller-login'] = 'seller/login';
$route['seller-login-do-login'] = 'seller/login/do_login';
$route['seller-dashboard'] = 'seller/seller_dashboard';
$route['seller-logout'] = 'seller/login/logout';
$route['seller_upload_product'] = 'seller/product/upload_product';
$route['seller/manage_product/all/item'] = 'seller/product/manage_product';
$route['seller/manage_product/all/item/(:any)'] = 'seller/product/manage_product/$1';
$route['seller_manage_order'] = 'seller/order/manage_order';
$route['seller/order_details_data/(:any)'] = 'seller/order/order_details_data/$1';
$route['seller_report'] = 'seller/seller_dashboard/report';
$route['seller_setting'] = 'seller/setting';
$route['seller/report/(:any)'] = 'seller/seller_dashboard/report/$1';
$route['recover_password'] = 'recover_password/recover';
$route['recover_password_seller_password'] = 'recover_password/seller_password';
$route['Recover_password_password_reset'] = 'Recover_password/password_reset';
$route['recover_password_update_password'] = 'recover_password/update_password';
$route['recover_password/reset_password/token/(:any)'] = 'recover_password/reset_password/$1';
$route['recover_password/seller_reset_password/token/(:any)'] = 'recover_password/seller_reset_password/$1';
$route['recover_password_seller_update_password'] = 'recover_password/seller_update_password';

$route['404_override'] = 'my404';
$route['translate_uri_dashes'] = FALSE;