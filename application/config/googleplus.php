<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$ci =& get_instance();
$ci->load->model('Soft_settings');
$googleplus = $ci->Soft_settings->retrieve_social_setting_by_id('1');

$config['googleplus']['application_name'] = 'web';
$config['googleplus']['client_id']        = (!empty($googleplus->client_id)?$googleplus->client_id:null);
$config['googleplus']['client_secret']    = (!empty($googleplus->client_secret)?$googleplus->client_secret:null);
$config['googleplus']['redirect_uri']     = base_url('website/customer/signup/google_plus');
$config['googleplus']['api_key']          = (!empty($googleplus->api_key)?$googleplus->api_key:null);
$config['googleplus']['scopes']           = array('profile','email');