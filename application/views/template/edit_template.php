<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Edit template start -->
<div class="content-wrapper">
    <section class="content-header">
        <div class="header-icon">
            <i class="pe-7s-note2"></i>
        </div>
        <div class="header-title">
            <h1><?php echo display('edit_template') ?></h1>
            <small><?php echo display('edit_template') ?></small>
            <ol class="breadcrumb">
                <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
                <li><a href="#"><?php echo display('email_template') ?></a></li>
                <li class="active"><?php echo display('edit_template') ?></li>
            </ol>
        </div>
    </section>

    <section class="content">

        <!-- Alert Message -->
        <?php
            $message = $this->session->userdata('message');
            if (isset($message)) {
        ?>
        <div class="alert alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $message ?>                    
        </div>
        <?php 
            $this->session->unset_userdata('message');
            }
            $error_message = $this->session->userdata('error_message');
            $validatio_error = validation_errors();
            if (($error_message || $validatio_error)) {
        ?>
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $error_message ?>                    
            <?php echo $validatio_error ?>                    
        </div>
        <?php 
            $this->session->unset_userdata('error_message');
            }
        ?>

        <div class="row">
            <div class="col-sm-12">
                <div class="column">
                    <a href="<?php echo base_url('manage_template')?>" class="btn btn-success m-b-5 m-r-2"><i class="ti-align-justify"> </i> <?php echo display('manage_template')?></a>
                </div>
            </div>
        </div>

        <!-- New template -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4><?php echo display('edit_template') ?> </h4>
                        </div>
                    </div>
                  <?php echo form_open_multipart('cemail_template/template_update/item/'.$template_id, array('class' => 'form-vertical','id' => 'validate'))?>
                    <div class="panel-body">

                        <div class="form-group row">
                            <label for="name" class="col-sm-3 col-form-label"><?php echo display('name')?> <i class="text-danger">*</i></label>
                            <div class="col-sm-6">
                                <input class="form-control" name ="name" id="name" type="text" placeholder="<?php echo display('name') ?>"  required="" value="<?php echo $name?>">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="subject" class="col-sm-3 col-form-label"><?php echo display('subject')?> <i class="text-danger">*</i></label>
                            <div class="col-sm-6">
                                <input class="form-control" name ="subject" id="subject" type="text" placeholder="<?php echo display('subject') ?>"  required="" value="<?php echo $subject?>">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="message" class="col-sm-3 col-form-label"><?php echo display('message')?> <i class="text-danger">*</i></label>
                            <div class="col-sm-6">
                                <textarea class="form-control summernote" id="message" name="message" rows="10" cols="15" required ><?php echo $message1?></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="message" class="col-sm-3 col-form-label"><?php echo display('status')?> <i class="text-danger">*</i></label>
                            <div class="col-sm-6">
                                 <select class="form-control" name="status" id="status" required="">
                                    <option></option>
                                    <option value="1" <?php if ($status == 1) {echo "selected";}?>><?php echo display('order').' '.display('pending')?></option>
                                    <option value="2" <?php if ($status == 2) {echo "selected";}?>><?php echo display('order').' '.display('processing')?></option>
                                    <option value="3" <?php if ($status == 3) {echo "selected";}?>><?php echo display('order').' '.display('shipping')?></option>
                                    <option value="4" <?php if ($status == 4) {echo "selected";}?>><?php echo display('order').' '.display('delivered')?></option>
                                    <option value="5" <?php if ($status == 5) {echo "selected";}?>><?php echo display('order').' '.display('returned')?></option>
                                    <option value="6" <?php if ($status == 6) {echo "selected";}?>><?php echo display('order').' '.display('cancel')?></option>
                                    <option value="13"  <?php if ($status == 13) {echo "selected";}?>><?php echo display('order').' '.display('partial_delivery')?></option>
                                    <option value="7" <?php if ($status == 7) {echo "selected";}?>><?php echo display('general_template')?></option>
                                    <option value="8" <?php if ($status == 9) {echo "selected";}?>><?php echo display('customer_template')?></option>
                                    <option value="9" <?php if ($status == 8) {echo "selected";}?>><?php echo display('seller_template')?></option>
                                    <option value="10"  <?php if ($status == 10) {echo "selected";}?>><?php echo display('product').' '.display('pending')?></option>
                                    <option value="11"  <?php if ($status == 11) {echo "selected";}?>><?php echo display('product').' '.display('approved')?></option>
                                    <option value="12"  <?php if ($status == 12) {echo "selected";}?>><?php echo display('product').' '.display('deny')?></option>
                                </select>
                            </div>
                        </div>
        
                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-5 col-form-label"></label>
                            <div class="col-sm-6">
                                <input type="submit" id="add-template" class="btn btn-success btn-large" name="add-template" value="<?php echo display('update') ?>" />
                            </div>
                        </div>
                    </div>
                    <?php echo form_close()?>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Edit template end -->