<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Manage template Start -->
<div class="content-wrapper">
	<section class="content-header">
	    <div class="header-icon">
	        <i class="pe-7s-note2"></i>
	    </div>
	    <div class="header-title">
	        <h1><?php echo display('manage_template') ?></h1>
	        <small><?php echo display('manage_template') ?></small>
	        <ol class="breadcrumb">
	            <li><a href=""><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
	            <li><a href="#"><?php echo display('template') ?></a></li>
	            <li class="active"><?php echo display('manage_template') ?></li>
	        </ol>
	    </div>
	</section>

	<section class="content">

		<!-- Alert Message -->
	    <?php
	        $message = $this->session->userdata('message');
	        if (isset($message)) {
	    ?>
	    <div class="alert alert-info alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        <?php echo $message ?>                    
	    </div>
	    <?php 
	        $this->session->unset_userdata('message');
	        }
	        $error_message = $this->session->userdata('error_message');
	        if (isset($error_message)) {
	    ?>
	    <div class="alert alert-danger alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        <?php echo $error_message ?>                    
	    </div>
	    <?php 
	        $this->session->unset_userdata('error_message');
	        }
	    ?>

	    <?php if($this->permission->check_label('add_template')->create()->access()){?>
        <div class="row">
            <div class="col-sm-12">
                <div class="column">
                  	<a href="<?php echo base_url('cemail_template')?>" class="btn btn-success m-b-5 m-r-2"><i class="ti-plus"> </i> <?php echo display('add_template')?></a>
                </div>
            </div>
        </div>
    	<?php } ?>

		<!-- Manage template -->
		<div class="row">
		    <div class="col-sm-12">
		        <div class="panel panel-bd lobidrag">
		            <div class="panel-heading">
		                <div class="panel-title">
		                    <h4><?php echo display('manage_template') ?></h4>
		                </div>
		            </div>
		            <div class="panel-body">
		                <div class="table-responsive">
		                    <table id="dataTableExample2" class="table table-bordered table-striped table-hover">
								<thead>
									<tr>
										<th class="text-center"><?php echo display('sl') ?></th>
										<th class="text-center"><?php echo display('name') ?></th>
										<th class="text-center"><?php echo display('subject') ?></th>
										<th class="text-center"><?php echo display('message') ?></th>
										<th class="text-center"><?php echo display('status') ?></th>
										<th class="text-center"><?php echo display('action') ?></th>
									</tr>
								</thead>
								<tbody>
								<?php
								if ($template_list) {
									foreach ($template_list as $template) {
								?>
									<tr>
										<td class="text-center"><?php echo $template['sl']?></td>
										<td class="text-center"><?php echo $template['name']?></td>
										<td class="text-center"><?php echo $template['subject']?></td>
										<td class="text-center"><?php echo $template['message']?></td>
										<td class="text-center">
										<?php 
										if ($template['status'] == 1) {
											echo "<span class=\"label label-pill label-warning m-r-15\">".display('order').' '.display('pending')."</span>";
										}elseif ($template['status'] == 2) {
											echo "<span class=\"label label-pill label-info m-r-15\">".display('order').' '.display('processing')."</span>";
										}elseif ($template['status'] == 3) {
											echo "<span class=\"label label-pill label-success m-r-15\">".display('order').' '.display('shipping')."</span>";
										}elseif ($template['status'] == 4) {
											echo "<span class=\"label label-pill label-success m-r-15\">".display('order').' '.display('delivered')."</span>";
										}elseif ($template['status'] == 5) {
											echo "<span class=\"label label-pill label-danger m-r-15\">".display('order').' '.display('returned')."</span>";
										}elseif ($template['status'] == 6) {
											echo "<span class=\"label label-pill label-danger m-r-15\">".display('order').' '.display('cancel')."</span>";
										}elseif ($template['status'] == 13) {
											echo "<span class=\"label label-pill label-warning m-r-15\">".display('order').' '.display('partial_delivery')."</span>";
										}elseif ($template['status'] == 7) {
											echo "<span class=\"label label-pill label-success m-r-15\">".display('general_template')."</span>";
										}elseif ($template['status'] == 8) {
											echo "<span class=\"label label-pill label-success m-r-15\">".display('customer_template')."</span>";
										}elseif ($template['status'] == 9) {
											echo "<span class=\"label label-pill label-success m-r-15\">".display('seller_template')."</span>";
										}elseif ($template['status'] == 10) {
											echo "<span class=\"label label-pill label-warning m-r-15\">".display('product').' '.display('pending')."</span>";
										}elseif ($template['status'] == 11) {
											echo "<span class=\"label label-pill label-success m-r-15\">".display('product').' '.display('approved')."</span>";
										}elseif ($template['status'] == 12) {
											echo "<span class=\"label label-pill label-danger m-r-15\">".display('product').' '.display('deny')."</span>";
										}
										?>
										</td>
										<td>
											<center>
												<?php if($this->permission->check_label('add_template')->update()->access()){?>
												<a href="<?php echo base_url().'cemail_template/template_update_form/item/'.$template['id']; ?>" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="<?php echo display('update') ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
												<?php } ?>
											</center>
										</td>
									</tr>
								<?php
									}
								}
								?>
								</tbody>
		                    </table>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</section>
</div>
<!-- Manage template End -->