<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!-- Manage order Start -->
<div class="content-wrapper">
    <section class="content-header">
        <div class="header-icon">
            <i class="pe-7s-note2"></i>
        </div>
        <div class="header-title">
            <h1><?php echo display('manage_order') ?></h1>
            <small><?php echo display('manage_order') ?></small>
            <ol class="breadcrumb">
                <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
                <li><a href="#"><?php echo display('order') ?></a></li>
                <li class="active"><?php echo display('manage_order') ?></li>
            </ol>
        </div>
    </section>

    <section class="content">
        <!-- Alert Message -->
        <?php
        $message = $this->session->userdata('message');
        if (isset($message)) {
            ?>
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $message ?>
            </div>
            <?php
            $this->session->unset_userdata('message');
        }
        $error_message = $this->session->userdata('error_message');
        if (isset($error_message)) {
            ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $error_message ?>
            </div>
            <?php
            $this->session->unset_userdata('error_message');
        }
        ?>

        <?php if ($this->permission->check_label('manage_order')->access()) { ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="column">
                        <a href="<?php echo base_url('new_order') ?>" class="btn btn-info m-b-5 m-r-2"><i
                                    class="ti-plus"> </i> <?php echo display('new_order') ?></a>
                    </div>
                </div>
            </div>
        <?php } ?>

        <!-- Order filtering -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form action="<?php echo base_url('corder/manage_order/all/list') ?>" method="get">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group row">
                                        <label for="order_no"
                                               class="col-sm-4 col-form-label"><?php echo display('order_no') ?></label>
                                        <div class="col-sm-8">
                                            <input class="form-control" name="order_no" id="order_no" type="text"
                                                   placeholder="<?php echo display('order_no') ?>"
                                                   value="<?php if (isset($_GET['order_no'])) {
                                                       echo $_GET['order_no'];
                                                   } ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group row">
                                        <label for="pre_order_no"
                                               class="col-sm-5 col-form-label"><?php echo display('pre_order_no') ?></label>
                                        <div class="col-sm-7">
                                            <input class="form-control" name="pre_order_no" id="pre_order_no"
                                                   type="text" placeholder="<?php echo display('pre_order_no') ?>"
                                                   value="<?php if (isset($_GET['pre_order_no'])) {
                                                       echo $_GET['pre_order_no'];
                                                   } ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group row">
                                        <label for="customer"
                                               class="col-sm-4 col-form-label"><?php echo display('customer') ?> </label>
                                        <div class="col-sm-8">
                                            <input class="form-control" name="customer" id="customer" type="text"
                                                   placeholder="<?php echo display('customer') ?>"
                                                   value="<?php if (isset($_GET['customer'])) {
                                                       echo $_GET['customer'];
                                                   } ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group row">
                                        <label for="date"
                                               class="col-sm-4 col-form-label"><?php echo display('date') ?></label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control datepicker-manage" id="date"
                                                   data-range="true" data-multiple-dates-separator="---"
                                                   data-language='en' name="date"
                                                   placeholder="<?php echo display('date') ?>"
                                                   value="<?php if (isset($_GET['date'])) {
                                                       echo $_GET['date'];
                                                   } ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group row">
                                        <label for="order_status"
                                               class="col-sm-5 col-form-label"><?php echo display('order_status') ?> </label>
                                        <div class="col-sm-7">
                                            <select class="form-control" id="order_status" name="order_status">
                                                <option value=""></option>
                                                <option value="1" <?php if (isset($_GET['order_status'])) {
                                                    if ($_GET['order_status'] == 1) {
                                                        echo "selected";
                                                    }
                                                } ?>><?php echo display('pending') ?></option>
                                                <option value="2" <?php if (isset($_GET['order_status'])) {
                                                    if ($_GET['order_status'] == 2) {
                                                        echo "selected";
                                                    }
                                                } ?>><?php echo display('processing') ?></option>
                                                <option value="3" <?php if (isset($_GET['order_status'])) {
                                                    if ($_GET['order_status'] == 3) {
                                                        echo "selected";
                                                    }
                                                } ?>><?php echo display('shipping') ?></option>
                                                <option value="4" <?php if (isset($_GET['order_status'])) {
                                                    if ($_GET['order_status'] == 4) {
                                                        echo "selected";
                                                    }
                                                } ?>><?php echo display('delivered') ?></option>
                                                <option value="5" <?php if (isset($_GET['order_status'])) {
                                                    if ($_GET['order_status'] == 5) {
                                                        echo "selected";
                                                    }
                                                } ?>><?php echo display('returned') ?></option>
                                                <option value="6" <?php if (isset($_GET['order_status'])) {
                                                    if ($_GET['order_status'] == 6) {
                                                        echo "selected";
                                                    }
                                                } ?>><?php echo display('cancelled') ?></option>
                                                <option value="7" <?php if (isset($_GET['order_status'])) {
                                                    if ($_GET['order_status'] == 7) {
                                                        echo "selected";
                                                    }
                                                } ?>><?php echo display('partial_delivery') ?></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group row">
                                        <label for="shipping"
                                               class="col-sm-4 col-form-label"><?php echo display('shipping') ?></label>
                                        <div class="col-sm-8">
                                            <select class="form-control" id="shipping" name="shipping">
                                                <option value=""></option>
                                                <?php
                                                if ($shipping_method_list) {
                                                    foreach ($shipping_method_list as $method) {
                                                        ?>
                                                        <option value="<?php echo $method['method_id'] ?>" <?php if (isset($_GET['shipping'])) {
                                                            if ($_GET['shipping'] == $method['method_id']) {
                                                                echo "selected";
                                                            }
                                                        } ?>><?php echo $method['method_name'] ?></option>
                                                    <?php }
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group row">
                                        <div class="col-sm-7">
                                            <button type="submit"
                                                    class="btn btn-primary"><?php echo display('search') ?></button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Manage order report -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4><?php echo display('manage_order') ?></h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="dataTableExample3" class="table table-bordered table-striped table-hover">
                                <thead>
                                <tr>
                                    <th><?php echo display('sl') ?></th>
                                    <th><?php echo display('order_no') ?></th>
                                    <th><?php echo display('pre_order_no') ?></th>
                                    <th><?php echo display('invoice_no') ?></th>
                                    <th><?php echo display('customer') ?></th>
                                    <th><?php echo display('shipping') ?></th>
                                    <th><?php echo display('date') ?></th>
                                    <th><?php echo display('total_amount') ?></th>
                                    <th><?php echo display('paid') ?></th>
                                    <th><?php echo display('due') ?></th>
                                    <th style="width: 25%"><?php echo display('shipping_agent') ?></th>
                                    <th style="width: 25%"><?php echo display('status') ?></th>
                                    <th><?php echo display('order_status') ?></th>
                                    <th><?php echo display('action') ?></th>
                                </tr>
                                </thead>
                                <tbody class="invoiceStatus">
                                <?php
                                if ($orders_list) {
                                    $i = 1;
                                    foreach ($orders_list as $order) {
                                        ?>
                                        <tr>
                                            <td><?php echo $order['sl'] ?></td>
                                            <td>
                                                <?php if ($this->permission->check_label('manage_order')->read()->access()) { ?>
                                                    <a href="<?php echo base_url() . 'corder/order_details_data/' . $order['order_id'] ?>"><?php echo $order['order_no'] ?>
                                                    </a>
                                                    <?php
                                                } else {
                                                    echo $order['order_no'];
                                                }
                                                ?>
                                            </td>
                                            <td><?php echo $order['pre_order_id'] ?></td>
                                            <td><a href="<?php echo base_url() . 'cinvoice/invoice_inserted_data/'
                                                    . $order['invoice_id']; ?>"><?php echo $order['invoice'] ?></a></td>
                                            <td>
                                                <?php if ($this->permission->check_label('manage_order')->read()->access()) { ?>
                                                    <a href="<?php echo base_url() . 'ccustomer/customer_details/' . $order['customer_id']; ?>"><?php echo $order['customer_name'] ?></a>
                                                    <?php
                                                } else {
                                                    echo $order['customer_name'];
                                                }
                                                ?>
                                            </td>
                                            <td><?php echo $order['method_name'] ?></td>
                                            <td><?php echo $order['final_date'] ?></td>
                                            <td style="text-align: right;"><?php echo(($position == 0) ? $currency . ' ' . $order['total_amount'] : $order['total_amount'] . ' ' . $currency) ?></td>
                                            <td><?php echo(($position == 0) ? $currency . ' ' . $order['paid_amount'] : $order['paid_amount'] . ' ' . $currency) ?></td>
                                            <td><?php
                                                $due = $order['total_amount'] - $order['paid_amount'];
                                                echo(($position == 0) ? $currency . ' ' . $due : $due . ' ' . $currency) ?></td>
                                            <td>
                                                <?php if ($this->permission->check_label('manage_order')->update()->access()) { ?>
                                                    <form action="<?php echo base_url('corder/update_shipping_agent/' . $order['order_id'])
                                                    ?>" method="post" id="">
                                                        <select name="shipping_agent" id="shipping_agent"
                                                                class="form-control">
                                                            <option value=""></option>
                                                            <?php foreach ($shipping_agent_lists as $shipping_agent_list) {
                                                                if ($shipping_agent_list['agent_id'] ==
                                                                    $order['shipping_agent_id']) {
                                                                    ?>

                                                                    <option selected value="<?php echo
                                                                    $shipping_agent_list['agent_id'] ?>"><?php
                                                                        echo $shipping_agent_list['agent_name'] ?>
                                                                    </option>

                                                                <?php } else {
                                                                    ?>
                                                                    <option value="<?php echo $shipping_agent_list['agent_id'] ?>"><?php
                                                                        echo $shipping_agent_list['agent_name'] ?>
                                                                    </option>
                                                                <?php }

                                                            } ?>
                                                        </select>
                                                        <input type="submit" class="btn btn-xs btn-primary"
                                                               value="<?php echo display('update') ?>"/>
                                                    </form>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <?php if ($this->permission->check_label('manage_order')->update()->access()) { ?>
                                                    <form action="<?php echo base_url('corder/update_status/' . $order['order_id']) ?>"
                                                          method="post" id="">
                                                        <select class="form-control"
                                                                id="order_status"
                                                                name="order_status" required="" style="width: 80%">
                                                            <option value=""></option>
                                                            <?php if ($order['order_status'] <= 1) { ?>
                                                                <option value="1" <?php if ($order['order_status'] == '1') {
                                                                    echo "selected";
                                                                } ?>><?php echo display('pending') ?></option>
                                                            <?php } ?>
                                                            <?php if ($order['order_status'] <= 2) { ?>
                                                                <option value="2" <?php if ($order['order_status'] == '2') {
                                                                    echo "selected";
                                                                } ?>><?php echo display('processing') ?></option>
                                                            <?php } ?>
                                                            <?php if ($order['order_status'] <= 3) { ?>
                                                                <option value="3" <?php if ($order['order_status'] == '3') {
                                                                    echo "selected";
                                                                } ?>><?php echo display('shipping') ?></option>
                                                            <?php } ?>

                                                            <?php if (($order['order_status'] == 4) || ($order['order_status'] == 1) || ($order['order_status'] == 2) || ($order['order_status'] == 3)) { ?>
                                                                <option value="4" <?php if ($order['order_status'] == '4') {
                                                                    echo "selected";
                                                                } ?>><?php echo display('delivered') ?></option>
                                                            <?php } ?>

                                                            <?php if (($order['order_status'] == 5) || ($order['order_status'] == 1) || ($order['order_status'] == 2) || ($order['order_status'] == 3)) { ?>
                                                                <option value="5" <?php if ($order['order_status'] == '5') {
                                                                    echo "selected";
                                                                } ?>><?php echo display('returned') ?></option>
                                                            <?php } ?>

                                                            <?php if (($order['order_status'] == 6) || ($order['order_status'] == 1) || ($order['order_status'] == 2)) { ?>
                                                                <option value="6" <?php if ($order['order_status'] == '6') {
                                                                    echo "selected";
                                                                } ?>><?php echo display('cancelled') ?></option>
                                                            <?php } ?>

                                                            <?php if (($order['order_status'] == 7) || ($order['order_status'] == 1) || ($order['order_status'] == 2) || ($order['order_status'] == 3)) { ?>
                                                                <option value="7" <?php if ($order['order_status'] == '7') {
                                                                    echo "selected";
                                                                } ?>><?php echo display('partial_delivery') ?></option>
                                                            <?php } ?>
                                                        </select>

                                                        <input type="hidden"
                                                               value="<?php echo $order['customer_email'] ?>"
                                                               name="customer_email"/>

                                                        <?php if (($order['order_status'] == 1) || ($order['order_status'] == 2) || ($order['order_status'] == 3)) { ?>
                                                            <input type="submit" class="btn btn-xs btn-primary
                                                            order_status"
                                                                   value="<?php echo display('update') ?>"/>
                                                        <?php } ?>
                                                    </form>
                                                <?php } ?>
                                            </td>

                                            <td>

                                                <?php
                                                if ($order['order_status'] == 1) {
                                                    echo "<span class=\"label label-pill label-black m-r-15\">" . display('pending') . "</span>";
                                                } elseif ($order['order_status'] == 2) {
                                                    echo "<span class=\"label label-pill label-purple m-r-15\">" . display('processing') . "</span>";
                                                } elseif ($order['order_status'] == 3) {
                                                    echo "<span class=\"label label-pill label-info m-r-15\">" . display('shipping') . "</span>";
                                                } elseif ($order['order_status'] == 4) {
                                                    echo "<span class=\"label label-pill label-success m-r-15\">" . display('delivered') . "</span>";
                                                } elseif ($order['order_status'] == 5) {
                                                    echo "<span class=\"label label-pill label-warning m-r-15\">" . display('returned') . "</span>";
                                                } elseif ($order['order_status'] == 6) {
                                                    echo "<span class=\"label label-pill label-danger m-r-15\">" . display('cancelled') . "</span>";
                                                } elseif ($order['order_status'] == 7) {
                                                    echo "<span class=\"label label-pill label-pink m-r-15\">" . display('partial_delivery') . "</span>";
                                                }
                                                ?>
                                                <!--                                            for invoiced or not message-->

                                                <?php if ($order['invoice_id']) {
                                                    echo "<br><span class=\"label label-pill label-success m-r-15\">" . display
                                                        ('invoiced') . "</span>";
                                                } else {
                                                    echo "<span class=\"label label-pill label-danger m-t-15\">" . display
                                                        ('no_invoice') . "</span>";
                                                } ?>

                                            </td>
                                            <td>
                                                <center>
                                                    <?php if ($this->permission->check_label('manage_order')->read()->access()) { ?>
                                                        <?php if ($order['status'] == 1) { ?>
                                                            <button type="button"
                                                                    class="btn  btn-success btn-xs"><?php echo display('new') ?></button>
                                                            <a href="<?php echo base_url() . 'corder/order_paid_data/' . $order['order_id']; ?>"
                                                               class="btn btn-warning btn-xs" data-toggle="tooltip"
                                                               data-placement="left"
                                                               title="<?php echo display('invoice') ?>"><i
                                                                        class="fa fa-arrow-right"
                                                                        aria-hidden="true"></i></a>
                                                        <?php } else { ?>
                                                            <a href="<?php echo base_url() . 'cinvoice/invoice_details_data/item/' . $order['order_no']; ?>"
                                                               class="btn btn-info btn-xs" data-toggle="tooltip"
                                                               data-placement="left"
                                                               title="<?php echo display('invoice_details') ?>"><i
                                                                        class="fa fa-eye" aria-hidden="true"></i></a>
                                                        <?php } ?>

                                                        <a href="<?php echo base_url() . 'corder_tracking/order_traking/' . $order['order_id']; ?>"
                                                           class="btn btn-warning btn-xs" data-toggle="tooltip"
                                                           data-placement="left"
                                                           title="<?php echo display('order_tracking') ?>"><i
                                                                    class="fa fa-info" aria-hidden="true"></i></a>
                                                    <?php } ?>

                                                    <?php if ($this->permission->check_label('manage_order')->update()->access()) { ?>

                                                        <?php if ($order['order_status'] < 2) { ?>
                                                            <a href="<?php echo base_url() . 'corder/order_update_form/' . $order['order_id']; ?>"
                                                               class="btn btn-info btn-xs" data-toggle="tooltip"
                                                               data-placement="left"
                                                               title="<?php echo display('update') ?>"><i
                                                                        class="fa fa-pencil" aria-hidden="true"></i></a>
                                                        <?php } ?>

                                                        <?php if ($order['order_status'] == 7) { ?>
                                                            <a href="<?php echo base_url() . 'corder/order_return_form/' . $order['order_id']; ?>"
                                                               class="btn btn-warning btn-xs" data-toggle="tooltip"
                                                               data-placement="left"
                                                               title="<?php echo display('partial_delivery') ?>"><i
                                                                        class="fa fa-undo" aria-hidden="true"></i></a>
                                                        <?php }
                                                    } ?>

                                                    <?php if ($this->permission->check_label('manage_order')->delete()->access()) { ?>
                                                        <a href="<?php echo base_url('corder/order_delete/' . $order['order_id'] . '/' . $order['order_no']) ?>"
                                                           class="btn btn-danger btn-xs"
                                                           onclick="return confirm('<?php echo display('are_you_sure_want_to_delete') ?>');"
                                                           data-toggle="tooltip" data-placement="right" title=""
                                                           data-original-title="<?php echo display('delete') ?> "><i
                                                                    class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                    <?php } ?>

                                                </center>
                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-right">
                            <?php echo $links ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Manage order End -->

<script>
    //manage order
function orderStatus(id){
alert(id);
}

$('.order_status').on('click',function () {
    var agent = $('#shipping_agent').val();
    // if(agent){
    // alert(agent);
    //
    // }else {
    //
    // alert('nai');
    // }

    alert(agent);

})

</script>