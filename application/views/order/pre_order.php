<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Manage pre order Start -->
<div class="content-wrapper">
	<section class="content-header">
	    <div class="header-icon">
	        <i class="pe-7s-note2"></i>
	    </div>
	    <div class="header-title">
	        <h1><?php echo display('manage_pre_order') ?></h1>
	        <small><?php echo display('manage_pre_order') ?></small>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
	            <li><a href="#"><?php echo display('pre_order') ?></a></li>
	            <li class="active"><?php echo display('manage_pre_order') ?></li>
	        </ol>
	    </div>
	</section>

	<section class="content">
		<!-- Alert Message -->
	    <?php
	        $message = $this->session->userdata('message');
	        if (isset($message)) {
	    ?>
	    <div class="alert alert-info alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        <?php echo $message ?>                    
	    </div>
	    <?php 
	        $this->session->unset_userdata('message');
	        }
	        $error_message = $this->session->userdata('error_message');
	        if (isset($error_message)) {
	    ?>
	    <div class="alert alert-danger alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        <?php echo $error_message ?>                    
	    </div>
	    <?php 
	        $this->session->unset_userdata('error_message');
	        }
	    ?>

	    <?php if($this->permission->check_label('new_order')->create()->access()){?>
        <div class="row">
            <div class="col-sm-12">
                <div class="column">
                  	<a href="<?php echo base_url('new_order')?>" class="btn btn-info m-b-5 m-r-2"><i class="ti-plus"> </i> <?php echo display('new_order')?></a>
                </div>
            </div>
        </div>
    	<?php } ?>


        <!-- Invoice filtering -->
        <div class="row">
			<div class="col-sm-12">
		        <div class="panel panel-default">
		            <div class="panel-body">
		            	<form action="<?php echo base_url('corder/manage_pre_order/all/item')?>" method="get">
		     				<div class="row">

	                            <div class="col-sm-4">
	                                <div class="form-group row">
	                                    <label for="pre_order_no" class="col-sm-5 col-form-label"><?php echo display('pre_order_no')?></label>
	                                    <div class="col-sm-7">
	                                      	<input class="form-control" name ="pre_order_no" id="pre_order_no" type="text" placeholder="<?php echo display('pre_order_no') ?>" value="<?php if(isset($_GET['pre_order_no'])){ echo $_GET['pre_order_no'];}?>">
	                                    </div>
	                                </div>
	                            </div>
	                            
	                            <div class="col-sm-3">
	                                <div class="form-group row">
	                                    <label for="customer" class="col-sm-4 col-form-label"><?php echo display('customer')?> </label>
	                                    <div class="col-sm-8">
	                                      	<select class="form-control" name="customer" id="customer">
	                                      		<option></option>
	                                      		<?php
	                                      		if ($customer_list) {
	                                      			foreach ($customer_list as $customer) {
	                                      		?>
	                                      		<option value="<?php echo $customer['customer_id']?>" <?php if (isset($_GET['customer'])) {if ($_GET['customer'] == $customer['customer_id']) {echo "selected";}}?>><?php echo $customer['customer_mobile']?></option>
	                                      		<?php
	                                      			}
	                                      		}
	                                      		?>
	                                      	</select>
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-3">
	                                <div class="form-group row">
	                                    <label for="date" class="col-sm-2 col-form-label"><?php echo display('date')?></label>
	                                    <div class="col-sm-10">
	                                      	<input type="text" class="form-control datepicker-manage" id="date" data-range="true"  data-multiple-dates-separator="---" data-language='en' name="date" placeholder="<?php echo display('date') ?>" value="<?php if(isset($_GET['date'])){ echo $_GET['date'];}?>">
	                                    </div>
	                                </div>
	                            </div>
	                   
	                            <div class="col-sm-1">
	                                <div class="form-group row">
	                                    <div class="col-sm-7">
	                                      	<button type="submit" class="btn btn-primary"><?php echo display('search')?></button>
	                                    </div>
	                                </div>
	                            </div>

	                        </div>  
		            	</form>   
			        </div>
		        </div>
		    </div>
		</div>

		<!-- Manage pre order report -->
		<div class="row">
		    <div class="col-sm-12">
		        <div class="panel panel-bd lobidrag">
		            <div class="panel-heading">
		                <div class="panel-title">
		                    <h4><?php echo display('manage_pre_order') ?></h4>
		                </div>
		            </div>
		            <div class="panel-body">
		                <div class="table-responsive">
		                    <table id="dataTableExample3" class="table table-bordered table-striped table-hover">
		                    	<thead>
									<tr>
										<th><?php echo display('sl') ?></th>
										<th><?php echo display('pre_order_no') ?></th>
										<th><?php echo display('customer_name') ?></th>
										<th><?php echo display('date') ?></th>
										<th><?php echo display('total_amount') ?></th>
										<th><?php echo display('action') ?></th>
									</tr>
								</thead>
								<tbody>
								<?php
								if ($orders_list) {
									foreach ($orders_list as $order) {
								?>
									<tr>
										<td><?php echo $order['sl']?></td>
										<td>
											<?php if($this->permission->check_label('manage_pre_order')->read()->access()){?>
											<a href="<?php echo base_url().'corder/pre_order_details_data/'.$order['order_id'] ?>"><?php echo $order['id']?>
											</a>
											<?php
											}else{
												echo $order['id'];
											}
											?>
										</td>
										<td>
											<?php if($this->permission->check_label('manage_customer')->read()->access()){?>
											<a href="<?php echo base_url().'ccustomer/customer_details/'.$order['customer_id']; ?>"><?php echo $order['customer_name']?></a>
											<?php }else{
												echo $order['customer_name'];
											}?>			
										</td>
										<td><?php echo $order['final_date']?></td>
										<td style="text-align: right;"><?php echo (($position==0)?$currency.' '.$order['total_amount']:$order['total_amount'].' '.$currency) ?></td>
										<td>
											<center>
												<?php if($this->permission->check_label('manage_pre_order')->update()->access()){?>
												<?php if ($order['status'] == 1) { ?>
													<a href="<?php echo base_url().'corder/pre_order_paid_data/'.$order['order_id']; ?>" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="left" title="<?php echo display('order') ?>"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>

												<?php if($this->permission->check_label('manage_order')->delete()->access()){?>
												<a href="<?php echo base_url('corder/pre_order_delete/'.$order['order_id'])?>" class="btn btn-danger btn-sm"  onclick="return confirm('<?php echo display('are_you_sure_want_to_delete')?>');" data-toggle="tooltip" data-placement="right" title="" data-original-title="<?php echo display('delete') ?> "><i class="fa fa-trash-o" aria-hidden="true"></i></a>
												<?php } ?>

												<?php }else{ ?>
													<button class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="left" title="<?php echo display('ordered') ?>"><i class="fa fa-check" aria-hidden="true"></i></button>
												<?php } }?>

												<?php if($this->permission->check_label('manage_pre_order')->read()->access()){?>
													<a href="<?php echo base_url().'corder/pre_order_details_data/'.$order['order_id']; ?>" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="left" title="<?php echo display('order_details') ?>"><i class="fa fa-eye"></i></a>
												<?php } ?>

											</center>
										</td>
									</tr>
								<?php
									}
								}
								?>
								</tbody>
		                    </table>
                        <div class="text-right">
                            <?php echo $links?>
                        </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</section>
</div>
<!-- Manage pre order End -->