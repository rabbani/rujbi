<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Manage Customer Start -->
<div class="content-wrapper">
	<section class="content-header">
	    <div class="header-icon">
	        <i class="pe-7s-note2"></i>
	    </div>
	    <div class="header-title">
	        <h1><?php echo display('manage_customer') ?></h1>
	        <small><?php echo display('manage_your_customer') ?></small>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
	            <li><a href="#"><?php echo display('customer') ?></a></li>
	            <li class="active"><?php echo display('manage_customer') ?></li>
	        </ol>
	    </div>
	</section>

	<section class="content">
		<!-- Alert Message -->
	    <?php
	        $message = $this->session->userdata('message');
	        if (isset($message)) {
	    ?>
	    <div class="alert alert-info alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        <?php echo $message ?>                    
	    </div>
	    <?php 
	        $this->session->unset_userdata('message');
	        }
	        $error_message = $this->session->userdata('error_message');
	        if (isset($error_message)) {
	    ?>
	    <div class="alert alert-danger alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        <?php echo $error_message ?>                    
	    </div>
	    <?php 
	        $this->session->unset_userdata('error_message');
	        }
	    ?>

	    <?php if ($this->permission->check_label('add_customer')->access()) {?>
        <div class="row">
            <div class="col-sm-12">
                <div class="column">
                  	<a href="<?php echo base_url('ccustomer')?>" class="btn btn-success m-b-5 m-r-2"><i class="ti-plus"> </i> <?php echo display('add_customer')?></a>
                </div>
            </div>
        </div>
    	<?php }?>

 		<!-- Invoice filtering -->
        <div class="row">
			<div class="col-sm-12">
		        <div class="panel panel-default">
		            <div class="panel-body"> 

		            	<form action="<?php echo base_url('manage_customer')?>" method="get">
		     				<div class="row">

		     					<div class="col-sm-4">
	                                <div class="form-group row">
	                                    <label for="customer" class="col-sm-4 col-form-label"><?php echo display('customer')?></label>
	                                    <div class="col-sm-8">
	                                      	<input class="form-control" name ="customer" id="customer" type="text" placeholder="<?php echo display('customer') ?>" value="<?php if(isset($_GET['customer'])){ echo $_GET['customer'];}?>">
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-3">
	                                <div class="form-group row">
	                                    <label for="mobile" class="col-sm-4 col-form-label"><?php echo display('mobile')?></label>
	                                    <div class="col-sm-8">
	                                      	<input class="form-control" name ="mobile" id="mobile" type="text" placeholder="<?php echo display('mobile') ?>" value="<?php if(isset($_GET['mobile'])){ echo $_GET['mobile'];}?>">
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-3">
	                                <div class="form-group row">
	                                    <label for="email" class="col-sm-5 col-form-label"><?php echo display('email')?></label>
	                                    <div class="col-sm-7">
	                                      	<input class="form-control" name ="email" id="email" type="text" placeholder="<?php echo display('email') ?>" value="<?php if(isset($_GET['email'])){ echo $_GET['email'];}?>">
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-2">
	                                <div class="form-group row">
	                                    <div class="col-sm-7">
	                                      	<button type="submit" class="btn btn-primary"><?php echo display('search')?></button>
	                                    </div>
	                                </div>
	                            </div>

	                        </div>  
		            	</form>   
			        </div>
		        </div>
		    </div>
		</div>

		<!-- Manage Customer -->
		<div class="row">
		    <div class="col-sm-12">
		        <div class="panel panel-bd lobidrag">
		            <div class="panel-heading">
		                <div class="panel-title">
		                    <h4><?php echo display('manage_customer') ?></h4>
		                </div>
		            </div>
		            <div class="panel-body">
		                <div class="table-responsive">
		                    <table id="dataTableExample3" class="table table-bordered table-striped table-hover">
								<thead>
									<tr>
										<th><?php echo display('sl') ?></th>
										<th><?php echo display('customer_id') ?></th>
										<th><?php echo display('name') ?></th>
										<th><?php echo display('address') ?></th>
										<th><?php echo display('mobile') ?></th>
										<th><?php echo display('email') ?></th>
										<th style="text-align:center !Important"><?php echo display('action') ?></th>
									</tr>
								</thead>
								<tbody id="customer_list">
								<?php if ($customers_list) { ?>
								{customers_list}
									<tr>
										<td>{sl}</td>
										<td>{customer_code}</td>
										<td>
											<?php if ($this->permission->check_label('manage_customer')->read()->access()) {?>
											<a href="<?php echo base_url().'ccustomer/customer_details/{customer_id}'; ?>">{customer_name}</a>
											<?php
											}else{
												echo "{customer_name}";
											}
											?>
										</td>
										<td>{customer_short_address}</td>
										<td>{customer_mobile}</td>
										<td>{customer_email}</td>
										<td>
											<center>
												<?php if ($this->permission->check_label('manage_customer')->update()->access()) {?>
												<a href="<?php echo base_url().'ccustomer/customer_update_form/{customer_id}'; ?>" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="<?php echo display('update') ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
												<?php
												}if ($this->permission->check_label('manage_customer')->delete()->access()) {
												?>
												<a href="<?php echo base_url('ccustomer/customer_delete/{customer_id}')?>" class="btn btn-danger btn-sm" onclick="return confirm('<?php echo display('are_you_sure_want_to_delete')?>');" data-toggle="tooltip" data-placement="right" title="" data-original-title="<?php echo display('delete') ?> "><i class="fa fa-trash-o" aria-hidden="true"></i></a>
												<?php }	?>
											</center>
										</td>
									</tr>
								{/customers_list}
								<?php } ?>
								</tbody>
		                    </table>
		                </div>
		                <div class="text-right">
		                	<?php echo $links?>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</section>
</div>
<!-- Manage Customer End -->