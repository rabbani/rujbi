<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- customer details Start -->
<div class="content-wrapper">
	<section class="content-header">
	    <div class="header-icon">
	        <i class="pe-7s-note2"></i>
	    </div>
	    <div class="header-title">
	        <h1><?php echo display('customer_details') ?></h1>
	        <small><?php echo display('manage_customer_details') ?></small>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
	            <li><a href="#"><?php echo display('customer') ?></a></li>
	            <li class="active"><?php echo display('customer_details') ?></li>
	        </ol>
	    </div>
	</section>

	<!-- Supplier information -->
	<section class="content">
		<!-- Alert Message -->
	    <?php
	        $message = $this->session->userdata('message');
	        if (isset($message)) {
	    ?>
	    <div class="alert alert-info alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        <?php echo $message ?>                    
	    </div>
	    <?php 
	        $this->session->unset_userdata('message');
	        }
	        $error_message = $this->session->userdata('error_message');
	        if (isset($error_message)) {
	    ?>
	    <div class="alert alert-danger alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        <?php echo $error_message ?>                    
	    </div>
	    <?php 
	        $this->session->unset_userdata('error_message');
	        }
	    ?>

        <div class="row">
			<div class="col-sm-12">
		        <div class="panel panel-default">
		            <div class="panel-body"> 

		            	<form action="<?php echo base_url('customer_search_details')?>" method="post">
		     				<div class="row">
	                            
	                            <div class="col-sm-4">
	                                <div class="form-group row">
	                                    <label for="customer" class="col-sm-3 col-form-label"><?php echo display('customer')?> </label>
	                                    <div class="col-sm-8">
	                                    	<select class="form-control" name="customer_id">
	                                    		<option></option>
												<?php foreach ($customer_list as $customer) { ?>
					                           	<option value="<?php echo $customer['customer_id']?>" <?php if ($id == $customer['customer_id']) {echo "selected";}?>><?php echo $customer['customer_name']?></option>
												<?php } ?>
				                            </select>
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-3">
	                                <div class="form-group row">
	                                    <label for="customer_mobile" class="col-sm-3 col-form-label"><?php echo display('mobile')?></label>
	                                    <div class="col-sm-9">
	                                      	<select class="form-control" name="customer_mobile">
	                                      		<option></option>
												<?php foreach ($customer_list as $customer) { ?>
					                           	<option value="<?php echo $customer['customer_mobile']?>" <?php if ($mobile == $customer['customer_mobile']) {echo "selected";}?>><?php echo $customer['customer_mobile']?></option>
												<?php } ?>
				                            </select>
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-3">
	                                <div class="form-group row">
	                                    <label for="email" class="col-sm-3 col-form-label"><?php echo display('email')?></label>
	                                    <div class="col-sm-9">
	                                      	<input type="email" class="form-control" name="email" placeholder="<?php echo display('email')?>" value="<?php echo $email?>">
	                                    </div>
	                                </div>
	                            </div>
	                   
	                            <div class="col-sm-1">
	                                <div class="form-group row">
	                                    <div class="col-sm-7">
	                                      	<button type="submit" class="btn btn-primary"><?php echo display('search')?></button>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>  
		            	</form>
			        </div>
		        </div>
		    </div>
	    </div>

	    <?php
	    if ($customer_id) {
	    ?>

		<div class="row">
		    <div class="col-sm-12">
		        <div class="panel panel-bd lobidrag">
		            <div class="panel-heading">
		                <div class="panel-title">
		                    <h4><?php echo display('customer_information') ?></h4>
		                </div>
		            </div>

		            <div class="panel-body">
	  					<div style="float:left;margin-right: 50px">
							<?php echo display('customer_name')?> : {customer_name} <br>
							<?php echo display('address')?> : {customer_address}<br>
							<?php echo display('mobile')?> : {customer_mobile}<br>
							<?php echo display('email')?> : {customer_email}<br>
							<?php echo display('id')?> : {customer_code}<br>
						</div>

						<div style="float:left">
							<?php echo display('city')?> : {city}<br>
							<?php echo display('state')?> : {state}<br>
							<?php echo display('country')?> : {country}<br>
							<?php echo display('zip')?> : {zip}<br>
							<?php echo display('company')?> : {company}<br>
						</div>
		            </div>
		        </div>
		    </div>
		</div>

		<!-- Customer details -->
		<div class="row">
		    <div class="col-sm-12">
		        <div class="panel panel-bd lobidrag">
		            <div class="panel-heading">
		                <div class="panel-title">
		                    <h4><?php echo display('sales_report') ?></h4>
		                </div>
		            </div>
		            <div class="panel-body">
		                <div class="table-responsive">
		                    <table id="dataTableExample2" class="table table-bordered table-striped table-hover">
								<thead>
									<tr>
										<th class="text-center"><?php echo display('date') ?></th>
										<th class="text-center"><?php echo display('invoice_no') ?></th>
										<th class="text-center"><?php echo display('customer_name') ?></th>
										<th class="text-center"><?php echo display('ammount') ?></th>
									</tr>
								</thead>
								<tbody>
								<?php
								if ($salesData) {
									foreach ($salesData as $sale) {
								?>
									<tr>
										<td class="text-center"><?php echo $sale['final_date']?></td>
										<td class="text-center"><a href="<?php echo base_url('cinvoice/invoice_inserted_data/'.$sale['invoice_id'])?>"><?php echo $sale['invoice_id']?></td>
										<td class="text-center"><?php echo $sale['customer_name']?></td>
										<td class="text-right"><?php echo (($position==0)?$currency." ".$sale['total_amount']: $sale['total_amount']." ".$currency) ?></td>
									</tr>
								<?php
									}
								}
								?>
								</tbody>
								<tfoot>
									<tr>
										<td class="text-right" colspan="3" style="font-weight: bold"><?php echo display('total_ammount');?>:</td>
										<td class="text-right"><b><?php echo (($position==0)?"$currency {totaSalesAmt}":"{totaSalesAmt} $currency") ?></b></td>
									</tr>
								</tfoot>
		                    </table>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
		<?php } ?>
	</section>
</div>
<!-- customer details End  -->