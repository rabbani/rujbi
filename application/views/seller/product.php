<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Manage product Start -->
<div class="content-wrapper">
	<section class="content-header">
	    <div class="header-icon">
	        <i class="pe-7s-note2"></i>
	    </div>
	    <div class="header-title">
	        <h1><?php echo display('manage_product') ?></h1>
	        <small><?php echo display('manage_product') ?></small>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
	            <li><a href="#"><?php echo display('seller') ?></a></li>
	            <li class="active"><?php echo display('manage_product') ?></li>
	        </ol>
	    </div>
	</section>

	<section class="content">
		<!-- Alert Message -->
	    <?php
	        $message = $this->session->userdata('message');
	        if (isset($message)) {
	    ?>
	    <div class="alert alert-info alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        <?php echo $message ?>                    
	    </div>
	    <?php 
	        $this->session->unset_userdata('message');
	        }
	        $error_message = $this->session->userdata('error_message');
	        if (isset($error_message)) {
	    ?>
	    <div class="alert alert-danger alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        <?php echo $error_message ?>                    
	    </div>
	    <?php 
	        $this->session->unset_userdata('error_message');
	        }
	    ?>

        <div class="row">
            <div class="col-sm-12">
                <div class="column">
                  	<a href="<?php echo base_url('cseller/upload_product')?>" class="btn btn-success m-b-5 m-r-2"><i class="ti-plus"> </i> <?php echo display('upload_product')?></a>
                </div>
            </div>
        </div>

		<!-- Manage product -->
		<div class="row">
		    <div class="col-sm-12">
		        <div class="panel panel-bd lobidrag">
		            <div class="panel-heading">
		                <div class="panel-title">
		                    <h4><?php echo display('manage_product') ?></h4>
		                </div>
		            </div>
		            <div class="panel-body">
		                <div class="table-responsive">
		                    <table id="dataTableExample2" class="table table-bordered table-striped table-hover">
								<thead>
									<tr>
										<th><?php echo display('sl') ?></th>
										<th><?php echo display('seller') ?></th>
										<th><?php echo display('category') ?></th>
										<th><?php echo display('price') ?></th>
										<th><?php echo display('quantity') ?></th>
										<th><?php echo display('product_model') ?></th>
										<th><?php echo display('brand') ?></th>
										<th style="width: 25%"><?php echo display('status') ?></th>
										<th style="text-align:center !Important"><?php echo display('action') ?></th>
									</tr>
								</thead>
								<tbody>
								<?php if ($product_list) { 
									$i=1;
									foreach ($product_list as $product) {
								?>
									<tr>
										<td><?php echo $product['sl']?></td>
										<td><?php echo $product['first_name']." ".$product['last_name']." (".$product['seller_id'].")"?></td>
										<td><?php echo $product['category_name']?></td>
										<td><?php echo $product['price']?></td>
										<td><?php echo $product['quantity']?></td>
										<td><?php echo $product['product_model']?></td>
										<!-- <td> -->
										<?php 
											// $variant = $product['variant'];
											// $v = explode(",", $variant);
											// for ($i=0; $i < count($v) ; $i++) {
											// 	if ($v[$i]) {
											// 		$variant = $this->db->select('*')
											// 				->from('variant')
											// 				->where('variant_id',$v[$i])
											// 				->get()
											// 				->row();
											// 		echo $variant->variant_name.",";
											// 	}
											// }										
										?>
										<!-- </td> -->
										<td><?php echo $product['brand_name']?></td>
										<td>
										<?php 
										// if ($product['status'] == 1) {
										// 	echo "<span class=\"label label-warning m-r-15\">".display('pending')."</span>";
										// }elseif ($product['status'] == 2) {
										// 	echo "<span class=\"label label-success m-r-15\">".display('approved')."</span>";
										// }else{
										// 	echo "<span class=\"label label-danger m-r-15\">".display('denied')."</span>";
										// }
										?>
										<form action="<?php echo base_url('cseller/update_status/'.$product['upload_id'].'/'.$product['product_id'])?>" method="post" id="add_note_form">
											<select class="form-control" id="product_status" name="product_status" required="">
		                                       	<option value=""></option>
		                                        <option value="1" <?php if ($product['status'] == '1'){echo "selected";}?>><?php echo display('pending')?></option>

		                                        <option value="2" <?php if ($product['status'] == '2'){echo "selected";}?>><?php echo display('approved')?></option>

		                                        <option value="3" <?php if ($product['status'] == '3'){echo "selected";}?>><?php echo display('denied')?></option>
		                                    </select>

		                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal_<?php echo $i?>" title="<?php echo display('add_note')?>" /><i class="fa fa-plus" aria-hidden="true"></i></button>

		                                    <input type="hidden" value="<?php echo $product['email'] ?>" name="seller_email"/>

		                                    <div class="modal fade" id="myModal_<?php echo $i?>"  role="dialog">
						                        <div class="modal-dialog" role="document">
						                            <div class="modal-content">
						                                <div class="modal-header">
						                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						                                    <h1 class="modal-title"><?php echo display('add_note')?></h1>
						                                </div>
						                                <div class="modal-body">
							                                <div class="form-group row">
							                                    <label for="" class="col-sm-2 col-form-label"><?php echo display('add_note') ?> </label>
							                                    <div class="col-sm-10">
							                                    	<textarea name="add_note_<?php echo $i?>" class="form-control summernote" id="add_note_<?php echo $i?>" required></textarea>
							                                    </div>
							                                </div> 
						                                </div>
						                                <div class="modal-footer">
						                                    <button type="button" class="btn btn-success" data-dismiss="modal"><?php echo display('submit')?></button>
						                                </div>
						                            </div><!-- /.modal-content -->
						                        </div><!-- /.modal-dialog -->
						                    </div><!-- /.modal -->

						                    <input type="submit" class="btn btn-primary" value="<?php echo display('update') ?>" style="position: absolute;margin-left: 5px;" onclick="noteCheck('<?php echo $i?>');"/>

						                    <!-- Note check js -->
						                    <script type="text/javascript">
						                    	function noteCheck(id){
						                    		var add_note = $('#add_note_'+id).val();
						                    		if (add_note) {
						                    			return true;
						                    		}else{
						                    			alert('<?php echo display('please_add_note')?>');
						                    			return false;
						                    		}
						                    	}
						                    </script>
										</form>
										</td>
										<td>
											<center>
											<?php echo form_open()?>
												<a href="<?php echo base_url().'cseller/seller_product_update_form/'.$product['upload_id']; ?>" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="<?php echo display('update') ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>

												<a href="<?php echo base_url('cseller/delete_product/'.$product['upload_id'])?>" class="btn btn-danger btn-sm" onclick="return confirm('<?php echo display('are_you_sure_want_to_delete')?>');" data-toggle="tooltip" data-placement="right" title="" data-original-title="<?php echo display('delete') ?> "><i class="fa fa-trash-o" aria-hidden="true"></i></a>
											<?php echo form_close()?>
											</center>
										</td>
									</tr>
								<?php $i++;  } }?>
								</tbody>
		                    </table>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</section>
</div>
<!-- Manage product End -->