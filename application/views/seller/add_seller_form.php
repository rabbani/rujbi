<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<!-- Add new seller start -->
<div class="content-wrapper">
    <section class="content-header">
        <div class="header-icon">
            <i class="pe-7s-note2"></i>
        </div>
        <div class="header-title">
            <h1><?php echo display('add_seller') ?></h1>
            <small><?php echo display('add_seller') ?></small>
            <ol class="breadcrumb">
                <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
                <li><a href="#"><?php echo display('seller') ?></a></li>
                <li class="active"><?php echo display('add_seller') ?></li>
            </ol>
        </div>
    </section>

    <section class="content">
        <!-- Alert Message -->
        <?php
            $message = $this->session->userdata('message');
            if (isset($message)) {
        ?>
        <div class="alert alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $message ?>                    
        </div>
        <?php 
            $this->session->unset_userdata('message');
            }
            $error_message = $this->session->userdata('error_message');
            if (isset($error_message)) {
        ?>
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $error_message ?>                    
        </div>
        <?php 
            $this->session->unset_userdata('error_message');
            }
        ?>

        <div class="row">
            <div class="col-sm-12">
                <div class="column">
                    <a href="<?php echo base_url('cseller/manage_seller/all/item')?>" class="btn btn-success m-b-5 m-r-2"><i class="ti-align-justify"> </i> <?php echo display('manage_seller')?></a>
                </div>
            </div>
        </div>

        <!-- New seller -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-bd">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4><?php echo display('add_seller') ?> </h4>
                        </div>
                    </div>
                    <?php echo form_open('insert_seller', array('class' => 'form-vertical','id' => 'validate'))?>
                    <div class="panel-body">

                    	<div class="form-group row">
                            <label for="first_name" class="col-sm-3 col-form-label"><?php echo display('first_name') ?> <i class="text-danger">*</i></label>
                            <div class="col-sm-6">
                                <input class="form-control" name ="first_name" id="first_name" type="text" placeholder="<?php echo display('first_name') ?>"  required="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="last_name" class="col-sm-3 col-form-label"><?php echo display('last_name') ?> <i class="text-danger">*</i></label>
                            <div class="col-sm-6">
                                <input class="form-control" name ="last_name" id="last_name" type="text" placeholder="<?php echo display('last_name') ?>"  required="">
                            </div>
                        </div>
   
                       	<div class="form-group row">
                            <label for="email" class="col-sm-3 col-form-label"><?php echo display('email') ?> <i class="text-danger">*</i></label>
                            <div class="col-sm-6">
                                <input class="form-control" name ="email" id="email" type="email" placeholder="<?php echo display('email') ?>"  required="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-sm-3 col-form-label"><?php echo display('password') ?> <i class="text-danger">*</i></label>
                            <div class="col-sm-6">
                                <input class="form-control" name ="password" id="password" type="password" placeholder="<?php echo display('password') ?>"  required="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="mobile" class="col-sm-3 col-form-label"><?php echo display('mobile') ?> <i class="text-danger">*</i></label>
                            <div class="col-sm-6">
                                <input class="form-control" name ="mobile" id="mobile" type="text" placeholder="<?php echo display('mobile') ?>"  required="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="business_name" class="col-sm-3 col-form-label"><?php echo display('business_name') ?> <i class="text-danger">*</i></label>
                            <div class="col-sm-6">
                                <input class="form-control" name ="business_name" id="business_name" type="text" placeholder="<?php echo display('business_name') ?>" required>
                            </div>
                        </div>
   
                        <div class="form-group row">
                            <label for="address " class="col-sm-3 col-form-label"><?php echo display('address') ?> </label>
                            <div class="col-sm-6">
                                <textarea class="form-control" name="address" id="address " rows="3" placeholder="<?php echo display('address') ?>"></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="store_name" class="col-sm-3 col-form-label"><?php echo display('store_name') ?> </label>
                            <div class="col-sm-6">
                                <input class="form-control" name ="seller_store_name" id="store_name" type="text" placeholder="<?php echo display('store_name') ?>">
                                <span class="help-block small"><?php echo display('store_name_4_20_character_like_store_name') ?></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="identification_doc_no " class="col-sm-3 col-form-label"><?php echo display('identification_doc_no') ?></label>
                            <div class="col-sm-6">
                                <input class="form-control" name ="identification_doc_no" id="identification_doc_no" type="text" placeholder="<?php echo display('identification_doc_no') ?>">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="identification_type " class="col-sm-3 col-form-label"><?php echo display('identification_type') ?></label>
                            <div class="col-sm-6">
                                <select class="form-control select2" id="identification_type" name="identification_type" style="width: 100%">
                                    <option value=""><?php echo display('select_one') ?></option>
                                    <option value="1"><?php echo display('driving_licence')?></option>
                                    <option value="2"><?php echo display('national_id')?></option>
                                    <option value="3"><?php echo display('passport_no')?></option>
                                    <option value="4"><?php echo display('others')?></option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="affiliate_id" class="col-sm-3 col-form-label"><?php echo display('affiliate_id') ?></label>
                            <div class="col-sm-6">
                                <input class="form-control" name ="affiliate_id" id="affiliate_id" type="text" placeholder="<?php echo display('affiliate_id') ?>" >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-4 col-form-label"></label>
                            <div class="col-sm-6">
                                <input type="submit" id="add-seller" class="btn btn-primary btn-large" name="add-seller" value="<?php echo display('save') ?>" />
								<input type="submit" value="<?php echo display('save_and_add_another') ?>" name="add-seller-another" class="btn btn-large btn-success" id="add-seller-another">
                            </div>
                        </div>
                    </div>
                    <?php echo form_close()?>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Add new seller end -->

<!-- Check seller store name-->
<script type="text/javascript">
    $('body').on('blur', '#store_name', function(){
        var store_name = $(this).val();
        $.ajax
        ({
            url: "<?php echo base_url('check_seller_store_name')?>",
            data: {store_name:store_name},
            type: "post",
            success: function(data)
            {
                if (data == 1) {
                    toastr.error('Store name already exists !', " ", {
                        "timeOut": "300",
                        "extendedTImeout": "300"
                    });  
                }else{
                    toastr.success(data, " ", {
                        "timeOut": "200",
                        "extendedTImeout": "200"
                    });  
                }
            } 
        });
    });
</script>



