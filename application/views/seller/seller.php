<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Manage seller Start -->
<div class="content-wrapper">
	<section class="content-header">
	    <div class="header-icon">
	        <i class="pe-7s-note2"></i>
	    </div>
	    <div class="header-title">
	        <h1><?php echo display('manage_seller') ?></h1>
	        <small><?php echo display('manage_seller') ?></small>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
	            <li><a href="#"><?php echo display('seller') ?></a></li>
	            <li class="active"><?php echo display('manage_seller') ?></li>
	        </ol>
	    </div>
	</section>

	<section class="content">
		<!-- Alert Message -->
	    <?php
	        $message = $this->session->userdata('message');
	        if (isset($message)) {
	    ?>
	    <div class="alert alert-info alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        <?php echo $message ?>                    
	    </div>
	    <?php 
	        $this->session->unset_userdata('message');
	        }
	        $error_message = $this->session->userdata('error_message');
	        if (isset($error_message)) {
	    ?>
	    <div class="alert alert-danger alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        <?php echo $error_message ?>                    
	    </div>
	    <?php 
	        $this->session->unset_userdata('error_message');
	        }
	    ?>

	    <?php if ($this->permission->check_label('add_seller')->access()) {?>
        <div class="row">
            <div class="col-sm-12">
                <div class="column">
                  	<a href="<?php echo base_url('cseller')?>" class="btn btn-success m-b-5 m-r-2"><i class="ti-plus"> </i> <?php echo display('add_seller')?></a>
                </div>
            </div>
        </div>
    	<?php } ?>

 		<!-- Seller filtering -->
        <div class="row">
			<div class="col-sm-12">
		        <div class="panel panel-default">
		            <div class="panel-body"> 

		            	<form action="<?php echo base_url('seller/manage_product/all/item')?>" method="get">
		     				<div class="row">
	                            <div class="col-sm-3">
	                                <div class="form-group row">
	                                    <label for="mobile" class="col-sm-3 col-form-label"><?php echo display('mobile')?></label>
	                                    <div class="col-sm-9">
	                                      	<input class="form-control" name ="mobile" id="mobile" type="text" placeholder="<?php echo display('mobile') ?>" value="<?php if(isset($_GET['mobile'])){ echo $_GET['mobile'];}?>">
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-3">
	                                <div class="form-group row">
	                                    <label for="email" class="col-sm-3 col-form-label"><?php echo display('email')?></label>
	                                    <div class="col-sm-9">
	                                      	<input class="form-control" name ="email" id="email" type="text" placeholder="<?php echo display('email') ?>" value="<?php if(isset($_GET['email'])){ echo $_GET['email'];}?>">
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-4">
	                                <div class="form-group row">
	                                    <label for="business_name" class="col-sm-5 col-form-label"><?php echo display('business_name')?></label>
	                                    <div class="col-sm-7">
	                                      	<input class="form-control" name ="business_name" id="business_name" type="text" placeholder="<?php echo display('business_name') ?>" value="<?php if(isset($_GET['business_name'])){ echo $_GET['business_name'];}?>">
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-2">
	                                <div class="form-group row">
	                                    <div class="col-sm-7">
	                                      	<button type="submit" class="btn btn-primary"><?php echo display('search')?></button>
	                                    </div>
	                                </div>
	                            </div>

	                        </div>  
		            	</form>   
			        </div>
		        </div>
		    </div>
		</div>

		<!-- Manage seller -->
		<div class="row">
		    <div class="col-sm-12">
		        <div class="panel panel-bd lobidrag">
		            <div class="panel-heading">
		                <div class="panel-title">
		                    <h4><?php echo display('manage_seller') ?></h4>
		                </div>
		            </div>
		            <div class="panel-body">
		                <div class="table-responsive">
		                    <table id="dataTableExample3" class="table table-bordered table-striped table-hover">
								<thead>
									<tr>
										<th><?php echo display('sl') ?></th>
										<th><?php echo display('seller_id') ?></th>
										<th><?php echo display('name') ?></th>
										<th><?php echo display('email') ?></th>
										<th><?php echo display('mobile') ?></th>
										<th><?php echo display('address') ?></th>
										<th><?php echo display('business_name') ?></th>
										<th><?php echo display('status') ?></th>
										<?php if ($this->permission->check_label('manage_seller')->access()) {?>
										<th style="text-align:center !Important"><?php echo display('action') ?></th>
										<?php }	?>
									</tr>
								</thead>
								<tbody>
								<?php 
								if ($sellers_list) { 
									foreach ($sellers_list as $seller) {
								?>
									<tr>
										<td><?php echo $seller['sl']?></td>
										<td><?php echo $seller['seller_id']?></td>
										<td><?php echo $seller['first_name']." ".$seller['last_name']?></td>
										<td><?php echo $seller['email']?></td>
										<td><?php echo $seller['mobile']?></td>
										<td><?php echo $seller['address']?></td>
										<td><?php echo $seller['business_name']?></td>
										<td>
										<?php 
										if ($seller['status'] == 0) {
											echo "<span class=\"label label-pill label-danger m-r-15\">".display('denied')."</span>";
										}elseif ($seller['status'] == 1) {
											echo "<span class=\"label label-pill label-success m-r-15\">".display('approved')."</span>";
										}elseif ($seller['status'] == 2) {
											echo "<span class=\"label label-pill label-warning m-r-15\">".display('pending')."</span>";
										}
										?>
										</td>
										<td>
											<center>
												<?php if ($this->permission->check_label('manage_seller')->update()->access()) {?>
												<a href="<?php echo base_url().'cseller/seller_update_form/item/'.$seller['id']; ?>" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="<?php echo display('update') ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
												<?php } if ($this->permission->check_label('manage_seller')->delete()->access()) {?>
												<a href="<?php echo base_url('cseller/seller_delete/item/'.$seller['id'])?>" class="btn btn-danger btn-sm" onclick="return confirm('<?php echo display('are_you_sure_want_to_delete')?>');" data-toggle="tooltip" data-placement="right" title="" data-original-title="<?php echo display('delete') ?> "><i class="fa fa-trash-o" aria-hidden="true"></i></a>
												<?php } ?>
											</center>
										</td>
									</tr>
								<?php } }?>
								</tbody>
		                    </table>
		                </div>
		                <div class="text-right">
		                	<?php echo $links?>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</section>
</div>
<!-- Manage seller End -->