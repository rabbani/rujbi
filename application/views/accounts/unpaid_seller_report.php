<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Payment Report Start -->
<div class="content-wrapper">
    <section class="content-header">
        <div class="header-icon">
            <i class="pe-7s-note2"></i>
        </div>
        <div class="header-title">
            <h1><?php echo display('payment_report') ?></h1>
            <small><?php echo display('payment_report') ?></small>
            <ol class="breadcrumb">
                <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
                <li><a href="#"><?php echo display('accounts') ?></a></li>
                <li class="active"><?php echo display('payment_report') ?></li>
            </ol>
        </div>
    </section>

    <section class="content">
        <!-- Alert Message -->
        <?php
        $message = $this->session->userdata('message');
        if (isset($message)) {
            ?>
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $message ?>
            </div>
            <?php
            $this->session->unset_userdata('message');
        }
        $error_message = $this->session->userdata('error_message');
        if (isset($error_message)) {
            ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $error_message ?>
            </div>
            <?php
            $this->session->unset_userdata('error_message');
        }
        ?>

        <div class="row">
            <div class="col-sm-12">
                <div class="column">

                    <a href="<?php echo base_url('add_payment')?>" class="btn btn-success m-b-5 m-r-2"><i class="ti-plus"> </i> <?php echo display('add_payment')?></a>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form action="<?php echo base_url('caccounts/unpaid_seller_report/all/item')?>" method="get">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group row">
                                        <label for="order_no" class="col-sm-4 col-form-label"><?php echo display('seller')?></label>
                                        <div class="col-sm-8">
                                            <select class="form-control" id="seller_id" name="seller_id">
                                                <option></option>
                                                <?php
                                                if ($seller_list) {
                                                    foreach ($seller_list as $seller) {
                                                        ?>
                                                        <option value="<?php echo $seller['seller_id']?>" <?php if(isset($_GET['seller_id'])){if ($seller['seller_id'] == $seller_id) {echo "selected";}}?>><?php echo $seller['first_name'].' '.$seller['last_name']?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group row">
                                        <div class="col-sm-7">
                                            <button type="submit" class="btn btn-primary"><?php echo display('search')?></button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Payment report -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4><?php echo display('payment_report') ?> </h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="dataTableExample3" class="table table-bordered table-striped table-hover">
                                <thead>
                                <tr>
                                    <th><?php echo display('sl') ?></th>
                                    <th><?php echo display('seller') ?></th>
                                    <th><?php echo display('total_sales_amount') ?></th>
                                    <th><?php echo display('total_comission') ?></th>
                                    <th><?php echo display('total_payable') ?></th>
                                    <th><?php echo display('total_paid') ?></th>
                                    <th><?php echo display('total_unpaid') ?></th>
                                    <th><?php echo display('action') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if ($payment_report) {
                                    foreach ($payment_report as $payment) {
                                        if (($payment['total_paid_amount'] == 0) && $payment['payable_balance'] > 0) {
                                            ?>
                                            <tr>
                                                <td><?php echo $payment['sl']?></td>
                                                <td><?php echo $payment['seller_name']?></td>
                                                <td>
                                                    <?php
                                                    echo (($position==0)?$currency.$payment['total_balance']:$payment['total_balance'].$currency);
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    echo (($position==0)?$currency.$payment['total_comission']:$payment['total_comission'].$currency);
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    echo (($position==0)?$currency.$payment['payable_balance']:$payment['payable_balance'].$currency);
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    echo (($position==0)?$currency.$payment['total_paid_amount']:$payment['total_paid_amount'].$currency);
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    echo (($position==0)?$currency.$payment['total_unpaid_amount']:$payment['total_unpaid_amount'].$currency);
                                                    ?>
                                                </td>
                                                <td>
                                                    <center>
                                                        <?php
                                                        if (!(($payment['total_paid_amount'] > 0) && ($payment['total_paid_amount'] >= $payment['payable_balance']))) {
                                                            ?>
                                                            <a href="<?php echo base_url('add_payment')?>" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="<?php echo display('pay_now') ?>"><?php echo display('pay_now') ?></a>
                                                            <?php
                                                        }
                                                        if (($payment['total_paid_amount'] > 0) && ($payment['total_paid_amount'] >= $payment['payable_balance'])) {
                                                            ?>
                                                            <a href="javascript:void(0)" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="<?php echo display('paid') ?>"><?php echo display('paid') ?></a>
                                                            <?php
                                                        }else if (($payment['total_paid_amount'] >  0) && ($payment['total_paid_amount'] <  $payment['total_balance'])) {
                                                            ?>
                                                            <a href="javascript:void(0)" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="<?php echo display('partial_paid') ?>"><?php echo display('partial_paid') ?></a>
                                                            <?php
                                                        }elseif (($payment['total_paid_amount'] == 0) && $payment['payable_balance'] > 0) {
                                                            ?>
                                                            <a href="javascript:void(0)" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="<?php echo display('unpaid') ?>"><?php echo display('unpaid') ?></a>
                                                            <?php
                                                        }
                                                        ?>
                                                    </center>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="2" class="text-right"><b><?php echo display('total_ammount') ?>:</b></td>
                                    <td><b><?php echo (($position==0)?"$currency {SubTotalBlance}":"{SubTotalBlance} $currency")?></b></td>
                                    <td><b><?php echo (($position==0)?"$currency {SubTotalComission}":"{SubTotalComission} $currency")?></b></td>
                                    <td><b><?php echo (($position==0)?"$currency {SubTotalPayBlance}":"{SubTotalPayBlance} $currency")?></b></td>
                                    <td><b><?php echo (($position==0)?"$currency {SubTotalPaidBlance}":"{SubTotalPaidBlance} $currency")?></b></td>
                                    <td colspan="2"><b><?php echo (($position==0)?"$currency {SubTotalUnPaidBlance}":"{SubTotalUnPaidBlance} $currency")?></b></td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Payment Report End -->