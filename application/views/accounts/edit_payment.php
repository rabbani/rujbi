<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Edit Payment start -->
<div class="content-wrapper">
    <section class="content-header">
        <div class="header-icon">
            <i class="pe-7s-note2"></i>
        </div>
        <div class="header-title">
            <h1><?php echo display('edit_payment') ?></h1>
            <small><?php echo display('edit_payment') ?></small>
            <ol class="breadcrumb">
                <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
                <li><a href="#"><?php echo display('accounts') ?></a></li>
                <li class="active"><?php echo display('edit_payment') ?></li>
            </ol>
        </div>
    </section>

    <section class="content">

        <!-- Alert Message -->
        <?php
            $message = $this->session->userdata('message');
            if (isset($message)) {
        ?>
        <div class="alert alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $message ?>                    
        </div>
        <?php 
            $this->session->unset_userdata('message');
            }
            $error_message = $this->session->userdata('error_message');
            if (isset($error_message)) {
        ?>
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $error_message ?>                    
        </div>
        <?php 
            $this->session->unset_userdata('error_message');
            }
        ?>

        <div class="row">
            <div class="col-sm-12">
                <div class="column">
                  
                    <a href="<?php echo base_url('manage_payment')?>" class="btn btn-success m-b-5 m-r-2"><i class="ti-align-justify"> </i> <?php echo display('manage_payment')?></a>

                </div>
            </div>
        </div>

        <!-- Edit payment -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4><?php echo display('edit_payment') ?> </h4>
                        </div>
                    </div>
                    <?php echo form_open_multipart('caccounts/update_payment/item/'.$id,array('class' => 'form-vertical', 'id' => 'validate'))?>
                    <div class="panel-body">
                        <div class="form-group row">
                            <label for="payment_date" class="col-sm-3 col-form-label"><?php echo display('payment_date') ?> <i class="text-danger">*</i></label>
                            <div class="col-sm-6">
                                <input class="form-control datepicker-here" type="text" name="payment_date" id="payment_date" required value="<?php echo $date; ?>" />
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="seller_id" class="col-sm-3 col-form-label"><?php echo display('seller') ?> <i class="text-danger">*</i></label>
                            <div class="col-sm-6">
                                <select name="seller_id" id="seller_id" class="form-control" required="">
                                    <option value=""></option>
                                    <?php
                                    if ($seller_list) {
                                        foreach ($seller_list as $seller) {
                                            print_r();
                                        ?>
                                        <option value="<?php echo $seller['seller_id']?>" <?php if ($seller['seller_id'] == $seller_id) {echo "selected";}?>><?php echo $seller['first_name'].' '.$seller['last_name']?></option>
                                        <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="amount" class="col-sm-3 col-form-label"><?php echo display('ammount') ?> <i class="text-danger">*</i></label>
                            <div class="col-sm-6">
                                <input type="number" id="amount" name="amount" required class="form-control" placeholder="<?php echo display('ammount') ?>" value="<?php echo $amount; ?>" />
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="description" class="col-sm-3 col-form-label"><?php echo display('description') ?> </label>
                            <div class="col-sm-6">
                                <textarea class="form-control" name="description" id="description" rows="3" placeholder="<?php echo display('description') ?>"><?php echo $description; ?></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-4 col-form-label"></label>
                            <div class="col-sm-6">
                                <input type="submit" id="update-payment" class="btn btn-success" name="add-payment" value="<?php echo display('update') ?>" />
                            </div>
                        </div>
                    </div>
                    <?php echo form_close()?>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Edit payment end -->