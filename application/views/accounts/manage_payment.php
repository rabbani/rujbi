<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Manage Payment Start -->
<div class="content-wrapper">
	<section class="content-header">
	    <div class="header-icon">
	        <i class="pe-7s-note2"></i>
	    </div>
	    <div class="header-title">
	        <h1><?php echo display('manage_payment') ?></h1>
	        <small><?php echo display('manage_payment') ?></small>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
	            <li><a href="#"><?php echo display('accounts') ?></a></li>
	            <li class="active"><?php echo display('manage_payment') ?></li>
	        </ol>
	    </div>
	</section>

	<section class="content">
		<!-- Alert Message -->
	    <?php
	        $message = $this->session->userdata('message');
	        if (isset($message)) {
	    ?>
	    <div class="alert alert-info alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        <?php echo $message ?>                    
	    </div>
	    <?php 
	        $this->session->unset_userdata('message');
	        }
	        $error_message = $this->session->userdata('error_message');
	        if (isset($error_message)) {
	    ?>
	    <div class="alert alert-danger alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        <?php echo $error_message ?>                    
	    </div>
	    <?php 
	        $this->session->unset_userdata('error_message');
	        }
	    ?>

	    <?php if($this->permission->check_label('add_payment')->create()->access()){?>
        <div class="row">
            <div class="col-sm-12">
                <div class="column">
                    <a href="<?php echo base_url('add_payment')?>" class="btn btn-success m-b-5 m-r-2"><i class="ti-plus"> </i> <?php echo display('add_payment')?></a>
                </div>
            </div>
        </div>
        <?php } ?>

		<div class="row">
			<div class="col-sm-12">
		        <div class="panel panel-default">
		            <div class="panel-body"> 
	                	<form action="<?php echo base_url('caccounts/manage_payment/all/item')?>" method="get">
		     				<div class="row">
	                            <div class="col-sm-4">
	                                <div class="form-group row">
	                                    <label for="order_no" class="col-sm-4 col-form-label"><?php echo display('seller')?></label>
	                                    <div class="col-sm-8">
	                                      	<select class="form-control" id="seller_id" name="seller_id">
	                                      		<option></option>
	                                      		<?php
	                                      		if ($seller_list) {
	                                      			foreach ($seller_list as $seller) {
	                                      		?>
	                                      		<option value="<?php echo $seller['seller_id']?>" <?php if(isset($_GET['seller_id'])){if ($seller['seller_id'] == $seller_id) {echo "selected";}}?>><?php echo $seller['first_name'].' '.$seller['last_name']?></option>
	                                      		<?php
	                                      			}
	                                      		}
	                                      		?>
	                                      	</select>
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-4">
	                                <div class="form-group row">
	                                    <label for="date" class="col-sm-4 col-form-label"><?php echo display('date')?></label>
	                                    <div class="col-sm-8">
	                                      	<input type="text" class="form-control datepicker-manage" id="date" data-range="true" data-multiple-dates-separator="---" data-language="en" name="date" placeholder="<?php echo display('date')?>" value="<?php if(isset($_GET['date'])){echo $_GET['date'];}?>">
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-2">
	                                <div class="form-group row">
	                                    <div class="col-sm-7">
	                                      	<button type="submit" class="btn btn-primary"><?php echo display('search')?></button>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>  
		            	</form>		            
		            </div>
		        </div>
		    </div>
	    </div>

		<!-- Payment report -->
		<div class="row">
		    <div class="col-sm-12">
		        <div class="panel panel-bd lobidrag">
		            <div class="panel-heading">
		                <div class="panel-title">
		                    <h4><?php echo display('payment_report') ?> </h4>
		                </div>
		            </div>
		            <div class="panel-body">
		                <div class="table-responsive">
		                    <table id="dataTableExample3" class="table table-bordered table-striped table-hover">
				                <thead>
									<tr>
										<th><?php echo display('sl') ?></th>
										<th><?php echo display('date') ?></th>
										<th><?php echo display('seller') ?></th>
										<th><?php echo display('description') ?></th>
										<th><?php echo display('ammount') ?></th>
										<th><?php echo display('action') ?></th>
									</tr>
								</thead>
								<tbody>
								<?php
								if ($payment_list) {
									foreach ($payment_list as $payment) {
								?>
									<tr>
										<td><?php echo $payment->id?></td>
										<td><?php echo $payment->date?></td>
										<td><?php echo $payment->first_name.' '.$payment->last_name?></td>
										<td><?php echo $payment->description?></td>
										<td><?php echo (($position==0)?$currency.$payment->amount:$payment->amount.$currency) ?></td>
										<td>
											<center>
												<?php if($this->permission->check_label('manage_payment')->update()->access()){?>
												<a href="<?php echo base_url().'caccounts/payment_update_form/item/'.$payment->id; ?>" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="<?php echo display('update') ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
												<?php }if($this->permission->check_label('manage_payment')->delete()->access()){ ?>
												<a href="<?php echo base_url('caccounts/payment_delete/item/'.$payment->id)?>" class="btn btn-danger btn-sm" onclick="return confirm('<?php echo display('are_you_sure_want_to_delete')?>');" data-toggle="tooltip" data-placement="right" data-original-title="<?php echo display('delete') ?> "><i class="fa fa-trash-o" aria-hidden="true"></i></a>
												<?php } ?>
											</center>
										</td>
									</tr>
								<?php
									}
								}
								?>
								</tbody>
								<tfoot>
									<tr>
										<td colspan="4" class="text-right"><b><?php echo display('total_payment_ammount') ?>:</b></td>
										<td><b><?php echo (($position==0)?"$currency {SubTotalAmnt}":"{SubTotalAmnt} $currency")?></b></td>
										<td></td>
									</tr>
								</tfoot>
		                    </table>
		                </div>
		                <div class="text-right"><?php echo $links?></div>
		            </div>
		        </div>
		    </div>
		</div>
	</section>
</div>
<!-- Manage Payment End -->