<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!--Edit shipping agent start -->
<div class="content-wrapper">
    <section class="content-header">
        <div class="header-icon">
            <i class="pe-7s-note2"></i>
        </div>
        <div class="header-title">
            <h1><?php echo display('shipping_agent_edit') ?></h1>
            <small><?php echo display('shipping_agent_edit') ?></small>
            <ol class="breadcrumb">
                <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
                <li><a href="#"><?php echo display('web_settings') ?></a></li>
                <li class="active"><?php echo display('shipping_agent_edit') ?></li>
            </ol>
        </div>
    </section>

    <section class="content">

        <!-- Alert Message -->
        <?php
            $message = $this->session->userdata('message');
            if (isset($message)) {
        ?>
        <div class="alert alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $message ?>                    
        </div>
        <?php 
            $this->session->unset_userdata('message');
            }
            $error_message = $this->session->userdata('error_message');
            if (isset($error_message)) {
        ?>
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $error_message ?>                    
        </div>
        <?php 
            $this->session->unset_userdata('error_message');
            }
        ?>

        <!--Edit shipping agent -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4><?php echo display('shipping_agent_edit') ?> </h4>
                        </div>
                    </div>
                    <?php echo form_open_multipart('cshipping_agent/shipping_agent_update/item/{agent_id}',array('class' => 'form-vertical', 'id' => 'validate'))?>
                    <div class="panel-body">

                        <div class="form-group row">
                            <label for="agent_name" class="col-sm-3 col-form-label"><?php echo display('agent_name')?> <i
                                        class="text-danger">*</i></label>
                            <div class="col-sm-6">
                                <input class="form-control" name ="agent_name" id="agent_name" type="text"
                                       placeholder="<?php echo display('agent_name') ?>"  required=""
                                       value="{agent_name}">
                            </div>
                        </div>                        


                        <div class="form-group row">
                            <label for="phone" class="col-sm-3 col-form-label"><?php echo display('phone')?> <i
                                        class="text-danger">*</i></label>
                            <div class="col-sm-6">
                                <input class="form-control" name ="phone" id="phone" type="text"
                                       placeholder="<?php echo display('phone') ?>"  required="" value="{agent_phone}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-sm-3 col-form-label"><?php echo display('phone')?> <i
                                        class="text-danger">*</i></label>
                            <div class="col-sm-6">
                                <input class="form-control" name ="email" id="email" type="email"
                                       placeholder="<?php echo display('email') ?>"  required="" value="{agent_email}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="address" class="col-sm-3 col-form-label"><?php echo display('address')?></label>
                            <div class="col-sm-6">
                                <textarea name="address" class="form-control" placeholder="<?php echo display
                                ('address')?>"
                                          id="address" row="3" >{agent_address}</textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="ammount" class="col-sm-3 col-form-label"><?php echo display('ammount')?> <i class="text-danger">*</i></label>
                            <div class="col-sm-6">
                                <input class="form-control" name ="amount" id="ammount" type="number" placeholder="<?php echo display('ammount') ?>"  required="" value="{amount}">
                            </div>
                        </div>  
                
                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-4 col-form-label"></label>
                            <div class="col-sm-6">
                                <input type="submit" class="btn btn-success btn-large" value="<?php echo display('update') ?>" />
                            </div>
                        </div>
                    </div>
                    <?php echo form_close()?>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Edit shipping agent end -->