<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Add new shipping agent start -->
<div class="content-wrapper">
    <section class="content-header">
        <div class="header-icon">
            <i class="pe-7s-note2"></i>
        </div>
        <div class="header-title">
            <h1><?php echo display('add_shipping_agent') ?></h1>
            <small><?php echo display('add_shipping_agent') ?></small>
            <ol class="breadcrumb">
                <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
                <li><a href="#"><?php echo display('web_settings') ?></a></li>
                <li class="active"><?php echo display('add_shipping_agent') ?></li>
            </ol>
        </div>
    </section>

    <section class="content">

        <!-- Alert Message -->
        <?php
            $message = $this->session->userdata('message');
            if (isset($message)) {
        ?>
        <div class="alert alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $message ?>                    
        </div>
        <?php 
            $this->session->unset_userdata('message');
            }
            $error_message = $this->session->userdata('error_message');
            $validatio_error = validation_errors();
            if (($error_message || $validatio_error)) {
        ?>
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $error_message ?>                    
            <?php echo $validatio_error ?>                    
        </div>
        <?php 
            $this->session->unset_userdata('error_message');
            }
        ?>

        <div class="row">
            <div class="col-sm-12">
                <div class="column">
                
                  <a href="<?php echo base_url('manage_shipping_agent')?>" class="btn btn-success m-b-5 m-r-2"><i class="ti-align-justify"> </i> <?php echo display('manage_shipping_agent')?></a>

                </div>
            </div>
        </div>

        <!-- New customer -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4><?php echo display('add_shipping_agent') ?> </h4>
                        </div>
                    </div>
                  <?php echo form_open_multipart('insert_shipping_agent', array('class' => 'form-vertical','id' => 'validate'))?>
                    <div class="panel-body">



                        <div class="form-group row">
                            <label for="agent_name" class="col-sm-3 col-form-label"><?php echo display('agent_name')?> <i class="text-danger">*</i></label>
                            <div class="col-sm-6">
                                <input class="form-control" name ="agent_name" id="agent_name" type="text"
                                       placeholder="<?php echo display('agent_name') ?>"  required="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-sm-3 col-form-label"><?php echo display('phone')?> <i
                                        class="text-danger">*</i></label>
                            <div class="col-sm-6">
                                <input class="form-control" name ="phone" id="phone" type="number"
                                       placeholder="<?php echo display('phone') ?>"  required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-sm-3 col-form-label"><?php echo display('email')?></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="email" id="email"  placeholder="<?php echo display('email')
                                ?>" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="address" class="col-sm-3 col-form-label"><?php echo display('address')?></label>
                            <div class="col-sm-6">
                                <textarea name="address" class="form-control" placeholder="<?php echo display
                                ('address')?>" id="address" row="3" ></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="amount" class="col-sm-3 col-form-label"><?php echo display('ammount')?> <i class="text-danger">*</i></label>
                            <div class="col-sm-6">
                                <input class="form-control" name ="amount" id="amount" type="number" placeholder="<?php echo display('ammount') ?>"  required="">
                            </div>
                        </div>  
                
                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-4 col-form-label"></label>
                            <div class="col-sm-6">
                                <input type="submit" id="add-shipping_agent" class="btn btn-success btn-large" name="add-shipping_agent" value="<?php echo display('save') ?>" />
                                <input type="submit" id="add-shipping_agent-another" class="btn btn-primary btn-large" name="add-shipping_agent-another" value="<?php echo display('save_and_add_another') ?>" />
                            </div>
                        </div>
                    </div>
                    <?php echo form_close()?>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Add new shipping agent end -->



