<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!--Order tracking start -->
<div class="content-wrapper">
	<section class="content-header">
	    <div class="header-icon">
	        <i class="pe-7s-note2"></i>
	    </div>
	    <div class="header-title">
	        <h1><?php echo display('order_tracking') ?></h1>
	        <small><?php echo display('order_tracking') ?></small>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
	            <li><a href="#"><?php echo display('order') ?></a></li>
	            <li class="active"><?php echo display('order_tracking') ?></li>
	        </ol>
	    </div>
	</section>

	<section class="content">
		<!-- Alert Message -->
	    <?php
	        $message = $this->session->userdata('message');
	        if (isset($message)) {
	    ?>
	    <div class="alert alert-info alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        <?php echo $message ?>                    
	    </div>
	    <?php 
	        $this->session->unset_userdata('message');
	        }
	        $error_message = $this->session->userdata('error_message');
	        if (isset($error_message)) {
	    ?>
	    <div class="alert alert-danger alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        <?php echo $error_message ?>                    
	    </div>
	    <?php 
	        $this->session->unset_userdata('error_message');
	        }
	    ?>

		<!-- Order filtering -->
        <div class="row">
			<div class="col-sm-12">
		        <div class="panel panel-default">
		            <div class="panel-body"> 

		            	<form action="<?php echo base_url('customer/order/order_tracking')?>" method="get">
		     				<div class="row">
	                            <div class="col-sm-3">
	                                <div class="form-group row">
	                                    <label for="order_no" class="col-sm-4 col-form-label"><?php echo display('order_no')?></label>
	                                    <div class="col-sm-8">
	                                      	<input class="form-control" name ="order_no" id="order_no" type="text" placeholder="<?php echo display('order_no') ?>" value="<?php if(isset($_GET['order_no'])){ echo $_GET['order_no'];}?>">
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-3">
	                                <div class="form-group row">
	                                    <label for="order_status" class="col-sm-5 col-form-label"><?php echo display('order_status')?> </label>
	                                    <div class="col-sm-7">
	                                      	<select class="form-control" id="order_status" name="order_status">
		                                       	<option value=""></option>
		                                       	<option value="1" <?php if (isset($_GET['order_status'])){if ($_GET['order_status'] == 1) {echo "selected";}}?>><?php echo display('pending')?></option>
		                                        <option value="2" <?php if (isset($_GET['order_status'])){if ($_GET['order_status'] == 2) {echo "selected";}}?>><?php echo display('processing')?></option>
		                                        <option value="3" <?php if (isset($_GET['order_status'])){if ($_GET['order_status'] == 3) {echo "selected";}}?>><?php echo display('shipping')?></option>
		                                        <option value="4" <?php if (isset($_GET['order_status'])){if ($_GET['order_status'] == 4) {echo "selected";}}?>><?php echo display('delivered')?></option>
		                                        <option value="5" <?php if (isset($_GET['order_status'])){if ($_GET['order_status'] == 5) {echo "selected";}}?>><?php echo display('returned')?></option>
		                                        <option value="6" <?php if (isset($_GET['order_status'])){if ($_GET['order_status'] == 6) {echo "selected";}}?>><?php echo display('cancel')?></option>
		                                    </select>
	                                    </div>
	                                </div>
	                            </div>

	                        
	                            <div class="col-sm-4">
	                                <div class="form-group row">
	                                    <label for="date" class="col-sm-4 col-form-label"><?php echo display('date')?></label>
	                                    <div class="col-sm-8">
	                                      	<input type="text" class="form-control datepicker-manage" id="date" data-range="true"  data-multiple-dates-separator="---" data-language='en' name="date" placeholder="<?php echo display('date') ?>" value="<?php if(isset($_GET['date'])){ echo $_GET['date'];}?>">
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-2">
	                                <div class="form-group row">
	                                    <div class="col-sm-7">
	                                      	<button type="submit" class="btn btn-primary"><?php echo display('search')?></button>
	                                    </div>
	                                </div>
	                            </div>

	                        </div>  
		            	</form>   
			        </div>
		        </div>
		    </div>
		</div>

		<!--Order tracking -->
		<div class="row">
		    <div class="col-sm-12">
		        <div class="panel panel-bd lobidrag">
		            <div class="panel-heading">
		                <div class="panel-title">
		                    <h4><?php echo display('order_tracking') ?></h4>
		                </div>
		            </div>
		            <div class="panel-body">
		                <div class="table-responsive">
		                    <table id="dataTableExample3" class="table table-bordered table-striped table-hover">
		                    	<thead>
									<tr>
										<th><?php echo display('sl') ?></th>
										<th><?php echo display('date') ?></th>
										<th><?php echo display('order_no') ?></th>
										<th><?php echo display('total_amount') ?></th>
										<th><?php echo display('status') ?></th>
									</tr>
								</thead>
								<tbody>
								<?php
								if ($order_list) {
									foreach ($order_list as $order) {
								?>
									<tr>
										<td><?php echo $order['sl']?></td>
										<td><?php echo $order['date']?></td>
										<td>
											<a href="<?php echo base_url().'customer/order/order_details_data/'.$order['order_id']; ?>" ><?php echo $order['id']?></a>
										</td>
										<td style="text-align: right;"><?php echo (($position==0)?$currency.' '.$order['total_amount']:$order['total_amount'].' '.$currency) ?></td>
										<td class="text-center">
										<?php 
										if ($order['order_status'] == 1) {
											echo "<span class=\"label label-warning m-r-15\">".display('order_placed')."</span>";
										}elseif ($order['order_status'] == 2) {
											echo "<span class=\"label label-primary m-r-15\">".display('order_is_processing')."</span>";
										}elseif ($order['order_status'] == 3) {
											echo "<span class=\"label label-info m-r-15\">".display('order_is_shipping')."</span>";
										}elseif ($order['order_status'] == 4) {
											echo "<span class=\"label label-success m-r-15\">".display('order_delivered')."</span>";
										}elseif ($order['order_status'] == 5) {
											echo "<span class=\"label label-warning m-r-15\">".display('order_returned')."</span>";
										}elseif ($order['order_status'] == 6) {
											echo "<span class=\"label label-danger m-r-15\">".display('order_canceled')."</span>";
										}
										?>

										<?php
											$order_traking = $this->Orders->order_traking_count_customer($order['order_id']);
											if ($order_traking > 0) {
											?>
                                            <a href="<?php echo base_url().'customer/order/order_traking/'.$order['order_id']; ?>" class="btn btn-xs btn-warning" data-toggle="tooltip" data-placement="left" title="<?php echo display('order_tracking') ?>"><?php echo $order_traking?></a>
                                        <?php
                                    	}
                                        ?>

                                        <a href="<?php echo base_url().'customer/order/order_traking/'.$order['order_id']; ?>" class="btn btn-xs btn-info" data-toggle="tooltip" data-placement="left" title="<?php echo display('order_tracking') ?>"><i class="fa fa-eye"></i></a>
										</td>
									</tr>
								<?php
									}
								}
								?>
								</tbody>
		                    </table>
		                </div>
		                <div class="text-right"><?php echo $links?></div>
		            </div>
		        </div>
		    </div>
		</div>
	</section>
</div>
<!--Order tracking end -->