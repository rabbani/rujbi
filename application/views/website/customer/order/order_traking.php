<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Order Tracking Start -->
<div class="content-wrapper">
    <section class="content-header">
        <div class="header-icon">
            <i class="pe-7s-note2"></i>
        </div>
        <div class="header-title">
            <h1><?php echo display('order')?></h1>
            <small><?php echo display('order_tracking')?></small>
            <ol class="breadcrumb">
                <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home')?></a></li>
                <li><a href="#"><?php echo display('order')?></a></li>
                <li class="active"><?php echo display('order_tracking')?></li>
            </ol>
        </div>
    </section>

    <section class="content">
        <!-- Alert Message -->
        <?php
            $message = $this->session->userdata('message');
            if (isset($message)) {
        ?>
        <div class="alert alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $message ?>                    
        </div>
        <?php 
            $this->session->unset_userdata('message');
            }
            $error_message = $this->session->userdata('error_message');
            if (isset($error_message)) {
        ?>
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $error_message ?>                    
        </div>
        <?php 
            $this->session->unset_userdata('error_message');
            }
        if (validation_errors()) {
        ?>
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo validation_errors(); ?>
        </div>
        <?php
            }
        ?>

        <?php
        if ($order_traking) {
        ?>
        <div class="row">
            <div class="col-sm-12" id="status-timeline" class="status-container">

                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title text-center">
                            <h4><?php echo display('order_no').' : '.$order_no?> </h4>
                        </div>
                    </div>
                </div>

                <?php
                $i=1;
                foreach ($order_traking as $order) {
                ?>
                <div class="status-timeline-block">
                    <div class="status-timeline-img status-picture"><?php echo $i?></div>
                    <div class="status-timeline-content">
                        <?php
                        if ($order->order_status == 1) {
                           echo '<h3>'.display('order_placed').'</h3>';
                        }elseif ($order->order_status == 2) {
                           echo '<h3>'.display('order_is_processing').'</h3>';
                        }elseif ($order->order_status == 3) {
                           echo '<h3>'.display('order_is_shipping').'</h3>';
                        }elseif ($order->order_status == 4) {
                           echo '<h3>'.display('order_delivered').'</h3>';
                        }elseif ($order->order_status == 5) {
                           echo '<h3>'.display('order_returned').'</h3>';
                        }elseif ($order->order_status == 6) {
                           echo '<h3>'.display('order_canceled').'</h3>';
                        }elseif ($order->order_status == 7) {
                           echo '<h3>'.display('message_send_to_customer').'<br>';
                           echo '<h4>'.strip_tags($order->message).'</h4>';
                        }elseif ($order->order_status == 8) {
                           echo '<h3>'.display('message_from_customer').'<br>';
                           echo '<h4>'.strip_tags($order->message).'</h4>';
                        }
                        ?>
                        <h5><?php echo $order->date?></h5>

                               <div>
                            <?php
                            if ($order->user_id) {
                                $user = $this->db->select('*')
                                        ->from('users')
                                        ->join('user_login','users.user_id = user_login.user_id')
                                        ->where('users.user_id',$order->user_id)
                                        ->get()
                                        ->row(); 

                                echo display('name').': '.$user->first_name." ".$user->last_name.'<br>';

                                $result = $this->db->select('*')
                                        ->from('sec_user_access_tbl')
                                        ->where('fk_user_id',$user->user_id)
                                        ->join('sec_role_tbl','sec_role_tbl.role_id = sec_user_access_tbl.fk_role_id')
                                        ->get()
                                        ->row();
                                if ($result) {
                                   echo display('role').': '.$result->role_name;
                                }else{
                                    echo display('role').': '.display('admin');
                                }
                                
                            }else{
                                $customer = $this->db->select('*')
                                        ->from('customer_information')
                                        ->where('customer_id',$order->customer_id)
                                        ->get()
                                        ->row();
                            ?>
                            <?php
                                echo $customer->customer_name;
                                echo "<br>".display('customer');
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <?php
                $i++;
                }
                ?>
            </div>
            <form action="<?php echo base_url('website/customer/corder/submit_message/'.$order_id)?>" method="post">
                <!-- Send email to customer -->
                <div class="row">
                    <div class="form-group row">
                        <label for="message" class="col-sm-4 col-form-label"><?php echo display('message') ?> <i class="text-danger">*</i></label>
                        <div class="col-sm-5">
                            <textarea class="form-control summernote" name="message" rows="6" placeholder="<?php echo display('message')?>" ></textarea>
                        </div>
                        <input type="hidden" name="customer_email" value="<?php echo $customer_email?>">
                    </div>
                </div>       

                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group row">
                            <div class="col-sm-6"></div>
                            <div class="col-sm-4">
                                <input type="submit" class="btn btn-success" value="<?php echo display('submit') ?>" />
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <?php
        }
        ?>
    </section>
</div>
<!-- Order Tracking End -->