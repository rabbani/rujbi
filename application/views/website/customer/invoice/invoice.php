<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Manage Invoice Start -->
<div class="content-wrapper">
	<section class="content-header">
	    <div class="header-icon">
	        <i class="pe-7s-note2"></i>
	    </div>
	    <div class="header-title">
	        <h1><?php echo display('manage_invoice') ?></h1>
	        <small><?php echo display('manage_your_invoice') ?></small>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
	            <li><a href="#"><?php echo display('invoice') ?></a></li>
	            <li class="active"><?php echo display('manage_invoice') ?></li>
	        </ol>
	    </div>
	</section>

	<section class="content">
		<!-- Alert Message -->
	    <?php
	        $message = $this->session->userdata('message');
	        if (isset($message)) {
	    ?>
	    <div class="alert alert-info alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        <?php echo $message ?>                    
	    </div>
	    <?php 
	        $this->session->unset_userdata('message');
	        }
	        $error_message = $this->session->userdata('error_message');
	        if (isset($error_message)) {
	    ?>
	    <div class="alert alert-danger alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        <?php echo $error_message ?>                    
	    </div>
	    <?php 
	        $this->session->unset_userdata('error_message');
	        }
	    ?>

	    <!-- Invoice filtering -->
        <div class="row">
			<div class="col-sm-12">
		        <div class="panel panel-default">
		            <div class="panel-body"> 

		            	<form action="<?php echo base_url('website/customer/cinvoice/manage_invoice')?>" method="get">
		     				<div class="row">

	                            <div class="col-sm-4">
	                                <div class="form-group row">
	                                    <label for="invoice_no" class="col-sm-4 col-form-label"><?php echo display('invoice_no')?></label>
	                                    <div class="col-sm-8">
	                                      	<input class="form-control" name ="invoice_no" id="invoice_no" type="text" placeholder="<?php echo display('invoice_no') ?>" value="<?php if(isset($_GET['invoice_no'])){ echo $_GET['invoice_no'];}?>">
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-4">
	                                <div class="form-group row">
	                                    <label for="date" class="col-sm-4 col-form-label"><?php echo display('date')?></label>
	                                    <div class="col-sm-8">
	                                      	<input type="text" class="form-control datepicker-here" id="date" data-range="true"  data-multiple-dates-separator="---" data-language='en' name="date" placeholder="<?php echo display('date') ?>" value="<?php if(isset($_GET['date'])){ echo $_GET['date'];}?>">
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-2">
	                                <div class="form-group row">
	                                    <div class="col-sm-8">
	                                      	<button type="submit" class="btn btn-primary"><?php echo display('search')?></button>
	                                    </div>
	                                </div>
	                            </div>

	                        </div>  
		            	</form>   
			        </div>
		        </div>
		    </div>
		</div>

		<!-- Manage Invoice report -->
		<div class="row">
		    <div class="col-sm-12">
		        <div class="panel panel-bd lobidrag">
		            <div class="panel-heading">
		                <div class="panel-title">
		                    <h4><?php echo display('manage_invoice') ?></h4>
		                </div>
		            </div>
		            <div class="panel-body">
		                <div class="table-responsive">
		                    <table id="dataTableExample3" class="table table-bordered table-striped table-hover">
		                    	<thead>
									<tr>
										<th><?php echo display('sl') ?></th>
										<th><?php echo display('invoice_no') ?></th>
										<th><?php echo display('date') ?></th>
										<th><?php echo display('total_amount') ?></th>
									</tr>
								</thead>
								<tbody>
								<?php
								if ($invoices_list) {
									foreach ($invoices_list as $invoice) {
								?>
									<tr>
										<td><?php echo $invoice['sl']?></td>
										<td><?php echo $invoice['invoice']?></td>
										<td><?php echo $invoice['final_date']?></td>
										<td style="text-align: right;"><?php echo (($position==0)?$currency.' '.$invoice['total_amount']:$invoice['total_amount'].' '.$currency) ?></td>
									</tr>
								<?php
									}
								}
								?>
								</tbody>
		                    </table>
		                </div>
		            	<div class="text-right"><?php echo $links?></div>
		            </div>

		        </div>
		    </div>
		</div>
	</section>
</div>
<!-- Manage Invoice End -->