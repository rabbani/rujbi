<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Admin Home Start -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="header-icon">
            <i class="pe-7s-world"></i>
        </div>
        <div class="header-title">
            <h1><?php echo display('dashboard')?></h1>
            <small><?php echo display('home')?></small>
            <ol class="breadcrumb">
                <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home')?></a></li>
                <li class="active"><?php echo display('dashboard')?></li>
            </ol>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Alert Message -->
        <?php
            $message = $this->session->userdata('message');
            if (isset($message)) {
            $this->session->unset_userdata('error_message');
        ?>
        <div class="alert alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $message ?>                    
        </div>
        <?php 
            $this->session->unset_userdata('message');
            }
            $error_message = $this->session->userdata('error_message');
            if (isset($error_message)) {
            $this->session->unset_userdata('message');
        ?>
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $error_message ?>                    
        </div>
        <?php 
            $this->session->unset_userdata('error_message');
            }
        ?>
        <!-- First Counter -->
        <div class="row">

            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
                <div class="panel panel-bd" style="height:100px">
                    <div class="panel-body">
                        <div class="statistic-box text-center">
                            <h2><span class="count-number"><?php echo $total_invoice?></span> <span class="slight"> </span></h2>
                            <div style="font-size:15px"><?php echo display('total_invoice')?></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
                <div class="panel panel-bd" style="height:100px">
                    <div class="panel-body">
                        <div class="statistic-box text-center">
                            <h2><span class="count-number"><?php echo $total_order?></span> <span class="slight"> </span></h2>
                            <div style="font-size:15px"><?php echo display('total_order')?></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section> <!-- /.content -->
</div> <!-- /.content-wrapper -->
<!-- Admin Home end -->
 
