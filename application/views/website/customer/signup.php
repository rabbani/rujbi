<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<div class="page-breadcrumbs">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('home')?>"><?php echo display('home')?></a></li>
            <li class="active"><?php echo display('sign_up')?></li>
        </ol>
    </div>
</div>
<!-- /.End of page breadcrumbs -->
<div class="form-content">

    <?php
        $message = $this->session->userdata('message');
        if (isset($message)) {
    ?>
       <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
          <strong><?php echo $message ?></strong>
        </div>
    <?php 
        $this->session->unset_userdata('message');
        }
        $error_message = $this->session->userdata('error_message');
        if (isset($error_message)) {
    ?>
      <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
        <strong><?php echo $error_message ?></strong>
      </div>
    <?php 
        $this->session->unset_userdata('error_message');
        }
    ?>

    <h2><?php echo display('sign_up')?></h2>
    <form class="login_content signup_content" action="<?php echo base_url('user_signup')?>" method="post" id="SignUpForm">
      <!--   <div class="social-btn text-center">
            <a href="#" class="btn btn-fb"><i class="fa fa-facebook"></i><?php echo display('facebook')?></a>
            <a href="#" class="btn btn-plush"><i class="fa fa-google-plus"></i><?php echo display('google_plus')?></a>
        </div>
        <div class="ui horizontal divider"><?php echo display('or')?> </div> -->
        
        <p><?php echo display('sign_up_using_your_email')?></p>
        <div class="form-group">
            <input class="form-control" name="first_name" id="first_name" placeholder="<?php echo display('first_name')?>" type="text" required>
        </div>
        <div class="form-group">
            <input class="form-control" name="last_name" id="last_name" placeholder="<?php echo display('last_name')?>" type="text" required>
        </div>
        <div class="form-group">
            <input class="form-control" type="email" name="email" placeholder="<?php echo display('email')?>">
        </div>
        <div class="form-group">
            <input class="form-control" type="password" name="password" placeholder="<?php echo display('password')?>" required>
        </div> 
        <div class="form-group">
            <input class="form-control" type="text" name="phone" placeholder="<?php echo display('phone')?>" required>
        </div>
        <button type="submit" class="btn btn-warning btn-block btn-submit"><?php echo display('create_account')?></button>
    </form>
</div>
<!-- /.End of page content -->