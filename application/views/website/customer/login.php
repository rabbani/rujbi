<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<div class="page-breadcrumbs">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('home')?>"><?php echo display('home')?></a></li>
            <li class="active"><?php echo display('login')?></li>
        </ol>
    </div>
</div>
<!-- /.End of page breadcrumbs -->
<div class="form-content">

    <?php
        $message = $this->session->userdata('message');
        if (isset($message)) {
    ?>
       <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
          <strong><?php echo $message ?></strong>
        </div>
    <?php 
        $this->session->unset_userdata('message');
        }
        $error_message = $this->session->userdata('error_message');
        if (isset($error_message)) {
    ?>
      <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
        <strong><?php echo $error_message ?></strong>
      </div>
    <?php 
        $this->session->unset_userdata('error_message');
        }
    ?>

    <h2><?php echo display('login')?></h2>

  <!--   <p><?php echo display('choose_one_of_the_following_methods')?></p>
    <div class="social-btn text-center">
        <?php if ( ! $this->facebook->is_authenticated()) : ?>
            <a href="<?php echo $this->facebook->login_url(); ?>" class="btn btn-fb"><i class="fa fa-facebook"></i><?php echo display('facebook')?></a>
        <?php endif ;?>
        <a href="<?php echo $this->googleplus->loginURL();?>" class="btn btn-plush"><i class="fa fa-google-plus"></i><?php echo display('google_plus')?></a>
    </div>
    <div class="ui horizontal divider"><?php echo display('or')?> </div> -->


    <p><?php echo display('sign_in_using_your_email')?></p>
    <form action="<?php echo base_url('do_login')?>" method="post">
        <div class="form-group">
            <input class="form-control" type="text" name="email" id="email" placeholder="<?php echo display('email_or_phone')?>" type="text" value="<?php echo get_cookie("email");?>">
        </div>
        <div class="form-group">
            <input class="form-control" name="password" id="password" placeholder="<?php echo display('password')?>" type="password" required value="<?php echo get_cookie("password");?>">
        </div>
        <div class="block-content">
            <div class="checkbox checkbox-success">
                <input id="remember1" type="checkbox" name="remember_me" value="1">
                <label for="remember1"><?php echo display('remember_me')?></label>
            </div>
            <a href="<?php echo base_url('recover_password')?>" class="forgot"><?php echo display('forgot_password')?></a>
        </div>
        <button type="submit" class="btn btn-warning btn-block btn-submit"><?php echo display('login')?> &#8702;</button>
        <div class="have-ac"><?php echo display('dont_have_an_account')?> <a href="<?php echo base_url('signup')?>"><?php echo display('sign_up')?></a></div>
    </form>
</div>
<!-- /.End of page content -->