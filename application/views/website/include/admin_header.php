<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<header class="main-header">
    <nav class="topBar hidden-xs">
        <div class="container">
            <ul class="list-inline pull-left">
                <li><span class="text-primary"><?php echo display('have_a_question')?> </span> <?php echo display('call')?> <?php echo (isset($mobile)) ? $mobile :display('none') ?></li>
            </ul>
            <ul class="topBarNav pull-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="false"><?php if($selected_currency_name){echo $selected_currency_icon;}?> <?php echo $selected_currency_name?><i class="fa fa-angle-down ml-5"></i>
                    </a>
                    <ul class="dropdown-menu w-100" role="menu">
                        <?php
                        $currency_new_id = $this->session->userdata('currency_new_id');
                        if ($currency_info) {
                            foreach ($currency_info as $currency_n) {
                        ?>
                        <li id="change_currency"><a href="#"><?php echo $currency_n->currency_icon;?> <?php echo $currency_n->currency_name?></a></li>
                        <input type="hidden" value="<?php echo $currency_n->currency_id?>">
                        <?php
                            }
                        }
                        ?>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="false"> <?php
                    if ($this->session->userdata('language') == 'english') {
                      echo "<span class=\"flag-icon flag-icon-gb mr-5\"></span>";
                    }elseif ($this->session->userdata('language') == 'bangla') {
                       echo "<span class=\"flag-icon flag-icon-bd mr-5\"></span>";
                    }
                    ?> <span> <?php echo ucfirst($this->session->userdata('language'));?> <i class="fa fa-angle-down ml-5"></i></span> </a>
                    <ul class="dropdown-menu w-100" role="menu">
                    <?php
                    if ($languages) {
                        foreach ($languages as $language) {
                    ?>
                        <li id="change_language"><a href="javascript:void(0)"><?php
                    if (lcfirst($language) == 'english') {
                      echo "<span class=\"flag-icon flag-icon-gb mr-5\"></span>";
                    }elseif (lcfirst($language) == 'bangla') {
                       echo "<span class=\"flag-icon flag-icon-bd mr-5\"></span>";
                    }
                    ?><?php echo $language?></a></li>
                        <input type="hidden" value="<?php echo $language?>">
                    <?php } }?>
                    </ul>
                </li>
                <li class="dropdown">
                    <?php
                    if ($this->seller_auth->is_logged()) {
                        $seller_id = $this->session->userdata('seller_id');
                        $seller_info = $this->db->select()
                                ->from('seller_information')
                                ->where('seller_id',$seller_id)
                                ->get()
                                ->row();
                    ?>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="false"> <span class=""><img src="<?php echo $seller_info->image;?>" height="15" width="16" style="margin: 5px"></span><span class="hidden-xs"><?php echo $seller_info->business_name;?>
                        <i class="fa fa-angle-down ml-5"></i></span> </a>
                    <ul class="dropdown-menu w-150" role="menu">
                        <li><a href="<?php echo base_url('seller-dashboard')?>" target="_blank"><?php echo display('dashboard')?></a></li>
                        <li><a href="<?php echo base_url('seller-logout')?>"><?php echo display('logout')?></a></li>
                    </ul>
                    <?php
                    }else{
                    ?>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="false"> <i class="fa fa-user mr-5"></i><span class="hidden-xs"><?php echo display('my_account'); ?><i class="fa fa-angle-down ml-5"></i></span> </a>
                    <ul class="dropdown-menu w-150" role="menu">
                        <li><a href="<?php echo base_url('seller-login')?>"><?php echo display('seller_login')?></a></li>
                        <li><a href="<?php echo base_url('seller-signup')?>"><?php echo display('seller_signup')?></a></li>
                    </ul>
                    <?php
                    }
                    ?>
                </li>
            </ul>
        </div>
    </nav>
    <!-- /.End of Top Bar -->
    <div class="middleBar">
        <div class="container">
            <div class="row display-table">
                <div class="col-xs-7 col-sm-3 col-md-2 col-lg-2 vertical-align text-left">
                    <div class="btnLogo-row">
                        <div class="sidebar-toggle-btn">
                            <button type="button" id="sidebarCollapse" class="btn">
                                <i class="lnr lnr-menu"></i>
                            </button>
                        </div>
                        <div class="header-logo">
                            <a href="<?php echo base_url()?>"> <img src="<?php echo (isset($logo)) ? base_url().$logo :"logo" ?>" class="img-responsive" alt=""></a>
                        </div>
                        <!-- /.End of Logo -->
                    </div>
                </div>
                <div class="col-sm-7 col-md-8 col-lg-7 vertical-align text-center hidden-xs">
                    <?php echo form_open('category_product_search','class="navbar-search"') ?>
                        <div class="input-group">
                            <div class="autocomplete">
                                <input type="text" id="product_name" class="form-control search-field product-search-field" dir="ltr" name="product_name" placeholder="<?php echo display('search_product_name_here')?>" required value="<?php echo $this->input->post('product_name')?>"/>
                                <div class="search_results" id="style-1"></div>
                            </div>
                            <div class="input-group-addon search-categories hidden-sm">
                                <select name="category_id" id="select_category" class="postform resizeselect" required="">
                                    <option value="all" selected="selected"><?php echo display("all_categories")?></option>
                                    <?php 
                                    if ($pro_category_list) { 
                                        foreach ($pro_category_list as $category) {
                                    ?>
                                    <option value="<?php echo $category['category_id']?>" <?php if ($category['category_id'] == $this->input->post('category_id')) {echo "selected";}?>><?php echo $category['category_name'];?></option>
                                    <?php 
                                        } 
                                    } 
                                    ?>
                                </select>
                                <select id="cat_select">
                                    <option id="cat_option"></option>
                                </select>
                            </div>
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-warning" ><span class="lnr lnr-magnifier"></span><?php echo display('search')?></button>
                            </div>
                        </div>
                    <?php echo form_close() ?>
                    <!-- /.End of Product Search Area -->
                </div>

                <div class="col-xs-5 col-sm-2 col-md-2 col-lg-3 vertical-align text-right">
                    <ul class="header-nav pull-right">
                        <li class="dropdown hnav-li" id="tab_up_cart">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="false"> <i class="flaticon-shopping-bag extra-icon"></i> 
                                <div class="nav-label">
                                    <span class="label-sub"><?php echo display('cart')?></span>
                                    <span class="icon-tips"><b class="bb-tri"></b><strong><?php echo $this->cart->total_items();?></strong></span>
                                    <i class="fa fa-angle-down ml-5"></i>
                                </div>
                            </a>
                            <?php
                            if ($this->cart->contents()) {
                            ?>
                            <ul class="dropdown-menu cart w-250 shopping-cart" role="menu">
                                <li class="shopping-cart-header">
                                    <i class="lnr lnr-cart cart-icon"></i><span class="badge"><?php echo $this->cart->total_items();?></span>
                                    <div class="shopping-cart-total">
                                        <span class="lighter-text"><?php echo display('total')?>:</span>
                                        <span class="main-color-text"><?php echo (($position==0)?$currency.number_format($this->cart->total(), 2, '.', ','):number_format($this->cart->total(), 2, '.', ',').$currency)?></span>
                                    </div>
                                </li>
                                <?php
                                foreach ($this->cart->contents() as $items):
                                ?>
                                <li class="clearfix">
                                    <img src="<?php echo base_url().$items['options']['image'];?>" alt="item1" />
                                    <span class="item-name"><?php echo $items['name']; ?></span>
                                    <span class="item-price"><?php echo (($position==0)?$currency.' '.$this->cart->format_number($items['price']):$this->cart->format_number($items['price']).' '.$currency) ?></span>
                                    <span class="item-quantity">Quantity: <?php echo $items['qty']; ?></span>
                                </li>
                                <?php
                                endforeach; 
                                ?>
                                <li class="text-center">
                                    <a href="<?php echo base_url('view_cart')?>" class="shopping-cart-btn"><?php echo display('view_cart')?></a>
                                    <a href="<?php echo base_url('checkout')?>" class="shopping-cart-btn"><?php echo display('checkout')?></a>
                                </li>
                            </ul>
                            <!--/. End of shopping cart -->
                            <?php
                            }else{
                            ?>
                            <ul class="dropdown-menu cart w-250 shopping-cart" role="menu">
                                 <li class="clearfix">
                                    <?php echo display('you_have').' '.$this->cart->total_items().' '.display('items_in_your_cart')?>
                                </li>
                            </ul>
                            <?php
                            }
                            ?>
                        </li>
                        <li class="dropdown hnav-li hidden-xs">
                            <?php
                            if ($this->user_auth->is_logged()) {
                                $customer_id = $this->session->userdata('customer_id');
                                $cus_info = $this->db->select('*')
                                        ->from('customer_information')
                                        ->where('customer_id',$customer_id)
                                        ->get()
                                        ->row();
                            ?>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="false"> <span class=""><img src="<?php echo $cus_info->image;?>" height="30" width="30"></span></i>
                                <div class="nav-label">
                                    <?php echo $cus_info->customer_name;?>
                                    <i class="fa fa-angle-down ml-5"></i>
                                </div>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo base_url('customer_dashboard')?>" target="_blank"><?php echo display('dashboard')?></a></li>
                                <li><a href="<?php echo base_url('website/customer/login/logout')?>"><?php echo display('logout')?></a></li>
                            </ul>
                            <?php
                            }else{
                            ?>
                           <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="false"> <i class="flaticon-user extra-icon"></i>
                                <div class="nav-label">
                                    <span class="label-sup"><?php echo display('your')?></span>
                                    <span class="label-sub"><?php echo display('account')?></span>
                                    <i class="fa fa-angle-down ml-5"></i>
                                </div>
                            </a>
                            <ul class="dropdown-menu user-register w-260" role="menu">
                                <li>
                                    <form action="<?php echo base_url('do_login')?>" method="post">
                                        <div class="user-info text-center">
                                            <h2><?php echo display('login')?></h2>
                                            <!-- <p><?php echo display('choose_one_of_the_following_methods')?></p>
                                            <div class="social-btn text-center">
                                                <a href="#" class="btn btn-fb"><i class="fa fa-facebook"></i><?php echo display('facebook')?></a>
                                                <a href="<?php echo $this->googleplus->loginURL();?>" class="btn btn-plush"><i class="fa fa-google-plus"></i><?php echo display('google_plus')?></a>
                                            </div>
                                            <div class="ui horizontal divider"><?php echo display('or')?> </div> -->
                                            <p><?php echo display('sign_in_using_your_email')?></p>
                                            <div class="form-group">
                                                <input class="form-control" name="email" id="email" placeholder="<?php echo display('email_or_phone')?>" type="text" required value="<?php echo get_cookie("email");?>">
                                            </div>
                                            <div class="form-group">
                                                <input class="form-control" name="password" id="password" placeholder="<?php echo display('password')?>" type="password" required value="<?php echo get_cookie("password");?>">
                                            </div>
                                            <div class="block-content">
                                                <div class="checkbox checkbox-success">
                                                    <input id="remember" type="checkbox" name="remember_me" value="1">
                                                    <label for="remember"><?php echo display('remember_me')?></label>
                                                </div>
                                                <a href="<?php echo base_url('recover_password')?>" class="forgot"><?php echo display('forgot_password')?></a>
                                            </div>
                                            <button type="submit" class="btn btn-warning btn-block"><?php echo display('login')?> &#8702;</button>
                                            <div class="have-ac"><?php echo display('dont_have_an_account')?> <a href="<?php echo base_url('signup')?>"><?php echo display('sign_up')?></a></div>
                                        </div>
                                    </form>
                                    <!-- /.End of Login -->
                                </li>
                            </ul>
                            <?php
                            }
                            ?>
                        </li>
                    </ul><!--  /.End of Header Nav -->
                </div>
            </div>

            <div class="row mobile-search">
                <div class="col-xs-12">
                    <form action="<?php echo base_url('search_category_product')?>">
                        <div class="input-group mobile-search-input">
                            <input type="text" class="form-control" placeholder="<?php echo display('search_product_name_here')?>" name="product_name" value="<?php echo $this->input->get('product_name')?>">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                            </span>
                        </div><!-- /input-group -->
                    </form>
                </div>
            </div>

            <!-- /. End of mobile search area -->
        </div>
    </div>
    <!-- /.End of Middel Top Bar -->
    <?php
    if ($this->uri->segment(1) != 'home') {
    ?>
    <div class="main-nav">
        <div class="container">
            <nav class="navbar">
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <?php
                        if ($top_category_list) {
                            $i=0;
                            foreach ($category_list as $parent_category) {
                                if ($i == 7)break;
                            $sub_parent_cat =  $this->db->select('*')
                                                    ->from('product_category')
                                                    ->where('parent_category_id',$parent_category->category_id)
                                                    ->order_by('menu_pos')
                                                    ->get()
                                                    ->result(); 
                        ?>
                        <li class="<?php if($sub_parent_cat){echo("dropdown mega-dropdown");}?>">
                            <a href="<?php echo base_url(remove_space($parent_category->category_name).'/'.$parent_category->category_id)?>" class="dropdown-toggle text-capitalize" data-toggle="<?php if($sub_parent_cat){ echo "dropdown";}?>"><?php echo $parent_category->category_name?> <?php if($sub_parent_cat){ ?><span class="fa fa-angle-down ml-5 pull-right"></span><?php } ?></a>
                            <?php
                            if ($sub_parent_cat){
                            ?>
                            <ul class="dropdown-menu mega-dropdown-menu dropdown-menu-bg row">
                                <?php  foreach ($sub_parent_cat as $parent_cat) { ?>
                                <li class="col-sm-3">
                                    <ul>
                                        <li class="dropdown-header"><a href="<?php echo base_url(remove_space($parent_cat->category_name).'/'.$parent_cat->category_id)?>"><?php echo $parent_cat->category_name?> </a></li>
                                        <?php 
                                        $sub_cat =  $this->db->select('*')
                                                        ->from('product_category')
                                                        ->where('parent_category_id',$parent_cat->category_id)
                                                        ->order_by('menu_pos')
                                                        ->get()
                                                        ->result(); 
                                        if ($sub_cat) {
                                            foreach ($sub_cat as $s_p_cat) {
                                        ?>
                                        <li><a href="<?php echo base_url(remove_space($s_p_cat->category_name).'/'.$s_p_cat->category_id)?>"><i class="fa fa-angle-right"></i> <?php echo $s_p_cat->category_name?></a></li>
                                          <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </li>
                                <?php } ?>
                                </li>
                            </ul>
                            <?php } ?>
                        </li>
                            <?php
                            $i++;
                            }
                        }
                        ?>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
    <?php
    }
    ?>
</header>

<!-- Loader image for product search -->
<style type="text/css">
    input.loading {
        background: #fff url(<?php echo base_url('assets/website/img/loader.gif')?>) no-repeat center !important;
    }
</style>

<!-- Some ajax code  -->
<script type="text/javascript">
    //Product search by product name
    $('body').on('keyup', '#product_name', function() {
        var product_name = $('#product_name').val();
        var category_id  = $('#select_category').val();

        //Product name check
        if (product_name == 0) {
            $('.search_results').css("display", "none");
            return false;
        }
        
        $.ajax({
            type: "post",
            async: false,
            url: '<?php echo base_url('website/Category/category_product_search_ajax')?>',
            data: {product_name: product_name,category_id:category_id},
            beforeSend: function(){
                $('#product_name').addClass('loading');
            },
            success: function(data) {
                $('.search_results').css("display", "block");
                $('#product_name').removeClass('loading');
                if (data) {
                    $('.search_results').html(data);
                }
            },
            error: function() {
                alert('Request Failed, Please check your code and try again!');
            }
        });
    });

    //Change currency ajax
    $('body').on('click', '#change_currency', function() {
        var currency_id  = $(this).next().val();
        $.ajax({
            type: "post",
            async: true,
            url: '<?php echo base_url('website/Home/change_currency')?>',
            data: {currency_id:currency_id},
            success: function(data) {
                if (data == 2) {
                    location.reload();
                }else{
                    location.reload();
                }
            },
            error: function() {
                alert('Request Failed, Please check your code and try again!');
            }
        });
    }); 
      
    //Change language ajax
    $('body').on('click', '#change_language', function() {
        var language  = $(this).next().val();
        $.ajax({
            type: "post",
            async: true,
            url: '<?php echo base_url('website/Home/change_language')?>',
            data: {language:language},
            success: function(data) {
                if (data == 2) {
                    location.reload();
                }else{
                    location.reload();
                }
            },
            error: function() {
                alert('Request Failed, Please check your code and try again!');
            }
        });
    }); 
</script>