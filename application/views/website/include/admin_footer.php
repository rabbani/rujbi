<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<footer class="big-footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-3">
                <div class="footer-box">
                    <div class="footer-logo">
                        <a href="<?php echo base_url()?>"><img src="<?php echo (isset($logo)) ? $logo :"logo" ?>" class="img-responsive" alt=""></a>
                    </div>
                </div>
            </div>
            <?php
            if ($footer_block) {
                foreach ($footer_block as $footer) {
                    echo $footer->details;
                }
            }
            ?>
        </div>
        <?php
        if ($category_list) {
        ?>
        <div class="row">
            <div class="col-sm-12">
                <div class="popular-search">
                    <div class="footer-title2">
                        <hr>
                    </div>
                    <div class="serch-word">
                        <?php
                        foreach ($category_list as $category) {
                        ?>
                        <a class="text-capitalize" href="<?php echo base_url(remove_space($category->category_name).'/'.$category->category_id)?>" target="_blank"><?php echo $category->category_name?></a>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
        }
        ?>
        <div class="app-download">
            <div class="row">
                <div class="col-sm-12">
                    <div class="footer-title2">
                        <hr>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="app-text">
                        <?php echo $web_settings[0]['footer_text']?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="apps pull-right">
                        <p><?php echo display('download_our_app')?></p>
                        <a href="<?php echo $web_settings[0]['app_url']?>" target="_blank"><img src="<?php echo $web_settings[0]['app_logo']?>" class="img-responsive" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- /.End of Footer -->
<footer class="mobile-footer">
    <div class="container">
        <div class="row">
            <ul class="footer-menu list-unstyled text-center">
                <li><a href="#">Help</a></li>
                <li><a href="#">Purchase Protection</a></li>
                <li><a href="#">Privacy Policy </a></li>
                <li><a href="#">Contact Information</a></li>
                <li><a href="#">Return Policy</a></li>
            </ul>
            <span class="copyright"><?php echo (isset($footer_text)) ? $footer_text :"Footer Text" ?></span>
        </div>
    </div>
</footer>
<!-- /.End of mobile footer -->