<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title><?php echo (isset($title)) ? $title :"Rujbi" ?></title>



        <meta property="og:title" content="<?php echo @$product_name?>" />
        <meta property="og:description" content='<?php echo @$product_description?>' />
        <meta property="og:url" content="<?php echo base_url().@$category_name.'/'.@$product_name.'/'.
            @$product_id?>" />
        <meta property="og:site_name" content="<?php echo base_url();?>" />
        <meta property="og:image" content="<?php echo base_url().@$product_image?>"/>
        <meta property="og:image:width" content="100" />
        <meta property="og:image:height" content="100" />



        <link rel="shortcut icon" href="<?php echo (isset($favicon)) ? $favicon :"Rujibi" ?>">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
        <link href="<?php echo base_url('assets/website/css/bootstrap.min.css')?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/website/plugins/animate/animate.min.css')?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/website/plugins/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/website/plugins/flag-icon/css/flag-icon.min.css')?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/website/plugins/linearicons/linearicons.min.css')?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/website/plugins/metismenu/metisMenu.min.css')?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/website/plugins/malihu-scrollbar/jquery.mCustomScrollbar.min.css')?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/website/plugins/PhotoSwipe/photoswipe.min.css')?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/website/plugins/PhotoSwipe/default-skin/default-skin.min.css')?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/website/plugins/slick/slick.min.css')?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/website/plugins/slick/slick-theme.min.css')?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/website/plugins/magnific-Popup/magnific-popup.min.css')?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/website/plugins/star-rating/star-rating.min.css')?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/website/plugins/ion.rangeSlider/css/ion.rangeSlider.min.css')?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/website/plugins/ion.rangeSlider/css/ion.rangeSlider.skinFlat.min.css')?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/website/css/style.css')?>" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/css/cmxform.css" rel="stylesheet" type="text/css"/>
        <script src="<?php echo base_url('assets/website/js/jquery-3.3.1.min.js')?>"></script>
        <script src="<?php echo base_url()?>assets/js/jquery.validate.min.js" type="text/javascript"></script>
    </head>

    <body>

        <div class="<?php if($this->uri->segment('1') == 'home')echo("page-wrapper");?>">
            <nav id="sidebar">
                <div id="dismiss">
                    <i class="glyphicon glyphicon-arrow-left"></i>
                </div>
                <ul class="metismenu list-unstyled" id="mobile-menu">
                    <?php
                    if ($category_list) {
                        foreach ($category_list as $parent_category) {
                        $sub_parent_cat =  $this->db->select('*')
                                                ->from('product_category')
                                                ->where('parent_category_id',$parent_category->category_id)
                                                ->order_by('menu_pos')
                                                ->get()
                                                ->result(); 
                    ?>
                    <li class="">
                        <a href="#" aria-expanded="false"><?php echo $parent_category->category_name?><?php if($sub_parent_cat){echo "<span class=\"fa arrow\"></span>";} ?></a>
                        <?php
                        if ($sub_parent_cat){
                        ?>
                        <ul aria-expanded="false">
                            <?php  foreach ($sub_parent_cat as $parent_cat) { 
                                $sub_cat =  $this->db->select('*')
                                                    ->from('product_category')
                                                    ->where('parent_category_id',$parent_cat->category_id)
                                                    ->order_by('menu_pos')
                                                    ->get()
                                                    ->result();
                            ?>
                            <li>
                                <a href="#" aria-expanded="false"><?php echo $parent_cat->category_name?><?php if($sub_cat){echo "<span class=\"fa arrow\"></span>";} ?></a>
                                <ul aria-expanded="false">
                                    <?php
                                    if ($sub_cat) {
                                        foreach ($sub_cat as $s_p_cat) {
                                    ?>
                                    <li><a href="<?php echo base_url('category/'.$s_p_cat->category_id)?>"><?php echo $s_p_cat->category_name?></a></li>
                                    <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </li>
                            <?php } ?>
                        </ul>
                        <?php
                        }
                        ?>
                    </li>
                    <?php
                        }
                    }
                    ?>
                </ul>
            </nav>
            <!-- /.End of mobile menu side bar -->
            <div class="overlay"></div>
            <!-- /.End of promotion banner -->
            <?php $this->load->view('website/include/admin_header'); ?>
                {content}
            <?php $this->load->view('website/include/admin_footer'); ?>
        </div>
        
        <script src="<?php echo base_url('assets/website/js/bootstrap.min.js')?>"></script>
        <script src="<?php echo base_url('assets/website/plugins/metismenu/metisMenu.min.js')?>"></script>
        <script src="<?php echo base_url('assets/website/plugins/malihu-scrollbar/jquery.mCustomScrollbar.concat.min.js')?>"></script>
        <script src="<?php echo base_url('assets/website/plugins/slick/slick.min.js')?>"></script>
        <script src="<?php echo base_url('assets/website/plugins/elevatezoom/jquery.elevateZoom-3.0.8.min.js')?>"></script>
        <script src="<?php echo base_url('assets/website/plugins/PhotoSwipe/photoswipe.min.js')?>"></script>
        <script src="<?php echo base_url('assets/website/plugins/PhotoSwipe/photoswipe-ui-default.min.js')?>"></script>
        <script src="<?php echo base_url('assets/website/plugins/magnific-Popup/jquery.magnific-popup.min.js')?>"></script>
        <script src="<?php echo base_url('assets/website/plugins/star-rating/star-rating.min.js')?>"></script>
        <script src="<?php echo base_url('assets/website/plugins/theia-sticky-sidebar/ResizeSensor.min.js')?>"></script>
        <script src="<?php echo base_url('assets/website/plugins/theia-sticky-sidebar/theia-sticky-sidebar.min.js')?>"></script>
        <script src="<?php echo base_url('assets/website/plugins/ion.rangeSlider/js/ion.rangeSlider.min.js')?>"></script>
        <script src="<?php echo base_url('assets/website/plugins/jQuery-slimScroll/jquery.slimscroll.min.js')?>"></script>
        <script src="<?php echo base_url('assets/website/plugins/prognroll/prognroll.min.js')?>"></script>
        <script src="<?php echo base_url('assets/website/js/jquery.simpleSocialShare.min.js')?>"></script>
        <script src="<?php echo base_url('assets/website/js/script.js')?>"></script>
        <!-- Subscriber entry by ajax,jquery -->
        <script type="text/javascript">
            // Validate
            $("#validate").validate();
            //Simple share
            $('.share-button').simpleSocialShare();

           //Add to cart by ajax
            function add_to_cart(product_id){
                
                if (product_id == 0) {
                    alert('<?php echo display('please_select_product')?>');
                    return false;
                }

                $.ajax({
                    type: "post",
                    async: true,
                    url: '<?php echo base_url('website/Home/add_to_cart')?>',
                    data: {product_id:product_id},
                    success: function(data) {
                        if (data == 2) {
                            alert('out of stock !');
                        }else if(data == 3){
                            alert('Something is wrong !');
                        }else{
                            alert('Product added to cart.');
                            $("#tab_up_cart").load(location.href+" #tab_up_cart>*","");
                        }
                    },
                    error: function() {
                        alert('Request Failed, Please check your code and try again!');
                    }
                });
            }
        </script>

    </body>

</html>