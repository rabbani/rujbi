<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<div class="page-breadcrumbs">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url()?>"><?php echo display('home')?></a></li>
            <li><a href="javascript:void(0)"><?php echo display('seller_product')?></a></li>
            <li class="active"><?php echo $seller_name?></li>
        </ol>
    </div>
</div>
<!-- /.End of page breadcrumbs -->
<div class="category-content hidden-xs">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 leftSidebar hidden-xs">
                <div class="sidebar-widget">
                    <div class="sidebar-widget-title">
                        <?php if ($max_value) { ?>
                        <div class="price-Pips">
                            <form action="" method="post" id="priceForm">
                                <h3 class="sidebar-title"><?php echo display('by_price')?></h3>
                                <input type="text" class="price-range" value="price-range" name="price-range" />
                            </form>
                        </div>
                        <?php } if ($variant_list) {
                        ?>
                        <div class="product-size">
                            <h3 class="sidebar-title"><?php echo display('by_variant')?>:</h3>
                            <?php
                            if ($variant_list) {
                                $i=1;
                                foreach ($variant_list as $variant) {
                            ?>
                            <input type="radio" class="size1" name="size" id="<?php echo $i?>" value="<?php
                                $currentURL = current_url();
                                $params = $_SERVER['QUERY_STRING'];
                                $size = $this->input->get('size');
                                if ($params) {
                                    if ($size) {
                                        $new_param = str_replace("size=".$size, "size=".$variant['variant_id'] ,$params);
                                        echo $fullURL = $currentURL . '?' .$new_param;
                                    }else{
                                        echo $fullURL = $currentURL . '?' .$params.'&size='.$variant['variant_id'];
                                    }
                                }else{
                                    echo $fullURL = $currentURL.'?size='.$variant['variant_id'];
                                }
                            ?>" <?php if ($size == $variant['variant_id']) {echo "checked"; }?>/>
                            <label for="<?php echo $i?>"><span class="size"><?php echo $variant['variant_name']?></span></label>
                            <?php
                            $i++;
                                }
                            }
                            ?>
                        </div>
                        <!--  /.End of product color selector -->
                        <?php }                     
                        $lang_id = 0;
                        $user_lang = $this->session->userdata('language');
                        if (empty($user_lang)) {
                            $lang_id = 'english';
                        }else{
                            $lang_id = $user_lang;
                        }
                        ?>
                        <div class="sidebar-review">
                            <h3 class="sidebar-title"><?php echo display('rating')?>:</h3>
                            <div class="checkbox checkbox-success">
                                <input id="rating1" type="checkbox" class="check_value" value="<?php
                                $currentURL = current_url();
                                $params = $_SERVER['QUERY_STRING'];
                                $rate = $this->input->get('rate');
                                if ($params) {
                                    if ($rate) {
                                        $new_param = str_replace("rate=".$rate, "rate=4-5" ,$params);
                                        echo $fullURL = $currentURL . '?' .$new_param;
                                    }else{
                                        echo $fullURL = $currentURL . '?' .$params.'&rate=4-5';
                                    }
                                }else{
                                    echo $fullURL = $currentURL.'?rate=4-5';
                                }
                            ?>" <?php if ($this->input->get('rate') == '4-5'){ echo("checked"); }?>>
                                <label for="rating1"> 
                                    <span class="product-rating">
                                        <span class="star-rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                        <a href="javascript:void(0)" class="review-link">
                                            <span class="count">(4-5)</span>
                                        </a>
                                    </span>
                                </label>
                            </div>
                            <div class="checkbox checkbox-success">
                                <input id="rating2" type="checkbox" class="check_value" value="<?php
                                $currentURL = current_url();
                                $params = $_SERVER['QUERY_STRING'];
                                $rate = $this->input->get('rate');
                                if ($params) {
                                    if ($rate) {
                                        $new_param = str_replace("rate=".$rate, "rate=3-5" ,$params);
                                        echo $fullURL = $currentURL . '?' .$new_param;
                                    }else{
                                        echo $fullURL = $currentURL . '?' .$params.'&rate=3-5';
                                    }
                                }else{
                                    echo $fullURL = $currentURL.'?rate=3-5';
                                }
                            ?>" <?php if ($this->input->get('rate') == '3-5'){ echo("checked");}?>>
                                <label for="rating2"> 
                                    <span class="product-rating">
                                        <span class="star-rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                        <a href="javascript:void(0)" class="review-link">
                                            <span class="count">(3-5)</span>
                                        </a>
                                    </span>
                                </label>
                            </div>
                            <div class="checkbox checkbox-success">
                                <input id="rating3" type="checkbox" class="check_value" value="<?php
                                $currentURL = current_url();
                                $params = $_SERVER['QUERY_STRING'];
                                $rate = $this->input->get('rate');
                                if ($params) {
                                    if ($rate) {
                                        $new_param = str_replace("rate=".$rate, "rate=2-5" ,$params);
                                        echo $fullURL = $currentURL . '?' .$new_param;
                                    }else{
                                        echo $fullURL = $currentURL . '?' .$params.'&rate=2-5';
                                    }
                                }else{
                                    echo $fullURL = $currentURL.'?rate=2-5';
                                }
                            ?>" <?php if ($this->input->get('rate') == '2-5') {echo("checked");}?>>
                                <label for="rating3"> 
                                    <span class="product-rating">
                                        <span class="star-rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                        <a href="javascript:void(0)" class="review-link">
                                            <span class="count">(2-5)</span>
                                        </a>
                                    </span>
                                </label>
                            </div>
                            <div class="checkbox checkbox-success">
                                <input id="rating4" type="checkbox" class="check_value" value="<?php
                                $currentURL = current_url();
                                $params = $_SERVER['QUERY_STRING'];
                                $rate = $this->input->get('rate');
                                if ($params) {
                                    if ($rate) {
                                        $new_param = str_replace("rate=".$rate, "rate=1-5" ,$params);
                                        echo $fullURL = $currentURL . '?' .$new_param;
                                    }else{
                                        echo $fullURL = $currentURL . '?' .$params.'&rate=1-5';
                                    }
                                }else{
                                    echo $fullURL = $currentURL.'?rate=1-5';
                                }
                            ?>" <?php if ($this->input->get('rate') == '1-5') {echo("checked");}?>>
                                <label for="rating4"> 
                                    <span class="product-rating">
                                        <span class="star-rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                        <a href="javascript:void(0)" class="review-link">
                                            <span class="count">(1-5)</span>
                                        </a>
                                    </span>
                                </label>
                            </div>
                        </div>
                        <!--  /.End of review ratiing -->
                        <div class="sidebar-review">
                            <h3 class="sidebar-title"><?php echo display('seller_score')?>:</h3>
                            <div class="checkbox checkbox-success">
                                <input id="rating5" type="checkbox" class="check_value" value="<?php
                                $currentURL = current_url();
                                $params = $_SERVER['QUERY_STRING'];
                                $seller_score = $this->input->get('seller_score');
                                if ($params) {
                                    if ($seller_score) {
                                        $new_param = str_replace("seller_score=".$seller_score, "seller_score=4-5" ,$params);
                                        echo $fullURL = $currentURL . '?' .$new_param;
                                    }else{
                                        echo $fullURL = $currentURL . '?' .$params.'&seller_score=4-5';
                                    }
                                }else{
                                    echo $fullURL = $currentURL.'?seller_score=4-5';
                                }
                            ?>" <?php if ($this->input->get('seller_score') == '4-5'){ echo("checked"); }?>>
                                <label for="rating5"> 
                                    <span class="product-rating">
                                        <span class="star-rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                        <a href="javascript:void(0)" class="review-link">
                                            <span class="count">(4-5)</span>
                                        </a>
                                    </span>
                                </label>
                            </div>
                            <div class="checkbox checkbox-success">
                                <input id="rating6" type="checkbox" class="check_value" value="<?php
                                $currentURL = current_url();
                                $params = $_SERVER['QUERY_STRING'];
                                $seller_score = $this->input->get('seller_score');
                                if ($params) {
                                    if ($seller_score) {
                                        $new_param = str_replace("seller_score=".$seller_score, "seller_score=3-5" ,$params);
                                        echo $fullURL = $currentURL . '?' .$new_param;
                                    }else{
                                        echo $fullURL = $currentURL . '?' .$params.'&seller_score=3-5';
                                    }
                                }else{
                                    echo $fullURL = $currentURL.'?seller_score=3-5';
                                }
                            ?>" <?php if ($this->input->get('seller_score') == '3-5'){ echo("checked");}?>>
                                <label for="rating6"> 
                                    <span class="product-rating">
                                        <span class="star-rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                        <a href="javascript:void(0)" class="review-link">
                                            <span class="count">(3-5)</span>
                                        </a>
                                    </span>
                                </label>
                            </div>
                            <div class="checkbox checkbox-success">
                                <input id="rating7" type="checkbox" class="check_value" value="<?php
                                $currentURL = current_url();
                                $params = $_SERVER['QUERY_STRING'];
                                $seller_score = $this->input->get('seller_score');
                                if ($params) {
                                    if ($seller_score) {
                                        $new_param = str_replace("seller_score=".$seller_score, "seller_score=2-5" ,$params);
                                        echo $fullURL = $currentURL . '?' .$new_param;
                                    }else{
                                        echo $fullURL = $currentURL . '?' .$params.'&seller_score=2-5';
                                    }
                                }else{
                                    echo $fullURL = $currentURL.'?seller_score=2-5';
                                }
                            ?>" <?php if ($this->input->get('seller_score') == '2-5') {echo("checked");}?>>
                                <label for="rating7"> 
                                    <span class="product-rating">
                                        <span class="star-rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                        <a href="javascript:void(0)" class="review-link">
                                            <span class="count">(2-5)</span>
                                        </a>
                                    </span>
                                </label>
                            </div>
                            <div class="checkbox checkbox-success">
                                <input id="rating8" type="checkbox" class="check_value" value="<?php
                                $currentURL = current_url();
                                $params = $_SERVER['QUERY_STRING'];
                                $seller_score = $this->input->get('seller_score');
                                if ($params) {
                                    if ($seller_score) {
                                        $new_param = str_replace("seller_score=".$seller_score, "seller_score=1-5" ,$params);
                                        echo $fullURL = $currentURL . '?' .$new_param;
                                    }else{
                                        echo $fullURL = $currentURL . '?' .$params.'&seller_score=1-5';
                                    }
                                }else{
                                    echo $fullURL = $currentURL.'?seller_score=1-5';
                                }
                            ?>" <?php if ($this->input->get('seller_score') == '1-5') {echo("checked");}?>>
                                <label for="rating8"> 
                                    <span class="product-rating">
                                        <span class="star-rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                        <a href="javascript:void(0)" class="review-link">
                                            <span class="count">(1-5)</span>
                                        </a>
                                    </span>
                                </label>
                            </div>
                        </div>
                        <!--  /.End of seller ratiing -->
                        <div class="review-content">
                            <div class="review-product-info">
                                <h5 class="review-product-name"><?php echo display('name')?> : <?php echo $seller_name?></h5>
                                <div class="review-product-brand"></div>
                            </div>
                            <form class="review-form">
                                <div class="rating-content">
                                    <label for="input-1" class="control-label"><?php echo display('rate_it')?>: *</label>
                                    <input type="text" id="input-1" name="rating" class="rating rating-loading" data-min="0" data-max="5" data-step="1" value="2">
                                </div>
                                <div class="form-group">
                                    <label><?php echo display('title')?> *</label>
                                    <input type="text" name="title" class="form-control" id="title" placeholder="<?php echo display('title')?>">

                                    <input type="hidden" name="seller_id" value="<?php echo $seller_id?>">
                                </div>
                                <div class="form-group">
                                    <label><?php echo display('review')?> *</label>
                                    <textarea class="form-control" name="review"  placeholder="<?php echo display('review')?>" rows="5" id="review"></textarea>
                                </div>
                                <input type="button" id="seller_review_submit" class="btn btn-warning" value="<?php echo display('submit')?>">
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-9 content">
                <div class="filter-row align-items-center justify-content-between">
                    <div></div>
                    <div class="filter-title">
                        <h1><?php echo $seller_name?></h1>
                        <span>
                        <?php
                            echo $item = count($seller_product);
                            if ($item <= 0) {
                                echo " ".display('item_found');
                            }elseif ($item > 0) {
                               echo " ".display('items_found');
                            }
                        ?>
                        </span>
                    </div>
                    <div>
                        <select name="popularity" id="popularity">
                            <option value="javascript:void(0)"><?php echo display('sort_by')?></option>
                            <option value="<?php
                                $currentURL = current_url();
                                $params = $_SERVER['QUERY_STRING'];
                                $sort = $this->input->get('sort');
                                if ($params) {
                                    if ($sort) {
                                        $new_param = str_replace("sort=".$sort, "sort=new" ,$params);
                                        echo $fullURL = $currentURL . '?' .$new_param;
                                    }else{
                                        echo $fullURL = $currentURL . '?' .$params.'&sort=new';
                                    }
                                }else{
                                    echo $fullURL = $currentURL.'?sort=new';
                                }
                            ?>" <?php if ($this->input->get('sort') == 'new') {echo "selected";} ?>><?php echo display('new')?></option>
                            <option value="<?php
                                $currentURL = current_url();
                                $params = $_SERVER['QUERY_STRING'];
                                $sort = $this->input->get('sort');
                                if ($params) {
                                    if ($sort) {
                                        $new_param = str_replace("sort=".$sort, "sort=discount" ,$params);
                                        echo $fullURL = $currentURL . '?' .$new_param;
                                    }else{
                                        echo $fullURL = $currentURL . '?' .$params.'&sort=discount';
                                    }
                                }else{
                                    echo $fullURL = $currentURL.'?sort=discount';
                                }
                            ?>" <?php if ($this->input->get('sort') == 'discount') {echo "selected";} ?>><?php echo display('discount')?></option>
                            <option value="<?php
                                $currentURL = current_url();
                                $params = $_SERVER['QUERY_STRING'];
                                $sort = $this->input->get('sort');
                                if ($params) {
                                    if ($sort) {
                                        $new_param = str_replace("sort=".$sort, "sort=low_to_high" ,$params);
                                        echo $fullURL = $currentURL . '?' .$new_param;
                                    }else{
                                        echo $fullURL = $currentURL . '?' .$params.'&sort=low_to_high';
                                    }
                                }else{
                                    echo $fullURL = $currentURL.'?sort=low_to_high';
                                }
                            ?>" <?php if ($this->input->get('sort') == 'low_to_high') {echo "selected";} ?>><?php echo display('price_low_to_high')?></option>
                            <option value="<?php
                                $currentURL = current_url();
                                $params = $_SERVER['QUERY_STRING'];
                                $sort = $this->input->get('sort');
                                if ($params) {
                                    if ($sort) {
                                        $new_param = str_replace("sort=".$sort, "sort=high_to_low" ,$params);
                                        echo $fullURL = $currentURL . '?' .$new_param;
                                    }else{
                                        echo $fullURL = $currentURL . '?' .$params.'&sort=high_to_low';
                                    }
                                }else{
                                    echo $fullURL = $currentURL.'?sort=high_to_low';
                                }
                            ?>" <?php if ($this->input->get('sort') == 'high_to_low') {echo "selected";} ?>><?php echo display('price_high_to_low')?></option>
                        </select> 
                    </div>
                </div>
                <!-- /.End of filter summary -->
                <div class="row mr-0">
                    <?php
                    $page = ! empty( $_GET['page'] ) ? (int) $_GET['page'] : 1;
                    $total = count($seller_product); //total items in array    
                    $limit = 16; //per page    
                    $totalPages = ceil( $total/ $limit ); //calculate total pages
                    $page = max($page, 1); //get 1 page when $_GET['page'] <= 0
                    $page = min($page, $totalPages); //get last page when $_GET['page'] > $totalPages
                    $offset = ($page - 1) * $limit;
                    if( $offset < 0 ) $offset = 0;

                    $new_seller_product = array_slice( $seller_product, $offset, $limit );

                    if ($new_seller_product) {
                        foreach ($new_seller_product as $product) {
                            $select_single_category = $this->Categories->select_single_category_by_id($product['category_id']);
                    ?>
                    <div class="col-xs-6 col-sm-4 col-md-3 pa-0">
                        <div class="product-box">
                            <div class="imagebox">
                                <span class="product-cat"><a href="<?php echo base_url(remove_space($select_single_category->category_name).'/'.$product['category_id'])?>"><?php echo $select_single_category->category_name?></a></span>
                                <a href="<?php echo base_url(remove_space($select_single_category->category_name).'/'.remove_space($product['title']).'/'.$product['product_id'])?>">
                                    <h3 class="product-name"><?php echo $product['title']?></h3>
                                    <div class="product-thumb">
                                        <img src="<?php echo base_url()?>assets/website/img/loader.svg" data-src="<?php echo base_url().$product['thumb_image_url']?>" alt="">
                                    </div>
                                </a>
                                <span>
                                    <b>
                                    <?php
                                    if ($product['brand_name']) {
                                        echo $product['brand_name'];
                                    }else{
                                        echo $product['first_name'].' '.$product['last_name'];
                                    }
                                    ?>
                                    </b>
                                </span>
                                <div class="price-cart">
                                    <?php
                                       $currency_new_id  =  $this->session->userdata('currency_new_id');

                                        if (empty($currency_new_id)) {
                                            $result  =  $cur_info = $this->db->select('*')
                                                ->from('currency_info')
                                                ->where('default_status','1')
                                                ->get()
                                                ->row();
                                            $currency_new_id = $result->currency_id;
                                        }

                                        if (!empty($currency_new_id)) {
                                            $cur_info = $this->db->select('*')
                                                    ->from('currency_info')
                                                    ->where('currency_id',$currency_new_id)
                                                    ->get()
                                                    ->row();

                                            $target_con_rate = $cur_info->convertion_rate;
                                            $position1 = $cur_info->currency_position;
                                            $currency1 = $cur_info->currency_icon;
                                        }
                                    ?>

                                    <?php if ($product['on_sale'] == 1 && !empty($product['offer_price'])) { ?>
                                    <span class="price">
                                        <span class="price-amount">
                                            <ins><span class="amount"> 
                                            <?php
                                                if ($target_con_rate > 1) {
                                                    $price = $product['offer_price'] * $target_con_rate;
                                                    echo (($position1==0)?$currency1." ".number_format($price, 2, '.', ','):number_format($price, 2, '.', ',')." ".$currency1);
                                                }

                                                if ($target_con_rate <= 1) {
                                                    $price = $product['offer_price'] * $target_con_rate;
                                                    echo (($position1==0)?$currency1." ".number_format($price, 2, '.', ','):number_format($price, 2, '.', ',')." ".$currency1);
                                                }
                                            ?>
                                            </span></ins>
                                            <del><span class="amount">
                                            <?php 
                                                if ($target_con_rate > 1) {
                                                    $price = $product['price'] * $target_con_rate;
                                                    echo (($position1==0)?$currency1." ".number_format($price, 2, '.', ','):number_format($price, 2, '.', ',')." ".$currency1);
                                                }

                                                if ($target_con_rate <= 1) {
                                                    $price = $product['price'] * $target_con_rate;
                                                    echo (($position1==0)?$currency1." ".number_format($price, 2, '.', ','):number_format($price, 2, '.', ',')." ".$currency1);
                                                }
                                            ?>
                                            </span></del>
                                            <span class="amount"> </span>
                                        </span>
                                    </span><!-- /.Price -->
                                    <?php
                                    }else{
                                    ?>
                                    <span class="price">
                                        <span class="price-amount">
                                            <ins><span class="amount"> 
                                            <?php 
                                                if ($target_con_rate > 1) {
                                                    $price = $product['price'] * $target_con_rate;
                                                    echo (($position1==0)?$currency1." ".number_format($price, 2, '.', ','):number_format($price, 2, '.', ',')." ".$currency1);
                                                }

                                                if ($target_con_rate <= 1) {
                                                    $price = $product['price'] * $target_con_rate;
                                                    echo (($position1==0)?$currency1." ".number_format($price, 2, '.', ','):number_format($price, 2, '.', ',')." ".$currency1);
                                                }
                                            ?>  
                                            </span></ins>
                                            <span class="amount"> </span>
                                        </span>
                                    </span><!-- /.Price -->
                                    <?php
                                    }
                                    ?>
                                    <div class="rating_stars">
                                        <div class="rating-wrap">
                                        <?php
                                        $rater = $this->Categories->get_total_rater_by_product_id($product['product_id']);
                                        $result = $this->Categories->get_total_rate_by_product_id($product['product_id']);

                                        if ($result->rates != null) {
                                            $total_rate = $result->rates/$rater;
                                            if (gettype($total_rate) == 'integer') {
                                                for ($t=1; $t <= $total_rate; $t++) {
                                                   echo "<i class=\"fa fa-star\"></i>";
                                                }
                                                for ($tt=$total_rate; $tt < 5; $tt++) { 
                                                    echo "<i class=\"fa fa-star-o\"></i>";
                                                }
                                            }elseif (gettype($total_rate) == 'double') {
                                                $pieces = explode(".", $total_rate);
                                                for ($q=1; $q <= $pieces[0]; $q++) {
                                                   echo "<i class=\"fa fa-star\"></i>";
                                                   if ($pieces[0] == $q) {
                                                       echo "<i class=\"fa fa-star-half-o\"></i>";
                                                       for ($qq=$pieces[0]; $qq < 4; $qq++) { 
                                                            echo "<i class=\"fa fa-star-o\"></i>";
                                                        }
                                                   }
                                                }

                                            }else{
                                                for ($w=0; $w <= 4; $w++) {
                                                   echo "<i class=\"fa fa-star-o\"></i>";
                                                }
                                            }
                                        }else{
                                            for ($o=0; $o <= 4; $o++) {
                                               echo "<i class=\"fa fa-star-o\"></i>";
                                            }
                                        }
                                        ?>
                                        </div>
                                        <div class="total-rating">(<?php echo $rater?>)</div>
                                    </div><!-- Rating --> 
                                </div><!-- /.price-add-to-cart -->
                                <div class="box-bottom">
                                    <?php
                                    if (! $product['variant_id']) {
                                    ?>
                                    <div class="btn-add-cart">
                                        <a href="javascript:void(0)" onclick="add_to_cart('<?php echo $product['product_id']?>')" title="">
                                            <img src="<?php echo base_url('assets/website/img/add-cart.png')?>" alt=""><?php echo display('add_to_cart')?>
                                        </a>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                    <div class="compare-wishlist">
                                        <a href="javascript:void(0)" class="wishlist" title="" name="<?php echo $product['product_id']?>">
                                            <img src="<?php echo base_url('assets/website/img/wishlist.png')?>" alt=""><?php echo display('wishlist')?>
                                        </a>
                                    </div>
                                </div><!-- /.box-bottom -->
                            </div>
                        </div>
                        <!-- /.End of product box -->
                    </div>
                    <?php } }?>
                    <?php
                    if ($total > 0) {
                    ?>
                    <div class="col-xs-12 col-sm-12">
                        <div class="pagination-widget">
                            <?php
                            $start      = ( ( $page - $total ) > 0 ) ? $page - $total : 1;
                            $end        = ( ( $page + $total ) < $totalPages ) ? $page + $total : $totalPages;

                            $html       = '<ul class="pagination">';
                            $class      = ( $page == 1 ) ? "disabled" : "";
                            $html       .= '<li class="' . $class . '"><a href="?page=' . ( $page - 1 ) . '">⇽</a></li>';
                         
                            if ( $start > 1 ) {
                                $html   .= '<li><a href="?page=1">1</a></li>';
                                $html   .= '<li class="disabled"><span>...</span></li>';
                            }
                         
                            for ( $i = $start ; $i <= $end; $i++ ) {
                                $class  = ( $page == $i ) ? "active" : "";
                                $html   .= '<li class="' . $class . '"><a href="?page=' . $i . '">' . $i . '</a></li>';
                            }
                         
                            if ( $end < $totalPages ) {
                                $html   .= '<li class="disabled"><span>...</span></li>';
                                $html   .= '<li><a href="?page=' . $totalPages . '">' . $totalPages . '</a></li>';
                            }
                         
                            $class       = ( $page == $totalPages ) ? "disabled" : "";
                            $html       .= '<li class="' . $class . '"><a href="?page=' . ( $page + 1 ) . '">⇾</a></li>';
                         
                            echo $html  .= '</ul>';
                            ?>
                        </div>
                    </div>
                    <!-- /.End of pagination -->
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.End of category content -->
<div class="category-mobile">
    <div class="mobile-filter-nav align-items-center justify-content-between">
        <div class="col-xs-6">
            <select name="popularity" id="popularity_mobile">
                <option value="javascript:void(0)"><?php echo display('sort_by')?></option>
                <option value="<?php
                    $currentURL = current_url();
                    $params = $_SERVER['QUERY_STRING'];
                    $sort = $this->input->get('sort');
                    if ($params) {
                        if ($sort) {
                            $new_param = str_replace("sort=".$sort, "sort=new" ,$params);
                            echo $fullURL = $currentURL . '?' .$new_param;
                        }else{
                            echo $fullURL = $currentURL . '?' .$params.'&sort=new';
                        }
                    }else{
                        echo $fullURL = $currentURL.'?sort=new';
                    }
                ?>" <?php if ($this->input->get('sort') == 'new') {echo "selected";} ?>><?php echo display('new')?></option>
                <option value="<?php
                    $currentURL = current_url();
                    $params = $_SERVER['QUERY_STRING'];
                    $sort = $this->input->get('sort');
                    if ($params) {
                        if ($sort) {
                            $new_param = str_replace("sort=".$sort, "sort=discount" ,$params);
                            echo $fullURL = $currentURL . '?' .$new_param;
                        }else{
                            echo $fullURL = $currentURL . '?' .$params.'&sort=discount';
                        }
                    }else{
                        echo $fullURL = $currentURL.'?sort=discount';
                    }
                ?>" <?php if ($this->input->get('sort') == 'discount') {echo "selected";} ?>><?php echo display('discount')?></option>
                <option value="<?php
                    $currentURL = current_url();
                    $params = $_SERVER['QUERY_STRING'];
                    $sort = $this->input->get('sort');
                    if ($params) {
                        if ($sort) {
                            $new_param = str_replace("sort=".$sort, "sort=low_to_high" ,$params);
                            echo $fullURL = $currentURL . '?' .$new_param;
                        }else{
                            echo $fullURL = $currentURL . '?' .$params.'&sort=low_to_high';
                        }
                    }else{
                        echo $fullURL = $currentURL.'?sort=low_to_high';
                    }
                ?>" <?php if ($this->input->get('sort') == 'low_to_high') {echo "selected";} ?>><?php echo display('price_low_to_high')?></option>
                <option value="<?php
                    $currentURL = current_url();
                    $params = $_SERVER['QUERY_STRING'];
                    $sort = $this->input->get('sort');
                    if ($params) {
                        if ($sort) {
                            $new_param = str_replace("sort=".$sort, "sort=high_to_low" ,$params);
                            echo $fullURL = $currentURL . '?' .$new_param;
                        }else{
                            echo $fullURL = $currentURL . '?' .$params.'&sort=high_to_low';
                        }
                    }else{
                        echo $fullURL = $currentURL.'?sort=high_to_low';
                    }
                ?>" <?php if ($this->input->get('sort') == 'high_to_low') {echo "selected";} ?>><?php echo display('price_high_to_low')?></option>
            </select> 
        </div>
        <div class="col-xs-6 filter text-center">
            <button type="button" class="" data-toggle="modal" data-target=".filter-modal"><i class="fa fa-filter" aria-hidden="true"></i><?php echo display('filter')?></button>
        </div>
    </div>
    <div class="modal filter-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <!-- /.End of accessories -->
                <div class="sidebar-widget-title">
                    <?php if ($max_value) { ?>
                    <div class="price-Pips">
                        <form action="" method="post" id="priceForm1">
                            <h3 class="sidebar-title"><?php echo display('by_price')?></h3>
                            <input type="text" class="price-range" value="price-range" name="price-range" />
                        </form>
                    </div>
                    <?php } if ($variant_list) { ?>
                    <div class="product-size">
                        <h3 class="sidebar-title"><?php echo display('by_variant')?>:</h3>
                        <?php
                        if ($variant_list) {
                            $i=1;
                            foreach ($variant_list as $variant) {
                        ?>
                        <input type="radio" class="size1" name="size" id="<?php echo $i?>" value="<?php
                            $currentURL = current_url();
                            $params = $_SERVER['QUERY_STRING'];
                            $size = $this->input->get('size');
                            if ($params) {
                                if ($size) {
                                    $new_param = str_replace("size=".$size, "size=".$variant['variant_id'] ,$params);
                                    echo $fullURL = $currentURL . '?' .$new_param;
                                }else{
                                    echo $fullURL = $currentURL . '?' .$params.'&size='.$variant['variant_id'];
                                }
                            }else{
                                echo $fullURL = $currentURL.'?size='.$variant['variant_id'];
                            }
                        ?>" <?php if ($size == $variant['variant_id']) {echo "checked"; }?>/>
                        <label for="<?php echo $i?>"><span class="size"><?php echo $variant['variant_name']?></span></label>
                        <?php
                        $i++;
                            }
                        }
                        ?>
                    </div>
                    <!--  /.End of product color selector -->
                    <?php
                    }                     
                    $lang_id = 0;
                    $user_lang = $this->session->userdata('language');
                    if (empty($user_lang)) {
                        $lang_id = 'english';
                    }else{
                        $lang_id = $user_lang;
                    }
                    ?>
                    <div class="sidebar-review">
                        <h3 class="sidebar-title"><?php echo display('rating')?>:</h3>
                        <div class="checkbox checkbox-success">
                            <input id="rating1" type="checkbox" class="check_value" value="<?php
                            $currentURL = current_url();
                            $params = $_SERVER['QUERY_STRING'];
                            $rate = $this->input->get('rate');
                            if ($params) {
                                if ($rate) {
                                    $new_param = str_replace("rate=".$rate, "rate=4-5" ,$params);
                                    echo $fullURL = $currentURL . '?' .$new_param;
                                }else{
                                    echo $fullURL = $currentURL . '?' .$params.'&rate=4-5';
                                }
                            }else{
                                echo $fullURL = $currentURL.'?rate=4-5';
                            }
                        ?>" <?php if ($this->input->get('rate') == '4-5'){ echo("checked"); }?>>
                            <label for="rating1"> 
                                <span class="product-rating">
                                    <span class="star-rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-o"></i>
                                    </span>
                                    <a href="javascript:void(0)" class="review-link">
                                        <span class="count">(4-5)</span>
                                    </a>
                                </span>
                            </label>
                        </div>
                        <div class="checkbox checkbox-success">
                            <input id="rating2" type="checkbox" class="check_value" value="<?php
                            $currentURL = current_url();
                            $params = $_SERVER['QUERY_STRING'];
                            $rate = $this->input->get('rate');
                            if ($params) {
                                if ($rate) {
                                    $new_param = str_replace("rate=".$rate, "rate=3-5" ,$params);
                                    echo $fullURL = $currentURL . '?' .$new_param;
                                }else{
                                    echo $fullURL = $currentURL . '?' .$params.'&rate=3-5';
                                }
                            }else{
                                echo $fullURL = $currentURL.'?rate=3-5';
                            }
                        ?>" <?php if ($this->input->get('rate') == '3-5'){ echo("checked");}?>>
                            <label for="rating2"> 
                                <span class="product-rating">
                                    <span class="star-rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                    </span>
                                    <a href="javascript:void(0)" class="review-link">
                                        <span class="count">(3-5)</span>
                                    </a>
                                </span>
                            </label>
                        </div>
                        <div class="checkbox checkbox-success">
                            <input id="rating3" type="checkbox" class="check_value" value="<?php
                            $currentURL = current_url();
                            $params = $_SERVER['QUERY_STRING'];
                            $rate = $this->input->get('rate');
                            if ($params) {
                                if ($rate) {
                                    $new_param = str_replace("rate=".$rate, "rate=2-5" ,$params);
                                    echo $fullURL = $currentURL . '?' .$new_param;
                                }else{
                                    echo $fullURL = $currentURL . '?' .$params.'&rate=2-5';
                                }
                            }else{
                                echo $fullURL = $currentURL.'?rate=2-5';
                            }
                        ?>" <?php if ($this->input->get('rate') == '2-5') {echo("checked");}?>>
                            <label for="rating3"> 
                                <span class="product-rating">
                                    <span class="star-rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                    </span>
                                    <a href="javascript:void(0)" class="review-link">
                                        <span class="count">(2-5)</span>
                                    </a>
                                </span>
                            </label>
                        </div>
                        <div class="checkbox checkbox-success">
                            <input id="rating4" type="checkbox" class="check_value" value="<?php
                            $currentURL = current_url();
                            $params = $_SERVER['QUERY_STRING'];
                            $rate = $this->input->get('rate');
                            if ($params) {
                                if ($rate) {
                                    $new_param = str_replace("rate=".$rate, "rate=1-5" ,$params);
                                    echo $fullURL = $currentURL . '?' .$new_param;
                                }else{
                                    echo $fullURL = $currentURL . '?' .$params.'&rate=1-5';
                                }
                            }else{
                                echo $fullURL = $currentURL.'?rate=1-5';
                            }
                        ?>" <?php if ($this->input->get('rate') == '1-5') {echo("checked");}?>>
                            <label for="rating4"> 
                                <span class="product-rating">
                                    <span class="star-rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                    </span>
                                    <a href="javascript:void(0)" class="review-link">
                                        <span class="count">(1-5)</span>
                                    </a>
                                </span>
                            </label>
                        </div>
                    </div>
                    <!--  /.End of review ratiing -->
                    <div class="sidebar-review">
                        <h3 class="sidebar-title"><?php echo display('seller_score')?>:</h3>
                        <div class="checkbox checkbox-success">
                            <input id="rating5" type="checkbox" class="check_value" value="<?php
                            $currentURL = current_url();
                            $params = $_SERVER['QUERY_STRING'];
                            $seller_score = $this->input->get('seller_score');
                            if ($params) {
                                if ($seller_score) {
                                    $new_param = str_replace("seller_score=".$seller_score, "seller_score=4-5" ,$params);
                                    echo $fullURL = $currentURL . '?' .$new_param;
                                }else{
                                    echo $fullURL = $currentURL . '?' .$params.'&seller_score=4-5';
                                }
                            }else{
                                echo $fullURL = $currentURL.'?seller_score=4-5';
                            }
                        ?>" <?php if ($this->input->get('seller_score') == '4-5'){ echo("checked"); }?>>
                            <label for="rating5"> 
                                <span class="product-rating">
                                    <span class="star-rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-o"></i>
                                    </span>
                                    <a href="javascript:void(0)" class="review-link">
                                        <span class="count">(4-5)</span>
                                    </a>
                                </span>
                            </label>
                        </div>
                        <div class="checkbox checkbox-success">
                            <input id="rating6" type="checkbox" class="check_value" value="<?php
                            $currentURL = current_url();
                            $params = $_SERVER['QUERY_STRING'];
                            $seller_score = $this->input->get('seller_score');
                            if ($params) {
                                if ($seller_score) {
                                    $new_param = str_replace("seller_score=".$seller_score, "seller_score=3-5" ,$params);
                                    echo $fullURL = $currentURL . '?' .$new_param;
                                }else{
                                    echo $fullURL = $currentURL . '?' .$params.'&seller_score=3-5';
                                }
                            }else{
                                echo $fullURL = $currentURL.'?seller_score=3-5';
                            }
                        ?>" <?php if ($this->input->get('seller_score') == '3-5'){ echo("checked");}?>>
                            <label for="rating6"> 
                                <span class="product-rating">
                                    <span class="star-rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                    </span>
                                    <a href="javascript:void(0)" class="review-link">
                                        <span class="count">(3-5)</span>
                                    </a>
                                </span>
                            </label>
                        </div>
                        <div class="checkbox checkbox-success">
                            <input id="rating7" type="checkbox" class="check_value" value="<?php
                            $currentURL = current_url();
                            $params = $_SERVER['QUERY_STRING'];
                            $seller_score = $this->input->get('seller_score');
                            if ($params) {
                                if ($seller_score) {
                                    $new_param = str_replace("seller_score=".$seller_score, "seller_score=2-5" ,$params);
                                    echo $fullURL = $currentURL . '?' .$new_param;
                                }else{
                                    echo $fullURL = $currentURL . '?' .$params.'&seller_score=2-5';
                                }
                            }else{
                                echo $fullURL = $currentURL.'?seller_score=2-5';
                            }
                        ?>" <?php if ($this->input->get('seller_score') == '2-5') {echo("checked");}?>>
                            <label for="rating7"> 
                                <span class="product-rating">
                                    <span class="star-rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                    </span>
                                    <a href="javascript:void(0)" class="review-link">
                                        <span class="count">(2-5)</span>
                                    </a>
                                </span>
                            </label>
                        </div>
                        <div class="checkbox checkbox-success">
                            <input id="rating8" type="checkbox" class="check_value" value="<?php
                            $currentURL = current_url();
                            $params = $_SERVER['QUERY_STRING'];
                            $seller_score = $this->input->get('seller_score');
                            if ($params) {
                                if ($seller_score) {
                                    $new_param = str_replace("seller_score=".$seller_score, "seller_score=1-5" ,$params);
                                    echo $fullURL = $currentURL . '?' .$new_param;
                                }else{
                                    echo $fullURL = $currentURL . '?' .$params.'&seller_score=1-5';
                                }
                            }else{
                                echo $fullURL = $currentURL.'?seller_score=1-5';
                            }
                        ?>" <?php if ($this->input->get('seller_score') == '1-5') {echo("checked");}?>>
                            <label for="rating8"> 
                                <span class="product-rating">
                                    <span class="star-rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                    </span>
                                    <a href="javascript:void(0)" class="review-link">
                                        <span class="count">(1-5)</span>
                                    </a>
                                </span>
                            </label>
                        </div>
                    </div>
                    <!--  /.End of seller ratiing -->
                <!--     <div class="review-content">
                        <div class="review-product-info">
                            <h5 class="review-product-name"><?php echo display('name')?> : <?php echo $seller_name?></h5>
                            <div class="review-product-brand"></div>
                        </div>
                        <form class="review-form">
                            <div class="rating-content">
                                <label for="input-1" class="control-label"><?php echo display('rate_it')?>: *</label>
                                <input type="text" id="input-1" name="rating" class="rating rating-loading" data-min="0" data-max="5" data-step="1" value="2">
                            </div>
                            <div class="form-group">
                                <label><?php echo display('title')?> *</label>
                                <input type="text" name="title" class="form-control" id="title" placeholder="<?php echo display('title')?>">

                                <input type="hidden" name="seller_id" value="<?php echo $seller_id?>">
                            </div>
                            <div class="form-group">
                                <label><?php echo display('review')?> *</label>
                                <textarea class="form-control" name="review"  placeholder="<?php echo display('review')?>" rows="5" id="review"></textarea>
                            </div>
                            <input type="button" id="seller_review_submit" class="btn btn-warning" value="<?php echo display('submit')?>">
                        </form>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
    <div class="page-title">
        <h2 class="title"><?php echo $seller_name?></h2>
        <h5 class="sub-title"> 
        <?php
            echo $item = count($seller_product);
            if ($item <= 0) {
                echo " ".display('item_found');
            }elseif ($item > 0) {
               echo " ".display('items_found');
            }
        ?>                   
        </h5>
    </div>
    <ul class="category">
       <?php
        $page = ! empty( $_GET['page'] ) ? (int) $_GET['page'] : 1;
        $total = count($seller_product); //total items in array    
        $limit = 16; //per page    
        $totalPages = ceil( $total/ $limit ); //calculate total pages
        $page = max($page, 1); //get 1 page when $_GET['page'] <= 0
        $page = min($page, $totalPages); //get last page when $_GET['page'] > $totalPages
        $offset = ($page - 1) * $limit;
        if( $offset < 0 ) $offset = 0;
        $seller_product = array_slice( $seller_product, $offset, $limit );

        if ($seller_product) {
            foreach ($seller_product as $product) {
                $select_single_category = $this->Categories->select_single_category_by_id($product['category_id']);
        ?>
        <li class="col-xs-4">
            <div class="product-box">
                <div class="imagebox">
                    <span class="product-cat"><a href="<?php echo base_url(remove_space($select_single_category->category_name).'/'.$product['category_id'])?>"><?php echo $select_single_category->category_name?></a></span>
                    <a href="<?php echo base_url(remove_space($select_single_category->category_name).'/'.remove_space($product['title']).'/'.$product['product_id'])?>">
                        <h3 class="product-name"><?php echo $product['title']?></h3>
                        <div class="product-thumb">
                            <img src="<?php echo base_url()?>assets/website/img/loader.svg" data-src="<?php echo base_url().$product['thumb_image_url']?>" alt="">
                        </div>
                    </a>
                    <span>
                        <b>
                        <?php
                        if ($product['brand_name']) {
                            echo $product['brand_name'];
                        }else{
                            echo $product['first_name'].' '.$product['last_name'];
                        }
                        ?>
                        </b>
                    </span>
                    <div class="price-cart">
                        <?php
                           $currency_new_id =  $this->session->userdata('currency_new_id');

                            if (empty($currency_new_id)) {
                                $result  =  $cur_info = $this->db->select('*')
                                                    ->from('currency_info')
                                                    ->where('default_status','1')
                                                    ->get()
                                                    ->row();
                                $currency_new_id = $result->currency_id;
                            }

                            if (!empty($currency_new_id)) {
                                $cur_info = $this->db->select('*')
                                                    ->from('currency_info')
                                                    ->where('currency_id',$currency_new_id)
                                                    ->get()
                                                    ->row();

                                $target_con_rate = $cur_info->convertion_rate;
                                $position1 = $cur_info->currency_position;
                                $currency1 = $cur_info->currency_icon;
                            }
                        ?>

                        <?php if ($product['on_sale'] == 1 && !empty($product['offer_price'])) { ?>
                        <span class="price">
                            <span class="price-amount">
                                <ins><span class="amount"> 
                                <?php
                                    if ($target_con_rate > 1) {
                                        $price = $product['offer_price'] * $target_con_rate;
                                        echo (($position1==0)?$currency1." ".number_format($price, 2, '.', ','):number_format($price, 2, '.', ',')." ".$currency1);
                                    }

                                    if ($target_con_rate <= 1) {
                                        $price = $product['offer_price'] * $target_con_rate;
                                        echo (($position1==0)?$currency1." ".number_format($price, 2, '.', ','):number_format($price, 2, '.', ',')." ".$currency1);
                                    }
                                ?>
                                </span></ins>
                                <del><span class="amount">
                                <?php 
                                    if ($target_con_rate > 1) {
                                        $price = $product['price'] * $target_con_rate;
                                        echo (($position1==0)?$currency1." ".number_format($price, 2, '.', ','):number_format($price, 2, '.', ',')." ".$currency1);
                                    }

                                    if ($target_con_rate <= 1) {
                                        $price = $product['price'] * $target_con_rate;
                                        echo (($position1==0)?$currency1." ".number_format($price, 2, '.', ','):number_format($price, 2, '.', ',')." ".$currency1);
                                    }
                                ?>
                                </span></del>
                                <span class="amount"> </span>
                            </span>
                        </span><!-- /.Price -->
                        <?php
                        }else{
                        ?>
                        <span class="price">
                            <span class="price-amount">
                                <ins><span class="amount"> 
                                <?php 
                                    if ($target_con_rate > 1) {
                                        $price = $product['price'] * $target_con_rate;
                                        echo (($position1==0)?$currency1." ".number_format($price, 2, '.', ','):number_format($price, 2, '.', ',')." ".$currency1);
                                    }

                                    if ($target_con_rate <= 1) {
                                        $price = $product['price'] * $target_con_rate;
                                        echo (($position1==0)?$currency1." ".number_format($price, 2, '.', ','):number_format($price, 2, '.', ',')." ".$currency1);
                                    }
                                ?>  
                                </span></ins>
                                <span class="amount"> </span>
                            </span>
                        </span><!-- /.Price -->
                        <?php
                        }
                        ?>
                        <div class="rating_stars">
                            <div class="rating-wrap">
                            <?php
                            $rater = $this->Categories->get_total_rater_by_product_id($product['product_id']);
                            $result = $this->Categories->get_total_rate_by_product_id($product['product_id']);

                            if ($result->rates != null) {
                                $total_rate = $result->rates/$rater;
                                if (gettype($total_rate) == 'integer') {
                                    for ($t=1; $t <= $total_rate; $t++) {
                                       echo "<i class=\"fa fa-star\"></i>";
                                    }
                                    for ($tt=$total_rate; $tt < 5; $tt++) { 
                                        echo "<i class=\"fa fa-star-o\"></i>";
                                    }
                                }elseif (gettype($total_rate) == 'double') {
                                    $pieces = explode(".", $total_rate);
                                    for ($q=1; $q <= $pieces[0]; $q++) {
                                       echo "<i class=\"fa fa-star\"></i>";
                                       if ($pieces[0] == $q) {
                                           echo "<i class=\"fa fa-star-half-o\"></i>";
                                           for ($qq=$pieces[0]; $qq < 4; $qq++) { 
                                                echo "<i class=\"fa fa-star-o\"></i>";
                                            }
                                       }
                                    }

                                }else{
                                    for ($w=0; $w <= 4; $w++) {
                                       echo "<i class=\"fa fa-star-o\"></i>";
                                    }
                                }
                            }else{
                                for ($o=0; $o <= 4; $o++) {
                                   echo "<i class=\"fa fa-star-o\"></i>";
                                }
                            }
                            ?>
                            </div>
                            <div class="total-rating">(<?php echo $rater?>)</div>
                        </div><!-- Rating --> 
                    </div><!-- /.price-add-to-cart -->
                </div>
            </div>
            <!-- /.End of product box -->
        </li>
        <?php 
            } 
        }
        ?>
    </ul>
    <div class="pagination-widget">
        <?php if ($total > 0) { ?>
        <div class="col-xs-12 col-sm-12">
            <div class="pagination-widget">
                <?php
                $start      = ( ( $page - $total ) > 0 ) ? $page - $total : 1;
                $end        = ( ( $page + $total ) < $totalPages ) ? $page + $total : $totalPages;

                $html       = '<ul class="pagination">';
                $class      = ( $page == 1 ) ? "disabled" : "";
                $html       .= '<li class="' . $class . '"><a href="?page=' . ( $page - 1 ) . '">⇽</a></li>';
             
                if ( $start > 1 ) {
                    $html   .= '<li><a href="?page=1">1</a></li>';
                    $html   .= '<li class="disabled"><span>...</span></li>';
                }
             
                for ( $i = $start ; $i <= $end; $i++ ) {
                    $class  = ( $page == $i ) ? "active" : "";
                    $html   .= '<li class="' . $class . '"><a href="?page=' . $i . '">' . $i . '</a></li>';
                }
             
                if ( $end < $totalPages ) {
                    $html   .= '<li class="disabled"><span>...</span></li>';
                    $html   .= '<li><a href="?page=' . $totalPages . '">' . $totalPages . '</a></li>';
                }
             
                $class       = ( $page == $totalPages ) ? "disabled" : "";
                $html       .= '<li class="' . $class . '"><a href="?page=' . ( $page + 1 ) . '">⇾</a></li>';
             
                echo $html  .= '</ul>';
                ?>
            </div>
        </div>
        <?php } ?>
        <!-- /.End of pagination -->
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        /*------------------------------------
        Price range slide
        -------------------------------------- */
        $(".price-range").ionRangeSlider({
            type: "double",
            grid: true,
            min: <?php echo $min_value?>,
            max: <?php echo $max_value?>,
            from: <?php if ($from_price == 0) {echo 'null';}else{echo $from_price;}?>,
            to: <?php if ($to_price == 0) {echo 'null';}else{echo $to_price;}?>,
            prefix: "<?php echo $default_currency_icon;?> ", 
            onChange: function (data) {

                //field "search";
                var pattern = /[?]/;
                var URL = location.search;
                var fullURL = document.URL;

                if(pattern.test(URL))
                {
                    var $_GET = {};
                    if(document.location.toString().indexOf('?') !== -1) {
                        var query = document.location
                                       .toString()
                                       // get the query string
                                       .replace(/^.*?\?/, '')
                                       // and remove any existing hash string (thanks, @vrijdenker)
                                       .replace(/#.*$/, '')
                                       .split('&');

                        for(var i=0, l=query.length; i<l; i++) {
                           var aux = decodeURIComponent(query[i]).split('=');
                           $_GET[aux[0]] = aux[1];
                        }
                    }

                    //Get from value by get method
                    if ($_GET['price']) {
                        var fullURL = window.location.href.split('?')[0];
                        var url = window.location.search;
                        url = url.replace("price="+$_GET['price'], 'price='+data.from+'-'+data.to);
                        window.location.href = fullURL+url;
                    }else{
                        var url = window.location.search;
                        window.location.href = url+'&price=' + data.from+'-'+data.to;
                    }

                }else{
                    var fullURL = window.location.href.split('?')[0];
                    window.location.href = fullURL.split('?')[0]+'?price=' + data.from+'-'+data.to
                }
            }
        });
        /*------------------------------------
        Product search by size
        -------------------------------------- */
        $('body').on('click', '.size1', function() {
            var size_location = $(this).val();
            window.location.href = size_location;
        });
        /*------------------------------------
        Sorting product by category
        -------------------------------------- */
        $('#popularity').on('change',function(){
            var sorting_location = $(this).val();
            window.location.href = sorting_location;
        });
        /*------------------------------------
        Sorting product by category for mobile
        -------------------------------------- */
        $('#popularity_mobile').on('change',function(){
            var sorting_location = $(this).val();
            window.location.href = sorting_location;
        });
        /*------------------------------------
        Sort by rating
        -------------------------------------- */
        $('.check_value').on('click',function(){
            var rating_location = $(this).val();
            window.location.href = rating_location;
        });
        /*------------------------------------
        Brand
        -------------------------------------- */
        $('body').on('click', '.brand_class', function () {
            var brand_location = $(this).val();
            window.location.href = brand_location;
        });
    });
</script>

<!-- Wishlist js -->
<script type="text/javascript">
    //Add wishlist
    $('body').on('click', '.wishlist', function() {
        var product_id  = $(this).attr('name');
        var customer_id = '<?php echo $this->session->userdata('customer_id')?>';
        if (customer_id == 0) {
            alert('<?php echo display('please_login_first')?>');
            return false;
        }
        $.ajax({
            type: "post",
            async: true,
            url: '<?php echo base_url('website/Home/add_wishlist')?>',
            data: {product_id:product_id,customer_id:customer_id},
            success: function(data) {
                if (data == '1') {
                    alert('<?php echo display('product_added_to_wishlist')?>');
                }else if(data == '2'){
                    alert('<?php echo display('product_already_exists_in_wishlist')?>')
                }else if(data == '3'){
                    alert('<?php echo display('please_login_first')?>')
                }
            },
            error: function() {
                alert('Request Failed, Please check your code and try again!');
            }
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){
        //Add review
        $('body').on('click', '#seller_review_submit', function() {

            var seller_id  = '<?php echo $seller_id?>';
            var title       = $('#title').val();
            var review_msg  = $('#review').val();
            var customer_id = '<?php echo $this->session->userdata('customer_id')?>';
            var rate        = $('#input-1').val();

            //Review msg check
            if (review_msg == 0 || title == 0) {
                alert('<?php echo display('please_write_your_comment')?>');
                return false;
            }

            //Customer id check
            if (customer_id == 0) {
                alert('<?php echo display('please_login_first')?>');
                return false;
            }

            $.ajax({
                type: "post",
                async: true,
                url: '<?php echo base_url('website/product/submit_seller_review')?>',
                data: {seller_id:seller_id,customer_id:customer_id,review_msg:review_msg,rate:rate,title:title},
                success: function(data) {
                    if (data == '1') {
                        $('#review_msg').val('');
                        alert('<?php echo display('your_review_added')?>');
                        location.reload();
                    }else if(data == '2'){
                        $('#review_msg').val('');
                        alert('<?php echo display('thanks_you_already_reviewed')?>');
                        location.reload();
                    }else if(data == '3'){
                        $('#review_msg').val('');
                        alert('<?php echo display('please_login_first')?>');
                        location.reload();
                    }
                },
                error: function() {
                    alert('Request Failed, Please check your code and try again!');
                }
            });
        });
    });
</script>