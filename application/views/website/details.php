<?php defined('BASEPATH') OR exit('No direct script access allowed');
$select_first_category  = $this->Categories->select_first_category($category_id);
$select_secnd_category  = $this->Categories->select_secnd_category($category_id);
$select_third_category  = $this->Categories->select_third_category($category_id);
$select_single_category = $this->Categories->select_single_category($category_id);
$select_parent_category = $this->Categories->select_parent_category($category_id);
?>
<div class="page-breadcrumbs">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('/')?>"><?php echo display('home')?></a></li>
            <?php if($select_first_category){?>
            <li><a href="<?php echo base_url(remove_space($select_first_category['category_name']).'/'.$select_first_category['category_id'])?>"><?php if($select_first_category){echo $select_first_category['category_name'];}?></a></li>
            <?php } ?>
            <?php if ($select_secnd_category) { ?>
            <li><a href="<?php echo base_url(remove_space($select_secnd_category['category_name']).'/'.$select_secnd_category['category_id'])?>"><?php echo $select_secnd_category['category_name']?></a></li>
            <?php } ?>
            <?php if ($select_third_category) { ?>
            <li><a href="<?php echo base_url(remove_space($select_third_category['category_name']).'/'.$select_third_category['category_id'])?>"><?php echo $select_third_category['category_name']?></a></li>
            <?php } ?>
            <li class="active"><?php echo $product_name?></li>
        </ol>
    </div>
</div>
<!-- /.End of page breadcrumbs -->
<div class="product-details-content">
    <div class="product-details-inner">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="product-header align-items-center">
                        <div class="col-sm-8">
                            <h1 class="product-title"><?php echo $product_name?></h1>
                            <ul class="entry-meta">
                                <?php
                                if ($business_name) {
                                ?>
                                <li>
                                    <div class="sold-by-meta">
                                        <span class="sold-by-label"><?php echo display('seller')?> : </span>
                                        <a href="<?php echo base_url('seller_product/list/'.$seller_id)?>"><?php echo $first_name." ".$last_name?></a>
                                    </div>
                                </li>
                                <?php
                                }
                                if (!empty($brand_id)) {
                                ?>
                                <li>
                                    <div class="sold-by-meta">
                                        <span class="sold-by-label"><?php echo display('brand')?> : </span>
                                        <a href="<?php echo base_url('brand_product/list/'.$brand_id)?>"><?php echo $brand_name?></a>
                                    </div>
                                </li>
                                <?php
                                }
                                ?>
                                <li class="meta-sku"> <?php echo display('product_model')?>: <span class="meta-value"><?php echo $product_model?></span></li>
                                <li>
                                    <div class="product-rating">
                                        <div class="star-rating">
                                        <?php
                                            $rater = $this->Categories->get_total_rater_by_product_id($product_id);
                                            $result = $this->Categories->get_total_rate_by_product_id($product_id);

                                            if ($result->rates != null) {
                                                $total_rate = $result->rates/$rater;
                                                if (gettype($total_rate) == 'integer') {
                                                    for ($t=1; $t <= $total_rate; $t++) {
                                                       echo "<i class=\"fa fa-star\"></i>";
                                                    }
                                                    for ($tt=$total_rate; $tt < 5; $tt++) { 
                                                        echo "<i class=\"fa fa-star-o\"></i>";
                                                    }
                                                }elseif (gettype($total_rate) == 'double') {
                                                    $pieces = explode(".", $total_rate);
                                                    for ($q=1; $q <= $pieces[0]; $q++) {
                                                       echo "<i class=\"fa fa-star\"></i>";
                                                       if ($pieces[0] == $q) {
                                                           echo "<i class=\"fa fa-star-half-o\"></i>";
                                                           for ($qq=$pieces[0]; $qq < 4; $qq++) { 
                                                                echo "<i class=\"fa fa-star-o\"></i>";
                                                            }
                                                       }
                                                    }

                                                }else{
                                                    for ($w=0; $w <= 4; $w++) {
                                                       echo "<i class=\"fa fa-star-o\"></i>";
                                                    }
                                                }
                                            }else{
                                                for ($o=0; $o <= 4; $o++) {
                                                   echo "<i class=\"fa fa-star-o\"></i>";
                                                }
                                            }
                                        ?>
                                        </div>
                                        <a href="javascript:void(0)" class="review-link">
                                            (<span class="count"><?php echo $rater;?> </span><?php echo display('review')?>)
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-4">
                            <div class="social-links">
                                <a class="share-button fb" data-share-url="<?php echo  current_url();?>" data-share-network="facebook" data-share-text="Share this awesome link on Facebook" data-share-title="Facebook Share" data-share-via="" data-share-tags="" data-share-media="" href="#"><i class="fa fa-facebook"></i></a>

                                <a class="share-button tw" data-share-url="<?php echo  current_url();?>" data-share-network="twitter" data-share-text="Share this awesome link on Twitter" data-share-title="Twitter Share" data-share-via="" data-share-tags="" data-share-media="" href="#"><i class="fa fa-twitter"></i></a>

                                <a class="share-button gp" data-share-url="<?php echo  current_url();?>" data-share-network="googleplus" data-share-text="Share this awesome link on Google+" data-share-title="Google+ Share" data-share-via="" data-share-tags="" data-share-media="" href="#"><i class="fa fa-google-plus"></i></a>

                                <a class="share-button li" data-share-url="<?php echo  current_url();?>" data-share-network="linkedin" data-share-text="Share this awesome link on LinkedIn" data-share-title="LinkedIn Share" data-share-via="" data-share-tags="" data-share-media="" href="#"><i class="fa fa-linkedin"></i></a>

                                <a class="share-button pr" data-share-url="<?php echo  current_url();?>" data-share-network="pinterest" data-share-text="Share this awesome link on Pinterest" data-share-title="Pinterest Share" data-share-via="" data-share-tags="" data-share-media="" href="#"><i class="fa fa-pinterest"></i></a>
                            </div>
                            <!-- /.End of social link -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="wrapper">
                        <!-- Product Images & Alternates -->
                        <div class="product-images">
                            <div class="row">
                                <div class="col-xs-3 col-sm-2">
                                    <!-- Begin product thumb nav -->
                                    <ul class="thumb-nav">
                                        <?php
                                        if ($get_thumb_image) {
                                            foreach ($get_thumb_image as $image) {
                                                if ($image->image_type == 1) {
                                        ?>
                                        <li><img src="<?php echo base_url().$image->image_path?>" alt=""></li>
                                        <?php
                                                }
                                            }
                                        }
                                        ?>
                                    </ul>
                                    <!-- End product thumb nav -->
                                </div>
                                <div class="col-xs-9 col-sm-10">
                                    <!-- Begin Product Images Slider -->
                                    <div class="main-img-slider">
                                        <?php
                                        if ($get_thumb_image) {
                                            foreach ($get_thumb_image as $image) {
                                                if ($image->image_type == 2) {
                                        ?>
                                        <figure>
                                            <a href="<?php echo base_url().$image->image_path?>" data-size="1400x1400">
                                                <img src="<?php echo base_url().$image->image_path?>" data-lazy="<?php echo base_url().$image->image_path?>" data-zoom-image="<?php echo base_url().$image->image_path?>"  alt="<?php echo $image->image_name?>" />
                                            </a>
                                        </figure>
                                        <?php
                                                }
                                            }
                                        }
                                        ?>
                                    </div>
                                    <!-- End Product Images Slider -->
                                </div>
                            </div>
                        </div>
                        <!-- End Product Images & Alternates -->
                        <!-- Begin Product Image Zoom -->
                        <!-- This should live at bottom of page before closing body tag So that it renders on top of all page elements. -->
                        <!-- Root element of PhotoSwipe. Must have class pswp. -->
                        <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
                            <!-- Background of PhotoSwipe. It's a separate element, as animating opacity is faster than rgba(). -->
                            <div class="pswp__bg"></div>
                            <!-- Slides wrapper with overflow:hidden. -->
                            <div class="pswp__scroll-wrap">
                                <!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
                                <div class="pswp__container">
                                    <!-- don't modify these 3 pswp__item elements, data is added later on -->
                                    <div class="pswp__item"></div>
                                    <div class="pswp__item"></div>
                                    <div class="pswp__item"></div>
                                </div>
                                <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
                                <div class="pswp__ui pswp__ui--hidden">
                                    <div class="pswp__top-bar">
                                        <!--  Controls are self-explanatory. Order can be changed. -->
                                        <div class="pswp__counter"></div>
                                        <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                                        <button class="pswp__button pswp__button--share" title="Share"></button>
                                        <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                                        <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                                        <!-- Preloader demo https://codepen.io/dimsemenov/pen/yyBWoR -->
                                        <!-- element will get class pswp__preloader-active when preloader is running -->
                                        <div class="pswp__preloader">
                                            <div class="pswp__preloader__icn">
                                                <div class="pswp__preloader__cut">
                                                    <div class="pswp__preloader__donut"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--  /.pswp__top-bar -->
                                    <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                                        <div class="pswp__share-tooltip"></div> 
                                    </div>
                                    <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
                                    <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>

                                    <div class="pswp__caption">
                                        <div class="pswp__caption__center"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--  End product zoom  -->
                    </div>
                    <!-- wrapper -->
                </div>
                <div class="col-sm-6">
                    <div class="product-summary-content">
                        <div class="product-summary">
                            <div class="product-price">
                                <?php
                                $currency_new_id = $this->session->userdata('currency_new_id');
                                if (empty($currency_new_id)) {
                                    $result  =  $cur_info = $this->db->select('*')
                                                        ->from('currency_info')
                                                        ->where('default_status','1')
                                                        ->get()
                                                        ->row();
                                    $currency_new_id = $result->currency_id;
                                }

                                if (!empty($currency_new_id)) {
                                    $cur_info = $this->db->select('*')
                                                        ->from('currency_info')
                                                        ->where('currency_id',$currency_new_id)
                                                        ->get()
                                                        ->row();

                                    $target_con_rate = $cur_info->convertion_rate;
                                    $position1 = $cur_info->currency_position;
                                    $currency1 = $cur_info->currency_icon;
                                }
                                ?>
                                <?php if ($on_sale == 1 && !empty($offer_price)) { ?>
                                <ins>
                                    <span class="woocommerce-Price-amount amount">
                                        <span class="woocommerce-Price-currencySymbol">
                                        <?php
//                                            if ($target_con_rate > 1) {
                                                $offer_price = $offer_price * $target_con_rate;
                                                echo (($position1==0)?$currency1." ".number_format($offer_price, 0, '.', ','):number_format($offer_price, 0, '.', ',')." ".$currency1);
//                                            }
//
//                                            if ($target_con_rate <= 1) {
//                                                $offer_price = $offer_price * $target_con_rate;
//                                                echo (($position1==0)?$currency1." ".number_format($offer_price, 2,  ','):number_format($offer_price, 2, ',')." ".$currency1);
//                                            }
                                        ?>
                                        </span>
                                    </span>
                                </ins>
                                <del>
                                    <span class="woocommerce-Price-amount amount">
                                        <span class="woocommerce-Price-currencySymbol">
                                        <?php 
//                                        if ($target_con_rate > 1) {
                                            $price = $price * $target_con_rate;
                                            echo (($position1==0)?$currency1." ".number_format($price, 0, ',')
                                                :number_format($price, 0, '.',',')." ".$currency1);
//                                        }

//                                        if ($target_con_rate <= 1) {
//                                            $price = $price * $target_con_rate;
//                                            echo (($position1==0)?$currency1." ".number_format($price, 2, '.', ','):number_format($price, 2, '.', ',')." ".$currency1);
//                                        }
                                        ?>
                                        </span>
                                    </span>
                                </del>
                                <?php
                                }else{
                                ?>
                                <ins>
                                    <span class="woocommerce-Price-amount amount">
                                        <span class="woocommerce-Price-currencySymbol">
                                        <?php 
//                                            if ($target_con_rate > 1) {
                                                $price = $price * $target_con_rate;
                                                echo (($position1==0)?$currency1." ".number_format($price, 0, '.', ','):number_format($price, 0, '.', ',')." ".$currency1);
//                                            }
//
//                                            if ($target_con_rate <= 1) {
//                                                $price = $price * $target_con_rate;
//                                                echo (($position1==0)?$currency1." ".number_format($price, 2, '.', ','):number_format($price, 2, '.', ',')." ".$currency1);
//                                            }
                                        ?>
                                        </span>
                                    </span>
                                </ins>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                        <ul class="summary-header">
                            <?php
                            if ($seller_id) {
                            ?>
                            <li>
                                <div class="sold-by-meta">
                                    <span class="sold-by-label"><?php echo display('seller')?> : </span>
                                    <a href="<?php echo base_url('seller_product/list/'.$seller_id)?>"><?php echo $first_name." ".$last_name?></a>
                                </div>
                            </li>
                            <?php
                            }if ($brand_id) {
                            ?>
                            <li>
                                <div class="sold-by-meta">
                                    <span class="sold-by-label"><?php echo display('brand')?> : </span>
                                    <a href="<?php echo base_url('brand_product/list/'.$brand_id)?>"><?php echo $brand_name?></a>
                                </div>
                            </li>
                            <?php
                            }
                            ?>
                        </ul>
                        <div class="short-description">
                            <?php
                            $lang_id = 0;
                            $user_lang = $this->session->userdata('language');
                            if (empty($user_lang)) {
                                $lang_id = 'english';
                            }else{
                                $lang_id = $user_lang;
                            }

                            $description = $this->db->select('*')
                                                ->from('product_description')
                                                ->where('product_id',$product_id)
                                                ->where('lang_id',$lang_id)
                                                ->where('description_type',1)
                                                ->get()
                                                ->row();
                            if ($description) {
                                echo $description->description;
                            } 
                            if ($variant) {
                            ?>
                            <div class="product-size">
                                <h5><?php echo display('variant')?>:</h5>
                                <?php
                                $exploded = explode(',', $variant);
                                $i=1;
                                foreach ($exploded as $elem) {
                                $this->db->select('*');
                                $this->db->from('variant');
                                $this->db->where('variant_id',$elem);
                                $this->db->order_by('variant_name','asc');
                                $result = $this->db->get()->row();
                                ?>
                                <input type="radio" name="size" id="m<?php echo $i;?>" value="<?php echo $result->variant_id?>" />
                                <label for="m<?php echo $i;?>"><span class="size1"><?php echo $result->variant_name?></span></label>
                                <?php
                                $i++;
                                }
                                ?>
                            </div>
                            <?php
                            }
                            ?>
                            <!--  /.End of product Size -->
                        </div>
                        <form class="cart-row">
                            <div class="quantity">
                                <div class="number-spinner">
                                    <span class="qty qty-minus" data-dir="dwn">-</span>
                                    <input type="text" class="form-control text-center" value="1" id="qty" max="<?php echo $quantity?>">
                                    <span class="qty qty-plus" data-dir="up">+</span>
                                    <input type="hidden" value="<?php echo $quantity?>" name="stock" id="stock">
                                </div>
                            </div>
                            <?php
                            if ($quantity >= 1) {
                            ?>
                            <button class="cart-btn" type="button" onclick="buy_now('<?php echo $product_id?>')"><?php echo display('buy_now')?></button>
                            <button class="cart-btn scarlet" type="button" onclick="cart_btn('<?php echo $product_id?>')"><?php echo display('add_to_cart')?></button>
                            <?php
                            }else{
                                if ($pre_order == 1) {
                            ?>
                            <button class="cart-btn" type="button" onclick="pre_order('<?php echo $product_id?>')"><?php echo display('pre_order')?></button>
                            <?php
                                }
                            }
                            ?>
                        </form>

                        <div class="cart-row">
                            <a href="javascript:void(0)" class="add-wishlist wishlist" data-toggle="tooltip" data-placement="top" title="<?php echo display('wishlist')?>"><i class="lnr lnr-heart"></i><span><?php echo display('save_for_the_future')?> </span></a>
                        </div>
                        <div class="phone-order">
                            <div class="stock-text">
                                <?php 
                                if ($quantity > 10) {
                                   echo display('more_than_ten_pieces_in_stock');
                                }elseif (($quantity >= 1) && ($quantity <= 10)){
                                    echo $quantity." ".display('pieces_in_stock');
                                }elseif (($pre_order_quantity > 0)) {
                                    echo display('0_piece_in_stock');
                                }elseif (($pre_order_quantity <= 0) && ($quantity <= 0)){
                                    echo "<p style='color:red'>".display('out_of_stock')."</p>";
                                }
                                ?>
                            </div>
                            <div class="d-flex">
                                <div class="support-icon">
                                    <img src="<?php echo base_url('assets/website/img/support.png')?>">
                                </div>
                                <div class="support">
                                    <div class="order-text">
                                        <?php echo display('dial_order_to_phone')?>
                                    </div>
                                    <div class="order-number">
                                        <?php echo $mobile?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="product-meta">
                            <?php
                            if ($category_name) {
                            ?>
                            <div class="posted-in">
                                <strong><?php echo display('category')?>: </strong>
                                <a href="javascript:void(0)"> <?php echo $category_name ?></a>
                            </div>
                            <?php
                            }
                            if ($tag) {
                            ?>
                            <div class="tag-as">
                                <strong><?php echo display('tag')?>: </strong>
                                <a href="javascript:void(0)"><?php echo $tag?></a>
                            </div>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    if ($related_product) {
    ?>
    <div class="similar-products-content">
        <div class="container">
            <h3 class="title-widget"><span><?php echo display('similar_product')?></span></h3>
            <div class="products-slide">
                <?php
                if ($related_product) {
                    foreach ($related_product as $product) {
                ?>
                <div class="product-box">
                    <div class="imagebox">
                        <span class="product-cat"><a href="<?php echo base_url(remove_space($product->category_name).'/'.$product->category_id)?>"><?php echo $product->category_name?></a></span>
                        <a href="<?php echo base_url(remove_space($product->category_name).'/'.remove_space($product->title).'/'.$product->product_id)?>">
                            <h3 class="product-name"><?php echo $product->title?></h3>
                            <div class="product-thumb">
                                <img src="<?php echo base_url()?>assets/website/img/loader.svg" data-src="<?php echo base_url().$product->thumb_image_url?>" alt="">
                            </div>
                        </a>
                        <span>
                            <b>
                            <?php
                            if ($product->brand_name) {
                                echo $product->brand_name;
                            }else{
                                echo $product->first_name.' '.$product->last_name;
                            }
                            ?>
                            </b>
                        </span>
                        <div class="price-cart">
                            <?php
                               $currency_new_id  =  $this->session->userdata('currency_new_id');

                                if (empty($currency_new_id)) {
                                    $result  =  $cur_info = $this->db->select('*')
                                                        ->from('currency_info')
                                                        ->where('default_status','1')
                                                        ->get()
                                                        ->row();
                                    $currency_new_id = $result->currency_id;
                                }

                                if (!empty($currency_new_id)) {
                                    $cur_info = $this->db->select('*')
                                                        ->from('currency_info')
                                                        ->where('currency_id',$currency_new_id)
                                                        ->get()
                                                        ->row();

                                    $target_con_rate = $cur_info->convertion_rate;
                                    $position1 = $cur_info->currency_position;
                                    $currency1 = $cur_info->currency_icon;
                                }
                            ?>
                            <?php if ($product->on_sale == 1 && !empty($product->offer_price)) { ?>
                            <span class="price">
                                <span class="price-amount">
                                    <ins><span class="amount"> 
                                    <?php
//                                        if ($target_con_rate > 1) {
                                            $price = $product->offer_price * $target_con_rate;
                                            echo (($position1==0)?$currency1." ".number_format($price, 0, '.', ',')
                                                :number_format($price, 0, '.', ',')." ".$currency1);
//                                        }
//
//                                        if ($target_con_rate <= 1) {
//                                            $price = $product->offer_price * $target_con_rate;
//                                            echo (($position1==0)?$currency1." ".number_format($price, 2, '.', ','):number_format($price, 2, '.', ',')." ".$currency1);
//                                        }
                                    ?>
                                    </span></ins>
                                    <del><span class="amount">
                                    <?php 
//                                        if ($target_con_rate > 1) {
                                            $price = $product->price * $target_con_rate;
                                            echo (($position1==0)?$currency1." ".number_format($price, 0, '.', ',')
                                                :number_format($price, 0, '.', ',')." ".$currency1);
//                                        }
//
//                                        if ($target_con_rate <= 1) {
//                                            $price = $product->price * $target_con_rate;
//                                            echo (($position1==0)?$currency1." ".number_format($price, 2, '.', ','):number_format($price, 2, '.', ',')." ".$currency1);
//                                        }
                                    ?>
                                    </span></del>
                                    
                                </span>
                            </span><!-- /.Price -->
                            <?php
                            }else{
                            ?>
                            <span class="price">
                                <span class="price-amount">
                                    <ins><span class="amount"> 
                                    <?php 
//                                        if ($target_con_rate > 1) {
                                            $price = $product->price * $target_con_rate;
                                            echo (($position1==0)?$currency1." ".number_format($price, 0, '.', ',')
                                                :number_format($price, 0, '.', ',')." ".$currency1);
//                                        }
//
//                                        if ($target_con_rate <= 1) {
//                                            $price = $product->price * $target_con_rate;
//                                            echo (($position1==0)?$currency1." ".number_format($price, 2, '.', ','):number_format($price, 2, '.', ',')." ".$currency1);
//                                        }
                                    ?>  
                                    </span></ins>
                                    
                                </span>
                            </span><!-- /.Price -->
                            <?php
                            }
                            ?>
                            <div class="rating_stars">
                                <div class="rating-wrap">
                                <?php
                                $rater = $this->Categories->get_total_rater_by_product_id($product->product_id);
                                $result = $this->Categories->get_total_rate_by_product_id($product->product_id);

                                if ($result->rates != null) {
                                    $total_rate = $result->rates/$rater;
                                    if (gettype($total_rate) == 'integer') {
                                        for ($t=1; $t <= $total_rate; $t++) {
                                           echo "<i class=\"fa fa-star\"></i>";
                                        }
                                        for ($tt=$total_rate; $tt < 5; $tt++) { 
                                            echo "<i class=\"fa fa-star-o\"></i>";
                                        }
                                    }elseif (gettype($total_rate) == 'double') {
                                        $pieces = explode(".", $total_rate);
                                        for ($q=1; $q <= $pieces[0]; $q++) {
                                           echo "<i class=\"fa fa-star\"></i>";
                                           if ($pieces[0] == $q) {
                                               echo "<i class=\"fa fa-star-half-o\"></i>";
                                               for ($qq=$pieces[0]; $qq < 4; $qq++) { 
                                                    echo "<i class=\"fa fa-star-o\"></i>";
                                                }
                                           }
                                        }

                                    }else{
                                        for ($w=0; $w <= 4; $w++) {
                                           echo "<i class=\"fa fa-star-o\"></i>";
                                        }
                                    }
                                }else{
                                    for ($o=0; $o <= 4; $o++) {
                                       echo "<i class=\"fa fa-star-o\"></i>";
                                    }
                                }
                                ?>
                                </div>
                                <div class="total-rating">(<?php echo $rater?>)</div>
                            </div><!-- Rating --> 
                        </div><!-- /.price-add-to-cart -->
                        <div class="box-bottom">
                            <?php
                            if (! $product->variant_id) {
                            ?>
                            <div class="btn-add-cart">
                                <a href="javascript:void(0)" onclick="add_to_cart('<?php echo $product->product_id?>')" title="">
                                    <img src="<?php echo base_url('assets/website/img/add-cart.png')?>" alt=""><?php echo display('add_to_cart')?>
                                </a>
                            </div>
                            <?php
                            }
                            ?>
                            <div class="compare-wishlist">
                                <a href="javascript:void(0)" class="wishlist" title="" name="<?php echo $product->product_id?>">
                                    <img src="<?php echo base_url('assets/website/img/wishlist.png')?>" alt=""><?php echo display('wishlist')?>
                                </a>
                            </div>
                        </div><!-- /.box-bottom -->
                    </div>
                </div>
                <!-- /.End of product box -->
                <?php
                    }
                }
                ?>
            </div>
        </div>
    </div>
    <?php
    }
    ?>
    <?php
        $total_rate = 0;
        $rater = $this->Categories->get_total_rater_by_product_id($product_id);
        $result = $this->Categories->get_total_rate_by_product_id($product_id);

        if ($result->rates != null) {
            $total_rate = $result->rates/$rater;
        }
    ?>
    <div class="product-details-tab">
        <div class="text-center">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab1default" data-toggle="tab"><?php echo display('specification')?></a></li>
                <li><a href="#tab2default" data-toggle="tab"><?php echo display('feedback')?> (<?php echo $rater?>)</a></li>
                <li><a href="#tab3default" data-toggle="tab"><?php echo display('description')?></a></li>
                <li><a href="#tab4default" data-toggle="tab"><?php echo display('seller_guarantee')?></a></li>
                <li><a href="#tab5default" data-toggle="tab"><?php echo display('refund_policy')?></a></li>
            </ul>
        </div>
        <div class="tab-content">
            <div class="tab-pane fade in active" id="tab1default">
                <div class="container">
                    <?php
                    $lang_id = 0;
                    $user_lang = $this->session->userdata('language');
                    if (empty($user_lang)) {
                        $lang_id = 'english';
                    }else{
                        $lang_id = $user_lang;
                    }
                    $description = $this->db->select('*')
                                        ->from('product_description')
                                        ->where('product_id',$product_id)
                                        ->where('lang_id',$lang_id)
                                        ->where('description_type',2)
                                        ->get()
                                        ->row();
                    if ($description) {
                        echo $description->description;
                    }
                    ?>
                </div>
            </div>
            <div class="tab-pane fade" id="tab2default">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-7 content">
                            <div class="rating-block-wrapper">
                                <h3 class="title"><?php echo display('ratings_and_reviews')?></h3>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="rating-block text-center">
                                            <h4><?php echo display('avg_user_rating')?></h4>
                                            <div class="rating-point center-block">
                                                <i class="glyphicon glyphicon-star"></i>
                                                <h3>
                                                <?php 
                                                if ($total_rate) {
                                                    echo number_format("$total_rate",1,".",".");
                                                }else{
                                                    echo "0";
                                                }
                                                ?></h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="rating-position">
                                            <h4><?php echo display('rating_breakdown')?></h4>
                                            <div class="rating-dimension">
                                                <div class="rating-quantity">
                                                    <div><?php echo display('5')?> <span class="glyphicon glyphicon-star"></span></div>
                                                </div>
                                                <div class="rating-percent">
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="5" style="width: 100%">
                                                            <span class="sr-only">100% Complete (success)</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="user-rating">
                                                <?php  
                                                    echo $this->Products_model->get_total_five_start_rating($product_id,5);
                                                ?>
                                                </div>
                                            </div><!-- /.End of rating dimension -->
                                            <div class="rating-dimension">
                                                <div class="rating-quantity">
                                                    <div><?php echo display('4')?> <span class="glyphicon glyphicon-star"></span></div>
                                                </div>
                                                <div class="rating-percent">
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="5" style="width: 80%">
                                                            <span class="sr-only">80% Complete (primary)</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="user-rating">
                                                <?php  
                                                    echo $this->Products_model->get_total_five_start_rating($product_id,4);
                                                ?></div>
                                            </div><!-- /.End of rating dimension -->
                                            <div class="rating-dimension">
                                                <div class="rating-quantity">
                                                    <div><?php echo display('3')?> <span class="glyphicon glyphicon-star"></span></div>
                                                </div>
                                                <div class="rating-percent">
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="5" style="width: 60%">
                                                            <span class="sr-only">60% Complete (info)</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="user-rating"> <?php  
                                                    echo $this->Products_model->get_total_five_start_rating($product_id,3);
                                                ?></div>
                                            </div><!-- /.End of rating dimension -->
                                            <div class="rating-dimension">
                                                <div class="rating-quantity">
                                                    <div><?php echo display('2')?> <span class="glyphicon glyphicon-star"></span></div>
                                                </div>
                                                <div class="rating-percent">
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="5" style="width: 40%">
                                                            <span class="sr-only">40% Complete (warning)</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="user-rating"> <?php  
                                                    echo $this->Products_model->get_total_five_start_rating($product_id,2);
                                                ?></div>
                                            </div><!-- /.End of rating dimension -->
                                            <div class="rating-dimension">
                                                <div class="rating-quantity">
                                                    <div><?php echo display('1')?> <span class="glyphicon glyphicon-star"></span></div>
                                                </div>
                                                <div class="rating-percent">
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="5" style="width: 20%">
                                                            <span class="sr-only">20% Complete (danger)</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="user-rating"> <?php  
                                                    echo $this->Products_model->get_total_five_start_rating($product_id,1);
                                                ?></div>
                                            </div><!-- /.End of rating dimension -->
                                        </div>
                                    </div>          
                                </div>  
                            </div>
                            <div class="review-block-wrapper">
                                <?php
                                if ($review_list) {
                                    foreach ($review_list as $review) {
                                ?>
                                <div class="review-block loadMoreicon" style="display:none;">
                                    <div class="review-block-rate">
                                        <button type="button" class="btn 
                                        <?php
                                        if($review->rate == 5){
                                            echo("btn-success");
                                        }elseif($review->rate == 4){
                                            echo("btn-info");
                                        }elseif($review->rate == 3){
                                            echo("btn-warning");
                                        }elseif($review->rate == 2){
                                            echo("btn-danger");
                                        }else{
                                            echo("btn-danger");
                                        }
                                        ?> btn-xs" aria-label="Left Align">
                                            <?php echo $review->rate?> <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                        </button>
                                    </div>
                                    <div class="review-block-title"><?php echo $review->title?></div>
                                    <div class="review-block-description"><?php echo $review->comments?></div>
                                    <div class="review-meta-row align-items-center justify-content-between">
                                        <div>
                                            <span class="review-block-name">
                                                <?php  
                                                    $get_customer_name = $this->Products_model->get_customer_name($review->reviewer_id);
                                                    if ($get_customer_name) {
                                                        echo $get_customer_name->customer_name;;
                                                    }
                                                ?>
                                            </span>
                                            <i class="fa fa-check-circle" aria-hidden="true"></i>
                                            <span class="review-block-date">
                                            <?php
                                                $format = 'DATE_RFC822';
                                                $time = time();
                                                echo standard_date($format, strtotime($review->date_time));
                                                echo " ";
                                             
                                                $now = time();
                                                $units = 1;
                                                echo timespan(strtotime($review->date_time), $now, $units);
                                                echo display('ago');
                                            ?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                    }
                                }
                                ?>
                                <div class="review-footer">
                                    <div class="view-all-review">
                                        <a href="javascript:void(0)" id="loadMore"><?php echo display('view_more')?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-5 rightSidebar">
                            <div class="review-content">         
                                <div class="review-product-info">
                                    <h4 class="review-product-name"><?php echo $product_name?></h4>
                                    <div class="review-product-brand"><?php echo $category_name?></div>
                                </div>
                                <form class="review-form">
                                    <div class="rating-content">
                                        <label for="input-1" class="control-label"><?php echo display('rate_it')?>: *</label>
                                        <input type="text" id="input-1" name="rating" class="rating rating-loading" data-min="0" data-max="5" data-step="1" value="2">
                                    </div>
                                    <div class="form-group">
                                        <label><?php echo display('title')?> *</label>
                                        <input type="text" name="title" class="form-control" id="title" placeholder="<?php echo display('title')?>">
                                        <input type="hidden" name="product_id" value="<?php echo $product_id?>">
                                    </div>
                                    <div class="form-group">
                                        <label><?php echo display('review')?> *</label>
                                        <textarea class="form-control" name="review"  placeholder="<?php echo display('review')?>" rows="5" id="review"></textarea>
                                    </div>
                                    <input type="button" id="review_submit" class="btn btn-warning" value="<?php echo display('submit')?>">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Rating or review product -->
            <div class="tab-pane fade" id="tab3default">
                <div class="container">
                    <div class="product-description">
                      <?php
                        $lang_id = 0;
                        $user_lang = $this->session->userdata('language');
                        if (empty($user_lang)) {
                            $lang_id = 'english';
                        }else{
                            $lang_id = $user_lang;
                        }
                        $description = $this->db->select('*')
                                            ->from('product_description')
                                            ->where('product_id',$product_id)
                                            ->where('lang_id',$lang_id)
                                            ->where('description_type',1)
                                            ->get()
                                            ->row();
                        if ($description) {
                            echo $description->description;
                        }
                        ?>
                    </div>
                    <!-- /.End of product description -->
                </div>
            </div>
            <div class="tab-pane fade" id="tab4default">
                <div class="container">
                    <div class="product-description">
                      <?php
                        if ($seller_guarantee) {
                            echo $seller_guarantee;
                        }
                        ?>
                    </div>
                    <!-- /.End of product description -->
                </div>
            </div>
            <div class="tab-pane fade" id="tab5default">
                <div class="container">
                    <div class="product-description">
                      <?php
                        if ($refund_policy) {
                            echo $refund_policy;
                        }
                        ?>
                    </div>
                    <!-- /.End of product description -->
                </div>
            </div>
        </div>
    </div>
    <?php
    if ($best_sales_category) {
    ?>
    <div class="similar-products-content">
        <div class="container">
            <h3 class="title-widget"><span><?php echo display('you_may_alo_be_intersted_in')?></span></h3>
            <div class="products-slide">
                <?php
                if ($best_sales_category) {
                    foreach ($best_sales_category as $product) {
                ?>
                <div class="product-box">
                    <div class="imagebox">
                        <span class="product-cat"><a href="<?php echo base_url(remove_space($product->category_name).'/'.$product->category_id)?>"><?php echo $product->category_name?></a></span>
                        <a href="<?php echo base_url(remove_space($product->category_name).'/'.remove_space($product->title).'/'.$product->product_id)?>">
                            <h3 class="product-name"><?php echo $product->title?></h3>
                            <div class="product-thumb">
                                <img src="<?php echo base_url()?>assets/website/img/loader.svg" data-src="<?php echo base_url().$product->thumb_image_url?>" alt="">
                            </div>
                        </a>
                        <span>
                          <b>
                            <?php
                            if ($product->brand_name) {
                                echo $product->brand_name;
                            }else{
                                echo $product->first_name.' '.$product->last_name;
                            }
                            ?>
                            </b>
                        </span>
                        <div class="price-cart">
                            <?php
                               $currency_new_id  =  $this->session->userdata('currency_new_id');
                                if (empty($currency_new_id)) {
                                    $result  =  $cur_info = $this->db->select('*')
                                                        ->from('currency_info')
                                                        ->where('default_status','1')
                                                        ->get()
                                                        ->row();
                                    $currency_new_id = $result->currency_id;
                                }

                                if (!empty($currency_new_id)) {
                                    $cur_info = $this->db->select('*')
                                                    ->from('currency_info')
                                                    ->where('currency_id',$currency_new_id)
                                                    ->get()
                                                    ->row();

                                    $target_con_rate = $cur_info->convertion_rate;
                                    $position1 = $cur_info->currency_position;
                                    $currency1 = $cur_info->currency_icon;
                                }
                            ?>

                            <?php if ($product->on_sale == 1 && !empty($product->offer_price)) { ?>
                            <span class="price">
                                <span class="price-amount">
                                    <ins><span class="amount"> 
                                    <?php
//                                        if ($target_con_rate > 1) {
                                            $price = $product->offer_price * $target_con_rate;
                                            echo (($position1==0)?$currency1." ".number_format($price, 0, '.', ',')
                                                :number_format($price, 0, '.', ',')." ".$currency1);
//                                        }
//
//                                        if ($target_con_rate <= 1) {
//                                            $price = $product->offer_price * $target_con_rate;
//                                            echo (($position1==0)?$currency1." ".number_format($price, 2, '.', ','):number_format($price, 2, '.', ',')." ".$currency1);
//                                        }
                                    ?>
                                    </span></ins>
                                    <del><span class="amount">
                                    <?php 
//                                        if ($target_con_rate > 1) {
                                            $price = $product->price * $target_con_rate;
                                            echo (($position1==0)?$currency1." ".number_format($price, 0, '.', ',')
                                                :number_format($price, 0, '.', ',')." ".$currency1);
//                                        }
//
//                                        if ($target_con_rate <= 1) {
//                                            $price = $product->price * $target_con_rate;
//                                            echo (($position1==0)?$currency1." ".number_format($price, 2, '.', ','):number_format($price, 2, '.', ',')." ".$currency1);
//                                        }
                                    ?>
                                    </span></del>
                                    
                                </span>
                            </span><!-- /.Price -->
                            <?php
                            }else{
                            ?>
                            <span class="price">
                                <span class="price-amount">
                                    <ins><span class="amount"> 
                                    <?php 
//                                        if ($target_con_rate > 1) {
                                            $price = $product->price * $target_con_rate;
                                            echo (($position1==0)?$currency1." ".number_format($price, 0, '.', ',')
                                                :number_format($price, 0, '.', ',')." ".$currency1);
//                                        }
//
//                                        if ($target_con_rate <= 1) {
//                                            $price = $product->price * $target_con_rate;
//                                            echo (($position1==0)?$currency1." ".number_format($price, 2, '.', ','):number_format($price, 2, '.', ',')." ".$currency1);
//                                        }
                                    ?>  
                                    </span></ins>
                                    
                                </span>
                            </span><!-- /.Price -->
                            <?php
                            }
                            ?>
                            <div class="rating_stars">
                                <div class="rating-wrap">
                                <?php
                                $rater = $this->Categories->get_total_rater_by_product_id($product->product_id);
                                $result = $this->Categories->get_total_rate_by_product_id($product->product_id);

                                if ($result->rates != null) {
                                    $total_rate = $result->rates/$rater;
                                    if (gettype($total_rate) == 'integer') {
                                        for ($t=1; $t <= $total_rate; $t++) {
                                           echo "<i class=\"fa fa-star\"></i>";
                                        }
                                        for ($tt=$total_rate; $tt < 5; $tt++) { 
                                            echo "<i class=\"fa fa-star-o\"></i>";
                                        }
                                    }elseif (gettype($total_rate) == 'double') {
                                        $pieces = explode(".", $total_rate);
                                        for ($q=1; $q <= $pieces[0]; $q++) {
                                           echo "<i class=\"fa fa-star\"></i>";
                                           if ($pieces[0] == $q) {
                                               echo "<i class=\"fa fa-star-half-o\"></i>";
                                               for ($qq=$pieces[0]; $qq < 4; $qq++) { 
                                                    echo "<i class=\"fa fa-star-o\"></i>";
                                                }
                                           }
                                        }

                                    }else{
                                        for ($w=0; $w <= 4; $w++) {
                                           echo "<i class=\"fa fa-star-o\"></i>";
                                        }
                                    }
                                }else{
                                    for ($o=0; $o <= 4; $o++) {
                                       echo "<i class=\"fa fa-star-o\"></i>";
                                    }
                                }
                                ?>
                                </div>
                                <div class="total-rating">(<?php echo $rater?>)</div>
                            </div><!-- Rating --> 
                        </div><!-- /.price-add-to-cart -->
                        <div class="box-bottom">
                            <?php
                            if (! $product->variant_id) {
                            ?>
                            <div class="btn-add-cart">
                                <a href="javascript:void(0)" onclick="add_to_cart('<?php echo $product->product_id?>')" title="">
                                    <img src="<?php echo base_url('assets/website/img/add-cart.png')?>" alt=""><?php echo display('add_to_cart')?>
                                </a>
                            </div>
                            <?php
                            }
                            ?>
                            <div class="compare-wishlist">
                                <a href="javascript:void(0)" class="wishlist" title="" name="<?php echo $product->product_id?>">
                                    <img src="<?php echo base_url('assets/website/img/wishlist.png')?>" alt=""><?php echo display('wishlist')?>
                                </a>
                            </div>
                        </div><!-- /.box-bottom -->
                    </div>
                </div>
                <?php
                    }
                }
                ?>
            </div>
        </div>
    </div>
    <?php
    }
    ?>
</div>
<!-- Rating or review product -->
<div class="text-center" id="sub_msg"></div>
<div class="newsletter">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-7">
                <h5 class="newsletter-title"><?php echo display('sign_up_to_newsletter')?></h5>
                <span class="newsletter-marketing-text"><?php echo display('sign_up_for_news_and')?> <strong><?php echo display('offers')?></strong></span>
            </div>
            <div class="col-xs-12 col-sm-5">
                <form>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="<?php echo display('enter_your_email')?>" id="sub_email">
                        <span class="input-group-btn">
                            <button class="btn btn-secondary" type="button" id="smt_btn"><?php echo display('sign_up')?></button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /.End of newsletter -->
<script type="text/javascript">  
    //Subscribe entry
    $('body').on('click', '#smt_btn', function() {
        var sub_email = $('#sub_email').val();
        if (sub_email == 0) {
            alert('<?php echo display('please_enter_your_email')?>');
            return false;
        }
        var check_email =  validateEmail(sub_email);

        if (check_email == true) {
            $.ajax({
                type: "post",
                async: true,
                url: '<?php echo base_url('website/Home/add_subscribe')?>',
                data: {sub_email:sub_email},
                success: function(data) {
                    if (data == '2') {
                        $("#sub_msg").html('<p style="color:green"><?php echo display('subscribe_successfully')?></p>');
                        $('#sub_msg').hide().fadeIn('slow');
                        $('#sub_msg').fadeIn(700);
                        $('#sub_msg').hide().fadeOut(2000);
                        $("#sub_email").val(" ");
                    }else{
                        $("#sub_msg").html('<p style="color:red"><?php echo display('failed')?></p>'); 
                        $('#sub_msg').hide().fadeIn('slow');
                        $('#sub_msg').fadeIn(700);
                        $('#sub_msg').hide().fadeOut(2000);
                        $("#sub_email").val(" ");
                    }
                },
                error: function() {
                    alert('Request Failed, Please check your code and try again!');
                }
            });
        }else{
            alert('<?php echo display('please_enter_valid_email')?>');
            return false;
        }
        //Valid email cheker
        function validateEmail(email) 
        {
            var re = /\S+@\S+\.\S+/;
            return re.test(email);
        }
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        //Add review
        $('body').on('click', '#review_submit', function() {

            var product_id  = '<?php echo $product_id?>';
            var title       = $('#title').val();
            var review_msg  = $('#review').val();
            var customer_id = '<?php echo $this->session->userdata('customer_id')?>';
            var rate        = $('#input-1').val();

            //Review msg check
            if (review_msg == 0 || title == 0) {
                alert('<?php echo display('please_write_your_comment')?>');
                return false;
            }

            //Customer id check
            if (customer_id == 0) {
                alert('<?php echo display('please_login_first')?>');
                return false;
            }

            $.ajax({
                type: "post",
                async: true,
                url: '<?php echo base_url('website/product/submit_review')?>',
                data: {product_id:product_id,customer_id:customer_id,review_msg:review_msg,rate:rate,title:title},
                success: function(data) {
                    if (data == '1') {
                        $('#review_msg').val('');
                        alert('<?php echo display('your_review_added')?>');
                        location.reload();
                    }else if(data == '2'){
                        $('#review_msg').val('');
                        alert('<?php echo display('thanks_you_already_reviewed')?>');
                        location.reload();
                    }else if(data == '3'){
                        $('#review_msg').val('');
                        alert('<?php echo display('please_login_first')?>');
                        location.reload();
                    }
                },
                error: function() {
                    alert('Request Failed, Please check your code and try again!');
                }
            });
        });
    });
</script>
<script type="text/javascript">
    //Add wishlist
    $('body').on('click', '.wishlist', function() {
        var product_id  = $(this).attr('name');
        var customer_id = '<?php echo $this->session->userdata('customer_id')?>';
        if (customer_id == 0) {
            alert('Please login first !');
            return false;
        }
        $.ajax({
            type: "post",
            async: true,
            url: '<?php echo base_url('website/Home/add_wishlist')?>',
            data: {product_id:product_id,customer_id:customer_id},
            success: function(data) {
                if (data == '1') {
                    alert('<?php echo display('product_added_to_wishlist')?>');
                }else if(data == '2'){
                    alert('<?php echo display('product_already_exists_in_wishlist')?>')
                }else if(data == '3'){
                    alert('<?php echo display('please_login_first')?>')
                }
            },
            error: function() {
                alert('Request Failed, Please check your code and try again!');
            }
        });
    });  
</script>
<script type="text/javascript">
    //load more icon slice
    $(".loadMoreicon").slice(0, 5).show();
        if ($(".loadMoreicon:hidden").length == 0) {
            $("#loadMore").hide();
    }
    $('body').on('click','#loadMore',function(e) {
        e.preventDefault();
        $(".loadMoreicon:hidden").slice(0, 5).slideDown();
        if ($(".loadMoreicon:hidden").length == 0) {
            $("#load").fadeOut('slow');
            $("#loadMore").hide();
        }
    });
    //Load more icon end
</script>
<!-- Add to cart product -->
<script type="text/javascript">
    //Add to cart by ajax
    function cart_btn(product_id){

        var qnty     = $('#qty').val();
        var variant  = $('input[name=size]:checked').val();
        var stock    = <?php echo $quantity?>;

        if (stock < qnty) {
            alert('<?php echo display('you_can_not_buy_more_than')?> '+stock);
            return false;
        }

        if (product_id == 0) {
            alert('<?php echo display('please_select_product')?>');
            return false;
        }
        if (qnty <= 0) {
            alert('<?php echo display('please_keep_quantity_up_to_zero')?>');
            return false;
        }
        
        $.ajax({
            type: "post",
            async: true,
            url: '<?php echo base_url('website/Home/add_to_cart_details')?>',
            data: {product_id:product_id,qnty:qnty,variant:variant},
            success: function(data) {
                if (data == 1) {
                    alert('<?php echo display('product_added_to_cart')?>');
                    $("#tab_up_cart").load(location.href+" #tab_up_cart>*","");
                }else if(data == 2){
                    alert('<?php echo display('out_of_stock')?>');
                }else if(data == 3){
                    alert('<?php echo display('you_can_not_buy_more_than')?> '+stock);
                    return false;
                }
            },
            error: function() {
                alert('Request Failed, Please check your code and try again!');
            }
        });
    }

    //Add to cart and buy now by ajax
    function buy_now(product_id){
        var qnty     = $('#qty').val();
        var variant  = $('input[name=size]:checked').val();
        var stock    = <?php echo $quantity?>;

        if (stock < qnty) {
            alert('<?php echo display('you_can_not_buy_more_than')?> '+stock);
            return false;
        }

        if (product_id == 0) {
            alert('<?php echo display('please_select_product')?>');
            return false;
        }
        if (qnty <= 0) {
            alert('<?php echo display('please_keep_quantity_up_to_zero')?>');
            return false;
        }
        
        $.ajax({
            type: "post",
            async: true,
            url: '<?php echo base_url('website/Home/add_to_cart_details')?>',
            data: {product_id:product_id,qnty:qnty,variant:variant},
            success: function(data) {
                if (data == 1) {
                    window.location.href = '<?php echo base_url('checkout')?>';
                }else if(data == 2){
                    alert('<?php echo display('out_of_stock')?>');
                }else if(data == 3){
                    alert('<?php echo display('you_can_not_pre_order_more_than')?> '+stock);
                }
            },
            error: function() {
                alert('Request Failed, Please check your code and try again!');
            }
        });
    }

    //Add to cart and buy now by ajax
    function pre_order(product_id){
        var qnty     = $('#qty').val();
        var variant  = $('input[name=size]:checked').val();
        var stock    = <?php echo $pre_order_quantity?>;

        if (stock < qnty) {
            alert('<?php echo display('you_can_not_pre_order_more_than')?> '+stock);
            return false;
        }

        if (product_id == 0) {
            alert('<?php echo display('please_select_product')?>');
            return false;
        }
        if (qnty <= 0) {
            alert('<?php echo display('please_keep_quantity_up_to_zero')?>');
            return false;
        }
        
        $.ajax({
            type: "post",
            async: true,
            url: '<?php echo base_url('website/Home/add_to_cart_details')?>',
            data: {product_id:product_id,qnty:qnty,variant:variant},
            success: function(data) {
                if (data == 1) {
                    alert('<?php echo display('product_added_to_cart')?>');
                    $("#tab_up_cart").load(location.href+" #tab_up_cart>*","");
                }else if(data == 2){
                    alert('<?php echo display('out_of_stock')?>');
                }else if(data == 3){
                    alert('<?php echo display('you_can_not_pre_order_more_than')?> '+stock);
                }
            },
            error: function() {
                alert('Request Failed, Please check your code and try again!');
            }
        });
    }
</script>