<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<div class="page-breadcrumbs">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('home')?>"><?php echo display('home')?></a></li>
            <li class="active"><?php echo display('view_cart')?></li>
        </ol>
    </div>
</div>
<!-- /.End of page breadcrumbs -->
<?php
if ($this->cart->contents()) {
?>
<div class="cart-list-content">
    <div class="container">
        <?php
            $message = $this->session->userdata('message');
            if (isset($message)) {
        ?>
           <div class="alert alert-success alert-dismissible">
              <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
              <strong><?php echo $message ?></strong>
            </div>
        <?php 
            $this->session->unset_userdata('message');
            }
            $error_message = $this->session->userdata('error_message');
            if (isset($error_message)) {
        ?>
          <div class="alert alert-danger alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
            <strong><?php echo $error_message ?></strong>
          </div>
        <?php 
            $this->session->unset_userdata('error_message');
            }
        ?>
        <div class="row">
            <div class="col-sm-12">
                <h1><?php echo display('cart')?></h1>
                <?php echo form_open('home/update_cart',array('id'=>'update_cart','method'=>'post')); ?>
                <div class="shopping-cart-list">
                    <div class="column-labels">
                        <label class="product-image"><?php echo display('image')?></label>
                        <label class="product-details"><?php echo display('product')?></label>
                        <label class="price-title"><?php echo display('price')?></label>
                        <label class="product-quantity"><?php echo display('in_stock')?></label>
                        <label class="product-quantity"><?php echo display('quantity')?></label>
                        <label class="product-removal"><?php echo display('remove')?></label>
                        <label class="total-price-title"><?php echo display('total')?></label>
                        <label class="total-price-title"><?php echo display('status')?></label>
                    </div>
                    <?php $i = 1; $cgst = 0; $sgst = 0; $igst = 0; $discount = 0; ?>
                    <?php foreach ($this->cart->contents() as $items): ?>
                    <?php echo form_hidden($i.'[rowid]', $items['rowid']);

                        if (!empty($items['options']['cgst'])) {
                           $cgst = $cgst + ($items['options']['cgst'] * $items['qty']);
                        }

                        if (!empty($items['options']['sgst'])) {
                           $sgst = $sgst + ($items['options']['sgst'] * $items['qty']);
                        }

                        if (!empty($items['options']['igst'])) {
                           $igst = $igst + ($items['options']['igst'] * $items['qty']);
                        }

                        //Calculation for discount
                        if (!empty($items['discount'])) {
                           $discount = $discount + ($items['discount'] * $items['qty']) + $this->session->userdata('coupon_amnt');
                           $this->session->set_userdata('total_discount',$discount);
                        }
                    ?>
                    <div class="product-cart-list">
                        <div class="product-image">
                            <img src="<?php echo base_url().$items['options']['image']; ?>" alt="">
                        </div>
                        <div class="product-details">
                            <div class="product-name"><?php echo $items['name']; ?></div>
                            <p class="product-description"><?php echo $items['options']['model']; ?></p>
                        </div>
                        <div class="cart-product-price"><?php echo (($position==0)?$currency." ".$this->cart->format_number($items['price']):$this->cart->format_number($items['price'])." ".$currency) ?></div>
                        <div class="product-quantity">
                        <?php
                        if ($items['pre_order'] == 1) {
                            $stock = $this->db->select('pre_order_quantity')
                                    ->from('product_information')
                                    ->where('product_id',$items['product_id'])
                                    ->get()
                                    ->row();
                            echo $stock->pre_order_quantity;
                        }else{
                            $stock = $this->db->select('quantity')
                                    ->from('product_information')
                                    ->where('product_id',$items['product_id'])
                                    ->get()
                                    ->row();
                            echo $stock->quantity;
                        }
                        ?>
                        </div>
                        <div class="product-quantity">
                            <?php echo form_input(array('type' => 'number','class' => 'text-center auto_adjust_amnt','name' => 'qty', 'value' => $items['qty'], 'maxlength' => '3', 'size' => '5','min'=>'1')); ?>
                            <?php echo form_input(array('type' => 'hidden','name' =>'product_id', 'value' => $items['product_id'])); ?>
                            <?php echo form_input(array('type' => 'hidden','name' =>'variant_id', 'value' => $items['variant'])); ?>
                        </div>
                        <div class="product-removal">
                            <a href="<?php echo base_url('website/home/delete_cart_by_click/'.$items['rowid'])?>">
                                <span class="lnr lnr-trash"></span>
                            </a>
                        </div>
                        <div class="total-price"><?php echo (($position==0)?$currency." ".$this->cart->format_number($items['subtotal']):$this->cart->format_number($items['subtotal'])." ".$currency) ?></div>
                        <div class="total-price"><?php
                        if ($items['pre_order'] == 1) {
                           echo display('pre_order');
                        }else{
                            echo display('order');
                        }
                        ?></div>
                    </div>
                    <!-- /.End of product cart list -->
                    <?php $i++; ?>
                    <?php endforeach; ?>
                </div>
                <div class="btn-row">
                    <div>
                        <a href="<?php echo base_url()?>" class="btn btn-default"><?php echo display('continue_shopping')?></a>
                    </div>
                    <div>
                        <a href="<?php echo base_url('website/home/destroy_cart/')?>" class="btn btn-danger mr-5"><?php echo display('clear_cart')?></a>
                    </div>
                </div>
                <?php echo form_close()?>
            </div>
        </div>
    </div>
</div>
<div class="calculate-content">
    <div class="container">
        <div class="row">

            <div class="col-sm-4">
                <div class="coupon-form">
                    <h2><?php echo display('coupon_code')?></h2>
                    <p><?php echo display('enter_your_coupon_here')?></p>
                    <form class="coupon" action="<?php echo base_url('home/apply_coupon')?>" method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" name="coupon_code" placeholder="<?php echo display('enter_your_coupon_here')?>" required="">
                        </div>
                        <button type="submit" class="btn btn-default"><?php echo display('apply_coupon')?></button>
                    </form>
                </div>
                <!-- /.End of coupon -->
            </div>
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
                <div class="cart-totals">
                    <h2><?php echo display('cart_total')?></h2>
                    <div class="cart-totals-border">
                        <div class="totals-item">
                            <label><?php echo display('sub_total')?></label>
                            <div class="totals-value" id="cart-subtotal"><?php echo (($position==0)?$currency.number_format($this->cart->total(), 2, '.', ','):number_format($this->cart->total(), 2, '.', ',').$currency)?></div>
                        </div>
                        <?php
                        $coupon_amnt = $this->session->userdata('coupon_amnt');
                        if ($coupon_amnt > 0) {
                        ?>
                        <div class="totals-item">
                            <label><?php echo display('coupon_discount')?></label>
                            <div class="totals-value" id="cart-tax"><?php
                            if ($coupon_amnt > 0) {
                            echo (($position==0)?$currency.number_format($coupon_amnt, 2, '.', ','):number_format($coupon_amnt, 2, '.', ',').$currency);
                            }
                            ?></div>
                        </div>
                        <?php
                        }
                        ?>
                        <div class="totals-item totals-item-total">
                            <label><?php echo display('grand_total')?></label>
                            <div class="totals-value" id="cart-total"><?php
                                $cart_total = $this->cart->total() - $this->session->userdata('coupon_amnt');
                                $this->session->set_userdata('cart_total',$cart_total);
                                $total_amnt = $this->_cart_contents['cart_total'] = $cart_total;
                                echo (($position==0)?$currency.number_format($total_amnt, 2, '.', ','):number_format($total_amnt, 2, '.', ',').$currency);
                            ?></div>
                        </div>
                    </div>
                    <a href="<?php echo base_url('checkout')?>" class="btn checkout"><?php echo display('proceed_to_checkout')?></a>
                </div>
                <!-- /.End of cart totals -->
            </div>
        </div>
    </div>
</div>
<?php }else{ ?>
<div class="cart-list-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="shopping-cart-list">
                    <div class="column-labels">
                       <?php echo display('oops_your_cart_is_empty')?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<!-- Update cart by on change quantity -->
<script type="text/javascript">
    //Auto adjustment amount of cart by ajax
    $(".auto_adjust_amnt").on("keyup change", function() {
        var qty = $(this).val();
        var product_id = $(this).next().val();
        var variant = $(this).next().next().val();
        $.ajax({
            type:'POST',
            url: '<?php echo base_url('website/home/update_cart')?>',
            data:{qty:qty,product_id:product_id,variant:variant},
            cache:false,
            success:function(data){
                if (data == 1) {
                    location.reload();
                }else if(data == 2){
                    location.reload();
                }else if(data == 3){
                    location.reload();
                }
            },
            error: function(data){
                console.log("error");
            }
        });

    });
</script>
