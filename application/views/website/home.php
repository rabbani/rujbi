<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="page-content">
    <div class="container">
        <?php
        $message = $this->session->userdata('message');
        if (isset($message)) {
            ?>
            <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                <strong><?php echo $message ?></strong>
            </div>
            <?php
            $this->session->unset_userdata('message');
        }
        $error_message = $this->session->userdata('error_message');
        if (isset($error_message)) {
            ?>
            <div class="alert alert-danger alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                <strong><?php echo $error_message ?></strong>
            </div>
            <?php
            $this->session->unset_userdata('error_message');
        }
        ?>
        <div class="content-row">
            <div class="col col-1 hidden-xs hidden-sm">
                <nav class="vertical-menu-content">
                    <ul class="list-group vertical-menu yamm make-absolute">
                        <li class="list-group-item"><span><i
                                        class="fa fa-list-ul"></i> <?php echo display('all_departments') ?></span></li>
                        <?php
                        if ($category_list) {
                            foreach ($category_list as $parent_category) {
                                $sub_parent_cat = $this->db->select('*')
                                    ->from('product_category')
                                    ->where('parent_category_id', $parent_category->category_id)
                                    ->order_by('menu_pos')
                                    ->get()
                                    ->result();
                                ?>
                                <li class="width-md menu-item menu-item-has-children animate-dropdown dropdown">
                                    <a title="<?php echo $parent_category->category_name ?>" data-hover="dropdown"
                                       href="<?php
                                       echo base_url(remove_space($parent_category->category_name) . '/'
                                           . $parent_category->category_id) ?>"
                                       data-toggle="<?php if ($sub_parent_cat) {
                                           echo "dropdown";
                                       } else {
                                           echo "";
                                       } ?>" class="dropdown-toggle text-capitalize" aria-haspopup="true"><span
                                                class=""><img src="<?php echo $parent_category->cat_favicon ?>"
                                                              height="15"
                                                              width="16"></span> <?php echo $parent_category->category_name ?>
                                        <div class="hover-fix"></div>
                                    </a>
                                    <?php
                                    if ($sub_parent_cat) {
                                        ?>
                                        <ul role="menu" class="dropdown-menu">
                                            <li class="menu-item animate-dropdown menu-item-object-static_block">
                                                <div class="yamm-content">
                                                    <div class="row">
                                                        <?php foreach ($sub_parent_cat as $parent_cat) { ?>
                                                            <div class="col-sm-6">
                                                                <div class="column-inner">
                                                                    <ul class="nav-categories">
                                                                        <li class="nav-title">
                                                                            <a
                                                                                    href="<?php echo base_url
                                                                                    (remove_space
                                                                                        ($parent_cat->category_name) .
                                                                                    '/' . $parent_cat->category_id) ?>"><?php echo $parent_cat->category_name ?></a>
                                                                        </li>
                                                                        <?php
                                                                        $sub_cat = $this->db->select('*')
                                                                            ->from('product_category')
                                                                            ->where('parent_category_id', $parent_cat->category_id)
                                                                            ->order_by('menu_pos')
                                                                            ->get()
                                                                            ->result();
                                                                        if ($sub_cat) {
                                                                            foreach ($sub_cat as $s_p_cat) {
                                                                                ?>
                                                                                <li>
                                                                                    <a href="<?php echo base_url(remove_space($s_p_cat->category_name) . '/' . $s_p_cat->category_id) ?>"><?php echo $s_p_cat->category_name ?></a>
                                                                                </li>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    <?php } ?>
                                </li>
                                <?php
                            }
                        }
                        ?>
                    </ul>
                </nav>
            </div>

            <div class="col col-2">
                <div class="slider">
                    <?php
                    if ($slider_list_home) {
                        foreach ($slider_list_home as $slider) {

                            ?>
                            <div class="item">
                                <a href="<?php echo $slider['slider_link'] ?>" target="_blank">
                                    <img src="<?php echo $slider['slider_image'] ?>" class="img-responsive slider-img"
                                         alt="sliderImage">
                                </a>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
                <!-- /.End of slider -->
            </div>

            <div class="col col-3 promotion-product-content hidden-xs hidden-md">
                <h3 class="promotion-title"><a href="javascript:void(0)"><?php echo display('buy_now_promotions') ?></a>
                </h3>
                <?php
                if ($promotion_product) {
                    foreach ($promotion_product as $product) {
                        $select_single_category = $this->Categories->select_single_category_by_id($product->category_id);
                        ?>
                        <div class="promotion-product">
                            <a class="media"
                               href="<?php echo base_url(remove_space($select_single_category->category_name) . '/' . remove_space($product->title) . '/' . $product->product_id) ?>">
                                <div class="media-left">
                                    <img src="<?php echo base_url() . $product->thumb_image_url ?>" class="media-object"
                                         alt="<?php echo $product->title ?>">
                                </div>
                                <div class="media-body">
                                    <h4 class="product-title"><?php echo $product->title ?></h4>
                                    <?php if ($product->details) { ?>
                                        <div class="offer">
                                            <?php echo $product->details; ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </a>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
        </div>
    </div>
    <div class="banner-content">
        <div class="container text-center">
            <?php
            if ($select_home_adds) {
                foreach ($select_home_adds as $adds) {
                    if ($adds->adv_position == 1) {
                        echo $adds->adv_code;
                    }
                }
            }
            ?>
        </div>
    </div>
    <!-- /.End of Banner -->
    <div class="container">
        <!-- Static style for -->
        <div class="products-content">
            <!-- /.End of header title -->
            <div class="products-row">
                <div class="category-col">
                    <ul class="nav nav-pills">
                        <?php if ($best_sales) { ?>
                            <li class="active"><a href="#product-tab-1"
                                                  data-toggle="tab"><?php echo display('best_sale_product') ?></a></li>
                            <?php
                        }
                        if ($most_popular_product) {
                            ?>
                            <li><a href="#product-tab-2"
                                   data-toggle="tab"><?php echo display('most_popular_product') ?></a></li>
                        <?php } ?>
                        <li><a href="#product-tab-3"
                               data-toggle="tab"><?php echo display('best_merchant_product') ?></a></li>
                    </ul>
                </div>
                <!-- /.Sidebar category nav -->
                <div class="product-col">
                    <div class="tab-content">
                        <?php if ($best_sales) { ?>
                            <div class="tab-pane product-tab-pane active" id="product-tab-1">
                                <div class="products-slide">
                                    <?php foreach ($best_sales as $product) { ?>
                                        <div class="product-box">
                                            <div class="imagebox">
                                                <span class="product-cat"><a
                                                            href="<?php echo base_url(remove_space($product->category_name) . '/' . $product->category_id) ?>"><?php echo $product->category_name ?></a></span>
                                                <a href="<?php echo base_url(remove_space($product->category_name) . '/' . remove_space($product->title) . '/' . $product->product_id) ?>">
                                                    <h3 class="product-name"><?php echo $product->title ?></h3>
                                                    <div class="product-thumb">
                                                        <img src="<?php echo base_url() ?>assets/website/img/loader.svg"
                                                             data-src="<?php echo base_url() . $product->thumb_image_url ?>"
                                                             alt="">
                                                    </div>
                                                </a>
                                                <div><b>
                                                        <?php
                                                        if ($product->brand_name) {
                                                            echo $product->brand_name;
                                                        } else {
                                                            echo $product->first_name . ' ' . $product->last_name;
                                                        }
                                                        ?>
                                                    </b></div>
                                                <div class="price-cart">
                                                    <?php
                                                    $currency_new_id = $this->session->userdata('currency_new_id');

                                                    if (empty($currency_new_id)) {
                                                        $result = $cur_info = $this->db->select('*')
                                                            ->from('currency_info')
                                                            ->where('default_status', '1')
                                                            ->get()
                                                            ->row();
                                                        $currency_new_id = $result->currency_id;
                                                    }

                                                    if (!empty($currency_new_id)) {
                                                        $cur_info = $this->db->select('*')
                                                            ->from('currency_info')
                                                            ->where('currency_id', $currency_new_id)
                                                            ->get()
                                                            ->row();

                                                        $target_con_rate = $cur_info->convertion_rate;
                                                        $position1 = $cur_info->currency_position;
                                                        $currency1 = $cur_info->currency_icon;
                                                    }
                                                    ?>

                                                    <?php if ($product->on_sale == 1 && !empty($product->offer_price)) { ?>
                                                        <span class="price">
                                                <span class="price-amount">
                                                    <ins><span class="amount">
                                                    <?php
//                                                    if ($target_con_rate > 1) {
                                                        $price = $product->offer_price * $target_con_rate;
                                                        echo(($position1 == 0) ? $currency1 . " " . number_format
                                                            ($price, 0, '.', ',') : number_format($price, 0, '.', ',
                                                            ') . " " . $currency1);
//                                                    }
//                                                    if ($target_con_rate <= 1) {
//                                                        $price = $product->offer_price * $target_con_rate;
//                                                        echo(($position1 == 0) ? $currency1 . " " . number_format
//                                                            ($price, 0, '.', ',') : number_format($price, 0, '.', ',
//                                                            ') . " " . $currency1);
//                                                    }
                                                    ?>
                                                    </span></ins>
                                                    <del><span class="amount">
                                                    <?php
//                                                    if ($target_con_rate > 1) {
                                                        $price = $product->price * $target_con_rate;
                                                        echo(($position1 == 0) ? $currency1 . " " . number_format
                                                            ($price, 0, '.', ',') : number_format($price, 0, '.', ',
                                                            ') . " " . $currency1);
//                                                    }
//
//                                                    if ($target_con_rate <= 1) {
//                                                        $price = $product->price * $target_con_rate;
//                                                        echo(($position1 == 0) ? $currency1 . " " . number_format($price, 2, '.', ',') : number_format($price, 2, '.', ',') . " " . $currency1);
//                                                    }
                                                    ?>
                                                    </span></del>
                                                    <span class="amount"> </span>
                                                </span>
                                            </span><!-- /.Price -->
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <span class="price">
                                                <span class="price-amount">
                                                    <ins><span class="amount">
                                                    <?php
//                                                    if ($target_con_rate > 1) {
                                                        $price = $product->price * $target_con_rate;
                                                        echo(($position1 == 0) ? $currency1 . " " . number_format
                                                            ($price, 0, '.', ',') : number_format($price, 0, '.', ',
                                                            ') . " " . $currency1);
//                                                    }
//
//                                                    if ($target_con_rate <= 1) {
//                                                        $price = $product->price * $target_con_rate;
//                                                        echo(($position1 == 0) ? $currency1 . " " . number_format($price, 2, '.', ',') : number_format($price, 2, '.', ',') . " " . $currency1);
//                                                    }
                                                    ?>
                                                    </span></ins>
                                                    <span class="amount"> </span>
                                                </span>
                                            </span><!-- /.Price -->
                                                        <?php
                                                    }
                                                    ?>
                                                    <div class="rating_stars">
                                                        <div class="rating-wrap">
                                                            <?php
                                                            $rater = $this->Categories->get_total_rater_by_product_id($product->product_id);
                                                            $result = $this->Categories->get_total_rate_by_product_id($product->product_id);

                                                            if ($result->rates != null) {
                                                                $total_rate = $result->rates / $rater;
                                                                if (gettype($total_rate) == 'integer') {
                                                                    for ($t = 1; $t <= $total_rate; $t++) {
                                                                        echo "<i class=\"fa fa-star\"></i>";
                                                                    }
                                                                    for ($tt = $total_rate; $tt < 5; $tt++) {
                                                                        echo "<i class=\"fa fa-star-o\"></i>";
                                                                    }
                                                                } elseif (gettype($total_rate) == 'double') {
                                                                    $pieces = explode(".", $total_rate);
                                                                    for ($q = 1; $q <= $pieces[0]; $q++) {
                                                                        echo "<i class=\"fa fa-star\"></i>";
                                                                        if ($pieces[0] == $q) {
                                                                            echo "<i class=\"fa fa-star-half-o\"></i>";
                                                                            for ($qq = $pieces[0]; $qq < 4; $qq++) {
                                                                                echo "<i class=\"fa fa-star-o\"></i>";
                                                                            }
                                                                        }
                                                                    }

                                                                } else {
                                                                    for ($w = 0; $w <= 4; $w++) {
                                                                        echo "<i class=\"fa fa-star-o\"></i>";
                                                                    }
                                                                }
                                                            } else {
                                                                for ($o = 0; $o <= 4; $o++) {
                                                                    echo "<i class=\"fa fa-star-o\"></i>";
                                                                }
                                                            }
                                                            ?>
                                                        </div>
                                                        <div class="total-rating">(<?php echo $rater ?>)</div>
                                                    </div><!-- Rating -->
                                                </div><!-- /.price-add-to-cart -->
                                                <div class="box-bottom">
                                                    <?php
                                                    if (!$product->variant_id) {
                                                        ?>
                                                        <div class="btn-add-cart">
                                                            <a href="javascript:void(0)"
                                                               onclick="add_to_cart('<?php echo $product->product_id ?>')"
                                                               title="">
                                                                <img src="<?php echo base_url('assets/website/img/add-cart.png') ?>"
                                                                     alt=""><?php echo display('add_to_cart') ?>
                                                            </a>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                    <div class="compare-wishlist">
                                                        <a href="javascript:void(0)" class="wishlist" title=""
                                                           name="<?php echo $product->product_id ?>">
                                                            <img src="<?php echo base_url('assets/website/img/wishlist.png') ?>"
                                                                 alt=""><?php echo display('wishlist') ?>
                                                        </a>
                                                    </div>
                                                </div><!-- /.box-bottom -->
                                            </div>
                                        </div>
                                        <!-- /.End of product box -->
                                    <?php } ?>
                                </div>
                                <!-- /.End of products slide -->
                            </div>
                        <?php }
                        if ($most_popular_product) { ?>
                            <div class="tab-pane product-tab-pane" id="product-tab-2">
                                <div class="products-slide">
                                    <?php
                                    foreach ($most_popular_product as $product) {
                                        ?>
                                        <div class="product-box">
                                            <div class="imagebox">
                                                <span class="product-cat"><a
                                                            href="<?php echo base_url(remove_space($product->category_name) . '/' . $product->category_id) ?>"><?php echo $product->category_name ?></a></span>
                                                <a href="<?php echo base_url(remove_space($product->category_name) . '/' . remove_space($product->title) . '/' . $product->product_id) ?>">
                                                    <h3 class="product-name"><?php echo $product->title ?></h3>
                                                    <div class="product-thumb">
                                                        <img src="<?php echo base_url() ?>assets/website/img/loader.svg"
                                                             data-src="<?php echo base_url() . $product->thumb_image_url ?>"
                                                             alt="">
                                                    </div>
                                                </a>
                                                <div><b>
                                                        <?php
                                                        if ($product->brand_name) {
                                                            echo $product->brand_name;
                                                        } else {
                                                            echo $product->first_name . ' ' . $product->last_name;
                                                        }
                                                        ?>
                                                    </b></div>
                                                <div class="price-cart">
                                                    <?php
                                                    $currency_new_id = $this->session->userdata('currency_new_id');

                                                    if (empty($currency_new_id)) {
                                                        $result = $cur_info = $this->db->select('*')
                                                            ->from('currency_info')
                                                            ->where('default_status', '1')
                                                            ->get()
                                                            ->row();
                                                        $currency_new_id = $result->currency_id;
                                                    }

                                                    if (!empty($currency_new_id)) {
                                                        $cur_info = $this->db->select('*')
                                                            ->from('currency_info')
                                                            ->where('currency_id', $currency_new_id)
                                                            ->get()
                                                            ->row();

                                                        $target_con_rate = $cur_info->convertion_rate;
                                                        $position1 = $cur_info->currency_position;
                                                        $currency1 = $cur_info->currency_icon;
                                                    }
                                                    ?>

                                                    <?php if ($product->on_sale == 1 && !empty($product->offer_price)) { ?>
                                                        <span class="price">
                                                <span class="price-amount">
                                                    <ins><span class="amount">
                                                    <?php
//                                                    if ($target_con_rate > 1) {
                                                        $price = $product->offer_price * $target_con_rate;
                                                        echo(($position1 == 0) ? $currency1 . " " . number_format
                                                            ($price, 0, '.', ',') : number_format($price, 0, '.', ',
                                                            ') . " " . $currency1);
//                                                    }
//                                                    if ($target_con_rate <= 1) {
//                                                        $price = $product->offer_price * $target_con_rate;
//                                                        echo(($position1 == 0) ? $currency1 . " " . number_format($price, 2, '.', ',') : number_format($price, 2, '.', ',') . " " . $currency1);
//                                                    }
                                                    ?>
                                                    </span></ins>
                                                    <del><span class="amount">
                                                    <?php
//                                                    if ($target_con_rate > 1) {
                                                        $price = $product->price * $target_con_rate;
                                                        echo(($position1 == 0) ? $currency1 . " " . number_format
                                                            ($price, 0, '.', ',') : number_format($price, 0, '.', ',
                                                            ') . " " . $currency1);
//                                                    }
//
//                                                    if ($target_con_rate <= 1) {
//                                                        $price = $product->price * $target_con_rate;
//                                                        echo(($position1 == 0) ? $currency1 . " " . number_format($price, 2, '.', ',') : number_format($price, 2, '.', ',') . " " . $currency1);
//                                                    }
                                                    ?>
                                                    </span></del>
                                                    <span class="amount"> </span>
                                                </span>
                                            </span><!-- /.Price -->
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <span class="price">
                                                <span class="price-amount">
                                                    <ins><span class="amount">
                                                    <?php
//                                                    if ($target_con_rate > 1) {
                                                        $price = $product->price * $target_con_rate;
                                                        echo(($position1 == 0) ? $currency1 . " " . number_format
                                                            ($price, 0, '.', ',') : number_format($price, 0, '.', ',
                                                            ') . " " . $currency1);
//                                                    }
//
//                                                    if ($target_con_rate <= 1) {
//                                                        $price = $product->price * $target_con_rate;
//                                                        echo(($position1 == 0) ? $currency1 . " " . number_format($price, 2, '.', ',') : number_format($price, 2, '.', ',') . " " . $currency1);
//                                                    }
                                                    ?>
                                                    </span></ins>
                                                    <span class="amount"> </span>
                                                </span>
                                            </span><!-- /.Price -->
                                                        <?php
                                                    }
                                                    ?>
                                                    <div class="rating_stars">
                                                        <div class="rating-wrap">
                                                            <?php
                                                            $rater = $this->Categories->get_total_rater_by_product_id($product->product_id);
                                                            $result = $this->Categories->get_total_rate_by_product_id($product->product_id);

                                                            if ($result->rates != null) {
                                                                $total_rate = $result->rates / $rater;
                                                                if (gettype($total_rate) == 'integer') {
                                                                    for ($t = 1; $t <= $total_rate; $t++) {
                                                                        echo "<i class=\"fa fa-star\"></i>";
                                                                    }
                                                                    for ($tt = $total_rate; $tt < 5; $tt++) {
                                                                        echo "<i class=\"fa fa-star-o\"></i>";
                                                                    }
                                                                } elseif (gettype($total_rate) == 'double') {
                                                                    $pieces = explode(".", $total_rate);
                                                                    for ($q = 1; $q <= $pieces[0]; $q++) {
                                                                        echo "<i class=\"fa fa-star\"></i>";
                                                                        if ($pieces[0] == $q) {
                                                                            echo "<i class=\"fa fa-star-half-o\"></i>";
                                                                            for ($qq = $pieces[0]; $qq < 4; $qq++) {
                                                                                echo "<i class=\"fa fa-star-o\"></i>";
                                                                            }
                                                                        }
                                                                    }

                                                                } else {
                                                                    for ($w = 0; $w <= 4; $w++) {
                                                                        echo "<i class=\"fa fa-star-o\"></i>";
                                                                    }
                                                                }
                                                            } else {
                                                                for ($o = 0; $o <= 4; $o++) {
                                                                    echo "<i class=\"fa fa-star-o\"></i>";
                                                                }
                                                            }
                                                            ?>
                                                        </div>
                                                        <div class="total-rating">(<?php echo $rater ?>)</div>
                                                    </div><!-- Rating -->
                                                </div><!-- /.price-add-to-cart -->
                                                <div class="box-bottom">
                                                    <?php
                                                    if (!$product->variant_id) {
                                                        ?>
                                                        <div class="btn-add-cart">
                                                            <a href="javascript:void(0)"
                                                               onclick="add_to_cart('<?php echo $product->product_id ?>')"
                                                               title="">
                                                                <img src="<?php echo base_url('assets/website/img/add-cart.png') ?>"
                                                                     alt=""><?php echo display('add_to_cart') ?>
                                                            </a>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                    <div class="compare-wishlist">
                                                        <a href="javascript:void(0)" class="wishlist" title=""
                                                           name="<?php echo $product->product_id ?>">
                                                            <img src="<?php echo base_url('assets/website/img/wishlist.png') ?>"
                                                                 alt=""><?php echo display('wishlist') ?>
                                                        </a>
                                                    </div>
                                                </div><!-- /.box-bottom -->
                                            </div>
                                        </div>
                                        <!-- /.End of product box -->
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        <?php }
                        if ($best_merchant) { ?>
                            <div class="tab-pane product-tab-pane" id="product-tab-3">
                                <div class="products-slide">
                                    <?php
                                    foreach ($best_merchant as $merchant) {
                                        $product_list = $this->Homes->get_seller_product($merchant->seller_id);
                                        foreach ($product_list as $product) {
                                            ?>
                                            <div class="product-box">
                                                <div class="imagebox">
                                                    <span class="product-cat"><a
                                                                href="<?php echo base_url(remove_space($product->category_name) . '/' . $product->category_id) ?>"><?php echo $product->category_name ?></a></span>
                                                    <a href="<?php echo base_url(remove_space($product->category_name) . '/' . remove_space($product->title) . '/' . $product->product_id) ?>">
                                                        <h3 class="product-name"><?php echo $product->title ?></h3>
                                                        <div class="product-thumb">
                                                            <img src="<?php echo base_url() ?>assets/website/img/loader.svg"
                                                                 data-src="<?php echo base_url() . $product->thumb_image_url ?>"
                                                                 alt="">
                                                        </div>
                                                    </a>
                                                    <div><b>
                                                            <?php
                                                            if ($product->brand_name) {
                                                                echo $product->brand_name;
                                                            } else {
                                                                echo $product->first_name . ' ' . $product->last_name;
                                                            }
                                                            ?>
                                                        </b></div>
                                                    <div class="price-cart">
                                                        <?php
                                                        $currency_new_id = $this->session->userdata('currency_new_id');

                                                        if (empty($currency_new_id)) {
                                                            $result = $cur_info = $this->db->select('*')
                                                                ->from('currency_info')
                                                                ->where('default_status', '1')
                                                                ->get()
                                                                ->row();
                                                            $currency_new_id = $result->currency_id;
                                                        }

                                                        if (!empty($currency_new_id)) {
                                                            $cur_info = $this->db->select('*')
                                                                ->from('currency_info')
                                                                ->where('currency_id', $currency_new_id)
                                                                ->get()
                                                                ->row();

                                                            $target_con_rate = $cur_info->convertion_rate;
                                                            $position1 = $cur_info->currency_position;
                                                            $currency1 = $cur_info->currency_icon;
                                                        }
                                                        ?>

                                                        <?php if ($product->on_sale == 1 && !empty($product->offer_price)) { ?>
                                                            <span class="price">
                                                    <span class="price-amount">
                                                        <ins><span class="amount">
                                                        <?php
//                                                        if ($target_con_rate > 1) {
                                                            $price = $product->offer_price * $target_con_rate;
                                                            echo(($position1 == 0) ? $currency1 . " " . number_format
                                                                ($price, 0, '.', ',') : number_format($price, 0, '.',
                                                                    ',') . " " . $currency1);
//                                                        }
//                                                        if ($target_con_rate <= 1) {
//                                                            $price = $product->offer_price * $target_con_rate;
//                                                            echo(($position1 == 0) ? $currency1 . " " . number_format($price, 2, '.', ',') : number_format($price, 2, '.', ',') . " " . $currency1);
//                                                        }
                                                        ?>
                                                        </span></ins>
                                                        <del><span class="amount">
                                                        <?php
//                                                        if ($target_con_rate > 1) {
                                                            $price = $product->price * $target_con_rate;
                                                            echo(($position1 == 0) ? $currency1 . " " . number_format
                                                                ($price, 0, '.', ',') : number_format($price, 0, '.',
                                                                    ',') . " " . $currency1);
//                                                        }
//
//                                                        if ($target_con_rate <= 1) {
//                                                            $price = $product->price * $target_con_rate;
//                                                            echo(($position1 == 0) ? $currency1 . " " . number_format($price, 2, '.', ',') : number_format($price, 2, '.', ',') . " " . $currency1);
//                                                        }
                                                        ?>
                                                        </span></del>
                                                        <span class="amount"> </span>
                                                    </span>
                                                </span><!-- /.Price -->
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <span class="price">
                                                    <span class="price-amount">
                                                        <ins><span class="amount">
                                                        <?php
//                                                        if ($target_con_rate > 1) {
                                                            $price = $product->price * $target_con_rate;
                                                            echo(($position1 == 0) ? $currency1 . " " . number_format
                                                                ($price, 0, '.', ',') : number_format($price, 0, '.',
                                                                    ',') . " " . $currency1);
//                                                        }
//
//                                                        if ($target_con_rate <= 1) {
//                                                            $price = $product->price * $target_con_rate;
//                                                            echo(($position1 == 0) ? $currency1 . " " . number_format($price, 2, '.', ',') : number_format($price, 2, '.', ',') . " " . $currency1);
//                                                        }
                                                        ?>
                                                        </span></ins>
                                                        <span class="amount"> </span>
                                                    </span>
                                                </span><!-- /.Price -->
                                                            <?php
                                                        }
                                                        ?>
                                                        <div class="rating_stars">
                                                            <div class="rating-wrap">
                                                                <?php
                                                                $rater = $this->Categories->get_total_rater_by_product_id($product->product_id);
                                                                $result = $this->Categories->get_total_rate_by_product_id($product->product_id);

                                                                if ($result->rates != null) {
                                                                    $total_rate = $result->rates / $rater;
                                                                    if (gettype($total_rate) == 'integer') {
                                                                        for ($t = 1; $t <= $total_rate; $t++) {
                                                                            echo "<i class=\"fa fa-star\"></i>";
                                                                        }
                                                                        for ($tt = $total_rate; $tt < 5; $tt++) {
                                                                            echo "<i class=\"fa fa-star-o\"></i>";
                                                                        }
                                                                    } elseif (gettype($total_rate) == 'double') {
                                                                        $pieces = explode(".", $total_rate);
                                                                        for ($q = 1; $q <= $pieces[0]; $q++) {
                                                                            echo "<i class=\"fa fa-star\"></i>";
                                                                            if ($pieces[0] == $q) {
                                                                                echo "<i class=\"fa fa-star-half-o\"></i>";
                                                                                for ($qq = $pieces[0]; $qq < 4; $qq++) {
                                                                                    echo "<i class=\"fa fa-star-o\"></i>";
                                                                                }
                                                                            }
                                                                        }

                                                                    } else {
                                                                        for ($w = 0; $w <= 4; $w++) {
                                                                            echo "<i class=\"fa fa-star-o\"></i>";
                                                                        }
                                                                    }
                                                                } else {
                                                                    for ($o = 0; $o <= 4; $o++) {
                                                                        echo "<i class=\"fa fa-star-o\"></i>";
                                                                    }
                                                                }
                                                                ?>
                                                            </div>
                                                            <div class="total-rating">(<?php echo $rater ?>)</div>
                                                        </div><!-- Rating -->
                                                    </div><!-- /.price-add-to-cart -->
                                                    <div class="box-bottom">
                                                        <?php
                                                        if (!$product->variant_id) {
                                                            ?>
                                                            <div class="btn-add-cart">
                                                                <a href="javascript:void(0)"
                                                                   onclick="add_to_cart('<?php echo $product->product_id ?>')"
                                                                   title="">
                                                                    <img src="<?php echo base_url('assets/website/img/add-cart.png') ?>"
                                                                         alt=""><?php echo display('add_to_cart') ?>
                                                                </a>
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>
                                                        <div class="compare-wishlist">
                                                            <a href="javascript:void(0)" class="wishlist" title=""
                                                               name="<?php echo $product->product_id ?>">
                                                                <img src="<?php echo base_url('assets/website/img/wishlist.png') ?>"
                                                                     alt=""><?php echo display('wishlist') ?>
                                                            </a>
                                                        </div>
                                                    </div><!-- /.box-bottom -->
                                                </div>
                                            </div>
                                            <!-- /.End of product box -->
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
        if ($block_list) {
            foreach ($block_list as $block) {
                if ($block['block_style'] == 1) {
                    $get_sub_category_list = $this->Homes->get_sub_category_list($block['block_cat_id']);
                    $category_list_by_id = $this->Homes->category_list_by_id($block['block_cat_id']);
                    if ($get_sub_category_list) { ?>
                        <div class="products-content">
                            <div class="header-title">
                                <ul>
                                    <li class="title-name"><img class="cat-icon lnr"
                                                                src="<?php echo $category_list_by_id->cat_favicon; ?>"
                                                                height="15"
                                                                width="15"><span><?php echo $category_list_by_id->category_name; ?>
                                            <i class="fa fa-angle-right" aria-hidden="true"></i></span></li>
                                </ul>
                            </div>
                            <!-- /.End of header title -->
                            <div class="products-row">
                                <div class="category-col">
                                    <div class="brand-slider slider-ht">
                                        <?php
                                        $c_end = 4;
                                        $count = 0;
                                        $category_info = $this->db->select('brand_id')
                                            ->from('product_information')
                                            ->where('category_id', $block['block_cat_id'])
                                            ->distinct('brand_id')
                                            ->get()
                                            ->result();

                                        $num = count($category_info) / 4;

                                        for ($i = 0; $i < ceil($num); $i++) {
                                            ?>
                                            <div class="brand-cat">
                                                <?php
                                                $category_info = $this->db->select('brand_id')
                                                    ->from('product_information')
                                                    ->where('category_id', $block['block_cat_id'])
                                                    ->limit($c_end, $count)
                                                    ->distinct('brand_id')
                                                    ->get()
                                                    ->result();
                                                $of = 1;
                                                foreach ($category_info as $category) {
                                                    $brand = $this->db->select('*')
                                                        ->from('brand')
                                                        ->where('brand_id', $category->brand_id)
                                                        ->get()
                                                        ->row();
                                                    if ($brand) {
                                                        ?>
                                                        <a href="<?php echo base_url('brand_product/list/' . $brand->brand_id) ?>"
                                                           title="<?php echo $brand->brand_name ?>"><img
                                                                    src="<?php echo $brand->brand_image ?>"
                                                                    class="img-responsive center-block"
                                                                    alt="<?php echo $brand->brand_name ?>"></a>
                                                        <?php
                                                    }
                                                    $of++;
                                                }
                                                ?>
                                            </div>
                                            <?php
                                            $count += 4;
                                        }
                                        ?>
                                    </div>
                                    <!-- /.End of brand slider -->
                                </div>
                                <div class="category-col cat-content">
                                    <?php
                                    $category = $this->Homes->get_one_level_sub_category($block['block_cat_id']);
                                    $featured_cat = $this->Homes->get_one_level_featured_category($block['block_cat_id']);

                                    if ($category) {
                                        $i = 0;
                                        foreach ($category as $cat) {
                                            if ($i == 13) {
                                                break;
                                            }
                                            ?>
                                            <div class="product-cat2">
                                                <a href="<?php echo base_url(remove_space($cat['category_name']) . '/' . $cat['category_id']) ?>"
                                                   class="cat"><?php echo $cat['category_name'] ?></a>
                                            </div>
                                            <?php
                                            $i++;
                                        }
                                    }
                                    ?>
                                </div>
                                <!-- /.End of category content -->
                                <div class="product-col-2" style="padding: 0 10px;">
                                    <div class="product-cat-slide">
                                        <?php
                                        if ($featured_cat) {
                                            foreach ($featured_cat as $f_cat) {
                                                ?>
                                                <div class="product-cat-item" style="height: 284px">
                                                    <a href="<?php echo base_url(remove_space($f_cat['category_name']) . '/' . $f_cat['category_id']) ?>">
                                                        <h5 class="product-cat-title"
                                                            style="height: 25px"><?php echo character_limiter($f_cat['category_name'], 30); ?></h5>
                                                        <img src="<?php echo $f_cat['cat_image'] ?>"
                                                             class="img-responsive" alt="">
                                                        <div class="product-cat-offer"><?php echo $f_cat['details'] ?></div>
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.End of Products Content -->
                        <?php
                    }
                } elseif ($block['block_style'] == 2) {
                    $get_sub_category_list = $this->Homes->get_sub_category_list($block['block_cat_id']);
                    $category_list_by_id = $this->Homes->category_list_by_id($block['block_cat_id']);
                    if ($get_sub_category_list) { ?>
                        <div class="products-content">
                            <div class="header-title">
                                <ul>
                                    <li class="title-name"><img class="cat-icon lnr"
                                                                src="<?php echo $category_list_by_id->cat_favicon; ?>"
                                                                height="15"
                                                                width="15"><span><?php echo $category_list_by_id->category_name; ?>
                                            <i class="fa fa-angle-right" aria-hidden="true"></i></span></li>
                                    <li class="view-all"><a href="#">><?php echo display('view_all') ?></a></li>
                                    <?php
                                    $i = 0;
                                    foreach ($get_sub_category_list as $all_sub) {
                                        if ($i == 10) {
                                            break;
                                        }
                                        ?>
                                        <li class="cat-link"><a
                                                    href="<?php echo base_url(remove_space($all_sub['category_name']) . '/' . $all_sub['category_id']) ?>"><?php echo $all_sub['category_name'] ?></a>
                                        </li>
                                        <?php
                                        $i++;
                                    }
                                    ?>
                                </ul>
                            </div>
                            <!-- /.End of header title -->
                            <div class="products-row">
                                <div class="category-col">
                                    <div class="brand-slider">
                                        <?php
                                        $c_end = 6;
                                        $count = 0;
                                        $category_info = $this->db->select('brand_id')
                                            ->from('product_information')
                                            ->where('category_id', $block['block_cat_id'])
                                            ->distinct('brand_id')
                                            ->get()
                                            ->result();

                                        $num = count($category_info) / 6;

                                        for ($i = 0; $i < ceil($num); $i++) {
                                            ?>
                                            <div class="brand-cat">
                                                <?php
                                                $category_info = $this->db->select('brand_id')
                                                    ->from('product_information')
                                                    ->where('category_id', $block['block_cat_id'])
                                                    ->limit($c_end, $count)
                                                    ->distinct('brand_id')
                                                    ->get()
                                                    ->result();
                                                $of = 1;
                                                foreach ($category_info as $category) {
                                                    $brand = $this->db->select('*')
                                                        ->from('brand')
                                                        ->where('brand_id', $category->brand_id)
                                                        ->get()
                                                        ->row();
                                                    ?>
                                                    <a href="<?php echo base_url(remove_space($brand->category_name) . '/' . $block['block_cat_id'] . '?brand=' . $brand->brand_id) ?>"
                                                       title="<?php echo $brand->brand_name ?>"><img
                                                                src="<?php echo $brand->brand_image ?>"
                                                                class="img-responsive center-block"
                                                                alt="<?php echo $brand->brand_name ?>"></a>
                                                    <?php
                                                    $of++;
                                                }
                                                ?>
                                            </div>
                                            <?php
                                            $count += 6;
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="category-col cat-content2">
                                    <?php
                                    $category = $this->Homes->get_sub_category($block['block_cat_id']);
                                    $featured_cat = $this->Homes->get_featured_category2($block['block_cat_id']);
                                    $featured_cat_single = $this->Homes->get_featured_category2_single($block['block_cat_id']);
                                    if ($category) {
                                        foreach ($category as $cat) {
                                            ?>
                                            <div class="product-cat2">
                                                <a href="<?php echo base_url(remove_space($cat['category_name']) . '/' . $cat['category_id']) ?>"
                                                   class="cat"><?php echo $cat['category_name'] ?></a>
                                                <?php
                                                if (!empty($cat['categorieslevelone'])) {
                                                    foreach ($cat['categorieslevelone'] as $sub_cat) {
                                                        ?>
                                                        <a href="<?php echo base_url(remove_space($cat['category_name']) . '/' . $sub_cat['category_id']) ?>"
                                                           class="sub-cat"><?php echo $sub_cat['category_name'] ?></a>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                                <div class="banner-col">
                                    <a href="<?php echo base_url(remove_space($featured_cat_single->category_name) . '/' . $featured_cat_single->category_id) ?>"
                                       title="<?php echo $featured_cat_single->category_name ?>">
                                        <img src="<?php echo $featured_cat_single->cat_image ?>" class="img-responsive"
                                             alt="<?php echo $featured_cat_single->category_name ?>">
                                    </a>
                                </div>
                                <div class="home-category-col">
                                    <ul class="products">
                                        <?php
                                        if ($featured_cat) {
                                            $i = 0;
                                            foreach ($featured_cat as $f_cat) {
                                                if ($i == 4) {
                                                    break;
                                                }
                                                ?>
                                                <li class="col-xs-6 col-sm-6">
                                                    <a href="<?php echo base_url(remove_space($f_cat['category_name']) . '/' . $f_cat['category_id']) ?>"
                                                       class="product-cat-box">
                                                        <div class="product-cat-content">
                                                            <img src="<?php echo base_url() ?>assets/website/img/cat-bg.png"
                                                                 class="img-responsive center-block cat-bg-img"
                                                                 alt="<?php echo $f_cat['category_name'] ?>">
                                                            <img src="<?php echo $f_cat['cat_image'] ?>"
                                                                 class="img-responsive center-block cat-main-img"
                                                                 alt="<?php echo $f_cat['category_name'] ?>">
                                                            <h5 class="cat-title"><?php echo $f_cat['category_name'] ?></h5>
                                                            <h6 class="cat-title"><?php echo $f_cat['details'] ?></h6>
                                                        </div>
                                                    </a>
                                                </li>
                                                <?php
                                                $i++;
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- /.End of Products Content -->
                        <?php
                    }
                } elseif ($block['block_style'] == 3) {
                    $get_sub_category_list = $this->Homes->get_sub_category_list($block['block_cat_id']);
                    $category_list_by_id = $this->Homes->category_list_by_id($block['block_cat_id']);
                    if ($get_sub_category_list) { ?>
                        <div class="products-content">
                            <div class="header-title">
                                <ul>
                                    <li class="title-name"><img class=""
                                                                src="<?php echo $category_list_by_id->cat_favicon ?>"
                                                                height="15"
                                                                width="15"><span> <?php echo $category_list_by_id->category_name; ?>
                                            <i class="fa fa-angle-right" aria-hidden="true"></i></span></li>
                                    <li class="view-all"><a href="#"><?php echo display('view_all') ?></a></li>
                                    <?php
                                    $i = 0;
                                    foreach ($get_sub_category_list as $all_sub) {
                                        if ($i == 10) {
                                            break;
                                        }
                                        ?>
                                        <li class="cat-link"><a
                                                    href="<?php echo base_url(remove_space($all_sub['category_name']) . '/' . $all_sub['category_id']) ?>"><?php echo $all_sub['category_name'] ?></a>
                                        </li>
                                        <?php
                                        $i++;
                                    }
                                    ?>
                                </ul>
                            </div>
                            <!-- /.End of header title -->
                            <div class="products-row">
                                <div class="category-col">
                                    <div class="brand-slider">
                                        <?php
                                        $c_end = 6;
                                        $count = 0;
                                        $category_info = $this->db->select('brand_id')
                                            ->from('product_information')
                                            ->where('category_id', $block['block_cat_id'])
                                            ->distinct('brand_id')
                                            ->get()
                                            ->result();

                                        $num = count($category_info) / 6;

                                        for ($i = 0; $i < ceil($num); $i++) {
                                            ?>
                                            <div class="brand-cat">
                                                <?php
                                                $category_info = $this->db->select('brand_id')
                                                    ->from('product_information')
                                                    ->where('category_id', $block['block_cat_id'])
                                                    ->limit($c_end, $count)
                                                    ->distinct('brand_id')
                                                    ->get()
                                                    ->result();
                                                $of = 1;
                                                foreach ($category_info as $category) {
                                                    $brand = $this->db->select('*')
                                                        ->from('brand')
                                                        ->where('brand_id', $category->brand_id)
                                                        ->get()
                                                        ->row();
                                                    if ($brand) {
                                                        ?>
                                                        <a href="<?php echo base_url(remove_space($brand->category_name) . '/' . $block['block_cat_id'] . '?brand=' . $brand->brand_id) ?>"
                                                           title="<?php echo $brand->brand_name ?>"><img
                                                                    src="<?php echo $brand->brand_image ?>"
                                                                    class="img-responsive center-block"
                                                                    alt="<?php echo $brand->brand_name ?>"></a>
                                                        <?php
                                                    }
                                                    $of++;
                                                }
                                                ?>
                                            </div>
                                            <?php
                                            $count += 6;
                                        }
                                        ?>
                                    </div>
                                    <!-- /.End of brand slider -->
                                </div>
                                <div class="category-col cat-content">
                                    <?php
                                    $category = $this->Homes->get_sub_category($block['block_cat_id']);
                                    $featured_cat = $this->Homes->get_featured_category($block['block_cat_id']);
                                    if ($category) {
                                        foreach ($category as $cat) { ?>
                                            <div class="product-cat2">
                                                <a href="<?php echo base_url(remove_space($cat['category_name']) . '/' . $cat['category_id']) ?>"
                                                   class="cat"><?php echo $cat['category_name'] ?></a>
                                                <?php
                                                if ($cat) {
                                                    foreach ($cat['categorieslevelone'] as $sub_cat) {
                                                        ?>
                                                        <a href="<?php echo base_url(remove_space($sub_cat['category_name']) . '/' . $sub_cat['category_id']) ?>"
                                                           class="sub-cat"><?php echo $sub_cat['category_name'] ?></a>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                                <!-- /.End of category content -->
                                <div class="product-col-2">
                                    <ul class="products">
                                        <?php
                                        if ($featured_cat) {
                                            $i = 0;
                                            foreach ($featured_cat as $f_cat) {
                                                if ($i == 6) {
                                                    break;
                                                }
                                                ?>
                                                <li class="col-xs-6 col-sm-6 col-md-4">
                                                    <a href="<?php echo base_url(remove_space($f_cat['category_name']) . '/' . $f_cat['category_id']) ?>"
                                                       target="_blank" class="product-cat-box">
                                                        <div class="product-cat-content">
                                                            <img src="<?php echo base_url() ?>assets/website/img/cat-bg.png"
                                                                 class="img-responsive center-block cat-bg-img"
                                                                 alt="<?php echo $f_cat['category_name'] ?>">
                                                            <img src="<?php echo $f_cat['cat_image'] ?>"
                                                                 class="img-responsive center-block cat-main-img"
                                                                 alt="<?php echo $f_cat['category_name'] ?>">
                                                            <h5 class="cat-title"><?php echo $f_cat['category_name'] ?></h5>
                                                            <h6 class="cat-title"><?php echo $f_cat['details'] ?></h6>
                                                        </div>
                                                    </a>
                                                    <!-- /.End of product category -->
                                                </li>
                                                <?php
                                                $i++;
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- /.End of Products Content -->
                        <?php
                    }
                }
            }
        }
        ?>
    </div>
    <?php
    $category_info = $this->db->select('b.category_name,a.category_id')
        ->from('product_information a')
        ->join('product_category b', 'a.category_id = b.category_id')
        ->limit(8)
        ->group_by('a.category_id')
        ->get()
        ->result();
    if ($category_info) {
        ?>
        <div class="container hidden-xs">
            <div class="brand-cat-content">
                <div class="tabs-menu">
                    <ul class="nav nav-tabs">
                        <?php
                        $i = 0;
                        if ($category_info) {
                            foreach ($category_info as $cat) {
                                $brand_info = $this->db->select('b.*')
                                    ->from('product_information a')
                                    ->join('brand b', 'a.brand_id = b.brand_id')
                                    ->where('a.category_id', $cat->category_id)
                                    ->group_by('a.brand_id')
                                    ->get()
                                    ->result();
                                if ($brand_info) {
                                    ?>
                                    <li class="<?php if ($i == 0):echo "active";endif; ?>"><a
                                                href="#brand-cat<?php echo $i; ?>"
                                                data-toggle="tab"><?php echo $cat->category_name ?></a></li>
                                    <?php
                                    $i++;
                                }
                            }
                        }
                        ?>
                    </ul>
                </div>
                <div class="tab-content">
                    <?php
                    $i = 0;
                    foreach ($category_info as $cat) {
                        $brand_info = $this->db->select('b.*')
                            ->from('product_information a')
                            ->join('brand b', 'a.brand_id = b.brand_id')
                            ->where('a.category_id', $cat->category_id)
                            ->group_by('a.brand_id')
                            ->get()
                            ->result();
                        if ($brand_info) {
                            ?>
                            <div class="tab-pane <?php if ($i == 0):echo "in active";endif; ?>"
                                 id="brand-cat<?php echo $i; ?>">
                                <div class="brand-logo">
                                    <div class="row">
                                        <?php
                                        if ($brand_info) {
                                            foreach ($brand_info as $brand) {
                                                ?>
                                                <div class="col-xs-4 col-sm-2 logo-item">
                                                    <a href="<?php echo base_url('brand_product/list/' . $brand->brand_id) ?>"
                                                       target="_blank"><img class="img-responsive center-block"
                                                                            src="<?php echo $brand->brand_image ?>"
                                                                            alt=""></a>
                                                </div>
                                                <?php
                                            }
                                            $i++;
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
        <!--  /.End of Products Brands -->
        <?php
    }
    if ($best_sales) {
        ?>
        <div class="variable-slider">
            <div class="container">
                <div class="variable-width">
                    <?php
                    foreach ($best_sales as $product) {
                        $select_single_category = $this->Categories->select_single_category_by_id($product->category_id);

                        ?>
                        <div class="variable-item">
                            <a href="<?php echo base_url(remove_space($select_single_category->category_name) . '/' . remove_space($product->title) . '/' . $product->product_id) ?>"
                               class="cat-box">
                                <h5 class="cat-title"><?php echo $product->title ?></h5>
                                <img src="<?php echo base_url() . $product->thumb_image_url ?>" class="img-responsive"
                                     alt="">
                                <div class="price-cart">
                                    <?php
                                    $currency_new_id = $this->session->userdata('currency_new_id');

                                    if (empty($currency_new_id)) {
                                        $result = $cur_info = $this->db->select('*')
                                            ->from('currency_info')
                                            ->where('default_status', '1')
                                            ->get()
                                            ->row();
                                        $currency_new_id = $result->currency_id;
                                    }

                                    if (!empty($currency_new_id)) {
                                        $cur_info = $this->db->select('*')
                                            ->from('currency_info')
                                            ->where('currency_id', $currency_new_id)
                                            ->get()
                                            ->row();

                                        $target_con_rate = $cur_info->convertion_rate;
                                        $position1 = $cur_info->currency_position;
                                        $currency1 = $cur_info->currency_icon;
                                    }
                                    ?>
                                    <?php if ($product->on_sale == 1 && !empty($product->offer_price)) { ?>
                                        <span class="price">
                                <span class="price-amount">
                                    <ins><span class="amount">
                                    <?php
                                    if ($target_con_rate > 1) {
                                        $price = $product->offer_price * $target_con_rate;
                                        echo(($position1 == 0) ? $currency1 . " " . number_format($price, 2, '.', ',') : number_format($price, 2, '.', ',') . " " . $currency1);
                                    }

                                    if ($target_con_rate <= 1) {
                                        $price = $product->offer_price * $target_con_rate;
                                        echo(($position1 == 0) ? $currency1 . " " . number_format($price, 2, '.', ',') : number_format($price, 2, '.', ',') . " " . $currency1);
                                    }
                                    ?>
                                    </span></ins>
                                    <del><span class="amount">
                                    <?php
                                    if ($target_con_rate > 1) {
                                        $price = $product->price * $target_con_rate;
                                        echo(($position1 == 0) ? $currency1 . " " . number_format($price, 2, '.', ',') : number_format($price, 2, '.', ',') . " " . $currency1);
                                    }

                                    if ($target_con_rate <= 1) {
                                        $price = $product->price * $target_con_rate;
                                        echo(($position1 == 0) ? $currency1 . " " . number_format($price, 2, '.', ',') : number_format($price, 2, '.', ',') . " " . $currency1);
                                    }
                                    ?>
                                    </span></del>
                                    <span class="amount"> </span>
                                </span>
                            </span><!-- /.Price -->
                                        <?php
                                    } else {
                                        ?>
                                        <span class="price">
                                <span class="price-amount">
                                    <ins><span class="amount">
                                    <?php
                                    if ($target_con_rate > 1) {
                                        $price = $product->price * $target_con_rate;
                                        echo(($position1 == 0) ? $currency1 . " " . number_format($price, 2, '.', ',') : number_format($price, 2, '.', ',') . " " . $currency1);
                                    }

                                    if ($target_con_rate <= 1) {
                                        $price = $product->price * $target_con_rate;
                                        echo(($position1 == 0) ? $currency1 . " " . number_format($price, 2, '.', ',') : number_format($price, 2, '.', ',') . " " . $currency1);
                                    }
                                    ?>
                                    </span></ins>
                                    <span class="amount"> </span>
                                </span>
                            </span><!-- /.Price -->
                                        <?php
                                    }
                                    ?>
                                    <div class="rating_stars">
                                        <div class="rating-wrap">
                                            <?php
                                            $rater = $this->Categories->get_total_rater_by_product_id($product->product_id);
                                            $result = $this->Categories->get_total_rate_by_product_id($product->product_id);

                                            if ($result->rates != null) {
                                                $total_rate = $result->rates / $rater;
                                                if (gettype($total_rate) == 'integer') {
                                                    for ($t = 1; $t <= $total_rate; $t++) {
                                                        echo "<i class=\"fa fa-star\"></i>";
                                                    }
                                                    for ($tt = $total_rate; $tt < 5; $tt++) {
                                                        echo "<i class=\"fa fa-star-o\"></i>";
                                                    }
                                                } elseif (gettype($total_rate) == 'double') {
                                                    $pieces = explode(".", $total_rate);
                                                    for ($q = 1; $q <= $pieces[0]; $q++) {
                                                        echo "<i class=\"fa fa-star\"></i>";
                                                        if ($pieces[0] == $q) {
                                                            echo "<i class=\"fa fa-star-half-o\"></i>";
                                                            for ($qq = $pieces[0]; $qq < 4; $qq++) {
                                                                echo "<i class=\"fa fa-star-o\"></i>";
                                                            }
                                                        }
                                                    }

                                                } else {
                                                    for ($w = 0; $w <= 4; $w++) {
                                                        echo "<i class=\"fa fa-star-o\"></i>";
                                                    }
                                                }
                                            } else {
                                                for ($o = 0; $o <= 4; $o++) {
                                                    echo "<i class=\"fa fa-star-o\"></i>";
                                                }
                                            }
                                            ?>
                                        </div>
                                        <div class="total-rating">(<?php echo $rater ?>)</div>
                                    </div><!-- Rating -->
                                </div><!-- /.price-add-to-cart -->
                            </a>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php } ?>
    <!-- /.Variable Width Slider -->
    <?php if ($home_cat_list) { ?>
        <div class="mobile-cat">
            <div class="container">
                <div class="row">
                    <?php
                    foreach ($home_cat_list as $category) {
                        ?>
                        <div class="col-xs-4 pr">
                            <a href="<?php echo base_url(remove_space($category->category_name) . '/' . $category->category_id) ?>"
                               class="cat-box feature-cat">
                                <h5 class="cat-title"><?php echo $category->category_name ?></h5>
                                <img src="<?php echo $category->cat_image ?>" class="img-responsive"
                                     alt="<?php echo $category->category_name ?>">
                                <div class="discount"><?php echo $category->details ?></div>
                            </a>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
<div class="text-center" id="sub_msg"></div>
<div class="newsletter">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-7">
                <h5 class="newsletter-title"><?php echo display('sign_up_to_newsletter') ?></h5>
                <span class="newsletter-marketing-text"><?php echo display('sign_up_for_news_and') ?>
                    <strong><?php echo display('offers') ?></strong></span>
            </div>
            <div class="col-xs-12 col-sm-5">
                <form>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="<?php echo display('enter_your_email') ?>"
                               id="sub_email">
                        <span class="input-group-btn">
                            <button class="btn btn-secondary" type="button"
                                    id="smt_btn"><?php echo display('sign_up') ?></button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /.End of newsletter -->
<script type="text/javascript">
    //Add wishlist
    $('body').on('click', '.wishlist', function () {
        var product_id = $(this).attr('name');
        var customer_id = '<?php echo $this->session->userdata('customer_id')?>';
        if (customer_id == 0) {
            alert('<?php echo display('please_login_first')?>');
            return false;
        }
        $.ajax({
            type: "post",
            async: true,
            url: '<?php echo base_url('add_wishlist')?>',
            data: {product_id: product_id, customer_id: customer_id},
            success: function (data) {
                if (data == '1') {
                    alert('<?php echo display('product_added_to_wishlist')?>');
                } else if (data == '2') {
                    alert('<?php echo display('product_already_exists_in_wishlist')?>')
                } else if (data == '3') {
                    alert('<?php echo display('please_login_first')?>')
                }
            },
            error: function () {
                alert('Request Failed, Please check your code and try again!');
            }
        });
    });
</script>
<script type="text/javascript">
    //Subscribe entry
    $('body').on('click', '#smt_btn', function () {
        var sub_email = $('#sub_email').val();
        if (sub_email == 0) {
            alert('<?php echo display('please_enter_your_email')?>');
            return false;
        }
        var check_email = validateEmail(sub_email);

        if (check_email == true) {

            $.ajax({
                type: "post",
                async: true,
                url: '<?php echo base_url('add_subscribe')?>',
                data: {sub_email: sub_email},
                success: function (data) {
                    if (data == '2') {
                        $("#sub_msg").html('<p style="color:green"><?php echo display('subscribe_successfully')?></p>');
                        $('#sub_msg').hide().fadeIn('slow');
                        $('#sub_msg').fadeIn(700);
                        $('#sub_msg').hide().fadeOut(2000);
                        $("#sub_email").val(" ");
                    } else {
                        $("#sub_msg").html('<p style="color:red"><?php echo display('failed')?></p>');
                        $('#sub_msg').hide().fadeIn('slow');
                        $('#sub_msg').fadeIn(700);
                        $('#sub_msg').hide().fadeOut(2000);
                        $("#sub_email").val(" ");
                    }
                },
                error: function () {
                    alert('Request Failed, Please check your code and try again!');
                }
            });
        } else {
            alert('<?php echo display('please_enter_valid_email')?>');
            return false;
        }

        //Valid email cheker
        function validateEmail(email) {
            var re = /\S+@\S+\.\S+/;
            return re.test(email);
        }
    });
</script>