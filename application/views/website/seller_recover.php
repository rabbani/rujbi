<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<div class="page-breadcrumbs">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('home')?>"><?php echo display('home')?></a></li>
            <li class="active"><?php echo display('reset_password')?></li>
        </ol>
    </div>
</div>
<!-- /.End of page breadcrumbs -->
<div class="lost-password">

    <?php
        $message = $this->session->userdata('message');
        if (isset($message)) {
    ?>
       <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
          <strong><?php echo $message ?></strong>
        </div>
    <?php 
        $this->session->unset_userdata('message');
        }
        $error_message = $this->session->userdata('error_message');
        if (isset($error_message)) {
    ?>
      <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
        <strong><?php echo $error_message ?></strong>
      </div>
    <?php 
        $this->session->unset_userdata('error_message');
        }
    ?>

    <form action="<?php echo base_url('Recover_password_password_reset')?>" method="post">
        <p><?php echo display('please_enter_your_email_adderss')?></p>
        <div class="form-group">
            <label class="control-label" for="email1"><?php echo display('email')?> <abbr class="required" title="required">*</abbr></label>
            <input type="email" id="email1" class="form-control" name="seller_email" required="" placeholder="<?php echo display('email')?>">
        </div>
        <button type="submit" class="btn btn-warning"><?php echo display('submit')?></button>
    </form>
</div>
<!-- /.End of lost password -->