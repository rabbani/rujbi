<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<div class="page-breadcrumbs">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('home')?>"><?php echo display('home')?></a></li>
            <li class="active"><?php echo display('reset_password')?></li>
        </ol>
    </div>
</div>
<!-- /.End of page breadcrumbs -->
<div class="lost-password">

    <?php
        $message = $this->session->userdata('message');
        if (isset($message)) {
    ?>
       <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
          <strong><?php echo $message ?></strong>
        </div>
    <?php 
        $this->session->unset_userdata('message');
        }
        $error_message = $this->session->userdata('error_message');
        if (isset($error_message)) {
    ?>
      <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
        <strong><?php echo $error_message ?></strong>
      </div>
    <?php 
        $this->session->unset_userdata('error_message');
        }
    ?>

    <form action="<?php echo base_url('recover_password_update_password')?>" method="post">
        <p><?php echo display('please_enter_new_password')?></p>
        <div class="form-group">
            <label class="control-label" for="password1"><?php echo display('password')?> <abbr class="required" title="required">*</abbr></label>
            <input type="password" id="password1" class="form-control" name="password" required="" placeholder="<?php echo display('password')?>">
        </div>  
        <div class="form-group">
            <label class="control-label" for="cpassword"><?php echo display('confirm_password')?> <abbr class="required" title="required">*</abbr></label>
            <input type="password" id="cpassword" class="form-control" name="cpassword" required="" placeholder="<?php echo display('confirm_password')?>">

            <input type="hidden" name="_token" value="<?php echo $this->uri->segment(3)?>" required>
        </div> 
        <button type="submit" class="btn btn-warning">Reset password</button>
    </form>
</div>
<!-- /.End of lost password -->