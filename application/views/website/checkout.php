<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<div class="page-breadcrumbs">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('home')?>"><?php echo display('home')?></a></li>
            <li class="active"><?php echo display('checkout')?></li>
        </ol>
    </div>
</div>
<?php
    if ($this->cart->contents()) {
?>
<div class="container">
    <?php
        $message = $this->session->userdata('message');
        if (isset($message)) {
    ?>
       <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
          <strong><?php echo $message ?></strong>
        </div>
    <?php 
        $this->session->unset_userdata('message');
        }
        $error_message = $this->session->userdata('error_message');
        if (isset($error_message)) {
    ?>
      <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
        <strong><?php echo $error_message ?></strong>
      </div>
    <?php 
        $this->session->unset_userdata('error_message');
        }
    ?>

    <!-- /.End of page breadcrumbs -->
    <form action="<?php echo base_url('submit_checkout')?>" class="checkout-conent" method="post" id="validateForm">
        <div class="container">
            <h1><?php echo display('checkout')?></h1>
            <div class="row">
                <div class="col-sm-8">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <i class="fa fa-question-circle"></i> <?php echo display('returning_customer')?> <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><?php echo display('click_here_to_login')?></a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <?php
                                        if ($this->user_auth->is_logged() != 1) {
                                        ?>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label" for="login_email"><?php echo display('email')?> <abbr class="required" title="required">*</abbr></label>
                                                <input type="email" class="form-control" name="email" id="login_email" placeholder="<?php echo display('email')?>">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label" for="login_password"><?php echo display('password')?> <abbr class="required" title="required">*</abbr></label>
                                                <input type="password" class="form-control" name="password" id="login_password" placeholder="<?php echo display('password')?>">
                                            </div>
                                        </div>
                                        <?php
                                        }
                                        ?>
                                        <div class="col-sm-12">
                                            <?php
                                            if ($this->user_auth->is_logged() == 1) {
                                            ?>
                                            <a href="<?php echo base_url('website/home/logout')?>" class="btn btn-danger"><?php echo display('logout')?></a>
                                            <?php
                                            }else{
                                            ?>
                                            <div class="checkbox checkbox-success">
                                                <input id="brand1" type="checkbox" name="">
                                                <label for="brand1"><?php echo display('remember_me')?></label>
                                            </div>
                                            <button type="button" class="btn btn-warning customer_login"><?php echo display('login')?></button>
                                            <a href="#" class="lost-pass"><?php echo display('lost_your_password')?></a>
                                       <!--      <div class="social-content">
                                                <h5><?php echo display('connect_with')?>:</h5>
                                                <a href="#" class="social-connect fac"><i class="fa fa-facebook"></i></a>
                                                <a href="#" class="social-connect twi"><i class="fa fa-twitter"></i></a>
                                                <a href="#" class="social-connect goo"><i class="fa fa-google-plus"></i></a>
                                                <a href="#" class="social-connect lin"><i class="fa fa-linkedin"></i></a>
                                            </div> -->
                                            <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                       <!--  <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <i class="fa fa-question-circle"></i> <?php echo display('social_sign_in')?> <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><?php echo display('click_here_to_login')?></a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="social-content">
                                        <a href="#" class="social-connect fac"><i class="fa fa-facebook"></i></a>
                                        <a href="#" class="social-connect twi"><i class="fa fa-twitter"></i></a>
                                        <a href="#" class="social-connect goo"><i class="fa fa-google-plus"></i></a>
                                        <a href="#" class="social-connect lin"><i class="fa fa-linkedin"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <i class="fa fa-question-circle"></i> <?php echo display('use_coupon_code')?> <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><?php echo display('enter_your_coupon_here')?></a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body coupon">
                                    <div class="form-inline">
                                        <form class="coupon" action="<?php echo base_url('home/apply_coupon')?>" method="post">
                                            <input type="text" class="form-control" name="coupon_code" placeholder="<?php echo display('enter_your_coupon_here')?>">
                                            <button class="btn btn-warning"><?php echo display('apply_coupon')?></button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="billing-form">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3 class="billing-title"><?php echo display('billing_details')?></h3>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="first_name"><?php echo display('first_name')?> <abbr class="required" title="required">*</abbr></label>
                                    <input type="text" class="form-control" name="first_name" id="first_name" placeholder="<?php echo display('first_name')?>" required value="<?php echo $this->session->userdata('first_name')?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="last_name"><?php echo display('last_name')?> <abbr class="required" title="required">*</abbr></label>
                                    <input type="text" class="form-control" name="last_name" id="last_name" placeholder="<?php echo display('last_name')?>" required value="<?php echo $this->session->userdata('last_name')?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="customer_email"><?php echo display('email')?> </label>
                                    <input type="email" class="form-control" name="customer_email" id="customer_email" placeholder="<?php echo display('customer_email')?>" value="<?php echo $this->session->userdata('customer_email')?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="customer_mobile"><?php echo display('mobile')?> <abbr class="required" title="required">*</abbr></label>
                                    <input type="text" class="form-control" name="customer_mobile" id="customer_mobile" placeholder="<?php echo display('customer_mobile')?>" required value="<?php echo $this->session->userdata('customer_mobile')?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="country"><?php echo display('country')?> <abbr class="required" title="required">*</abbr></label>
                                    <select name="country" id="country" class="form-control" required>
                                        <option value=""><?php echo display('select_country')?></option>
                                        <?php
                                        if ($selected_country_info) {
                                            foreach ($selected_country_info as $country) {
                                        ?>
                                        <option value="<?php echo $country->id?>" <?php if (18 == $country->id) { echo "selected";} ?>><?php echo $country->name?> </option>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label"><?php echo display('address')?> <abbr class="required" title="required">*</abbr></label>
                                    <input type="text" placeholder="<?php echo display('customer_address_1')?>" name="customer_address_1" id="customer_address_1" class="form-control" required value="<?php echo $this->session->userdata('customer_address_1')?>">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="customer_address_2" id="customer_address_2" placeholder="<?php echo display('customer_address_2')?>" class="form-control" value="<?php echo $this->session->userdata('customer_address_2')?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="city"><?php echo display('city')?> </label>
                                    <input type="text" name="city" id="city" placeholder="<?php echo display('city')?>" class="form-control" value="<?php echo $this->session->userdata('city')?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="state"><?php echo display('state')?> <abbr class="required" title="required">*</abbr></label>
                                    <select name="state" id="state" class="form-control" required>
                                        <option value=""> --- <?php echo display('select_state')?> --- </option>
                                        <?php
                                        if ($state_list) {
                                            foreach ($state_list as $state) {
                                        ?>
                                        <option value="<?php echo $state->name?>" <?php if ($state->id == 348) { echo "selected";} ?>><?php echo $state->name?> </option>
                                        <?php
                                            }
                                        }
                                        ?>

                                    </select>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label" for="zip"><?php echo display('zip')?> </label>
                                            <input type="text" name="zip" id="zip" placeholder="<?php echo display('zip')?>" class="form-control" value="<?php echo $this->session->userdata('zip')?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label" for="company"><?php echo display('company')?></label>
                                            <input type="text" name="company" id="company" placeholder="<?php echo display('company')?>" class="form-control" value="<?php echo $this->session->userdata('company')?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        if ($this->user_auth->is_logged() != 1) {
                        ?>
                        <div class="checkbox checkbox-success">
                            <input id="creat_ac" type="checkbox" name="creat_ac" value="1">
                            <label for="creat_ac" data-toggle="collapse" data-target="#account-pass"><?php echo display('create_account')?></label> 
                        </div>
                        <div class="collapse" id="account-pass">
                            <div class="form-group">
                                <label class="control-label" for="ac_pass"><?php echo display('password')?> <abbr class="required" title="required">*</abbr></label>
                                <input type="password" name="ac_pass" id="ac_pass" placeholder="<?php echo display('password')?>" class="form-control" required value="<?php echo $this->session->userdata('ac_pass')?>" <?php if ($this->session->userdata('creat_ac') == 1){echo "checked";} ?>>
                            </div>
                        </div>
                        <?php
                        }
                        ?>

                        <div class="checkbox checkbox-success">
                            <input type="checkbox" id="privacy_policy" name="privacy_policy" value="1" required="" <?php if ($this->session->userdata('privacy_policy') == 1){echo "checked";} ?>>
                            <label for="privacy_policy"><?php echo display('privacy_policy')?></label>
                        </div>

                        <div class="checkbox checkbox-success">
                            <input type="checkbox" id="diff_ship_adrs" name="diff_ship_adrs" value="1" <?php if ($this->session->userdata('diff_ship_adrs') == 1) {echo "checked";}?>>
                            <label for="diff_ship_adrs"  data-toggle="collapse" data-target="#billind-different-address"><?php echo display('ship_to_a_different_address')?></label>
                        </div>

                        <div class="collapse <?php if ($this->session->userdata('diff_ship_adrs') == 1) {echo "in";}?>" id="billind-different-address">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label" for="ship_first_name"><?php echo display('first_name')?> <abbr class="required" title="required">*</abbr></label>
                                        <input type="text" name="ship_first_name" id="ship_first_name" placeholder="<?php echo display('first_name')?>" class="form-control" required value="<?php echo $this->session->userdata('ship_first_name')?>">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="ship_last_name"><?php echo display('last_name')?> <abbr class="required" title="required">*</abbr></label>
                                        <input type="text" name="ship_last_name" id="ship_last_name" placeholder="<?php echo display('last_name')?>" class="form-control" required value="<?php echo $this->session->userdata('ship_last_name')?>">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="ship_email"><?php echo display('email')?> </label>
                                        <input type="email" id="ship_email" name="ship_email" class="form-control" placeholder="<?php echo display('email')?>" value="<?php echo $this->session->userdata('ship_email')?>">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="ship_mobile"><?php echo display('mobile')?> <abbr class="required" title="required">*</abbr></label>
                                        <input type="text" name="ship_mobile" id="ship_mobile" placeholder="<?php echo display('mobile')?>" class="form-control" required value="<?php echo $this->session->userdata('ship_mobile')?>">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="ship_country"><?php echo display('country')?></label>
                                         <select name="ship_country" id="ship_country" class="form-control">
                                        <option value=""><?php echo display('select_country')?> </option>
                                            <?php
                                            if ($selected_country_info) {
                                                foreach ($selected_country_info as $country) {
                                            ?>
                                            <option value="<?php echo $country->id?>" <?php if (18 == $country->id) {echo "selected";}?>><?php echo $country->name?> </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label" for="ship_address_1"><?php echo display('address')?> <abbr class="required" title="required">*</abbr></label>
                                        <input type="text" name="ship_address_1" id="ship_address_1" placeholder="<?php echo display('customer_address_1')?>" class="form-control" required value="<?php echo $this->session->userdata('ship_address_1')?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="ship_address_2" id="ship_address_2" placeholder="<?php echo display('customer_address_2')?>" class="form-control" value="<?php echo $this->session->userdata('ship_address_2')?>">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="ship_city"><?php echo display('city')?> </label>
                                        <input type="text" name="ship_city" id="ship_city" class="form-control" placeholder="<?php echo display('city')?>" value="<?php echo $this->session->userdata('ship_city')?>">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="ship_state"><?php echo display('state')?> <abbr class="required" title="required">*</abbr></label>
                                        <select name="ship_state" id="ship_state" class="form-control">
                                           <option value=""><?php echo display('state')?> </option>
                                           <?php
                                            if ($ship_state_list) {
                                                foreach ($ship_state_list as $ship_state) {
                                            ?>
                                            <option value="<?php echo $ship_state->name?>" <?php if ($ship_state->id == 348) { echo "selected";} ?>><?php echo $ship_state->name?> </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label" for="ship_zip"><?php echo display('zip')?></label>
                                                <input type="text" name="ship_zip" id="ship_zip" placeholder="<?php echo display('zip')?>" class="form-control" value="<?php echo $this->session->userdata('ship_zip')?>">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label" for="ship_company"><?php echo display('company')?></label>
                                                <input type="text" name="ship_company" id="ship_company" placeholder="<?php echo display('company')?>" class="form-control" value="<?php echo $this->session->userdata('ship_company')?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="order_details"><?php echo display('order_details')?></label>
                            <textarea class="form-control" id="order_details" rows="5" placeholder="<?php echo display('order_details')?>"><?php echo $this->session->userdata('order_details')?></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="check-orde">
                        <h4><?php echo display('order_details')?></h4>
                        <table class="table" id="cart_details">
                            <thead>
                                <tr>
                                    <th class="product-name"><?php echo display('product')?></th>
                                    <th class="product-total"><?php echo display('variant')?></th>
                                    <th class="product-total"><?php echo display('total')?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $i = 1; $cgst = 0; $sgst = 0; $igst = 0; $discount = 0;$coupon_amnt=0;
                                foreach ($this->cart->contents() as $items):
                                    echo form_hidden($i.'[rowid]', $items['rowid']);

                                    if (!empty($items['options']['cgst'])) {
                                       $cgst = $cgst + ($items['options']['cgst'] * $items['qty']);
                                    }

                                    if (!empty($items['options']['sgst'])) {
                                       $sgst = $sgst + ($items['options']['sgst'] * $items['qty']);
                                    }

                                    if (!empty($items['options']['igst'])) {
                                       $igst = $igst + ($items['options']['igst'] * $items['qty']);
                                    }

                                    //Calculation for discount
                                    if (!empty($items['discount'])) {
                                       $discount = $discount + ($items['discount'] * $items['qty']) + $this->session->userdata('coupon_amnt');
                                       $this->session->set_userdata('total_discount',$discount);
                                    }
                                ?>
                                <tr class="cart_item">
                                    <td class="product-name">
                                        <?php echo $items['name']; ?>  
                                        <strong class="product-sum">× <?php echo $items['qty']; ?></strong> 
                                    </td>
                                    <td class="product-total"> 
                                    <?php
                                        if (!empty($items['variant'])) {
                                            $this->db->select('variant_name');
                                            $this->db->from('variant');
                                            $this->db->where('variant_id',$items['variant']);
                                            $query = $this->db->get();
                                            $var = $query->row();
                                            echo $var->variant_name;
                                        }
                                    ?> 
                                    </td>
                                    <td class="product-total text-right">
                                        <span class="woocommerce-Price-amount amount"><?php echo (($position==0)?$currency ." ". $this->cart->format_number($items['actual_price'] * $items['qty']):$this->cart->format_number($items['actual_price'] * $items['qty'])." ". $currency) ?>
                                        </span>
                                    </td>
                                </tr>
                                <?php $i++;?>
                                <?php endforeach; ?>
                            </tbody>
                            <tfoot>
                                <?php
                                if ($discount > 0) {
                                ?>
                                <tr class="cart-subtotal">
                                    <th><?php echo display('total_discount')?></th>
                                    <td></td>
                                    <td class="text-right">
                                        <span class="woocommerce-Price-amount amount">
                                            <?php echo (($position==0)?$currency ." ". number_format($discount, 2, '.', ','):number_format($discount, 2, '.', ',')." ". $currency)?>
                                        </span>
                                    </td>
                                </tr>
                                <?php
                                }
                                $total_tax = $cgst+$sgst+$igst;
                                $this->session->set_userdata('total_tax',$total_tax);
                                if ($total_tax > 0) {
                                ?>
                                <tr class="cart-subtotal">
                                    <th><?php echo display('total_tax')?></th>
                                    <td></td>
                                    <td class="text-right">
                                        <span class="woocommerce-Price-amount amount">
                                        <?php 
                                        $this->_cart_contents['total_tax'] = $total_tax;
                                        echo (($position==0)?$currency ." ". number_format($total_tax, 2, '.', ','):number_format($total_tax, 2, '.', ',')." ". $currency);
                                        ?>
                                        </span>
                                    </td>
                                </tr>
                                <?php 
                                }if ($this->_cart_contents['cart_ship_name']) {
                                ?>
                                <tr class="order-total">
                                    <th><?php echo $this->_cart_contents['cart_ship_name']?></th>
                                    <td></td>
                                    <td class="text-right">
                                        <strong>
                                            <span class="woocommerce-Price-amount amount">
                                            <?php
                                                $total_ship_cost = $this->_cart_contents['cart_ship_cost'];
                                                $this->session->set_userdata('cart_ship_cost',$total_ship_cost);
                                                echo (($position==0)?$currency ." ". number_format($total_ship_cost, 2, '.', ','):number_format($total_ship_cost, 2, '.', ',')." ". $currency);
                                            ?>
                                            </span>
                                        </strong>
                                    </td>
                                </tr>                      
                                <?php
                                }
                                $coupon_amnt = $this->session->userdata('coupon_amnt');
                                if ($coupon_amnt > 0) {
                                ?>
                                <tr class="order-total">
                                    <th><?php echo display('coupon_discount')?></th>
                                    <td></td>
                                    <td>
                                        <strong>
                                            <span class="woocommerce-Price-amount amount">
                                           <?php
                                            if ($coupon_amnt > 0) {
                                            echo (($position==0)?$currency ." ". number_format($coupon_amnt, 2, '.', ','):number_format($coupon_amnt, 2, '.', ',')." ". $currency);
                                            }

                                            ?>
                                            </span>
                                        </strong>
                                    </td>
                                </tr>   
                                <?php
                                }
                                ?>
                                <tr class="order-total">
                                    <th><?php echo display('total')?></th>
                                    <td></td>
                                    <td class="text-right">
                                        <strong>
                                            <span class="woocommerce-Price-amount amount">
                                            <?php 
                                                $cart_total = $this->cart->total() + $this->_cart_contents['cart_ship_cost'] + $total_tax - $coupon_amnt;
                                               $this->session->set_userdata('cart_total',$cart_total);
                                                $total_amnt = $this->_cart_contents['cart_total'] = $cart_total;
                                                echo (($position==0)?$currency ." ". number_format($total_amnt, 2, '.', ','):number_format($total_amnt, 2, '.', ',')." ". $currency);
                                            ?>
                                            </span>
                                        </strong>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                        <!-- /.End of product list table -->
                        <div class="payment-block" id="payment">
                            <h4><?php echo display('delivery_details')?></h4>
                            <?php
                            if ($select_shipping_method) {
                                $i=1;
                                foreach ($select_shipping_method as $shipping_method) {
                            ?>
                            <div class="payment-item">
                                <input type="radio" class="shipping_cost" name="method_id" id="<?php echo $shipping_method->method_id?>" data-parent="#payment<?php echo $shipping_method->method_id?>" data-target="#description_cre<?php echo $shipping_method->method_id?>" value="<?php echo $shipping_method->charge_amount?>" alt="<?php echo $shipping_method->method_name?>" <?php if ($this->session->userdata('method_id') == $shipping_method->method_id){echo "checked";}elseif($i==1){echo "checked";}?> >
                                <label for="<?php echo $shipping_method->method_id?>"> <?php echo $shipping_method->method_name?> </label>
                                <div class="description collapse" id="description_cre<?php echo $shipping_method->method_id?>">
                                    <p><?php echo $shipping_method->details?> - 
                                        <?php
                                            $default_currency_id =  $this->session->userdata('currency_id');
                                            $currency_new_id     =  $this->session->userdata('currency_new_id');

                                            if (empty($currency_new_id)) {
                                                $result  =  $cur_info = $this->db->select('*')
                                                                    ->from('currency_info')
                                                                    ->where('default_status','1')
                                                                    ->get()
                                                                    ->row();
                                                $currency_new_id = $result->currency_id;
                                            }

                                            if (!empty($currency_new_id)) {
                                                $cur_info = $this->db->select('*')
                                                                    ->from('currency_info')
                                                                    ->where('currency_id',$currency_new_id)
                                                                    ->get()
                                                                    ->row();

                                                $target_con_rate = $cur_info->convertion_rate;
                                                $position1 = $cur_info->currency_position;
                                                $currency1 = $cur_info->currency_icon;
                                            }

                                            if ($target_con_rate > 1) {
                                                $price = $shipping_method->charge_amount * $target_con_rate;
                                                echo (($position1==0)?$currency1." ".number_format($price, 2, '.', ','):number_format($price, 2, '.', ',')." ".$currency1);
                                            }

                                            if ($target_con_rate <= 1) {
                                                $price = $shipping_method->charge_amount * $target_con_rate;
                                                echo (($position1==0)?$currency1." ".number_format($price, 2, '.', ','):number_format($price, 2, '.', ',')." ".$currency1);
                                            }
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <?php
                                $i++;
                                }
                            }
                            ?>
                            <h4><?php echo display('payment')?></h4>

                            <!-- /.Cash on delivery -->
                            <div class="payment-item">
                                <input type="radio" name="payment_method" id="payment_method_dir" data-parent="#payment2" data-target="#description_dir" value="1" <?php if ($this->session->userdata('payment_method') == 1) {echo "checked ='checked'";} ?> checked >
                                <label for="payment_method_dir"> <?php echo display('cash_on_delivery')?> </label>
                                <div class="description collapse" id="description_dir">
                                    <p><?php echo display('cash_on_delivery')?></p>
                                </div>
                            </div>
                            <?php
                            if ($bitcoin_status == 1) {
                            ?>
                          <!--   <div class="payment-item">
                                <input type="radio" name="payment_method" id="payment_method_bit" data-parent="#payment" data-target="#description_bit" value="3" <?php if ($this->session->userdata('payment_method') == 3 ) { echo "checked = 'checked'"; } ?>>
                                <label for="payment_method_bit"><?php echo display('bitcoin')?>  <img src="<?php echo base_url()?>assets/website/img/bitcoin.png" alt=""></label>
                                <div class="description collapse" id="description_bit">
                                    <p><?php echo display('bitcoin')?></p>
                                </div>
                            </div> -->
                            <!-- /.Bitcoin -->  
                            <?php } ?>
                            <?php
                            if ($payeer_status == 1) {
                            ?>
                           <!--  <div class="payment-item">
                                <input type="radio" name="payment_method" id="payment_method_payeer" data-parent="#payment" data-target="#description_payer" value="4" <?php if ($this->session->userdata('payment_method') == 4 ) { echo "checked = 'checked'"; } ?>>
                                <label for="payment_method_payeer"><?php echo display('payeer')?>  <img src="<?php echo base_url()?>assets/website/img/payeer.png" alt=""></label>
                                <div class="description collapse" id="description_payer">
                                    <p><?php echo display('payeer')?></p>
                                </div>
                            </div> -->
                            <?php } ?>
                            <?php
                            if ($paypal_status == 1) {
                            ?>
                            <!-- <div class="payment-item">
                                <input type="radio" name="payment_method" id="payment_method_paypal" data-parent="#payment" data-target="#description_paypal" value="5" <?php if ($this->session->userdata('payment_method') == 5 ) { echo "checked = 'checked'"; } ?>>
                                <label for="payment_method_paypal"><?php echo display('paypal')?>  <img src="<?php echo base_url()?>assets/website/img/AM_mc_vs_dc_ae.jpg" alt=""> 
                                </label>
                                <div class="description collapse" id="description_paypal">
                                    <p><?php echo display('paypal')?></p>
                                </div>
                            </div> -->
                            <!-- /.PayPal -->
                            <?php } ?>
                         
                        </div>
                        <!-- /.End of payment method --> 
                        <button type="submit" class="btn btn-warning btn-block"><?php echo display('confirm_order')?></button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<?php }else{ ?>
<div class="container">
    <h1><?php echo display('oops_your_cart_is_empty')?></h1>
</div>
<?php } ?>

<script type="text/javascript">
    $(document).ready(function(){
        $("#validateForm").validate({
            errorElement: 'span',
            errorClass: 'help-block',
            rules: { 
                first_name: {
                    required: true
                }, 
                ship_first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },  
                ship_last_name: {
                    required: true
                },
                customer_mobile: {
                    required: true,
                },     
                ship_mobile: {
                    required: true,
                },
                country: {
                    required: true,
                },  
                ship_country: {
                    required: true,
                },
                customer_address_1: {
                    required: true,
                },    
                ship_address_1: {
                    required: true,
                },
                state: {
                    required: true,
                },  
                ship_state: {
                    required: true,
                },
            },
            messages: {
                first_name: {
                    required: "<?php echo display('first_name_is_required');?>",
                }, 
                ship_first_name: {
                    required: "<?php echo display('first_name_is_required');?>",
                },
                last_name: {
                    required: "<?php echo display('last_name_is_required');?>",
                },  
                ship_last_name: {
                    required: "<?php echo display('last_name_is_required');?>",
                },  
                customer_mobile: {
                    required: "<?php echo display('mobile_is_required');?>",
                },
                ship_mobile: {
                    required: "<?php echo display('mobile_is_required');?>",
                },
                country: {
                    required: "<?php echo display('country_is_required');?>",
                },          
                ship_country: {
                    required: "<?php echo display('country_is_required');?>",
                },  
                customer_address_1: {
                    required: "<?php echo display('address_is_required');?>",
                }, 
                ship_address_1: {
                    required: "<?php echo display('address_is_required');?>",
                },
                state: {
                    required: "<?php echo display('state_is_required');?>",
                },         
                ship_state: {
                    required: "<?php echo display('state_is_required');?>",
                },
            },
            errorPlacement: function(error, element) {
                if (error) {
                    $(element).parent().attr('class','form-group has-error');
                    $(element).parent().append(error);
                }else{
                    $(element).parent().attr('class','form-group');
                }
            },
            success: function (error,element) {
                $(element).parent().attr('class','form-group');
            }
        });
    });
</script>

<!-- Customer login by ajax start-->
<script type="text/javascript">
    $('body').on('click', '.customer_login', function() {
        var login_email    = $('#login_email').val();
        var login_password = $('#login_password').val();

        if (login_email == 0 || login_password == 0) {
            alert('<?php echo display('please_type_email_and_password');?>');
            return false;
        }

        $.ajax({
            type: "post",
            async: true,
            url: '<?php echo base_url('website/customer/Login/checkout_login')?>',
            data: {login_email:login_email,login_password:login_password},
            success: function(data) {
                if (data == 'true') {
                    alert('<?php echo display('login_successfully');?>');
                    location.reload();
                }else{
                    alert('<?php echo display('wrong_username_or_password');?>');
                    location.reload();
                }
            },
            error: function() {
                alert('Request Failed, Please check your code and try again!');
            }
        });
    }); 
</script>
<!-- Customer login by ajax start-->

<!-- Retrive district ajax code start-->
<script type="text/javascript">
    //Retrive district
    $('body').on('change', '#country', function() {
        var country_id = $('#country').val();
        if (country_id == 0) {
            alert('<?php echo display('country_is_required');?>');
            return false;
        }
        $.ajax({
            type: "post",
            async: true,
            url: '<?php echo base_url('website/Home/retrive_district')?>',
            data: {country_id:country_id},
            success: function(data) {
                if (data) {
                    $("#state").html(data);
                }else{
                    $("#state").html('<p style="color:red"><?php echo display('failed');?> </p>'); 
                }
            },
            error: function() {
                alert('Request Failed, Please check your code and try again!');
            }
        });
    }); 
</script>
<!-- Retrive district ajax code end-->

<!-- Retrive shipping district ajax code start-->
<script type="text/javascript">
    //Retrive district
    $('body').on('change', '#ship_country', function() {
        var country_id = $('#ship_country').val();
        if (country_id == 0) {
            alert('<?php echo display('country_is_required');?>');
            return false;
        }
        $.ajax({
            type: "post",
            async: true,
            url: '<?php echo base_url('website/Home/retrive_district')?>',
            data: {country_id:country_id},
            success: function(data) {
                if (data) {
                    $("#ship_state").html(data);
                }else{
                    $("#ship_state").html('<p style="color:red><?php echo display('failed');?> !</p>'); 
                }
            },
            error: function() {
                alert('Request Failed, Please check your code and try again!');
            }
        });
    }); 
</script>
<!-- Retrive shipping district ajax code end -->

<!-- Push delivery cost to cache by ajax -->
<script type="text/javascript">

    //Shipping to different address
    $('#diff_ship_adrs').click(function() {
        var check=$('[name="diff_ship_adrs"]:checked').length;
        if (check > 0) {
            $('input[name="diff_ship_adrs"]').attr("checked", "checked");
        }else {
            $('input[name="diff_ship_adrs"]').removeAttr('checked');
        }
    });
    
    //Privacy policy
    $('#privacy_policy').click(function() {
        var check=$('[name="privacy_policy"]:checked').length;
        if (check > 0) {
            $('input[name="privacy_policy"]').attr("checked", "checked");
        }else {
            $('input[name="privacy_policy"]').removeAttr('checked');
        }
    });   

    //Create account
    $('#creat_ac').click(function() {
        var check = $('[name="creat_ac"]:checked').length;
        if (check > 0) {
            $('input[name="creat_ac"]').attr("checked", "checked");
        }else {
            $('input[name="creat_ac"]').removeAttr('checked');
        }
    });

    //Onkeyup change session value
    $('body').on('keyup click change', '#first_name,#last_name,#customer_email,#customer_mobile,#customer_address_1,#customer_address_2,#company,#city,#zip,#country,#state,#ac_pass,#privacy_policy,.shipping_cost,#ship_first_name,#ship_last_name,#ship_email,#ship_mobile,#ship_country,#ship_address_1,#ship_address_2,#ship_city,#ship_state,#ship_zip,#ship_company,#order_details,#creat_ac', function() {

        var shipping_cost   = $('input[name=method_id]:checked').val();
        var ship_cost_name  = $('input[name=method_id]:checked').attr('alt');
        var method_id       = $('input[name=method_id]:checked').attr('id');

        //Ship and billing info
        var first_name      = $('#first_name').val();
        var last_name       = $('#last_name').val();
        var customer_email  = $('#customer_email').val();
        var customer_mobile = $('#customer_mobile').val();
        var customer_address_1 = $('#customer_address_1').val();
        var customer_address_2 = $('#customer_address_2').val();
        var company         = $('#company').val();
        var city            = $('#city').val();
        var zip             = $('#zip').val();
        var country         = $('#country').val();
        var state           = $('#state').val();
        var ac_pass        = $('#ac_pass').val();
        var privacy_policy  = $('#privacy_policy').attr("checked") ? 1 : 0;
        var creat_ac        = $('#creat_ac').attr("checked") ? 1 : 0;

        var ship_first_name = $('#ship_first_name').val();
        var ship_last_name  = $('#ship_last_name').val();
        var ship_company    = $('#ship_company').val();
        var ship_mobile     = $('#ship_mobile').val();
        var ship_email      = $('#ship_email').val();
        var ship_address_1  = $('#ship_address_1').val();
        var ship_address_2  = $('#ship_address_2').val();
        var ship_city       = $('#ship_city').val();
        var ship_zip        = $('#ship_zip').val();
        var ship_country    = $('#ship_country').val();
        var ship_state      = $('#ship_state').val();
        var payment_method  = $('input[name=\'payment_method\']:checked').val();
        var order_details   = $('#order_details ').val();
        var diff_ship_adrs  = $('#diff_ship_adrs').attr("checked") ? 1 : 0;
        $.ajax({
            type: "post",
            async: true,
            url: '<?php echo base_url('website/Home/set_ship_cost_cart')?>',
            data: {
                shipping_cost:shipping_cost,
                ship_cost_name:ship_cost_name,
                method_id:method_id,
                first_name:first_name,
                last_name:last_name,
                customer_email:customer_email,
                customer_mobile:customer_mobile,
                customer_address_1:customer_address_1,
                customer_address_2:customer_address_2,
                company:company,
                city:city,
                zip:zip,
                country:country,
                state:state,
                ac_pass:ac_pass,
                privacy_policy:privacy_policy,
                creat_ac:creat_ac,
                ship_first_name:ship_first_name,
                ship_last_name:ship_last_name,
                ship_company:ship_company,
                ship_mobile:ship_mobile,
                ship_email:ship_email,
                ship_address_1:ship_address_1,
                ship_address_2:ship_address_2,
                ship_city:ship_city,
                ship_zip:ship_zip,
                ship_country:ship_country,
                ship_state:ship_state,
                payment_method:payment_method,
                order_details:order_details,
                diff_ship_adrs:diff_ship_adrs,
            },
            success: function(data) {
                return true;
            },
            error: function() {
               // alert('Request Failed, Please check your code and try again!');
            }
        });
    }); 
</script>
<!-- Push delivery cost to cache by ajax  -->