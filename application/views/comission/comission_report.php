<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Comission Report Start -->
<div class="content-wrapper">
	<section class="content-header">
	    <div class="header-icon">
	        <i class="pe-7s-note2"></i>
	    </div>
	    <div class="header-title">
	        <h1><?php echo display('comission_report') ?></h1>
	        <small><?php echo display('comission_report') ?></small>
	        <ol class="breadcrumb">
	            <li><a href=""><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
	            <li><a href="#"><?php echo display('comission') ?></a></li>
	            <li class="active"><?php echo display('comission_report') ?></li>
	        </ol>
	    </div>
	</section>

	<section class="content">

		<!-- Alert Message -->
	    <?php
	        $message = $this->session->userdata('message');
	        if (isset($message)) {
	    ?>
	    <div class="alert alert-info alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        <?php echo $message ?>                    
	    </div>
	    <?php 
	        $this->session->unset_userdata('message');
	        }
	        $error_message = $this->session->userdata('error_message');
	        if (isset($error_message)) {
	    ?>
	    <div class="alert alert-danger alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        <?php echo $error_message ?>                    
	    </div>
	    <?php 
	        $this->session->unset_userdata('error_message');
	        }
	    ?>

        <!-- Invoice filtering -->
        <div class="row">
			<div class="col-sm-12">
		        <div class="panel panel-default">
		            <div class="panel-body"> 

		            	<form action="<?php echo base_url('ccomission/comission_report')?>" method="get">
		     				<div class="row">
	                            
	                            <div class="col-sm-3">
	                                <div class="form-group row">
	                                    <label for="seller" class="col-sm-4 col-form-label"><?php echo display('seller')?> </label>
	                                    <div class="col-sm-8">
	                                      	<select class="form-control" name="seller_id" id="seller">
	                                      		<option></option>
	                                      		<?php
	                                      		if ($seller_list) {
	                                      			foreach ($seller_list as $seller) {
	                                      		?>
	                                      		<option value="<?php echo $seller['seller_id']?>" <?php if (isset($_GET['seller_id'])) {if ($_GET['seller_id'] == $seller['seller_id']) {echo "selected";}}?>><?php echo $seller['first_name'].' '.$seller['last_name']?></option>
	                                      		<?php
	                                      			}
	                                      		}
	                                      		?>
	                                      	</select>
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-3">
	                                <div class="form-group row">
	                                    <label for="product" class="col-sm-4 col-form-label"><?php echo display('product')?> </label>
	                                    <div class="col-sm-8">
	                                      	<select class="form-control" name="product_id" id="product">
	                                      		<option></option>
	                                      		<?php
	                                      		if ($manage_product) {
	                                      			foreach ($manage_product as $product) {
	                                      		?>
	                                      		<option value="<?php echo $product['product_id']?>" <?php if (isset($_GET['product_id'])) {if ($_GET['product_id'] == $product['product_id']) {echo "selected";}}?>><?php echo $product['title'].' ('.$product['product_model'].')'?></option>
	                                      		<?php
	                                      			}
	                                      		}
	                                      		?>
	                                      	</select>
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-3">
	                                <div class="form-group row">
	                                    <label for="category" class="col-sm-5 col-form-label"><?php echo display('category')?> </label>
	                                    <div class="col-sm-7">
	                                      	<select class="form-control" name="category_id" id="category">
	                                      		<option></option>
	                                      		<?php
	                                      		if ($category_list) {
	                                      			foreach ($category_list as $category) {
	                                      		?>
	                                      		<option value="<?php echo $category['category_id']?>" <?php if (isset($_GET['product_id'])) {if ($_GET['category_id'] == $category['category_id']) {echo "selected";}}?>><?php echo $category['category_name']?></option>
	                                      		<?php
	                                      			}
	                                      		}
	                                      		?>
	                                      	</select>
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-1">
	                                <div class="form-group row">
	                                    <div class="col-sm-7">
	                                      	<button type="submit" class="btn btn-primary"><?php echo display('search')?></button>
	                                    </div>
	                                </div>
	                            </div>

	                        </div>  
		            	</form>   
			        </div>
		        </div>
		    </div>
		</div>

		<!-- Comission Report -->
		<div class="row">
		    <div class="col-sm-12">
		        <div class="panel panel-bd lobidrag">
		            <div class="panel-heading">
		                <div class="panel-title">
		                    <h4><?php echo display('comission_report') ?></h4>
		                </div>
		            </div>
		            <div class="panel-body">
		                <div class="table-responsive">
		                    <table id="dataTableExample3" class="table table-bordered table-striped table-hover">
								<thead>
									<tr>
										<th class="text-center"><?php echo display('sl') ?></th>
										<th class="text-center"><?php echo display('seller') ?></th>
										<th class="text-center"><?php echo display('product') ?></th>
										<th class="text-center"><?php echo display('category') ?></th>
										<th class="text-center"><?php echo display('percentage') ?></th>
										<th class="text-center"><?php echo display('quantity') ?></th>
										<th class="text-center"><?php echo display('rate') ?></th>
										<th class="text-center"><?php echo display('total_price') ?></th>
										<th class="text-center"><?php echo display('discount') ?></th>
										<th class="text-center"><?php echo display('comission') ?></th>
									</tr>
								</thead>
								<tbody>
								<?php
								if ($comission_report) {
									foreach ($comission_report as $comission) {
								?>
									<tr>
										<td class="text-center"><?php echo $comission['sl']?></td>
										<td class="text-center"><?php
										$seller_id = $comission['seller_id'];
										echo $this->Comissions->seller_name($seller_id);
										?></td>
										<td class="text-center"><?php
										$product_id = $comission['product_id'];
										echo $this->Comissions->product_name($product_id);
										?></td>
										<td class="text-center"><?php echo $comission['category_name']?></td>
										<td class="text-center"><?php echo $comission['seller_percentage']?> %</td>
										<td class="text-center"><?php echo $comission['quantity']?></td>
										<td class="text-center"><?php echo (($position==0)?$currency.$comission['rate']:$comission['rate'].$currency) ?></td>
										<td class="text-center"><?php echo (($position==0)?$currency.$comission['total_price']:$comission['total_price'].$currency) ?></td>
										<td class="text-center"><?php echo (($position==0)?$currency.$comission['discount']:$comission['discount'].$currency) ?></td>
										<td class="text-center"><?php echo (($position==0)?$currency.$comission['total_income']:$comission['total_income'].$currency) ?></td>
									</tr>
								<?php
									}
								}
								?>
								</tbody>
								<tfoot>
									<td colspan="7" class="text-right"><b><?php echo display('total_amount')?>:</b></td>
									<td class="text-right"><b><?php echo (($position==0)?"$currency {total_price}":"{total_price} $currency") ?></b></td>
									<td class="text-right"><b><?php echo (($position==0)?"$currency {total_discount}":"{total_discount} $currency") ?></b></td>
									<td class="text-right"><b><?php echo (($position==0)?"$currency {total_amount}":"{total_amount} $currency") ?></b></td>
								</tfoot>
		                    </table>
		                </div>
		                <div class="text-right">
		                	<?php echo $links?>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</section>
</div>
<!-- Comission Report End -->