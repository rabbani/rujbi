<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php
    $CI =& get_instance();
    $CI->load->model('Soft_settings');
    $Soft_settings = $CI->Soft_settings->retrieve_setting_editdata();
?>

<!-- Printable area start -->
<script type="text/javascript">
function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
	// document.body.style.marginTop="-45px";
    window.print();
    document.body.innerHTML = originalContents;
}
</script>
<!-- Printable area end -->

<style>

.sign {
    border-top: 1px solid #ddd;
    margin: 20px;
    text-align: center;
}
</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="header-icon">
            <i class="pe-7s-note2"></i>
        </div>
        <div class="header-title">
            <h1><?php echo display('invoice_details') ?></h1>
            <small><?php echo display('invoice_details') ?></small>
            <ol class="breadcrumb">
                <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
                <li><a href="#"><?php echo display('invoice') ?></a></li>
                <li class="active"><?php echo display('invoice_details') ?></li>
            </ol>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
    	<!-- Alert Message -->
	    <?php
	        $message = $this->session->userdata('message');
	        if (isset($message)) {
	    ?>
	    <div class="alert alert-info alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        <?php echo $message ?>                    
	    </div>
	    <?php 
	        $this->session->unset_userdata('message');
	        }
	        $error_message = $this->session->userdata('error_message');
	        if (isset($error_message)) {
	    ?>
	    <div class="alert alert-danger alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        <?php echo $error_message ?>                    
	    </div>
	    <?php 
	        $this->session->unset_userdata('error_message');
	        }
	    ?>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-bd">
	                <div id="printableArea">
	                    <div class="panel-body">
	                       <table border="0">
						      <tr>
						        <td>
						        <table class="table">
						            <tr>
						            	{company_info}
						               	<td colspan="2" align="center" style="border-bottom:2px #333 solid;"><span style="font-size: 17pt; font-weight:bold;">{company_name}</span><br>
						                    {address}<br>
						                    {mobile}<br>
						                    {email}<br>
						                    {website}
						                </td>
						                {/company_info}
						            </tr>
						            <br>
						            <tr>
						              <td align="left">
						              	<div><b>{customer_name}</b></div>
						              	<div><b>{customer_mobile}</b></div>
						              	<div style="width: 150px">{customer_address}</div>
						              	<div>{customer_email}</div>
						              </td>
						              <td align="right">
						              	<div><?php echo display('invoice_no') ?>:{invoice_no}</div>
						              	<?php if ($order_no) { ?>
		                                <div><strong><?php echo display('order_no') ?>: {order_no}</strong></div>
		                                <?php } ?>
		                                <div><?php echo display('date')?>:{final_date}</div>
						              </td>
						            </tr>
						          </table>

						          <table class="table">
						            <tr>
						            	<th><?php echo display('product')?></th>
						              	<th><?php echo display('quantity')?></th>
						              	<th><?php echo display('price')?></th>
						              	<th><?php echo display('dis')?></th>
						              	<th><?php echo display('total')?></th>
						            </tr>
						            {invoice_all_data}
									<tr>
										<th style="width: 110px">{product_name} - ({product_model})</th>
                                    	<td>{quantity}</td>
                                        <td><?php echo (($position==0)?"$currency {rate}":"{rate} $currency") ?></td>
                                        <td><?php echo (($position==0)?"$currency {discount}":"{discount} $currency") ?></td>
                                        <td><?php echo (($position==0)?"$currency {total_price}":"{total_price} $currency") ?></td>
                                    </tr>
                                    {/invoice_all_data}
						            <tr>
						              <td align="left"><nobr></nobr></td>
						              <td align="left" colspan="3"><nobr><?php echo display('delivery_charge')?></nobr></td>
						              <td align="right"><nobr><?php echo (($position==0)?"$currency {service_charge}":"{service_charge} $currency") ?></nobr></td>
						            </tr>
						            <tr>
						              <td align="left"><nobr></nobr></td>
						              <td align="left" colspan="3"><nobr><?php echo display('total_discount')?>t</nobr></td>
						              <td align="right"><nobr><?php echo (($position==0)?"$currency {total_discount}":"{total_discount} $currency") ?></nobr></nobr></td>
						            </tr>
						            <tr>
						              <td align="left"><nobr></nobr></td>
						              <td align="left" colspan="3"><nobr><strong><?php echo display('grand_total')?></strong></nobr></td>
						              <td align="right"><nobr><strong><?php echo (($position==0)?"$currency {total_amount}":"{total_amount} $currency") ?></strong></nobr></td>
						            </tr>
						            <tr>
						              <td align="left"><nobr></nobr></td>
						              <td align="left" colspan="3"><nobr><?php echo display('paid_ammount') ?></nobr></td>
						              <td align="right"><nobr><?php echo (($position==0)?"$currency {paid_amount}":"{paid_amount} $currency") ?></nobr></td>
						            </tr>
						            <tr>
						              <td align="left"><nobr></nobr></td>
						              <td align="left" colspan="3"><nobr><?php echo display('due') ?></nobr></td>
						              <td align="right"><nobr><?php echo (($position==0)?"$currency {due_amount}":"{due_amount} $currency") ?></nobr></td>
						            </tr>
						          </table>
								    <table width="100%">
								    	<?php if ($invoice_details) { ?>
								    	<tr style="margin-bottom: 20px;width: 20px">
								    		<td><?php echo display('details')?>:{invoice_details}</td>
								    	</tr>
								    	<?php } ?>
							          	<tr style="margin-top:20px;">
							          		<td>
												<div class="sign" style="border-top: 1px solid #ddd;margin: 20px;text-align: center;"><?php echo display('sign_office')?></div>
							          		</td>
							          		<td><div class="sign" style="border-top: 1px solid #ddd;margin: 20px;text-align: center;"><?php echo display('customer_sign')?></div></td>
							          	</tr>
						          	</table>
						        </td>
						      </tr>
						    </table>
	                    </div>
	                </div>

                    <div class="panel-footer text-left">
                     	<a  class="btn btn-danger" href="<?php echo base_url('cinvoice/manage_invoice');?>"><?php echo display('cancel') ?></a>
						<a  class="btn btn-info" href="#" onclick="printDiv('printableArea')"><span class="fa fa-print"></span>
						</a>
                    </div>

                </div>
            </div>
        </div>
    </section> <!-- /.content -->
</div> <!-- /.content-wrapper -->