<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Manage Invoice Start -->
<div class="content-wrapper">
	<section class="content-header">
	    <div class="header-icon">
	        <i class="pe-7s-note2"></i>
	    </div>
	    <div class="header-title">
	        <h1><?php echo display('manage_invoice') ?></h1>
	        <small><?php echo display('manage_your_invoice') ?></small>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
	            <li><a href="#"><?php echo display('invoice') ?></a></li>
	            <li class="active"><?php echo display('manage_invoice') ?></li>
	        </ol>
	    </div>
	</section>

	<section class="content">
		<!-- Alert Message -->
	    <?php
	        $message = $this->session->userdata('message');
	        if (isset($message)) {
	    ?>
	    <div class="alert alert-info alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        <?php echo $message ?>                    
	    </div>
	    <?php 
	        $this->session->unset_userdata('message');
	        }
	        $error_message = $this->session->userdata('error_message');
	        if (isset($error_message)) {
	    ?>
	    <div class="alert alert-danger alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        <?php echo $error_message ?>                    
	    </div>
	    <?php 
	        $this->session->unset_userdata('error_message');
	        }
	    ?>

	    <?php if($this->permission->check_label('new_invoice')->access()){?>
        <div class="row">
            <div class="col-sm-12">
                <div class="column">
                  <a href="<?php echo base_url('new_invoice')?>" class="btn btn-info m-b-5 m-r-2"><i class="ti-align-justify"> </i> <?php echo display('new_invoice')?></a>
                </div>
            </div>
        </div>
    	<?php } ?>

        <!-- Invoice filtering -->
        <div class="row">
			<div class="col-sm-12">
		        <div class="panel panel-default">
		            <div class="panel-body"> 

		            	<form action="<?php echo base_url('cinvoice/manage_invoice/all/item')?>" method="get">
		     				<div class="row">

	                            <div class="col-sm-4">
	                                <div class="form-group row">
	                                    <label for="invoice_no" class="col-sm-5 col-form-label"><?php echo display('invoice_no')?></label>
	                                    <div class="col-sm-7">
	                                      	<input class="form-control" name ="invoice_no" id="invoice_no" type="text" placeholder="<?php echo display('invoice_no') ?>" value="<?php if(isset($_GET['invoice_no'])){ echo $_GET['invoice_no'];}?>">
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-4">
	                                <div class="form-group row">
	                                    <label for="order_no" class="col-sm-5 col-form-label"><?php echo display('order_no')?></label>
	                                    <div class="col-sm-7">
	                                      	<input class="form-control" name ="order_no" id="order_no" type="text" placeholder="<?php echo display('order_no') ?>" value="<?php if(isset($_GET['order_no'])){ echo $_GET['order_no'];}?>">
	                                    </div>
	                                </div>
	                            </div>
	                            
	                            <div class="col-sm-4">
	                                <div class="form-group row">
	                                    <label for="customer" class="col-sm-4 col-form-label"><?php echo display('mobile')?> </label>
	                                    <div class="col-sm-8">
	                                      	<select class="form-control" name="customer" id="customer">
	                                      		<option></option>
	                                      		<?php
	                                      		if ($customer_list) {
	                                      			foreach ($customer_list as $customer) {
	                                      		?>
	                                      		<option value="<?php echo $customer['customer_id']?>" <?php if (isset($_GET['customer'])) {if ($_GET['customer'] == $customer['customer_id']) {echo "selected";}}?>><?php echo $customer['customer_mobile']?></option>
	                                      		<?php
	                                      			}
	                                      		}
	                                      		?>
	                                      	</select>
	                                    </div>
	                                </div>
	                            </div>

<!--                                <div class="col-sm-4">-->
<!--	                                <div class="form-group row">-->
<!--	                                    <label for="customer" class="col-sm-4 col-form-label">--><?php //echo display('name')
//                                            ?><!-- </label>-->
<!--	                                    <div class="col-sm-8">-->
<!--	                                      	<select class="form-control" name="customer_name" id="customer">-->
<!--	                                      		<option></option>-->
<!--	                                      		--><?php
//	                                      		if ($customer_list) {
//	                                      			foreach ($customer_list as $customer) {
//	                                      		?>
<!--	                                      		<option value="--><?php //echo $customer['customer_name']?><!--" --><?php //if (isset
//                                                ($_GET['customer'])) {if ($_GET['customer'] == $customer['customer_id']) {echo "selected";}}?><!-->--><?php //echo $customer['customer_name']?><!--</option>-->
<!--	                                      		--><?php
//	                                      			}
//	                                      		}
//	                                      		?>
<!--	                                      	</select>-->
<!--	                                    </div>-->
<!--	                                </div>-->
<!--	                            </div>-->

                                <div class="col-sm-4">
                                    <div class="form-group row">
                                        <label for="customer_name"
                                               class="col-sm-4 col-form-label"><?php echo display('name') ?> </label>
                                        <div class="col-sm-8">
                                            <input class="form-control" name="customer_name" id="customer_name" type="text"
                                                   placeholder="<?php echo display('name') ?>"
                                                   value="<?php if (isset($_GET['customer'])) {
                                                       echo $_GET['customer'];
                                                   } ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group row">
                                        <label for="shipping" class="col-sm-4 col-form-label"><?php echo display('shipping')?></label>
                                        <div class="col-sm-8">
                                            <select class="form-control" id="shipping" name="shipping">
                                                <option value=""></option>
                                                <?php
                                                if ($shipping_method_list) {
                                                    foreach ($shipping_method_list as $method) {
                                                        ?>
                                                        <option value="<?php echo $method['method_id']?>" <?php if (isset($_GET['shipping'])){if ($_GET['shipping'] == $method['method_id']) {echo "selected";}}?>><?php echo $method['method_name']?></option>
                                                    <?php } }?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

	                            <div class="col-sm-4">
	                                <div class="form-group row">
	                                    <label for="date" class="col-sm-5 col-form-label"><?php echo display('date')?></label>
	                                    <div class="col-sm-7">
	                                      	<input type="text" class="form-control datepicker-here" id="date"
                                                   data-range="true"  data-multiple-dates-separator="---"
                                                   data-language='en' name="date" placeholder="<?php echo display('date') ?>"
                                                   value="<?php if(isset($_GET['date'])){ echo $_GET['date'];}?>">
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-2">
	                                <div class="form-group row">
	                                    <div class="col-sm-8">
	                                      	<button type="submit" class="btn btn-primary"><?php echo display('search')?></button>
	                                    </div>
	                                </div>
	                            </div>

	                        </div>  
		            	</form>   
			        </div>
		        </div>
		    </div>
		</div>

		<!-- Manage Invoice -->
		<div class="row">
		    <div class="col-sm-12">
		        <div class="panel panel-bd lobidrag">
		            <div class="panel-heading">
		                <div class="panel-title">
		                    <h4><?php echo display('manage_invoice') ?></h4>
		                </div>
		            </div>
		            <div class="panel-body">
		                <div class="table-responsive">
		                    <table id="dataTableExample3" class="table table-bordered table-striped table-hover">
		                    	<thead>
									<tr>
										<th><?php echo display('sl') ?></th>
										<th><?php echo display('invoice_no') ?></th>
										<th><?php echo display('order_no') ?></th>
										<th><?php echo display('customer_name') ?></th>
										<th><?php echo display('shipping') ?></th>
										<th><?php echo display('date')?></th>
										<th><?php echo display('total_amount') ?></th>
										<th><?php echo display('action') ?></th>
									</tr>
								</thead>
								<tbody>
								<?php if ($invoices_list) { 
									$i=1;
									foreach ($invoices_list as $invoice) {
								?>
									<tr>
										<td><?php echo $invoice['sl']?></td>
										<td>
											<?php if($this->permission->check_label('manage_invoice')->read()->access()){?>
											<a href="<?php echo base_url().'cinvoice/invoice_inserted_data/'.$invoice['invoice_id']; ?>"><?php echo $invoice['invoice']?>
											</a>
											<?php 
											}else{ 
												echo $invoice['invoice'];
											} 
											?>
										</td>
                                        <td>
                                            <?php
                                            $orders = $this->db->select('order_id')->from('order')->where('order_no',
                                                $invoice['order_no'])->get()->result();
                                            if($orders){
                                            ?>

                                            <a href="<?php echo base_url().'corder/order_details_data/'
                                                .$orders[0]->order_id

                                            ?>"><?php echo $invoice['order_no'];}?></a>
                                        </td>
										<td>
											<?php if($this->permission->module('manage_customer')->read()->access()){?>
											<a href="<?php echo base_url().'ccustomer/customer_details/'.$invoice['customer_id']; ?>"><?php echo $invoice['customer_name']?></a>
											<?php 
											}else{
												echo $invoice['customer_name'];
											} 
											?>
										</td>
                                        <td> <?php echo $invoice['method_name']?> </td>
										<td><?php echo $invoice['final_date']?></td>
										<td style="text-align: right;"><?php echo (($position==0)?$currency.' '.$invoice['total_amount']:$invoice['total_amount'].' '.$currency) ?></td>
										<td>
											<center>
												<?php if($this->permission->check_label('manage_invoice')->read()->access()){?>
												<a href="<?php echo base_url().'cinvoice/pos_invoice_inserted_data/'.$invoice['invoice_id']; ?>" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="left" title="<?php echo display('pos_invoice') ?>"><i class="fa fa-window-restore" aria-hidden="true"></i></a>
												<a href="<?php echo base_url().'cinvoice/invoice_inserted_data/'.$invoice['invoice_id']; ?>" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="left" title="<?php echo display('invoice') ?>"><i class="fa fa-window-restore" aria-hidden="true"></i></a>
												<?php } ?>
											</center>
										</td>
									</tr>
								<?php $i++; } }?>
								</tbody>
		                    </table>
		                </div>
		                <div class="text-right">
		                	<?php echo $links?>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</section>
</div>
<!-- Manage Invoice End -->