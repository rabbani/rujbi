<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Sales Report Start -->
<div class="content-wrapper">
	<section class="content-header">
	    <div class="header-icon">
	        <i class="pe-7s-note2"></i>
	    </div>
	    <div class="header-title">
	        <h1><?php echo display('sales_overview_report') ?></h1>
	        <small><?php echo display('sales_overview_report') ?></small>
	        <ol class="breadcrumb">
	            <li><a href="index.html"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
	            <li><a href="#"><?php echo display('report') ?></a></li>
	            <li class="active"><?php echo display('sales_overview_report') ?></li>
	        </ol>
	    </div>
	</section>

	<section class="content">

		<div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
                <div class="panel panel-bd" style="height:100px">
                    <div class="panel-body">
                        <div class="statistic-box text-center">
                            <h2><span class="count-number"><?php echo $lifetime_orders?></span> <span class="slight"> </span></h2>
                            <div style="font-size:15px"><?php echo display('lifetime_orders')?></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
                <div class="panel panel-bd" style="height:100px">
                    <div class="panel-body">
                        <div class="statistic-box text-center">
                            <h2><?php echo (($position==0)?"$currency <span class=\"count-number\">".$lifetime_orders_sell."</span>":"<span class=\"count-number\">".$lifetime_orders_sell."</span> $currency") ?></h2>
                            <div style="font-size:15px"><?php echo display('order_total')?></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
                <div class="panel panel-bd" style="height:100px">
                    <div class="panel-body">
                        <div class="statistic-box text-center">
                            <h2><span class="count-number"><?php echo $order_delivered?></span> <span class="slight"> </span></h2>
                            <div style="font-size:15px"><?php echo display('order_delivered')?></div>
                        </div>
                    </div>
                </div>
            </div>
           
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
                <div class="panel panel-bd" style="height:100px">
                    <div class="panel-body">
                        <div class="statistic-box text-center">
                            <h2><?php echo (($position==0)?"$currency <span class=\"count-number\">".$order_delivered_sell."</span>":"<span class=\"count-number\">".$order_delivered_sell."</span> $currency") ?></h2>
                            <div style="font-size:15px"><?php echo display('total_sale')?></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
                <div class="panel panel-bd" style="height:100px">
                    <div class="panel-body">
                        <div class="statistic-box text-center">
                            <h2><span class="count-number"><?php echo $total_product?></span> <span class="slight"> </span></h2>
                            <div style="font-size:15px"><?php echo display('products')?></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
                <div class="panel panel-bd" style="height:100px">
                    <div class="panel-body">
                        <div class="statistic-box text-center">
                            <h2><span class="count-number"><?php echo $total_seller?></span> <span class="slight"> </span></h2>
                            <div style="font-size:15px"><?php echo display('merchants')?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

		<!-- Order Report -->
		<div class="row">
		    <div class="col-sm-12">
		        <div class="panel panel-bd lobidrag">
		            <div class="panel-heading">
		                <div class="panel-title">
		                    <h4><?php echo display('order_report') ?></h4>
		                    <small><?php echo display('sell_report_based_on_order') ?></small>
		                    <div class="pull-right" style="margin-top: -15px">
								<input type="text" name="sales_report" class="form-control datepicker-order_report" data-language='en' data-min-view="months" data-view="months" data-date-format="m-yyyy" placeholder="Select Month" value="<?php if(isset($_GET['re'])){if($_GET['re'] == 'order'){echo $_GET['date'];}}?>" />
		                    </div>
		                </div>
		            </div>
		            <div class="panel-body">
		               	<canvas id="order_report" height="110"></canvas>
		            </div>
		        </div>

		        <script type="text/javascript">
		        	//Date picker sales report
				    $('.datepicker-order_report').datepicker({
				        onSelect(formattedDate, date, inst){
							var fullURL = window.location.href.split('?')[0];
					       	window.location.href = fullURL+'?re=order&date='+formattedDate;
					    }
				    }); 
		        </script>

		        <?php
		        if ($shipping_method_list) {
		        ?>
		        <div class="row">
		        	<?php
		        	foreach ($shipping_method_list as $method_list) {
		        	?>
		            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
		                <div class="panel panel-bd" style="height:100px">
		                    <div class="panel-body">
		                        <div class="statistic-box text-center">
		                            <h2><span class="count-number"><?php echo $this->Reports->get_total_delivered_order($method_list['method_id']);?></span> <span class="slight"> </span></h2>
		                            <div style="font-size:15px"><?php echo $method_list['method_name']?></div>
		                        </div>
		                    </div>
		                </div>
		            </div>

		            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
		                <div class="panel panel-bd" style="height:100px">
		                    <div class="panel-body">
		                        <div class="statistic-box text-center">
		                            <h2>
		                            <?php
		                            $total = $this->Reports->get_total_delivered_money($method_list['method_id']);
		                            echo (($position==0)?"$currency <span class=\"count-number\">".$total->total_amount."</span>":"<span class=\"count-number\">".$total->total_amount."</span> $currency") ?>
		                            </h2>
		                            <div style="font-size:15px"><?php echo $method_list['method_name'].' '.display('sell')?></div>
		                        </div>
		                    </div>
		                </div>
		            </div>
		            <?php
		            }
		            ?>
		        </div>
		        <?php
		        	}
		        ?>
		    </div>
		</div>

		<!-- Sell Report -->
		<div class="row">
		    <div class="col-sm-12">
		        <div class="panel panel-bd lobidrag">
		            <div class="panel-heading">
		                <div class="panel-title">
		                    <h4><?php echo display('sell_report') ?></h4>
		                    <small><?php echo display('sell_report_based_on_shipping_product') ?></small>

		                    <div class="pull-right" style="margin-top: -15px">
								<input type="text" name="sales_report" class="form-control datepicker-sell-report" data-language='en' data-min-view="months" data-view="months" data-date-format="m-yyyy" placeholder="Select Month" value="<?php if(isset($_GET['re'])){if($_GET['re'] == 'sell'){echo $_GET['date'];}}?>" />
		                    </div>

		                </div>
		            </div>
		            <div class="panel-body">
                        <!-- Tab panels -->
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="tab1">
                                <div class="panel-body">
                                    <canvas id="sell_report" height="110"></canvas>
                                </div>
                            </div>
                        </div>
		            </div>
		        </div>

		        <script type="text/javascript">
		        	//Date picker sales report
				    $('.datepicker-sell-report').datepicker({
				        onSelect(formattedDate, date, inst){
							var fullURL = window.location.href.split('?')[0];
					       	window.location.href = fullURL+'?re=sell&date='+formattedDate;
					    }
				    }); 
		        </script>

		        <?php
		        if ($shipping_method_list) {
		        ?>
		        <div class="row">
		        	<?php
		        	foreach ($shipping_method_list as $method_list) {
		        	?>
		            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
		                <div class="panel panel-bd" style="height:100px">
		                    <div class="panel-body">
		                        <div class="statistic-box text-center">
		                            <h2><span class="count-number"><?php echo $this->Reports->get_total_shipping_order($method_list['method_id']);?></span> <span class="slight"> </span></h2>
		                            <div style="font-size:15px"><?php echo display('delivered').' '.$method_list['method_name']?></div>
		                        </div>
		                    </div>
		                </div>
		            </div>

		            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
		                <div class="panel panel-bd" style="height:100px">
		                    <div class="panel-body">
		                        <div class="statistic-box text-center">
		                            <h2><?php
		                            $total = $this->Reports->get_total_shipping_money($method_list['method_id']);
		                            echo (($position==0)?"$currency <span class=\"count-number\">".$total->total_amount."</span>":"<span class=\"count-number\">".$total->total_amount."</span> $currency") ?></h2>
		                            <div style="font-size:15px"><?php echo display('sell').' '.$method_list['method_name']?></div>
		                        </div>
		                    </div>
		                </div>
		            </div>
		            <?php
		            }
		            ?>
		        </div>
	         	<?php
	            }
	            ?>
		    </div>
		</div>		

		<!-- Net Sell Report -->
		<div class="row">
		    <div class="col-sm-12">
		        <div class="panel panel-bd lobidrag">
		            <div class="panel-heading">
		                <div class="panel-title">
		                    <h4><?php echo display('net_sell_report') ?></h4>
		                    <small><?php echo display('sell_report_based_on_delivered_product') ?></small>

		                   <div class="pull-right" style="margin-top: -15px">
								<input type="text" name="sales_report" class="form-control datepicker-net-sell-report" data-language='en' data-min-view="months" data-view="months" data-date-format="m-yyyy" placeholder="Select Month" value="<?php if(isset($_GET['re'])){if($_GET['re'] == 'net_sell'){echo $_GET['date'];}}?>" />
		                    </div>
		                </div>
		            </div>
		            <div class="panel-body">
		               	<canvas id="net_sell_report" height="110"></canvas>
		            </div>
		        </div>

		        <script type="text/javascript">
		        	//Date picker sales report
				    $('.datepicker-net-sell-report').datepicker({
				        onSelect(formattedDate, date, inst){
							var fullURL = window.location.href.split('?')[0];
					       	window.location.href = fullURL+'?re=net_sell&date='+formattedDate;
					    }
				    }); 
		        </script>

		        <div class="row">
		            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
		                <div class="panel panel-bd" style="height:100px">
		                    <div class="panel-body">
		                        <div class="statistic-box text-center">
		                            <h2><span class="count-number"><?php echo $lifetime_orders?></span> <span class="slight"> </span></h2>
		                            <div style="font-size:15px"><?php echo display('lifetime_orders')?></div>
		                        </div>
		                    </div>
		                </div>
		            </div>

		            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
		                <div class="panel panel-bd" style="height:100px">
		                    <div class="panel-body">
		                        <div class="statistic-box text-center">
		                            <h2><span class="count-number"><?php echo $order_delivered?></span> <span class="slight"> </span></h2>
		                            <div style="font-size:15px"><?php echo display('delivered')?></div>
		                        </div>
		                    </div>
		                </div>
		            </div>

		            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
		                <div class="panel panel-bd" style="height:100px">
		                    <div class="panel-body">
		                        <div class="statistic-box text-center">
		                            <h2><span class="count-number"><?php echo $cancelled_order?></span> <span class="slight"> </span></h2>
		                            <div style="font-size:15px"><?php echo display('cancelled')?></div>
		                        </div>
		                    </div>
		                </div>
		            </div>
		           
		            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
		                <div class="panel panel-bd" style="height:100px">
		                    <div class="panel-body">
		                        <div class="statistic-box text-center">
		                            <h2><span class="count-number"><?php echo $returned_order?></span> <span class="slight"> </span></h2>
		                            <div style="font-size:15px"><?php echo display('returned')?></div>
		                        </div>
		                    </div>
		                </div>
		            </div>

		            <!-- <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
		                <div class="panel panel-bd" style="height:100px">
		                    <div class="panel-body">
		                        <div class="statistic-box text-center">
		                            <h2><span class="count-number">10</span> <span class="slight"> </span></h2>
		                            <div style="font-size:15px">Partial Delivery</div>
		                        </div>
		                    </div>
		                </div>
		            </div> -->

		            <!-- <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
		                <div class="panel panel-bd" style="height:100px">
		                    <div class="panel-body">
		                        <div class="statistic-box text-center">
		                            <h2><span class="count-number">10</span> <span class="slight"> </span></h2>
		                            <div style="font-size:15px">Lost</div>
		                        </div>
		                    </div>
		                </div>
		            </div> -->
		        </div>
		    </div>
		</div>

	</section>
</div>
<!-- Sales Report End -->

<!-- ChartJs JavaScript -->
<script src="<?php echo base_url()?>assets/plugins/chartJs/Chart.min.js" type="text/javascript"></script>

<script type="text/javascript">
    // Order Report
    var ctx = document.getElementById("order_report");
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [<?php
                    for ($i=1; $i <= 31; $i++) {
                      echo '"'.$i.'",';
                    }
                ?>],
            datasets: [
                {
                    label: "<?php echo display('order_amount')?>",
                    data: [<?php
                            for ($i=1; $i <= 31; $i++) {
                               $delivered = $this->Reports->order_report($i);
                               if (!empty($delivered->total_amount)) {
                                    echo $delivered->total_amount.",";
                               }else{
                                echo ",";
                               }
                            }
                        ?>],
                    borderColor: "rgba(55, 160, 0, 0.9)",
                    borderWidth: "0",
                    backgroundColor: "rgba(55,160,0,0.9)"
                }
            ]
        },
        options: {
            scales: {
                yAxes: [{
                        ticks: {
                            callback: function(label, index, labels) {
		                        return label/1000+'k';
		                    }
                        },
                        scaleLabel: {
		                    display: true,
		                    labelString: '1k = 1000'
		                }
                    }]
            }
        }
    });

    //Sales Report
    var ctx = document.getElementById("sell_report");
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [<?php
                    for ($i=1; $i <= 31; $i++) {
                      echo '"'.$i.'",';
                    }
                ?>],
            datasets: [
                {
                    label: "<?php echo display('order_amount')?>",
                    data: [<?php
                            for ($i=1; $i <= 31; $i++) {
                               $delivered = $this->Reports->sell_report($i);
                               if (!empty($delivered->total_amount)) {
                                    echo $delivered->total_amount.",";
                               }else{
                                echo ",";
                               }
                            }
                        ?>],
                    borderColor: "rgba(55, 160, 0, 0.9)",
                    borderWidth: "0",
                    backgroundColor: "rgba(55,160,0,0.9)"
                }
            ]
        },
        options: {
            scales: {
                yAxes: [{
                        ticks: {
                            callback: function(label, index, labels) {
		                        return label/1000+'k';
		                    }
                        },
                        scaleLabel: {
		                    display: true,
		                    labelString: '1k = 1000'
		                }
                    }]
            }
        }
    });  

    //Net Sales Report
    var ctx = document.getElementById("net_sell_report");
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [<?php
                    for ($i=1; $i <= 31; $i++) {
                      echo '"'.$i.'",';
                    }
                ?>],
            datasets: [
                {
                    label: "<?php echo display('order_amount')?>",
                    data: [<?php
                            for ($i=1; $i <= 31; $i++) {
                               $delivered = $this->Reports->net_sell_report($i);
                               if (!empty($delivered->total_amount)) {
                                    echo $delivered->total_amount.",";
                               }else{
                                echo ",";
                               }
                            }
                        ?>],
                    borderColor: "rgba(55, 160, 0, 0.9)",
                    borderWidth: "0",
                    backgroundColor: "rgba(55,160,0,0.9)"
                }
            ]
        },
        options: {
            scales: {
                yAxes: [{
                        ticks: {
                            callback: function(label, index, labels) {
		                        return label/1000+'k';
		                    }
                        },
                        scaleLabel: {
		                    display: true,
		                    labelString: '1k = 1000'
		                }
                    }]
            }
        }
    }); 
</script>

