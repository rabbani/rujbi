<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Sales Details Report Start -->
<div class="content-wrapper">
	<section class="content-header">
	    <div class="header-icon">
	        <i class="pe-7s-note2"></i>
	    </div>
	    <div class="header-title">
	        <h1><?php echo display('sales_details_report') ?></h1>
	        <small><?php echo display('sell_report_based_on_shipping_product') ?></small>
	        <ol class="breadcrumb">
	            <li><a href="index.html"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
	            <li><a href="#"><?php echo display('report') ?></a></li>
	            <li class="active"><?php echo display('sales_details_report') ?></li>
	        </ol>
	    </div>
	</section>

	<section class="content">

		 <!-- Invoice filtering -->
        <div class="row">
			<div class="col-sm-12">
		        <div class="panel panel-default">
		            <div class="panel-body"> 

		            	<form action="<?php echo base_url('creport/sales_details_report/all/item')?>" method="get">
		     				<div class="row">
	                            <div class="col-sm-3">
	                                <div class="form-group row">
	                                    <label for="order_no" class="col-sm-4 col-form-label"><?php echo display('order_no')?></label>
	                                    <div class="col-sm-8">
	                                      	<input class="form-control" name ="order_no" id="order_no" type="text" placeholder="<?php echo display('order_no') ?>" value="<?php if(isset($_GET['order_no'])){ echo $_GET['order_no'];}?>">
	                                    </div>
	                                </div>
	                            </div>
	                            
	                            <div class="col-sm-3">
	                                <div class="form-group row">
	                                    <label for="customer" class="col-sm-4 col-form-label"><?php echo display('customer')?> </label>
	                                    <div class="col-sm-8">
	                                      	<input class="form-control" name ="customer" id="customer" type="text" placeholder="<?php echo display('customer') ?>" value="<?php if(isset($_GET['customer'])){ echo $_GET['customer'];}?>">
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-3">
	                                <div class="form-group row">
	                                    <label for="invoice_no" class="col-sm-4 col-form-label"><?php echo display('invoice_no')?></label>
	                                    <div class="col-sm-8">
	                                      	<input class="form-control" name ="invoice_no" id="invoice_no" type="text" placeholder="<?php echo display('invoice_no') ?>/01-10" value="<?php if(isset($_GET['invoice_no'])){ echo $_GET['invoice_no'];}?>">
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-3">
	                                <div class="form-group row">
	                                    <label for="delivery" class="col-sm-4 col-form-label"><?php echo display('delivery')?> </label>
	                                    <div class="col-sm-8">
	                                      	<select class="form-control" id="delivery" name="shipping_id">
		                                       	<option value=""></option>
		                                       	<?php
		                                       	if ($shipping_method_list) {
		                                       		foreach ($shipping_method_list as $shipp) {
		                                       	?>
		                                       	<option value="<?php echo $shipp['method_id']?>" <?php if(isset($_GET['shipping_id'])){ if ($_GET['shipping_id'] == $shipp['method_id']) {echo "selected";}}?>><?php echo $shipp['method_name']?></option>
		                                       	<?php
		                                       		}
		                                       	}
		                                       	?>
		                                    </select>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="row">

	                        	<div class="col-sm-4">
	                                <div class="form-group row">
	                                    <label for="date" class="col-sm-3 col-form-label"><?php echo display('date')?></label>
	                                    <div class="col-sm-8">
	                                      	<input type="text" class="form-control datepicker-manage" id="date" data-range="true"  data-multiple-dates-separator="---" data-language='en' name="date" placeholder="<?php echo display('date') ?>" value="<?php if(isset($_GET['date'])){ echo $_GET['date'];}?>">
	                                    </div>
	                                </div>
	                            </div>   

	                            <div class="col-sm-4">
	                                <div class="form-group row">
	                                    <label for="status" class="col-sm-3 col-form-label"><?php echo display('status')?></label>
	                                    <div class="col-sm-8">
	                                      	<select class="form-control" id="status" name="status">
		                                       	<option value=""></option>
		                                       	<option value="1" <?php if (isset($_GET['status'])){if ($_GET['status'] == 1) {echo "selected";}}?>><?php echo display('pending')?></option>
		                                        <option value="2" <?php if (isset($_GET['status'])){if ($_GET['status'] == 2) {echo "selected";}}?>><?php echo display('processing')?></option>
		                                        <option value="3" <?php if (isset($_GET['status'])){if ($_GET['status'] == 3) {echo "selected";}}?>><?php echo display('shipping')?></option>
		                                        <option value="4" <?php if (isset($_GET['status'])){if ($_GET['status'] == 4) {echo "selected";}}?>><?php echo display('delivered')?></option>
		                                        <option value="5" <?php if (isset($_GET['status'])){if ($_GET['status'] == 5) {echo "selected";}}?>><?php echo display('returned')?></option>
		                                        <option value="6" <?php if (isset($_GET['status'])){if ($_GET['status'] == 6) {echo "selected";}}?>><?php echo display('cancel')?></option>
		                                        <option value="7" <?php if (isset($_GET['status'])){if ($_GET['status'] == 7) {echo "selected";}}?>><?php echo display('partial_delivery')?></option>
		                                    </select>
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-2">
	                                <div class="form-group row">
	                                    <div class="col-sm-7">
	                                      	<button type="submit" class="btn btn-primary"><?php echo display('search')?></button>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
		            	</form>   
			        </div>
		        </div>
		    </div>
		</div>

		<!-- Manage order report -->
		<div class="row">
		    <div class="col-sm-12">
		        <div class="panel panel-bd lobidrag">
		            <div class="panel-heading">
		                <div class="panel-title">
		                    <h4><?php echo display('sales_details_report') ?></h4>
		                    <small><?php echo display('sell_report_based_on_shipping_product') ?></small>
		                </div>
		            </div>
		            <div class="panel-body">
		                <div class="table-responsive">
		                    <table id="dataTableExample3" class="table table-bordered table-striped table-hover">
		                    	<thead>
									<tr>
										<th><?php echo display('sl') ?></th>
										<th><?php echo display('date') ?></th>
										<th><?php echo display('order_no') ?></th>
										<th><?php echo display('invoice_no') ?></th>
										<th><?php echo display('name') ?></th>
										<th><?php echo display('address') ?></th>
										<th><?php echo display('phone') ?></th>
										<th><?php echo display('delivery') ?></th>
										<th><?php echo display('status') ?></th>
										<th><?php echo display('product_price') ?></th>
										<th><?php echo display('delivery_charge') ?></th>
										<th><?php echo display('total_price') ?></th>
										<th><?php echo display('paid') ?></th>
										<th><?php echo display('due') ?></th>
									</tr>
								</thead>
								<tbody>
								<?php
								if ($sell_report_shipping) {
									$i=1;
									foreach ($sell_report_shipping as $invoice) {
								?>
									<tr>
										<td><?php echo $this->uri->segment('5')+$i?></td>
										<td><?php echo $invoice->date?></td>
										<td><?php echo $invoice->order_no?></td>
										<td><?php echo $invoice->invoice?></td>
										<td><?php echo $invoice->customer_name?></td>
										<td><?php echo $invoice->customer_short_address?></td>
										<td><?php echo $invoice->customer_mobile?></td>
										<td><?php echo $invoice->method_name?></td>
										<td>
										<?php 
										$invoice_status = $invoice->invoice_status;
										if ($invoice_status == 1) {
											echo "<span class=\"label label-pill label-black m-r-15\">".display('pending')."</span>";
										}elseif ($invoice_status == 2) {
											echo "<span class=\"label label-pill label-purple m-r-15\">".display('processing')."</span>";
										}elseif ($invoice_status == 3) {
											echo "<span class=\"label label-pill label-info m-r-15\">".display('shipping')."</span>";
										}elseif ($invoice_status == 4) {
											echo "<span class=\"label label-pill label-success m-r-15\">".display('delivered')."</span>";
										}elseif ($invoice_status == 5) {
											echo "<span class=\"label label-pill label-warning m-r-15\">".display('returned')."</span>";
										}elseif ($invoice_status == 6) {
											echo "<span class=\"label label-pill label-danger m-r-15\">".display('cancel')."</span>";
										}elseif ($invoice_status == 7) {
											echo "<span class=\"label label-pill label-pink m-r-15\">".display('partial_delivery')."</span>";
										}
										?>
										</td>
										<td style="text-align: right;"><?php echo (($position==0)?$currency.' '.$invoice->total_amount - $invoice->service_charge:$invoice->total_amount - $invoice->service_charge.' '.$currency) ?>
										</td>
										<td style="text-align: right;"><?php echo (($position==0)?$currency.' '.$invoice->service_charge:$invoice->service_charge.' '.$currency) ?>
										</td>
										<td style="text-align: right;"><?php echo (($position==0)?$currency.' '.$invoice->total_amount:$invoice->total_amount.' '.$currency) ?>
										</td>
										<td style="text-align: right;"><?php echo (($position==0)?$currency.' '.$invoice->paid_amount:$invoice->paid_amount.' '.$currency) ?>
										</td>
										<td style="text-align: right;">
										<?php 
										$due = $invoice->total_amount-$invoice->paid_amount;
										echo (($position==0)?$currency.' '.$due:$due.' '.$currency);
										?>
										</td>
									</tr>
								<?php
									$i++;
									}
								}
								?>
								</tbody>
								<tfoot>
									<td colspan="9" class="text-right"><b><?php echo display('total_amount')?>:</b></td>
									<td class="text-right"><b><?php echo (($position==0)?"$currency {SubTotalProductPrice}":"{SubTotalProductPrice} $currency") ?></b></td>
									<td class="text-right"><b><?php echo (($position==0)?"$currency {SubTotalDelCharge}":"{SubTotalDelCharge} $currency") ?></b></td>
									<td class="text-right"><b><?php echo (($position==0)?"$currency {SubTotalAmnt}":"{SubTotalAmnt} $currency") ?></b></td>
									<td class="text-right"><b><?php echo (($position==0)?"$currency {SubTotalPaid}":"{SubTotalPaid} $currency") ?></b></td>
									<td class="text-right"><b><?php echo (($position==0)?"$currency {SubTotalDue}":"{SubTotalDue} $currency") ?></b></td>
								</tfoot>
		                    </table>
		                </div>
		                <div class="text-right">
		                	<?php echo $links?>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>

	</section>
</div>
<!-- Sales Report End -->