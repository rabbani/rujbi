<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Stock report product wise-->
<div class="content-wrapper">
	<section class="content-header">
	    <div class="header-icon">
	        <i class="pe-7s-note2"></i>
	    </div>
	    <div class="header-title">
	        <h1><?php echo display('stock_management') ?></h1>
	        <small><?php echo display('stock_management') ?></small>
	        <ol class="breadcrumb">
	            <li><a href="index.html"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
	            <li><a href="#"><?php echo display('report') ?></a></li>
	            <li class="active"><?php echo display('stock_management') ?></li>
	        </ol>
	    </div>
	</section>

	<section class="content">

		<!-- Stock filtering -->
        <div class="row">
			<div class="col-sm-12">
		        <div class="panel panel-default">
		            <div class="panel-body"> 

		            	<form action="<?php echo base_url('stock')?>" method="get">
		     				<div class="row">
	        
	                            <div class="col-sm-4">
	                                <div class="form-group row">
	                                    <label for="product_id" class="col-sm-5 col-form-label"><?php echo display('product_name')?> </label>
	                                    <div class="col-sm-7">
	                                      	<input type="text" name="product_name" class="form-control" placeholder="<?php echo display('product_name')?>" value="<?php if (isset($_GET['product_name'])) {echo $_GET['product_name'];}?>">
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-4">
	                                <div class="form-group row">
	                                    <label for="product_model" class="col-sm-5 col-form-label"><?php echo display('product_model')?> </label>
	                                    <div class="col-sm-7">
	                                      	<input type="text" name="product_model" class="form-control" placeholder="<?php echo display('product_model')?>" value="<?php if (isset($_GET['product_model'])) {echo $_GET['product_model'];}?>">
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-4">
	                                <div class="form-group row">
	                                    <label for="seller" class="col-sm-5 col-form-label"><?php echo display('seller')?> </label>
	                                    <div class="col-sm-7">
	                                      	<select class="form-control" name="seller" id="seller">
	                                      		<option></option>
	                                      		<?php
	                                      		if ($seller_list) {
	                                      			foreach ($seller_list as $seller) {
	                                      		?>
	                                      		<option value="<?php echo $seller['seller_id']?>" <?php if (isset($_GET['seller'])) {if ($_GET['seller'] == $seller['seller_id']) {echo "selected";}}?>><?php echo $seller['first_name'].' '.$seller['last_name']?></option>
	                                      		<?php
	                                      			}
	                                      		}
	                                      		?>
	                                      	</select>
	                                    </div>
	                                </div>
	                            </div>
	                   
	                            <div class="col-sm-1">
	                                <div class="form-group row">
	                                    <div class="col-sm-7">
	                                      	<button type="submit" class="btn btn-primary"><?php echo display('search')?></button>
	                                    </div>
	                                </div>
	                            </div>

	                        </div>  
		            	</form>   
			        </div>
		        </div>
		    </div>
		</div>

		<!-- Stock report -->
		<div class="row">
		    <div class="col-sm-12">
		        <div class="panel panel-bd lobidrag">
		            <div class="panel-heading">
		                <div class="panel-title">
		                    <h4><?php echo display('stock_management') ?></h4>
		                </div>
		            </div>
		            <div class="panel-body">
		                <div class="table-responsive">
		                    <table id="dataTableExample3" class="table table-bordered table-striped table-hover">
		                    	<thead>
									<tr>
										<th><?php echo display('sl') ?></th>
										<th><?php echo display('product_name') ?></th>
										<th><?php echo display('seller') ?></th>
										<th><?php echo display('lifetime_restocked') ?></th>
										<th><?php echo display('wastage') ?></th>
										<th><?php echo display('last_stock') ?></th>
										<th><?php echo display('sold_out') ?></th>
										<th><?php echo display('current_stock') ?></th>
										
									</tr>
								</thead>
								<tbody>
								<?php
								if ($manage_product) {
									$i=1;
									foreach ($manage_product as $product) {
								?>
									<tr>
										<td><?php echo $this->uri->segment('3')+$i?></td>
										<td><b><?php echo $product['title'].' ('.$product['product_model'].')'?></b></td>
										<td><?php echo $product['first_name'].' '.$product['last_name']?></td>
										<td><?php echo $lifetime_restocked = $this->Reports->lifetime_restocked($product['product_id']);?></td>
										<td><?php echo abs($this->Reports->lifetime_wastage($product['product_id']));?></td>
										<td><?php echo $last_stock = $this->Reports->last_stock($product['product_id']);?></td>
										<td><?php echo $total_sold_out = $this->Reports->total_sold_out($product['product_id']); ?></td>
										<td><?php echo $last_stock - $total_sold_out?></td>
									</tr>
								<?php
									$i++;
									}
								}
								?>
								</tbody>
		                    </table>
		                </div>
		                <div class="text-right"><?php echo $links?></div>
		            </div>
		        </div>
		    </div>
		</div>

	</section>
</div>
<!--Stock report product wise-->