<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Merchant Sell Report Start -->
<div class="content-wrapper">
	<section class="content-header">
	    <div class="header-icon">
	        <i class="pe-7s-note2"></i>
	    </div>
	    <div class="header-title">
	        <h1><?php echo display('merchant_sell_report_in_details') ?></h1>
	        <small><?php echo display('sell_report_based_on_delivered_product') ?></small>
	        <ol class="breadcrumb">
	            <li><a href="index.html"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
	            <li><a href="#"><?php echo display('report') ?></a></li>
	            <li class="active"><?php echo display('merchant_sell_report_in_details') ?></li>
	        </ol>
	    </div>
	</section>

	<section class="content">

		<!-- Invoice filtering -->
        <div class="row">
			<div class="col-sm-12">
		        <div class="panel panel-default">
		            <div class="panel-body"> 

		            	<form action="<?php echo base_url('creport/sales_details_report/all/item')?>" method="get">
		     				<div class="row">

	                            <div class="col-sm-4">
	                                <div class="form-group row">
	                                    <label for="order_no" class="col-sm-5 col-form-label"><?php echo display('order_no')?></label>
	                                    <div class="col-sm-7">
	                                      	<input class="form-control" name ="order_no" id="order_no" type="text" placeholder="<?php echo display('order_no') ?>" value="<?php if(isset($_GET['order_no'])){ echo $_GET['order_no'];}?>">
	                                    </div>
	                                </div>
	                            </div>
	                            
	                            <div class="col-sm-3">
	                                <div class="form-group row">
	                                    <label for="seller" class="col-sm-4 col-form-label"><?php echo display('seller')?> </label>
	                                    <div class="col-sm-8">
	                                      	<select class="form-control" name="seller" id="seller">
	                                      		<option></option>
	                                      		<?php
	                                      		if ($seller_list) {
	                                      			foreach ($seller_list as $seller) {
	                                      		?>
	                                      		<option value="<?php echo $seller['seller_id']?>" <?php if (isset($_GET['seller'])) {if ($_GET['seller'] == $seller['seller_id']) {echo "selected";}}?>><?php echo $seller['mobile']?></option>
	                                      		<?php
	                                      			}
	                                      		}
	                                      		?>
	                                      	</select>
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-3">
	                                <div class="form-group row">
	                                    <label for="date" class="col-sm-2 col-form-label"><?php echo display('date')?></label>
	                                    <div class="col-sm-10">
	                                      	<input type="text" class="form-control datepicker-manage" id="date" data-range="true"  data-multiple-dates-separator="---" data-language='en' name="date" placeholder="<?php echo display('date') ?>" value="<?php if(isset($_GET['date'])){ echo $_GET['date'];}?>">
	                                    </div>
	                                </div>
	                            </div>
	                   
	                            <div class="col-sm-1">
	                                <div class="form-group row">
	                                    <div class="col-sm-7">
	                                      	<button type="submit" class="btn btn-primary"><?php echo display('search')?></button>
	                                    </div>
	                                </div>
	                            </div>

	                        </div>  
		            	</form>   
			        </div>
		        </div>
		    </div>
		</div>

		<!-- Merchant sell report -->
		<div class="row">
		    <div class="col-sm-12">
		        <div class="panel panel-bd lobidrag">
		            <div class="panel-heading">
		                <div class="panel-title">
		                    <h4><?php echo display('merchant_sell_report_in_details') ?></h4>
		                    <small><?php echo display('sell_report_based_on_delivered_product') ?></small>
		                </div>
		            </div>
		            <div class="panel-body">
		                <div class="table-responsive">
		                    <table id="dataTableExample3" class="table table-bordered table-striped table-hover">
		                    	<thead>
									<tr>
										<th><?php echo display('sl') ?></th>
										<th><?php echo display('date') ?></th>
										<th><?php echo display('order_no') ?></th>
										<th><?php echo display('merchant_name') ?></th>
										<th><?php echo display('phone') ?></th>
										<th><?php echo display('status') ?></th>
										<th><?php echo display('price') ?></th>
										<th><?php echo display('comission') ?></th>
										<th><?php echo display('payable_amount') ?></th>
									</tr>
								</thead>
								<tbody>
								<?php
								if ($merchant_sell_report) {
									$i=1;
									foreach ($merchant_sell_report as $order) {
								?>
									<tr>
										<td><?php echo $this->uri->segment('5')+$i?></td>
										<td><?php echo $order['date']?></td>
										<td><a href="<?php echo base_url().'corder/order_details_data/'.$order['order_id']?>"><?php echo $order['order_no']?></a>
										</td>
										<td><?php echo $order['first_name'].' '.$order['last_name'] ?></td>
										<td><?php echo $order['mobile']?></td>
										<td>
											<?php 
										$order_status = $order['order_status'];
										if ($order_status == 1) {
											echo "<span class=\"label label-pill label-warning m-r-15\">".display('pending')."</span>";
										}elseif ($order_status == 2) {
											echo "<span class=\"label label-pill label-info m-r-15\">".display('processing')."</span>";
										}elseif ($order_status == 3) {
											echo "<span class=\"label label-pill label-success m-r-15\">".display('shipping')."</span>";
										}elseif ($order_status == 4) {
											echo "<span class=\"label label-pill label-success m-r-15\">".display('delivered')."</span>";
										}elseif ($order_status == 5) {
											echo "<span class=\"label label-pill label-danger m-r-15\">".display('returned')."</span>";
										}elseif ($order_status == 6) {
											echo "<span class=\"label label-pill label-danger m-r-15\">".display('cancel')."</span>";
										}
										?>
										</td>
										<td style="text-align: right;"><?php echo (($position==0)?$currency.' '.$order['total_price']:$order['total_price'].' '.$currency) ?></td>
										<td style="text-align: right;"><?php echo (($position==0)?$currency.' '.$order['total_comission']:$order['total_comission'].' '.$currency) ?></td>
										<td style="text-align: right;"><?php echo (($position==0)?$currency.' '.$order['payable_amount']:$order['payable_amount'].' '.$currency) ?>
										</td>
									</tr>
								<?php
									$i++;
									}
								}
								?>
								</tbody>
								<tfoot>
									<td colspan="6" class="text-right"><b><?php echo display('total_amount')?>:</b></td>
									<td class="text-right"><b><?php echo (($position==0)?"$currency {sub_total_price}":"{sub_total_price} $currency") ?></b></td>
									<td class="text-right"><b><?php echo (($position==0)?"$currency {sub_total_comission}":"{sub_total_comission} $currency") ?></b></td>
									<td class="text-right"><b><?php echo (($position==0)?"$currency {sub_total_payable}":"{sub_total_payable} $currency") ?></b></td>
								</tfoot>
		                    </table>
		                </div>
		                <div class="text-right">
		                	<?php echo $links?>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>

	</section>
</div>
<!--Merchant Sales Report End -->
