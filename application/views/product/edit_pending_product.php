<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<!-- Edit pending product start -->
<div class="content-wrapper">
    <section class="content-header">
        <div class="header-icon">
            <i class="pe-7s-note2"></i>
        </div>
        <div class="header-title">
            <h1><?php echo display('update_pending_product') ?></h1>
            <small><?php echo display('update_pending_product') ?></small>
            <ol class="breadcrumb">
                <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
                <li><a href="#"><?php echo display('product') ?></a></li>
                <li class="active"><?php echo display('update_pending_product') ?></li>
            </ol>
        </div>
    </section>

    <section class="content">
        <!-- Alert Message -->
        <?php
            $message = $this->session->userdata('message');
            if (isset($message)) {
        ?>
        <div class="alert alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $message ?>                    
        </div>
        <?php 
            $this->session->unset_userdata('message');
            }
            $error_message = $this->session->userdata('error_message');
            if (isset($error_message)) {
        ?>
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $error_message ?>                    
        </div>
        <?php 
            $this->session->unset_userdata('error_message');
            }
        ?>

        <div class="row">
            <div class="col-sm-12">
                <div class="column">
                    <a href="<?php echo base_url('cproduct/manage_product/all/item')?>" class="btn btn-success m-b-5
                    m-r-2"><i class="ti-align-justify"> </i> <?php echo display('manage_product')?></a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="panel-group" id="accordion">
                    <!-- Edit product -->
                    <div class="panel panel-bd">
                        <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><?php echo display('step_1')?>: <?php echo display('upload_product')?> <i class="fa fa-caret-down"></i></a></h4>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label for="seller" class="col-sm-3 col-form-label"><?php echo display('seller')?><i class="text-danger">*</i></label>
                                            <div class="col-sm-9">
                                                <select class="form-control select2" id="seller" name="seller_id"  required="">
                                                    <option value=""></option>
                                                    <?php 
                                                    if ($sellers_list) {
                                                        foreach ($sellers_list as $seller) {
                                                    ?>
                                                    <option value="<?php echo $seller['seller_id']?>" <?php if ($seller_id == $seller['seller_id']) {echo "selected";}?>><?php echo $seller['first_name']." ".$seller['last_name']." (".$seller['seller_id'].")" ?></option>
                                                    <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                       <div class="form-group row">
                                            <label for="category_id" class="col-sm-3 col-form-label"><?php echo display('category')?><i class="text-danger">*</i></label>
                                            <div class="col-sm-9">
                                                <select class="form-control select2" id="category_id" name="category_id" required="">
                                                    <option value=""></option>
                                                    <?php 
                                                    if ($category_list) {
                                                        foreach ($category_list as $category) {
                                                    ?>
                                                    <option value="<?php echo $category['category_id']?>" <?php if ($category_id == $category['category_id']) {echo "selected"; }?>><?php echo $category['category_name'] ?></option>
                                                    <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row"> 
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label for="price" class="col-sm-3 col-form-label"><?php echo display('price')?><i class="text-danger">*</i></label>
                                            <div class="col-sm-9">
                                               <input class="form-control" name ="price" id="price" type="number" placeholder="<?php echo display('price') ?>" value="<?php echo $price?>"  required="">
                                               <input name ="product_id" id="product_id" type="hidden" value="<?php echo $product_id?>"  required="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label for="quantity" class="col-sm-3 col-form-label"><?php echo display('quantity')?><i class="text-danger">*</i></label>
                                            <div class="col-sm-9">
                                               <input class="form-control" name ="quantity" id="quantity" type="number" placeholder="<?php echo display('quantity') ?>" value="<?php echo $quantity?>"  required="">
                                            </div>
                                        </div>
                                    </div>
                                </div>                                        

                                <div class="row"> 
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label for="product_model" class="col-sm-3 col-form-label"><?php echo display('product_model')?><i class="text-danger">*</i></label>
                                            <div class="col-sm-9">
                                              <input class="form-control" name ="product_model" id="product_model" type="text" placeholder="<?php echo display('product_model') ?>" value="<?php echo $product_model?>" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label for="variant" class="col-sm-3 col-form-label"><?php echo display('variant')?> </label>
                                            <div class="col-sm-9">
                                                <select name="variant[]" class="form-control" multiple style="width: 100%" id="variant">
                                                    <option value=""></option>
                                                    <?php if ($variant_list){ 
                                                        foreach ($variant_list as $variant_lis) {
                                                    ?>
                                                    <option value="<?php echo $variant_lis['variant_id']?>"><?php echo $variant_lis['variant_name']?></option>
                                                    <?php } }?>
                                                    <?php
                                                    if ($variant) {
                                                        $exploded = explode(',', $variant);
                                                        foreach ($exploded as $elem) {
                                                        $this->db->select('*');
                                                        $this->db->from('variant');
                                                        $this->db->where('variant_id',$elem);
                                                        $this->db->order_by('variant_name','asc');
                                                        $result = $this->db->get()->row();
                                                    ?>
                                                    <option value="<?php echo $result->variant_id?>" selected="" ><?php echo $result->variant_name?></option>
                                                    <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row"> 
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label for="unit" class="col-sm-3 col-form-label"><?php echo display('unit')?></label>
                                            <div class="col-sm-9">
                                               <select class="form-control select2" id="unit" name="unit">
                                                    <option value=""></option>
                                                    <?php 
                                                    if ($unit_list) {
                                                        foreach ($unit_list as $unit) {
                                                    ?>
                                                    <option value="<?php echo $unit['unit_id']?>" <?php if ($unit_id == $unit['unit_id']) {echo "selected"; }?>><?php echo $unit['unit_short_name'] ?></option>
                                                    <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label for="brand" class="col-sm-3 col-form-label"><?php echo display('brand')?></label>
                                            <div class="col-sm-9">
                                                <select class="form-control select2" id="brand" name="product_brand">
                                                    <option value=""></option>
                                                    <?php 
                                                    if ($brand_list) {
                                                        foreach ($brand_list as $brand) {
                                                    ?>
                                                    <option value="<?php echo $brand['brand_id']?>" <?php if ($brand_id == $brand['brand_id']) {echo "selected";}?>><?php echo $brand['brand_name'] ?></option>
                                                    <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>         

                                <div class="row"> 
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label for="product_type" class="col-sm-3 col-form-label"><?php echo display('product_type')?></label>
                                            <div class="col-sm-9">
                                                <input class="form-control" name ="product_type" id="product_type" type="text" placeholder="<?php echo display('product_type') ?>" value="<?php echo $product_type?>">

                                                <input type="hidden" id="status" name="status" value="<?php echo $status?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label for="tag" class="col-sm-3 col-form-label"><?php echo display('tag')?></label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control tag_value" data-role="tagsinput" name="tag" placeholder="<?php echo display('tag')?>" value="<?php echo $tag?>">

                                                <input type="hidden" name="upload_id" value="<?php echo $upload_id?>" id="upload_id">
                                            </div>
                                        </div>
                                    </div>
                                </div> 


                                <div class="row"> 
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label for="best_sale" class="col-sm-3 col-form-label"><?php echo display('best_sale') ?></label>
                                            <div class="col-md-9">
                                                <select class="form-control" id="best_sale" name="best_sale" style="width: 100%">
                                                    <option value=""><?php echo display('select_one') ?></option>
                                                    <option value="1" <?php if ($best_sale == 1) {echo "selected";}?>><?php echo display('yes') ?></option>
                                                    <option value="0" <?php if ($best_sale == 0) {echo "selected";}?>><?php echo display('no') ?></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div> 

                                    <div class="comission">

                                        <div class="col-sm-6">
                                            <div class="form-group row">
                                                <label for="comission" class="col-sm-3 col-form-label"><?php echo display('comission') ?></label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="comission" value="<?php echo $comission?>" id="comission">
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label for="pre_order" class="col-sm-3 col-form-label"><?php echo display('pre_order') ?> </label>
                                            <div class="col-md-9">
                                                <select class="form-control" id="pre_order" name="pre_order" required="">
                                                    <option value=""></option>
                                                    <option value="1" <?php if ($pre_order == 1) {echo "selected";}?>><?php echo display('yes') ?></option>
                                                    <option value="0" <?php if ($pre_order == 0) {echo "selected";}?>><?php echo display('no') ?></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group row pre_order_quantity" style="<?php if ($pre_order == 0) { echo "display: none"; }?>">
                                            <label for="pre_order_quantity" class="col-sm-3 col-form-label"><?php echo display('pre_order_quantity')?> <i class="text-danger">*</i></label>
                                            <div class="col-md-9">
                                                <input class="form-control" name="pre_order_quantity" type="number" required="" placeholder="<?php echo display('pre_order_quantity') ?>" min="0" id="pre_order_quantity" value="<?php echo $pre_order_quantity?>">
                                            </div>
                                        </div> 
                                    </div>
                                </div>                              

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label for="on_promotion" class="col-sm-3 col-form-label"><?php echo display('on_promotion') ?></label>
                                            <div class="col-md-9">
                                                <select class="form-control select2" id="on_promotion" name="on_promotion" style="width: 100%">
                                                    <option value=""><?php echo display('select_one') ?></option>
                                                    <option value="1" <?php if ($on_promotion == 1) {echo "selected";}?>><?php echo display('yes') ?></option>
                                                    <option value="0" <?php if ($on_promotion == 0) {echo "selected";}?>><?php echo display('no') ?></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group row on_promotion" style="<?php if ($on_promotion == 0) { echo "display: none"; }?>">
                                            <label for="details" class="col-sm-3 col-form-label"><?php echo display('details')?> <i class="text-danger">*</i></label>
                                            <div class="col-md-9">
                                                <textarea class="form-control" name="details" placeholder="<?php echo display('details') ?>" id="details"><?php echo $details?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>  

                                <div class="row text-right">
                                    <div class="form-group">
                                        <label for="" class="col-sm-5 col-form-label"></label>
                                        <div class="col-sm-12">
                                            <button type="button" id="update-seller" class="btn btn-info btn-large" name="add-seller"><?php echo display('next') ?> <i class="fa fa-caret-down"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Product Image -->
                    <div class="panel panel-bd">
                        <div class="panel-heading">
                            <h4 class="panel-title"><a href="#collapse2" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo display('step_2')?>: <?php echo display('product_image')?> <i class="fa fa-caret-down"></i></a></h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse">
                            <div class="panel-body">
                                <!-- File upload -->
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div id="image_list">
                                                <?php
                                                $i=0;
                                                if ($upload_image) {
                                                    foreach ($upload_image as $image) {
                                                ?>
                                                   <div class="das" style="padding: 10px; float: left;">
                                                        <?php if ($image['status'] == 1) { ?>
                                                        <input type="checkbox" id="primary_image<?php echo $i?>" name="primary_image" value="1" <?php if ($image['status'] == 1) { echo "checked";}?> disabled >
                                                        <label for="primary_image<?php echo $i?>"><?php echo display('primary')?></label>
                                                        <?php }else{ ?>
                                                        <input type="checkbox" id="primary_ima<?php echo $i?>" name="primary_image" value="1" class="make_primary">
                                                        <?php } ?>

                                                        <input type="hidden" name="img_id" class="img_id" value="<?php echo $image['upload_image_id']?>">
                                                        <input type="hidden" name="pro_id" class="pro_id" value="<?php echo $image['upload_id']?>">

                                                        <img src="<?php echo base_url().$image['image_url']?>" width="80" height="80" >

                                                        <input type="hidden" value="<?php echo $image['image_url']?>" class="image_path">

                                                        <a href="javascript:void(0);" class="btn btn-danger btn-sm delete_image" name="<?php echo $image['image_name']?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                    </div>
                                                <?php
                                                    }
                                                }
                                                ?>
                                                </div>    
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <form action="<?php echo base_url('cproduct/update_upload_image')?>" id="imageUploadForm" enctype="multipart/form-data">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label for="imageUpload" class="col-sm-3 col-form-label"><?php echo display('image') ?> <i class="text-danger">*</i></label>
                                                                <div class="col-sm-9">
                                                                    <input class="form-control" name="file" type="file" id="imageUpload" data-toggle="tooltip" data-placement="top" title="" aria-required="true" data-original-title="<?php echo display('image_size_width_3000_height_3000') ?>"/>

                                                                   <input name="upload_id" type="hidden" value="<?php echo $this->uri->segment('3')?>" />
                                                                </div>
                                                            </div>
                                                        </div>                                                     
                                                    </div>

                                                    <div class="row text-right">
                                                        <div class="form-group row">
                                                            <label for="example-text-input" class="col-sm-3 col-form-label"></label>
                                                            <div class="col-sm-12">
                                                                <button type="button" id="upload_image" class="btn btn-info btn-large" name="upload_image"><?php echo display('next') ?> <i class="fa fa-caret-down"></i> </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!-- Product Title -->
                    <div class="panel panel-bd">
                        <div class="panel-heading">
                            <h4 class="panel-title"><a href="#collapse3" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo display('step_3')?>: <?php echo display('product_title')?> <i class="fa fa-caret-down"></i></a></h4>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse">
                            <div class="panel-body">
                                <form action="<?php echo base_url('cproduct_update_upload_title/')?>"
                                      id="updateTitle" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-6 m-b-20">
                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs">   
                                                <?php
                                                $i=1;
                                                if ($language) {
                                                    foreach ($language as $value) {
                                                ?>
                                                <li class="<?php if($i==1){echo "active";}else{echo " ";}?>"><a href="#tab<?php echo $i?>" data-toggle="tab" aria-expanded="true"><?php echo $value?></a></li>
                                                <?php
                                                    $i++;
                                                    }
                                                }
                                                ?>
                                            </ul>
                                            <!-- Tab panels -->
                                            <div class="tab-content">
                                                <?php
                                               if ($language) {
                                                    $i=1;
                                                    foreach ($language as $value) {
                                                        $product = $this->Products->retrive_pending_product_title($upload_id,$value);
                                                ?>
                                                <div class="tab-pane fade <?php if($i==1){echo "active in";}else{echo " ";}?>" id="tab<?php echo $i?>">
                                                    <div class="panel-body">
                                                        <div class="form-group row">
                                                            <label for="upload_title_<?php echo $i?>" class="col-sm-2 col-form-label"><?php echo display('product_title') ?> <i class="text-danger">*</i></label>
                                                            <div class="col-sm-10">
                                                                <input class="form-control" name ="upload_title[]" id="upload_title_<?php echo $i?>" type="text" value="<?php echo (!empty($product)?$product->title:null) ?>" placeholder="<?php echo display('product_title') ?>">

                                                                <input name ="lang_id[]" type="hidden" value="<?php echo strtolower((!empty($product)?$product->lang_id:$value))?>">
                                                                <input name ="upload_id" type="hidden" value="<?php echo (!empty($product)?$product->upload_id:$upload_id)?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                                    $i++;
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row text-right">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <button type="submit" class="btn btn-info btn-large"><?php echo display('next') ?> <i class="fa fa-caret-down"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>

                    <!-- Product Description -->
                    <div class="panel panel-bd">
                        <div class="panel-heading">
                            <h4 class="panel-title"><a href="#collapse4" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo display('step_4')?>: <?php echo display('product_description')?> <i class="fa fa-caret-down"></i></a></h4>
                        </div>

                        <div id="collapse4" class="panel-collapse collapse">

                            <div class="panel-body">
                                <form action="<?php echo base_url('cproduct_update_upload_escription/')?>"
                                      id="updateDescription" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 m-b-20">
                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs">   
                                                <?php
                                                $i=1;
                                                if ($language) {
                                                    foreach ($language as $value) {
                                                ?>
                                                <li class="<?php if($i==1){echo "active";}else{echo " ";}?>"><a href="#des_tab<?php echo $i?>" data-toggle="tab" aria-expanded="true"><?php echo $value?></a></li>
                                                <?php
                                                    $i++;
                                                    }
                                                }
                                                ?>
                                            </ul>
                                            <!-- Tab panels -->
                                            <div class="tab-content">
                                                <?php
                                                $i=1;
                                                if ($language) {
                                                    foreach ($language as $value) {
                                                        $product = $this->Products->retrive_pending_product_description($upload_id,$value);
                                                ?>
                                                <div class="tab-pane fade <?php if($i==1){echo "active in";}else{echo " ";}?>" id="des_tab<?php echo $i?>">
                                                    <div class="panel-body">
                                                        <div class="form-group row">
                                                            <label for="description<?php echo $i?>" class="col-sm-2 col-form-label"><?php echo display('description') ?> <i class="text-danger">*</i></label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control summernote" name ="description[]" id="description<?php echo $i?>" type="text" placeholder="<?php echo display('description') ?>"><?php echo (!empty($product)?$product->description:null)?></textarea>
                                                                <input name ="lang_id[]" type="hidden" value="<?php echo strtolower((!empty($product)?$product->lang_id:$value))?>">
                                                                <input name ="upload_id" type="hidden" value="<?php echo (!empty($product)?$product->upload_id:$upload_id)?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                                    $i++;
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
              
                                    <div class="row text-right">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <button type="submit"  class="btn btn-info btn-large"><?php echo display('next') ?> <i class="fa fa-caret-down"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <!-- Product Specification -->
                    <div class="panel panel-bd">
                        <div class="panel-heading">
                            <h4 class="panel-title"><a href="#collapse5" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo display('step_5')?>: <?php echo display('specification')?> <i class="fa fa-caret-down"></i></a></h4>
                        </div>

                        <div id="collapse5" class="panel-collapse collapse">
                            <div class="panel-body">
                                <form action="<?php echo base_url('cproduct_update_upload_specification/')?>"
                                      id="updateSpecification" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 m-b-20">
                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs">   
                                                <?php
                                                $i=1;
                                                if ($language) {
                                                    foreach ($language as $value) {
                                                ?>
                                                <li class="<?php if($i==1){echo "active";}else{echo " ";}?>"><a href="#sp_tab<?php echo $i?>" data-toggle="tab" aria-expanded="true"><?php echo $value?></a></li>
                                                <?php
                                                    $i++;
                                                    }
                                                }
                                                ?>
                                            </ul>
                                            <!-- Tab panels -->
                                            <div class="tab-content">
                                                <?php
                                                $i=1;
                                                if ($language) {
                                                    foreach ($language as $value) {
                                                        $product = $this->Products->retrive_pending_product_specification($upload_id,$value);
                                                ?>
                                                <div class="tab-pane fade <?php if($i==1){echo "active in";}else{echo " ";}?>" id="sp_tab<?php echo $i?>">
                                                    <div class="panel-body">

                                                        <div class="form-group row">
                                                            <label for="description<?php echo $i?>" class="col-sm-2 col-form-label"><?php echo display('specification') ?> <i class="text-danger">*</i></label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control summernote" name ="description[]" id="description<?php echo $i?>" type="text" placeholder="<?php echo display('description') ?>"><?php echo (!empty($product)?$product->description:null)?></textarea>
                                                                <input name ="lang_id[]" type="hidden" value="<?php echo strtolower((!empty($product)?$product->lang_id:$value))?>">

                                                                <input name ="upload_id" type="hidden" value="<?php echo (!empty($product)?$product->upload_id:$upload_id)?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                                    $i++;
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
              
                                    <div class="row text-right">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <button type="submit"  class="btn btn-info btn-large"><?php echo display('submit') ?> <i class="fa fa-caret-down"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Edit pending product end -->

<!-- Product upload ajax -->
<script type="text/javascript">
    $(document).ready(function (e) {

        //Update seller product
        $('#update-seller').on('click', function() {

            var seller_id   = $('#seller').val();
            var category_id = $('#category_id').val();
            var price       = $('#price').val();
            var quantity    = $('#quantity').val();
            var product_model    = $('#product_model').val();
            var variant     = $('#variant').val();
            var unit        = $('#unit').val();
            var brand       = $('#brand').val();
            var product_type     = $('#product_type').val();
            var tag_value   = $('.tag_value').val();
            var upload_id   = $('#upload_id').val();
            var product_id  = $('#product_id').val();
            var status      = $('#status').val();

            var on_promotion= $('#on_promotion').val();
            var best_sale   = $('#best_sale').val();
            var details     = $('#details').val();
            var pre_order   = $('#pre_order').val();
            var pre_order_quantity   = $('#pre_order_quantity').val();
            var comission   = $('#comission').val();


            //Pre order quantity
            if (pre_order == 1) {
                if (!pre_order_quantity) {
                   toastr.error('<?php echo display('please_fill_up_all_required_field')?>', " ", {
                        "timeOut": "3000",
                        "extendedTImeout": "500"
                    });
                   return false;
                }
            }  

            //Onpromotion price
            if (on_promotion == 1) {
                if (!details) {
                   toastr.error('<?php echo display('please_fill_up_all_required_field')?>', " ", {
                        "timeOut": "3000",
                        "extendedTImeout": "500"
                    });
                   return false;
                }
            }

            if (!quantity) {
               toastr.error('<?php echo display('please_fill_up_all_required_field')?>', " ", {
                    "timeOut": "3000",
                    "extendedTImeout": "500"
                });
               return false;
            }

            if ((seller_id == 0) ||(product_id == 0) || (category_id == 0) || (price == 0) || (quantity == 0) || (product_model == 0)) {
                toastr.error('<?php echo display('please_fill_up_all_required_field')?>', " ", {
                    "timeOut": "1000",
                    "extendedTImeout": "500"
                }); 
            }else{
                $.ajax({
                    url: "<?php echo base_url('cproduct_update_upload_product')?>",
                    type: "post",
                    data: {seller_id,product_id,category_id,price,quantity,product_model,variant,unit,brand,product_type,tag_value,upload_id,status,on_promotion,best_sale,details,pre_order,pre_order_quantity,comission},

                    success: function(data) {
                        if (data == 1) {
                            $('a[href=\'#collapse2\']').trigger('click');
                        }
                    },
                    error: function() {
                        alert("Error !");
                    }
                });
            }
        });

        //Image upload form
        $('#imageUploadForm').on('submit',(function(e) {
            e.preventDefault();
            var formData = new FormData(this);
            $.ajax({
                type:'POST',
                url: $(this).attr('action'),
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                    
                    if (data == 1) {
                        toastr.success('<?php echo display('upload_successfully')?>', " ", {
                            "timeOut": "1000",
                            "extendedTImeout": "500"
                        }); 
                        $("#image_list").load(location.href+" #image_list>*","");
                    }else if (data == 2) {
                        toastr.error('<?php echo display('already_exists')?>', " ", {
                            "timeOut": "1000",
                            "extendedTImeout": "500"
                        }); 
                    }else if (data == 3) {
                        toastr.error('<?php echo display('already_exists')?>', " ", {
                            "timeOut": "1000",
                            "extendedTImeout": "500"
                        }); 
                    }else{
                        toastr.error(data, " ", {
                            "timeOut": "1000",
                            "extendedTImeout": "500"
                        });  
                    }
                },
                error: function(data){
                    console.log("error");
                }
            });
        }));

        //Select file/image
        $("#imageUpload").on("change", function() {
            $("#imageUploadForm").submit();
        });

        //Click to next button
        $('#upload_image').on('click',(function(e){
            //Trigger to next tab
            $('a[href=\'#collapse3\']').trigger('click');
        }));    

        //Update title by ajax
        $('#updateTitle').on('submit',(function(e) {
            e.preventDefault();
            var formData = new FormData(this);
            $.ajax({
                type:'POST',
                url: $(this).attr('action'),
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                    if (data == 1) {
                       $('a[href=\'#collapse4\']').trigger('click');
                    }else if (data == 2) {
                        toastr.error('<?php echo display('already_exists')?>', " ", {
                            "timeOut": "1000",
                            "extendedTImeout": "500"
                        }); 
                    }else if (data == 3) {
                        toastr.error('<?php echo display('please_fill_up_all_required_field')?>', " ", {
                            "timeOut": "1000",
                            "extendedTImeout": "500" 
                        }); 
                    }else{
                        toastr.error(data, " ", {
                            "timeOut": "1000",
                            "extendedTImeout": "500"
                        });  
                    }
                },
                error: function(data){
                    console.log("error");
                }
            });
        }));

        //Click to next button
        $('#uploadDesc').on('click',(function(e){
            //Trigger to next tab
            $('a[href=\'#collapse4\']').trigger('click');
        }));

        //Update description by ajax 
        $('#updateDescription').on('submit',(function(e) {
            e.preventDefault();
            var formData = new FormData(this);
            $.ajax({
                type:'POST',
                url: $(this).attr('action'),
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                    if (data == 1) {
                        $('a[href=\'#collapse5\']').trigger('click');
                    }else if (data == 2) {
                        toastr.error('<?php echo display('already_exists')?>', " ", {
                            "timeOut": "1000",
                            "extendedTImeout": "500"
                        }); 
                    }else if (data == 3) {
                        toastr.error('<?php echo display('please_fill_up_all_required_field')?>', " ", {
                            "timeOut": "1000",
                            "extendedTImeout": "500"
                        }); 
                    }else{
                        toastr.error(data, " ", {
                            "timeOut": "1000",
                            "extendedTImeout": "500"
                        });  
                    }
                },
                error: function(data){
                    console.log("error");
                }
            });
        }));    

        //Update specification by ajax 
        $('#updateSpecification').on('submit',(function(e) {
            e.preventDefault();
            var formData = new FormData(this);
            $.ajax({
                type:'POST',
                url: $(this).attr('action'),
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                success:function(data){
                    if (data == 1) {
                        toastr.success('<?php echo display('successfully_updated')?>', " ", {
                            "timeOut": "1000",
                            "extendedTImeout": "500"
                        });
                        window.location.href = '<?php echo base_url('cproduct/pending_product/all/item')?>';
                    }else if (data == 2) {
                        toastr.error('<?php echo display('already_exists')?>', " ", {
                            "timeOut": "1000",
                            "extendedTImeout": "500"
                        }); 
                    }else if (data == 3) {
                        toastr.error('<?php echo display('please_fill_up_all_required_field')?>', " ", {
                            "timeOut": "1000",
                            "extendedTImeout": "500"
                        }); 
                    }else{
                        toastr.error(data, " ", {
                            "timeOut": "1000",
                            "extendedTImeout": "500"
                        });  
                    }
                },
                error: function(data){
                    console.log("error");
                }
            });
        }));

        //Click to next button
        $('#next_spec').on('click',(function(e){
            //Trigger to next tab
            $('a[href=\'#collapse5\']').trigger('click');
        }));
    });
</script>

<!-- Check seller store name-->
<script type="text/javascript">
    $('body').on('blur', '#store_name', function(){
        var store_name = $(this).val();
        $.ajax
        ({
            url: "<?php echo base_url('cproduct_check_seller_store_name')?>",
            data: {store_name:store_name},
            type: "post",
            success: function(data)
            {
                if (data == 1) {
                    toastr.error('Store name already exists !', " ", {
                        "timeOut": "1000",
                        "extendedTImeout": "500"
                    });  
                }else{
                    toastr.success(data, " ", {
                        "timeOut": "1000",
                        "extendedTImeout": "500"
                    });  
                }
            } 
        });
    });
</script>

<!-- Delete image -->
<script type="text/javascript">
    // Delete image
    $('body').on('click', '.delete_image', function() {
        var image_id   = $(this).attr("name");
        var image_path = $(this).prev().val();

        $.ajax({
            url: "<?php echo base_url('cproduct_delete_upload_image')?>",
            type: "post",
            data: {image_id,image_path},
            success: function(data) {
                if (data == 1) {
                    toastr.success('Image successfully delete.', " ", {
                        "timeOut": "3000",
                        "extendedTImeout": "500"
                    });
                    $("#image_list").load(location.href+" #image_list>*","");
                }
            },
            error: function() {
                alert("Error !");
            }
        });
    });

    //Make image primary by ajax
    $('body').on('click', '.make_primary', function() {
        var image_id  = $(this).next().val();
        var upload_id = $(this).next().next().val();

        $.ajax({
            url: "<?php echo base_url('cproduct_make_pending_img_primary')?>",
            type: "post",
            data: {image_id,upload_id},
            success: function(data) {
                if (data == 1) {
                    toastr.success('<?php echo display('primary_image_set')?>', " ", {
                        "timeOut": "3000",
                        "extendedTImeout": "500"
                    });
                    $("#image_list").load(location.href+" #image_list>*","");
                }
            },
            error: function() {
                alert("Error !");
            }
        });
    });

    //Retrive comission by category id
    $('body').on('change', '#category_id', function() {
        var category_id   = $(this).val();

        $.ajax({
            url: "<?php echo base_url('retrive_com_value')?>",
            type: "post",
            data: {category_id:category_id},
            success: function(data) {
                if (data) {
                  $('.comission').html(data);
                }
            },
            error: function() {
                alert("Error !");
            }
        });
    });
</script>

<!--Select ads type by javascript start-->
<script type="text/javascript">
    $(document).ready(function() {

        //On promotion details add
        $('#on_promotion').on('change', function() {
            var onsale = $('#on_promotion option:selected').val();
            if (onsale == 1) {
                $('.on_promotion').css({'display': 'block'});
            }else {
                $('.on_promotion').css({'display': 'none'});
            }
        });

        //Pre order add
        $('#pre_order').on('change', function() {
            var onsale = $('#pre_order option:selected').val();
            if (onsale == 1) {
                $('.pre_order_quantity').css({'display': 'block'});
            }else {
                $('.pre_order_quantity').css({'display': 'none'});
            }
        });

    });
</script>
<!--Select ads type by javascript end-->