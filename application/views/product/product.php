<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Manage product Start -->
<div class="content-wrapper">
	<section class="content-header">
	    <div class="header-icon">
	        <i class="pe-7s-note2"></i>
	    </div>
	    <div class="header-title">
	        <h1><?php echo display('manage_product') ?></h1>
	        <small><?php echo display('manage_product') ?></small>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
	            <li><a href="#"><?php echo display('seller') ?></a></li>
	            <li class="active"><?php echo display('manage_product') ?></li>
	        </ol>
	    </div>
	</section>

	<section class="content">
		<!-- Alert Message -->
	    <?php
	        $message = $this->session->userdata('message');
	        if (isset($message)) {
	    ?>
	    <div class="alert alert-info alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        <?php echo $message ?>                    
	    </div>
	    <?php 
	        $this->session->unset_userdata('message');
	        }
	        $error_message = $this->session->userdata('error_message');
	        if (isset($error_message)) {
	    ?>
	    <div class="alert alert-danger alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        <?php echo $error_message ?>                    
	    </div>
	    <?php 
	        $this->session->unset_userdata('error_message');
	        }
	    ?>

	    <?php if($this->permission->check_label('add_product')->access()){?>
        <div class="row">
            <div class="col-sm-12">
                <div class="column">
                  	<a href="<?php echo base_url('cproduct')?>" class="btn btn-success m-b-5 m-r-2"><i class="ti-plus"> </i> <?php echo display('add_product')?></a>
                </div>
            </div>
        </div>
    	<?php } ?>

		<!-- Manage product -->
		<div class="row">
			<div class="col-sm-12">
		        <div class="panel panel-default">
		            <div class="panel-body"> 
	     				<div class="row"> 

	     					<form action="<?php echo base_url('cproduct/manage_product/all/item')?>" method="get">
	                            <div class="col-sm-3">
	                                <div class="form-group row">
	                                    <label for="seller" class="col-sm-3 col-form-label"><?php echo display('seller')?> </label>
	                                    <div class="col-sm-9">
	                                      	<select class="form-control" name="seller" id="seller">
	                                      		<option></option>
	                                      		<?php
	                                      		if ($seller_list) {
	                                      			foreach ($seller_list as $seller) {
	                                      		?>
	                                      		<option value="<?php echo $seller['seller_id']?>" <?php if (isset($_GET['seller'])) {if ($_GET['seller'] == $seller['seller_id']) {echo "selected";}}?>><?php echo $seller['first_name'].' '.$seller['last_name']?></option>
	                                      		<?php
	                                      			}
	                                      		}
	                                      		?>
	                                      	</select>
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-3">
	                                <div class="form-group row">
	                                    <label for="model" class="col-sm-3 col-form-label"><?php echo display('model')?></label>
	                                    <div class="col-sm-9">
	                                      	<input class="form-control" name ="model" id="model" type="text" placeholder="<?php echo display('model') ?>" value="<?php if(isset($_GET['model'])){ echo $_GET['model'];}?>">
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-3">
	                                <div class="form-group row">
	                                    <label for="title" class="col-sm-3 col-form-label"><?php echo display('title')?></label>
	                                    <div class="col-sm-9">
	                                      	<input class="form-control" name ="title" id="title" type="text" placeholder="<?php echo display('title') ?>" value="<?php if(isset($_GET['title'])){ echo $_GET['title'];}?>">
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-3">
	                                <div class="form-group row">
	                                    <label for="category" class="col-sm-3 col-form-label"><?php echo display('category')?></label>
	                                    <div class="col-sm-9">
											<select class="form-control" name="category" id="category">
	                                      		<option></option>
	                                      		<?php
	                                      		if ($category_list) {
	                                      			foreach ($category_list as $category) {
	                                      		?>
	                                      		<option value="<?php echo $category['category_id']?>" <?php if (isset($_GET['category'])) {if ($_GET['category'] == $category['category_id']) {echo "selected";} }?>><?php echo $category['category_name']?></option>
	                                      		<?php
	                                      			}
	                                      		}
	                                      		?>
	                                      	</select>
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-2">
	                                <div class="form-group row">
	                                    <div class="col-sm-7">
	                                      	<button type="submit" class="btn btn-primary"><?php echo display('search')?></button>
	                                    </div>
	                                </div>
	                            </div>
	                        </form>   
                        </div>        
			        </div>
		        </div>
		    </div>
		</div>

		<!-- Product search -->
  <!--       <script type="text/javascript">
            //Product search by ajax 
	        $('#seller,#model,#title,#category').on('change keyup',(function(e) {
	            e.preventDefault();
	            var seller_id = $('#seller').val();
	            var model 	  = $('#model').val();
	            var title 	  = $('#title').val();
	            var category  = $('#category').val();

	            var url_location = '<?php echo base_url('manage_product')?>?seller='+seller_id+'&model='+model+'&title='+title+'&category='+category;
            	window.location.href = url_location;
	        }));
        </script> -->

		<div class="row">
		    <div class="col-sm-12">
		        <div class="panel panel-bd lobidrag">
		            <div class="panel-heading">
		                <div class="panel-title">
		                    <h4><?php echo display('manage_product') ?></h4>
		                </div>
		            </div>
		            <div class="panel-body">
		                <div class="table-responsive">
		                    <table id="dataTableExample3" class="table table-bordered table-striped table-hover">
								<thead>
									<tr>
										<th><?php echo display('sl') ?></th>
										<th><?php echo display('product_title') ?></th>
										<th><?php echo display('seller') ?></th>
										<th><?php echo display('category') ?></th>
										<th><?php echo display('price') ?></th>
										<th><?php echo display('offer_price') ?></th>
										<th><?php echo display('quantity') ?></th>
										<th><?php echo display('pre_order_quantity') ?></th>
										<th><?php echo display('product_model') ?></th>
										<th><?php echo display('brand') ?></th>
										<th style="width: 50%"><?php echo display('status') ?></th>
										<th style="text-align:center !Important"><?php echo display('action') ?></th>
									</tr>
								</thead>
								<tbody>
								<?php if ($product_list) { 
									$i=1;
									foreach ($product_list as $product) {
								?>
									<tr>
										<td><?php echo $product['sl']?></td>
										<td><?php echo $product['title']?></td>
										<td><?php echo $product['first_name']." ".$product['last_name']." (".$product['seller_id'].")"?></td>
										<td><?php echo $product['category_name']?></td>
										<td><?php echo $product['price']?></td>
										<td><?php echo $product['offer_price']?></td>
                                        <td><?php echo $product['quantity']?></td>
										<td><?php echo $product['pre_order_quantity']?></td>
										<td><?php echo $product['product_model']?></td>
										<td><?php echo $product['brand_name']?></td>
										<td>
										<?php if($this->permission->check_label('manage_product')->update()->access()){?>
										<form action="<?php echo base_url('cproduct/update_approved_status/'.$product['product_id'])?>" method="post" id="add_note_form">
											<select class="form-control" id="product_status" name="product_status" required="">
		                                       	<option value=""></option>
		                                        <option value="1" <?php if ($product['status'] == '1'){echo "selected";}?>><?php echo display('pending')?></option>

		                                        <option value="2" <?php if ($product['status'] == '2'){echo "selected";}?>><?php echo display('approved')?></option>

		                                        <option value="3" <?php if ($product['status'] == '3'){echo "selected";}?>><?php echo display('denied')?></option>
		                                    </select>

		                                    <input type="hidden" value="<?php echo $product['email'] ?>" name="seller_email"/>

						                    <input type="submit" class="btn btn-xs btn-primary" value="<?php echo display('update') ?>" />
										</form>
										<?php }	?>
										</td>
										<td>
											<center>
												<?php if($this->permission->check_label('manage_product')->update()->access()){?>
												<a href="<?php echo base_url().'cproduct/product_update_form/item/'.$product['product_id']; ?>" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="left" title="<?php echo display('update') ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
												<?php } if($this->permission->check_label('manage_product')->delete()->access()){?>
												<a href="<?php echo base_url('cproduct/delete_product/item/'.$product['product_id'])?>" class="btn btn-danger btn-xs" onclick="return confirm('<?php echo display('are_you_sure_want_to_delete')?>');" data-toggle="tooltip" data-placement="right" title="" data-original-title="<?php echo display('delete') ?> "><i class="fa fa-trash-o" aria-hidden="true"></i></a>
												<?php } ?>
											</center>
										</td>
									</tr>
								<?php $i++;  } }?>
								</tbody>
		                    </table>
		                </div>
		                <div class="text-right">
		                	<?php echo $links?>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</section>
</div>
<!-- Manage product End -->