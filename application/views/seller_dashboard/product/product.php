<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Manage product Start -->
<div class="content-wrapper">
	<section class="content-header">
	    <div class="header-icon">
	        <i class="pe-7s-note2"></i>
	    </div>
	    <div class="header-title">
	        <h1><?php echo display('manage_product') ?></h1>
	        <small><?php echo display('manage_product') ?></small>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
	            <li><a href="#"><?php echo display('seller') ?></a></li>
	            <li class="active"><?php echo display('manage_product') ?></li>
	        </ol>
	    </div>
	</section>

	<section class="content">
		<!-- Alert Message -->
	    <?php
	        $message = $this->session->userdata('message');
	        if (isset($message)) {
	    ?>
	    <div class="alert alert-info alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        <?php echo $message ?>                    
	    </div>
	    <?php 
	        $this->session->unset_userdata('message');
	        }
	        $error_message = $this->session->userdata('error_message');
	        if (isset($error_message)) {
	    ?>
	    <div class="alert alert-danger alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        <?php echo $error_message ?>                    
	    </div>
	    <?php 
	        $this->session->unset_userdata('error_message');
	        }
	    ?>
	    <!-- Product Filtering -->
	    <div class="row">
			<div class="col-sm-12">
		        <div class="panel panel-default">
		            <div class="panel-body"> 
	     				<div class="row"> 

                            <div class="col-sm-3">
                                <div class="form-group row">
                                    <label for="model" class="col-sm-3 col-form-label"><?php echo display('model')?></label>
                                    <div class="col-sm-9">
                                      	<input class="form-control" name ="model" id="model" type="text" placeholder="<?php echo display('model') ?>" required value="<?php if(isset($_GET['model'])){ echo $_GET['model'];}?>">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group row">
                                    <label for="title" class="col-sm-3 col-form-label"><?php echo display('title')?></label>
                                    <div class="col-sm-9">
                                      	<input class="form-control" name ="title" id="title" type="text" placeholder="<?php echo display('title') ?>" required value="<?php if(isset($_GET['title'])){ echo $_GET['title'];}?>">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group row">
                                    <label for="category" class="col-sm-3 col-form-label"><?php echo display('category')?></label>
                                    <div class="col-sm-9">
										<select class="form-control" name="category" id="category">
                                      		<option></option>
                                      		<?php
                                      		if ($category_list) {
                                      			foreach ($category_list as $category) {
                                      		?>
                                      		<option value="<?php echo $category['category_id']?>" <?php if (isset($_GET['category'])) {if ($_GET['category'] == $category['category_id']) {echo "selected";} }?>><?php echo $category['category_name']?></option>
                                      		<?php
                                      			}
                                      		}
                                      		?>
                                      	</select>
                                    </div>
                                </div>
                            </div>
                        </div>        
			        </div>
		        </div>
		    </div>
		</div>

		<!-- Product search -->
        <script type="text/javascript">
            //Product search by ajax 
	        $('#seller,#model,#title,#category').on('change keyup',(function(e) {
	            e.preventDefault();
	            var seller_id = $('#seller').val();
	            var model 	  = $('#model').val();
	            var title 	  = $('#title').val();
	            var category  = $('#category').val();

	            var url_location = '<?php echo base_url('seller/manage_product/all/item')?>?model='+model+'&title='+title+'&category='+category;
            	window.location.href = url_location;
	        }));
        </script>

        <div class="row">
            <div class="col-sm-12">
                <div class="column">
                  	<a href="<?php echo base_url('seller_upload_product')?>" class="btn btn-success m-b-5 m-r-2"><i class="ti-plus"> </i> <?php echo display('upload_product')?></a>
                </div>
            </div>
        </div>

		<!-- Manage product -->
		<div class="row">
		    <div class="col-sm-12">
		        <div class="panel panel-bd lobidrag">
		            <div class="panel-heading">
		                <div class="panel-title">
		                    <h4><?php echo display('manage_product') ?></h4>
		                </div>
		            </div>
		            <div class="panel-body">
		                <div class="table-responsive">
		                    <table id="dataTableExample3" class="table table-bordered table-striped table-hover">
								<thead>
									<tr>
										<th><?php echo display('sl') ?></th>
										<th><?php echo display('title') ?></th>
										<th><?php echo display('category') ?></th>
										<th><?php echo display('price') ?></th>
										<th><?php echo display('quantity') ?></th>
										<th><?php echo display('in_stock') ?></th>
										<th><?php echo display('product_model') ?></th>
										<th><?php echo display('variant') ?></th>
										<th><?php echo display('brand') ?></th>
										<th><?php echo display('status') ?></th>
										<th style="text-align:center !Important"><?php echo display('action') ?></th>
									</tr>
								</thead>
								<tbody>
								<?php if ($product_list) { 
									foreach ($product_list as $product) {
								?>
									<tr>
										<td><?php echo $product['sl']?></td>
										<td><?php echo $product['title']?></td>
										<td><?php echo $product['category_name']?></td>
										<td><?php echo $product['price']?></td>
										<td><?php echo $product['quantity'];?></td>
										<td>
										<?php 
											echo $this->Products->get_product_stock($product['product_id']);
										?>
										</td>
										<td><?php echo $product['product_model']?></td>
										<td>
										<?php 
											$variant = $product['variant_id'];
											$v = explode(",", $variant);
											for ($i=0; $i < count($v) ; $i++) {
												if ($v[$i]) {
													$variant = $this->db->select('*')
															->from('variant')
															->where('variant_id',$v[$i])
															->get()
															->row();
													echo $variant->variant_name.",";
												}
											}										
										?>
										</td>
										<td><?php echo $product['brand_name']?></td>
										<td><?php 
											if ($product['status'] == 1) {
												echo "<span class=\"label label-warning m-r-15\">".display('pending')."</span>";
											}elseif ($product['status'] == 2) {
												echo "<span class=\"label label-success m-r-15\">".display('approved')."</span>";
											}else{
												echo "<span class=\"label label-danger m-r-15\">".display('denied')."</span>";
											}

										?></td>
										<td>
											<center>
												<a href="<?php echo base_url().'seller/product/seller_product_update_form/'.$product['upload_id']; ?>" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="<?php echo display('update') ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
											</center>
										</td>
									</tr>
								<?php } }?>
								</tbody>
		                    </table>
		                </div>
		                <div class="text-right"><?php echo $links?></div>
		            </div> 
		        </div>
		    </div>
		</div>
	</section>
</div>
<!-- Manage product End -->