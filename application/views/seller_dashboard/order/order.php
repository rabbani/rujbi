<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Manage order start -->
<div class="content-wrapper">
	<section class="content-header">
	    <div class="header-icon">
	        <i class="pe-7s-note2"></i>
	    </div>
	    <div class="header-title">
	        <h1><?php echo display('manage_order') ?></h1>
	        <small><?php echo display('manage_order') ?></small>
	        <ol class="breadcrumb">
	            <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
	            <li><a href="#"><?php echo display('order') ?></a></li>
	            <li class="active"><?php echo display('manage_order') ?></li>
	        </ol>
	    </div>
	</section>

	<section class="content">
		<!-- Alert Message -->
	    <?php
	        $message = $this->session->userdata('message');
	        if (isset($message)) {
	    ?>
	    <div class="alert alert-info alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        <?php echo $message ?>                    
	    </div>
	    <?php 
	        $this->session->unset_userdata('message');
	        }
	        $error_message = $this->session->userdata('error_message');
	        if (isset($error_message)) {
	    ?>
	    <div class="alert alert-danger alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        <?php echo $error_message ?>                    
	    </div>
	    <?php 
	        $this->session->unset_userdata('error_message');
	        }
	    ?>

	    <!-- Order filtering -->
        <div class="row">
			<div class="col-sm-12">
		        <div class="panel panel-default">
		            <div class="panel-body"> 

		            	<form action="<?php echo base_url('seller_manage_order')?>" method="get">
		     				<div class="row">
	                            <div class="col-sm-4">
	                                <div class="form-group row">
	                                    <label for="order_no" class="col-sm-4 col-form-label"><?php echo display('order_no')?></label>
	                                    <div class="col-sm-8">
	                                      	<input class="form-control" name ="order_no" id="order_no" type="text" placeholder="<?php echo display('order_no') ?>" value="<?php if(isset($_GET['order_no'])){ echo $_GET['order_no'];}?>">
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-4">
	                                <div class="form-group row">
	                                    <label for="pre_order_no" class="col-sm-5 col-form-label"><?php echo display('pre_order_no')?></label>
	                                    <div class="col-sm-7">
	                                      	<input class="form-control" name ="pre_order_no" id="pre_order_no" type="text" placeholder="<?php echo display('pre_order_no') ?>" value="<?php if(isset($_GET['pre_order_no'])){ echo $_GET['pre_order_no'];}?>">
	                                    </div>
	                                </div>
	                            </div>
	                            
	                            <div class="col-sm-4">
	                                <div class="form-group row">
	                                    <label for="customer" class="col-sm-4 col-form-label"><?php echo display('customer')?> </label>
	                                    <div class="col-sm-8">
	                                      	<select class="form-control" name="customer" id="customer">
	                                      		<option></option>
	                                      		<?php
	                                      		if ($customer_list) {
	                                      			foreach ($customer_list as $customer) {
	                                      		?>
	                                      		<option value="<?php echo $customer['customer_id']?>" <?php if (isset($_GET['customer'])) {if ($_GET['customer'] == $customer['customer_id']) {echo "selected";}}?>><?php echo $customer['customer_mobile']?></option>
	                                      		<?php
	                                      			}
	                                      		}
	                                      		?>
	                                      	</select>
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-4">
	                                <div class="form-group row">
	                                    <label for="date" class="col-sm-4 col-form-label"><?php echo display('date')?></label>
	                                    <div class="col-sm-8">
	                                      	<input type="text" class="form-control datepicker-manage" id="date" data-range="true"  data-multiple-dates-separator="---" data-language='en' name="date" placeholder="<?php echo display('date') ?>" value="<?php if(isset($_GET['date'])){ echo $_GET['date'];}?>">
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-4">
	                                <div class="form-group row">
	                                    <label for="order_status" class="col-sm-5 col-form-label"><?php echo display('order_status')?> </label>
	                                    <div class="col-sm-7">
	                                      	<select class="form-control" id="order_status" name="order_status">
		                                       	<option value=""></option>
		                                       	<option value="1" <?php if (isset($_GET['order_status'])){if ($_GET['order_status'] == 1) {echo "selected";}}?>><?php echo display('pending')?></option>
		                                        <option value="2" <?php if (isset($_GET['order_status'])){if ($_GET['order_status'] == 2) {echo "selected";}}?>><?php echo display('processing')?></option>
		                                        <option value="3" <?php if (isset($_GET['order_status'])){if ($_GET['order_status'] == 3) {echo "selected";}}?>><?php echo display('shipping')?></option>
		                                        <option value="4" <?php if (isset($_GET['order_status'])){if ($_GET['order_status'] == 4) {echo "selected";}}?>><?php echo display('delivered')?></option>
		                                        <option value="5" <?php if (isset($_GET['order_status'])){if ($_GET['order_status'] == 5) {echo "selected";}}?>><?php echo display('returned')?></option>
		                                        <option value="6" <?php if (isset($_GET['order_status'])){if ($_GET['order_status'] == 6) {echo "selected";}}?>><?php echo display('cancel')?></option>
		                                    </select>
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="col-sm-2">
	                                <div class="form-group row">
	                                    <div class="col-sm-7">
	                                      	<button type="submit" class="btn btn-primary"><?php echo display('search')?></button>
	                                    </div>
	                                </div>
	                            </div>

	                        </div>  
		            	</form>   
			        </div>
		        </div>
		    </div>
		</div>

		<!-- Manage order -->
		<div class="row">
		    <div class="col-sm-12">
		        <div class="panel panel-bd lobidrag">
		            <div class="panel-heading">
		                <div class="panel-title">
		                    <h4><?php echo display('manage_order') ?></h4>
		                </div>
		            </div>
		            <div class="panel-body">
		                <div class="table-responsive">
		                    <table id="dataTableExample3" class="table table-bordered table-striped table-hover">
		                    	<thead>
									<tr>
										<th><?php echo display('sl') ?></th>
										<th><?php echo display('date') ?></th>
										<th><?php echo display('order_no') ?></th>
										<th><?php echo display('pre_order_no') ?></th>
										<th><?php echo display('customer_name') ?></th>
										<th><?php echo display('date') ?></th>
										<th><?php echo display('status') ?></th>
										<th><?php echo display('action') ?></th>
									</tr>
								</thead>
								<tbody>
								<?php
								if ($orders_list) {
									foreach ($orders_list as $order) {
								?>
									<tr>
										<td><?php echo $order['sl']?></td>
										<td><?php echo $order['date']?></td>
										<td>
											<a href="<?php echo base_url().'seller/order_details_data/'.$order['order_id'] ?>"><?php echo $order['id']?>
											</a>
										</td>
										<td><?php echo $order['pre_order_id']?></td>
										<td><?php echo $order['customer_name']?></td>
										<td><?php echo $order['date']?></td>
										<td>
										<?php 
										if ($order['order_status'] == 1) {
											echo "<span class=\"label label-pill label-warning m-r-15\">".display('pending')."</span>";
										}elseif ($order['order_status'] == 2) {
											echo "<span class=\"label label-pill label-info m-r-15\">".display('processing')."</span>";
										}elseif ($order['order_status'] == 3) {
											echo "<span class=\"label label-pill label-success m-r-15\">".display('shipping')."</span>";
										}elseif ($order['order_status'] == 4) {
											echo "<span class=\"label label-pill label-success m-r-15\">".display('delivered')."</span>";
										}elseif ($order['order_status'] == 5) {
											echo "<span class=\"label label-pill label-danger m-r-15\">".display('returned')."</span>";
										}elseif ($order['order_status'] == 6) {
											echo "<span class=\"label label-pill label-danger m-r-15\">".display('cancel')."</span>";
										}
										?>	
										</td>
										<td>
											<center>
												<a href="<?php echo base_url().'seller/order_details_data/'.$order['order_id'] ?>" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="<?php echo display('view') ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
											</center>
										</td>
									</tr>
								<?php
									}
								}
								?>
								</tbody>
		                    </table>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</section>
</div>
<!-- Manage order End -->