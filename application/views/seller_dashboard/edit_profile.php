<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Edit Profile Page Start -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="header-icon"><i class="pe-7s-user-female"></i></div>
        <div class="header-title">
            <h1><?php echo display('update_profile') ?></h1>
            <small><?php echo display('your_profile') ?></small>
            <ol class="breadcrumb">
                <li><a href="#"><i class="pe-7s-home"></i><?php echo display('home') ?></a></li>
                <li><a href="#"><?php echo display('profile') ?></a></li>
                <li class="active"><?php echo display('update_profile') ?></li>
            </ol>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12 col-md-4">
            </div>
            <div class="col-sm-12 col-md-4">

                <!-- Alert Message -->
                <?php
                    $message = $this->session->userdata('message');
                    if (isset($message)) {
                ?>
                <div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $message ?>                    
                </div>
                <?php 
                    $this->session->unset_userdata('message');
                    }
                    $error_message = $this->session->userdata('error_message');
                    if (isset($error_message)) {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $error_message ?>                    
                </div>
                <?php 
                    $this->session->unset_userdata('error_message');
                    }
                ?>
                <?php echo form_open_multipart('seller/seller_dashboard/update_profile',array('class' => 'form-vertical','id' => 'validate' ))?>

                <div class="card">
                    <div class="card-header">
                        <div class="card-header-menu">
                            <i class="fa fa-bars"></i>
                        </div>
                        <div class="card-header-headshot" style="background-image: url({image});"></div>
                    </div>
                    <div class="card-content">
                        <div class="card-content-member">
                            <h4 class="m-t-0">{first_name} {last_name}</h4>
                        </div>
                        <div class="card-content-languages">
                            <div class="card-content-languages-group">
                                <div>
                                    <h4><?php echo display('first_name') ?>:</h4>
                                </div>
                                <div>
                                    <ul>
                                        <input type="text" placeholder="<?php echo display('first_name') ?>" class="form-control" id="first_name" name="first_name" value="{first_name}" required />
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content-languages-group">
                                <div>
                                    <h4><?php echo display('last_name') ?>:</h4>
                                </div>
                                <div>
                                    <ul>
                                        <li><input type="text" placeholder="<?php echo display('last_name') ?>" class="form-control" id="last_name" name="last_name" value="{last_name}" required  /></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content-languages-group">
                                <div>
                                    <h4><?php echo display('email') ?>:</h4>
                                </div>
                                <div>
                                    <ul>
                                        <li><input type="email" placeholder="<?php echo display('email') ?>" class="form-control" id="email" name="email" value="{email}" required /></li>
                                    </ul>
                                </div>
                            </div>   
          
                            <div class="card-content-languages-group">
                                <div>
                                    <h4><?php echo display('address') ?>:</h4>
                                </div>
                                <div>
                                    <ul>
                                        <li><input type="text" placeholder="<?php echo display('address') ?>" class="form-control" id="address" name="address" value="{address}" /></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="card-content-languages-group">
                                <div>
                                    <h4><?php echo display('store_name') ?>:</h4>
                                </div>
                                <div>
                                    <ul>
                                        <li><input type="text" placeholder="<?php echo display('store_name') ?>" class="form-control" id="store_name" name="store_name" value="{seller_store_name}" />
                                            <span class="help-block small"><?php echo display('store_name_4_20_character_like_store_name') ?></span>
                                        </li>
                                    </ul>
                                </div>
                            </div>  

                            <div class="card-content-languages-group">
                                <div>
                                    <h4><?php echo display('business_name') ?>:</h4>
                                </div>
                                <div>
                                    <ul>
                                        <li><input type="text" placeholder="<?php echo display('business_name') ?>" class="form-control" id="business_name" name="business_name" value="{business_name}" /></li>
                                    </ul>
                                </div>
                            </div>        

                            <div class="card-content-languages-group">
                                <div>
                                    <h4><?php echo display('identification_doc_no') ?>:</h4>
                                </div>
                                <div>
                                    <ul>
                                        <li><input type="text" placeholder="<?php echo display('identification_doc_no') ?>" class="form-control" id="identification_doc_no" name="identification_doc_no" value="{identification_doc_no}"/></li>
                                    </ul>
                                </div>
                            </div>         

                            <div class="card-content-languages-group">
                                <div>
                                    <h4><?php echo display('identification_type') ?>:</h4>
                                </div>
                                <div>
                                    <ul>
                                        <li>
                                            <select class="form-control select2" id="identification_type" name="identification_type" style="width: 100%">
                                                <option value=""><?php echo display('select_one') ?></option>
                                                <option value="1" <?php if ($identification_type == '1') {echo "selected";}?>><?php echo display('driving_licence')?></option>
                                                <option value="2" <?php if ($identification_type == '2') {echo "selected";}?>><?php echo display('national_id')?></option>
                                                <option value="3" <?php if ($identification_type == '3') {echo "selected";}?>><?php echo display('passport_no')?></option>
                                                <option value="4" <?php if ($identification_type == '4') {echo "selected";}?>><?php echo display('others')?></option>
                                            </select>
                                        </li>
                                    </ul>
                                </div>
                            </div>      
                            <div class="card-content-languages-group">
                                <div>
                                    <h4><?php echo display('affiliate_id') ?>:</h4>
                                </div>
                                <div>
                                    <ul>
                                        <li><input type="text" placeholder="<?php echo display('affiliate_id') ?>" class="form-control" id="affiliate_id" name="affiliate_id" value="{affiliate_id}"/></li>
                                    </ul>
                                </div>
                            </div>                                                   

                            <div class="card-content-languages-group">
                                <div>
                                    <h4><?php echo display('image') ?>:</h4>
                                </div>
                                <div>
                                    <ul>
                                        <li><input type="file" id="image" name="image" value="{image}" /></li>
                                        <input type="hidden" name="old_image" value="{image}" />
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="card-footer-stats">
                          <button type="submit" class="btn btn-success" style="margin-left: 90px;"><?php echo display('update_profile') ?></button>
                        </div>
                    </div>
                </div>
                <?php echo form_close()?>
            </div>
        </div> 
    </section> <!-- /.content -->
</div> <!-- /.content-wrapper -->
<!-- Edit Profile Page End -->