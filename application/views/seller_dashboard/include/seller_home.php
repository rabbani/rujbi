<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Seller Home Start -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="header-icon">
            <i class="pe-7s-world"></i>
        </div>
        <div class="header-title">
            <h1><?php echo display('dashboard')?></h1>
            <small><?php echo display('home')?></small>
            <ol class="breadcrumb">
                <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home')?></a></li>
                <li class="active"><?php echo display('dashboard')?></li>
            </ol>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Alert Message -->
        <?php
            $message = $this->session->userdata('message');
            if (isset($message)) {
            $this->session->unset_userdata('error_message');
        ?>
        <div class="alert alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $message ?>                    
        </div>
        <?php 
            $this->session->unset_userdata('message');
            }
            $error_message = $this->session->userdata('error_message');
            if (isset($error_message)) {
            $this->session->unset_userdata('message');
        ?>
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $error_message ?>                    
        </div>
        <?php 
            $this->session->unset_userdata('error_message');
            }
        ?>
        <!-- First Counter -->
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
                <div class="panel panel-bd" style="height:100px">
                    <div class="panel-body">
                        <div class="statistic-box text-center">
                            <h2><?php echo (($position==0)?"$currency <span class=\"count-number\">".$total_balance."</span>":"<span class=\"count-number\">".$total_balance."</span> $currency") ?></h2>
                            <div style="font-size:15px"><?php echo display('balance')?></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
                <div class="panel panel-bd" style="height:100px">
                    <div class="panel-body">
                        <div class="statistic-box text-center">
                            <h2><?php echo (($position==0)?"$currency <span class=\"count-number\">".$total_comission."</span>":"<span class=\"count-number\">".$total_comission."</span> $currency") ?></h2>
                            <div style="font-size:15px"><?php echo display('comission')?></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
                <div class="panel panel-bd" style="height:100px">
                    <div class="panel-body">
                        <div class="statistic-box text-center">
                            <h2><?php echo (($position==0)?"$currency <span class=\"count-number\">".$total_minimum_balance."</span>":"<span class=\"count-number\">".$total_minimum_balance."</span> $currency") ?></h2>
                            <div style="font-size:15px"><?php echo display('my_balance')?></div>
                        </div>
                    </div>
                </div>
            </div>
           
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
                <div class="panel panel-bd" style="height:100px">
                    <div class="panel-body">
                        <div class="statistic-box text-center">
                            <h2><?php echo (($position==0)?"$currency <span class=\"count-number\">".$total_paid_amount."</span>":"<span class=\"count-number\">".$total_paid_amount."</span> $currency") ?></h2>
                            <div style="font-size:15px"><?php echo display('total_paid')?></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
                <div class="panel panel-bd" style="height:100px">
                    <div class="panel-body">
                        <div class="statistic-box text-center">
                            <h2><?php echo (($position==0)?"$currency <span class=\"count-number\">".$total_unpaid_balance."</span>":"<span class=\"count-number\">".$total_unpaid_balance."</span> $currency") ?></h2>
                            <div style="font-size:15px"><?php echo display('total_unpaid')?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <!-- Bar Chart -->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-bd lobidisable">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4><?php echo display('product_status')?></h4>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs product_status_tab" style="float: right;margin-top: -35px;">
                                <li class="active"><a href="#tab1" data-toggle="tab"><?php echo display('monthly')?></a></li>
                                <li><a href="#tab2" data-toggle="tab"><?php echo display('yearly')?></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel-body">
                        <!-- Tab panels -->
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="tab1">
                                <div class="panel-body">
                                    <canvas id="barChart" height="150"></canvas>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab2">
                                <div class="panel-body">
                                    <canvas id="barChart2" height="150"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> <!-- /.content -->
</div> <!-- /.content-wrapper -->
<!-- Seller Home end -->

<!-- ChartJs JavaScript -->
<script src="<?php echo base_url()?>assets/plugins/chartJs/Chart.min.js" type="text/javascript"></script>

<script type="text/javascript">
    //bar chart 1
    var ctx = document.getElementById("barChart");
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [<?php
                    for ($i=1; $i <= 31; $i++) {
                      echo '"'.$i.'",';
                    }
                ?>],
            datasets: [
                {
                    label: "<?php echo display('approved')?>",
                    data: [<?php
                            for ($i=1; $i <= 31; $i++) {
                               $approved = $this->Seller_dashboards->monthly_product_deliver($i,2);
                               if (!empty($approved->total_product)) {
                                    echo $approved->total_product.",";
                               }else{
                                    echo ",";
                               }
                            }
                        ?>],
                    borderColor: "rgba(55, 160, 0, 0.9)",
                    borderWidth: "0",
                    backgroundColor: "rgba(55,160,0,0.9)"
                },
                {
                    label: "<?php echo display('deny')?>",
                    data: [<?php
                            for ($i=1; $i <= 31; $i++) {
                               $approved = $this->Seller_dashboards->monthly_product_deliver($i,3);
                               if (!empty($approved->total_product)) {
                                    echo $approved->total_product.",";
                               }else{
                                    echo ",";
                               }
                            }
                        ?>],
                    borderColor: "rgba(0,0,0,0.09)",
                    borderWidth: "0",
                    backgroundColor: "rgb(242, 14, 94,0.7)"
                }
            ]
        },
        options: {
            scales: {
                yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
            }
        }
    });

    // bar chart 2
    var ctx = document.getElementById("barChart2");
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [<?php
                    for ($i=1; $i <= 12; $i++) {
                        if ($i==1) {
                            echo '"January",';
                        }elseif ($i==2) {
                            echo '"February",';
                        }elseif ($i==3) {
                            echo '"March",';
                        }elseif ($i==4) {
                            echo '"April",';
                        }elseif ($i==5) {
                            echo '"May",';
                        }elseif ($i==6) {
                           echo '"June",';
                        }elseif ($i==7) {
                           echo '"July",';
                        }elseif ($i==8) {
                           echo '"August",';
                        }elseif ($i==9) {
                           echo '"September",';
                        }elseif ($i==10) {
                           echo '"October",';
                        }elseif ($i==11) {
                           echo '"November",';
                        }elseif ($i==12) {
                           echo '"December"';
                        }
                    }
                ?>],
            datasets: [
                {
                    label: "<?php echo display('approved')?>",
                    data: [<?php
                            for ($i=1; $i <= 12; $i++) {
                               $approved = $this->Seller_dashboards->yearly_product_status($i,2);
                               if (!empty($approved->total_product)) {
                                    echo $approved->total_product.",";
                               }else{
                                echo ",";
                               }
                            }
                        ?>],
                    borderColor: "rgba(55, 160, 0, 0.9)",
                    borderWidth: "0",
                    backgroundColor: "rgba(55,160,0,0.9)"
                },
                {
                    label: "<?php echo display('deny')?>",
                    data: [<?php
                            for ($i=1; $i <= 12; $i++) {
                               $deny = $this->Seller_dashboards->yearly_product_status($i,3);
                               if (!empty($deny->total_product)) {
                                    echo $deny->total_product.",";
                               }else{
                                echo ",";
                               }
                            }
                        ?>],
                    borderColor: "rgba(0,0,0,0.09)",
                    borderWidth: "0",
                    backgroundColor: "rgb(242, 14, 94,0.7)"
                }
            ]
        },
        options: {
            scales: {
                yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
            }
        }
    });
</script>
 
