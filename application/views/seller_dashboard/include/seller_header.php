<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php
    $CI =& get_instance();
    $CI->load->model('Soft_settings');
    $CI->load->model('seller/Seller_dashboards');
    $soft_settings = $CI->Soft_settings->retrieve_setting_editdata();
    $seller        = $CI->Seller_dashboards->profile_edit_data();
?>
<!-- Admin header start -->
<header class="main-header"> 
    <a href="<?php echo base_url('seller-dashboard')?>" class="logo">
        <span class="logo-mini">
            <!--<b>A</b>BD-->
            <img src="<?php if (isset($soft_settings[0]['favicon'])) {
               echo $soft_settings[0]['favicon']; }?>" alt="">
        </span>
        <span class="logo-lg">
            <img src="<?php if (isset($soft_settings[0]['logo'])) {
               echo $soft_settings[0]['logo']; }?>" alt="">
        </span>
    </a>
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"> <!-- Sidebar toggle button-->
            <span class="sr-only">Toggle navigation</span>
            <span class="pe-7s-keypad"></span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                <!-- settings -->
                <li class="dropdown dropdown-user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="<?php echo display('setting')?>"> <i class="pe-7s-settings"></i></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url('seller/seller_dashboard/edit_profile')?>"><i class="pe-7s-users"></i><?php echo display('update_profile') ?></a></li>
                        <li><a href="<?php echo base_url('seller/seller_dashboard/change_password_form')?>"><i class="pe-7s-settings"></i><?php echo display('change_password') ?></a></li>
                        <li><a href="<?php echo base_url('seller/seller_dashboard/logout')?>"><i class="pe-7s-key"></i><?php echo display('logout') ?></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>

<aside class="main-sidebar">
	<!-- sidebar -->
	<div class="sidebar">
	    <!-- Sidebar user panel -->
	    <div class="user-panel text-center">
	        <div class="image">
	            <img src="<?php if(!empty($seller->image)){echo $seller->image;}?>" class="img-circle" alt="User Image">
	        </div>
	        <div class="info">
	            <p><?php if($seller->first_name){echo $seller->first_name.' '.$seller->last_name;}?></p>
	            <a href="#"><i class="fa fa-circle text-success"></i> <?php echo display('online') ?></a>
	        </div>
	    </div>
	    <!-- sidebar menu -->
	    <ul class="sidebar-menu">

	        <li class="<?php if ($this->uri->segment('1') == ("")) { echo "active";}else{ echo " ";}?>">
	            <a href="<?php echo base_url('seller-dashboard')?>"><i class="ti-dashboard"></i> <span><?php echo display('dashboard') ?></span>
	                <span class="pull-right-container">
	                    <span class="label label-success pull-right"></span>
	                </span>
	            </a>
	        </li>

            <!-- Product menu start -->
            <li class="treeview <?php if ($this->uri->segment('2') == ("manage_product") || $this->uri->segment('2') == ("upload_product")) { echo "active";}else{ echo " ";}?>">
                <a href="#">
                    <i class="ti-layout-accordion-list"></i><span><?php echo display('product') ?></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('seller_upload_product')?>"><?php echo display('upload_product') ?></a></li>
                    <li><a href="<?php echo base_url('seller/manage_product/all/item')?>"><?php echo display('manage_product') ?></a></li>
                </ul>
            </li>
            <!-- Product menu end -->

            <!-- Order menu start -->
            <li class="treeview <?php if ($this->uri->segment('2') == ("new_order") || $this->uri->segment('2') == ("manage_order")) { echo "active";}else{ echo " ";}?>">
                <a href="#">
                    <i class="ti-truck"></i><span><?php echo display('order') ?></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('seller_manage_order')?>"><?php echo display('manage_order') ?></a></li>
                </ul>
            </li>
            <!-- Order menu end -->            

            <!-- Report menu start -->
            <li class="treeview <?php if ($this->uri->segment('2') == ("new_order") || $this->uri->segment('2') == ("report")) { echo "active";}else{ echo " ";}?>">
                <a href="#">
                    <i class="fa fa-file"></i><span><?php echo display('report') ?></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('seller_report')?>"><?php echo display('report') ?></a></li>
                </ul>
            </li>
            <!-- Report menu end -->

            <!-- Setting menu start -->
            <li class="treeview <?php if ($this->uri->segment('2') == ("setting")) { echo "active";}else{ echo " ";}?>">
                <a href="#">
                    <i class="ti-settings"></i><span><?php echo display('setting') ?></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('seller_setting')?>"><?php echo display('seller_guarantee') ?></a></li>
                </ul>
            </li>
            <!-- Setting menu end -->
	    </ul>
	</div> <!-- /.sidebar -->
</aside>
<!-- Admin header end -->