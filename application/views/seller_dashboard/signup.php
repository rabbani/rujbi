<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<div class="page-breadcrumbs">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('home')?>"><?php echo display('home')?></a></li>
            <li class="active"><?php echo display('sign_up')?></li>
        </ol>
    </div>
</div>
<!-- /.End of page breadcrumbs -->


<div class="form-bg" <?php if ($seller_image) {?> style="background-image: url(<?php echo $seller_image?>);" <?php } ?>>
  <div class="container">
      <div class="row">
          <div class="col-sm-7">
              <div class="Delivery-Steps">
                  <div class="media">
                    <div class="media-left">
                      <a href="#">
                        <img src="<?php echo base_url('assets/website/img/reg.png')?>" alt="Registration">
                      </a>
                    </div>
                    <div class="media-body">
                      <h3><?php echo display('register')?></h3>
                    </div>
                  </div>
                  <div class="media">
                    <div class="media-left">
                      <a href="#">
                        <img src="<?php echo base_url('assets/website/img/list.png')?>" alt="Registration">
                      </a>
                    </div>
                    <div class="media-body">
                      <h3><?php echo display('product_listing')?></h3>
                    </div>
                  </div>
                  <div class="media">
                    <div class="media-left">
                      <a href="#">
                        <img src="<?php echo base_url('assets/website/img/order.png')?>" alt="Registration">
                      </a>
                    </div>
                    <div class="media-body">
                      <h3><?php echo display('please_accept_the_order')?></h3>
                    </div>
                  </div>
                  <div class="media">
                    <div class="media-left">
                      <a href="#">
                        <img src="<?php echo base_url('assets/website/img/delivery.png')?>" alt="Registration">
                      </a>
                    </div>
                    <div class="media-body">
                      <h3><?php echo display('product_delivery')?></h3>
                    </div>
                  </div>
                  <div class="media">
                    <div class="media-left">
                      <a href="#">
                        <img src="<?php echo base_url('assets/website/img/receive.png')?>" alt="Registration">
                      </a>
                    </div>
                    <div class="media-body">
                      <h3><?php echo display('accept_the_price')?></h3>
                    </div>
                  </div>
              </div>
          </div>
          <div class="col-sm-5">
              <div class="seller_form">
                  <div class="form-content">

                      <?php
                          $message = $this->session->userdata('message');
                          if (isset($message)) {
                      ?>
                         <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            <strong><?php echo $message ?></strong>
                          </div>
                      <?php 
                          $this->session->unset_userdata('message');
                          }
                          $error_message = $this->session->userdata('error_message');
                          if (isset($error_message)) {
                      ?>
                        <div class="alert alert-danger alert-dismissible">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                          <strong><?php echo $error_message ?></strong>
                        </div>
                      <?php 
                          $this->session->unset_userdata('error_message');
                          }
                      ?>

                      <h2><?php echo display('seller_signup')?></h2>

                      <!-- <p><?php echo display('choose_one_of_the_following_methods')?></p> -->

                      <form class="login_content signup_content" action="<?php echo base_url('seller/signup/seller_signup')?>" method="post">

                          <!-- <div class="social-btn text-center">
                              <a href="#" class="btn btn-fb"><i class="fa fa-facebook"></i><?php echo display('facebook')?></a>
                              <a href="#" class="btn btn-plush"><i class="fa fa-google-plus"></i><?php echo display('google_plus')?></a>
                          </div>
                          <div class="ui horizontal divider"><?php echo display('or')?> </div> -->

                          <p><?php echo display('sign_up_using_your_email')?></p>


                          <div class="row">
                            <div class="col-md-6">
                               <div class="form-group">
                                  <input class="form-control" name="first_name" id="first_name" placeholder="<?php echo display('first_name')?>" type="text" required>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                  <input class="form-control" name="last_name" id="last_name" placeholder="<?php echo display('last_name')?>" type="text" required>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                  <input class="form-control" name="business_name" id="business_name" placeholder="<?php echo display('business_name')?>" type="text" required>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                  <input class="form-control" name="mobile" id="mobile" placeholder="<?php echo display('mobile')?>" type="text" required>
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                              <input class="form-control" type="email" name="email" placeholder="<?php echo display('email')?>" required>
                          </div>
                          <div class="form-group">
                              <textarea class="form-control" rows="5"  name="address" placeholder="<?php echo display('address')?>" required></textarea>
                          </div>
                          <div class="form-group">
                              <input class="form-control" type="password" name="password" placeholder="<?php echo display('password')?>" required>
                          </div>
                          <button type="submit" class="btn btn-warning btn-block btn-submit"><?php echo display('create_account')?></button>
                      </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
<?php if ($seller_policy) { ?>
<div class="container">
   <div class="row">
        <div class="col-sm-12">
            <?php echo $seller_policy?>
        </div>
    </div>
</div>
<?php } ?>
<!-- /.End of page content -->