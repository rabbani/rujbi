<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Admin Home Start -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="header-icon">
            <i class="pe-7s-world"></i>
        </div>
        <div class="header-title">
            <h1><?php echo display('dashboard')?></h1>
            <small><?php echo display('home')?></small>
            <ol class="breadcrumb">
                <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home')?></a></li>
                <li class="active"><?php echo display('dashboard')?></li>
            </ol>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Alert Message -->
        <?php
            $message = $this->session->userdata('message');
            if (isset($message)) {
        ?>
        <div class="alert alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $message ?>                    
        </div>
        <?php 
            $this->session->unset_userdata('message');
            }
            $error_message = $this->session->userdata('error_message');
            if (isset($error_message)) {
        ?>
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $error_message ?>                    
        </div>
        <?php 
            $this->session->unset_userdata('error_message');
            }
        ?>
        <!-- First Counter -->

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
                <div class="panel panel-bd" style="height:100px">
                    <div class="panel-body">
                        <div class="statistic-box text-center">
                            <h2><span class="count-number"><?php echo $lifetime_orders?></span> <span class="slight"> </span></h2>
                            <div style="font-size:15px"><?php echo display('lifetime_orders')?></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
                <div class="panel panel-bd" style="height:100px">
                    <div class="panel-body">
                        <div class="statistic-box text-center">
                            <h2><?php echo (($position==0)?"$currency <span class=\"count-number\">".$lifetime_orders_sell."</span>":"<span class=\"count-number\">".$lifetime_orders_sell."</span> $currency") ?></h2>
                            <div style="font-size:15px"><?php echo display('order_total')?></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
                <div class="panel panel-bd" style="height:100px">
                    <div class="panel-body">
                        <div class="statistic-box text-center">
                            <h2><span class="count-number"><?php echo $total_customer?></span> <span class="slight"> </span></h2>
                            <div style="font-size:15px"><?php echo display('total_customer')?></div>
                        </div>
                    </div>
                </div>
            </div>
           
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
                <div class="panel panel-bd" style="height:100px">
                    <div class="panel-body">
                        <div class="statistic-box text-center">
                            <h2><?php echo (($position==0)?"$currency <span class=\"count-number\">".$order_delivered_sell."</span>":"<span class=\"count-number\">".$order_delivered_sell."</span> $currency") ?></h2>
                            <div style="font-size:15px"><?php echo display('total_sale')?></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
                <div class="panel panel-bd" style="height:100px">
                    <div class="panel-body">
                        <div class="statistic-box text-center">
                            <h2><span class="count-number"><?php echo $total_product?></span> <span class="slight"> </span></h2>
                            <div style="font-size:15px"><?php echo display('products')?></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
                <div class="panel panel-bd" style="height:100px">
                    <div class="panel-body">
                        <div class="statistic-box text-center">
                            <h2><span class="count-number"><?php echo $total_seller?></span> <span class="slight"> </span></h2>
                            <div style="font-size:15px"><?php echo display('merchants')?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <!-- Bar Chart -->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-bd lobidisable">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4><?php echo display('order_status')?></h4>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs pull-right order_status" style="margin-top: -35px">
                                <li class="active"><a href="#tab1" data-toggle="tab"><?php echo display('monthly')?></a></li>
                                <li><a href="#tab2" data-toggle="tab"><?php echo display('yearly')?></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel-body">
                        <!-- Tab panels -->
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="tab1">
                                <div class="panel-body">
                                    <canvas id="barChart" height="110"></canvas>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab2">
                                <div class="panel-body">
                                    <canvas id="barChart2" height="110"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- Line Chart -->
            <div class="col-sm-12 col-md-8">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4><?php echo display('invoice_order')?></h4>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs pull-right order_status" style="margin-top: -35px">
                                <li class="active"><a href="#tab3" data-toggle="tab"><?php echo display('monthly')?></a></li>
                                <li><a href="#tab4" data-toggle="tab"><?php echo display('yearly')?></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel-body">

                        <!-- Tab panels -->
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="tab3">
                                <div class="panel-body">
                                    <canvas id="invoiceMonthly" height="150"></canvas>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab4">
                                <div class="panel-body">
                                    <canvas id="lineChart" height="150"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- //Pending order -->
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
                <div class="panel panel-bd lobidisable">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4><?php echo display('pending_order')?></h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="message_inner">
                            <div class="message_widgets">
                                <?php
                                if ($pending_order) {
                                    foreach ($pending_order as $order) {
                                ?>
                                    <div class="inbox-item">
                                        <strong class="inbox-item-author"><?php echo display('order_no')?> : <a href="<?php echo base_url('corder/order_details_data/').$order['order_id']?>">(<?php echo $order['order_no']?>)</a></strong>
                                        <span class="inbox-item-date">-<?php echo $order['date']?></span>
                                        <p class="inbox-item-text"><?php echo $order['city']?></p>
                                        <span class="profile-status away pull-right"></span>
                                    </div>
                                <?php 
                                    }
                                } 
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <!-- Pie Chart -->
            <div class="col-sm-12 col-md-8">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4><?php echo display('shipping_overview_distric_wise')?></h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive" style="height: 305px">
                            <table id="" class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th><?php echo display('sl') ?></th>
                                        <th><?php echo display('district') ?></th>
                                        <th><?php echo display('customer_no') ?></th>
                                        <th><?php echo display('percentage') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i=1;
                                    if ($state_list) {
                                        foreach ($state_list as $state) {
                                    ?>
                                    <tr>
                                        <td><?php echo $i?></td>
                                        <td><?php echo $state->name?></td>
                                        <td><?php  echo $this->Orders->get_total_customer($state->name);
                                        ?></td>
                                        <td><?php  echo $this->Orders->get_customer_prcentage($state->name);
                                        ?> %</td>
                                    </tr>
                                    <?php
                                        $i++;
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


            <!-- Shipping status -->
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
                
                <div class="panel panel-bd lobidisable">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4><?php echo display('shipping_overview')?></h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div id="ship_status" style="height: 300px;"></div>
                    </div>
                </div>     
            </div>
        </div>
    </section> <!-- /.content -->
</div> <!-- /.content-wrapper -->
<!-- Admin Home end -->
 
<!-- ChartJs JavaScript -->
<script src="<?php echo base_url()?>assets/plugins/chartJs/Chart.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/plugins/google-chart/loader.js" type="text/javascript"></script>
<script type="text/javascript">
    //bar chart 1
    var ctx = document.getElementById("barChart");
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [<?php
                    for ($i=1; $i <= 31; $i++) {
                      echo '"'.$i.'",';
                    }
                ?>],
            datasets: [
                {
                    label: "<?php echo display('delivered')?>",
                    data: [<?php
                            for ($i=1; $i <= 31; $i++) {
                               $delivered = $this->Orders->monthly_order_deliver($i,4);
                               if (!empty($delivered->total_order)) {
                                    echo $delivered->total_order.",";
                               }else{
                                echo ",";
                               }
                            }
                        ?>],
                    borderColor: "rgba(55, 160, 0, 0.9)",
                    borderWidth: "0",
                    backgroundColor: "rgba(55,160,0,0.9)"
                },
                {
                    label: "<?php echo display('cancel')?>",
                    data: [<?php
                            for ($i=1; $i <= 31; $i++) {
                               $delivered = $this->Orders->monthly_order_deliver($i,6);
                               if (!empty($delivered->total_order)) {
                                    echo $delivered->total_order.",";
                               }else{
                                echo ",";
                               }
                            }
                        ?>],
                    borderColor: "rgba(0,0,0,0.09)",
                    borderWidth: "0",
                    backgroundColor: "rgb(242, 14, 94,0.7)"
                },
                {
                    label: "<?php echo display('return')?>",
                    data: [<?php
                            for ($i=1; $i <= 31; $i++) {
                               $delivered = $this->Orders->monthly_order_deliver($i,5);
                               if (!empty($delivered->total_order)) {
                                    echo $delivered->total_order.",";
                               }else{
                                echo ",";
                               }
                            }
                        ?>],
                    borderColor: "rgba(0,0,0,0.09)",
                    borderWidth: "0",
                    backgroundColor: "rgb(253, 197, 14,0.7)"
                }
            ]
        },
        options: {
            scales: {
                yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
            }
        }
    });

    //bar chart 2
    var ctx = document.getElementById("barChart2");
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [<?php
                    for ($i=1; $i <= 12; $i++) {
                        if ($i==1) {
                            echo '"January",';
                        }elseif ($i==2) {
                            echo '"February",';
                        }elseif ($i==3) {
                            echo '"March",';
                        }elseif ($i==4) {
                            echo '"April",';
                        }elseif ($i==5) {
                            echo '"May",';
                        }elseif ($i==6) {
                           echo '"June",';
                        }elseif ($i==7) {
                           echo '"July",';
                        }elseif ($i==8) {
                           echo '"August",';
                        }elseif ($i==9) {
                           echo '"September",';
                        }elseif ($i==10) {
                           echo '"October",';
                        }elseif ($i==11) {
                           echo '"November",';
                        }elseif ($i==12) {
                           echo '"December"';
                        }
                    }
                ?>],
            datasets: [
                {
                    label: "<?php echo display('delivered')?>",
                    data: [<?php
                            for ($i=1; $i <= 12; $i++) {
                               $delivered = $this->Orders->yearly_order_status($i,4);
                               if (!empty($delivered->total_order)) {
                                    echo $delivered->total_order.",";
                               }else{
                                echo ",";
                               }
                            }
                        ?>],
                    borderColor: "rgba(55, 160, 0, 0.9)",
                    borderWidth: "0",
                    backgroundColor: "rgba(55,160,0,0.9)"
                },
                {
                    label: "<?php echo display('cancel')?>",
                    data: [<?php
                            for ($i=1; $i <= 12; $i++) {
                               $delivered = $this->Orders->yearly_order_status($i,5);
                               if (!empty($delivered->total_order)) {
                                    echo $delivered->total_order.",";
                               }else{
                                echo ",";
                               }
                            }
                        ?>],
                    borderColor: "rgba(0,0,0,0.09)",
                    borderWidth: "0",
                    backgroundColor: "rgb(242, 14, 94,0.7)"
                },
                {
                    label: "<?php echo display('return')?>",
                    data: [<?php
                            for ($i=1; $i <= 12; $i++) {
                               $delivered = $this->Orders->yearly_order_status($i,6);
                               if (!empty($delivered->total_order)) {
                                    echo $delivered->total_order.",";
                               }else{
                                echo ",";
                               }
                            }
                        ?>],
                    borderColor: "rgba(0,0,0,0.09)",
                    borderWidth: "0",
                    backgroundColor: "rgb(253, 197, 14,0.7)"
                }
            ]
        },
        options: {
            scales: {
                yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
            }
        }
    });

    //total invoice monthly
    var ctx = document.getElementById("invoiceMonthly");
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [<?php
                    for ($i=1; $i <= 31; $i++) {
                      echo '"'.$i.'",';
                    }
                ?>],
            datasets: [
                {
                    label: "<?php echo display('total_invoice')?>",
                    data: [<?php
                            for ($i=1; $i <= 31; $i++) {
                               $delivered = $this->Orders->monthly_total_invoice($i);
                               if (!empty($delivered->total_invoice)) {
                                    echo $delivered->total_invoice.",";
                               }else{
                                echo ",";
                               }
                            }
                        ?>],
                    borderColor: "rgba(55, 160, 0, 0.9)",
                    borderWidth: "0",
                    backgroundColor: "rgba(55,160,0,0.9)"
                }
            ]
        },
        options: {
            scales: {
                yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
            }
        }
    });

    //line chart for invoice order
    var ctx = document.getElementById("lineChart");
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: [<?php
                    for ($i=1; $i <= 12; $i++) {
                        if ($i==1) {
                            echo '"January",';
                        }elseif ($i==2) {
                            echo '"February",';
                        }elseif ($i==3) {
                            echo '"March",';
                        }elseif ($i==4) {
                            echo '"April",';
                        }elseif ($i==5) {
                            echo '"May",';
                        }elseif ($i==6) {
                           echo '"June",';
                        }elseif ($i==7) {
                           echo '"July",';
                        }elseif ($i==8) {
                           echo '"August",';
                        }elseif ($i==9) {
                           echo '"September",';
                        }elseif ($i==10) {
                           echo '"October",';
                        }elseif ($i==11) {
                           echo '"November",';
                        }elseif ($i==12) {
                           echo '"December"';
                        }
                    }
                ?>],
            datasets: [
                {
                    label: "<?php echo display('invoice_order')?>",
                    borderColor: "rgba(55, 160, 0, 0.9)",
                    borderWidth: "1",
                    backgroundColor: "rgba(55, 160, 0, 0.5)",
                    pointHighlightStroke: "rgba(26,179,148,1)",
                    data: [<?php
                            for ($i=1; $i <= 12; $i++) {
                               $delivered = $this->Orders->yearly_order_invoice($i);
                               if (!empty($delivered->total_invoice)) {
                                    echo $delivered->total_invoice.",";
                               }else{
                                echo ",";
                               }
                            }
                        ?>]
                }
            ]
        },
        options: {
            responsive: true,
            tooltips: {
                mode: 'index',
                intersect: false
            },
            hover: {
                mode: 'nearest',
                intersect: true
            }

        }
    });
</script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
                ['Task', 'Hours per Day'],
            <?php
                if ($get_percentage_of_shipping) {
                    foreach ($get_percentage_of_shipping as $shipping) {
            ?>
                ['<?php echo $shipping->method_name?>', <?php echo $shipping->total_order?>],
            <?php
                    }
                }
           ?>

        ]);

        var options = {
          title: '<?php echo display('shipping_overview')?>',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('ship_status'));

        chart.draw(data, options);
    }
</script>