<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php
    $CI =& get_instance();
    $CI->load->model('Soft_settings');
    $CI->load->model('Reports');
    $CI->load->model('Users');
    $CI->load->model('Orders');
    $CI->load->model('Sellers');
    $soft_settings = $CI->Soft_settings->retrieve_setting_editdata();
    $users         = $CI->Users->profile_edit_data();
    $new_order     = $CI->Orders->new_order_count();
    $new_order_tracking     = $CI->Orders->new_order_tracking_count();
    $new_pre_order = $CI->Orders->new_pre_order_count();
    $total_pending_seller = $CI->Sellers->total_pending_seller();
?>
<!-- Admin header end -->
<header class="main-header"> 
    <a href="<?php echo base_url('dashboard')?>" class="logo"> <!-- Logo -->
        <span class="logo-mini">
            <!--<b>A</b>BD-->
            <img src="<?php if (isset($soft_settings[0]['favicon'])) {
               echo $soft_settings[0]['favicon']; }?>" alt="">
        </span>
        <span class="logo-lg">
            <!--<b>Admin</b>BD-->
            <img src="<?php if (isset($soft_settings[0]['logo'])) {
               echo $soft_settings[0]['logo']; }?>" alt="">
        </span>
    </a>
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"> <!-- Sidebar toggle button-->
            <span class="sr-only">Toggle navigation</span>
            <span class="pe-7s-keypad"></span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <?php if ($this->permission->module('manage_order')->access()) { ?>
                <!-- Order Notification -->
                <li class="dropdown messages-menu">
                    <a href="<?php echo base_url('corder/manage_order/all/item')?>" aria-expanded="false" title="<?php echo display('pending_order')?>">
                        <i class="pe-7s-cart"></i>
                        <span class="label label-warning"><?php echo $new_order?></span>
                    </a>
                </li>
                <?php }if ($this->permission->module('manage_seller')->access()) { ?>
                <!-- Seller Notification -->
                <li class="dropdown messages-menu">
                    <a href="<?php echo base_url('manage_seller')?>" aria-expanded="false" title="<?php echo display('pending_seller')?>">
                        <i class="pe-7s-user"></i>
                        <span class="label label-warning"><?php echo $total_pending_seller?></span>
                    </a>
                </li>
                <?php } ?>
                <!-- settings -->
                <li class="dropdown dropdown-user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="pe-7s-settings"></i></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url('edit_profile')?>"><i class="pe-7s-users"></i><?php echo display('user_profile') ?></a></li>
                        <li><a href="<?php echo base_url('change_password_form')?>"><i class="pe-7s-settings"></i><?php echo display('change_password') ?></a></li>
                        <li><a href="<?php echo base_url('dashboard_logout')?>"><i class="pe-7s-key"></i><?php echo display('logout') ?></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>

<aside class="main-sidebar">
	<!-- sidebar -->
	<div class="sidebar">
	    <!-- Sidebar user panel -->
	    <div class="user-panel text-center">
	        <div class="image">
	            <img src="<?php echo $users[0]['logo']?>" class="img-circle" alt="User Image">
	        </div>
	        <div class="info">
	            <p><?php echo $users[0]['first_name']." ".$users[0]['last_name']?></p>
	            <a href="#"><i class="fa fa-circle text-success"></i> <?php echo display('online') ?></a>
	        </div>
	    </div>
	    <!-- sidebar menu -->
	    <ul class="sidebar-menu">

	        <li class="<?php if ($this->uri->segment('1') == ("")) { echo "active";}else{ echo " ";}?>">
	            <a href="<?php echo base_url('dashboard')?>"><i class="ti-dashboard"></i> <span><?php echo display('dashboard') ?></span>
	                <span class="pull-right-container">
	                    <span class="label label-success pull-right"></span>
	                </span>
	            </a>
	        </li>

            <?php
            if ($this->permission->module('invoice')->access()) {
            ?>
            <!-- Invoice menu start -->
            <li class="treeview <?php if ($this->uri->segment('1') == ("cinvoice")) { echo "active";}else{ echo " ";}?>">
                <a href="#">
                    <i class="ti-layout-accordion-list"></i><span><?php echo display('invoice') ?></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <?php if($this->permission->check_label('new_invoice')->create()->access()){?>
                    <li><a href="<?php echo base_url('new_invoice')?>"><?php echo display('new_invoice') ?></a></li>
                    <?php } ?>
                    <?php if($this->permission->check_label('manage_invoice')->access()){?>
                    <li><a href="<?php echo base_url('cinvoice/manage_invoice/all/item')?>"><?php echo display('manage_invoice') ?></a></li>
                    <?php } ?>
                </ul>
            </li>
            <!-- Invoice menu end -->
            <?php
            }
            if ($this->permission->module('order')->access()) {
            ?>
            <!-- Order menu start -->
            <li class="treeview <?php if ($this->uri->segment('2') == ("new_order") || $this->uri->segment('2') == ("manage_order")) { echo "active";}else{ echo " ";}?>">
                <?php if($this->permission->check_label('manage_order')->access()){?>
                <a href="#">
                    <i class="ti-truck"></i><span><?php echo display('order') ?></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                        <?php
                        if ($new_order > 0) {
                        ?>
                        <small class="label pull-right bg-yellow"><?php echo $new_order?></small>
                        <?php
                        }
                        ?>
                    </span>
                </a>
                <?php } ?>
                <ul class="treeview-menu">
                    <?php if($this->permission->check_label('new_order')->create()->access()){?>
                    <li><a href="<?php echo base_url('new_order')?>"><?php echo display('new_order') ?></a></li>
                    <?php } if($this->permission->check_label('manage_order')->access()){?>
                    <li><a href="<?php echo base_url('corder/manage_order/all/item')?>"><?php echo display('manage_order') ?></a></li>
                    <?php } ?>
                </ul>
            </li>
            <!-- Order menu end -->
            <?php
            }if ($this->permission->check_label('pre_order')->access()) {
            ?>
            <!-- Pre-order menu start -->
            <li class="treeview <?php if ($this->uri->segment('2') == ("manage_pre_order") || $this->uri->segment('2') == ("new_pre_order")) { echo "active";}else{ echo " ";}?>">
                <?php if ($this->permission->check_label('manage_pre_order')->access()) { ?>
                <a href="#">
                    <i class="ti-truck"></i><span><?php echo display('pre_order') ?></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                        <?php if ($new_pre_order > 0) { ?>
                        <small class="label pull-right bg-yellow"><?php echo $new_pre_order?></small>
                        <?php } ?>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('corder/manage_pre_order/all/item')?>"><?php echo display('manage_pre_order') ?></a></li>
                </ul>
                <?php } ?>
            </li>
            <!--Pre-order menu end -->     
            <?php
            }if ($this->permission->module('stock_management')->access()) {
            ?>
            <!-- Stock menu start -->
            <li class="treeview <?php if ($this->uri->segment('2') == ("stock")) { echo "active";}else{ echo " ";}?>">
                <a href="<?php echo base_url('stock')?>">
                    <i class="ti-truck"></i><span><?php echo display('stock_management') ?></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
            </li>
            <!--Stock menu end -->
            <?php
            }if ($this->permission->module('order_tracking')->access()) {
            ?>
            <!-- Order tracking menu start -->
            <li class="treeview <?php if ($this->uri->segment('2') == ("order_tracking")) { echo "active";}else{ echo " ";}?>">
                <a href="<?php echo base_url('corder_tracking')?>">
                    <i class="ti-truck"></i><span><?php echo display('order_tracking') ?></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
            </li>
            <!-- Order tracking menu end -->
            <?php
            }if ($this->permission->module('product')->access()) {
            ?>
            <!-- Product menu start -->
            <li class="treeview <?php if ($this->uri->segment('1') == ("cproduct")) { echo "active";}else{ echo " ";}?>">
                <a href="#">
                    <i class="ti-bag"></i><span><?php echo display('product') ?></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu">
                    <?php if ($this->permission->check_label('product')->create()->access()) { ?>
                    <li><a href="<?php echo base_url('cproduct')?>"><?php echo display('add_product') ?></a></li>
                    <?php } if ($this->permission->check_label('manage_product')->access()) { ?>
                    <li><a href="<?php echo base_url('cproduct/manage_product/all/item')?>"><?php echo display('manage_product') ?></a></li>
                    <?php } if ($this->permission->check_label('pending_product')->access()) { ?>
                    <li><a href="<?php echo base_url('cproduct/pending_product/all/item')?>"><?php echo display('pending_product') ?></a></li>
                    <?php } if ($this->permission->check_label('denied_product')->access()) {?>
                    <li><a href="<?php echo base_url('cproduct/denied_product/all/item')?>"><?php echo display('denied_product') ?></a></li>
                    <?php } ?>
                </ul>
            </li>
            <!-- Product menu end -->
            <?php
            }if ($this->permission->check_label('customer')->access()) {
            ?>
            <!-- Customer menu start -->
            <li class="treeview <?php if ($this->uri->segment('1') == ("ccustomer")) { echo "active";}else{ echo " ";}?>">
                <a href="#">
                    <i class="fa fa-handshake-o"></i><span><?php echo display('customer') ?></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <?php if ($this->permission->check_label('add_customer')->create()->access()) { ?>
                    <li><a href="<?php echo base_url('ccustomer')?>"><?php echo display('add_customer') ?></a></li>
                    <?php }if ($this->permission->check_label('manage_customer')->access()) { ?>
                    <li><a href="<?php echo base_url('manage_customer')?>"><?php echo display('manage_customer') ?></a></li>
                    <?php }if ($this->permission->check_label('customer_details')->read()->access()) { ?>
                    <li><a href="<?php echo base_url('customer_details')?>"><?php echo display('customer_details') ?></a></li>
                    <?php } ?>
                </ul>
            </li>
            <!-- Customer menu end --> 
            <?php
            }if ($this->permission->check_label('seller')->access()) {
            ?>
            <!-- Seller menu start -->
            <li class="treeview <?php if ($this->uri->segment('1') == ("cseller")) { echo "active";}else{ echo " ";}?>">
                <a href="#">
                    <i class="fa fa-handshake-o"></i><span><?php echo display('seller') ?></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <?php if ($this->permission->check_label('add_seller')->create()->access()) {?>
                    <li><a href="<?php echo base_url('cseller')?>"><?php echo display('add_seller') ?></a></li>
                    <?php }if ($this->permission->check_label('manage_seller')->access()) { ?>
                    <li><a href="<?php echo base_url('cseller/manage_seller/all/item')?>"><?php echo display('manage_seller') ?></a></li>
                    <?php }if ($this->permission->check_label('seller_policy')->read()->access()) { ?>
                   <li><a href="<?php echo base_url('seller_policy')?>"><?php echo display('seller_policy') ?></a></li>
                    <?php } ?>
                </ul>
            </li>
            <!-- Seller menu end --> 
            <?php }if ($this->permission->check_label('category')->access()) { ?>
            <!-- Category menu start -->
            <li class="treeview <?php if ($this->uri->segment('1') == ("ccategory")) { echo "active";}else{ echo " ";}?>">
                <a href="#">
                    <i class="ti-tag"></i><span><?php echo display('category') ?></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <?php if ($this->permission->check_label('add_category')->create()->access()) { ?>
                    <li><a href="<?php echo base_url('ccategory')?>"><?php echo display('add_category') ?></a></li>
                    <?php
                    }if ($this->permission->check_label('manage_category')->access()) {
                    ?>
                    <li><a href="<?php echo base_url('manage_category')?>"><?php echo display('manage_category') ?></a></li>
                    <?php } ?>
                </ul>
            </li>
            <!-- Category menu end -->
            <?php }if ($this->permission->check_label('brand')->access()) { ?>
            <!-- Brand menu start -->
            <li class="treeview <?php if ($this->uri->segment('1') == ("cbrand")) { echo "active";}else{ echo " ";}?>">
                <a href="#">
                    <i class="ti-apple"></i><span><?php echo display('brand') ?></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <?php if ($this->permission->check_label('add_brand')->create()->access()) { ?>
                    <li><a href="<?php echo base_url('cbrand')?>"><?php echo display('add_brand') ?></a></li>
                    <?php }if ($this->permission->check_label('manage_brand')->access()) { ?>
                    <li><a href="<?php echo base_url('manage_brand')?>"><?php echo display('manage_brand') ?></a></li>
                    <?php } ?>
                </ul>
            </li>
            <!-- Brand menu end -->   
            <?php }if ($this->permission->check_label('variant')->access()) { ?>
            <!-- Variant menu start -->
            <li class="treeview <?php if ($this->uri->segment('1') == ("cvariant")) { echo "active";}else{ echo " ";}?>">
                <a href="#">
                    <i class="ti-wand"></i><span><?php echo display('variant') ?></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <?php if ($this->permission->check_label('add_variant')->create()->access()) { ?>
                    <li><a href="<?php echo base_url('cvariant')?>"><?php echo display('add_variant') ?></a></li>
                    <?php }if ($this->permission->check_label('manage_variant')->access()) { ?>
                    <li><a href="<?php echo base_url('manage_variant')?>"><?php echo display('manage_variant') ?></a></li>
                    <?php } ?>
                </ul>
            </li>
            <!-- Variant menu end -->
            <?php }if ($this->permission->check_label('unit')->access()) { ?>
            <!-- Unit menu start -->
            <li class="treeview <?php if ($this->uri->segment('1') == ("cunit")) { echo "active";}else{ echo " ";}?>">
                <a href="#">
                    <i class="ti-ruler-pencil"></i><span><?php echo display('unit') ?></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <?php if ($this->permission->check_label('add_unit')->create()->access()) { ?>
                    <li><a href="<?php echo base_url('cunit')?>"><?php echo display('add_unit') ?></a></li>
                    <?php }if ($this->permission->check_label('manage_unit')->access()) { ?>
                    <li><a href="<?php echo base_url('manage_unit')?>"><?php echo display('manage_unit') ?></a></li>
                    <?php } ?>
                </ul>
            </li>
            <!-- Unit menu end -->
            <?php }if ($this->permission->check_label('comission')->access()) { ?>
            <!-- Comission start-->
            <li class="treeview <?php if ($this->uri->segment('1') == ("ccomission")) { echo "active";}else{ echo " ";}?>">
                <a href="#">
                    <i class="ti-bag"></i><span><?php echo display('comission') ?></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <?php if ($this->permission->check_label('add_comission')->create()->access()) { ?>
                    <li><a href="<?php echo base_url('ccomission')?>"><?php echo display('add_comission') ?></a></li>
                    <?php }if ($this->permission->check_label('manage_comission')->access()) { ?>
                    <li><a href="<?php echo base_url('manage_comission')?>"><?php echo display('manage_comission') ?></a></li>
                    <?php }if ($this->permission->check_label('comission_report')->read()->access()) { ?>
                    <li><a href="<?php echo base_url('comission_report')?>"><?php echo display('comission_report') ?></a></li>
                    <?php } ?>
                </ul>
            </li>
            <!-- Comission end-->
            <?php }if ($this->permission->check_label('currency')->access()) { ?>
            <!-- Currency menu start -->
            <li class="treeview <?php if ($this->uri->segment('1') == ("ccurrency")) { echo "active";}else{ echo " ";}?>">
                <a href="#">
                    <i class="ti-money"></i><span><?php echo display('currency') ?></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <?php if ($this->permission->check_label('manage_comission')->create()->access()) { ?>
                    <li><a href="<?php echo base_url('ccurrency')?>"><?php echo display('add_currency') ?></a></li>
                    <?php }if ($this->permission->check_label('manage_currency')->access()) { ?>
                    <li><a href="<?php echo base_url('manage_currency')?>"><?php echo display('manage_currency') ?></a></li>
                    <?php } ?>
                </ul>
            </li>
            <!-- Currency menu end -->    
            <?php }if ($this->permission->check_label('email_template')->access()) {  ?>
            <!-- Email template menu start -->
            <li class="treeview <?php if ($this->uri->segment('1') == ("cemail_template")) { echo "active";}else{ echo " ";}?>">
                <a href="#">
                    <i class="ti-email"></i><span><?php echo display('email_template') ?></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <?php if ($this->permission->check_label('add_template')->create()->access()) { ?>
                    <li><a href="<?php echo base_url('cemail_template')?>"><?php echo display('add_template') ?></a></li>
                    <?php }if ($this->permission->check_label('manage_template')->access()) { ?>
                    <li><a href="<?php echo base_url('manage_template')?>"><?php echo display('manage_template') ?></a></li>
                    <?php } ?>
                </ul>
            </li>
            <!-- Email template menu end -->
            <?php }if ($this->permission->check_label('sales_report')->access()) { ?>
            <!-- Report menu start -->
            <li class="treeview <?php if ($this->uri->segment('2') == ("sales_report") || $this->uri->segment('2') == ("sales_details_report") || $this->uri->segment('2') == ("merchant_sell_report")) { echo "active";}else{ echo " ";}?>">
                <a href="#">
                    <i class="ti-book"></i><span><?php echo display('sales_report') ?></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <?php if ($this->permission->check_label('sales_overview_report')->read()->access()) { ?>
                    <li><a href="<?php echo base_url('sales_report')?>"><?php echo display('sales_overview_report') ?></a></li>
                    <?php }if ($this->permission->check_label('sales_report')->read()->access()) { ?>
                    <li><a href="<?php echo base_url('creport/sales_details_report/all/item')?>"><?php echo display('sales_details_report') ?></a></li>
                        <li><a href="<?php echo base_url('creport/order_details_report/all/item')?>"><?php echo display('order_details_report') ?></a></li>
                    <?php }if ($this->permission->check_label('merchant_sell_report_in_details')->read()->access()) { ?>
                    <li><a href="<?php echo base_url('creport/merchant_sell_report/all/item')?>"><?php echo display('merchant_sell_report_in_details') ?></a></li>
                    <?php } ?>
                </ul>
            </li>
            <!-- Report menu end -->
            <?php }if ($this->permission->check_label('accounts')->access()) { ?>
            <!-- Accounts menu start -->
            <li class="treeview <?php if ($this->uri->segment('1') == ("caccounts")) { echo "active";}else{ echo " ";}?>">
                <a href="#">
                    <i class="ti-pencil-alt"></i><span><?php echo display('accounts') ?></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <?php if ($this->permission->check_label('add_payment')->create()->access()) { ?>
                    <li><a href="<?php echo base_url('add_payment')?>"><?php echo display('payment') ?></a></li>
                    <?php }if ($this->permission->check_label('manage_payment')->access()) { ?>
                    <li><a href="<?php echo base_url('caccounts/manage_payment/all/item')?>"><?php echo display('manage_payment') ?></a></li>
                    <?php }if ($this->permission->check_label('payment_report')->read()->access()) { ?>
                    <li><a href="<?php echo base_url('caccounts/payment_report/all/item')?>"><?php echo display('payment_report') ?></a></li>
                    <?php } if ($this->permission->check_label('paid')->read()->access()) { ?>
                        <li><a href="<?php echo base_url('caccounts/paid_seller_report/all/item')?>"><?php echo display('paid') ?></a></li>
                    <?php }if ($this->permission->check_label('unpaid')->read()->access()) { ?>
                        <li><a href="<?php echo base_url('caccounts/unpaid_seller_report/all/item')?>"><?php echo display('unpaid') ?></a></li>
                    <?php } ?>
                </ul>
            </li>
            <!-- Accounts menu end -->
            <?php }if ($this->permission->check_label('data_synchronizer')->access()) { ?>
            <!-- Synchronizer setting start -->
            <li class="treeview <?php if ($this->uri->segment('2') == ("form") || $this->uri->segment('2') == ("synchronize") || $this->uri->segment('1') == ("backup_restore")) { echo "active";}else{ echo " ";}?>">
                <a href="#">
                    <i class="ti-reload"></i><span><?php echo display('data_synchronizer') ?></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <?php if ($this->permission->check_label('setting')->update()->access()) { ?>
                    <?php 
                        $localhost=false;
                        if (in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1', '::1', 'localhost'))) {
                    ?>
                    <li><a href="<?php echo base_url('data_synchronizer_form')?>"><?php echo display('setting') ?></a></li>
                    <?php
                        } 
                    ?>
                    <?php }if ($this->permission->check_label('synchronize')->update()->access()) { ?>
                    <li><a href="<?php echo base_url('synchronize')?>"><?php echo display('synchronize') ?></a></li>
                    <?php }if ($this->permission->check_label('backup_and_restore')->access()) { ?>
                    <li><a href="<?php echo base_url('backup_restore/')?>"><?php echo display('backup_and_restore') ?></a></li>
                    <?php } ?>
                </ul>
            </li>
            <!-- Synchronizer setting end -->
            <?php }if ($this->permission->check_label('web_settings')->access()) { ?>
            <!-- Website Settings menu start -->
            <li class="treeview <?php if ($this->uri->segment('1') == ("cblock") || $this->uri->segment('1') == ("cweb_setting") || $this->uri->segment('1') == ("cblock") || $this->uri->segment('1') == ("cproduct_review") || $this->uri->segment('1') == ("csubscriber") || $this->uri->segment('1') == ("cwishlist") || $this->uri->segment('1') == ("cweb_footer") || $this->uri->segment('1') == ("clink_page") || $this->uri->segment('1') == ("ccoupon") || $this->uri->segment('1') == ("cabout_us") || $this->uri->segment('1') == ("cour_location") || $this->uri->segment('1') == ("cshipping_method") || $this->uri->segment('1') == ("cpayment_method")) { echo "active";}else{ echo " ";}?>">
                <a href="#">
                    <i class="ti-settings"></i><span><?php echo display('web_settings') ?></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('add_slider')?>"><?php echo display('slider') ?></a></li>
                    <li><a href="<?php echo base_url('cweb_setting_submit_add')?>"><?php echo display('advertisement') ?> </a></li>
                    
                    <li><a href="<?php echo base_url('cblock')?>"><?php echo display('block') ?> </a></li>
                    
                    <li><a href="<?php echo base_url('cproduct_review')?>"><?php echo display('product_review') ?> </a></li>
                    
                    <li><a href="<?php echo base_url('csubscriber')?>"><?php echo display('subscriber') ?> </a></li>
                    
                    <li><a href="<?php echo base_url('cwishlist')?>"><?php echo display('wishlist') ?> </a></li>
                    
                    <li><a href="<?php echo base_url('cweb_footer')?>"><?php echo display('web_footer') ?> </a></li>
                    
                    <li><a href="<?php echo base_url('clink_page')?>"><?php echo display('link_page') ?> </a></li>
                    
                    <li><a href="<?php echo base_url('ccoupon')?>"><?php echo display('coupon') ?> </a></li>
                    
                    <li><a href="<?php echo base_url('manage_contact_form')?>"><?php echo display('contact_form') ?> </a></li>

                    <li><a href="<?php echo base_url('cabout_us')?>"><?php echo display('about_us') ?> </a></li>

                    <li><a href="<?php echo base_url('cour_location')?>"><?php echo display('our_location') ?> </a></li>

                    <li><a href="<?php echo base_url('cshipping_method')?>"><?php echo display('shipping_method') ?> </a></li>
                    <li><a href="<?php echo base_url('cshipping_agent')?>"><?php echo display('shipping_agent') ?>
                        </a></li>

                    <!-- <li><a href="<?php echo base_url('cpayment_method')?>"><?php echo display('payment_method') ?> </a></li> -->
                    
                    <li><a href="<?php echo base_url('setting')?>"><?php echo display('setting') ?> </a></li>
                </ul>
            </li>
            <!-- Website Settings menu end -->
            <?php }if ($this->permission->check_label('software_settings')->access()) { ?>
            <!-- Software Settings menu start -->
            <li class="treeview <?php if ($this->uri->segment('1') == ("company_setup") || $this->uri->segment('1') == ("user") || $this->uri->segment('1') == ("csoft_setting") || $this->uri->segment('1') == ("language") ) { echo "active";}else{ echo " ";}?>">
                <a href="#">
                    <i class="ti-settings"></i><span><?php echo display('software_settings') ?></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('company_setup_company_update_form')?>"><?php echo display('manage_company') ?></a></li>
                    <li><a href="<?php echo base_url('user')?>"><?php echo display('add_user') ?></a></li>
                    <li><a href="<?php echo base_url('manage_user')?>"><?php echo display('manage_users') ?> </a></li>
                    <li><a href="<?php echo base_url('language')?>"><?php echo display('language') ?> </a></li>
                    <li><a href="<?php echo base_url('email_configuration')?>"><?php echo display('email_configuration') ?> </a></li>
                    <li><a href="<?php echo base_url('payment_gateway_setting')?>"><?php echo display('payment_gateway_setting') ?> </a></li>
                    <li><a href="<?php echo base_url('social_login_setting')?>"><?php echo display('social_setting') ?> </a></li>
                    <li><a href="<?php echo base_url('csoft_setting')?>"><?php echo display('setting') ?> </a></li>

                    <!-- User role assign here start-->
                    <li class="treeview <?php if(($this->uri->segment(1)=="role") || ($this->uri->segment(1)=="permission_setup")){echo "active";}?>">
                        <a href="#">
                            <span><?php echo display('role')?></span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?php echo base_url('create_system_role') ?>"><?php echo display('add_role')?></a></li>
                            <li><a href="<?php echo base_url('role_list') ?>"><?php echo display('role_list')?></a></li>
                            <li><a href="<?php echo base_url('user_access_role') ?>"><?php echo display('user_access_role')?></a></li>
                        </ul>
                    </li> 
                    <!-- User role assign here end-->

                </ul>
            </li>
            <!-- Software Settings menu end -->
            <?php } ?>
	    </ul>
	</div> <!-- /.sidebar -->
</aside>