<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Add permission setup start -->
<div class="content-wrapper">
    <section class="content-header">
        <div class="header-icon">
            <i class="pe-7s-note2"></i>
        </div>
        <div class="header-title">
            <h1><?php echo display('add_menu') ?></h1>
            <small><?php echo display('add_menu') ?></small>
            <ol class="breadcrumb">
                <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
                <li><a href="#"><?php echo display('web_settings') ?></a></li>
                <li class="active"><?php echo display('add_menu') ?></li>
            </ol>
        </div>
    </section>

    <section class="content">

        <!-- Alert Message -->
        <?php
            $message = $this->session->userdata('message');
            if (isset($message)) {
        ?>
        <div class="alert alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $message ?>                    
        </div>
        <?php 
            $this->session->unset_userdata('message');
            }
            $error_message = $this->session->userdata('error_message');
            $validatio_error = validation_errors();
            if (($error_message || $validatio_error)) {
        ?>
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $error_message ?>                    
            <?php echo $validatio_error ?>                    
        </div>
        <?php 
            $this->session->unset_userdata('error_message');
            }
        ?>

        <div class="row">
            <div class="col-sm-12">
                <div class="column">
                
                  <a href="<?php echo base_url('permission_setup/menu_item_list')?>" class="btn btn-success m-b-5 m-r-2"><i class="ti-align-justify"> </i> <?php echo display('manage_menu')?></a>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4><?php echo display('add_menu') ?> </h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <?php echo form_open_multipart("permission_setup/save") ?>
                        <div class="form-group row">
                            <label for="menu_title"  class="col-sm-3 col-form-label" >Menu title</label>
                            <div class="col-sm-9">
                                <input name="menu_title" class="form-control" required type="text">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="page_url" class="col-sm-3  col-form-label">Page url</label>
                            <div class="col-sm-9">
                                <input name="page_url" class="form-control" type="text">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="module" class="col-sm-3 col-form-label">Module</label>
                            <div class="col-sm-9">
                                <input name="module" class="form-control" required type="text">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="parent_menu" class="col-sm-3 col-form-label">Parent menu</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="parent_menu" id="parent_menu">
                                    <option value="">--Select--</option>
                                    <?php 
                                       foreach($p_menu as $val){
                                        echo '<option value="'.$val->menu_id.'">'.$val->menu_title.'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group text-right">
                            <button type="reset" class="btn btn-primary w-md m-b-5"><?php echo display('reset') ?></button>
                            <button type="submit" class="btn btn-success w-md m-b-5">Add</button>
                        </div>
                        <?php echo form_close() ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Add permission setup end -->

