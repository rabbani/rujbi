<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Manage Menu list start -->
<div class="content-wrapper">
    <section class="content-header">
        <div class="header-icon">
            <i class="pe-7s-note2"></i>
        </div>
        <div class="header-title">
            <h1>Manage Menu List</h1>
            <small>Manage Menu List</small>
            <ol class="breadcrumb">
                <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
                <li><a href="#"><?php echo display('web_settings') ?></a></li>
                <li class="active">Manage Menu List</li>
            </ol>
        </div>
    </section>

    <section class="content">

        <!-- Alert Message -->
        <?php
            $message = $this->session->userdata('message');
            if (isset($message)) {
        ?>
        <div class="alert alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $message ?>                    
        </div>
        <?php 
            $this->session->unset_userdata('message');
            }
            $error_message = $this->session->userdata('error_message');
            $validatio_error = validation_errors();
            if (($error_message || $validatio_error)) {
        ?>
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $error_message ?>                    
            <?php echo $validatio_error ?>                    
        </div>
        <?php 
            $this->session->unset_userdata('error_message');
            }
        ?>

        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4>Manage Menu List </h4>
                        </div>
                    </div>
                    <div class="">
                        <table class=" table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th><?php echo display('sl_no') ?></th>
                                    <th>Menu title</th>
                                    <th>Page_url</th>
                                    <th>Module</th>
                                    <th>Parent menu</th>
                                    <th><?php echo display('status') ?></th>
                                    <th width="100"><?php echo display('action') ?></th> 
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($menu_item_list)) ?>
                                <?php $sl = 1; ?>
                                <?php foreach ($menu_item_list as $value) { 
                                    $parent = $this->db->select('menu_title')->where('menu_id',$value->parent_menu)->get('sec_menu_item')->row();
                                ?>
                                <tr>
                                    <td><?php echo $sl++; ?></td>
                                   <td><?php echo $value->menu_title; ?></td>
                                    <td><?php echo $value->page_url; ?></td>
                                    <td><?php echo $value->module; ?></td>
                                    <td><?php echo @$parent->menu_title; ?></td>
                                    <td><?php echo (($value->is_report==1)?'Is Report':'Not Report'); ?></td>
                                    <td>
                                        <a href="<?php echo base_url("permission_setup/edit/$value->menu_id") ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        <a href="<?php echo base_url("permission_setup/delete/$value->menu_id") ?>" onclick="return confirm('<?php echo display("are_you_sure") ?>')" class="btn btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                <?php } ?> 
                            </tbody>
                        </table>
                        <?php echo @$links;?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Manage Menu list end -->



 