<?php

$cache_file = "customer.json";
   header('Content-Type: text/javascript; charset=utf8');
?>
var customerList = <?php echo file_get_contents($cache_file); ?> ; 

APchange = function(event, ui){
	$(this).data("autocomplete").menu.activeMenu.children(":first-child").trigger("click");
}
    $(function() {
      
        $( ".customerSelection" ).autocomplete(
		{
            source:customerList,
			delay:300,
			focus: function(event, ui) {
				$(this).parent().find(".customer_hidden_value").val(ui.item.value);
				$(this).val(ui.item.label);
				return false;
			},
			select: function(event, ui) {
				$(this).parent().find(".customer_hidden_value").val(ui.item.value);
				$(this).val(ui.item.label);
				var base_url = $("#base_url").val();
				$(this).unbind("change");

				$.ajax({
            		type: "post",
            		async: true,
            		url: base_url+'retrive_customer_name',
            		data: {customer_id:ui.item.value},
            		success: function(data) {
            			$('#customer_name').html('<p style="color: green;font-size: 14px;font-style: italic;font-weight: bold;">'+data+'</p>');
		            },
		            error: function() {
		                alert('Request Failed, Please check your code and try again!');
		            }
		        });

				return false;
			}
		});

		$( ".customerSelection" ).focus(function(){
			$(this).change(APchange);
		});
    });
