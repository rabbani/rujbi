$(document).ready(function () {
    "use strict";

    //form validate
    $("#validate").validate();

    //Select 2 
    $("select.form-control:not(.dont-select-me)").select2({
        placeholder: "Select option",
        allowClear: true
    });

    //Product list scrolling
    $('.product-list').slimScroll({
        size: '3px',
        height: '345px',
        allowPageScroll: true,
        railVisible: true
    });

    //Product Grid scrolling
    $('.product-grid').slimScroll({
        size: '3px',
        height: '720px',
        allowPageScroll: true,
        railVisible: true
    });

    //summernote
    $('.summernote').summernote({
        height: 300, // set editor height
        minHeight: null, // set minimum height of editor
        maxHeight: null, // set maximum height of editor
        focus: true     // set focus to editable area after initializing summernote
    });

    // Message
    $('.message_inner').slimScroll({
        size: '3px',
        height: '320px'
    });

    //Date picker
    $('.datepicker-here').datepicker({
        language: 'en',
        dateFormat: 'yyyy-mm-dd',
    });

    //Date picker manage
    $('.datepicker-manage').datepicker({
        language: 'en',
        dateFormat: 'yyyy-mm-dd',
    });

    //Select current date
    // var dp = $('.datepicker-here').datepicker().data('datepicker');
    // if (typeof(dp) != "undefined") {
    //     dp.selectDate(new Date());
    // }
});